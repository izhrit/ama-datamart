drop temporary table if exists rkind;
create temporary table rkind
(
  casenumber      varchar(10),
  procedure_type  char(1),
  text            varchar(100),
  Queue_number    varchar(5),
  iLender         int,
  iClaim          int
);

LOAD DATA LOCAL
INFILE 'D:\\work\\vva\\Products\\AMA\\datamart\\doc\\rkind\\rkind.csv' 
INTO TABLE rkind
character set utf8
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(casenumber,procedure_type,text,Queue_number,iLender,iClaim)
;

select '';
select 
  count(*) `треб.`
  , count(distinct text) `видов` 
from rkind;

set @count_rkind:= (select count(*)  from rkind);

select '' 'статистика требований по очередям:';
select 
	rpad(Queue_number,4,' ') `очередь`
	, lpad(count(*),4,' ') `треб.` 
	, lpad(concat(100*count(*)/@count_rkind,' %'),10,' ') `_____доля` 
	,  CASE Queue_number 
         WHEN '1' THEN 'ущерб'
         WHEN '2' THEN 'зарплата' 
         WHEN '3' THEN 'коммерческие кроме штрафов' 
         WHEN '3.1' THEN 'штрафы' 
         ELSE Queue_number
	   END
      `название очереди`
from rkind 
group by `очередь`
order by Queue_number;

drop temporary table if exists rkind_stat;
create temporary table rkind_stat
(
   q1   int
  ,q2   int
  ,q3   int
  ,q31  int
  ,q4   int
  ,q41  int
  ,q5   int
  ,q57  int
  ,q6   int
  ,t    int
  ,text varchar(100)
);

insert into rkind_stat
select 
   sum(if(Queue_number='1',1,0))
  ,sum(if(Queue_number='2',1,0))
  ,sum(if(Queue_number='3',1,0))
  ,sum(if(Queue_number='3.1',1,0))
  ,sum(if(Queue_number='4',1,0))
  ,sum(if(Queue_number='4.1',1,0))
  ,sum(if(Queue_number='5',1,0))
  ,sum(if(Queue_number='57',1,0))
  ,sum(if(Queue_number='6',1,0))
  ,count(*)
  ,text
from rkind
where length(rkind.text)>=4 and concat('',rkind.text * 1) <> rkind.text
group by text;

select 'количество использование разных видов требований:' ``;
select 
  lpad(count(*),6,' ') `треб.`
  , if (1=t,' 1 раз',
    if (2=t,' 2 раза',
    if (3=t,' 3 раза',
    if (4=t,' 4 раза',
    if (5=t,' 5 раз',
    if (6=t,' 6 раз',
    if (7=t,' 7 раз',
    if (8=t,' 8 раз',
    if (9=t,' 9 раз',
            '10 и более раз'))))))))) `использований` 
from rkind_stat
group by `использований` 
order by `использований`;

delimiter //
drop function if exists fine_percent//
create function fine_percent(q int, t int, m int)
returns varchar(4)
deterministic
begin
	declare p float;
	select round(100*q/t,0) into p;
	return lpad(if(m>p,'',concat(p,'%')),4,' ');
end //
delimiter ;

select '' 'распределение по очередям top150 популярных видов требований:';
select 
   fine_percent(q1,t,10)  `___1`
  ,fine_percent(q2,t,10)  `___2`
  ,fine_percent(q3,t,10)  `___3`
  ,fine_percent(q31,t,10) `_3.1`
  ,lpad(concat(t,'(',fine_percent(t,@count_rkind,1),')'),  11,' ') `______total`
  ,text
from rkind_stat
order by t desc
limit 150;

set @count_rkind_q1:= (select count(*)  from rkind where '1'=Queue_number);

select '' 'самые популярные 30 видов требований 1й очереди:';
select 
    Queue_number `оч`
  , lpad(concat(count(*),'(',fine_percent(count(*),@count_rkind_q1,1),')'),  11,' ') `_________тр`
  , rpad(left(rkind.text,60),60,' ') `вид_требования_____________________________________________`
  , fine_percent(q2,t,10)  `______в_2`
  , fine_percent(q3,t,10)  `______в_3`
  , fine_percent(q31,t,10) `______в_3.1`
from rkind
inner join rkind_stat on rkind_stat.text=rkind.text
where Queue_number in ('1') and length(rkind.text)>=4 and concat('',rkind.text * 1) <> rkind.text
group by `оч`, rkind.text
order by `оч`, `_________тр` desc
limit 30
;

set @count_rkind_q2:= (select count(*)  from rkind where '2'=Queue_number);

select '' 'самые популярные 30 видов требований 2й очереди:';
select 
    Queue_number `оч`
  , lpad(concat(count(*),'(',fine_percent(count(*),@count_rkind_q2,1),')'),  11,' ') `_________тр`
  , rpad(left(rkind.text,60),60,' ') `вид_требования________________________________________________`
  , fine_percent(q1,t,10)  `__в_1`
  , fine_percent(q3,t,10)  `__в_3`
  , fine_percent(q31,t,10) `__в_3.1`
from rkind
inner join rkind_stat on rkind_stat.text=rkind.text
where Queue_number in ('2') and length(rkind.text)>=4 and concat('',rkind.text * 1) <> rkind.text
group by `оч`, rkind.text
order by `оч`, `_________тр` desc
limit 30
;

set @count_rkind_q3:= (select count(*)  from rkind where '3'=Queue_number);

select '' 'самые популярные 30 видов требований 3й очереди:';
select 
    Queue_number `оч`
  , lpad(concat(count(*),'(',fine_percent(count(*),@count_rkind_q3,1),')'),  11,' ') `_________тр`
  , rpad(left(rkind.text,60),60,' ') `вид_требования________________________________________________`
  , fine_percent(q1,t,10)  `__в_1`
  , fine_percent(q2,t,10)  `__в_2`
  , fine_percent(q31,t,10) `__в_3.1`
from rkind
inner join rkind_stat on rkind_stat.text=rkind.text
where Queue_number in ('3') and length(rkind.text)>=4 and concat('',rkind.text * 1) <> rkind.text
group by `оч`, rkind.text
order by `оч`, `_________тр` desc
limit 30
;

set @count_rkind_q31:= (select count(*)  from rkind where '3.1'=Queue_number);
select '' 'самые популярные 30 видов требований 3й очереди "штрафы":';
select 
    Queue_number `оч`
  , lpad(concat(count(*),'(',fine_percent(count(*),@count_rkind_q31,1),')'),  11,' ') `_________тр`
  , rpad(left(rkind.text,60),60,' ') `вид_требования________________________________________________`
  , fine_percent(q1,t,10)  `__в_1`
  , fine_percent(q2,t,10)  `__в_2`
  , fine_percent(q3,t,10) `__в_3`
from rkind
inner join rkind_stat on rkind_stat.text=rkind.text
where Queue_number in ('3.1') and length(rkind.text)>=4 and concat('',rkind.text * 1) <> rkind.text
group by `оч`, rkind.text
order by `оч`, `_________тр` desc
limit 30
;
