@set SERVICENAME=mysql57
@set DATADIR=C:\ProgramData\MySQL\MySQL Server 5.7\data\

sc stop %SERVICENAME%
:loop1
@sc query %SERVICENAME% | find "STOPPED"
@if errorlevel 1 (
  @timeout 1
  sc query %SERVICENAME% | find "Состояние"
  @goto loop1
)

xcopy /E "%DATADIR%*" M:\
copy /Y my.ram.ini my.ini

sc start %SERVICENAME%
:loop2
@sc query %SERVICENAME% | find "RUNNING"
@if errorlevel 1 (
  @timeout 1
  sc query %SERVICENAME% | find "Состояние"
  @goto loop2
)

