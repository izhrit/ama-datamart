@set SERVICENAME=mysql57

sc stop %SERVICENAME%
:loop1
@sc query %SERVICENAME% | find "STOPPED"
@if errorlevel 1 (
  @timeout 1
  sc query %SERVICENAME% | find "Состояние"
  @goto loop1
)

copy /Y my.org.ini my.ini

sc start %SERVICENAME%
:loop2
@sc query %SERVICENAME% | find "RUNNING"
@if errorlevel 1 (
  @timeout 1
  sc query %SERVICENAME% | find "Состояние"
  @goto loop2
)

