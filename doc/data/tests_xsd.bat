@pushd %~dp0
@set xml_dir=%~dp0..\..\src\uc\forms\ama\datamart\procedure\registry\tests\
@set xsd_fname=rtk-2.xsd
@call :test_xml rtk-example0
@call :test_xml rtk-example1
@call :test_xml rtk-example2
@call :test_xml rtk-example3
@call :test_xml rtk-example4
@call :test_xml rtk-example5
@popd
@exit

:test_xml
@call CScript //nologo scripts\tests.js %xml_dir%%1.xml %xsd_fname% %1.normalized.xml
@exit /B