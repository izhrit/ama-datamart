
if (WScript.FullName.indexOf("cscript.exe")==-1)
{
  WScript.Echo("This script may be runned only with CScript..  Try to run create_sql.bat")
  WScript.Quit(1)
}

var fso = new ActiveXObject("Scripting.FileSystemObject");
var shell= new ActiveXObject("WScript.Shell");

var LoadXsd = function (fpath_xsd)
		{
			var xsd_doc = new ActiveXObject('MSXML2.DOMDocument.6.0');
			var res = xsd_doc.load(fpath_xsd);
			if (res)
			{
				return xsd_doc;
			}
			else
			{
				WScript.Echo('loading of xsd scheme is failed!');
				return null;
			}
		}

function CheckXml(fpath_in,fpath_xsd,fpath_out)
{
  var doc = new ActiveXObject("MSXML2.DOMDocument.6.0");
  doc.async = 'false';
  WScript.Echo('fpath_in=' + fpath_in);
  doc.validateOnParse = 'true';

  var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
  xs.add('myNamespace', LoadXsd(fpath_xsd));
  doc.schemas= xs;

  doc.load(fpath_in);

  if (doc && doc.documentElement && !doc.getElementsByTagName('parsererror').length)
  {
    doc.save(fpath_out);
    WScript.Echo('fpath_out=' + fpath_out);
  }
  else
  {
    WScript.Echo('wrong xml:');
    if (!doc)
    {
      WScript.Echo('!doc');
    }
    else
    {
      if (!doc.documentElement)
        WScript.Echo('!doc.documentElement');
      // WScript.Echo(doc.getElementsByTagName('parsererror'));
      WScript.Echo(doc.parsererror);
      if (doc.parseError && 0 != doc.parseError.errorCode)
      {
        var exmsg = "\nReason: " + doc.parseError.reason +
          "\nLine: " + doc.parseError.line + "\n";
        WScript.Echo(exmsg);
      }
    }
  }
}

var varg= WScript.Arguments;
CheckXml(varg.Item(0),varg.Item(1),varg.Item(2));
