﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ama_dm_ztest_controls
{
    public partial class FormTestMaster : Form
    {
        public FormTestMaster()
        {
            InitializeComponent();

            buttonExposure0.Anchor= (AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right);
            buttonExposure1.Anchor = (AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right);
            buttonExposure5.Anchor = (AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right);
            buttonExposure6.Anchor = (AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right);
            buttonObjectKM.Anchor = (AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right);
        }

        bool m_TreeMode = false;
        bool m_ShowOnlyActual = false;
        private void linkLabelEfrsbMessageType_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (ama.datamart.FormChooseEfrsbMessageType frm = new ama.datamart.FormChooseEfrsbMessageType())
            {
                frm.Settings.TreeMode = m_TreeMode;
                frm.Settings.ShowOnlyActual = m_ShowOnlyActual;
                frm.Settings.implemented = new string[] {
                    "об СК"
                    ,"Сведения о результатах торгов"
                    ,"f"
                };
                frm.Settings.actual = new string[] {
                    "о результатах заседания КК"
                    ,"Сведения о собрании кредиторов"
                    ,"b"
                };
                frm.Settings.CurrentProcedureName = "Тестовый вид процедуры";
                DialogResult res = frm.ShowDialog(this);
                if (DialogResult.OK == res)
                {
                    linkLabelEfrsbMessageType.Text = frm.Choosed_Efrsb_message_type.Readable_short_about;
                    m_TreeMode = frm.Settings.TreeMode;
                    m_ShowOnlyActual = frm.Settings.ShowOnlyActual;
                }
            }
        }

        private void buttonExposure0_Click(object sender, EventArgs e)
        {
            DoExposure(0);
        }

        private void buttonExposure1_Click(object sender, EventArgs e)
        {
            DoExposure(1);
        }

        private void buttonExposure5_Click(object sender, EventArgs e)
        {
            DoExposure(5,"ООО Роги и ноги");
        }

        private void buttonExposure6_Click(object sender, EventArgs e)
        {
            DoExposure(6);
        }

        void DoExposure(int CountOfSelectedObjects= 0, string ProcedureName = null)
        {
            if (string.IsNullOrEmpty(ProcedureName))
                ProcedureName = "ООО \"Гладиолус\"";
            using (ama.datamart.FormExposureSettings frm = new ama.datamart.FormExposureSettings())
            {
                frm.ProcedureName = ProcedureName;
                frm.CountOfSelectedObjects = CountOfSelectedObjects;
                PrepareExposureSettings(frm.Settings);
                DialogResult res= frm.ShowDialog(this);
                if (DialogResult.OK==res)
                {

                }
            }
        }

        static readonly ama.datamart.ExposureRecipientPro recipient_Банкрот_форум = new ama.datamart.ExposureRecipientPro
        {
            id = "1"
            , name = "Банкрот форум"
            , url = "https://bankrotforum.ru/"
            , descr= "Помощь в организации торгов"
        };
        static readonly ama.datamart.ExposureRecipientPro recipient_Портал_ДА = new ama.datamart.ExposureRecipientPro
        {
            id = "2"
            , name = "Портал ДА"
            , url = "https://portal-da.ru/"
            , descr = "Публикация информации об имуществе в наличии максимально широкому, неопределённому кругу лиц"
        };
        static readonly ama.datamart.ExposureRecipientPro recipient_Наш_любимый_оценщик = new ama.datamart.ExposureRecipientPro
        {
            id = "3"
            , name = "Агентство независимых экспертиз «Гранд Истейт»"
            , url = "https://grand-ocenka.ru"
            , descr = "Оценка имущества"
        };
        void PrepareExposureSettings(ama.datamart.Exposure_settings settings)
        {
            settings.recipients.Clear();
            settings.recipients.Add(recipient_Наш_любимый_оценщик);
            settings.recipients.Add(recipient_Банкрот_форум);
            settings.recipients.Add(recipient_Портал_ДА);
        }

        ama.datamart.Данные_вкладки_экспозиция_объекта_КМ exposure = null;

        static void FillTestClasses(List<ama.datamart.Efrsb_property_class> classes)
        {
            classes.Add(new ama.datamart.Efrsb_property_class { code = "0101003", name = "Здания и сооружения предприятий лесной, деревообрабатывающей, целлюлозно-бумажной, стекольной, фарфоро-фаянсовой, полиграфической промышленности и предприятий промышленности строительных материалов" });
            classes.Add(new ama.datamart.Efrsb_property_class { code = "0101004", name = "Здания предприятий легкой, пищевой, микробиологической, мукомольно-крупяной, комбикормовой и медицинской промышленности" });
            classes.Add(new ama.datamart.Efrsb_property_class { code = "0101010", name = "Здания для органов государственного управления, обороны, государственной безопасности, финансов и иностранных представительств" });
        }

        static void FillTestRecipients(ama.datamart.ExposureRecipients Круг_лиц_с_доступом)
        {
            Круг_лиц_с_доступом.Transmit = true;
            Круг_лиц_с_доступом.ToSelectedPro = true;
            Круг_лиц_с_доступом.SelectedPro.Add(recipient_Банкрот_форум);
        }

        void SafeInitExposure()
        {
            if (null == exposure)
            {
                exposure = new ama.datamart.Данные_вкладки_экспозиция_объекта_КМ();
                exposure.Категория = ama.datamart.Категория_ОбъектаКМ_для_экспозиции.Недвижимость;
                exposure.Стадия = ama.datamart.Стадия_экспозиции_ОбъектаКМ.Инвентаризация;

                FillTestRecipients(exposure.Круг_лиц_с_доступом);
                FillTestClasses(exposure.КлассификацияЕФРСБ);
            }
        }

        private void buttonObjectKM_Click(object sender, EventArgs e)
        {
            SafeInitExposure();
            using (FormKM frm = new FormKM())
            {
                PrepareExposureSettings(frm.ucExposure.ucExposureRecipients.Settings);
                frm.ucExposure.DataLoad(exposure);
                DialogResult res = frm.ShowDialog(this);
                if (DialogResult.OK == res)
                {
                    frm.ucExposure.DataSave(exposure);
                }
            }
        }
    }
}
