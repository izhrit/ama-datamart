﻿namespace ama_dm_ztest_controls
{
    partial class FormTestMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelEfrsbMessageType = new System.Windows.Forms.Label();
            this.linkLabelEfrsbMessageType = new System.Windows.Forms.LinkLabel();
            this.buttonExposure0 = new System.Windows.Forms.Button();
            this.buttonExposure1 = new System.Windows.Forms.Button();
            this.buttonExposure5 = new System.Windows.Forms.Button();
            this.buttonExposure6 = new System.Windows.Forms.Button();
            this.buttonObjectKM = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelEfrsbMessageType
            // 
            this.labelEfrsbMessageType.AutoSize = true;
            this.labelEfrsbMessageType.Location = new System.Drawing.Point(12, 9);
            this.labelEfrsbMessageType.Name = "labelEfrsbMessageType";
            this.labelEfrsbMessageType.Size = new System.Drawing.Size(152, 13);
            this.labelEfrsbMessageType.TabIndex = 2;
            this.labelEfrsbMessageType.Text = "Тип сообщения для ЕФРСБ:";
            // 
            // linkLabelEfrsbMessageType
            // 
            this.linkLabelEfrsbMessageType.AutoSize = true;
            this.linkLabelEfrsbMessageType.Location = new System.Drawing.Point(22, 22);
            this.linkLabelEfrsbMessageType.Name = "linkLabelEfrsbMessageType";
            this.linkLabelEfrsbMessageType.Size = new System.Drawing.Size(265, 13);
            this.linkLabelEfrsbMessageType.TabIndex = 3;
            this.linkLabelEfrsbMessageType.TabStop = true;
            this.linkLabelEfrsbMessageType.Text = "Кликните, чтобы выбрать тип сообщения ЕФРСБ..";
            this.linkLabelEfrsbMessageType.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelEfrsbMessageType_LinkClicked);
            // 
            // buttonExposure0
            // 
            this.buttonExposure0.Location = new System.Drawing.Point(15, 55);
            this.buttonExposure0.Name = "buttonExposure0";
            this.buttonExposure0.Size = new System.Drawing.Size(356, 23);
            this.buttonExposure0.TabIndex = 4;
            this.buttonExposure0.Text = "Настройки экспозиции для 0 выбранных объектов";
            this.buttonExposure0.UseVisualStyleBackColor = true;
            this.buttonExposure0.Click += new System.EventHandler(this.buttonExposure0_Click);
            // 
            // buttonExposure1
            // 
            this.buttonExposure1.Location = new System.Drawing.Point(15, 84);
            this.buttonExposure1.Name = "buttonExposure1";
            this.buttonExposure1.Size = new System.Drawing.Size(356, 23);
            this.buttonExposure1.TabIndex = 5;
            this.buttonExposure1.Text = "Настройки экспозиции для 1 выбранного объекта";
            this.buttonExposure1.UseVisualStyleBackColor = true;
            this.buttonExposure1.Click += new System.EventHandler(this.buttonExposure1_Click);
            // 
            // buttonExposure5
            // 
            this.buttonExposure5.Location = new System.Drawing.Point(15, 113);
            this.buttonExposure5.Name = "buttonExposure5";
            this.buttonExposure5.Size = new System.Drawing.Size(356, 23);
            this.buttonExposure5.TabIndex = 6;
            this.buttonExposure5.Text = "Настройки экспозиции для 5 выбранных объектов";
            this.buttonExposure5.UseVisualStyleBackColor = true;
            this.buttonExposure5.Click += new System.EventHandler(this.buttonExposure5_Click);
            // 
            // buttonExposure6
            // 
            this.buttonExposure6.Location = new System.Drawing.Point(15, 142);
            this.buttonExposure6.Name = "buttonExposure6";
            this.buttonExposure6.Size = new System.Drawing.Size(356, 23);
            this.buttonExposure6.TabIndex = 7;
            this.buttonExposure6.Text = "Настройки экспозиции для 6 выбранных объектов";
            this.buttonExposure6.UseVisualStyleBackColor = true;
            this.buttonExposure6.Click += new System.EventHandler(this.buttonExposure6_Click);
            // 
            // buttonObjectKM
            // 
            this.buttonObjectKM.Location = new System.Drawing.Point(15, 189);
            this.buttonObjectKM.Name = "buttonObjectKM";
            this.buttonObjectKM.Size = new System.Drawing.Size(356, 23);
            this.buttonObjectKM.TabIndex = 10;
            this.buttonObjectKM.Text = "Объект конкурсной массы";
            this.buttonObjectKM.UseVisualStyleBackColor = true;
            this.buttonObjectKM.Click += new System.EventHandler(this.buttonObjectKM_Click);
            // 
            // FormTestMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 226);
            this.Controls.Add(this.buttonObjectKM);
            this.Controls.Add(this.buttonExposure6);
            this.Controls.Add(this.buttonExposure5);
            this.Controls.Add(this.buttonExposure1);
            this.Controls.Add(this.buttonExposure0);
            this.Controls.Add(this.linkLabelEfrsbMessageType);
            this.Controls.Add(this.labelEfrsbMessageType);
            this.MinimumSize = new System.Drawing.Size(390, 200);
            this.Name = "FormTestMaster";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Классификация имущества";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelEfrsbMessageType;
        private System.Windows.Forms.LinkLabel linkLabelEfrsbMessageType;
        private System.Windows.Forms.Button buttonExposure0;
        private System.Windows.Forms.Button buttonExposure1;
        private System.Windows.Forms.Button buttonExposure5;
        private System.Windows.Forms.Button buttonExposure6;
        private System.Windows.Forms.Button buttonObjectKM;
    }
}

