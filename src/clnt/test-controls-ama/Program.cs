﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Forms;

namespace ama_dm_ztest_controls
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ProcessArgs(args);
            Application.Run(m_Form);
        }

        static Form m_Form;
        static void ProcessArgs(string [] args)
        {
            foreach (string arg in args)
            {
                string [] aparts = arg.Split('=');
                string opt = aparts[0];
                switch (opt)
                {
                    case "--form": PrepareForm(aparts[1]); break;
                    case "--web-root": ama.datamart.StaticHtmlControl.Web_root_directory_path= aparts[1]; break;
                }
            }
            if (null == ama.datamart.StaticHtmlControl.Web_root_directory_path)
                InitDefaultWebRoot();
            if (null== m_Form)
                m_Form = new FormTestMaster();
        }

        static void InitDefaultWebRoot()
        {
            string StartupPath= Application.StartupPath;
            ama.datamart.StaticHtmlControl.Web_root_directory_path= Path.Combine(StartupPath, @"..\..\..\ama-dm-controls\phtml\");
        }

        static void PrepareForm(string form)
        {
            switch (form)
            {
                case "FormChooseEfrsbMessageType":
                    m_Form = new ama.datamart.FormChooseEfrsbMessageType();
                    break;
                case "StartExposure":
                    m_Form = new ama.datamart.FormExposureSettings();
                    break;
                case "AssetExposure":
                    m_Form = new FormKM();
                    break;
                case "FormTestEfrsbClasses":
                default:
                    m_Form = new FormTestMaster();
                    break;
            }
        }
    }
}
