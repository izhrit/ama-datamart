﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ama_dm_ztest_controls
{
    public partial class FormKM : Form
    {
        public FormKM()
        {
            InitializeComponent();

            tabControl.Anchor |= (AnchorStyles.Bottom | AnchorStyles.Right);
            ucExposure.Anchor |= (AnchorStyles.Bottom | AnchorStyles.Right);
            buttonCancel.Anchor= (AnchorStyles.Bottom | AnchorStyles.Right);
            buttonOK.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
        }
    }
}
