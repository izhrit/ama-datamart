using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Web.Script.Serialization;

using ama.datamart;

namespace test_ama_dm_client
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            ProcessArgs(args);
            switch (action)
            {
                case "prepare-auth":        test_PrepareAuth(); break;
                case "auth":                TestAuth();  break;
                case "auth-to-upload":      TestAuth_to_upload(); break;
                case "view-report":         TestViewReport(); break;
                case "sub-level":           TestGetSubLevelInfo(); break;
                case "get-anketa-np":       TestGetAnketaNP(); break;
                case "get-anketa-np-list":  TestGetAnketaNPList(); break;
                case "using-procedure":     TestUsingProcedure(); break;
                case "create-assembly":     TestCreateAssembly(); break;
                case "delete-assembly":     TestDeleteAssembly(); break;
                case "get-starts":          TestGetStarts(); break;

                case "get-exposure-recipients": TestGetExposureRecipientProList(); break;
                case "store-exposure-objects": TestStoreExposureObjects(); break;
                case "get-exposure-digests": TestGetExposureDigests(); break;
                case "sync-exposure": TestSyncExposure(); break;
                case "md5-exposure": TestExposure_md5(); break;
                case "md5-text-exposure": TestExposure_md5_text(); break;

                case "store-exposure-document": TestStoreExposureDocument(); break;
                case "delete-exposure-documents": TestDeleteExposureDocuments(); break;

                case "get-wcalendars":      TestGetWcalendar(); break;
                case "local-cal-show":      TestLocalWcalendarShow(); break;
                case "local-cal-update":    TestLocalWcalendarUpdate(); break;
                case "local-cal-days":      TestLocalWcalendarDays(); break;

                case "upload":
                default:                    TestUpload();  break;
            }
        }

        static void test_PrepareAuth()
        {
            if (null==test_time)
                test_time= DateTime.Now;
            string auth = Client.PrepareUrlArgument_auth(license_token, contract, manager, test_time);
            Console.Write(auth);
        }

        static void TestViewReport()
        {
            TestReportView frm= new TestReportView();

            string relative_path_to_index_html = @"..\..\report\index.html";
            string path_to_app_exe_folder = Path.GetDirectoryName(Application.ExecutablePath);
            frm.PathToIndex = Path.Combine(path_to_app_exe_folder, relative_path_to_index_html);

            frm.ReportPath = xml_file_to_load;

            frm.ShowDialog();
        }

        static void TestAuth()
        {
            Client c = new Client(dm_base_url);
            try
            {
                c.Auth(login, password, category);
                Console.Out.WriteLine("c.access_token.Length={0}", c.access_token.Length);
            }
            catch
            {
                Console.Out.WriteLine("can not authorize!");
            }
        }

        static void TestAuth_to_upload()
        {
            if (null == test_time)
                test_time = DateTime.Now;
            Client c = new Client(dm_base_url);
            c.test_mode = true;
            try
            {
                c.Auth(license_token, contract, manager, test_time);
                Console.Out.WriteLine("c.access_token.Length={0}", c.access_token.Length);
            }
            catch
            {
                Console.Out.WriteLine("can not authorize!");
            }
        }

        static Client Auth()
        {
            Client c = new Client(dm_base_url);
            if (!string.IsNullOrEmpty(access_token))
            {
                c.access_token = access_token;
            }
            else
            {
                try
                {
                    c.Auth(login, password, category);
                }
                catch (Exception)
                {
                    Console.Out.WriteLine("can not authorize!");
                    return null;
                }
            }
            return c;
        }

        static void TraceDebtor(Client.Debtor debtor)
        {
            if (null!= debtor)
            {
                Console.Out.WriteLine("  debtor:");
                Console.Out.WriteLine("    name        :{0}", debtor.name);
                Console.Out.WriteLine("    inn         :{0}", debtor.inn);
                Console.Out.WriteLine("    ogrn        :{0}", debtor.ogrn);
                Console.Out.WriteLine("    snils       :{0}",debtor.snils);
            }
        }

        static void TraceManager(Client.Manager_for_auth manager)
        {
            if (null != manager)
            {
                Console.Out.WriteLine("  manager:");
                Console.Out.WriteLine("    name        :{0}", manager.name);
                Console.Out.WriteLine("    surname     :{0}", manager.surname);
                Console.Out.WriteLine("    patronymic  :{0}", manager.patronymic);
                Console.Out.WriteLine("    efrsb_number:{0}", manager.efrsb_number);
                Console.Out.WriteLine("    inn         :{0}", manager.inn);
            }
        }

        static void TraceProcedure(Client.Procedure procedure)
        {
            if (null != procedure)
            {
                Console.Out.WriteLine("  procedure:");
                Console.Out.WriteLine("    case_number :{0}", procedure.case_number);
                Console.Out.WriteLine("    type        :{0}", procedure.type);
            }
        }

        static void TraceConstraints(Client.Constraints constraints)
        {
            Console.Out.WriteLine("constraints problems:");
            if (null!=constraints.problems && 0!= constraints.problems.Count)
            {
                foreach (Client.Constraints_problem p in constraints.problems)
                    Console.Out.WriteLine("  {0}:{1}",p.type,p.details);
            }
            Console.Out.WriteLine("constraints parameters:");
            TraceDebtor(constraints.parameters.debtor);
            TraceManager(constraints.parameters.manager);
            TraceProcedure(constraints.parameters.procedure);
        }

        static void TestUpload()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                Client.UploadResponce r= c.Upload(upload_info);
                Console.Out.WriteLine("casebook login=\"{0}\", password=\"{1}\"",r.login,new String('*',r.password.Length));
                if (!string.IsNullOrEmpty(r.error_text))
                    Console.Out.WriteLine(r.error_text);
                if (null != r.constraints)
                    TraceConstraints(r.constraints);
            }
            catch (System.Net.WebException ex)
            {
                Console.Out.WriteLine("can not upload!");
                Console.Out.Write(ex.Message);
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not upload!");
                Console.Out.Write(ex);
                if (null!=ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        static void TestUsingProcedure()
        {
            Client c = new Client(dm_base_url);

            try
            {
                c.UsingProcedure(
                ContractNumber: contract
                , license_token: license_token
                , inn: upload_info.debtor.inn
                , ogrn: upload_info.debtor.ogrn
                , snils: upload_info.debtor.snils
                , section: section
                );
            }
            catch
            {
                Console.Out.Write("got error!");
            }
        }

        static void TestGetAnketaNPList()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                Client.AnketaNP[] alist= c.GetAnketaNPList(q);
                foreach (Client.AnketaNP a in alist)
                {
                    Console.Out.WriteLine("id_MData:{0},Name:{1},SNILS:{2},INN:{3},Email:{4}",a.id_MData,a.Name,a.SNILS,a.INN,a.Email);
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not GetAnketaNPList!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        static void Trace_Дни(string month, System.Collections.Generic.IEnumerable<Client.День_рабочего_календаря> days)
        {
            Console.Out.WriteLine("    {0}:",month);
            foreach (Client.День_рабочего_календаря d in days)
            {
                Console.Out.WriteLine("          {0} - {1} рабочий", d.день,("true"==d.рабочий)?"  ":"НЕ");
            }
        }

        static void Trace_Дни(string month, System.Collections.Generic.IEnumerable<Рабочий_календарь.AДень> days)
        {
            Console.Out.WriteLine("    {0}:", month);
            foreach (Рабочий_календарь.AДень d in days)
            {
                Console.Out.WriteLine("          {0} - {1} рабочий", d.дата.ToShortDateString(), d.рабочий ? "  " : "НЕ");
            }
        }

        static void TraceWcalendarHeader(Рабочий_календарь w)
        {
            Console.Out.WriteLine("Календарь на {0} год, Revision={1}, Регион:\"{2}\" (ОКАТО:{3}):", w.Год, w.Revision, w.Регион.Наименование, w.Регион.ОКАТО);
        }

        static void TraceWcalendar()
        {
            IEnumerable<Рабочий_календарь> календари = Рабочие_календари.Instance.Все();
            if (null!= календари)
            {
                foreach (Рабочий_календарь w in календари)
                {
                    TraceWcalendarHeader(w);
                    Trace_Дни("Январь", w.Дни.Январь);
                    Trace_Дни("Февраль", w.Дни.Февраль);
                    Trace_Дни("Март", w.Дни.Март);
                    Trace_Дни("Апрель", w.Дни.Апрель);
                    Trace_Дни("Май", w.Дни.Май);
                    Trace_Дни("Июнь", w.Дни.Июнь);
                    Trace_Дни("Июль", w.Дни.Июль);
                    Trace_Дни("Август", w.Дни.Август);
                    Trace_Дни("Сентябрь", w.Дни.Сентябрь);
                    Trace_Дни("Октябрь", w.Дни.Октябрь);
                    Trace_Дни("Ноябрь", w.Дни.Ноябрь);
                    Trace_Дни("Декабрь", w.Дни.Декабрь);
                }
            }
        }

        static void TestLocalWcalendarShow()
        {
            Рабочие_календари.Использовать_путь_к_файловому_хранилищу(file_storeage_path);
            Рабочие_календари.Загрузить_из_папки();
            TraceWcalendar();
        }

        static void TestLocalWcalendarDays()
        {
            Рабочие_календари.Использовать_путь_к_файловому_хранилищу(file_storeage_path);
            Рабочие_календари.Загрузить_из_папки();
            foreach (Рабочий_календарь w in Рабочие_календари.Instance.Все())
            {
                TraceWcalendarHeader(w);
                Console.WriteLine(" // пн  |  вт  |  ср  |  чт  |  пт  |  сб  |  вс");
                Trace_Days("Январь", w.Дни.Январь);
                Trace_Days("Февраль", w.Дни.Февраль);
                Trace_Days("Март", w.Дни.Март);
                Trace_Days("Апрель", w.Дни.Апрель);
                Trace_Days("Май", w.Дни.Май);
                Trace_Days("Июнь", w.Дни.Июнь);
                Trace_Days("Июль", w.Дни.Июль);
                Trace_Days("Август", w.Дни.Август);
                Trace_Days("Сентябрь", w.Дни.Сентябрь);
                Trace_Days("Октябрь", w.Дни.Октябрь);
                Trace_Days("Ноябрь", w.Дни.Ноябрь);
                Trace_Days("Декабрь", w.Дни.Декабрь);
            }
        }

        static void Trace_Days(string month, System.Collections.Generic.IEnumerable<Рабочий_календарь.AДень> days)
        {
            Console.Out.WriteLine(" // {0}:", month);
            int last_DayOfWeek = -1;
            foreach (Рабочий_календарь.AДень d in days)
            {
                int DayOfWeek= (int)d.дата.DayOfWeek;
                DayOfWeek--;
                if (-1 == DayOfWeek)
                    DayOfWeek = 6;
                if (-1==last_DayOfWeek)
                {
                    for (var i= 0; i< DayOfWeek; i++)
                        Console.Out.Write("       ");
                }
                Console.Out.Write(d.рабочий ? "  true" : " false");
                Console.Out.Write(",");
                if (6 == DayOfWeek)
                    Console.Out.WriteLine();
                last_DayOfWeek= DayOfWeek;
            }
            Console.Out.WriteLine();
        }

        static void TestLocalWcalendarUpdate()
        {
            Client c = Auth();
            if (null == c)
                return;

            Рабочие_календари.Использовать_путь_к_файловому_хранилищу(file_storeage_path);
            Рабочие_календари.Загрузить_из_папки();

            if (null == test_time)
                test_time = DateTime.Now;

            Console.WriteLine(Рабочие_календари.Обновить_с_сервера(c,test_time));
        }

        static void TestGetWcalendar()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                Client.Рабочий_календарь[] сlist = c.Получить_рабочие_календари(revision_greater_than,year_greater_than);
                foreach (Client.Рабочий_календарь w in сlist)
                {
                    Console.Out.WriteLine("Календарь на {0} год, Revision={1}, Регион:\"{2}\" (ОКАТО:{3}):",w.Год,w.Revision,w.Регион.Наименование, w.Регион.ОКАТО);
                    Trace_Дни("Январь", w.Дни_календаря.Январь);
                    Trace_Дни("Февраль", w.Дни_календаря.Февраль);
                    Trace_Дни("Март", w.Дни_календаря.Март);
                    Trace_Дни("Апрель", w.Дни_календаря.Апрель);
                    Trace_Дни("Май", w.Дни_календаря.Май);
                    Trace_Дни("Июнь", w.Дни_календаря.Июнь);
                    Trace_Дни("Июль", w.Дни_календаря.Июль);
                    Trace_Дни("Август", w.Дни_календаря.Август);
                    Trace_Дни("Сентябрь", w.Дни_календаря.Сентябрь);
                    Trace_Дни("Октябрь", w.Дни_календаря.Октябрь);
                    Trace_Дни("Ноябрь", w.Дни_календаря.Ноябрь);
                    Trace_Дни("Декабрь", w.Дни_календаря.Декабрь);
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not GetProcedureStarts!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        static void TestGetExposureRecipientProList()
        {
            Client c = Auth();
            if (null == c)
                return;

            List<ExposureRecipientPro> recipients = c.GetExposureRecipientProList();
            foreach (ExposureRecipientPro r in recipients)
            {
                Console.Out.WriteLine("id=\"{0}\",name=\"{1}\",url=\"{2}\"",r.id,r.name,r.url);
            }
        }

        static void TestStoreExposureObjects()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                try
                {
                    c.StoreExposureAssets(id_MProcedure,exposure_data_to_store);
                }
                catch (Client.Exception cex)
                {
                    Console.Out.WriteLine("status:{0}",cex.status);
                    Console.Out.WriteLine("response:{0}",cex.response_body);
                    throw cex;
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not StoreExposureAssets!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        static void TestStoreExposureDocument()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                try
                {
                    string filename = Path.GetFileName(filepath);
                    byte[] content = File.ReadAllBytes(filepath);
                    Документ_для_экспозиции_на_загрузку документ= new Документ_для_экспозиции_на_загрузку 
                        { ID_Object = ID_Object, filename = filename, content= content };
                    c.StoreExposureDocument(id_MProcedure, документ);
                }
                catch (Client.Exception cex)
                {
                    Console.Out.WriteLine("status:{0}", cex.status);
                    Console.Out.WriteLine("response:{0}", cex.response_body);
                    throw cex;
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not StoreExposureAssets!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        static void TestDeleteExposureDocuments()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                try
                {
                    JavaScriptSerializer s = new JavaScriptSerializer();
                    string txt_filename_документов = File.ReadAllText(filepath);
                    List<string> filename_документов = s.Deserialize<List<string>>(txt_filename_документов);
                    c.DeleteExposureDocuments(id_MProcedure,ID_Object, filename_документов);
                }
                catch (Client.Exception cex)
                {
                    Console.Out.WriteLine("status:{0}", cex.status);
                    Console.Out.WriteLine("response:{0}", cex.response_body);
                    throw cex;
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not StoreExposureAssets!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        class Test_Источник_имущества_в_ПАУ_для_экспозиции : IИсточник_имущества_в_ПАУ_для_экспозиции2
        {
            string documents_path;
            List<ОбъектКМ_для_экспозиции> объекты;

            public IEnumerator<ОбъектКМ_для_экспозиции_digest> Читать_все_объекты_КМ(Процедура_над_должником процедура)
            {
                объекты.Sort((o1, o2) => { return string.Compare(o1.ID_Object, o2.ID_Object); });
                foreach (ОбъектКМ_для_экспозиции o in объекты)
                {
                    ОбъектКМ_для_экспозиции_digest res= new ОбъектКМ_для_экспозиции_digest
                    {
                        ID_Object = o.ID_Object,
                        md5 = o.md5()
                    };
                    string dir_path = Path.Combine(documents_path, o.ID_Object);
                    if (Directory.Exists(dir_path))
                    {
                        string [] filenames= Directory.GetFiles(dir_path);
                        if (null != filenames && 0 != filenames.Length)
                        {
                            foreach (string filename in filenames)
                            {
                                byte[] content = File.ReadAllBytes(filename);
                                string md5= Hash.md5(content);
                                Документ_к_ОбъектуКМ документ = new Документ_к_ОбъектуКМ { filename= Path.GetFileName(filename), size= content.Length, md5= md5 };
                                res.Документы.Add(документ);
                            }
                        }
                    }
                    yield return res;
                }
            }

            ОбъектКМ_для_экспозиции Internal_Получить_объект_КМ(Процедура_над_должником процедура, string ID_Object)
            {
                foreach (ОбъектКМ_для_экспозиции o in объекты)
                {
                    if (ID_Object == o.ID_Object)
                        return o;
                }
                return null;
            }

            public ОбъектКМ_для_экспозиции Получить_объект_КМ(Процедура_над_должником процедура, string ID_Object)
            {
                throw new Exception("Получить_объект_КМ не реализовано!");
            }

            public List<ОбъектКМ_для_экспозиции> Получить_объекты_КМ(Процедура_над_должником процедура, IList<string> ID_Objects)
            {
                List<ОбъектКМ_для_экспозиции> объекты_КМ = new List<ОбъектКМ_для_экспозиции>();
                foreach (string ID_Object in ID_Objects)
                {
                    ОбъектКМ_для_экспозиции Объект_КМ = Internal_Получить_объект_КМ(процедура,ID_Object);
                    объекты_КМ.Add(Объект_КМ);
                }
                return объекты_КМ;
            }

            public Документ_для_экспозиции_на_загрузку Подготовить_документ_для_загрузки(Процедура_над_должником процедура, string ID_Object, string filename)
            {
                string filepath = Path.Combine(Path.Combine(documents_path, ID_Object), filename);
                byte[] content = File.ReadAllBytes(filepath);
                return new Документ_для_экспозиции_на_загрузку { ID_Object= ID_Object, content=content, filename= filename };
            }

            public Test_Источник_имущества_в_ПАУ_для_экспозиции(string km_objects_path)
            {
                JavaScriptSerializer s = new JavaScriptSerializer();
                объекты = s.Deserialize<List<ОбъектКМ_для_экспозиции>>(File.ReadAllText(km_objects_path));

                documents_path = Path.GetDirectoryName(km_objects_path);
            }
        }

        static void TestExposure_md5()
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            string txt_o= File.ReadAllText(filepath);
            ОбъектКМ_для_экспозиции o = s.Deserialize<ОбъектКМ_для_экспозиции>(txt_o);
            System.Console.Write(o.md5());
        }

        static void TestExposure_md5_text()
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            string txt_o = File.ReadAllText(filepath);
            ОбъектКМ_для_экспозиции o = s.Deserialize<ОбъектКМ_для_экспозиции>(txt_o);
            System.Console.Write(o.to_string_for_md5());
        }

        static void TestSyncExposure()
        {
            Client c = Auth();
            if (null == c)
                return;

            Test_Источник_имущества_в_ПАУ_для_экспозиции источник_пау = new Test_Источник_имущества_в_ПАУ_для_экспозиции(km_objects_path);

            try
            {
                Обмен_данных_экспозиции_КМ обмен = new Обмен_данных_экспозиции_КМ(источник_пау, c);
                обмен.on_Запрос_объектов_с_витрины += Обмен_on_Запрос_объектов_с_витрины;
                обмен.on_Получены_объекты_с_витрины += Обмен_on_Получены_объекты_с_витрины;
                обмен.on_Отправка_информация_об_объектах_на_витрину += Обмен_on_Отправка_информация_об_объектах_на_витрину;
                обмен.on_Отправлена_информация_об_объектах_на_витрину += Обмен_on_Отправлена_информация_об_объектах_на_витрину;

                обмен.on_Отправка_документа_на_витрину += Обмен_on_Отправка_документа_на_витрину;
                обмен.on_Отправлен_документ_на_витрину += Обмен_on_Отправлен_документ_на_витрину;
                обмен.on_Отправка_списка_документов_для_удаления_с_витрины += Обмен_on_Отправка_списка_документов_для_удаления_с_витрины;
                обмен.on_Отправлен_список_документов_для_удаления_с_витрины += Обмен_on_Отправлен_список_документов_для_удаления_с_витрины;
                обмен.Синхронизировать_объекты_КМ_на_экспозицию(procedure_debtor);
            }
            catch (Client.Exception cex)
            {
                Console.Out.WriteLine("status:{0}", cex.status);
                Console.Out.WriteLine("response:{0}", cex.response_body);
                throw cex;
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not StoreExposureAssets!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        private static void Обмен_on_Отправлен_список_документов_для_удаления_с_витрины()
        {
            Console.WriteLine(".. Список документов для удаления с витрины отправлен");
        }

        private static void Обмен_on_Отправка_списка_документов_для_удаления_с_витрины(string ID_Object, List<string> документы)
        {
            Console.WriteLine("Отправка списка документов для удаления с витрин для объекта КМ {0} ({1})..", ID_Object, string.Join(", ", документы.ToArray()));
        }

        private static void Обмен_on_Отправлен_документ_на_витрину()
        {
            Console.WriteLine(".. Документ на витрину отправлен");
        }

        private static void Обмен_on_Отправка_документа_на_витрину(Документ_для_экспозиции_на_загрузку документ)
        {
            Console.WriteLine("Отправка на витрину документа размером {0} b: \"{1}\" для объекта КМ {2} ..",документ.content.Length,документ.filename,документ.ID_Object);
        }

        private static void Обмен_on_Отправлена_информация_об_объектах_на_витрину(List<ОбъектКМ_для_экспозиции> объекты_на_сохранение, List<string> объекты_на_удаление)
        {
            Console.WriteLine(".. Объекты на витрину отправлены");
        }

        private static void Обмен_on_Отправка_информация_об_объектах_на_витрину(int объектов_на_сохранение, int объектов_на_удаление)
        {
            Console.WriteLine("Отправка объектов на витрину (на сохранение: {0}, на удаление: {1}) ..", объектов_на_сохранение, объектов_на_удаление);
        }

        private static void Обмен_on_Получены_объекты_с_витрины(int total_count, int portion_size)
        {
            Console.WriteLine(".. Получена порция объектов с витрины ({0} из {1}) ..", portion_size, total_count);
        }

        private static void Обмен_on_Запрос_объектов_с_витрины(int max_portion_size)
        {
            Console.WriteLine("Запрос порции объектов с витрины (max {0}) ..",max_portion_size);
        }

        static void TestGetExposureDigests()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                Имущество_для_экспозиции_digest d= 
                    !max_portion_size.HasValue
                    ? c.GetExposureAssetDigestsOrderedBy_ID_Object(procedure_debtor,after_ID_Object)
                    : c.GetExposureAssetDigestsOrderedBy_ID_Object(procedure_debtor, after_ID_Object, max_portion_size.Value);
                Console.WriteLine("total_count:{0}",d.total_count);
                Console.WriteLine("after_ID_Object:{0}", d.after_ID_Object);
                Console.WriteLine("count_after_ID_Object:{0}", d.count_after_ID_Object);
                if (null==d.portion || 0==d.portion.Count)
                {
                    Console.Out.WriteLine("no objects!");
                }
                else
                {
                    foreach (ОбъектКМ_для_экспозиции_digest o in d.portion)
                    {
                        Console.WriteLine("---------------");
                        Console.WriteLine("ID_Object:{0}",o.ID_Object);
                        Console.WriteLine("md5:{0}", o.md5);
                        foreach (Документ_к_ОбъектуКМ a in o.Документы)
                            Console.WriteLine("   \"{0}\" (size:{1},md5:{2})", a.filename,a.size,a.md5);
                    }
                }
            }
            catch (Client.Exception cex)
            {
                Console.Out.WriteLine("status:{0}", cex.status);
                Console.Out.WriteLine("response:{0}", cex.response_body);
                throw cex;
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not StoreExposureAssets!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        static void TestGetStarts()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                Client.ProcedureStart[] alist = c.GetProcedureStarts();
                foreach (Client.ProcedureStart s in alist)
                {
                    Console.Out.WriteLine("{0},ИНН:{1},СНИЛС:{2},ОГРН:{3},CaseNumber:{4},АУ:ИНН:{5},РегНомер:{6},Суд:{7},Адрес:{8}"
                        , s.Наименование, s.ИНН, s.СНИЛС, s.ОГРН, s.CaseNumber,s.m_INN,s.m_efrsbNumber,s.Суд,s.Адрес);
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not GetProcedureStarts!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        static void TestGetAnketaNP()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                byte[] anketa_bytes = c.GetAnketaNP(id);
                Console.Out.WriteLine("Read {0} bytes of anketa",anketa_bytes.Length);
                string md5 = Hash.md5(anketa_bytes);
                Console.Out.WriteLine("md5: " + md5);
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not GetAnketaNP!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        static void TestGetSubLevelInfo()
        {
            Client c = Auth();
            if (null != c)
            {
                try
                {
                    Client.SubLevelResponce r = c.GetSubLevel(sub_level_info);
                    int i = 0;
                    while (string.IsNullOrEmpty(r.region) && i < try_again)
                    {
                        r = c.GetSubLevel(sub_level_info);
                        System.Threading.Thread.Sleep(100);
                        i++;
                    }
                    if (string.IsNullOrEmpty(r.region))
                    {
                        Console.Out.WriteLine("can not get sublevel {0} times!", try_again);
                    }
                    else
                    {
                        Console.Out.WriteLine("region=\"{0}\"", r.region);
                        Console.Out.WriteLine("common=\"{0}\"", r.common_sum);
                        Console.Out.WriteLine("employable_sum=\"{0}\"", r.employable_sum);
                        Console.Out.WriteLine("infant_sum=\"{0}\"", r.infant_sum);
                        Console.Out.WriteLine("pensioner_sum=\"{0}\"", r.pensioner_sum);
                        Console.Out.WriteLine("reglaments:");
                        foreach (Client.SubLevel_Reglament reglament in r.reglament)
                        {
                            Console.Out.WriteLine("---------------------------");
                            Console.Out.WriteLine("title=\"{0}\"", reglament.title);
                            Console.Out.WriteLine("date=\"{0}\"", reglament.date);
                            Console.Out.WriteLine("url=\"{0}\"", reglament.url);
                        }
                    }
                }
                catch (System.Net.WebException ex)
                {
                    Console.Out.WriteLine("can not get sublevel information!");
                    Console.Out.Write(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine("can not get sublevel information!");
                    Console.Out.Write(ex);
                    if (null != ex.InnerException)
                    {
                        Console.Out.WriteLine();
                        Console.Out.Write(ex.InnerException);
                    }
                }
            }
        }

        static void TestCreateAssembly()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                string id_Meeting = c.CreateAssembly(meeting_info);
                Console.Out.WriteLine("id_Meeting:{0}", id_Meeting);
            }
            catch (Client.Failed_to_execute fe)
            {
                Console.Out.WriteLine(fe.Message);
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not CreateAssembly!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        static void TestDeleteAssembly()
        {
            Client c = Auth();
            if (null == c)
                return;

            try
            {
                c.DeleteAssembly(id);
                Console.Out.WriteLine("deleted!");
            }
            catch (Client.Failed_to_execute fe)
            {
                Console.Out.WriteLine(fe.Message);
                Client.DeleteAssemblyResponse dar = fe.response as Client.DeleteAssemblyResponse;
                if (null!=dar)
                    Console.Out.WriteLine("id_Meeting_can_be_forgotten:{0}", dar.id_Meeting_can_be_forgotten);
            }
            catch (Client.Exception ce)
            {
                Console.Out.WriteLine("can not DeleteAssembly!");
                Console.Out.WriteLine(ce.Message);
                Console.Out.WriteLine("status:{0}", ce.status);
                Console.Out.WriteLine(ce.response_body);
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("can not DeleteAssembly!");
                Console.Out.Write(ex);
                if (null != ex.InnerException)
                {
                    Console.Out.WriteLine();
                    Console.Out.Write(ex.InnerException);
                }
            }
        }

        static string dm_base_url = "http://local.test/dm/api.php";

        static string login;
        static string password;
        static string category;

        static string access_token;

        static string action;

        static string xml_file_to_load;

        static Client.UploadInfo upload_info = new Client.UploadInfo();
        static Client.SubLevelInfo sub_level_info = new Client.SubLevelInfo();
        static Имущество_для_экспозиции exposure_data_to_store = new Имущество_для_экспозиции();

        static Процедура_над_должником procedure_debtor = new Процедура_над_должником();
        static string after_ID_Object= null;
        static int ?max_portion_size= null;
        static string km_objects_path = null;

        static string id_MProcedure;
        static string ID_Object;
        static string filepath;

        static string license_token;
        static string contract;

        static DateTime? test_time;
        static Client.Manager_for_auth manager = null;

        static string id;
        static string q;
        static string section;

        static int try_again = 0;

        static int? year_greater_than = null;
        static int? revision_greater_than = null;
        static string file_storeage_path;

        static Client.MeetingInfo meeting_info = new Client.MeetingInfo();

        static string ReadArg1FileText(string fname)
        {
            try
            {
                return File.ReadAllText(fname);
            }
            catch (Exception ex)
            {
                Console.WriteLine("can not read text from file \"{0}\"",fname);
                throw ex;
            }
        }

        static void ProcessArgs(string[] args)
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            for (int i= 0; i<args.Length; i++)
            {
                string [] ap = args[i].Split('=');
                switch (ap[0])
                {
                    case "--login": login= ap[1]; break;
                    case "--password": password = ap[1]; break;
                    case "--action": action = ap[1]; break;
                    case "--category": category = ap[1]; break;
                    case "--try-again": try_again = int.Parse(ap[1]); break;
                    case "--access-token": access_token = ap[1]; break;
                    case "--zipped-content": upload_info.zipped_content = File.ReadAllBytes(ap[1]); break;
                    case "--json-upload-info": upload_info = s.Deserialize<Client.UploadInfo>(ReadArg1FileText(ap[1])); break;
                    case "--allow-ctb": upload_info.allow_ctb= true; break;
                    case "--content-hash": upload_info.Content_hash = ap[1]; break;
                    case "--ama-version": upload_info.AmaVersion = ap[1]; break;
                    case "--xml-file-to-load": xml_file_to_load= ap[1]; break;

                    case "--base-url": dm_base_url = ap[1]; break;
                    case "--section": section = ap[1]; break;

                    case "--license_token": license_token= ap[1]; break;
                    case "--contract": contract= ap[1]; break;
                    case "--test-time": test_time= DateTime.Parse(ap[1]); break;
                    case "--manager-path": manager= s.Deserialize<Client.Manager_for_auth>(ReadArg1FileText(ap[1])); break;

                    case "--addres": sub_level_info.addres= ap[1]; break;
                    case "--start-date": sub_level_info.start_date = ap[1]; break;

                    case "--id": id = ap[1]; break;
                    case "--q": q = ap[1]; break;

                    case "--revision-greater-than": revision_greater_than = int.Parse(ap[1]); break;
                    case "--year-greater-than": year_greater_than = int.Parse(ap[1]); break;

                    case "--json-meeting-info": meeting_info = s.Deserialize<Client.MeetingInfo>(ReadArg1FileText(ap[1])); break;
                    case "--file-storage-path": file_storeage_path = ap[1]; break;

                    case "--exposure-objects": exposure_data_to_store = s.Deserialize<Имущество_для_экспозиции>(ReadArg1FileText(ap[1])); break;

                    case "--procedure-debtor": procedure_debtor = s.Deserialize<Процедура_над_должником>(ReadArg1FileText(ap[1])); break;
                    case "--after-ID-Object": after_ID_Object = ap[1]; break;
                    case "--max-portion-size": max_portion_size = int.Parse(ap[1]); break;
                    case "--km-objects": km_objects_path = ap[1]; break;

                    case "--id-mprocedure": id_MProcedure= ap[1]; break;
                    case "--id-object": ID_Object = ap[1]; break;
                    case "--filepath": filepath = ap[1]; break;
                }
            }
        }
    }
}
