﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Web.Script.Serialization;

namespace test_ama_dm_client
{
    [ComVisible(true)]
    public partial class TestReportView : Form
    {
        public string PathToIndex;
        public string ReportPath;

        public TestReportView()
        {
            InitializeComponent();
            webBrowserReport.ObjectForScripting = this;
        }

        private void buttonOpenReport_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog frm= new OpenFileDialog())
            {
                string relative_path_to_examples = @"..\..\..\uc\forms\ama\datamart\procedure\section\report\tests\";
                string path_to_app_exe_folder = Path.GetDirectoryName(Application.ExecutablePath);
                string path = Path.Combine(path_to_app_exe_folder, relative_path_to_examples);

                frm.InitialDirectory = Path.GetFullPath(new Uri(path).LocalPath)
                    .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                    .ToUpperInvariant();

                frm.Filter = "html files (*.html)|*.txt|All files (*.*)|*.*";
                frm.FilterIndex = 2;
                frm.RestoreDirectory = true;

                if (DialogResult.OK==frm.ShowDialog())
                {
                    string xml_text= File.ReadAllText(frm.FileName);
                    webBrowserReport.Document.InvokeScript("cpw_ama_datamart_loadReportXml",new object []{ xml_text});
                }
            }
        }

        private void TestReportView_Load(object sender, EventArgs e)
        {
            webBrowserReport.Navigate(new Uri(PathToIndex, System.UriKind.Absolute));
        }

        class Header
        {
            public int level= 0;
            public string name= "";
            public string title= "";
        }

        List<Header> m_headers;

        private void listBoxHeaders_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iheader = listBoxHeaders.SelectedIndex;
            if (-1!=iheader)
            {
                listBoxHeaders.ClearSelected();
                Header header = m_headers[iheader];
                HtmlElementCollection elements = this.webBrowserReport.Document.Body.All;
                foreach (HtmlElement element in elements)
                {
                    string nameAttribute = element.GetAttribute("Name");
                    if (!string.IsNullOrEmpty(nameAttribute) && nameAttribute == header.name)
                    {
                        element.ScrollIntoView(true);
                        break;
                    }
                }
            }
        }

        // методы, который вызывается из js..
        public void ShowHeaders(string json_headers)
        {
            m_headers = new JavaScriptSerializer().Deserialize<List<Header>>(json_headers);
            listBoxHeaders.Items.Clear();
            foreach (Header h in m_headers)
            {
                string txt = string.Format("{0}{1} {2} (#{3})",new string(' ',h.level),h.level,h.title,h.name);
                listBoxHeaders.Items.Add(txt);
            }
        }

        public string GetReport()
        {
            return string.IsNullOrEmpty(ReportPath) ? null : File.ReadAllText(ReportPath);
        }

        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush;

            // Get the item from the collection.
            TabPage _tabPage = tabControl1.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = tabControl1.GetTabRect(e.Index);
            _tabBounds.X += 10;

            if (e.State == DrawItemState.Selected)
            {

                // Draw a different background color, and don't paint a focus rectangle.
                _textBrush = new SolidBrush(Color.Black);
                g.FillRectangle(SystemBrushes.ControlLightLight, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                g.FillRectangle(SystemBrushes.Control, e.Bounds);
            }

            // Use our own font.
            Font _tabFont = new Font("Arial", 10.0f, FontStyle.Bold, GraphicsUnit.Pixel);

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Near;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
        }
    }
}
