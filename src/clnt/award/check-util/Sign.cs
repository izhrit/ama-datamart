﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AwardVoteUtils
{
    public class Sign
    {

        public static VerifyAnswer VerifyCAPICOM(string pathToTxtFile, string pathToSigFile)
        {
            string signatureContent = File.ReadAllText(pathToSigFile);
            string bulletinContent = File.ReadAllText(pathToTxtFile);

            string clearTextFile = Path.Combine(Path.GetDirectoryName(pathToTxtFile), string.Format("{0}.txt", "clearText"));

            return VerifyAttachedSignature(signatureContent, bulletinContent, clearTextFile);
        }

        public static VerifyAnswer VerifyAttachedSignature(string base64SignedContent, string bulletinContent, string clearTextFile)
        {
            CAPICOM.SignedData signedData = new CAPICOM.SignedData();
            signedData.Verify(base64SignedContent, false,
              CAPICOM.CAPICOM_SIGNED_DATA_VERIFY_FLAG.CAPICOM_VERIFY_SIGNATURE_ONLY);
            string clearText = signedData.Content;

            CAPICOM.Signer s = (CAPICOM.Signer)signedData.Signers[1];
            var SignerCert = (CAPICOM.Certificate)s.Certificate;
            string SubjectName = SignerCert.SubjectName;

            File.WriteAllText(clearTextFile, clearText);
            string clearTextContent = File.ReadAllText(clearTextFile);

            byte[] clearTextBytes = Encoding.UTF8.GetBytes(clearTextContent);
            byte[] bulletinContentBytes = Encoding.UTF8.GetBytes(bulletinContent);

            bool fileEqueals = FileEquals(clearTextBytes, bulletinContentBytes);

            List<KeyValuePair<string, string>> kvs = SubjectName.Split(',').Select(x => new KeyValuePair<string, string>(x.Split('=')[0].Trim(), x.Split('=')[1].Trim())).ToList();

            VerifyAnswer answer = new VerifyAnswer();
            answer.VerifyResult = fileEqueals;
            answer.SubjectName = kvs.Where(x => x.Key == "CN").FirstOrDefault().Value;
            answer.Thumbprint = SignerCert.Thumbprint;

            return answer;
        }

        static bool FileEquals(byte[] byteContent1, byte[] byteContent2)
        {
            if (byteContent1.Length == byteContent2.Length)
            {
                for (int i = 0; i < byteContent1.Length; i++)
                {
                    if (byteContent1[i] != byteContent2[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }

    public class VerifyAnswer
    {
        public bool VerifyResult { get; set; }
        public string SubjectName { get; set; }
        public string Thumbprint { get; set; }
    }
}
