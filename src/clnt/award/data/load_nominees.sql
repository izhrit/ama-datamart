LOAD DATA LOCAL
INFILE 'nominees.csv' 
INTO TABLE AwardNominee
character set cp1251
FIELDS TERMINATED BY ';' ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(lastName,firstName,middleName,efrsbNumber,SRO,URL)
;

select * from AwardNominee\G