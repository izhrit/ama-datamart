﻿using System;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;

using fa.datamart;

namespace test_ama_dm_client
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            ProcessArgs(args);
            switch (action)
            {
                case "prepare-auth": test_PrepareAuth(); break;
                case "auth": TestAuth(); break;
                case "get-km": test_GetKM(); break;
                case "get-managers": test_GetManagers(); break;
                case "upload": test_Upload(); break;
                case "using": TestUsing(); break;
            }
        }

        static void TestAuth()
        {
            if (null == test_time)
                test_time = DateTime.Now;
            Client c = new Client(dm_base_url);
            c._testMode = true;
            try
            {
                c.Auth(license_token, viewer_email, test_time);
                Console.WriteLine("c.phpsessid.Length={0}", c._phpsessid.Length);
            }
            catch
            {
                Console.WriteLine("can not authorize!");
            }
        }

        static Client CreateAuthClient()
        {
            Client c = new Client(dm_base_url);
            c._testMode = true;
            try
            {
                c.Auth(license_token, viewer_email, test_time);
            }
            catch
            {
                Console.WriteLine("can not authorize!");
            }
            return c;
        }

        static void TestUsing()
        {
            Client c = new Client(dm_base_url);

            try
            {
                c.Using(
                    ContractNumber: contract
                    , license_token: license_token
                    , section: section
                );
                Console.Out.Write("ok!");
            }
            catch
            {
                Console.Out.Write("got error!");
            }
        }

        static void test_GetKM()
        {
            Client c = CreateAuthClient();
            try
            {
                string km_responce = c.GetKM(debtor_inn);
                Console.WriteLine(km_responce.Replace("\r\n", "\n").Replace("\n", "\r\n"));
                Console.WriteLine(c._log);
            }
            catch
            {
                Console.WriteLine("can not get km!");
            }
        }

        static void test_GetManagers()
        {
            Client c = CreateAuthClient();
            try
            {
                АУ[] managers= c.GetManagersForDebtor(debtor_inn);
                foreach (АУ au in managers)
                {
                    Console.WriteLine("АУ");
                    Console.WriteLine("      Фамилия:{0}", au.Фамилия);
                    Console.WriteLine("          Имя:{0}", au.Имя);
                    Console.WriteLine("     Отчество:{0}", au.Отчество);
                    Console.WriteLine("          ИНН:{0}", au.ИНН);
                    Console.WriteLine("  Номер_ЕФРСБ:{0}", au.Номер_ЕФРСБ);
                }
            }
            catch
            {
                Console.WriteLine("can not get managers!");
            }
        }

        static void test_Upload()
        {
            Client c = CreateAuthClient();
            try
            {
                c.UploadDeals(debtor_inn,manager_inn,File.ReadAllText(xml_file_to_load));
            }
            catch// (Exception ex)
            {
                Console.WriteLine("can not upload!");
            }
        }

        static void test_PrepareAuth()
        {
            Client c = new Client(dm_base_url);
            if (null == test_time)
                test_time = DateTime.Now;
            string auth = Client.PrepareUrlArgument_fauth(license_token, viewer_email, test_time);
            Console.Write(auth);
        }


        static string dm_base_url = "http://local.test/dm/api.php";

        static string action;

        static string license_token;
        static string contract;
        static string section;

        static DateTime? test_time;
        static string viewer_email;
        static string debtor_inn;
        static string manager_inn;
        static string xml_file_to_load;

        static void ProcessArgs(string[] args)
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            for (int i = 0; i < args.Length; i++)
            {
                string[] ap = args[i].Split('=');
                switch (ap[0])
                {
                    case "--action": action = ap[1]; break;

                    case "--base-url": dm_base_url = ap[1]; break;

                    case "--license-token": license_token = ap[1]; break;
                    case "--test-time": test_time = DateTime.Parse(ap[1]); break;
                    case "--viewer-email": viewer_email = ap[1]; break;
                    case "--debtor-inn": debtor_inn = ap[1]; break;
                    case "--manager-inn": manager_inn = ap[1]; break;
                    case "--xml-file-to-load": xml_file_to_load = ap[1]; break;

                    case "--section": section = ap[1]; break;
                    case "--contract": contract = ap[1]; break;
                }
            }
        }
    }
}
