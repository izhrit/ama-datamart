﻿/*
 * ВНИМАНИЕ! ATTENTION! AHTUNG!
 * Данный файл редактируется и тестируется в проекте "datamart"!
 * Он НЕ должен редактироваться в проекте "FA"!

Основной класс обеспечивающий связь с витриной данных: fa.datamart.Client

у fa.datamart.Client есть также поля с префиксом proxy_ (host,port,login,password)
  в которые необходимо прописать настройки связи с интернетом

у fa.datamart.Client есть поле base_url
  значение которого рекомендуется устанавливать из конфиг-файла
  значение "по умолчанию": http://local.test/dm/api.php
  "боевое" значение: https://datamart.rsit.ru/api.php
  значение для тестовой витрины (рекомендуется использовать 
    при тестировании и отладке): https://datamart-rsit.ru/api.php

Для автоматического перехода на витрину данных необходимо использовать 
URL пользовательского интерфейса витрины данных 
  значение которого также рекомендуется устанавливать из конфиг-файла
  "боевое" значение: https://datamart.rsit.ru/ui.php
  значение для тестовой витрины (рекомендуется использовать 
    при тестировании и отладке): https://datamart-test.rsit.ru/ui.php

При этом авторизационные данные передаются витрине при помощи 
аргумента URL "fauth", например:
https://datamart.rsit.ru/ui.php?fauth=as876ghqewr78

Значение аргументы fauth должно создаваться вызовом 
статического метода 
fa.datamart.Client.PrepareUrlArgument_fauth

В качестве параметров передаётся:
- текущий токен короткоживущей лицензии, под которой работает сейчас пользователь
- электронный адрес наблюдателя, от имени которого осуществляется вход на витрину
- номер договора для использования ФА

 */
using System.Net;
using System.Web;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Web.Script.Serialization;

using System.Security.Cryptography;
using System;

namespace fa.datamart
{
    public class АУ
    {
        public string Фамилия;
        public string Имя;
        public string Отчество;
        public string ИНН;
        public string Номер_ЕФРСБ;
    }

    public interface IClient
    {
        string BaseAuthUrl { get; }

        bool Auth(string license_token, string viewer_email, System.DateTime? time = null);
        string GetKM(string debtor_inn);

        АУ[] GetManagersForDebtor(string debtor_inn);
        void UploadDeals(string debtor_inn, string manager_inn, string deals_xml);

        void Using(string ContractNumber, string license_token, string section);
    }

    public class Client : IClient
    {
        public string _log="";
        public string _domain { get { return "local.test"; } }
        public string BaseAuthUrl { get { return $"http://{_domain}/dm/ui.php"; } }
        public string _baseBackendUrl { get { return $"http://{_domain}/dm/ui-backend.php"; } }
        public string _baseUrl { get; set; } 
        public string _phpsessid;

        public string ProxyHost { get; set; }
        public int ProxyPort { get; set; }
        public string ProxyLogin { get; set; }
        public string ProxyPassword { get; set; }

        public bool _testMode = false;

        public Client(string base_url = null)
        {
            if (!string.IsNullOrEmpty(base_url))
                this._baseUrl = base_url;
        }

        public void Using(string ContractNumber, string license_token, string section)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_baseBackendUrl).Append("?action=fa.using&license=").Append(license_token);
            if (!string.IsNullOrEmpty(ContractNumber))
                sb.Append("&contract=").Append(ContractNumber);
            if (!string.IsNullOrEmpty(section))
                sb.Append("&section=").Append(section);
            string url = sb.ToString();

            CookieCollection cookies;
            HttpStatusCode status;
            HttpGet(url, out status, out cookies);
        }

        string HttpGet(string url, out HttpStatusCode status, out CookieCollection cookies)
        {
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);

            if (null!=_phpsessid)
            {
                CookieContainer container= request.CookieContainer = new CookieContainer();
                Cookie cookie = new Cookie("PHPSESSID", _phpsessid, "/", _domain);
                container.Add(cookie);
            }

            request.Method = "GET";
            return ReadResponse(request, out status, out cookies);
        }

        string HttpPost(string url, byte[] postBytes, out HttpStatusCode status, out CookieCollection cookies, string ContentType = "application/x-www-form-urlencoded")
        {
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);

            CookieContainer container= request.CookieContainer= new CookieContainer();
            if (null != _phpsessid)
            {
                Cookie cookie = new Cookie("PHPSESSID", _phpsessid, "/", _domain);
                container.Add(cookie);
            }

            request.Method = "POST";
            request.ContentType = ContentType;

            if (null == postBytes || 0 == postBytes.Length)
            {
                request.ContentLength = 0;
            }
            else
            {
                request.ContentLength = postBytes.Length;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(postBytes, 0, postBytes.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }
            }

            return ReadResponse(request, out status, out cookies);
        }

        static string ReadResponse(HttpWebRequest request, out HttpStatusCode status, out CookieCollection cookies)
        {
            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
            using (Stream receiveStream = response.GetResponseStream())
            using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
            {
                status = response.StatusCode;
                string result = ReadResponseText(status, readStream);
                cookies = response.Cookies;
                return result;
            }
        }

        static string ReadResponseText(HttpStatusCode status, StreamReader readStream)
        {
            if (HttpStatusCode.OK == status)
            {
                return readStream.ReadToEnd();
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                for (int count = 0; count < 100; count++) // читаем только первые 100 строк сообщения об ошибке!
                {
                    string line = readStream.ReadLine();
                    if (null == line)
                    {
                        break;
                    }
                    else
                    {
                        sb.AppendLine(line);
                    }
                }
                return sb.ToString();
            }
        }

        public class Auth_responce
        {
            public bool ok;
            public string error;
        }

        void Auth(string url, string data)
        {
            ASCIIEncoding ascii = new ASCIIEncoding();
            byte[] postBytes = ascii.GetBytes(data);
            HttpStatusCode status;
            CookieCollection cookies;
            string response_body = HttpPost(url, postBytes, out status, out cookies);

            if (HttpStatusCode.OK != status)
                throw new DatamartClientException("Can not auth. Bad Http status code", status, response_body);

            if (cookies.Count == 0)
                throw new DatamartClientException("Can not auth. Empty cookies.", status, response_body);

            JavaScriptSerializer s = new JavaScriptSerializer();
            Auth_responce auth_responce= null;
            try
            {
                auth_responce = s.Deserialize<Auth_responce>(response_body);
            }
            catch (Exception ex)
            {
                throw new DatamartClientException("can not parse responce!", status, response_body, ex);
            }

            if (auth_responce.ok)
            {
                _phpsessid = cookies["PHPSESSID"].Value;
            }
            else
            {
                throw new DatamartClientException("auth error: " + auth_responce.error, status, response_body);
            }
        }

        public bool Auth(string license_token, string viewer_email, System.DateTime? time = null)
        {
            string auth_arg = Prepare_auth(license_token, viewer_email, time);

            string url= string.Format("{0}?action=login&as=viewer", _baseBackendUrl);
            string data = "Login=&Password=&fauth=" + HttpUtility.UrlEncode(auth_arg);

            try
            {
                Auth(url,data);
                return true;
            }
            catch (Exception ex)
            {
                _log = string.Format("Auth failed: {0}", ex.Message);
                return false;
            }
        }

        public string GetKM(string debtor_inn)
        {
            try
            {
                string url = string.Format("{0}?action=procedure.info&inn={1}&section=km.xml", _baseBackendUrl, debtor_inn);
                ASCIIEncoding ascii = new ASCIIEncoding();
                HttpStatusCode status;
                CookieCollection cookies;
                string response_body = HttpGet(url, out status, out cookies);

                if (HttpStatusCode.OK != status)
                    throw new DatamartClientException("Can not get mass objects. Http status code not equal condition", status, response_body);

                return response_body;
            }
            catch (Exception ex)
            {
                _log = string.Format("Ошибка при получении конкурсной массы для ИНН {0}: {1}", debtor_inn, ex.Message);
                return string.Empty;
            }
        }

        static public string PrepareUrlArgument_fauth(string license_token, string email, System.DateTime? time = null)
        {
            return Prepare_auth(license_token, email, time);
        }

        static public string PrepareUrlArguments(string license_token, string email, string contract, string debtor_inn, string section = null, System.DateTime? time = null)
        {
            string auth = PrepareUrlArgument_fauth(license_token, email, time);
            string args = string.IsNullOrEmpty(section) 
                ? string.Format("{0}&debtor_inn={1}", auth, debtor_inn) 
                : string.Format("{0}&debtor_inn={1}&section={2}",auth,debtor_inn,section);
            return args;
        }

        static string Prepare_auth(string license_token, string email, System.DateTime? time, string key= "DTLPKCRMNPMCPDIADKOPAAAW")
        {
            if (null == time)
                time = System.DateTime.Now;
            Auth_info auth = new Auth_info
            {
                license_token = license_token,
                time = System.TimeZoneInfo.ConvertTimeToUtc(time.Value).ToString("O"),
                email = email
            };
            return auth.Encrypt(key);
        }

        public class Auth_info
        {
            public string license_token;
            public string time;
            public string email;

            public string Encrypt(string key)
            {
                JavaScriptSerializer s = new JavaScriptSerializer();
                string auth = s.Serialize(this);

                TripleDESCryptoServiceProvider tripleDESProvider = new TripleDESCryptoServiceProvider();
                tripleDESProvider.Key = ASCIIEncoding.ASCII.GetBytes(key);
                tripleDESProvider.Mode = CipherMode.ECB;

                byte[] bytes_Account = Encoding.UTF8.GetBytes(auth);
                byte[] encrypted_bytes_Account = tripleDESProvider.CreateEncryptor().TransformFinalBlock(bytes_Account, 0, bytes_Account.Length);
                string res = HttpUtility.UrlEncode(System.Convert.ToBase64String(encrypted_bytes_Account));
                return res;
            }
        }

        public class DatamartClientException : System.Exception
        {
            public HttpStatusCode status;
            public string response_body;
            public DatamartClientException(string text, HttpStatusCode s, string b)
                : base(text)
            {
                status = s;
                response_body = b;
            }
            public DatamartClientException(string text, HttpStatusCode s, string b, System.Exception e)
                : base(text, e)
            {
                status = s;
                response_body = b;
            }
        }

        void SafeFixProxy(HttpWebRequest request)
        {
            if (!string.IsNullOrEmpty(ProxyHost))
            {
                WebProxy proxy = new WebProxy(ProxyHost, ProxyPort);
                if (!string.IsNullOrEmpty(ProxyLogin))
                    proxy.Credentials = new NetworkCredential(ProxyLogin, ProxyPassword);
                request.Proxy = proxy;
            }
        }

        public АУ[] GetManagersForDebtor(string debtor_inn)
        {
            HttpStatusCode status;
            string response_body;

            string url = string.Format("{0}?action=fa.debtor-managers&debtor-inn={1}", _baseBackendUrl, debtor_inn);
            CookieCollection cookies;
            response_body = HttpGet(url, out status, out cookies);

            if (HttpStatusCode.OK != status)
                throw new DatamartClientException("Can not get managers!", status, response_body);

            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Deserialize<АУ[]>(response_body);
        }

        byte [] prepare_deals_content(string deals_xml)
        {
            using (MemoryStream stream= new MemoryStream())
            {
                using (Package package = Package.Open(stream, FileMode.Create))
                {
                    Uri uriContent = PackUriHelper.CreatePartUri(new Uri("deals.xml", UriKind.Relative));
                    PackagePart packagePartContent = package.CreatePart(uriContent, "text/xml", CompressionOption.Fast);

                    using (Stream part_steam = packagePartContent.GetStream())
                    using (TextWriter w = new StreamWriter(part_steam))
                    {
                        w.Write(deals_xml);
                    }
                }
                return stream.ToArray();
            }
        }

        public void UploadDeals(string debtor_inn, string manager_inn, string deals_xml)
        {
            string ContentType = null == deals_xml ? "application/null" : "application/zip";
            HttpStatusCode status;
            string response_body;

            string url = string.Format("{0}?action=fa.upload-deals&debtor-inn={1}&manager-inn={2}", _baseBackendUrl, debtor_inn, manager_inn);
            byte[] postBytes = prepare_deals_content(deals_xml);
            CookieCollection cookies;
            response_body = HttpPost(url, postBytes, out status, out cookies, ContentType);

            if (HttpStatusCode.OK != status)
                throw new DatamartClientException("Can not upload deals!", status, response_body);

            JavaScriptSerializer s = new JavaScriptSerializer();
            Dictionary<string,object> parsed_responce= (Dictionary<string, object>)s.DeserializeObject(response_body);

            object ok;
            if (!parsed_responce.TryGetValue("ok",out ok) || !(bool)ok)
                throw new DatamartClientException("Can not upload deals!", status, response_body);
        }
    }
}