﻿реализованные элементы управления для ПАУ..

предлагается подключать в исходники проект ama-dm-controls
либо прямо исходниками, либо в виде собранной dll

в составе файлов рабочий dm-ama.sln,
с тестовыми приложениями test-client-ama и test-controls-ama,
так что работоспособность можно проверить отдельно

приложение test-controls-ama собственно для тестирования элементов управления
приложение test-client-ama просто до кучи, в нём файлы dm_client.cs и dm_wcalendar.cs
которые включены в ПАУ в виде отдельных файлов

сейчас реализованы:
ama.datamart.UCEfrsbClasses - UserControl для указания классификации объекта КМ в соответствии с ЕФРСБ
ama.datamart.FormChooseEfrsbMessageType - форма для выбора типа сообщения на ЕФРСБ

перед использованием нужно корректно проинициализировать 
ama.datamart.StaticHtmlControls.Web_root_directory_path

указав путь к папке ama-dm-controls\phtml