pushd %~dp0

rd /S /Q dm-ama
mkdir dm-ama

mkdir dm-ama\ama-dm-controls
xcopy /E /EXCLUDE:exclude.txt ama-dm-controls\*   dm-ama\ama-dm-controls\

mkdir dm-ama\test-client-ama
xcopy /E /EXCLUDE:exclude.txt test-client-ama\*   dm-ama\test-client-ama\

mkdir dm-ama\test-controls-ama
xcopy /E /EXCLUDE:exclude.txt test-controls-ama\* dm-ama\test-controls-ama\

copy dm-ama.sln dm-ama\
copy readme.txt dm-ama\

popd