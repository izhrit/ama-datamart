﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.ComponentModel;

namespace ama.datamart
{
    public static class StaticHtmlControl
    {
        static public string Web_root_directory_path;

        public static void Start(WebBrowser webBrowser, string arg, object ObjectForScripting= null)
        {
            webBrowser.ScriptErrorsSuppressed = true; // To not display dialog boxes such as script error messages.
            webBrowser.AllowWebBrowserDrop = false; // To not control navigates to documents that are dropped onto it.
            webBrowser.IsWebBrowserContextMenuEnabled = false; // shortcut menu is disabled

            webBrowser.Navigating += WebBrowser_Navigating;
            webBrowser.NewWindow += WebBrowser_NewWindow;

            if (null!= ObjectForScripting)
                webBrowser.ObjectForScripting= ObjectForScripting;
            webBrowser.Navigate(Path.Combine(Web_root_directory_path, "index.html?" + arg));
        }

        private static void WebBrowser_NewWindow(object sender, CancelEventArgs e)
        {
            WebBrowser webBrowser = sender as WebBrowser;
            if (null != webBrowser)
            {
                HtmlElement link = webBrowser.Document.ActiveElement;
                string href_value = link.GetAttribute("href");
                if (!string.IsNullOrEmpty(href_value))
                    Cancel_event_for_opened_url(e, new Uri(href_value));
            }
        }

        private static void WebBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            Cancel_event_for_opened_url(e, e.Url);
        }

        static void Cancel_event_for_opened_url(CancelEventArgs ce, Uri uri)
        {
            if (null != uri)
            {
                string url = uri.ToString();
                if (url.StartsWith("http://") || url.StartsWith("https://"))
                {
                    try
                    {
                        Process.Start(url);
                        ce.Cancel = true;
                    }
                    catch
                    {
                        // ce.Cancel = false;
                    }
                }
            }
        }
    }

    public class StaticHtmlSettings
    {
        public string background_color = "#f0f0f0";

        public void FixBackgroundColor(Form frm)
        {
            background_color = string.Format("#{0:X}{1:X}{2:X}", frm.BackColor.R, frm.BackColor.G, frm.BackColor.B);
        }
    }
}
