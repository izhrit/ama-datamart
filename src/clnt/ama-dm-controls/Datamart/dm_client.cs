/*
 * ВНИМАНИЕ! ATTENTION! AHTUNG!
 * Данный файл редактируется и тестируется в проекте "datamart"!
 * Он НЕ должен редактироваться в проекте "AMA"!

документация на REST API приведена в
  https://datamart.rsit.ru/api.php

Основной класс обеспечивающий связь с витриной данных: ama.datamart.Client

он реализует методы, одноимённые REST API

у ama.datamart.Client есть также поля с префиксом proxy_ (host,port,login,password)
  в которые необходимо прописать настройки связи с интернетом

у ama.datamart.Client есть поле base_url
  значение которого рекомендуется устанавливать из конфиг-файла
  значение "по умолчанию": http://local.test/dm/api.php
  "боевое" значение: https://datamart.rsit.ru/api.php
  значение для тестовой витрины (рекомендуется использовать 
    при тестировании и отладке): https://datamart-test.rsit.ru/api.php

Для автоматического перехода на витрину данных необходимо использовать 
URL пользовательского интерфейса витрины данных 
  значение которого также рекомендуется устанавливать из конфиг-файла
  "боевое" значение: https://datamart.rsit.ru/ui.php
  значение для тестовой витрины (рекомендуется использовать 
    при тестировании и отладке): https://datamart-test.rsit.ru/ui.php

При этом авторизационные данные передаются витрине при помощи 
аргумента URL "auth", например:
https://datamart.rsit.ru/ui.php?auth=as876ghqewr78

При этом значение аргумента auth должно создаваться вызовом 
статического метода 
ama.datamart.Client.PrepareUrlArgument_auth

В качестве параметров передаётся:
- текущий токен короткоживущей лицензии, под которой работает сейчас пользователь
- номер договора для использования ПАУ
- информация об АУ, от имени которого работает пользователь (в версии ПРО, Лайт)

для версии УК:
- если пользователь - суперадмин и имеет доступ ко всем процедурам, 
  допустимо не указывать информацию об АУ
- если пользователь - не суперадмин и для него выставлены какие то хитрые 
  условия, к какой информаиции он имеет доступ, а к какой нет, то для такого 
  пользователя параметр auth просто не указывает (дял него автовход не доступен 
  и он должен вводить логин-пароль, например наблюдателя)

При вызове методов 
- CreateAssembly 
- DeleteAssembly

необходимо обрабатывать exception ama.datamart.Client.Failed_to_execute, 
чтобы показать пользователю предназначенное для него сообщение с сервера

для CreateAssembly:

string id_Meeting= null;
try { id_Meeting= client.CreateAssembly(m); }
catch (ama.datamart.Client.Failed_to_execute fe)
{
    MessageBox.Show(fe.message);
}

для DeleteAssembly можно ещё в соответствии с указанием сервера удалять, 
либо не удалять заседание из базы данных ПАУ:

try { client.DeleteAssembly(id_Meeting); }
catch (ama.datamart.Client.Failed_to_execute fe)
{
    MessageBox.Show(fe.message);
    ama.datamart.Client.DeleteAssemblyResponse dar = 
        fe.response as ama.datamart.Client.DeleteAssemblyResponse;
    if (null!=dar && dar.id_Meeting_can_be_forgotten)
        ;// Таки удалить заседание из базы данных ПАУ
}

 */
using System.Net;
using System.Web;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Collections.Generic;

using System.Security.Cryptography;

namespace ama.datamart
{
    public interface IClient : IExposure_client
    {
        void Auth(string login, string password, string category);
        void Auth(string license_token, string ContractNumber, Client.Manager_for_auth manager = null, System.DateTime? time = null);

        Client.UploadResponce Upload(Client.UploadInfo u);
        Client.SubLevelResponce GetSubLevel(Client.SubLevelInfo s);

        byte [] GetAnketaNP(string id_MData);
        Client.AnketaNP[] GetAnketaNPList(string query);

        Client.ProcedureStart[] GetProcedureStarts();

        void UsingProcedure(string inn, string ogrn, string snils, string ContractNumber, string license_token, string section);

        Client.Рабочий_календарь[] Получить_рабочие_календари(int? revision_greater_than = null, int? year_greater_than= null);

        string CreateAssembly(Client.MeetingInfo m); // возвращает id_Meeting
        void DeleteAssembly(string id_Meeting);

        // static string PrepareUrlArgument_auth(string license_token, string ContractNumber, Client.Manager_for_auth manager = null, System.DateTime? time = null);
    }

    public class Client : IClient
    {
        public string base_url = "http://local.test/dm/api.php";
        public string access_token;

        public string proxy_host;
        public int proxy_port;
        public string proxy_login;
        public string proxy_password;

        public bool test_mode = false;

        public Client(string base_url = null)
        {
            if (!string.IsNullOrEmpty(base_url))
                this.base_url = base_url;
        }

        void InternalAuth(string url, string data)
        {
            ASCIIEncoding ascii = new ASCIIEncoding();
            byte[] postBytes = ascii.GetBytes(data);
            HttpStatusCode status;
            string response_body = HttpPost(url, postBytes, out status);

            if (HttpStatusCode.OK != status)
                throw new Exception("Can not auth!", status, response_body);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Dictionary<string, object> res = null;

            try
            {
                res = (Dictionary<string, object>)serializer.DeserializeObject(response_body);
            }
            catch (Exception ex)
            {
                throw new Exception("Can not parse response!", status, response_body, ex);
            }

            object oaccess_token;
            if (res == null || !res.TryGetValue("token", out oaccess_token) || oaccess_token.GetType() != typeof(string))
                throw new Exception("Can not find access_token!", status, response_body);

            this.access_token = (string)oaccess_token;
        }

        public void Auth(string login, string password, string category)
        {
            string url = string.Format("{0}/Auth?login={1}&category={2}", base_url, login, category);
            string data = "password=" + HttpUtility.UrlEncode(password);
            InternalAuth(url, data);
        }

        public void Auth(string license_token, string ContractNumber, Manager_for_auth manager = null, System.DateTime? time = null)
        {
            string auth_arg = Prepare_auth(license_token, ContractNumber, manager, time, "DTLAH59GNPMCUA6ADKOPAAAW");
            string url = !test_mode
                ? string.Format("{0}/Auth", base_url)
                : string.Format("{0}/Auth?use-test-time={1}", base_url, System.TimeZoneInfo.ConvertTimeToUtc(time.Value).ToString("o"));
            string data = "auto=" + HttpUtility.UrlEncode(auth_arg);
            InternalAuth(url, data);
        }

        public Client.Рабочий_календарь[] Получить_рабочие_календари(int? revision_greater_than = null, int? year_greater_than = null)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}/GetWcalendar?", base_url);
            if (null != revision_greater_than)
                sb.AppendFormat("revision-greater-than={0}&", revision_greater_than);
            if (null != year_greater_than)
                sb.AppendFormat("year-greater-than={0}&", year_greater_than);
            string url = sb.ToString();

            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);

            request.Method = "GET";
            if (!string.IsNullOrEmpty(access_token))
                request.Headers.Add("ama-datamart-access-token", access_token);

            HttpStatusCode status;
            string response_body = ReadResponse(request, out status);

            if (HttpStatusCode.OK != status)
                throw new Exception("Can not GetWcalendar!", status, response_body);

            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Deserialize<Client.Рабочий_календарь[]>(response_body);
        }

        public byte[] GetAnketaNP(string id_MData)
        {
            string url = string.Format("{0}/GetAnketaNP?id={1}", base_url, id_MData);

            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);

            request.Method = "GET";
            if (!string.IsNullOrEmpty(access_token))
                request.Headers.Add("ama-datamart-access-token", access_token);

            return ReadBinaryResponse(request);
        }

        public class AnketaNP
        {
            public string Name;
            public string id_MData;
            public string SNILS;
            public string INN;
            public string Email;
        }

        public AnketaNP[] GetAnketaNPList(string query)
        {
            string url = string.Format("{0}/GetAnketaNPList?q={1}", base_url, query);

            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);

            request.Method = "GET";
            if (!string.IsNullOrEmpty(access_token))
                request.Headers.Add("ama-datamart-access-token", access_token);

            HttpStatusCode status;
            string response_body = ReadResponse(request, out status);

            if (HttpStatusCode.OK != status)
                throw new Exception("Can not get!", status, response_body);

            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Deserialize<AnketaNP[]>(response_body);
        }

        public Client.ProcedureStart[] GetProcedureStarts()
        {
            string url = string.Format("{0}/GetProcedureStarts", base_url);

            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);

            request.Method = "GET";
            if (!string.IsNullOrEmpty(access_token))
                request.Headers.Add("ama-datamart-access-token", access_token);

            HttpStatusCode status;
            string response_body = ReadResponse(request, out status);

            if (HttpStatusCode.OK != status)
                throw new Exception("Can not get!", status, response_body);

            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Deserialize<ProcedureStart[]>(response_body);
        }

        void PrepareUploadInfo_base_URL(UploadInfo_base u, StringBuilder sb)
        {
            if (!string.IsNullOrEmpty(u.AmaVersion))
                sb.Append("&ama-version=").Append(u.AmaVersion);

            sb.Append("&manager[name]=").Append(u.manager.name);
            sb.Append("&manager[surname]=").Append(u.manager.surname);
            sb.Append("&manager[patronymic]=").Append(u.manager.patronymic);
            sb.Append("&manager[efrsb_number]=").Append(u.manager.efrsb_number);
            if (!string.IsNullOrEmpty(u.manager.inn))
                sb.Append("&manager[inn]=").Append(u.manager.inn);

            if (null != u.manager.BankroTechAccount)
                sb.Append("&manager[bt]=").Append(u.manager.BankroTechAccount.Encrypt());

            sb.Append("&debtor[name]=").Append(u.debtor.name);
            sb.Append("&debtor[inn]=").Append(u.debtor.inn);
            sb.Append("&debtor[ogrn]=").Append(u.debtor.ogrn);
            sb.Append("&debtor[snils]=").Append(u.debtor.snils);

            sb.Append("&procedure[type]=").Append(u.procedure.type);
            sb.Append("&procedure[case_number]=").Append(u.procedure.case_number);
        }

        public UploadResponce Upload(UploadInfo u)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base_url).Append("/UploadProcedureInfo?ctb_allow=").Append(u.allow_ctb ? "true" : "false");

            PrepareUploadInfo_base_URL(u, sb);

            if (!string.IsNullOrEmpty(u.Content_hash))
                sb.Append("&content-hash=").Append(u.Content_hash);

            string ContentType = null == u.zipped_content ? "application/null" : "application/zip";
            HttpStatusCode status;
            string url = sb.ToString();
            string response_body = HttpPost(url, u.zipped_content, out status, ContentType, access_token);

            if (HttpStatusCode.OK != status)
                throw new Exception("Can not upload!", status, response_body);

            try
            {
                return new UploadResponce(response_body);
            }
            catch (Exception ex)
            {
                throw new Exception("Can not parse response!", status, response_body, ex);
            }
        }

        public BaseResponse StoreExposureAssets(string id_MProcedure, Имущество_для_экспозиции имущество)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base_url).Append("/StoreExposureAssets?id_MProcedure=").Append(id_MProcedure);

            string ContentType= "application/json; utf-8";

            JavaScriptSerializer s = new JavaScriptSerializer();
            string имущество_txt = s.Serialize(имущество);
            byte [] имущество_bytes= Encoding.UTF8.GetBytes(имущество_txt);

            HttpStatusCode status;
            string url = sb.ToString();
            string response_body = HttpPost(url, имущество_bytes, out status, ContentType, access_token);

            if (HttpStatusCode.OK != status)
                throw new Exception("Can not store!", status, response_body);

            try
            {
                return s.Deserialize<BaseResponse>(response_body);
            }
            catch (System.Exception ex)
            {
                throw new Exception("Can not parse response!", status, response_body, ex);
            }
        }

        public Имущество_для_экспозиции_digest GetExposureAssetDigestsOrderedBy_ID_Object
            (Процедура_над_должником процедура_над_должником, string after_ID_Object = null, int max_portion_size = 100)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base_url).Append("/GetExposureAssetDigestsOrderedBy_ID_Object?max_portion_size=").Append(max_portion_size);
            if (!string.IsNullOrEmpty(after_ID_Object))
                sb.Append("&after_ID_Object=").Append(after_ID_Object);

            string ContentType = "application/json; utf-8";

            JavaScriptSerializer s = new JavaScriptSerializer();
            string процедура_над_должником_txt = s.Serialize(процедура_над_должником);
            byte[] процедура_над_должником_bytes = Encoding.UTF8.GetBytes(процедура_над_должником_txt);

            HttpStatusCode status;
            string url = sb.ToString();
            string response_body = HttpPost(url, процедура_над_должником_bytes, out status, ContentType, access_token);

            if (HttpStatusCode.OK != status)
                throw new Exception("Can not get exsposure!", status, response_body);

            try
            {
                return s.Deserialize<Имущество_для_экспозиции_digest>(response_body);
            }
            catch (System.Exception ex)
            {
                throw new Exception("Can not parse response!", status, response_body, ex);
            }
        }

        public void StoreExposureDocument(string id_MProcedure, Документ_для_экспозиции_на_загрузку документ)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base_url).Append("/StoreExposureDocument?id_MProcedure=").Append(id_MProcedure);
            sb.Append("&ID_Object=").Append(документ.ID_Object);
            sb.Append("&FileName=").Append(документ.filename);

            string ContentType = "application/x-binary";

            HttpStatusCode status;
            string url = sb.ToString();
            string response_body = HttpPost(url, документ.content, out status, ContentType, access_token);

            SimpleBaseProcessResponse(status, response_body, "StoreExposureDocument");
        }

        public void DeleteExposureDocuments(string id_MProcedure, string ID_Object, List<string> filename_документов)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base_url).Append("/DeleteExposureDocuments?id_MProcedure=").Append(id_MProcedure);
            sb.Append("&ID_Object=").Append(ID_Object);

            string ContentType = "application/json; utf-8";

            JavaScriptSerializer s = new JavaScriptSerializer();
            string filename_документов_txt = s.Serialize(filename_документов);
            byte[] filename_документов_bytes = Encoding.UTF8.GetBytes(filename_документов_txt);

            HttpStatusCode status;
            string url = sb.ToString();
            string response_body = HttpPost(url, filename_документов_bytes, out status, ContentType, access_token);

            SimpleBaseProcessResponse(status, response_body, "DeleteExposureDocuments");
        }

        public void UsingProcedure(string inn, string ogrn, string snils, string ContractNumber, string license_token, string section)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base_url).Append("/UsingProcedure?license=").Append(license_token);
            if (!string.IsNullOrEmpty(ContractNumber))
                sb.Append("&contract=").Append(ContractNumber);
            if (!string.IsNullOrEmpty(snils))
                sb.Append("&snils=").Append(snils);
            if (!string.IsNullOrEmpty(ogrn))
                sb.Append("&ogrn=").Append(ogrn);
            if (!string.IsNullOrEmpty(inn))
                sb.Append("&inn=").Append(inn);
            if (!string.IsNullOrEmpty(section))
                sb.Append("&section=").Append(section);
            string url = sb.ToString();

            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);

            request.Method = "GET";
            if (!string.IsNullOrEmpty(access_token))
                request.Headers.Add("ama-datamart-access-token", access_token);

            HttpStatusCode status;
            string response_body = ReadResponse(request, out status);

            if (HttpStatusCode.OK != status)
                throw new Exception("Unsuccessefull sending UsingProcedure!", status, response_body);
        }

        public SubLevelResponce GetSubLevel(SubLevelInfo s)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base_url).Append("/GetSubLevelInformation?");
            sb.Append("&addres=").Append(HttpUtility.UrlEncode(s.addres));
            sb.Append("&date=").Append(HttpUtility.UrlEncode(s.start_date));
            string url = sb.ToString();

            HttpStatusCode status;
            string response_body = HttpGet(url, out status, access_token);

            if (HttpStatusCode.OK != status)
                throw new Exception("Can not get!", status, response_body);

            try
            {
                return new SubLevelResponce(response_body);
            }
            catch (Exception ex)
            {
                throw new Exception("Can not parse response!", status, response_body, ex);
            }
        }

        public List<ExposureRecipientPro> GetExposureRecipientProList()
        {
            string url = base_url + "/GetExposureRecipientProList";

            HttpStatusCode status;
            string response_body= HttpGet(url, out status, access_token);

            if (HttpStatusCode.OK != status)
                throw new Exception("Can not get!", status, response_body);

            try
            {
                JavaScriptSerializer s = new JavaScriptSerializer();
                return s.Deserialize<List<ExposureRecipientPro>>(response_body);
            }
            catch (Exception ex)
            {
                throw new Exception("Can not parse response!", status, response_body, ex);
            }
        }

        public string CreateAssembly(MeetingInfo m)
        {
            string url = string.Format("{0}/CreateAssembly", base_url);

            StringBuilder sb = new StringBuilder();
            Debtor d = m.debtor;
            sb.Append("Meeting[Должник][Наименование]=").Append(HttpUtility.UrlEncode(d.name));
            sb.Append("&Meeting[Должник][ИНН]=").Append(HttpUtility.UrlEncode(d.inn));
            sb.Append("&Meeting[Должник][ОГРН]=").Append(HttpUtility.UrlEncode(d.ogrn));
            sb.Append("&Meeting[Должник][СНИЛС]=").Append(HttpUtility.UrlEncode(d.snils));

            sb.Append("&Meeting[Ознакомление][С_материалами][С_даты]=").Append(HttpUtility.UrlEncode(m.review.data.date));
            sb.Append("&Meeting[Ознакомление][С_материалами][Время][Начало]=").Append(HttpUtility.UrlEncode(m.review.data.time.start));
            sb.Append("&Meeting[Ознакомление][С_материалами][Время][Конец]=").Append(HttpUtility.UrlEncode(m.review.data.time.end));
            sb.Append("&Meeting[Ознакомление][С_материалами][Адрес]=").Append(HttpUtility.UrlEncode(m.review.data.address));
            sb.Append("&Meeting[Ознакомление][С_результатами][До_даты]=").Append(HttpUtility.UrlEncode(m.review.result.date));
            sb.Append("&Meeting[Ознакомление][С_результатами][Время][Начало]=").Append(HttpUtility.UrlEncode(m.review.result.time.start));
            sb.Append("&Meeting[Ознакомление][С_результатами][Время][Конец]=").Append(HttpUtility.UrlEncode(m.review.result.time.end));
            sb.Append("&Meeting[Ознакомление][С_результатами][Адрес]=").Append(HttpUtility.UrlEncode(m.review.result.address));

            sb.Append("&Meeting[Дата_заседания]=").Append(HttpUtility.UrlEncode(m.date));
            sb.Append("&Meeting[Время_заседания]=").Append(HttpUtility.UrlEncode(m.time));

            sb.Append("&procedure_type=").Append(HttpUtility.UrlEncode(m.procedure_type));
            sb.Append("&case_number=").Append(HttpUtility.UrlEncode(m.case_number));
            sb.Append("&debtor_address=").Append(HttpUtility.UrlEncode(m.debtor_address));
            sb.Append("&manager_phone=").Append(HttpUtility.UrlEncode(m.manager_phone));

            string ContentType = "application/x-www-form-urlencoded";
            HttpStatusCode status;
            byte[] post = Encoding.UTF8.GetBytes(sb.ToString());
            string response_body = HttpPost(url, post, out status, ContentType, access_token);
            CreateAssemblyResponse r = BaseProcessResponse<CreateAssemblyResponse>(status,response_body, "CreateAssembly");
            return r.id_Meeting;
        }

        T BaseProcessResponse<T>(HttpStatusCode status, string response_body, string MethodName) where T : BaseResponse
        {
            if (HttpStatusCode.OK != status)
                throw new Exception(string.Format("Can not {0}!", MethodName), status, response_body);

            JavaScriptSerializer s = new JavaScriptSerializer();
            T r = null;
            try
            {
                r = s.Deserialize<T>(response_body);
            }
            catch (System.Exception ex)
            {
                throw new Exception(string.Format("can not parse response of {0}!", MethodName), status, response_body, ex);
            }
            if (!r.ok)
                throw new Failed_to_execute(r);
            return r;
        }

        BaseResponse SimpleBaseProcessResponse(HttpStatusCode status, string response_body, string MethodName)
        {
            return BaseProcessResponse<BaseResponse>(status, response_body, MethodName);
        }

        public void DeleteAssembly(string id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base_url).Append("/DeleteAssembly?");

            sb.Append("&id=").Append(id);

            string ContentType = "application/null";
            HttpStatusCode status;
            byte[] post = null;
            string url = sb.ToString();
            string response_body = HttpPost(url, post, out status, ContentType, access_token);
            BaseProcessResponse<DeleteAssemblyResponse>(status, response_body, "DeleteAssembly");
        }

        static public string PrepareUrlArgument_auth(string license_token, string ContractNumber, Manager_for_auth manager = null, System.DateTime? time = null)
        {
            return Prepare_auth(license_token, ContractNumber, manager, time, "DTLAH59GNPMCPDIADKOPAAAW");
        }

        static string Prepare_auth(string license_token, string ContractNumber, Manager_for_auth manager, System.DateTime? time, string key)
        {
            if (null == time)
                time = System.DateTime.Now;

            Auth_info auth = new Auth_info
            {
                license_token = license_token,
                ContractNumber = ContractNumber,
                time = System.TimeZoneInfo.ConvertTimeToUtc(time.Value).ToString("o"),
                manager = manager
            };

            return auth.Encrypt(key);
        }

        public class Exception : System.Exception
        {
            public HttpStatusCode status;
            public string response_body;
            public Exception(string text, HttpStatusCode s, string b)
                : base(text)
            {
                status = s;
                response_body = b;
            }
            public Exception(string text, HttpStatusCode s, string b, System.Exception e)
                : base(text, e)
            {
                status = s;
                response_body = b;
            }
        }

        public class Failed_to_execute : System.Exception
        {
            public BaseResponse response;
            public Failed_to_execute(BaseResponse response) : base(response.message)
            {
                this.response = response;
            }
        }

        void SafeFixProxy(HttpWebRequest request)
        {
            if (!string.IsNullOrEmpty(proxy_host))
            {
                WebProxy proxy = new WebProxy(proxy_host, proxy_port);
                if (!string.IsNullOrEmpty(proxy_login))
                    proxy.Credentials = new NetworkCredential(proxy_login, proxy_password);
                request.Proxy = proxy;
            }
        }

        string HttpGet(string url, out HttpStatusCode status, string access_token = null)
        {
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);

            request.Method = "GET";
            if (!string.IsNullOrEmpty(access_token))
                request.Headers.Add("ama-datamart-access-token", access_token);

            return ReadResponse(request, out status);
        }

        string HttpPost(string url, byte[] postBytes, out HttpStatusCode status, string ContentType = "application/x-www-form-urlencoded", string access_token = null)
        {
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);

            request.Method = "POST";
            request.ContentType = ContentType;
            if (!string.IsNullOrEmpty(access_token))
                request.Headers.Add("ama-datamart-access-token", access_token);

            if (null == postBytes || 0 == postBytes.Length)
            {
                request.ContentLength = 0;
            }
            else
            {
                request.ContentLength = postBytes.Length;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(postBytes, 0, postBytes.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }
            }

            return ReadResponse(request, out status);
        }

        static byte[] ReadBinaryResponse(HttpWebRequest request)
        {
            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
            using (Stream receiveStream = response.GetResponseStream())
            {
                HttpStatusCode status = response.StatusCode;
                if (HttpStatusCode.OK != status)
                {
                    using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                    {
                        string error_text = ReadErrorResponseText(readStream);
                        throw new Exception("Can not read binary stream!", status, error_text);
                    }
                }
                else
                {
                    string txt_content_len = response.Headers[HttpResponseHeader.ContentLength];
                    int content_len = int.Parse(txt_content_len);
                    byte[] bytes = new byte[content_len];
                    receiveStream.Read(bytes, 0, content_len);
                    return bytes;
                }
            }
        }

        static string ReadResponse(HttpWebRequest request, out HttpStatusCode status)
        {
            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
            using (Stream receiveStream = response.GetResponseStream())
            using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
            {
                status = response.StatusCode;
                return ReadResponseText(status, readStream);
            }
        }

        static string ReadErrorResponseText(StreamReader readStream)
        {
            StringBuilder sb = new StringBuilder();
            for (int count = 0; count < 100; count++) // в случае ошибки читаем первый 100 строк текста об ошибке
            {
                string line = readStream.ReadLine();
                if (null == line)
                {
                    break;
                }
                else
                {
                    sb.AppendLine(line);
                }
            }
            return sb.ToString();
        }

        static string ReadResponseText(HttpStatusCode status, StreamReader readStream)
        {
            return (HttpStatusCode.OK == status)
                    ? readStream.ReadToEnd()
                    : ReadErrorResponseText(readStream);
        }

        public class BankroTechAccount
        {
            public string login;
            public string password;
            public string domain_name;

            public string Encrypt()
            {
                JavaScriptSerializer s = new JavaScriptSerializer();
                string Account = s.Serialize(this);

                TripleDESCryptoServiceProvider tripleDESProvider = new TripleDESCryptoServiceProvider();
                tripleDESProvider.Key = ASCIIEncoding.ASCII.GetBytes("DITVFNEWNPMCPDIADKOPAAAW");
                tripleDESProvider.Mode = CipherMode.ECB;

                byte[] bytes_Account = Encoding.UTF8.GetBytes(Account);
                byte[] encrypted_bytes_Account = tripleDESProvider.CreateEncryptor().TransformFinalBlock(bytes_Account, 0, bytes_Account.Length);
                string res = HttpUtility.UrlEncode(System.Convert.ToBase64String(encrypted_bytes_Account));
                return res;
            }
        }

        public class Manager_for_auth
        {
            public string name;
            public string surname;
            public string patronymic;
            public string efrsb_number;
            public string inn;
        }

        public class Manager : Manager_for_auth
        {
            public BankroTechAccount BankroTechAccount;
        }

        public class Идентифицирующие_реквизиты_должника
        {
            public string ИНН;
            public string СНИЛС;
            public string ОГРН;

            public static void CopyFromTo(Идентифицирующие_реквизиты_должника from, Идентифицирующие_реквизиты_должника to)
            {
                to.ИНН = from.ИНН;
                to.СНИЛС = from.СНИЛС;
                to.ОГРН = from.ОГРН;
            }
        }

        public class Должник : Идентифицирующие_реквизиты_должника
        {
            public string Наименование;

            public static void CopyFromTo(Должник from, Должник to)
            {
                Идентифицирующие_реквизиты_должника.CopyFromTo(from,to);
                to.Наименование = from.Наименование;
            }
        }

        public class ProcedureStart : Должник
        {
            public string CaseNumber; // номер дела

            public string Адрес; // адрес должника
            public string Суд;

            public string m_INN; // ИНН Арбитражного управляющего
            public string m_efrsbNumber; // рег номер на ЕФРСБ Арбитражного управляющего
        }

        public class Процедура
        {
            public string Номер_дела;
            public string Тип;

            public static void CopyFromTo(Процедура from, Процедура to)
            {
                to.Номер_дела = from.Номер_дела;
                to.Тип = from.Тип;
            }
        }

        public class Debtor_id_info
        {
            public string inn;
            public string ogrn;
            public string snils;
        }

        public class Debtor : Debtor_id_info
        {
            public string name;
        }

        public class Procedure
        {
            public string case_number;
            public string type;
        }

        public class UploadInfo_base
        {
            public Manager manager = new Manager();
            public Debtor debtor = new Debtor();
            public Procedure procedure = new Procedure();
            public string AmaVersion;
        }

        public class UploadInfo : UploadInfo_base
        {
            public bool allow_ctb;
            public byte[] zipped_content;
            public string Content_hash;
        }

        public class UploadResponce
        {
            public string login;
            public string password;
            public string error_text;
            public Constraints constraints = null;

            public UploadResponce(string response_body)
            {
                JavaScriptSerializer s = new JavaScriptSerializer();
                UploadInternalResponseBody rb;
                try
                {
                    rb = s.Deserialize<UploadInternalResponseBody>(response_body);
                }
                catch (System.Exception ex)
                {
                    throw new System.Exception("can not deserialize json: " + response_body, ex);
                }

                TripleDESCryptoServiceProvider tripleDESProvider = new TripleDESCryptoServiceProvider();
                tripleDESProvider.Key = ASCIIEncoding.ASCII.GetBytes("DITVFNEWNPHFADIADKOPAAAW");
                tripleDESProvider.Mode = CipherMode.ECB;

                byte[] bytes_Signature = System.Convert.FromBase64String(rb.Signature);

                byte[] decrypted_bytes_Signature = tripleDESProvider.CreateDecryptor().TransformFinalBlock(bytes_Signature, 0, bytes_Signature.Length);
                string decrypted_Signature = Encoding.UTF8.GetString(decrypted_bytes_Signature);

                UploadInternalAnswer r;
                try
                {
                    r = s.Deserialize<UploadInternalAnswer>(decrypted_Signature);
                }
                catch (System.Exception ex)
                {
                    throw new System.Exception("can not deserialize json: " + decrypted_Signature, ex);
                }
                login = r.digest;
                password = r.subject;
                error_text = r.text;
                constraints = r.constraints;
            }
        }

        public class UploadInternalResponseBody
        {
            public string Signature;
        }

        public class Constraints_problem
        {
            public string type;
            public string details;
        }

        public class Constraints_parameters
        {
            public Manager_for_auth manager = new Manager_for_auth();
            public Debtor debtor = new Debtor();
            public Procedure procedure = new Procedure();
        }

        public class Constraints
        {
            public Constraints_parameters parameters;
            public List<Constraints_problem> problems;
        }

        public class UploadInternalAnswer
        {
            public string digest;
            public string subject;
            public string text;
            public Constraints constraints;
        }

        public class SubLevelInfo
        {
            public string addres;
            public string start_date;
        }

        public class SubLevelResponce
        {
            public string common_sum;
            public string employable_sum;
            public string infant_sum;
            public string pensioner_sum;
            public SubLevel_Reglament[] reglament;
            public string region;

            public SubLevelResponce(string response_body)
            {
                JavaScriptSerializer s = new JavaScriptSerializer();
                SubLevelInternalResponseBody rb;
                try
                {
                    rb = s.Deserialize<SubLevelInternalResponseBody>(response_body);
                }
                catch (System.Exception ex)
                {
                    throw new System.Exception("can not deserialize json: " + response_body, ex);
                }
                common_sum = rb.Common;
                employable_sum = rb.Employable;
                infant_sum = rb.Infant;
                pensioner_sum = rb.Pensioner;
                reglament = rb.Reglaments;
                region = rb.Region;
            }
        }

        public class SubLevel_Reglament
        {
            public string title;
            public string date;
            public string url;
        }

        public class SubLevelInternalResponseBody
        {
            public string Common;
            public string Employable;
            public string Infant;
            public string Pensioner;
            public SubLevel_Reglament[] Reglaments;
            public string Region;
        }

        public class BaseResponse
        {
            public bool ok;
            public string message;
        }

        public class DeleteAssemblyResponse : BaseResponse
        {
            public bool id_Meeting_can_be_forgotten;
        }

        public class CreateAssemblyResponse : BaseResponse
        {
            public string id_Meeting;
        }

        public class Auth_info
        {
            public string license_token;
            public string ContractNumber;
            public string time;
            public Manager_for_auth manager;

            public string Encrypt(string key)
            {
                JavaScriptSerializer s = new JavaScriptSerializer();
                string auth = s.Serialize(this);

                TripleDESCryptoServiceProvider tripleDESProvider = new TripleDESCryptoServiceProvider();
                tripleDESProvider.Key = ASCIIEncoding.ASCII.GetBytes(key);
                tripleDESProvider.Mode = CipherMode.ECB;

                byte[] bytes_Account = Encoding.UTF8.GetBytes(auth);
                byte[] encrypted_bytes_Account = tripleDESProvider.CreateEncryptor().TransformFinalBlock(bytes_Account, 0, bytes_Account.Length);
                string res = HttpUtility.UrlEncode(System.Convert.ToBase64String(encrypted_bytes_Account));
                return res;
            }
        }

        public class Регламентирующий_документ
        {
            public string Текстом;
            public string Название_номер;
            public string Дата;
            public string URL;
        }

        public class Timespan
        {
            public string start;
            public string end;
        }
        public class ReviewData
        {
            public string date;
            public Timespan time = new Timespan();
            public string address;
        }
        public class Review
        {
            public ReviewData data = new ReviewData();
            public ReviewData result = new ReviewData();
        }

        public class MeetingInfo
        {
            public Debtor debtor = new Debtor();
            public Review review = new Review();
            public string date;
            public string time;
            public string procedure_type;
            public string case_number;
            public string debtor_address;
            public string manager_phone;
        }

        public class День_рабочего_календаря
        {
            public string день;
            public string рабочий;
        }

        public class Регион
        {
            public string ОКАТО;
            public string Наименование;
        }

        public class Дни_рабочего_календаря
        {
            public День_рабочего_календаря[] Январь;
            public День_рабочего_календаря[] Февраль;
            public День_рабочего_календаря[] Март;
            public День_рабочего_календаря[] Апрель;
            public День_рабочего_календаря[] Май;
            public День_рабочего_календаря[] Июнь;
            public День_рабочего_календаря[] Июль;
            public День_рабочего_календаря[] Август;
            public День_рабочего_календаря[] Сентябрь;
            public День_рабочего_календаря[] Октябрь;
            public День_рабочего_календаря[] Ноябрь;
            public День_рабочего_календаря[] Декабрь;
        }

        public class Рабочий_календарь
        {
            public int Год;
            public Регион Регион;
            public int Revision;
            public Дни_рабочего_календаря Дни_календаря;
        }
    }
}