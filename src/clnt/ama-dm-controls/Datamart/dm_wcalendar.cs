/*
 * ВНИМАНИЕ! ATTENTION! AHTUNG!
 * Данный файл редактируется и тестируется в проекте "datamart"!
 * Он НЕ должен редактироваться в проекте "AMA"!

 Основной вариант использования в ПАУ в настоящее время:
------------------------
// начальная инициализация календарей:

string путь_к_файловому_хранилищу; // сюда подать путь к файловому храанилищу
ama.datamart.Рабочие_календари.Использовать_путь_к_файловому_хранилищу(путь_к_файловому_хранилищу);

ama.datamart.Рабочие_календари.Загрузить_из_папки(); // команда читает сохранённые в файловом хранилище календари

// обновление календарей (после установки программы и видимо в ходе обновления):

ama.datamart.Client ama_dm_client; // клиент витрины данных
// TODO: клиент перед использованием нужно инициализировать (установить base_url и настроить прокси) и вызвать метод авторизации..
string text_to_log= ama.datamart.Рабочие_календари.Обновить_с_сервера(ama_dm_client); // text_to_log логично вывести в progress log..

// для использование перекрыть метод ..:

bool[] RIT.AMA.SharpUtils.Actions.WorkCalendarManager.GetDaysInYear(int yearNo)
{
    switch (yearNo)
    {
        // тут те станартные варианты что есть сейчас..
        default:
            Рабочий_календарь ama_dm_календарь= Рабочие_календари.Календарь_РФ_на_год(yearNo);
            return null==ama_dm_календарь ? null : ama_dm_календарь.Признаки_рабочего_дня_по_номеру_дня_в_году();
    }
}
------------------------
 */
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace ama.datamart
{
    public abstract class Рабочий_календарь
    {
        public abstract int Год { get; internal set; }
        public abstract Client.Регион Регион { get; internal set; }
        public abstract int Revision { get; internal set; }
        public abstract AДни Дни { get; internal set; }

        public abstract bool[] Признаки_рабочего_дня_по_номеру_дня_в_году();

        public abstract class AДень
        {
            public abstract DateTime дата { get; internal set; }
            public abstract bool рабочий { get; internal set; }
        }

        public abstract class AДни
        {
            public abstract AДень[] Январь { get; internal set; }
            public abstract AДень[] Февраль { get; internal set; }
            public abstract AДень[] Март { get; internal set; }
            public abstract AДень[] Апрель { get; internal set; }
            public abstract AДень[] Май { get; internal set; }
            public abstract AДень[] Июнь { get; internal set; }
            public abstract AДень[] Июль { get; internal set; }
            public abstract AДень[] Август { get; internal set; }
            public abstract AДень[] Сентябрь { get; internal set; }
            public abstract AДень[] Октябрь { get; internal set; }
            public abstract AДень[] Ноябрь { get; internal set; }
            public abstract AДень[] Декабрь { get; internal set; }
        }

        public interface IStorer
        {
            IEnumerable<Client.Рабочий_календарь> Load();
            void Store(IEnumerable<Client.Рабочий_календарь> client_календари);
        }

        public interface IUpdateTimeStorer
        {
            DateTime? Load();
            void Store(DateTime время_обновления);
        }

        public interface IManager
        {
            void Clear();

            void Init(IEnumerable<Client.Рабочий_календарь> client_календари);
            string Update(Client client, IStorer storer, IUpdateTimeStorer time_storer, DateTime? time= null, int min_hours_to_update = 24);

            Рабочий_календарь Календарь_на_год(int year);
            IEnumerable<int> Года();
            IEnumerable<Рабочий_календарь> Все();
        }

        public interface IManager_из_хранилища : IManager
        {
            void Загрузить_из_хранилища();
            string Загрузить_новую_информацию_календарей_с_сервера(Client client, DateTime? time, int min_hours_to_update = 24);
        }
    }

    public class Рабочие_календари_base : Рабочий_календарь.IManager
    {
        List<Рабочий_календарь> календари;
        Dictionary<int, Dictionary<string, Рабочий_календарь>> by_Год_ОКАТО;

        public void Init(IEnumerable<Client.Рабочий_календарь> client_календари)
        {
            Clear();
            Append(client_календари);
        }

        void Append(IEnumerable<Client.Рабочий_календарь> client_календари)
        {
            if (null != client_календари)
            {
                foreach (Client.Рабочий_календарь client_календарь in client_календари)
                {
                    Рабочий_календарь календарь = new Рабочий_календарь_base(client_календарь);

                    if (null == календари)
                        календари = new List<Рабочий_календарь>();
                    календари.Add(календарь);

                    if (null == by_Год_ОКАТО)
                        by_Год_ОКАТО = new Dictionary<int, Dictionary<string, Рабочий_календарь>>();

                    Dictionary<string, Рабочий_календарь> by_ОКАТО;
                    if (!by_Год_ОКАТО.TryGetValue(календарь.Год, out by_ОКАТО))
                    {
                        by_ОКАТО = new Dictionary<string, Рабочий_календарь>();
                        by_Год_ОКАТО.Add(календарь.Год, by_ОКАТО);
                    }
                    by_ОКАТО[календарь.Регион.ОКАТО]= календарь;
                }
            }
        }

        public void Clear()
        {
            календари = null;
            by_Год_ОКАТО = null;
        }

        public Рабочий_календарь Календарь_на_год(int год)
        {
            if (null!= by_Год_ОКАТО)
            {
                Dictionary<string, Рабочий_календарь> by_ОКАТО;
                if (by_Год_ОКАТО.TryGetValue(год, out by_ОКАТО))
                {
                    Рабочий_календарь календарь;
                    if (by_ОКАТО.TryGetValue("00 000", out календарь)) // Российская Федерация..
                        return календарь;
                }
            }
            return null;
        }

        public IEnumerable<int> Года()
        {
            return календари
                .Where((w)=> { return "00 000"==w.Регион.ОКАТО; })
                .Select((w)=>{ return w.Год; });
        }

        public IEnumerable<Рабочий_календарь> Все()
        {
            return календари;
        }


        public string Update(Client client, Рабочий_календарь.IStorer storer, Рабочий_календарь.IUpdateTimeStorer time_storer, DateTime? time, int min_hours_to_update = 24)
        {
            DateTime? time_last_update = time_storer.Load();
            DateTime cur_time = null!= time ? time.Value : DateTime.Now;
            if (null!=time_last_update && (cur_time- time_last_update.Value).Hours < min_hours_to_update)
            {
                return string.Format("Рабочие календари обновлялись недостаточно давно чтобы запрашивать изменения ({0})", time_last_update);
            }
            else
            {
                int max_revision = 0;
                if (null != календари)
                {
                    foreach (Рабочий_календарь w in календари)
                    {
                        if (w.Revision > max_revision)
                            max_revision = w.Revision;
                    }
                }
                Client.Рабочий_календарь[] client_календари = client.Получить_рабочие_календари(max_revision);
                Append(client_календари);
                storer.Store(client_календари);
                time_storer.Store(cur_time);

                return 0== client_календари.Length
                    ? "На сервере нет обновлений годовых робочих календарей"
                    : string.Format("Загружено {0} обновлений годовых рабочих календарей с сервера", client_календари.Length);
            }
        }
    }

    public class Рабочий_календарь_base : Рабочий_календарь
    {
        public override int Год { get; internal set; }
        public override Client.Регион Регион { get; internal set; }
        public override int Revision { get; internal set; }
        public override AДни Дни { get; internal set; }

        internal Рабочий_календарь_base(Client.Рабочий_календарь client_календарь)
        {
            Год = client_календарь.Год;
            Регион = client_календарь.Регион;
            Revision = client_календарь.Revision;
            Дни = new Дни_календаря(client_календарь.Дни_календаря);
        }

        public class День : AДень
        {
            public override DateTime дата { get; internal set; }
            public override bool рабочий { get; internal set; }

            internal День(Client.День_рабочего_календаря d)
            {
                дата = DateTime.Parse(d.день);
                рабочий = "true" == d.рабочий;
            }
        }

        public override bool[] Признаки_рабочего_дня_по_номеру_дня_в_году()
        {
            int количество_дней_в_году = 29 == Дни.Февраль.Length ? 366 : 365;
            bool[] признаки_рабочего_дня_по_номеру_дня_в_году = new bool[количество_дней_в_году];
            int i = 0;
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Январь, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Февраль, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Март, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Апрель, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Май, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Июнь, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Июль, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Август, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Сентябрь, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Октябрь, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Ноябрь, ref i);
            fill(признаки_рабочего_дня_по_номеру_дня_в_году, Дни.Декабрь, ref i);
            return признаки_рабочего_дня_по_номеру_дня_в_году;
        }

        void fill(bool[] признаки_рабочего_дня_по_номеру_дня_в_году, AДень[] дни, ref int i)
        {
            foreach (AДень d in дни)
                признаки_рабочего_дня_по_номеру_дня_в_году[i++] = d.рабочий;
        }

        public class Дни_календаря : AДни
        {
            public override AДень[] Январь { get; internal set; }
            public override AДень[] Февраль { get; internal set; }
            public override AДень[] Март { get; internal set; }
            public override AДень[] Апрель { get; internal set; }
            public override AДень[] Май { get; internal set; }
            public override AДень[] Июнь { get; internal set; }
            public override AДень[] Июль { get; internal set; }
            public override AДень[] Август { get; internal set; }
            public override AДень[] Сентябрь { get; internal set; }
            public override AДень[] Октябрь { get; internal set; }
            public override AДень[] Ноябрь { get; internal set; }
            public override AДень[] Декабрь { get; internal set; }

            internal Дни_календаря(Client.Дни_рабочего_календаря client_дни)
            {
                Январь = create(client_дни.Январь);
                Февраль = create(client_дни.Февраль);
                Март = create(client_дни.Март);
                Апрель = create(client_дни.Апрель);
                Май = create(client_дни.Май);
                Июнь = create(client_дни.Июнь);
                Июль = create(client_дни.Июль);
                Август = create(client_дни.Август);
                Сентябрь = create(client_дни.Сентябрь);
                Октябрь = create(client_дни.Октябрь);
                Ноябрь = create(client_дни.Ноябрь);
                Декабрь = create(client_дни.Декабрь);
            }

            День[] create(Client.День_рабочего_календаря[] client_дни)
            {
                День[] res = new День[client_дни.Length];
                int i = 0;
                foreach (Client.День_рабочего_календаря d in client_дни)
                {
                    res[i++] = new День(d);
                }
                return res;
            }
        }
    }

    public abstract class Из_файловой_папки
    {
        protected string base_path;
        internal Из_файловой_папки(string base_path)
        {
            this.base_path = base_path;
        }

        public class Время_обновления : Из_файловой_папки, Рабочий_календарь.IUpdateTimeStorer
        {
            public Время_обновления(string base_path) : base(base_path) { }

            public DateTime? Load()
            {
                string filepath = FilePath;
                if (File.Exists(filepath))
                {
                    string txt_время_обновления = File.ReadAllText(filepath);
                    DateTime res;
                    if (DateTime.TryParse(txt_время_обновления, out res))
                        return res;
                }
                return null;
            }

            public void Store(DateTime время_обновления)
            {
                if (!Directory.Exists(base_path))
                    Directory.CreateDirectory(base_path);
                string txt_время_обновления = время_обновления.ToString();
                File.WriteAllText(FilePath, txt_время_обновления);
            }

            string FilePath { get { return Path.Combine(base_path, "time.txt"); } }
        }

        public class Рабочие_календари : Из_файловой_папки, Рабочий_календарь.IStorer
        {
            static System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            public Рабочие_календари(string base_path) : base(base_path) { }

            public IEnumerable<Client.Рабочий_календарь> Load()
            {
                if (Directory.Exists(base_path))
                {
                    string[] files = Directory.GetFiles(base_path, "*.wcal");
                    if (null != files && 0 != files.Length)
                    {
                        List<Client.Рабочий_календарь> client_календари = new List<Client.Рабочий_календарь>();
                        foreach (string file in files)
                        {
                            string content = File.ReadAllText(file);
                            Client.Рабочий_календарь client_календарь = serializer.Deserialize<Client.Рабочий_календарь>(content);
                            client_календари.Add(client_календарь);
                        }
                        return client_календари;
                    }
                }
                return null;
            }

            public void Store(IEnumerable<Client.Рабочий_календарь> client_календари)
            {
                if (!Directory.Exists(base_path))
                    Directory.CreateDirectory(base_path);
                foreach (Client.Рабочий_календарь календарь in client_календари)
                {
                    string filename = string.Format("{0}_{1}.wcal", календарь.Год, календарь.Регион.ОКАТО.Replace(" ", "_"));
                    File.WriteAllText(Path.Combine(base_path, filename), serializer.Serialize(календарь));
                }
            }
        }
    }

    public class Рабочие_календари_из_хранилища : Рабочие_календари_base, Рабочий_календарь.IManager_из_хранилища
    {
        Рабочий_календарь.IUpdateTimeStorer update_time_storer;
        Рабочий_календарь.IStorer storer;

        public Рабочие_календари_из_хранилища(Рабочий_календарь.IUpdateTimeStorer update_time_storer, Рабочий_календарь.IStorer storer)
        {
            this.update_time_storer = update_time_storer;
            this.storer = storer;
        }

        public void Загрузить_из_хранилища()
        {
            Init(storer.Load());
        }

        public string Загрузить_новую_информацию_календарей_с_сервера(Client client, DateTime? time, int min_hours_to_update = 24)
        {
            return Update(client, storer, update_time_storer, time, min_hours_to_update);
        }
    }

    public class Рабочие_календари : Рабочие_календари_из_хранилища
    {
        public Рабочие_календари(string base_path)
            : base(new Из_файловой_папки.Время_обновления(base_path), new Из_файловой_папки.Рабочие_календари(base_path))
        {
        }

        public static Рабочий_календарь.IManager_из_хранилища Instance { get; private set; }
        public static void Использовать_путь_к_файловому_хранилищу(string path_to_file_storage)
        {
            string base_path = Path.Combine(path_to_file_storage, "calendar");
            Instance = new Рабочие_календари(base_path);
        }
        public static void Загрузить_из_папки()
        {
            Instance.Загрузить_из_хранилища();
        }
        public static string Обновить_с_сервера(Client client, DateTime? time= null, int min_hours_to_update = 24)
        {
            return Instance.Загрузить_новую_информацию_календарей_с_сервера(client, time, min_hours_to_update);
        }
        public static Рабочий_календарь Календарь_РФ_на_год(int year)
        {
            return Instance.Календарь_на_год(year);
        }
    }
}