﻿namespace ama.datamart
{
    partial class UCEfrsbClasses
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkLabelClasses = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // linkLabelClasses
            // 
            this.linkLabelClasses.AutoEllipsis = true;
            this.linkLabelClasses.AutoSize = true;
            this.linkLabelClasses.Location = new System.Drawing.Point(0, 0);
            this.linkLabelClasses.MaximumSize = new System.Drawing.Size(150, 0);
            this.linkLabelClasses.Name = "linkLabelClasses";
            this.linkLabelClasses.Size = new System.Drawing.Size(144, 30);
            this.linkLabelClasses.TabIndex = 0;
            this.linkLabelClasses.TabStop = true;
            this.linkLabelClasses.Text = "Не выбран ни один класс! Кликните, чтобы указать..";
            this.linkLabelClasses.UseCompatibleTextRendering = true;
            this.linkLabelClasses.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelClasses_LinkClicked);
            // 
            // UCEfrsbClasses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.linkLabelClasses);
            this.Name = "UCEfrsbClasses";
            this.Resize += new System.EventHandler(this.UCEfrsbClasses_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkLabelClasses;
    }
}
