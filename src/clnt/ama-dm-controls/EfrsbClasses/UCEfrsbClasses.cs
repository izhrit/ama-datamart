﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ama.datamart
{
    public partial class UCEfrsbClasses : UserControl, IEfrsb_property_classes_editor
    {
        string m_NoClasses_Text;
        public UCEfrsbClasses()
        {
            InitializeComponent();
            m_NoClasses_Text = linkLabelClasses.Text;
        }

        List<Efrsb_property_class> m_classes = new List<Efrsb_property_class>();
        public void DataLoad(IList<Efrsb_property_class> classes)
        {
            m_classes.Clear();
            foreach (Efrsb_property_class c in classes)
            {
                m_classes.Add(c.Clone());
            }
            UpdateText();
        }

        public void DataSave(List<Efrsb_property_class> classes)
        {
            classes.Clear();
            foreach (Efrsb_property_class c in m_classes)
            {
                classes.Add(c.Clone());
            }
        }

        void UpdateText()
        {
            if (null == m_classes || 0 == m_classes.Count)
            {
                linkLabelClasses.Text = m_NoClasses_Text;
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach (Efrsb_property_class c in m_classes)
                {
                    sb.Append(" • ");
                    sb.Append(c.name);
                    sb.AppendLine(";");
                }
                linkLabelClasses.Text = sb.ToString();
            }
        }

        private void UCEfrsbClasses_Resize(object sender, EventArgs e)
        {
            Size s = this.Size;
            linkLabelClasses.MaximumSize= new Size(s.Width - 30,0);
        }

        private void linkLabelClasses_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (FormChooseEfrsbClasses frm= new FormChooseEfrsbClasses())
            {
                frm.DataLoad(m_classes);
                DialogResult res= frm.ShowDialog(this);
                if (DialogResult.OK==res)
                {
                    frm.DataSave(m_classes);
                    UpdateText();
                }
            }
        }
    }
}
