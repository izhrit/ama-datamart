﻿using System.Collections.Generic;

namespace ama.datamart
{
    public class Efrsb_property_class
    {
        public string code;
        public string name;

        public Efrsb_property_class Clone()
        {
            return new Efrsb_property_class { code = this.code, name = this.name };
        }
    }

    public interface IEfrsb_property_classes_editor
    {
        void DataLoad(IList<Efrsb_property_class> classes);
        void DataSave(List<Efrsb_property_class> classes);
    }
}
