﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Web.Script.Serialization;

namespace ama.datamart
{
    public partial class FormChooseEfrsbClasses : Form, IEfrsb_property_classes_editor
    {
        public FormChooseEfrsbClasses()
        {
            InitializeComponent();
        }

        private void FormChooseEfrsbClasses_Load(object sender, EventArgs e)
        {
            StaticHtmlControl.Start(webBrowser, "choose_efrsb_classes", new FormChooseEfrsbClasses_ObjectForScripting(this));
        }

        public StaticHtmlSettings Settings = new StaticHtmlSettings();

        internal List<Efrsb_property_class> m_classes = new List<Efrsb_property_class>();
        public void DataLoad(IList<Efrsb_property_class> classes)
        {
            m_classes.Clear();
            foreach (Efrsb_property_class c in classes)
            {
                m_classes.Add(c.Clone());
            }
        }

        public void DataSave(List<Efrsb_property_class> classes)
        {
            classes.Clear();
            foreach (Efrsb_property_class c in m_classes)
            {
                classes.Add(c.Clone());
            }
        }

        private void FormChooseEfrsbClasses_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!e.Cancel)
            {
                object res= webBrowser.Document.InvokeScript("GetEfrsbClassesToSave");
                if (null!=res)
                {
                    string txt_res = res.ToString();
                    JavaScriptSerializer s = new JavaScriptSerializer();
                    m_classes = s.Deserialize<List<Efrsb_property_class>>(txt_res);
                }
            }
        }
    }

    [ComVisible(true)]
    public class FormChooseEfrsbClasses_ObjectForScripting
    {
        FormChooseEfrsbClasses m_form;
        internal FormChooseEfrsbClasses_ObjectForScripting(FormChooseEfrsbClasses frm)
        {
            m_form = frm;
        }

        public string GetEfrsbClassesToLoad()
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Serialize(m_form.m_classes);
        }

        public string GetSettings()
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            m_form.Settings.FixBackgroundColor(m_form);
            return s.Serialize(m_form.Settings);
        }
    }
}
