﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Web.Script.Serialization;
using System.Diagnostics;

namespace ama.datamart
{
    public partial class FormExposureSettings : Form, IExposureRecipients_editor
    {
        string m_FormTitleTemplate;
        string m_ProcedureName;
        int m_CountOfSelectedObjects = 0;
        public Exposure_settings Settings = new Exposure_settings();

        public FormExposureSettings()
        {
            InitializeComponent();
            Settings.for_start = true;
            m_FormTitleTemplate = Text;
            ProcedureName = "";
            CountOfSelectedObjects = 0;
            UpdateRadioButtonsStyle();
            webBrowser.Anchor |= (AnchorStyles.Right| AnchorStyles.Bottom);
        }

        private void FormExposureSettings_Load(object sender, EventArgs e)
        {
            StaticHtmlControl.Start(webBrowser, "property_recipients", new FormExposureSettings_ObjectForScripting(this));
        }

        public string ProcedureName
        {
            get { return m_ProcedureName; }
            set
            {
                m_ProcedureName = value;
                Text = string.Format(m_FormTitleTemplate,m_ProcedureName);
            }
        }

        string build_numbering(int count, string n1, string n2, string n10)
        {
            return ((count % 10 == 1 && count % 100 != 11) ? n1 :
                    ((count % 10 >= 2 && count % 10 <= 4 && (count % 100 < 10 || count % 100 >= 20)) ? n2 : n10));
        }

        public int CountOfSelectedObjects
        {
            set
            {
                m_CountOfSelectedObjects = value;
                string choosed_numbering = build_numbering(m_CountOfSelectedObjects, n1: "выбран", n2: "выбрано", n10: "выбрано");
                string obj_numbering = build_numbering(m_CountOfSelectedObjects,n1:"объект",n2:"объекта",n10:"объектов");
                labelCountOfSelectedObjects.Text= string.Format("({2} {0} {1})", m_CountOfSelectedObjects, obj_numbering, choosed_numbering);
                if (0== m_CountOfSelectedObjects)
                {
                    radioButtonSelectedObjects.Checked = false;
                    radioButtonAllTheObjects.Checked = true;
                }
                else
                {
                    radioButtonSelectedObjects.Checked = true;
                    radioButtonAllTheObjects.Checked = false;
                }
            }
        }

        private void radioButtonAllTheObjects_CheckedChanged(object sender, EventArgs e)
        {
            UpdateRadioButtonsStyle();
        }

        private void radioButtonSelectedObjects_CheckedChanged(object sender, EventArgs e)
        {
            UpdateRadioButtonsStyle();
        }

        void UpdateRadioButtonsStyle()
        {
            UpdateRadioButtonStyle(radioButtonSelectedObjects);
            UpdateRadioButtonStyle(radioButtonAllTheObjects);
        }

        void UpdateRadioButtonStyle(RadioButton rb)
        {
            rb.Font = new Font(rb.Font, rb.Checked ? FontStyle.Bold : FontStyle.Regular);
        }

        internal ExposureRecipients m_Recipients= new ExposureRecipients();
        public void DataLoad(ExposureRecipients er)
        {
            ExposureRecipients.CopyFromTo(er, m_Recipients);
        }

        public void DataSave(ExposureRecipients er)
        {
            ExposureRecipients.CopyFromTo(m_Recipients, er);
        }

        private void FormExposureSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!e.Cancel)
            {
                object res = webBrowser.Document.InvokeScript("GetExposureRecipientsToSave");
                if (null != res)
                {
                    string txt_res = res.ToString();
                    JavaScriptSerializer s = new JavaScriptSerializer();
                    m_Recipients = s.Deserialize<ExposureRecipients>(txt_res);
                }
            }
        }
    }

    [ComVisible(true)]
    public class FormExposureSettings_ObjectForScripting
    {
        FormExposureSettings m_form;
        internal FormExposureSettings_ObjectForScripting(FormExposureSettings frm)
        {
            m_form = frm;
        }

        public string GetSettings()
        {
            Exposure_settings settings= m_form.Settings;
            settings.FixBackgroundColor(m_form);
            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Serialize(settings);
        }

        public string GetExposureRecipientsToLoad()
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Serialize(m_form.m_Recipients);
        }
    }
}
