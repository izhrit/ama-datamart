﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ama.datamart
{
    public partial class UCExposureRecipients : UserControl, IExposureRecipients_editor
    {
        public Exposure_settings Settings = new Exposure_settings();

        string m_NoRecipients_Text;
        public UCExposureRecipients()
        {
            InitializeComponent();
            //linkLabelRecipients.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            m_NoRecipients_Text = linkLabelRecipients.Text.Trim();
        }

        ExposureRecipients m_recipients= new ExposureRecipients();
        public void DataLoad(ExposureRecipients er)
        {
            ExposureRecipients.CopyFromTo(er,m_recipients);
            UpdateText();
        }

        public void DataSave(ExposureRecipients er)
        {
            ExposureRecipients.CopyFromTo(m_recipients, er);
            UpdateText();
        }

        void UpdateText()
        {
            if (m_recipients.Empty())
            {
                linkLabelRecipients.Text = m_NoRecipients_Text;
            }
            else
            {
                List<string> txt_recipients = new List<string>();
                if (m_recipients.ToAll)
                    txt_recipients.Add("все заинтересованные лица");
                if (m_recipients.ToSelectedPro)
                {
                    foreach (ExposureRecipientPro r in m_recipients.SelectedPro)
                        txt_recipients.Add(r.name);
                }
                linkLabelRecipients.Text= string.Join(", ", txt_recipients.ToArray());
            }
        }

        private void linkLabelRecipients_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (FormExposure frm = new FormExposure())
            {
                frm.Settings = this.Settings;
                frm.DataLoad(m_recipients);
                DialogResult res = frm.ShowDialog(this);
                if (DialogResult.OK == res)
                {
                    frm.DataSave(m_recipients);
                    UpdateText();
                }
            }
        }

        private void UCExposure_Resize(object sender, EventArgs e)
        {
            Size s = this.Size;
            linkLabelRecipients.MaximumSize = new Size(s.Width - 30, s.Height-2);
        }
    }
}
