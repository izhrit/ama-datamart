﻿namespace ama.datamart
{
    partial class FormExposureSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelChooseObjects = new System.Windows.Forms.Label();
            this.radioButtonAllTheObjects = new System.Windows.Forms.RadioButton();
            this.radioButtonSelectedObjects = new System.Windows.Forms.RadioButton();
            this.labelChooseSubjects = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelCountOfSelectedObjects = new System.Windows.Forms.Label();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // labelChooseObjects
            // 
            this.labelChooseObjects.AutoSize = true;
            this.labelChooseObjects.Location = new System.Drawing.Point(12, 9);
            this.labelChooseObjects.Name = "labelChooseObjects";
            this.labelChooseObjects.Size = new System.Drawing.Size(482, 13);
            this.labelChooseObjects.TabIndex = 0;
            this.labelChooseObjects.Text = "Укажите, какие объекты конкурсной массы вы хотите показывать на Витрине данных ПА" +
    "У:";
            // 
            // radioButtonAllTheObjects
            // 
            this.radioButtonAllTheObjects.AutoSize = true;
            this.radioButtonAllTheObjects.Checked = true;
            this.radioButtonAllTheObjects.Location = new System.Drawing.Point(24, 29);
            this.radioButtonAllTheObjects.Name = "radioButtonAllTheObjects";
            this.radioButtonAllTheObjects.Size = new System.Drawing.Size(229, 17);
            this.radioButtonAllTheObjects.TabIndex = 1;
            this.radioButtonAllTheObjects.TabStop = true;
            this.radioButtonAllTheObjects.Text = "Все, зарегистрированные по процедуре";
            this.radioButtonAllTheObjects.UseVisualStyleBackColor = true;
            this.radioButtonAllTheObjects.CheckedChanged += new System.EventHandler(this.radioButtonAllTheObjects_CheckedChanged);
            // 
            // radioButtonSelectedObjects
            // 
            this.radioButtonSelectedObjects.AutoSize = true;
            this.radioButtonSelectedObjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioButtonSelectedObjects.Location = new System.Drawing.Point(24, 52);
            this.radioButtonSelectedObjects.Name = "radioButtonSelectedObjects";
            this.radioButtonSelectedObjects.Size = new System.Drawing.Size(468, 17);
            this.radioButtonSelectedObjects.TabIndex = 2;
            this.radioButtonSelectedObjects.Text = "Выбранные в настоящий момент в разделе \"Конкурсная масса\" объекты";
            this.radioButtonSelectedObjects.UseVisualStyleBackColor = true;
            this.radioButtonSelectedObjects.CheckedChanged += new System.EventHandler(this.radioButtonSelectedObjects_CheckedChanged);
            // 
            // labelChooseSubjects
            // 
            this.labelChooseSubjects.AutoSize = true;
            this.labelChooseSubjects.Location = new System.Drawing.Point(12, 88);
            this.labelChooseSubjects.Name = "labelChooseSubjects";
            this.labelChooseSubjects.Size = new System.Drawing.Size(622, 13);
            this.labelChooseSubjects.TabIndex = 3;
            this.labelChooseSubjects.Text = "Укажите круг лиц, которым вы хотите сделать доступной информацию о вышеуказанных " +
    "объектах конкурсной массы:";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(559, 355);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(478, 355);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 8;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // labelCountOfSelectedObjects
            // 
            this.labelCountOfSelectedObjects.AutoSize = true;
            this.labelCountOfSelectedObjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCountOfSelectedObjects.Location = new System.Drawing.Point(493, 54);
            this.labelCountOfSelectedObjects.Name = "labelCountOfSelectedObjects";
            this.labelCountOfSelectedObjects.Size = new System.Drawing.Size(119, 13);
            this.labelCountOfSelectedObjects.TabIndex = 9;
            this.labelCountOfSelectedObjects.Text = "(выбрано N объектов)";
            // 
            // webBrowser
            // 
            this.webBrowser.Location = new System.Drawing.Point(20, 111);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(614, 238);
            this.webBrowser.TabIndex = 10;
            // 
            // FormExposureSettings
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(646, 388);
            this.Controls.Add(this.webBrowser);
            this.Controls.Add(this.labelCountOfSelectedObjects);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelChooseSubjects);
            this.Controls.Add(this.radioButtonSelectedObjects);
            this.Controls.Add(this.radioButtonAllTheObjects);
            this.Controls.Add(this.labelChooseObjects);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinimumSize = new System.Drawing.Size(662, 427);
            this.Name = "FormExposureSettings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Подготовка КМ {0} к торгам";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormExposureSettings_FormClosing);
            this.Load += new System.EventHandler(this.FormExposureSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelChooseObjects;
        private System.Windows.Forms.RadioButton radioButtonAllTheObjects;
        private System.Windows.Forms.RadioButton radioButtonSelectedObjects;
        private System.Windows.Forms.Label labelChooseSubjects;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label labelCountOfSelectedObjects;
        private System.Windows.Forms.WebBrowser webBrowser;
    }
}