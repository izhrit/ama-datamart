﻿using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace ama.datamart
{
    public static partial class Regions
    {
        static Dictionary<string, string> okato_by_name = null;
        static Dictionary<string, string> name_by_okato = null;
        public static string Get_OKATO_by_Region(string region)
        {
            if (null== okato_by_name)
            {
                Dictionary<string, string> d = new Dictionary<string, string>();
                foreach (string [] da in region_okato)
                {
                    string name = da[0];
                    string okato = da[1];
                    d.Add(name,okato);
                }
                okato_by_name = d;
            }
            string res;
            return !okato_by_name.TryGetValue(region,out res) ? null : res;
        }
        public static string Get_Region_by_OKATO(string okato)
        {
            if (null == name_by_okato)
            {
                Dictionary<string, string> d = new Dictionary<string, string>();
                foreach (string[] da in region_okato)
                {
                    string aname = da[0];
                    string aokato = da[1];
                    d.Add(aokato, aname);
                }
                name_by_okato = d;
            }
            string res;
            return !name_by_okato.TryGetValue(okato, out res) ? null : res;
        }
        static public void Fill(System.Windows.Forms.ComboBox cbox)
        {
            foreach (string [] region in region_okato)
            {
                string region_title = region[0];
                string region_okato = region[1];
                cbox.Items.Add(region_title);
            }
        }
    }

    public class ExposureRecipientPro
    {
        public string id;
        public string name;
        public string url;
        public string descr;

        public ExposureRecipientPro Clone()
        {
            return new ExposureRecipientPro { id= this.id, name= this.name, url= this.url, descr= this.descr };
        }
    }

    public class Exposure_settings : StaticHtmlSettings
    {
        public List<ExposureRecipientPro> recipients= new List<ExposureRecipientPro>();
        public bool for_start= false;
    }

    public class ExposureRecipients
    {
        public bool Transmit;
        public bool ToAll;
        public bool ToSelectedPro;
        public List<ExposureRecipientPro> SelectedPro = new List<ExposureRecipientPro>();

        public static void CopyFromTo(ExposureRecipients from, ExposureRecipients to)
        {
            to.Transmit = from.Transmit;
            to.ToAll = from.ToAll;
            to.ToSelectedPro = from.ToSelectedPro;
            to.SelectedPro.Clear();
            foreach (ExposureRecipientPro r in from.SelectedPro)
                to.SelectedPro.Add(r.Clone());
        }

        public bool Empty()
        {
            return !ToAll
                && (!ToSelectedPro || 0 == SelectedPro.Count);
        }
    }

    public interface IExposureRecipients_editor
    {
        void DataLoad(ExposureRecipients er);
        void DataSave(ExposureRecipients er);
    }

    internal static class Перечисления
    {
        static public void Fill(System.Type t, System.Windows.Forms.ComboBox cbox)
        {
            System.Type tstring = typeof(string);
            System.Reflection.MemberInfo[] members = t.GetMembers();

            foreach (System.Reflection.MemberInfo mi in members)
            {
                System.Reflection.FieldInfo fi = mi as System.Reflection.FieldInfo;
                if (null != fi && tstring == fi.FieldType)
                    cbox.Items.Add(fi.GetValue(null));
            }
        }
    }

    public static class Категория_ОбъектаКМ_для_экспозиции
    {
        static public string Недвижимость = "Недвижимость";
        static public string Транспорт = "Транспортные средства";
        static public string Оборудование = "Оборудование";
        static public string Ценные_бумаги = "Ценные бумаги";
        static public string Дебиторка = "Дебиторская задолженность";
        static public string Прочее = "Прочее";

        static public void Fill(System.Windows.Forms.ComboBox cbox)
        {
            Перечисления.Fill(typeof(Категория_ОбъектаКМ_для_экспозиции),cbox);
        }
    }

    public static class Стадия_экспозиции_ОбъектаКМ
    {
        static public string Инвентаризация = "Инвентаризация";
        static public string Отсутствие = "Зарегистрировано отсутствие";
        static public string Выбыло = "Выбыло из конкурсной массы";
        static public string На_продажу = "Определение условий продажи";
        static public string Без_торгов = "Продажа без торгов";
        static public string Без_торгов_покупается = "Определён покупатель без торгов";
        static public string Продано = "Продано";
        static public string Торги1 = "Первые торги";
        static public string Торги1_завершены = "Первые торги завершены";
        static public string Торги1_покупается= "Определён покупатель в первых торгах";
        static public string Торги2 = "Повторные торги";
        static public string Торги2_завершены = "Повторные торги завершены";
        static public string Торги2_покупается = "Определён покупатель в повторных торгах";
        static public string Торги3 = "Публичное предложение";
        static public string Торги3_завершены = "Публичное предложение завершено";
        static public string Торги3_покупается = "Определён покупатель в публичном предложении";

        static public void Fill(System.Windows.Forms.ComboBox cbox)
        {
            Перечисления.Fill(typeof(Стадия_экспозиции_ОбъектаКМ), cbox);
        }
    }

    public class Данные_вкладки_экспозиция_объекта_КМ
    {
        public string Категория;
        public string Стадия;
        public ExposureRecipients Круг_лиц_с_доступом = new ExposureRecipients();
        public List<Efrsb_property_class> КлассификацияЕФРСБ = new List<Efrsb_property_class>();

        public static void CopyFromTo(Данные_вкладки_экспозиция_объекта_КМ from, Данные_вкладки_экспозиция_объекта_КМ to)
        {
            to.Категория = from.Категория;
            to.Стадия = from.Стадия;
            ExposureRecipients.CopyFromTo(from.Круг_лиц_с_доступом,to.Круг_лиц_с_доступом);

            to.КлассификацияЕФРСБ.Clear();
            foreach (Efrsb_property_class c in from.КлассификацияЕФРСБ)
                to.КлассификацияЕФРСБ.Add(c.Clone());
        }
    }

    public interface IВкладка_экспозиция_объекта_КМ
    {
        void DataLoad(Данные_вкладки_экспозиция_объекта_КМ e);
        void DataSave(Данные_вкладки_экспозиция_объекта_КМ e);
    }

    public static class Hash
    {
        static System.Security.Cryptography.MD5 provider;
        static System.Text.StringBuilder sb;
        public static string md5(byte [] bytes)
        {
            if (null== provider)
                provider= System.Security.Cryptography.MD5.Create();
            if (null == sb)
                sb = new System.Text.StringBuilder();
            sb.Length = 0;
            byte[] hash = provider.ComputeHash(bytes);
            foreach (byte b in hash)
                sb.Append(b.ToString("x2").ToLower());

            return sb.ToString();
        }
    }

    public class ОбъектКМ_для_экспозиции : Данные_вкладки_экспозиция_объекта_КМ
    {
        public string ID_Object;

        public string Краткое_наименование;
        public string Развёрнутое_описание;

        public string Адрес;
        public string ОКАТО;

        public string to_string_for_md5()
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            ОбъектКМ_для_экспозиции for_md5 = new ОбъектКМ_для_экспозиции();
            CopyFromTo(this, for_md5);

            foreach (Efrsb_property_class c in for_md5.КлассификацияЕФРСБ)
                c.name = null;
            for_md5.КлассификацияЕФРСБ.Sort((a, b) => { return string.Compare(a.code, b.code); });

            foreach (ExposureRecipientPro r in for_md5.Круг_лиц_с_доступом.SelectedPro)
            {
                r.name = null;
                r.url = null;
                r.descr = null;
            }
            for_md5.Круг_лиц_с_доступом.SelectedPro.Sort((a, b) => { return string.Compare(a.id, b.id); });

            return s.Serialize(for_md5);
        }

        public string md5()
        {
            string txt = to_string_for_md5();
            string res= Hash.md5(System.Text.Encoding.UTF8.GetBytes(txt));
            return res;
        }

        public static void CopyFromTo(ОбъектКМ_для_экспозиции from, ОбъектКМ_для_экспозиции to)
        {
            Данные_вкладки_экспозиция_объекта_КМ.CopyFromTo(from,to);
            to.ID_Object = from.ID_Object;

            to.Краткое_наименование = from.Краткое_наименование;
            to.Развёрнутое_описание = from.Развёрнутое_описание;

            to.Адрес = from.Адрес;
            to.ОКАТО = from.ОКАТО;
        }
    }

    public class Процедура_над_должником
    {
        public Client.Должник Должник = new Client.Должник();
        public Client.Процедура Процедура = new Client.Процедура();

        public Процедура_над_должником()
        {
            Должник = new Client.Должник();
            Процедура = new Client.Процедура();
        }

        public Процедура_над_должником(Процедура_над_должником процедура_над_должником)
        {
            Должник = процедура_над_должником.Должник;
            Процедура = процедура_над_должником.Процедура;
        }
    }

    public class Имущество_для_экспозиции
    {
        public List<ОбъектКМ_для_экспозиции> ОбъектыКМ= new List<ОбъектКМ_для_экспозиции>();
        public List<string> ID_Object_ОбъектовКМ_для_удаления = new List<string>();

        public void Clear()
        {
            ОбъектыКМ.Clear();
            ID_Object_ОбъектовКМ_для_удаления.Clear();
        }

        public bool Empty()
        {
            return 0 == ОбъектыКМ.Count && 0 == ID_Object_ОбъектовКМ_для_удаления.Count;
        }
    }

    public class Документ_к_ОбъектуКМ
    {
        public string filename;
        public string md5;
        public int size;
    }

    public class ОбъектКМ_для_экспозиции_digest
    {
        public string ID_Object;
        public string md5;
        public List<Документ_к_ОбъектуКМ> Документы = new List<Документ_к_ОбъектуКМ>();
    }

    public class Документ_для_экспозиции_на_загрузку
    {
        public string ID_Object;
        public string filename;
        public byte[] content;
    }

    public class Имущество_для_экспозиции_digest
    {
        public List<ОбъектКМ_для_экспозиции_digest> portion;
        public int total_count;
        public string after_ID_Object;
        public int count_after_ID_Object;
        public string id_MProcedure;
    }

    public interface IExposure_client
    {
        List<ExposureRecipientPro> GetExposureRecipientProList();

        Имущество_для_экспозиции_digest GetExposureAssetDigestsOrderedBy_ID_Object
            (Процедура_над_должником процедура, string after_ID_Object = null, int max_portion_size = 100);
        Client.BaseResponse StoreExposureAssets(string id_MProcedure, Имущество_для_экспозиции имущество);

        void StoreExposureDocument(string id_MProcedure, Документ_для_экспозиции_на_загрузку документ);
        void DeleteExposureDocuments(string id_MProcedure, string ID_Object, List<string> filename_документов);
    }

    public interface IИсточник_имущества_в_ПАУ_для_экспозиции
    {
        IEnumerator<ОбъектКМ_для_экспозиции_digest> Читать_все_объекты_КМ(Процедура_над_должником процедура);
        ОбъектКМ_для_экспозиции Получить_объект_КМ(Процедура_над_должником процедура, string ID_Object);
    }

    public interface IИсточник_имущества_в_ПАУ_для_экспозиции2 : IИсточник_имущества_в_ПАУ_для_экспозиции
    {
        List<ОбъектКМ_для_экспозиции> Получить_объекты_КМ(Процедура_над_должником процедура, IList<string> ID_Objects);
        Документ_для_экспозиции_на_загрузку Подготовить_документ_для_загрузки(Процедура_над_должником процедура, string ID_Object, string filename);
    }

    public static partial class Regions
    {
        static string[][] region_okato = new string[][]
        {
            new string []{ "Республика Адыгея", "79 000" }
            ,new string []{ "Республика Башкортостан", "80 000" }
            ,new string []{ "Республика Бурятия", "81 000" }
            ,new string []{ "Республика Алтай", "84 000" }
            ,new string []{ "Республика Дагестан", "82 000" }
            ,new string []{ "Республика Ингушетия", "26 000" }
            ,new string []{ "Кабардино-Балкарская Республика", "83 000" }
            ,new string []{ "Республика Калмыкия", "85 000" }
            ,new string []{ "Карачаево-Черкесская Республика", "91 000" }
            ,new string []{ "Республика Карелия", "86 000" }
            ,new string []{ "Республика Коми", "87 000" }
            ,new string []{ "Республика Марий Эл", "88 000" }
            ,new string []{ "Республика Мордовия", "89 000" }
            ,new string []{ "Республика Саха (Якутия)", "98 000" }
            ,new string []{ "Республика Северная Осетия — Алания", "90 000" }
            ,new string []{ "Республика Татарстан", "92 000" }
            ,new string []{ "Республика Тыва", "93 000" }
            ,new string []{ "Удмуртская Республика", "94 000" }
            ,new string []{ "Республика Хакасия", "95 000" }
            ,new string []{ "Чеченская Республика", "96 000" }
            ,new string []{ "Чувашская Республика", "97 000" }
            ,new string []{ "Алтайский край", "01 000" }
            ,new string []{ "Краснодарский край", "03 000" }
            ,new string []{ "Красноярский край", "04 000" }
            ,new string []{ "Приморский край", "05 000" }
            ,new string []{ "Ставропольский край", "07 000" }
            ,new string []{ "Хабаровский край", "08 000" }
            ,new string []{ "Амурская область", "10 000" }
            ,new string []{ "Архангельская область", "11 000" }
            ,new string []{ "Астраханская область", "12 000" }
            ,new string []{ "Белгородская область", "14 000" }
            ,new string []{ "Брянская область", "15 000" }
            ,new string []{ "Владимирская область", "17 000" }
            ,new string []{ "Волгоградская область", "18 000" }
            ,new string []{ "Вологодская область", "19 000" }
            ,new string []{ "Воронежская область", "20 000" }
            ,new string []{ "Ивановская область", "24 000" }
            ,new string []{ "Иркутская область", "25 000" }
            ,new string []{ "Калининградская область", "27 000" }
            ,new string []{ "Калужская область", "29 000" }
            ,new string []{ "Камчатский край", "30 000" }
            ,new string []{ "Кемеровская область", "32 000" }
            ,new string []{ "Кировская область", "33 000" }
            ,new string []{ "Костромская область", "34 000" }
            ,new string []{ "Курганская область", "37 000" }
            ,new string []{ "Курская область", "38 000" }
            ,new string []{ "Ленинградская область", "41 000" }
            ,new string []{ "Липецкая область", "42 000" }
            ,new string []{ "Магаданская область", "44 000" }
            ,new string []{ "Московская область", "46 000" }
            ,new string []{ "Мурманская область", "47 000" }
            ,new string []{ "Нижегородская область", "22 000" }
            ,new string []{ "Новгородская область", "49 000" }
            ,new string []{ "Новосибирская область", "50 000" }
            ,new string []{ "Омская область", "52 000" }
            ,new string []{ "Оренбургская область", "53 000" }
            ,new string []{ "Орловская область", "54 000" }
            ,new string []{ "Пензенская область", "56 000" }
            ,new string []{ "Пермский край", "57 000" }
            ,new string []{ "Псковская область", "58 000" }
            ,new string []{ "Ростовская область", "60 000" }
            ,new string []{ "Рязанская область", "61 000" }
            ,new string []{ "Самарская область", "36 000" }
            ,new string []{ "Саратовская область", "63 000" }
            ,new string []{ "Сахалинская область", "64 000" }
            ,new string []{ "Свердловская область", "65 000" }
            ,new string []{ "Смоленская область", "66 000" }
            ,new string []{ "Тамбовская область", "68 000" }
            ,new string []{ "Тверская область", "28 000" }
            ,new string []{ "Томская область", "69 000" }
            ,new string []{ "Тульская область", "70 000" }
            ,new string []{ "Тюменская область", "71 000" }
            ,new string []{ "Ульяновская область", "73 000" }
            ,new string []{ "Челябинская область", "75 000" }
            ,new string []{ "Забайкальский край", "76 000" }
            ,new string []{ "Ярославская область", "78 000" }
            ,new string []{ "Москва", "45 000" }
            ,new string []{ "Санкт-Петербург", "40 000" }
            ,new string []{ "Еврейская автономная область", "99 000" }
            ,new string []{ "Ненецкий автономный округ", "11 100" }
            ,new string []{ "Ханты-Мансийский автономный округ - Югра Автономный округ", "71 100" }
            ,new string []{ "Чукотский автономный округ", "77 000" }
            ,new string []{ "Ямало-Ненецкий автономный округ", "71 140" }
            ,new string []{ "Республика Крым", "35 000" }
            ,new string []{ "Севастополь", "67 000" }
            ,new string []{ "Байконур", "55 000" }
        };
    }
}
