﻿namespace ama.datamart
{
    partial class UCExposure
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxState = new System.Windows.Forms.ComboBox();
            this.labelState = new System.Windows.Forms.Label();
            this.comboBoxCategory = new System.Windows.Forms.ComboBox();
            this.labelCategory = new System.Windows.Forms.Label();
            this.labelExposureRecipients = new System.Windows.Forms.Label();
            this.labelEfsbClasses = new System.Windows.Forms.Label();
            this.ucEfrsbClasses = new ama.datamart.UCEfrsbClasses();
            this.ucExposureRecipients = new ama.datamart.UCExposureRecipients();
            this.SuspendLayout();
            // 
            // comboBoxState
            // 
            this.comboBoxState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxState.FormattingEnabled = true;
            this.comboBoxState.Location = new System.Drawing.Point(172, 44);
            this.comboBoxState.Name = "comboBoxState";
            this.comboBoxState.Size = new System.Drawing.Size(292, 21);
            this.comboBoxState.TabIndex = 15;
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Location = new System.Drawing.Point(3, 47);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(163, 13);
            this.labelState.TabIndex = 14;
            this.labelState.Text = "Стадия работы с имуществом:";
            // 
            // comboBoxCategory
            // 
            this.comboBoxCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCategory.FormattingEnabled = true;
            this.comboBoxCategory.Location = new System.Drawing.Point(172, 7);
            this.comboBoxCategory.Name = "comboBoxCategory";
            this.comboBoxCategory.Size = new System.Drawing.Size(292, 21);
            this.comboBoxCategory.TabIndex = 13;
            // 
            // labelCategory
            // 
            this.labelCategory.AutoSize = true;
            this.labelCategory.Location = new System.Drawing.Point(3, 10);
            this.labelCategory.Name = "labelCategory";
            this.labelCategory.Size = new System.Drawing.Size(123, 13);
            this.labelCategory.TabIndex = 12;
            this.labelCategory.Text = "Категория имущества:";
            // 
            // labelExposureRecipients
            // 
            this.labelExposureRecipients.AutoSize = true;
            this.labelExposureRecipients.Location = new System.Drawing.Point(3, 83);
            this.labelExposureRecipients.Name = "labelExposureRecipients";
            this.labelExposureRecipients.Size = new System.Drawing.Size(281, 13);
            this.labelExposureRecipients.TabIndex = 11;
            this.labelExposureRecipients.Text = "Круг лиц, которым доступна информация об объекте:";
            // 
            // labelEfsbClasses
            // 
            this.labelEfsbClasses.AutoSize = true;
            this.labelEfsbClasses.Location = new System.Drawing.Point(3, 159);
            this.labelEfsbClasses.Name = "labelEfsbClasses";
            this.labelEfsbClasses.Size = new System.Drawing.Size(283, 13);
            this.labelEfsbClasses.TabIndex = 9;
            this.labelEfsbClasses.Text = "Классификация имущества в соответствии с ЕФРСБ:";
            // 
            // ucEfrsbClasses
            // 
            this.ucEfrsbClasses.AutoScroll = true;
            this.ucEfrsbClasses.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucEfrsbClasses.Location = new System.Drawing.Point(15, 175);
            this.ucEfrsbClasses.Name = "ucEfrsbClasses";
            this.ucEfrsbClasses.Size = new System.Drawing.Size(449, 112);
            this.ucEfrsbClasses.TabIndex = 10;
            // 
            // ucExposureRecipients
            // 
            this.ucExposureRecipients.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucExposureRecipients.Location = new System.Drawing.Point(15, 99);
            this.ucExposureRecipients.Name = "ucExposureRecipients";
            this.ucExposureRecipients.Size = new System.Drawing.Size(449, 49);
            this.ucExposureRecipients.TabIndex = 8;
            // 
            // UCExposure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBoxState);
            this.Controls.Add(this.labelState);
            this.Controls.Add(this.comboBoxCategory);
            this.Controls.Add(this.labelCategory);
            this.Controls.Add(this.labelExposureRecipients);
            this.Controls.Add(this.ucEfrsbClasses);
            this.Controls.Add(this.labelEfsbClasses);
            this.Controls.Add(this.ucExposureRecipients);
            this.MinimumSize = new System.Drawing.Size(400, 250);
            this.Name = "UCExposure";
            this.Size = new System.Drawing.Size(469, 300);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxState;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.ComboBox comboBoxCategory;
        private System.Windows.Forms.Label labelCategory;
        private System.Windows.Forms.Label labelExposureRecipients;
        private UCEfrsbClasses ucEfrsbClasses;
        private System.Windows.Forms.Label labelEfsbClasses;
        public UCExposureRecipients ucExposureRecipients;
    }
}
