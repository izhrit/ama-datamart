﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Web.Script.Serialization;
using System.Diagnostics;

namespace ama.datamart
{
    public partial class FormExposure : Form, IExposureRecipients_editor
    {
        public Exposure_settings Settings = new Exposure_settings();

        public FormExposure()
        {
            InitializeComponent();
            webBrowser.Anchor |= (AnchorStyles.Right| AnchorStyles.Bottom);
        }

        private void FormExposure_Load(object sender, EventArgs e)
        {
            StaticHtmlControl.Start(webBrowser, "property_recipients", new FormExposure_ObjectForScripting(this));
        }

        internal ExposureRecipients m_Recipients= new ExposureRecipients();
        public void DataLoad(ExposureRecipients er)
        {
            ExposureRecipients.CopyFromTo(er, m_Recipients);
        }

        public void DataSave(ExposureRecipients er)
        {
            ExposureRecipients.CopyFromTo(m_Recipients, er);
        }

        private void FormExposure_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!e.Cancel)
            {
                object res = webBrowser.Document.InvokeScript("GetExposureRecipientsToSave");
                if (null != res)
                {
                    string txt_res = res.ToString();
                    JavaScriptSerializer s = new JavaScriptSerializer();
                    m_Recipients = s.Deserialize<ExposureRecipients>(txt_res);
                }
            }
        }
    }

    [ComVisible(true)]
    public class FormExposure_ObjectForScripting
    {
        FormExposure m_form;
        internal FormExposure_ObjectForScripting(FormExposure frm)
        {
            m_form = frm;
        }

        public string GetSettings()
        {
            Exposure_settings settings= m_form.Settings;
            settings.FixBackgroundColor(m_form);
            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Serialize(settings);
        }

        public string GetExposureRecipientsToLoad()
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Serialize(m_form.m_Recipients);
        }
    }
}
