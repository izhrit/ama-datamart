﻿using System.Windows.Forms;

namespace ama.datamart
{
    public partial class UCExposure : UserControl, IВкладка_экспозиция_объекта_КМ
    {
        public UCExposure()
        {
            InitializeComponent();

            //ucExposureRecipients.BorderStyle = BorderStyle.None;
            ucExposureRecipients.Anchor |= AnchorStyles.Right;

            ucEfrsbClasses.Anchor |= (AnchorStyles.Right | AnchorStyles.Bottom);

            comboBoxCategory.Anchor|= AnchorStyles.Right;
            comboBoxState.Anchor |= AnchorStyles.Right;

            Категория_ОбъектаКМ_для_экспозиции.Fill(comboBoxCategory);
            Стадия_экспозиции_ОбъектаКМ.Fill(comboBoxState);
        }

        public void DataLoad(Данные_вкладки_экспозиция_объекта_КМ e)
        {
            comboBoxCategory.SelectedItem = e.Категория;
            comboBoxState.SelectedItem = e.Стадия;
            ucEfrsbClasses.DataLoad(e.КлассификацияЕФРСБ);
            ucExposureRecipients.DataLoad(e.Круг_лиц_с_доступом);
        }

        public void DataSave(Данные_вкладки_экспозиция_объекта_КМ e)
        {
            e.Категория= null== comboBoxCategory.SelectedItem ? null : comboBoxCategory.SelectedItem.ToString();
            e.Стадия= null == comboBoxState.SelectedItem ? null : comboBoxState.SelectedItem.ToString();
            ucEfrsbClasses.DataSave(e.КлассификацияЕФРСБ);
            ucExposureRecipients.DataSave(e.Круг_лиц_с_доступом);
        }
    }
}
