﻿namespace ama.datamart
{
    partial class UCExposureRecipients
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkLabelRecipients = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // linkLabelRecipients
            // 
            this.linkLabelRecipients.AutoEllipsis = true;
            this.linkLabelRecipients.AutoSize = true;
            this.linkLabelRecipients.Location = new System.Drawing.Point(0, 0);
            this.linkLabelRecipients.MaximumSize = new System.Drawing.Size(150, 0);
            this.linkLabelRecipients.Name = "linkLabelRecipients";
            this.linkLabelRecipients.Size = new System.Drawing.Size(137, 26);
            this.linkLabelRecipients.TabIndex = 0;
            this.linkLabelRecipients.TabStop = true;
            this.linkLabelRecipients.Text = "Кликните, чтобы указать получателя информации!";
            this.linkLabelRecipients.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelRecipients_LinkClicked);
            // 
            // UCExposureRecipients
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.linkLabelRecipients);
            this.Name = "UCExposureRecipients";
            this.Resize += new System.EventHandler(this.UCExposure_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkLabelRecipients;
    }
}
