﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Web.Script.Serialization;

namespace ama.datamart
{
    public partial class FormChooseEfrsbMessageType : Form
    {
        public FormChooseEfrsbMessageType()
        {
            InitializeComponent();
        }

        private void FormChooseEfrsbMessageType_Load(object sender, EventArgs e)
        {
            StaticHtmlControl.Start(webBrowser, "choose_efrsb_msg_type", new FormChooseEfrsbMessageType_ObjectForScripting(this));
        }

        public Choose_Efrsb_message_type_settings Settings= new Choose_Efrsb_message_type_settings();
        public Efrsb_message_type Choosed_Efrsb_message_type = null;
    }

    [ComVisible(true)]
    public class FormChooseEfrsbMessageType_ObjectForScripting
    {
        FormChooseEfrsbMessageType m_form;
        internal FormChooseEfrsbMessageType_ObjectForScripting(FormChooseEfrsbMessageType frm)
        {
            m_form = frm;
        }

        public void OnEfrsbMessageTypeChoosed(string json_msg_type,string json_Choose_Efrsb_message_type_settings)
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            m_form.Choosed_Efrsb_message_type = s.Deserialize<Efrsb_message_type>(json_msg_type);
            m_form.Settings= s.Deserialize<Choose_Efrsb_message_type_settings>(json_Choose_Efrsb_message_type_settings);
            m_form.DialogResult = DialogResult.OK;
            m_form.Close();
        }

        public string GetSettings()
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            m_form.Settings.FixBackgroundColor(m_form);
            return s.Serialize(m_form.Settings);
        }
    }
}
