﻿using System.Collections.Generic;

namespace ama.datamart
{
    public class Efrsb_message_type
    {
        public string db_value;
        public string Readable;
        public string Readable_short_about;
    }

    public class Choose_Efrsb_message_type_settings : StaticHtmlSettings
    {
        public bool TreeMode = false;
        public bool ShowOnlyActual = false;
        public string CurrentProcedureName = "РД";
        public string[] implemented;
        public string[] actual;
    }
}
