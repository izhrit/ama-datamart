﻿using System;
using System.Text;
using System.Web.Script.Serialization;

using pepro.datamart;

namespace test_ama_dm_client
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            ProcessArgs(args);
            switch (action)
            {
                case "using": TestUsing(); break;
            }
        }

        static void TestUsing()
        {
            Client c = new Client(dm_base_url);

            try
            {
                c.Using(
                    ContractNumber: contract
                    , license_token: license_token
                    , section: section
                );
            }
            catch
            {
                Console.Out.Write("got error!");
            }
        }

        static string dm_base_url = "http://local.test/dm/api.php";

        static string action;

        static string license_token;
        static string contract;

        static string section;

        static void ProcessArgs(string[] args)
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            for (int i= 0; i<args.Length; i++)
            {
                string [] ap = args[i].Split('=');
                switch (ap[0])
                {
                    case "--action": action = ap[1]; break;

                    case "--base-url": dm_base_url = ap[1]; break;
                    case "--section": section = ap[1]; break;

                    case "--license_token": license_token= ap[1]; break;
                    case "--contract": contract= ap[1]; break;
                }
            }
        }
    }
}
