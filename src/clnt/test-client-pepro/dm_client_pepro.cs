﻿/*
 * ВНИМАНИЕ! ATTENTION! AHTUNG!
 * Данный файл редактируется и тестируется в проекте "datamart"!
 * Он НЕ должен редактироваться в проекте "PePro"!

Основной класс обеспечивающий связь с витриной данных: pepro.datamart.Client

у pepro.datamart.Client есть также поля с префиксом proxy_ (host,port,login,password)
  в которые необходимо прописать настройки связи с интернетом

у pepro.datamart.Client есть поле base_url
  значение которого рекомендуется устанавливать из конфиг-файла
  значение "по умолчанию": http://local.test/dm/api.php
  "боевое" значение: https://datamart.rsit.ru/api.php
  значение для тестовой витрины (рекомендуется использовать 
    при тестировании и отладке): https://datamart-test.rsit.ru/api.php

 */
using System.Net;
using System.IO;
using System.Text;

namespace pepro.datamart
{
    public interface IClient
    {
        void Using(string ContractNumber, string license_token, string section);
    }

    public class Client : IClient
    {
        public string base_url = "http://local.test/dm/api.php";

        public string proxy_host;
        public int proxy_port;
        public string proxy_login;
        public string proxy_password;

        public bool test_mode = false;

        public Client(string base_url= null)
        {
            if (!string.IsNullOrEmpty(base_url))
                this.base_url = base_url;
        }

        public void Using(string ContractNumber, string license_token, string section)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(base_url).Append("/UsingPepro?license=").Append(license_token);
            if (!string.IsNullOrEmpty(ContractNumber))
                sb.Append("&contract=").Append(ContractNumber);
            if (!string.IsNullOrEmpty(section))
                sb.Append("&section=").Append(section);
            string url = sb.ToString();

            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);

            request.Method = "GET";
            HttpStatusCode status;
            string response_body = ReadResponse(request, out status);

            if (HttpStatusCode.OK != status)
                throw new Exception("Unsuccessefull sending UsingProcedure!", status, response_body);
        }

        public class Exception : System.Exception
        {
            public HttpStatusCode status;
            public string response_body;
            public Exception(string text, HttpStatusCode s, string b)
                : base(text)
            {
                status = s;
                response_body = b;
            }
            public Exception(string text, HttpStatusCode s, string b, System.Exception e)
                : base(text,e)
            {
                status = s;
                response_body = b;
            }
        }

        void SafeFixProxy(HttpWebRequest request)
        {
            if (!string.IsNullOrEmpty(proxy_host))
            {
                WebProxy proxy= new WebProxy(proxy_host, proxy_port);
                if (!string.IsNullOrEmpty(proxy_login))
                    proxy.Credentials = new NetworkCredential(proxy_login, proxy_password);
                request.Proxy = proxy;
            }
        }

        static string ReadResponse(HttpWebRequest request, out HttpStatusCode status)
        {
            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
            using (Stream receiveStream = response.GetResponseStream())
            using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
            {
                status = response.StatusCode;
                return ReadResponseText(status, readStream);
            }
        }

        static string ReadErrorResponseText(StreamReader readStream)
        {
            StringBuilder sb = new StringBuilder();
            for (int count = 0; count < 100; count++) // в случае ошибки читаем первый 100 строк текста об ошибке
            {
                string line = readStream.ReadLine();
                if (null == line)
                {
                    break;
                }
                else
                {
                    sb.AppendLine(line);
                }
            }
            return sb.ToString();
        }

        static string ReadResponseText(HttpStatusCode status, StreamReader readStream)
        {
            return (HttpStatusCode.OK == status)
                    ? readStream.ReadToEnd()
                    : ReadErrorResponseText(readStream);
        }
    }
}