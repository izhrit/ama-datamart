﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AwardVoteUtils;

namespace AwardVoteUtils.Tests
{
    [TestClass]
    public class SignTest
    {
        [TestMethod]
        public void VerifyCAPICOM_ExpectedTrue()
        {
            string pathToTxtFile = "Data/23.txt";
            string pathToSigFile = "Data/23.sig";

            VerifyAnswer expected = new VerifyAnswer();
            expected.VerifyResult = true;
            expected.Thumbprint = "0E4589143DBB560FE8822E5EE01A3C634114EB91";
            expected.SubjectName = "Kotov Nikita Igorevich";


            VerifyAnswer actual = Sign.VerifyCAPICOM(pathToTxtFile, pathToSigFile);

            Assert.IsTrue(actual.VerifyResult);
            Assert.AreEqual(expected.SubjectName, actual.SubjectName);
            Assert.AreEqual(expected.Thumbprint, actual.Thumbprint);                       
        }

        [TestMethod]
        public void VerifyCAPICOM_ExpectedFalse()
        {
            string pathToTxtFile = "Data/23_fail.txt";
            string pathToSigFile = "Data/23.sig";

            VerifyAnswer expected = new VerifyAnswer();            
            expected.VerifyResult = false;
            expected.Thumbprint = "0E4589143DBB560FE8822E5EE01A3C634114EB91";
            expected.SubjectName = "Kotov Nikita Igorevich";


            VerifyAnswer actual = Sign.VerifyCAPICOM(pathToTxtFile, pathToSigFile);

            Assert.IsFalse(actual.VerifyResult);
            Assert.AreEqual(expected.SubjectName, actual.SubjectName);
            Assert.AreEqual(expected.Thumbprint, actual.Thumbprint);            
        }
    }
}
