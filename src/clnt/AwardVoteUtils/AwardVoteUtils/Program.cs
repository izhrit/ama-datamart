﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Data.Common;
using System.IO;
using System.Configuration;
using System.Net;
using System.IO.Compression;

namespace AwardVoteUtils
{
    class Program
    {
        static void Main(string[] args)
        {
            //GetSignedVoices();

            Help();
            while (true)
            {
                string menuSelect = Console.ReadLine().ToLower();
                switch (menuSelect)
                {
                    case "help": Help(); break;
                    case "voting": GetResultVoting(); break;
                    case "voting -a": GetAllVoting(); break;
                    case "voices -s": GetSignedVoices(); break;
                    case "exit": Environment.Exit(0); break;
                    default: Console.WriteLine("undefinde command"); break;
                }
            }
        }

        static void Help()
        {
            Console.WriteLine("");
            Console.WriteLine("voting       Сформировать результаты голосования");
            Console.WriteLine("voting -a    Сформировать список всех голосов");
            Console.WriteLine("voices -s    Проверить голоса, подписанные сертификатами");
            Console.WriteLine("exit         Выход");
            Console.WriteLine("");
        }

        static void GetResultVoting()
        {
            using (WebClient client = new WebClient())
            {
                string pathForSave = Properties.Settings.Default.PathForSaveCsvFiles + "VotesForNominants.csv";

                client.DownloadFile(Properties.Settings.Default.DatamartAwardApiUrl + "&operations=VotesForNominants",
                    pathForSave);

                Console.WriteLine("\r\nРезультаты голосования сохранены в " + pathForSave + " \r\n");
            }
        }

        static void GetAllVoting()
        {
            using (WebClient client = new WebClient())
            {
                string pathForSave = Properties.Settings.Default.PathForSaveCsvFiles + "AllVotes.csv";

                client.DownloadFile(Properties.Settings.Default.DatamartAwardApiUrl + "&operations=AllVotes",
                    pathForSave);

                Console.WriteLine("\r\nРезультаты голосования сохранены в " + pathForSave + " \r\n");
            }
        }

        static void GetSignedVoices()
        {
            using (WebClient client = new WebClient())
            {
                string pathForSave = Properties.Settings.Default.PathForSaveCsvFiles + "SignedVoices.zip";

                client.DownloadFile(Properties.Settings.Default.DatamartAwardApiUrl + "&operations=GetSignedVoices",
                    pathForSave);

                Console.WriteLine("\r\nРезультаты голосования сохранены в " + pathForSave + " \r\n");

                string pathToFolderWithUnzipVoices = UnzipSigndeVoices(pathForSave);

                VerifySignVoices(pathToFolderWithUnzipVoices);
            }
        }

        private static string UnzipSigndeVoices(string pathToZip)
        {
            string pathForExtractZip = Path.Combine(Properties.Settings.Default.PathForSaveCsvFiles, "SignedVoices");
            if (Directory.Exists(pathForExtractZip))
            {
                Directory.Delete(pathForExtractZip, true);
                Directory.CreateDirectory(pathForExtractZip);
            }
            else
            {
                Directory.CreateDirectory(pathForExtractZip);
            }

            using (ZipArchive archive = ZipFile.OpenRead(pathToZip))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    entry.ExtractToFile(Path.Combine(pathForExtractZip, entry.FullName));
                }

                Console.WriteLine("\r\nРезультаты голосования распакованы в " + pathToZip + " \r\n");
            }

            return pathForExtractZip;
        }

        private static void VerifySignVoices(string pathToFolderWithUnzipVoices)
        {
            Console.WriteLine("\r\nНачинаем валидацию голосов, подписанных сертификатом \r\n");

            DirectoryInfo folderWithUnzipVoices = new DirectoryInfo(pathToFolderWithUnzipVoices);
            FileInfo[] files = folderWithUnzipVoices.GetFiles();

            int minId = files.Min(x => int.Parse(Path.GetFileNameWithoutExtension(x.FullName)));            
            int maxId = files.Max(x => int.Parse(Path.GetFileNameWithoutExtension(x.FullName)));

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Номер голоса;Результат проверки;Отпечаток;Имя субъекта;Результат проверки сертификата с ефрсб");

            for (int i = minId; i <= maxId; i++)
            {
                string txtFile = Path.Combine(pathToFolderWithUnzipVoices, string.Format("{0}.{1}", i.ToString(), "txt"));
                string sigFile = Path.Combine(pathToFolderWithUnzipVoices, string.Format("{0}.{1}", i.ToString(), "sig"));

                if (File.Exists(txtFile) && File.Exists(sigFile))
                {
                    Console.WriteLine("Проверяем голос " + i.ToString());

                    VerifyAnswer verifySign = Sign.VerifyCAPICOM(txtFile, sigFile);

                    sb.AppendLine(string.Format("{0};{1};{2};{3};{4}",
                        i,
                        verifySign.VerifyResult,
                        verifySign.Thumbprint,
                        verifySign.SubjectName,
                        verifySign.VerifyThumbprint));
                }
            }

            string pathForSave = Path.Combine(Properties.Settings.Default.PathForSaveCsvFiles, "SignedVoices.csv");
            File.WriteAllText(pathForSave, sb.ToString());
            Console.WriteLine("\r\nРезультаты проверки голосов сохранены в " + pathForSave + " \r\n");
        }
    }
}
