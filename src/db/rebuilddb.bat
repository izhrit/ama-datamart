@rem 
@rem данный скрипт необходимо запускать после изменения схемы данных
@rem в файле model\*.DM1
@rem скрипт пересоздаст базу данных
@rem и подготовит на будущее необходимые резервные копии
@rem 

@echo *******************************************************

@for /f "eol=# delims== tokens=1,2" %%i in (%~dp0..\etc\settings.txt) do @set %%i=%%j
@if exist %~dp0..\etc\settings.local.txt @for /f "eol=# delims== tokens=1,2" %%i in (%~dp0\..\etc\settings.local.txt) do @set %%i=%%j

@echo prepare create_sql
@call %~dp0\scripts\create_sql.bat

@echo prepare empty database %DBName%
@set SQL_DROP_CREATE=drop database %DBName%; create database %DBName% default character set utf8;
@echo %SQL_DROP_CREATE% | %~dp0\run_mysql.bat
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute create.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\create\create.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute post_create.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\create\post_create.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute fill_log_type.etalon.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\create\fill_log_type.etalon.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute procedures.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\create\procedures.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute migrations.sql
@call %~dp0\run_mysql.bat < %~dp0\migrations\z_for_create_new.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute datamart.etalon.sql 
@call %~dp0\run_mysql.bat < %~dp0\..\uc\forms\ama\datamart\tests\datamart.etalon.sql 
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo .
@echo fill PData
@call %PHP% %~dp0\scripts\fill_pdata.php

@echo .
@echo fill MUser
@call %PHP% %~dp0\scripts\fill_muserdata.php

@echo .
@echo fill PDDocFile
@call %PHP% %~dp0\scripts\fill_pdddocfiledata.php

call :prep_backup %~dp0\backups\test_dump.sql

exit

:prep_backup
call "%MYSQL_DIR_BIN%mysqldump.exe" --defaults-extra-file=%~dp0\..\etc\built-local\mysql.conf --hex-blob --host=%MYSQL_HOST% --default-character-set=utf8 --set-charset --routines --no-autocommit --extended-insert --result-file=%1 %DBName%
exit /B