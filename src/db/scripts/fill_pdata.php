<?

require_once __DIR__.'/../../srv/assets/config.php';
require_once __DIR__.'/../../srv/assets/helpers/db.php';
require_once __DIR__.'/../../srv/assets/helpers/log.php';

$zip_file_contents= array();

function prepare_zip($xml_file_path, $entry_name)
{
	$xml_file_name= basename($xml_file_path);
	$files_dir= mb_substr($xml_file_path,0,mb_strlen($xml_file_path)-mb_strlen($xml_file_name));

	$zip_file_path= $files_dir.$xml_file_name.'.zip';
	echo 'read '.$xml_file_path."\r\n";
		
	$zip = new ZipArchive;
	if ($zip->open($zip_file_path, ZipArchive::CREATE) !== TRUE)
	{
		echo "failed\r\n";
		return null;
	}
	else
	{
		$zip->addFile($xml_file_path, $entry_name);
		$zip->close();
		$file_size= filesize($zip_file_path);
		$md5= md5_file($zip_file_path);
		echo "ok size=$file_size, md5=$md5\r\n";
		echo "$zip_file_path\r\n";
		$res= file_get_contents($zip_file_path);
		unlink($zip_file_path);
		return $res;
	}
}

$report_file_path= __DIR__.'\\..\\..\\uc\\forms\\ama\\datamart\\procedure\\section\\report\\tests\\report.xml';
$files_dir= __DIR__.'\\..\\..\\uc\\forms\\ama\\datamart\\procedure\\section\\registry\\tests\\';
for ($i= 0; $i<5; $i++)
{
	$xml_file_name= 'rtk-example'.$i;
	$xml_file_path= $files_dir.$xml_file_name.'.xml';
	if (file_exists($xml_file_path))
	{
		$zip_file_path= $files_dir.$xml_file_name.'.zip';
		echo 'read '.$xml_file_path."\r\n";
		
		$zip = new ZipArchive;
		if ($zip->open($zip_file_path, ZipArchive::CREATE) !== TRUE)
		{
			echo "failed\r\n";
		}
		else
		{
			$zip->addFile($xml_file_path, 'registry.xml');
			if (1==($i%2))
				$zip->addFile($report_file_path, 'report.xml');
			$zip->close();
			$zip_file_contents[]= file_get_contents($zip_file_path);
			$file_size= filesize($zip_file_path);
			$md5= md5_file($zip_file_path);
			echo "ok size=$file_size, md5=$md5\r\n";
		}
	}
}
$file_contents_count= count($zip_file_contents);

$txt_query= "select id_MProcedure from MProcedure limit 100;";
$procedures= execute_query($txt_query,array());

$revision= 1;
foreach ($procedures as $procedure)
{
	echo 'procedure '.$procedure->id_MProcedure."\r\n";
	$txt_query= "insert into PData set fileData=?, id_MProcedure= ?, publicDate='2018-05-28', revision=?, ctb_allowed=?;";
	$cpw_allowed= ((0==$revision%2)?1:0);
	execute_query_no_result($txt_query,array('biii'
		,$zip_file_contents[$revision%$file_contents_count]
		,$procedure->id_MProcedure
		,$revision
		,$cpw_allowed));
	$txt_query= "update MProcedure set publicDate='2018-05-28', revision=? where id_MProcedure= ?;";
	execute_query_no_result($txt_query,array('ii',$revision,$procedure->id_MProcedure));
	if (1==$cpw_allowed)
	{
		$txt_query= "update MProcedure set ctb_publicDate=publicDate, ctb_revision= revision where id_MProcedure= ?;";
		execute_query_no_result($txt_query,array('i',$procedure->id_MProcedure));
	}
	$revision++;
}

$txt_query= "select id_MData, MData_Type from MData;";
$incomes= execute_query($txt_query,array());

foreach ($incomes as $income)
{
	$content= null;
	switch ($income->MData_Type)
	{
		case 'd':
			$content= prepare_zip(__DIR__.'/../../srv/web/docs/deals-example.xml', 'deals.xml');
			break;
		case 'a':
			$content= prepare_zip(__DIR__.'/../../srv/web/docs/anketanp-example.xml', 'anketanp.xml');
			break;
	}
	if (null!=$content)
	{
		$txt_query= "update MData set fileData=? where id_MData= ?;";
		execute_query_no_result($txt_query,array('bi',$content,$income->id_MData));
	}
}