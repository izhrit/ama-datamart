--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
--



--
--

DROP TABLE IF EXISTS `access_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_log` (
  `Details` text,
  `IP` varchar(40) DEFAULT NULL,
  `Id` int(11) DEFAULT NULL,
  `Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_Log_type` int(11) NOT NULL,
  `id_Log` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Log`),
  KEY `access_log_type` (`id_Log_type`),
  KEY `byIP` (`IP`),
  KEY `byId` (`Id`),
  KEY `byTime` (`Time`)
  CONSTRAINT `access_log_type` FOREIGN KEY (`id_Log_type`) REFERENCES `log_type` (`id_Log_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `anketanp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anketanp` (
  `Email` varchar(50) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `SNILS` varchar(13) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `id_AnketaNP` int(11) NOT NULL AUTO_INCREMENT,
  `id_MData` int(11) NOT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_AnketaNP`),
  KEY `byINN` (`INN`),
  KEY `byLastName` (`lastName`),
  KEY `bySNILS` (`SNILS`),
  KEY `refAnketaNP_MData` (`id_MData`)
  CONSTRAINT `refAnketaNP_MData` FOREIGN KEY (`id_MData`) REFERENCES `mdata` (`id_MData`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `application_data` longblob,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_Application` int(11) NOT NULL AUTO_INCREMENT,
  `id_MUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Application`),
  KEY `refApplication_MUser` (`id_MUser`)
  CONSTRAINT `refApplication_MUser` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `applicationfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationfile` (
  `data` longblob,
  `extension` varbinary(16) DEFAULT NULL,
  `id_ApplicationFile` int(11) NOT NULL AUTO_INCREMENT,
  `id_Application` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_ApplicationFile`),
  KEY `refApplicationFile_Application` (`id_Application`)
  CONSTRAINT `refApplicationFile_Application` FOREIGN KEY (`id_Application`) REFERENCES `application` (`id_Application`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `applicationrtk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationrtk` (
  `creditor_inn` varchar(12) DEFAULT NULL,
  `id_ApplicationRTK` int(11) NOT NULL AUTO_INCREMENT,
  `id_MData` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ApplicationRTK`),
  KEY `Ref3999` (`id_MData`)
  CONSTRAINT `RefMData99` FOREIGN KEY (`id_MData`) REFERENCES `mdata` (`id_MData`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachment` (
  `Content` longblob,
  `FileName` varchar(50) DEFAULT NULL,
  `id_Attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_SentEmail` int(11) NOT NULL,
  PRIMARY KEY (`id_Attachment`),
  KEY `refAttachment_EmailToSend` (`id_SentEmail`)
  CONSTRAINT `refAttachment_EmailToSend` FOREIGN KEY (`id_SentEmail`) REFERENCES `emailtosend` (`id_SentEmail`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `awardnominee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awardnominee` (
  `SRO` text,
  `URL` text,
  `firstName` varchar(50) DEFAULT NULL,
  `id_AwardNominee` int(11) NOT NULL AUTO_INCREMENT,
  `inn` varchar(13) NOT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_AwardNominee`),
  UNIQUE KEY `byINN` (`inn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `awardvote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awardvote` (
  `BulletinSignature` longblob,
  `BulletinText` longblob,
  `SRO` text,
  `VoteTime` datetime NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `id_AwardNominee` int(11) NOT NULL,
  `id_AwardVote` int(11) NOT NULL AUTO_INCREMENT,
  `inn` varchar(13) NOT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_AwardVote`),
  KEY `refAwardVote_AwardNominee` (`id_AwardNominee`),
  UNIQUE KEY `byINN` (`inn`)
  CONSTRAINT `refAwardVote_AwardNominee` FOREIGN KEY (`id_AwardNominee`) REFERENCES `awardnominee` (`id_AwardNominee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `ccdocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ccdocument` (
  `DocumentDate` datetime NOT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `id_CCDocument` int(11) NOT NULL AUTO_INCREMENT,
  `id_CourtCase` int(11) NOT NULL,
  PRIMARY KEY (`id_CCDocument`),
  KEY `CCDocument_CourtCase` (`id_CourtCase`)
  CONSTRAINT `CCDocument_CourtCase` FOREIGN KEY (`id_CourtCase`) REFERENCES `courtcase` (`id_CourtCase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `ccevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ccevent` (
  `EventTime` datetime NOT NULL,
  `EventType` char(1) NOT NULL,
  `id_CCDocument` int(11) NOT NULL,
  `id_CCEvent` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_CCEvent`),
  KEY `RefCCEvent_Document` (`id_CCDocument`)
  CONSTRAINT `RefCCEvent_Document` FOREIGN KEY (`id_CCDocument`) REFERENCES `ccdocument` (`id_CCDocument`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `ccpushedevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ccpushedevent` (
  `Time_dispatched` datetime NOT NULL,
  `Time_pushed` char(10) DEFAULT NULL,
  `id_CCEvent` int(11) NOT NULL,
  PRIMARY KEY (`id_CCEvent`),
  KEY `RefCCEvent_PushedEvent` (`id_CCEvent`)
  CONSTRAINT `RefCCEvent_PushedEvent` FOREIGN KEY (`id_CCEvent`) REFERENCES `ccevent` (`id_CCEvent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract` (
  `ContractNumber` varchar(40) DEFAULT NULL,
  `id_Contract` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Contract`),
  UNIQUE KEY `byContractNumber` (`ContractNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `contractuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractuser` (
  `UserName` varchar(70) DEFAULT NULL,
  `id_ContractUser` int(11) NOT NULL AUTO_INCREMENT,
  `id_Contract` int(11) NOT NULL,
  `id_MUser` int(11) NOT NULL,
  PRIMARY KEY (`id_ContractUser`),
  KEY `Ref_ContractUser_Contract` (`id_Contract`),
  KEY `Ref_ContractUser_User` (`id_MUser`)
  CONSTRAINT `Ref_ContractUser_Contract` FOREIGN KEY (`id_Contract`) REFERENCES `contract` (`id_Contract`),
  CONSTRAINT `Ref_ContractUser_User` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `court`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `court` (
  `Address` text,
  `CasebookTag` varchar(50) DEFAULT NULL,
  `Name` varchar(70) NOT NULL,
  `NumberPrefix` varchar(5) DEFAULT NULL,
  `SearchName` varchar(30) DEFAULT NULL,
  `ShortName` varchar(40) DEFAULT NULL,
  `id_Court` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Court`),
  KEY `byNumberPrefix` (`NumberPrefix`),
  UNIQUE KEY `byCasebookTag` (`CasebookTag`),
  UNIQUE KEY `byName` (`Name`),
  UNIQUE KEY `bySearchName` (`SearchName`),
  UNIQUE KEY `byShortName` (`ShortName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `courtcase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courtcase` (
  `id_CourtCase` int(11) NOT NULL AUTO_INCREMENT,
  `id_Debtor` int(11) NOT NULL,
  PRIMARY KEY (`id_CourtCase`),
  KEY `RefCourtCase_Debtor` (`id_Debtor`)
  CONSTRAINT `RefCourtCase_Debtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `courtcaseparticipant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courtcaseparticipant` (
  `id_CourtCase` int(11) NOT NULL,
  `id_MUser` int(11) NOT NULL,
  PRIMARY KEY (`id_CourtCase`,`id_MUser`),
  KEY `RefCourtCase_Participant` (`id_CourtCase`),
  KEY `RefMUser_CourtCaseParticipant` (`id_MUser`)
  CONSTRAINT `RefCourtCase_Participant` FOREIGN KEY (`id_CourtCase`) REFERENCES `courtcase` (`id_CourtCase`),
  CONSTRAINT `RefMUser_CourtCaseParticipant` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor` (
  `BankruptId` bigint(20) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `OGRN` varbinary(16) DEFAULT NULL,
  `SNILS` varchar(13) DEFAULT NULL,
  `TimeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TimeVerified` datetime DEFAULT NULL,
  `VerificationState` char(1) DEFAULT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Debtor`),
  KEY `byINN` (`INN`),
  KEY `byOGRN` (`OGRN`),
  KEY `bySNILS` (`SNILS`),
  KEY `for_verification` (`VerificationState`,`TimeVerified`),
  UNIQUE KEY `byBankruptId` (`BankruptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `doctype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctype` (
  `ExtraParams` longblob NOT NULL,
  `NameAddPart` varchar(250) DEFAULT NULL,
  `Name` varchar(250) NOT NULL,
  `id_DocType` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_DocType`),
  UNIQUE KEY `byName` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `efrsb_debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb_debtor` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `LegalAdress` varchar(400) DEFAULT NULL,
  `LegalCaseList` longblob,
  `Name` varchar(152) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `Revision` bigint(20) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Debtor`),
  KEY `Ref_Debtor_Manager` (`ArbitrManagerID`),
  KEY `byINN_2` (`INN`),
  KEY `byName_1` (`Name`),
  KEY `byOGRN_1` (`OGRN`),
  KEY `byRevision_1` (`Revision`),
  KEY `bySNILS_1` (`SNILS`),
  UNIQUE KEY `byBankruptId_1` (`BankruptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `efrsb_debtor_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb_debtor_manager` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `DateTime_MessageFirst` datetime DEFAULT NULL,
  `DateTime_MessageLast` datetime DEFAULT NULL,
  `Revision` bigint(20) DEFAULT NULL,
  `id_Debtor_Manager` int(11) NOT NULL,
  PRIMARY KEY (`id_Debtor_Manager`),
  KEY `Ref_Debtor_manager_Debtor` (`BankruptId`),
  KEY `Ref_Debtor_manager_Manager` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `efrsb_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb_manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `CorrespondenceAddress` varchar(300) DEFAULT NULL,
  `EMail` varchar(100) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `INN` varchar(12) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `OGRNIP` varchar(15) DEFAULT NULL,
  `Phone` varchar(12) DEFAULT NULL,
  `RegNum` varchar(30) DEFAULT NULL,
  `Revision` bigint(20) NOT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `SRORegDate` datetime DEFAULT NULL,
  `SRORegNum` varchar(30) DEFAULT NULL,
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Manager`),
  KEY `byFirstName` (`FirstName`),
  KEY `byINN_1` (`INN`),
  KEY `byLastName_1` (`LastName`),
  KEY `byMiddleName` (`MiddleName`),
  KEY `byRevision_2` (`Revision`),
  KEY `refManager_SRO` (`SRORegNum`),
  UNIQUE KEY `byArbitrManagerID_1` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `efrsb_sro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb_sro` (
  `CEOName` varchar(250) DEFAULT NULL,
  `ExtraFields` longblob,
  `INN` varchar(10) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `RegNum` varchar(30) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `SROEmail` varchar(100) DEFAULT NULL,
  `SROPassword` varchar(100) DEFAULT NULL,
  `ShortTitle` varchar(250) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `UrAdress` varchar(250) DEFAULT NULL,
  `id_SRO` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_SRO`),
  UNIQUE KEY `byRegNum_1` (`RegNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `emailtosend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailtosend` (
  `ErrorText` text,
  `id_SentEmail` int(11) NOT NULL,
  PRIMARY KEY (`id_SentEmail`),
  KEY `refEmailToSend_SentEmail` (`id_SentEmail`)
  CONSTRAINT `refEmailToSend_SentEmail` FOREIGN KEY (`id_SentEmail`) REFERENCES `sentemail` (`id_SentEmail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `ArbitrManagerID` int(11) NOT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `EventTime` datetime NOT NULL,
  `MessageGuid` varchar(32) DEFAULT NULL,
  `MessageNumber` varchar(30) DEFAULT NULL,
  `MessageType` char(1) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Event` int(11) NOT NULL AUTO_INCREMENT,
  `isActive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_Event`),
  KEY `RefEvent_Debtor` (`BankruptId`),
  KEY `RefEvent_Manager` (`ArbitrManagerID`),
  KEY `RefEvent_Message` (`efrsb_id`),
  UNIQUE KEY `byNumber` (`MessageNumber`),
  UNIQUE KEY `byRevision` (`Revision`),
  UNIQUE KEY `by_efrsb_id` (`efrsb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `fausing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fausing` (
  `ContractNumber` varchar(40) DEFAULT NULL,
  `IP` varchar(40) NOT NULL,
  `LicenseToken` text,
  `Section` char(4) DEFAULT NULL
  `UsingTime` datetime NOT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `gcevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gcevent` (
  `Body` longblob,
  `EventSource` varchar(20) DEFAULT NULL,
  `EventTimeEnd` datetime DEFAULT NULL,
  `EventTime` datetime NOT NULL,
  `EventType` char(1) NOT NULL,
  `id_GCEvent` int(11) NOT NULL AUTO_INCREMENT,
  `id_GoogleCalendar` int(11) NOT NULL,
  `id_MProcedure` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_GCEvent`),
  KEY `RefGCEvent_Calendar` (`id_GoogleCalendar`),
  KEY `RefGCEvent_MProcedure` (`id_MProcedure`)
  CONSTRAINT `RefGCEvent_Calendar` FOREIGN KEY (`id_GoogleCalendar`) REFERENCES `googlecalendar` (`id_GoogleCalendar`),
  CONSTRAINT `RefGCEvent_MProcedure` FOREIGN KEY (`id_MProcedure`) REFERENCES `mprocedure` (`id_MProcedure`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `gcpushedevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gcpushedevent` (
  `ErrorText` text,
  `Time_dispatched` datetime NOT NULL,
  `Time_pushed` datetime DEFAULT NULL,
  `id_GCEvent` int(11) NOT NULL,
  PRIMARY KEY (`id_GCEvent`),
  KEY `RefGCPushedEvent_Event` (`id_GCEvent`)
  CONSTRAINT `RefGCPushedEvent_Event` FOREIGN KEY (`id_GCEvent`) REFERENCES `gcevent` (`id_GCEvent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `googlecalendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googlecalendar` (
  `email` varchar(100) DEFAULT NULL,
  `id_GoogleCalendar` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  `lastUpload` datetime DEFAULT NULL,
  `nextSyncToken` varchar(50) DEFAULT NULL,
  `refresh_token` varchar(250) NOT NULL,
  PRIMARY KEY (`id_GoogleCalendar`),
  KEY `RefGoogleCalendar_Manager` (`id_Manager`)
  CONSTRAINT `RefGoogleCalendar_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `Description` text,
  `MaxAgeMinutes` int(11) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `id_JobSite` int(11) NOT NULL,
  `id_Job` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Job`),
  KEY `Ref_Job_Site` (`id_JobSite`),
  UNIQUE KEY `bySiteName` (`id_JobSite`,`Name`)
  CONSTRAINT `Ref_Job_Site` FOREIGN KEY (`id_JobSite`) REFERENCES `jobsite` (`id_JobSite`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `joblog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joblog` (
  `Finished` datetime DEFAULT NULL,
  `Started` datetime DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `id_JobLog` int(11) NOT NULL AUTO_INCREMENT,
  `id_Job` int(11) NOT NULL,
  PRIMARY KEY (`id_JobLog`),
  KEY `Ref_Log_Job` (`id_Job`),
  KEY `byJobStarted` (`id_Job`,`Started`)
  CONSTRAINT `Ref_Log_Job` FOREIGN KEY (`id_Job`) REFERENCES `job` (`id_Job`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `jobpart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobpart` (
  `Description` text,
  `Name` varchar(100) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `id_JobPart` int(11) NOT NULL AUTO_INCREMENT,
  `id_Job` int(11) NOT NULL,
  PRIMARY KEY (`id_JobPart`),
  KEY `Ref_Part_Job` (`id_Job`),
  UNIQUE KEY `byJobName` (`id_Job`,`Name`)
  CONSTRAINT `Ref_Part_Job` FOREIGN KEY (`id_Job`) REFERENCES `job` (`id_Job`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `jobpartlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobpartlog` (
  `Finished` datetime DEFAULT NULL,
  `Number` int(11) DEFAULT NULL,
  `Results` text,
  `Started` datetime DEFAULT NULL,
  `Status` char(10) DEFAULT NULL,
  `id_JobLog` int(11) NOT NULL,
  `id_JobPartLog` int(11) NOT NULL AUTO_INCREMENT,
  `id_JobPart` int(11) NOT NULL,
  PRIMARY KEY (`id_JobPartLog`),
  KEY `Ref_JobPart_Job` (`id_JobLog`),
  KEY `Ref_Log_Part` (`id_JobPart`)
  CONSTRAINT `Ref_JobPart_Job` FOREIGN KEY (`id_JobLog`) REFERENCES `joblog` (`id_JobLog`) ON DELETE CASCADE,
  CONSTRAINT `Ref_Log_Part` FOREIGN KEY (`id_JobPart`) REFERENCES `jobpart` (`id_JobPart`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `jobsite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobsite` (
  `Description` text,
  `Name` varchar(100) DEFAULT NULL,
  `id_JobSite` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_JobSite`),
  UNIQUE KEY `byName` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `leak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leak` (
  `ContragentInn` char(12) DEFAULT NULL,
  `ContragentName` varchar(100) DEFAULT NULL,
  `LeakDate` datetime DEFAULT NULL,
  `LeakSum` decimal(10,0) NOT NULL,
  `id_Leak` int(11) NOT NULL AUTO_INCREMENT,
  `id_PData` int(11) NOT NULL,
  `isAccredited` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id_Leak`),
  KEY `refLeak_PData` (`id_PData`)
  CONSTRAINT `refLeak_PData` FOREIGN KEY (`id_PData`) REFERENCES `pdata` (`id_PData`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `log_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_type` (
  `Name` varchar(100) NOT NULL,
  `id_Log_type` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Log_type`),
  UNIQUE KEY `byName` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankroTechAccVerified` bit(1) DEFAULT NULL,
  `BankroTechAcc` text,
  `ExtraParams` longblob,
  `HasIncoming` bit(1) NOT NULL DEFAULT b'0',
  `INN` varchar(12) DEFAULT NULL,
  `ManagerEmail_ChangeTime` datetime DEFAULT NULL,
  `ManagerEmail_Change` varchar(70) DEFAULT NULL,
  `ManagerEmail` varchar(100) DEFAULT NULL,
  `ManagerPassword_ChangeTime` datetime DEFAULT NULL,
  `ManagerPassword_Change` varchar(30) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `ProtocolDate` datetime DEFAULT NULL,
  `ProtocolNum` varchar(50) DEFAULT NULL,
  `TimeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TimeVerified` datetime DEFAULT NULL,
  `VerificationState` char(1) DEFAULT NULL,
  `efrsbNumber` varchar(50) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `id_Contract_Change` int(11) DEFAULT NULL,
  `id_Contract` int(11) DEFAULT NULL,
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `id_SRO` int(11) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_Manager`),
  KEY `Ref_Manager_SRO` (`id_SRO`),
  KEY `refManager_Contract` (`id_Contract`),
  UNIQUE KEY `byArbitrManagerID` (`ArbitrManagerID`),
  UNIQUE KEY `byEMailPassword` (`ManagerEmail`,`Password`),
  UNIQUE KEY `byEMail` (`ManagerEmail`),
  UNIQUE KEY `byEfrsbNumber` (`efrsbNumber`),
  UNIQUE KEY `byINN` (`INN`),
  UNIQUE KEY `byManagerEmail_Change` (`ManagerEmail_Change`)
  CONSTRAINT `Ref_Manager_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `efrsb_sro` (`id_SRO`),
  CONSTRAINT `refManager_Contract` FOREIGN KEY (`id_Contract`) REFERENCES `contract` (`id_Contract`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `managercertauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `managercertauth` (
  `Auth` varchar(32) DEFAULT NULL,
  `Body` longblob,
  `TimeStarted` datetime NOT NULL,
  `id_ManagerCertAuth` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ManagerCertAuth`),
  KEY `Ref_AuthByCert_Manager` (`id_Manager`)
  CONSTRAINT `Ref_AuthByCert_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `managerdocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `managerdocument` (
  `Body` longblob NOT NULL,
  `DocumentType` char(1) DEFAULT NULL,
  `FileName` varchar(100) NOT NULL,
  `PubLock` longblob,
  `id_ManagerDocument` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  PRIMARY KEY (`id_ManagerDocument`),
  KEY `RefManager_Document` (`id_Manager`)
  CONSTRAINT `RefManager_Document` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `manageruser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manageruser` (
  `DefaultViewer` bit(1) NOT NULL,
  `UserName` varchar(70) DEFAULT NULL,
  `id_MUser` int(11) NOT NULL,
  `id_ManagerUser` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  PRIMARY KEY (`id_ManagerUser`),
  KEY `RefManagerUser_MUser` (`id_MUser`),
  KEY `RefManagerUser_Manager` (`id_Manager`),
  UNIQUE KEY `byMUserManager` (`id_MUser`,`id_Manager`)
  CONSTRAINT `RefManagerUser_MUser` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`),
  CONSTRAINT `RefManagerUser_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdata` (
  `ContentHash` varchar(250) DEFAULT NULL,
  `MData_Type` char(1) NOT NULL,
  `fileData` longblob NOT NULL,
  `id_Debtor` int(11) DEFAULT NULL,
  `id_MData` int(11) NOT NULL AUTO_INCREMENT,
  `id_ManagerUser` int(11) NOT NULL,
  `publicDate` datetime NOT NULL,
  `revision` int(11) NOT NULL,
  PRIMARY KEY (`id_MData`),
  KEY `refMData_Debtor` (`id_Debtor`),
  KEY `refMData_ManagerUser` (`id_ManagerUser`)
  CONSTRAINT `refMData_Debtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `refMData_ManagerUser` FOREIGN KEY (`id_ManagerUser`) REFERENCES `manageruser` (`id_ManagerUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mdata_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdata_attachment` (
  `Content` longblob,
  `FileName` varchar(50) DEFAULT NULL,
  `id_MData_attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_MData` int(11) NOT NULL,
  PRIMARY KEY (`id_MData_attachment`),
  KEY `refAttachment_MData` (`id_MData`)
  CONSTRAINT `refAttachment_MData` FOREIGN KEY (`id_MData`) REFERENCES `mdata` (`id_MData`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting` (
  `Body` longblob NOT NULL,
  `Date` datetime NOT NULL,
  `State` char(1) NOT NULL DEFAULT 'a',
  `id_MProcedure` int(11) NOT NULL,
  `id_Meeting` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Meeting`),
  KEY `refMeeting_MProcedure` (`id_MProcedure`)
  CONSTRAINT `refMeeting_MProcedure` FOREIGN KEY (`id_MProcedure`) REFERENCES `mprocedure` (`id_MProcedure`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `meeting_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_document` (
  `Body` longblob,
  `DocumentName` varchar(250) NOT NULL,
  `FileName` varchar(40) NOT NULL,
  `Meeting_document_time` datetime NOT NULL,
  `Parameters` longblob,
  `id_Meeting_document` int(11) NOT NULL AUTO_INCREMENT,
  `id_Meeting` int(11) NOT NULL,
  PRIMARY KEY (`id_Meeting_document`),
  KEY `refMeeting_document_Meeting` (`id_Meeting`)
  CONSTRAINT `refMeeting_document_Meeting` FOREIGN KEY (`id_Meeting`) REFERENCES `meeting` (`id_Meeting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `Body` longblob NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `MessageGUID` varchar(32) DEFAULT NULL,
  `MessageInfo_MessageType` char(1) DEFAULT NULL,
  `Number` varchar(30) DEFAULT NULL,
  `OGRN` varchar(20) DEFAULT NULL,
  `PublishDate` datetime NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `SNILS` varchar(20) DEFAULT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Message` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Message`),
  KEY `refMessage_Debtor` (`BankruptId`),
  UNIQUE KEY `byRevision` (`Revision`),
  UNIQUE KEY `by_efrsb_id_1` (`efrsb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_crm2_contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_crm2_contract` (
  `AMAIsTrial` tinyint(4) DEFAULT NULL,
  `ContractDate` datetime DEFAULT NULL,
  `ContractNumber` varchar(20) DEFAULT NULL,
  `Id` varchar(40) NOT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_debtor` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `Body` longblob NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `LastMessageDate` datetime DEFAULT NULL,
  `Name` varchar(152) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `Revision` bigint(20) NOT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Debtor`),
  KEY `byArbitrManagerID` (`ArbitrManagerID`),
  UNIQUE KEY `byBankruptId` (`BankruptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_debtor_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_debtor_manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `id_Debtor_Manager` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Debtor_Manager`),
  KEY `byBankruptId` (`BankruptId`),
  KEY `debtor_manager_byArbitrManagerID` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_email` (
  `address` varchar(50) NOT NULL,
  `id_Email` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email`),
  UNIQUE KEY `byAddress` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_email_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_email_manager` (
  `id_Email` int(11) NOT NULL,
  `id_Manager` int(11) NOT NULL,
  `id_Message_Last` int(11) DEFAULT NULL,
  `id_Messages` longblob NOT NULL,
  PRIMARY KEY (`id_Email`,`id_Manager`),
  KEY `RefEmail_ManagerEmail` (`id_Email`),
  KEY `RefEmail_ManagerManager` (`id_Manager`),
  KEY `RefEmail_Manager_MessageLast` (`id_Message_Last`)
  CONSTRAINT `RefEmail_ManagerEmail` FOREIGN KEY (`id_Email`) REFERENCES `mock_efrsb_email` (`id_Email`),
  CONSTRAINT `RefEmail_ManagerManager` FOREIGN KEY (`id_Manager`) REFERENCES `mock_efrsb_manager` (`id_Manager`),
  CONSTRAINT `RefEmail_Manager_MessageLast` FOREIGN KEY (`id_Message_Last`) REFERENCES `mock_efrsb_message` (`id_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_event` (
  `ArbitrManagerID` int(11) NOT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `EventTime` datetime NOT NULL,
  `MessageInfo_MessageType` char(1) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Event` int(11) NOT NULL AUTO_INCREMENT,
  `isActive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_Event`),
  KEY `Ref2539` (`ArbitrManagerID`),
  KEY `byEventTime` (`EventTime`),
  KEY `refByEfrsb_id` (`efrsb_id`),
  KEY `refEfrsb_event_Debtor` (`BankruptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `Body` longblob NOT NULL,
  `CorrespondenceAddress` varchar(300) DEFAULT NULL,
  `DateDelete` datetime DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `INN` varchar(12) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `OGRNIP` varchar(12) DEFAULT NULL,
  `RegNum` varchar(30) DEFAULT NULL,
  `Revision` bigint(20) NOT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `SRORegDate` datetime DEFAULT NULL,
  `SRORegNum` varchar(30) NOT NULL,
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Manager`),
  KEY `refManager_SRO` (`SRORegNum`),
  UNIQUE KEY `byArbitrManagerID` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_message` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `Body` longblob NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `MessageGUID` varchar(32) DEFAULT NULL,
  `MessageInfo_MessageType` char(1) DEFAULT NULL,
  `Number` varchar(30) DEFAULT NULL,
  `OGRN` varchar(20) DEFAULT NULL,
  `PublishDate` datetime NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `SNILS` varchar(20) DEFAULT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Message` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Message`),
  KEY `refByArbitrManagerID` (`ArbitrManagerID`),
  KEY `refByBankruptId` (`BankruptId`),
  UNIQUE KEY `by_efrsb_id` (`efrsb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_sro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_sro` (
  `Body` longblob NOT NULL,
  `DateLastModif` datetime NOT NULL,
  `INN` varchar(10) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `RegNum` varchar(30) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `ShortTitle` varchar(250) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `UrAdress` varchar(250) DEFAULT NULL,
  `id_SRO` int(11) NOT NULL,
  PRIMARY KEY (`id_SRO`),
  UNIQUE KEY `byRegNum` (`RegNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mprocedure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mprocedure` (
  `Content_hash` varchar(250) DEFAULT NULL,
  `ExtraParams` longblob,
  `TimeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TimeVerified` datetime DEFAULT NULL,
  `VerificationState` char(1) DEFAULT NULL,
  `casenumber` varchar(60) DEFAULT NULL,
  `ctb_account_limit` bit(1) NOT NULL DEFAULT b'0',
  `ctb_bankrotech_id` varchar(45) DEFAULT NULL,
  `ctb_publicDate` datetime DEFAULT NULL,
  `ctb_revision_posted` bigint(20) DEFAULT NULL,
  `ctb_revision` bigint(20) NOT NULL DEFAULT '0',
  `ctb_update_error` bit(1) NOT NULL DEFAULT b'0',
  `features` char(1) DEFAULT NULL,
  `id_Debtor_Manager` int(11) DEFAULT NULL,
  `id_Debtor` int(11) NOT NULL,
  `id_MProcedure` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  `last_update_finish` datetime DEFAULT NULL,
  `last_update_log` longblob,
  `last_update_start` datetime DEFAULT NULL,
  `procedure_type` char(1) NOT NULL,
  `publicDate` datetime DEFAULT NULL,
  `registrationDate` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_MProcedure`),
  KEY `Debtor_MProcedure` (`id_Debtor`),
  KEY `RefMProcedure_Manager` (`id_Manager`),
  KEY `byCtbRevision` (`ctb_revision`),
  KEY `by_ctb_bankrotech_id` (`ctb_bankrotech_id`),
  KEY `by_id_Message1` (`id_Debtor_Manager`),
  UNIQUE KEY `NaturalKey` (`casenumber`,`procedure_type`,`id_Manager`,`id_Debtor`)
  CONSTRAINT `Debtor_MProcedure` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `RefMProcedure_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mprocedureuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mprocedureuser` (
  `id_MProcedure` int(11) NOT NULL,
  `id_ManagerUser` int(11) NOT NULL,
  `id_Request` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_MProcedure`,`id_ManagerUser`),
  KEY `RefMProcedureUser_MProcedure` (`id_MProcedure`),
  KEY `RefMProcedureUser_ManagerUser` (`id_ManagerUser`),
  KEY `refMProcedureUser_Request` (`id_Request`)
  CONSTRAINT `RefMProcedureUser_MProcedure` FOREIGN KEY (`id_MProcedure`) REFERENCES `mprocedure` (`id_MProcedure`),
  CONSTRAINT `RefMProcedureUser_ManagerUser` FOREIGN KEY (`id_ManagerUser`) REFERENCES `manageruser` (`id_ManagerUser`) ON DELETE CASCADE,
  CONSTRAINT `refMProcedureUser_Request` FOREIGN KEY (`id_Request`) REFERENCES `request` (`id_Request`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mrequest` (
  `ApplicantAddress` varchar(250) DEFAULT NULL,
  `ApplicantCategory` char(1) DEFAULT NULL,
  `ApplicantINN` varchar(12) DEFAULT NULL,
  `ApplicantName` varchar(250) DEFAULT NULL,
  `ApplicantOGRN` varbinary(16) DEFAULT NULL,
  `ApplicantSNILS` varchar(13) DEFAULT NULL,
  `CaseNumber` varchar(50) DEFAULT NULL,
  `CourtDecisionAddInfo` varchar(250) DEFAULT NULL,
  `CourtDecisionURL` varchar(250) DEFAULT NULL,
  `DateOfApplication` datetime DEFAULT NULL,
  `DateOfCreation` datetime NOT NULL,
  `DateOfOfferTill` datetime DEFAULT NULL,
  `DateOfOffer` datetime DEFAULT NULL,
  `DateOfRequestAct` datetime DEFAULT NULL,
  `DateOfRequest` datetime NOT NULL,
  `DateOfResponce` datetime DEFAULT NULL,
  `DebtorAddress2` varchar(250) DEFAULT NULL,
  `DebtorAddress` varchar(250) DEFAULT NULL,
  `DebtorCategory2` char(1) DEFAULT NULL,
  `DebtorCategory` char(1) NOT NULL DEFAULT 'n',
  `DebtorINN2` varchar(12) DEFAULT NULL,
  `DebtorINN` varchar(12) DEFAULT NULL,
  `DebtorName2` varchar(250) DEFAULT NULL,
  `DebtorName` varchar(250) DEFAULT NULL,
  `DebtorOGRN2` varbinary(16) DEFAULT NULL,
  `DebtorOGRN` varbinary(16) DEFAULT NULL,
  `DebtorSNILS2` varchar(13) DEFAULT NULL,
  `DebtorSNILS` varchar(13) DEFAULT NULL,
  `NextSessionDate` datetime DEFAULT NULL,
  `id_Court` int(11) NOT NULL,
  `id_MRequest` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) DEFAULT NULL,
  `id_SRO` int(11) NOT NULL,
  PRIMARY KEY (`id_MRequest`),
  KEY `Ref_MRequest_Manager` (`id_Manager`),
  KEY `Ref_Request_Court` (`id_Court`),
  KEY `Ref_Request_SRO` (`id_SRO`)
  CONSTRAINT `Ref_MRequest_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `Ref_Request_Court` FOREIGN KEY (`id_Court`) REFERENCES `court` (`id_Court`),
  CONSTRAINT `Ref_Request_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `efrsb_sro` (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `muser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muser` (
  `Confirmation_Token_Cookie` char(10) DEFAULT NULL,
  `Confirmation_Token` char(4) DEFAULT NULL,
  `Confirmed` bit(1) NOT NULL DEFAULT b'0',
  `CreatedTime` datetime NOT NULL DEFAULT '2020-12-30 00:00:00',
  `HasOutcoming` bit(1) NOT NULL DEFAULT b'0',
  `Monitoring` longblob,
  `UserBIK` varchar(9) DEFAULT NULL,
  `UserCheckingAccount` varchar(20) DEFAULT NULL,
  `UserContactEmail` varchar(70) DEFAULT NULL,
  `UserContactPhone` varchar(12) DEFAULT NULL,
  `UserEmail_ChangeTime` datetime DEFAULT NULL,
  `UserEmail_Change` varchar(70) DEFAULT NULL,
  `UserEmail` varchar(70) DEFAULT NULL,
  `UserExecutorName` varchar(250) DEFAULT NULL,
  `UserINN` varchar(12) DEFAULT NULL,
  `UserKPP` varchar(9) DEFAULT NULL,
  `UserMailingAddress` varchar(250) DEFAULT NULL,
  `UserName` varchar(70) DEFAULT NULL,
  `UserOGRNIP` varchar(15) DEFAULT NULL,
  `UserOKPO` varchar(14) DEFAULT NULL,
  `UserOfficialAddress` varchar(250) DEFAULT NULL,
  `UserPassword_ChangeTime` datetime DEFAULT NULL,
  `UserPassword_Change` varchar(30) DEFAULT NULL,
  `UserPassword` varchar(30) DEFAULT NULL,
  `UserPhone` varchar(12) DEFAULT NULL,
  `UserPhotoName` varchar(250) DEFAULT NULL,
  `UserPhoto` longblob,
  `UserTelegram` varchar(12) DEFAULT NULL,
  `UserViber` varchar(12) DEFAULT NULL,
  `UserWhatsApp` varchar(12) DEFAULT NULL,
  `id_MUser` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_MUser`),
  UNIQUE KEY `byUserEmail` (`UserEmail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `pdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdata` (
  `Content_hash` varchar(250) DEFAULT NULL,
  `Processed` bit(1) DEFAULT NULL,
  `ctb_allowed` bit(1) NOT NULL DEFAULT b'0',
  `fileData` longblob NOT NULL,
  `id_MProcedure` int(11) NOT NULL,
  `id_PData` int(11) NOT NULL AUTO_INCREMENT,
  `publicDate` datetime NOT NULL,
  `revision` bigint(20) NOT NULL,
  PRIMARY KEY (`id_PData`),
  KEY `RefPData_MProcedure` (`id_MProcedure`),
  KEY `byProcessedRevision` (`Processed`,`revision`),
  UNIQUE KEY `by_id_MProcedure_revision` (`id_MProcedure`,`revision`)
  CONSTRAINT `RefPData_MProcedure` FOREIGN KEY (`id_MProcedure`) REFERENCES `mprocedure` (`id_MProcedure`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `pddocfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pddocfile` (
  `Data` longblob NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `Page` int(11) NOT NULL,
  `id_PDDocFile` int(11) NOT NULL AUTO_INCREMENT,
  `id_PDDocument` int(11) NOT NULL,
  PRIMARY KEY (`id_PDDocFile`),
  KEY `RefPDDocFile_PDDocument` (`id_PDDocument`)
  CONSTRAINT `RefPDDocFile_PDDocument` FOREIGN KEY (`id_PDDocument`) REFERENCES `pddocument` (`id_PDDocument`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `pddocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pddocument` (
  `Comment` text,
  `DateLastChangingFiles` datetime DEFAULT NULL,
  `NameAddPart` varchar(250) DEFAULT NULL,
  `Name` varchar(250) NOT NULL,
  `StatusComment` varchar(250) DEFAULT NULL,
  `Status` char(1) DEFAULT NULL,
  `id_DocType` int(11) DEFAULT NULL,
  `id_PDDocument` int(11) NOT NULL AUTO_INCREMENT,
  `id_PreDebtor` int(11) NOT NULL,
  PRIMARY KEY (`id_PDDocument`),
  KEY `RefDocType_PDDocument` (`id_DocType`),
  KEY `RefPDDocument_PreDebtor` (`id_PreDebtor`)
  CONSTRAINT `RefDocType_PDDocument` FOREIGN KEY (`id_DocType`) REFERENCES `doctype` (`id_DocType`) ON DELETE SET NULL,
  CONSTRAINT `RefPDDocument_PreDebtor` FOREIGN KEY (`id_PreDebtor`) REFERENCES `predebtor` (`id_PreDebtor`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `peprousing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peprousing` (
  `ContractNumber` varchar(40) DEFAULT NULL,
  `IP` varchar(40) NOT NULL,
  `LicenseToken` text,
  `Section` char(4) DEFAULT NULL
  `UsingTime` datetime NOT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `predebtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `predebtor` (
  `CaseNumber` varchar(50) DEFAULT NULL,
  `ContractNumber` varchar(50) DEFAULT NULL,
  `DateOfApplication` datetime DEFAULT NULL,
  `DateOfContract` datetime DEFAULT NULL,
  `DebtorAddressArea` varchar(250) DEFAULT NULL,
  `DebtorAddressBlock` varchar(10) DEFAULT NULL,
  `DebtorAddressBuildStatus` char(1) DEFAULT NULL,
  `DebtorAddressCity` varchar(250) DEFAULT NULL,
  `DebtorAddressFlat` varchar(10) DEFAULT NULL,
  `DebtorAddressHouse` varchar(10) DEFAULT NULL,
  `DebtorAddressRegion` varchar(250) DEFAULT NULL,
  `DebtorAddressSettlment` varchar(250) DEFAULT NULL,
  `DebtorAddressStreet` varchar(250) DEFAULT NULL,
  `DebtorAddress` varchar(250) DEFAULT NULL,
  `DebtorBirthAddress` varchar(250) DEFAULT NULL,
  `DebtorBirthday` date DEFAULT NULL,
  `DebtorCabinetPassword` varchar(13) DEFAULT NULL,
  `DebtorCabinetUUID` varchar(27) DEFAULT NULL,
  `DebtorCategory` char(1) NOT NULL,
  `DebtorEmail` varchar(100) DEFAULT NULL,
  `DebtorIDCardType` varchar(50) DEFAULT NULL,
  `DebtorINN` varchar(12) DEFAULT NULL,
  `DebtorIdCardNumber` varchar(50) DEFAULT NULL,
  `DebtorIdCardSeries` varchar(50) DEFAULT NULL,
  `DebtorName` varchar(250) NOT NULL,
  `DebtorOGRN` varbinary(16) DEFAULT NULL,
  `DebtorPhone` varchar(12) DEFAULT NULL,
  `DebtorRegistrationAddress` varchar(250) DEFAULT NULL,
  `DebtorSNILS` varchar(13) DEFAULT NULL,
  `Note` varchar(250) DEFAULT NULL,
  `OldDebtorName2` varchar(250) DEFAULT NULL,
  `OldDebtorName` varchar(250) DEFAULT NULL,
  `ShowToAU` bit(1) NOT NULL DEFAULT b'0',
  `id_Court` int(11) DEFAULT NULL,
  `id_MUser` int(11) DEFAULT NULL,
  `id_Manager` int(11) DEFAULT NULL,
  `id_PreDebtor` int(11) NOT NULL AUTO_INCREMENT,
  `id_Region` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_PreDebtor`),
  KEY `RefPreDebtor_Court` (`id_Court`),
  KEY `RefPreDebtor_MUser` (`id_MUser`),
  KEY `RefPreDebtor_Manager` (`id_Manager`),
  KEY `RefPreDebtor_region` (`id_Region`)
  CONSTRAINT `RefPreDebtor_Court` FOREIGN KEY (`id_Court`) REFERENCES `court` (`id_Court`),
  CONSTRAINT `RefPreDebtor_MUser` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`),
  CONSTRAINT `RefPreDebtor_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `RefPreDebtor_region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `procedurestart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedurestart` (
  `AddedBySRO` bit(1) NOT NULL DEFAULT b'0',
  `ApplicantAddress` varchar(250) DEFAULT NULL,
  `ApplicantCategory` char(1) DEFAULT NULL,
  `ApplicantINN` varchar(12) DEFAULT NULL,
  `ApplicantName` varchar(250) DEFAULT NULL,
  `ApplicantOGRN` varbinary(16) DEFAULT NULL,
  `ApplicantSNILS` varchar(13) DEFAULT NULL,
  `CaseNumber` varchar(50) DEFAULT NULL,
  `CourtDecisionAddInfo` varchar(250) DEFAULT NULL,
  `CourtDecisionURL` varchar(250) DEFAULT NULL,
  `DateOfAcceptance` datetime DEFAULT NULL,
  `DateOfApplication` datetime DEFAULT NULL,
  `DateOfPrescription` datetime DEFAULT NULL,
  `DateOfRequestAct` datetime DEFAULT NULL,
  `DebtorAddress2` varchar(250) DEFAULT NULL,
  `DebtorAddress` varchar(250) DEFAULT NULL,
  `DebtorCategory2` char(1) DEFAULT NULL,
  `DebtorCategory` char(1) NOT NULL DEFAULT 'n',
  `DebtorINN2` varchar(12) DEFAULT NULL,
  `DebtorINN` varchar(12) DEFAULT NULL,
  `DebtorName2` varchar(250) DEFAULT NULL,
  `DebtorName` varchar(250) DEFAULT NULL,
  `DebtorOGRN2` varbinary(16) DEFAULT NULL,
  `DebtorOGRN` varbinary(16) DEFAULT NULL,
  `DebtorSNILS2` varchar(13) DEFAULT NULL,
  `DebtorSNILS` varchar(13) DEFAULT NULL,
  `NextSessionDate` datetime DEFAULT NULL,
  `PrescriptionAddInfo` varchar(250) DEFAULT NULL,
  `PrescriptionURL` varchar(250) DEFAULT NULL,
  `ReadyToRegistrate` bit(1) NOT NULL DEFAULT b'0',
  `ShowToSRO` bit(1) NOT NULL DEFAULT b'0',
  `TimeOfCreate` datetime NOT NULL,
  `TimeOfLastCheckingEFRSB` datetime DEFAULT NULL,
  `TimeOfLastChecking` datetime DEFAULT NULL,
  `efrsbPrescriptionAddInfo` varchar(250) DEFAULT NULL,
  `efrsbPrescriptionID` varchar(32) DEFAULT NULL,
  `efrsbPrescriptionNumber` varchar(30) DEFAULT NULL,
  `efrsbPrescriptionPublishDate` datetime DEFAULT NULL,
  `id_Contract` int(11) DEFAULT NULL,
  `id_Court` int(11) NOT NULL,
  `id_MRequest` int(11) DEFAULT NULL,
  `id_MUser` int(11) DEFAULT NULL,
  `id_Manager` int(11) DEFAULT NULL,
  `id_ProcedureStart` int(11) NOT NULL AUTO_INCREMENT,
  `id_SRO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ProcedureStart`),
  KEY `Ref_ProcedureStart_Contract` (`id_Contract`),
  KEY `Ref_ProcedureStart_Court` (`id_Court`),
  KEY `Ref_ProcedureStart_Manager` (`id_Manager`),
  KEY `Ref_ProcedureStart_User` (`id_MUser`),
  KEY `Ref_Start_Request` (`id_MRequest`),
  KEY `Ref_Start_SRO` (`id_SRO`)
  CONSTRAINT `Ref_ProcedureStart_Contract` FOREIGN KEY (`id_Contract`) REFERENCES `contract` (`id_Contract`),
  CONSTRAINT `Ref_ProcedureStart_Court` FOREIGN KEY (`id_Court`) REFERENCES `court` (`id_Court`),
  CONSTRAINT `Ref_ProcedureStart_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `Ref_ProcedureStart_User` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`),
  CONSTRAINT `Ref_Start_Request` FOREIGN KEY (`id_MRequest`) REFERENCES `mrequest` (`id_MRequest`),
  CONSTRAINT `Ref_Start_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `efrsb_sro` (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `procedureusing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedureusing` (
  `ContractNumber` varchar(40) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `IP` varchar(40) NOT NULL,
  `LicenseToken` text,
  `OGRN` varbinary(16) DEFAULT NULL,
  `SNILS` varchar(13) DEFAULT NULL,
  `Section` char(4) DEFAULT NULL
  `UsingTime` datetime NOT NULL,
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `procedureusing_indexed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedureusing_indexed` (
  `ContractNumber` varchar(10) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `IP` varchar(40) DEFAULT NULL,
  `OGRN` varbinary(16) DEFAULT NULL,
  `SNILS` varchar(13) DEFAULT NULL,
  `Section` char(4) DEFAULT NULL,
  `UsingTime` datetime DEFAULT NULL,
  KEY `byContractNumber` (`ContractNumber`),
  KEY `byIP` (`IP`),
  KEY `bySection` (`Section`),
  KEY `byUsingTime` (`UsingTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `push_receiver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `push_receiver` (
  `Category` char(1) NOT NULL,
  `Check_day_off` bit(1) NOT NULL DEFAULT b'0',
  `Destination` varchar(200) DEFAULT NULL,
  `For_Auction` char(1) DEFAULT NULL,
  `For_Committee` char(1) DEFAULT NULL,
  `For_CourtDecision` char(1) DEFAULT NULL,
  `For_MeetingPB` char(1) DEFAULT NULL,
  `For_MeetingWorker` char(1) DEFAULT NULL,
  `For_Meeting` char(1) DEFAULT NULL,
  `For_Message_after` datetime DEFAULT NULL,
  `ReceiverGUID` varchar(32) DEFAULT NULL,
  `Time_finish` int(11) DEFAULT NULL,
  `Time_start` int(11) DEFAULT NULL,
  `Timezone` int(11) DEFAULT NULL,
  `Transport` char(1) DEFAULT NULL,
  `id_Manager_Contract_MUser` int(11) NOT NULL,
  `id_Push_receiver` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Push_receiver`),
  UNIQUE KEY `byCategoryId` (`Category`,`id_Manager_Contract_MUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `push_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `push_request` (
  `DaysBefore` smallint(6) NOT NULL,
  `MessageNumber` varchar(30) NOT NULL,
  `id_Push_receiver` int(11) NOT NULL,
  `id_Push_request` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Push_request`),
  KEY `RefPush_request_Event` (`MessageNumber`),
  KEY `RefPush_request_Push_receiver` (`id_Push_receiver`)
  CONSTRAINT `RefPush_request_Push_receiver` FOREIGN KEY (`id_Push_receiver`) REFERENCES `push_receiver` (`id_Push_receiver`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `pushed_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pushed_event` (
  `ErrorText` text,
  `Time_dispatched` datetime NOT NULL,
  `Time_pushed` datetime DEFAULT NULL,
  `id_Event` int(11) NOT NULL,
  `id_Push_receiver` int(11) NOT NULL,
  PRIMARY KEY (`id_Event`,`id_Push_receiver`),
  KEY `RefPushed_event_Event` (`id_Event`),
  KEY `RefPushed_event_Push_receiver` (`id_Push_receiver`)
  CONSTRAINT `RefPushed_event_Event` FOREIGN KEY (`id_Event`) REFERENCES `event` (`id_Event`),
  CONSTRAINT `RefPushed_event_Push_receiver` FOREIGN KEY (`id_Push_receiver`) REFERENCES `push_receiver` (`id_Push_receiver`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `pushed_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pushed_message` (
  `ErrorText` text,
  `Time_dispatched` datetime NOT NULL,
  `Time_pushed` datetime DEFAULT NULL,
  `id_Message` int(11) NOT NULL,
  `id_Push_receiver` int(11) NOT NULL,
  PRIMARY KEY (`id_Message`,`id_Push_receiver`),
  KEY `RefPushed_message_Message` (`id_Message`),
  KEY `RefPushed_message_receiver` (`id_Push_receiver`)
  CONSTRAINT `RefPushed_message_Message` FOREIGN KEY (`id_Message`) REFERENCES `message` (`id_Message`) ON DELETE CASCADE,
  CONSTRAINT `RefPushed_message_receiver` FOREIGN KEY (`id_Push_receiver`) REFERENCES `push_receiver` (`id_Push_receiver`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `Name` varchar(250) NOT NULL,
  `OKATO` char(6) DEFAULT NULL,
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Region`),
  UNIQUE KEY `byOKATO` (`OKATO`),
  UNIQUE KEY `byRegionName` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request` (
  `Body` longblob NOT NULL,
  `State` char(1) NOT NULL DEFAULT 'a',
  `TimeLastChange` datetime NOT NULL,
  `id_Debtor` int(11) NOT NULL,
  `id_MUser` int(11) NOT NULL,
  `id_Request` int(11) NOT NULL AUTO_INCREMENT,
  `managerEfrsbNumber` varchar(50) DEFAULT NULL,
  `managerInn` varchar(12) DEFAULT NULL,
  `managerName` varchar(100) NOT NULL,
  PRIMARY KEY (`id_Request`),
  KEY `Debtor_Request` (`id_Debtor`),
  KEY `refRequest_MUser` (`id_MUser`)
  CONSTRAINT `Debtor_Request` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `refRequest_MUser` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `rosreestr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rosreestr` (
  `Address` text NOT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  `Name` varchar(70) NOT NULL,
  `id_Region` int(11) NOT NULL,
  `id_Rosreestr` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Rosreestr`),
  KEY `refRosreestr_Region` (`id_Region`)
  CONSTRAINT `refRosreestr_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `sentemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sentemail` (
  `EmailType` char(1) NOT NULL,
  `ExtraParams` longblob,
  `Message` longblob,
  `RecipientEmail` varchar(100) NOT NULL,
  `RecipientId` int(11) NOT NULL,
  `RecipientType` char(1) NOT NULL,
  `SenderServer` varchar(50) DEFAULT NULL,
  `SenderUser` varchar(50) DEFAULT NULL,
  `TimeDispatch` datetime NOT NULL,
  `TimeSent` datetime DEFAULT NULL,
  `id_SentEmail` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_SentEmail`),
  KEY `byRecipient` (`RecipientId`,`RecipientType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `TimeLastUsed` datetime NOT NULL,
  `TimeStarted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Token` varchar(100) DEFAULT NULL,
  `Who` char(1) NOT NULL,
  `id_Owner` int(11) NOT NULL,
  `id_Session` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Session`),
  UNIQUE KEY `byToken` (`Token`),
  UNIQUE KEY `byWhoOwner` (`Who`,`id_Owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `srodocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `srodocument` (
  `Body` longblob NOT NULL,
  `DocumentType` char(1) NOT NULL,
  `FileName` varchar(100) NOT NULL,
  `PubLock` longblob,
  `id_SRODocument` int(11) NOT NULL AUTO_INCREMENT,
  `id_SRO` int(11) NOT NULL,
  PRIMARY KEY (`id_SRODocument`),
  KEY `refSRO_Document` (`id_SRO`)
  CONSTRAINT `refSRO_Document` FOREIGN KEY (`id_SRO`) REFERENCES `efrsb_sro` (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `sub_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_level` (
  `Reglament_Date` date DEFAULT NULL,
  `Reglament_Title` varchar(250) DEFAULT NULL,
  `Reglament_Url` text,
  `StartDate` date NOT NULL,
  `Sum_Common` decimal(10,2) DEFAULT NULL,
  `Sum_Employable` decimal(10,2) DEFAULT NULL,
  `Sum_Infant` decimal(10,2) DEFAULT NULL,
  `Sum_Pensioner` decimal(10,2) DEFAULT NULL,
  `id_Contract` int(11) DEFAULT NULL,
  `id_Region` int(11) NOT NULL,
  `id_Sub_level` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Sub_level`),
  KEY `RefSubLevel_Contract` (`id_Contract`),
  KEY `Ref_Sub_level_Region` (`id_Region`)
  CONSTRAINT `RefSubLevel_Contract` FOREIGN KEY (`id_Contract`) REFERENCES `contract` (`id_Contract`),
  CONSTRAINT `Ref_Sub_level_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `MigrationName` varchar(250) NOT NULL,
  `MigrationNumber` varchar(250) NOT NULL,
  `MigrationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MigrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote` (
  `Answers` longblob,
  `CabinetKey` varchar(20) NOT NULL,
  `CabinetTime` datetime NOT NULL,
  `Confirmation_code_time` datetime DEFAULT NULL,
  `Confirmation_code` varchar(10) DEFAULT NULL,
  `Member` longblob,
  `id_Meeting` int(11) NOT NULL,
  `id_Vote` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Vote`),
  KEY `byCabinetKey` (`CabinetKey`),
  KEY `refVote_Meeting` (`id_Meeting`)
  CONSTRAINT `refVote_Meeting` FOREIGN KEY (`id_Meeting`) REFERENCES `meeting` (`id_Meeting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `vote_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote_document` (
  `Body` longblob,
  `FileName` varchar(40) NOT NULL,
  `Parameters` longblob,
  `Vote_document_time` datetime NOT NULL,
  `Vote_document_type` char(1) NOT NULL,
  `id_Vote_document` int(11) NOT NULL AUTO_INCREMENT,
  `id_Vote` int(11) NOT NULL,
  PRIMARY KEY (`id_Vote_document`),
  KEY `refVote_document_Vote` (`id_Vote`)
  CONSTRAINT `refVote_document_Vote` FOREIGN KEY (`id_Vote`) REFERENCES `vote` (`id_Vote`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `vote_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote_log` (
  `Vote_log_time` datetime NOT NULL,
  `Vote_log_type` char(1) NOT NULL,
  `body` longblob,
  `id_Vote_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_Vote` int(11) NOT NULL,
  PRIMARY KEY (`id_Vote_log`),
  KEY `refVote_log_Vote` (`id_Vote`)
  CONSTRAINT `refVote_log_Vote` FOREIGN KEY (`id_Vote`) REFERENCES `vote` (`id_Vote`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `wcalendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcalendar` (
  `Days` longblob,
  `Revision` int(11) NOT NULL,
  `Year` int(11) NOT NULL,
  `id_Region` int(11) DEFAULT NULL,
  `id_Wcalendar` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Wcalendar`),
  KEY `refWcalendar_Region` (`id_Region`),
  UNIQUE KEY `byRevision` (`Revision`),
  UNIQUE KEY `by_id_Region_Year` (`id_Region`,`Year`)
  CONSTRAINT `refWcalendar_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--
/*!50003 DROP FUNCTION IF EXISTS `FaUsing_Section_readable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `FaUsing_Section_readable`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return concat(
case aSection
when 1  then 'Создать новый финанализ'
when 2  then 'Импорт/балансы/1С'
when 3  then 'Импорт/балансы/ФНС'
when 4  then 'Импорт/КМ/ПАУ'
when 5  then 'Импорт/КМ/витрина'
when 6  then 'Импорт/Сделки/Excel'
when 7  then 'Сделки/Добавить'
when 8  then 'Сделки/Импортировать банковскую выписку'
when 9  then 'Сделки/Получить-Обновить аналитику'
when 10  then 'Документ/О признаказ плохого банкротства'
when 11  then 'Документ/Анализ контрагентов'
else 'неучтённое действие'
end
,' (сч.',aSection,')'
);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getNotifyDate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `getNotifyDate`(
	notifyType CHAR(1),
    eventTime DATETIME,
    checkDayOff BIT(1)
) RETURNS datetime
BEGIN
    DECLARE dateResult DATETIME;
	CASE
	  WHEN notifyType = 'a' THEN SET dateResult = '2000-01-01';
      WHEN notifyType = 'b' THEN SET dateResult = eventTime - interval 1 day;
      WHEN notifyType = 'c' THEN SET dateResult = eventTime - interval 3 day;
      WHEN notifyType = 'd' THEN SET dateResult = eventTime - interval 5 day;
      WHEN notifyType = 'e' THEN SET dateResult = eventTime - interval 15 day;
	  ELSE SET dateResult = eventTime;
	END CASE;
    CASE
	  WHEN WEEKDAY(dateResult) = 5 && checkDayOff = 0 THEN SET dateResult = dateResult - interval 1 day;
	  WHEN WEEKDAY(dateResult) = 6 && checkDayOff = 0 THEN SET dateResult = dateResult - interval 2 day;
	  ELSE SET dateResult = dateResult;
	END CASE;
	RETURN dateResult;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `PeproUsing_Section_readable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `PeproUsing_Section_readable`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return concat(
case aSection
when 1  then 'Создать исходящее'
when 2  then 'Печатать конверт'
when 3  then 'Печатать уведомление'
when 4  then 'Печатать опись вложение'
when 5  then 'Создать реестр'
when 6  then 'Создать партию отправлений на сайте Почта России'
when 7  then 'Создать файл для загрузки на сайт Почта России'
when 8  then 'Рассылка'
when 9  then 'Добавить шаблон'
when 10 then 'Добавить входящее'
when 11 then 'Печатать журнал входящих'
else 'неучтённое действие'
end
,' (сч.',aSection,')'
);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProceduDebtor_INN_OGRN_SNILS_lp_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `ProceduDebtor_INN_OGRN_SNILS_lp_id`(
  aINN   varchar(12)
 ,aOGRN  varchar(16)
 ,aSNILS varchar(13)
) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  if (aSNILS is not null and 0<>length(aSNILS) or length(aINN)=12) then
    return null;
  else
    return concat(ifnull(aINN,''),'.',ifnull(aOGRN,''));
  end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProceduDebtor_INN_OGRN_SNILS_np_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `ProceduDebtor_INN_OGRN_SNILS_np_id`(
  aINN   varchar(12)
 ,aOGRN  varchar(16)
 ,aSNILS varchar(13)
) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  if (aSNILS is not null and 0<>length(aSNILS) or length(aINN)=12) then
    return concat(ifnull(aINN,''),'.',ifnull(aSNILS,''));
  else
    return null;
  end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProcedureType_SafeDBValueForShort` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `ProcedureType_SafeDBValueForShort`(short_name char(3)) RETURNS varchar(2) CHARSET utf8
    DETERMINISTIC
begin
  case short_name
    when 'Н'  then return 'n';
    when 'Нс' then return 'n';
    when 'КП' then return 'k';
    when 'КПс' then return 'k';
    when 'РИ' then return 'i';
    when 'РД' then return 'd';
    when 'ВУ' then return 'v';
    when 'ВУс' then return 'v';
    when 'ОД' then return 'o';
    when 'ОДс' then return 'o';
    when 'Б'  then return 'b';
    else return 1/0;
  end case;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProcedureType_SafeShortForDBValue` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `ProcedureType_SafeShortForDBValue`(db_value char(1)) RETURNS varchar(2) CHARSET utf8
    DETERMINISTIC
begin
	case db_value
		when 'n' then return 'Н';
		when 'k' then return 'КП';
		when 'i' then return 'РИ';
		when 'd' then return 'РД';
		when 'v' then return 'ВУ';
		when 'o' then return 'ОД';
		when 'b' then return 'Б';
		else return db_value;
	end case;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProcedureUsing_Section_readable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `ProcedureUsing_Section_readable`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return if(83>convert(aSection,unsigned integer),ProcedureUsing_Section_readable_3_20(aSection),concat(
case aSection

when 83 then 'импорт банк выписки/начать'
when 84 then 'импорт банк выписки/завершить'
when 85 then 'импорт ден опер из excel/завершить'
when 86 then 'кнопка Должник'
when 87 then 'Кнопка Процедура'
when 88 then 'Подпись/загрузить'
when 89 then 'Подпись АУ/Добавить'
when 90 then 'Инструкция по добавлению подписи'
when 91 then 'Инструкция по массовому погашению'
when 92 then 'Анализ сделок/открытие заключ преднамер фикт'
when 93 then 'Финальный отчет на ЕФРСБ'
when 94 then 'Дебиторка/указать погаш сумму ссылка'
when 95 then 'Дебиторка/добавление погашения'
when 96 then 'Дебиторка/погашение/кнопка Создать доход'
when 97 then 'Дебиторка/погашение/учесть старые записи как погашения'
when 98 then 'Список процедур в excel'
when 99 then 'залоговый счет/добавление второго и последующего'
when 100 then 'Учетная группа Залоговый/открытие'
when 101 then 'Импорт объектов КМ из excel'
when 102 then 'Импорт НДС из excel'
when 103 then 'Импорт ден средств КМ из excel'
when 104 then 'Импорт дебиторской задолженности из excel'
when 105 then 'ФА ФЛ/показывать требования не включенные в реестр'
when 106 then 'Сводный анализ дебиторской задолженности'
when 107 then 'документ архива младше 30 дней'
when 108 then 'документ архива 30-180 дней'
when 109 then 'документ архива старше 180 дней'
when 110 then 'Текущее требование/зарегистрировать'
when 111 then 'Свойства должника/кнопка Печать'
when 112 then 'Дебиторка/добавление фото'
when 113 then 'Нажатие на Уведомление'
when 114 then 'Нажатие на уведомление по Мобилке'

else 'неучтённое действие'
end
,' (сч.',aSection,')'
));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProcedureUsing_Section_readable_3_19` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `ProcedureUsing_Section_readable_3_19`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
 return concat(
case aSection
when 1 then 'СК/сохранить'
when 2 then 'СК/протокол'
when 3 then 'СК/к повестке'
when 4 then 'СК/протокол открыть'
when 5 then 'РТК/создание ТК'
when 6 then 'РТК/МПТ/запланировать'
when 7 then 'РТК/печатать'
when 8 then 'Финансы/создание расхода (старая версия)'
when 9 then 'Делопроизводство/регистрация входящего'
when 10 then 'Делопроизводство/создать партию на сайте Почты'
when 11 then 'КМ/сохранить ИГ'
when 12 then 'КМ/создать объект'
when 13 then 'КМ/создать дебиторку'
when 14 then 'Планирование/сохранить задачу'
when 15 then 'Торги/сохранить лот'
when 16 then 'анализ сделок/добавить'
when 17 then 'ПГЗ/Должник/пакет'
when 18 then 'ПГЗ/Должник/печать'
when 19 then 'ПГЗ/Должник/редактировать'
when 20 then 'ПГЗ/КДЛ/пакет'
when 21 then 'ПГЗ/КДЛ/печать'
when 22 then 'ПГЗ/КДЛ/редактировать'
when 23 then 'ПГЗ/супруг/пакет'
when 24 then 'ПГЗ/супруг/печать'
when 25 then 'ПГЗ/супруг/редактировать'
when 26 then 'ЕФРСБ/подать'
when 27 then 'финанализ/фл/сформировать'
when 28 then 'ДПМ/ефрсб/опубликовать/введение/ри'
when 29 then 'ДПМ/ефрсб/опубликовать/введение/кп'
when 30 then 'ДПМ/СК/выполнено'
when 31 then 'РЖП/зарегистрировать'
when 32 then 'отчёт/превью/получатели'
when 33 then 'отчёт/превью/word'
when 34 then 'работники/уведомление'
when 35 then 'работники/уведомление/печать'
when 36 then 'работники/уведомление/редактировать'
when 37 then 'запросыпосчетам/основной/запросить'
when 38 then 'запросыпосчетам/основной/печать'
when 39 then 'запросыпосчетам/основной/редактировать'
when 40 then 'запросыпосчетам/закрыть/запросить'
when 41 then 'запросыпосчетам/закрыть/печать'
when 42 then 'запросыпосчетам/закрыть/редактировать'
else 'неучтённое действие'
end
,' (сч.',aSection,')'
);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProcedureUsing_Section_readable_3_20` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `ProcedureUsing_Section_readable_3_20`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return if(43>convert(aSection,unsigned integer),ProcedureUsing_Section_readable_3_19(aSection),concat(
case aSection

when 43 then 'Финансы/расход/прож.минимум'
when 44 then 'Финансы/расход/ефрсб'
when 45 then 'Финансы/расход/почта'
when 46 then 'Финансы/расход/коммерсант'
when 47 then 'Финансы/доход'
when 48 then 'Финансы/фото'
when 49 then 'Финансы/создать учётную группу'

when 50 then 'Финансы/запросить'
when 51 then 'Финансы/счёт/залоговый'
when 52 then 'Финансы/счёт/задатки'

when 53 then 'Работники/добавить'
when 54 then 'Работники/импортировать'
when 55 then 'Работники/собрание'

when 56 then 'Права/группа'
when 57 then 'Права/изменить уровень доступа'
when 58 then 'Права/назначить группу для процедуры'

when 59 then 'ФЛ/2НДФЛ'
when 60 then 'ФЛ/доход'
when 61 then 'ФЛ/супруг'
when 62 then 'ФЛ/иждивенец'

when 63 then 'Субсидиарка/Собрать'
when 64 then 'Субсидиарка/КДЛ'
when 65 then 'Субсидиарка/Нарушение'
when 66 then 'Субсидиарка/Привлечение'

when 67 then 'РТК/перед ликв.квотой'
when 68 then 'РТК/сокредитор'
when 69 then 'РТК/погашение'

when 70 then 'Обмен/настройка/фото'
when 71 then 'Обмен/настройка/авто'
when 72 then 'Обмен/настройка/google calendar'

when 73 then 'ПГЗ/должник/pdf'
when 74 then 'ПГЗ/супруг/pdf'
when 75 then 'ПГЗ/КДЛ/pdf'
when 76 then 'ПГЗ/банки/pdf'

when 77 then 'ефрсб/введение/мастер'
when 78 then 'ефрсб/введение/меню/подача'
when 79 then 'ефрсб/введение/редактировать'

when 80 then 'собрание дольщиков'

when 81 then 'Финансы/расход/общего вида'

when 82 then 'Документ/открытие из архива'

else 'неучтённое действие'
end
,' (сч.',aSection,')'
));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `short_fio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `short_fio`(LastName text, FistName text, MiddleName text) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return concat(LastName
  	, if(FistName   IS NULL || 0=length(FistName),   '', concat(' ', substr(FistName,   1, 1), '.'))
  	, if(MiddleName IS NULL || 0=length(MiddleName), '', concat(' ', substr(MiddleName, 1, 1), '.')));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Add_Debtor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `Add_Debtor`(
	  pInn VARCHAR(12)
	, pDebtorName VARCHAR(250)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)

	, out opid_Debtor int
)
begin
	declare lVerificationState char(1);
	declare lSnils VARCHAR(13);
	declare lInn VARCHAR(12);
	declare lOgrn VARCHAR(16);

	declare exit handler for 1172 
	begin 
		select null into opid_Debtor;
		select "Debtor" as error_type, 'w' as error_details; 
	end;

	call Fix_Debtor(pInn,pOgrn,pSnils,lInn,lOgrn,lSnils);

	if (lInn is null && lOgrn is null && lSnils is null) then
		select "Debtor" as error_type, 'i' as error_details; 
	else		
		select id_Debtor, VerificationState into opid_Debtor, lVerificationState from Debtor
		where ( (lInn   is not null and INN   is not null and   INN=lInn)
		     or (lSnils is not null and SNILS is not null and SNILS=lSnils)
		     or (lOgrn  is not null and OGRN  is not null and  OGRN=lOgrn))
		and (lInn   is null or Debtor.INN   is null or   lInn=Debtor.INN)
		and (lSnils is null or Debtor.SNILS is null or lSnils=Debtor.SNILS)
		and (lOgrn  is null or Debtor.OGRN  is null or  lOgrn=Debtor.OGRN)
		;
		if (opid_Debtor is null) then
			insert into Debtor
			set Name= pDebtorName, INN= lInn, SNILS= lSnils, OGRN= lOgrn;
			select last_insert_id() into opid_Debtor;
		else
			if ('v'=lVerificationState) then 
				select null into lVerificationState;
			elseif (lVerificationState is null || 's'=lVerificationState) then 
				update Debtor
				set Name=ifnull(pDebtorName,Name), INN=ifnull(lInn,INN), OGRN=ifnull(lOgrn,OGRN), SNILS=ifnull(lSnils,SNILS)
				where id_Debtor=opid_Debtor;
			else
				select null into opid_Debtor;
				select "Debtor" as error_type, lVerificationState as error_details;
			end if;
		end if;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Add_Manager` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `Add_Manager`(
	  pid_Contract int

	, pmFirstName VARCHAR(50)
	, pmMiddleName VARCHAR(50)
	, pmLastName VARCHAR(50)
	, pmEFRSBNum VARCHAR(50)
	, pmInn VARCHAR(12)
	, pmBTAccount TEXT

	, out opid_Manager int
)
begin
	declare lVerificationState char(1);
	declare lpmBTAccount TEXT;

	select id_Manager, VerificationState,  pmBTAccount
	into opid_Manager, lVerificationState, lpmBTAccount
	from Manager
	where id_Contract=pid_Contract && (efrsbNumber=pmEFRSBNum || INN=pmInn);

	if opid_Manager is null then
		insert into Manager
		set id_Contract= pid_Contract, efrsbNumber= pmEFRSBNum,
			firstName= pmFirstName, middleName= pmMiddleName, lastName= pmLastName, INN=pmInn, BankroTechAcc=pmBTAccount;
		select last_insert_id() into opid_Manager;
	else
		if ('v'=lVerificationState) then 
			update Manager
			set BankroTechAcc=pmBTAccount
			where id_Manager=opid_Manager;
		elseif (lVerificationState is null || 's'=lVerificationState) then 
			update Manager
			set firstName= pmFirstName, middleName= pmMiddleName, lastName= pmLastName, INN=pmInn, BankroTechAcc=pmBTAccount
			where id_Manager=opid_Manager;
		else
			select null into opid_Manager;
			select "Manager" as error_type, lVerificationState as error_details;
		end if;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Add_MProcedure` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `Add_MProcedure`(
	  pid_Manager int

	, pInn VARCHAR(12)
	, pDebtorName VARCHAR(250)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)

	, pCasenumber VARCHAR(60)
	, pFeatures CHAR(1)
	, pProcedure_type CHAR(3)

	, out opid_MProcedure int
)
begin
	declare lid_Debtor int;
	declare lProcedure_type CHAR(2);
	declare lProcedure_VerificationState char(1);
	declare lDebtor_VerificationState char(1);

	declare lDebtor_Inn VARCHAR(12);
	declare lDebtor_Name VARCHAR(250);
	declare lDebtor_Ogrn VARCHAR(16);
	declare lDebtor_Snils VARCHAR(13);

	declare exit handler for 1172 
	begin 
		select null into opid_MProcedure;
		select "MProcedure" as error_type, 'w' as error_details; 
	end;

	select ProcedureType_SafeDBValueForShort(pProcedure_type) into lProcedure_type;

	select MProcedure.id_MProcedure, MProcedure.VerificationState, Debtor.VerificationState,
		   Debtor.Name,  Debtor.INN,  Debtor.OGRN,  Debtor.SNILS,  Debtor.id_Debtor
	into   opid_MProcedure,          lProcedure_VerificationState, lDebtor_VerificationState,
		   lDebtor_Name, lDebtor_Inn, lDebtor_Ogrn, lDebtor_Snils, lid_Debtor
	from MProcedure
	inner join Debtor on Debtor.id_Debtor=MProcedure.id_Debtor
	where casenumber=pCasenumber && procedure_type=lProcedure_type && id_Manager=pid_Manager
		&& (pInn is not null or pSnils is not null or pOgrn is not null)
		&& (   (pInn   is not null and Debtor.INN   is not null and   Debtor.INN=pInn)
		    or (pSnils is not null and Debtor.SNILS is not null and Debtor.SNILS=pSnils)
		    or (pOgrn  is not null and Debtor.OGRN  is not null and  Debtor.OGRN=pOgrn))
		&& (pInn   is null or Debtor.INN   is null or   pInn=Debtor.INN)
		&& (pSnils is null or Debtor.SNILS is null or pSnils=Debtor.SNILS)
		&& (pOgrn  is null or Debtor.OGRN  is null or  pOgrn=Debtor.OGRN)
		;

	if (opid_MProcedure is not null) then
		if ('v'=lProcedure_VerificationState) then 
			select null into lProcedure_VerificationState;
		elseif (lProcedure_VerificationState is null) then 
			if (lDebtor_VerificationState is not null && 's'<>lDebtor_VerificationState && 'v'<>lDebtor_VerificationState) then 
				select null into opid_MProcedure;
				select "Debtor" as error_type, lDebtor_VerificationState as error_details;
			elseif (ifnull(lDebtor_Name,'')<>ifnull(pDebtorName,'') or
			        ifnull(lDebtor_Inn,'')<>ifnull(pINN,'') or
			        ifnull(lDebtor_Ogrn,'')<>ifnull(pOgrn,'') or
			        ifnull(lDebtor_Snils,'')<>ifnull(pSnils,''))
			then
				update Debtor set INN=pINN, OGRN=pOgrn, SNILS=pSnils, Name=pDebtorName where id_Debtor=lid_Debtor;
			end if;
		elseif ('s'=lProcedure_VerificationState) then 
			select null into lProcedure_VerificationState;
		else
			select null into opid_MProcedure;
			select "MProcedure" as error_type, lProcedure_VerificationState as error_details;
		end if;
	else
		call Add_Debtor(pInn,pDebtorName,pOgrn,pSnils,lid_Debtor);
		if (lid_Debtor is not null) then
			insert into MProcedure
			set  casenumber= pCasenumber
				,procedure_type= lProcedure_type
				,id_Manager= pid_Manager
				,features= pFeatures
				,id_Debtor= lid_Debtor;
			select last_insert_id() into opid_MProcedure;

			insert into MProcedureUser(id_MProcedure,id_ManagerUser)
				select opid_MProcedure, mu.id_ManagerUser
				from ManagerUser mu
				left join MProcedureUser mpu on mpu.id_ManagerUser=mu.id_ManagerUser
			                                 && mpu.id_MProcedure=opid_MProcedure
				where mu.id_Manager=pid_Manager
				&& 0<>mu.DefaultViewer
				&& mpu.id_ManagerUser is null;
		end if;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `exit_on_error` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `exit_on_error`()
begin
	if (0<>@@error_count) then
		kill connection_id();
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Fix_Debtor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `Fix_Debtor`(
	  pInn VARCHAR(12)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)

	, out poInn VARCHAR(12)
	, out poOgrn VARCHAR(16)
	, out poSnils VARCHAR(13)
)
begin
	select replace(pSnils,' ','') into poSnils;
	select if(poSnils='00000000000' or poSnils='',null,poSnils) into poSnils;
	select replace(pInn,' ','') into poInn;
	select if(poInn='',null,poInn) into poInn;
	select replace(pOgrn,' ','') into poOgrn;
	select if(poOgrn='',null,poOgrn) into poOgrn;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SafeUpdateEfrsbManager` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `SafeUpdateEfrsbManager`(
  pid_Manager int(11),
  pArbitrManagerID int(11),
  pSRORegNum varchar(30),
  pFirstName varchar(50),
  pMiddleName varchar(50),
  pLastName varchar(50),
  pOGRNIP varchar(15),
  pINN varchar(12),
  pSRORegDate datetime,
  pRegNum varchar(30),
  pCorrespondenceAddress varchar(300),
  pRevision bigint(20)
)
BEGIN
	declare lArbitrManagerID int(11);
	declare lid_SRO int(11);

	select ArbitrManagerID into lArbitrManagerID
	from efrsb_manager where id_Manager=pid_Manager;

	if (lArbitrManagerID is null) then
		insert into efrsb_manager
		set id_Manager=pid_Manager, ArbitrManagerID=pArbitrManagerID
		, SRORegNum=pSRORegNum, SRORegDate=pSRORegDate, RegNum=pRegNum
		, FirstName=pFirstName, MiddleName=pMiddleName, LastName=pLastName
		, OGRNIP=pOGRNIP, INN=pINN
		, CorrespondenceAddress=pCorrespondenceAddress
		, Revision=pRevision
		;
	else
		update efrsb_manager
		set ArbitrManagerID=pArbitrManagerID
		, SRORegNum=pSRORegNum, SRORegDate=pSRORegDate, RegNum=pRegNum
		, FirstName=pFirstName, MiddleName=pMiddleName, LastName=pLastName
		, OGRNIP=pOGRNIP, INN=pINN
		, CorrespondenceAddress=pCorrespondenceAddress
		, Revision=pRevision
		where id_Manager=pid_Manager;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SafeUploadAnketa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `SafeUploadAnketa`(
	  pManagerINN varchar(12), pid_MUser int

	, pDebtorFirstName varchar(50), pDebtorLastName varchar(50), pDebtorMiddleName varchar(50)
	, pDebtorEmail varchar(50), pDebtorSNILS varchar(13), pDebtorINN varchar(12)

	, pFileData LONGBLOB, pContent_hash varchar(250)

	, out error_text varchar(250)
)
BEGIN
	declare lrevision bigint;
	declare lid_MData, lid_ManagerUser, lid_Manager int;

	select id_ManagerUser, m.id_Manager into lid_ManagerUser, lid_Manager
	from ManagerUser mu
	inner join Manager m on mu.id_Manager=m.id_Manager
	where INN = pManagerINN and id_MUser = pid_MUser;
	if (lid_ManagerUser is null) then
		set error_text = concat("Отсутствует связь АУ и наблюдателя (ИНН АУ: ",pManagerINN,", id_MUser=",pid_MUser,")");
	else
		select if(max(revision) is null, 1, max(revision) + 1) into lrevision from MData;

		insert into MData
		set id_ManagerUser = lid_ManagerUser, id_Debtor = null, fileData = pFileData,
		revision= lrevision, publicDate= now(), MData_Type='a', ContentHash = pContent_hash;

		select last_insert_id() into lid_MData;

		insert into AnketaNP
		set firstName = pDebtorFirstName, lastName = pDebtorLastName, middleName = pDebtorMiddleName,
		Email= pDebtorEmail, SNILS= pDebtorSNILS, INN= pDebtorINN, id_MData = lid_MData;

		set error_text = null;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SafeUploadDeals` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `SafeUploadDeals`(
	  pManagerINN varchar(12), pid_MUser int, pDebtorINN varchar(12)
	, pFileData LONGBLOB, pContent_hash varchar(250)
)
BEGIN
	declare lrevision bigint;
	declare lid_ManagerUser, lid_Debtor, lid_Manager int;

	select mu.id_ManagerUser, d.id_Debtor, m.id_Manager into lid_ManagerUser, lid_Debtor, lid_Manager
	from ManagerUser mu
	inner join Manager m on m.id_Manager=mu.id_Manager
	inner join MProcedure p on p.id_Manager=m.id_Manager
	inner join Debtor d on d.id_Debtor=p.id_Debtor
	where m.INN=pManagerINN and mu.id_MUser=pid_MUser and d.INN=pDebtorINN
	order by p.publicDate desc
	limit 1;

	if (lid_ManagerUser is null) then
		select concat("Отсутствует связь АУ и наблюдателя (ИНН АУ: ",pManagerINN,", id_MUser=",pid_MUser,")") message;
	else
		select if(max(revision) is null, 1, max(revision) + 1) into lrevision from MData;

		delete from MData
		where id_ManagerUser=lid_ManagerUser and id_Debtor=lid_Debtor and MData_Type='d';

		insert into MData
		set id_ManagerUser = lid_ManagerUser, id_Debtor = lid_Debtor, fileData = pFileData,
		revision= lrevision, publicDate= now(), MData_Type='d', ContentHash = pContent_hash;

		update Manager set HasIncoming=1 where id_Manager=lid_Manager;
		update MUser set HasOutcoming=1 where id_MUser=pid_MUser;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SafeUploadProcedureInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `SafeUploadProcedureInfo`(
	  pid_Contract int

	, pmFirstName VARCHAR(50), pmMiddleName VARCHAR(50), pmLastName VARCHAR(50)
	, pmEFRSBNum VARCHAR(50), pmInn VARCHAR(12), pmBTAccount TEXT

	, pInn VARCHAR(12), pDebtorName VARCHAR(250)
	, pOgrn VARCHAR(16), pSnils VARCHAR(13)

	, pCasenumber VARCHAR(60), pFeatures CHAR(1), pProcedure_type CHAR(2)

	, pallow_ctb BIT(1)

	, pContent_hash VARCHAR(250), pFileData LONGBLOB
)
BEGIN
	declare lid_Manager int;
	declare lid_MProcedure int;
	declare lSnils VARCHAR(13);
	declare lid_Contract int;

	select id_Contract into lid_Contract from Manager where efrsbNumber=pmEFRSBNum;

	if (lid_Contract<>pid_Contract) then
		select "Manager" as error_type, 'c' as error_details; 
	else
		select replace(pSnils,' ','') into lSnils;
		select if(lSnils='00000000000',null,lSnils) into lSnils;

		if ((''=pInn || pInn is null) && (''=pOgrn || pOgrn is null) && (''=lSnils || lSnils is null)) then
			select "Debtor" as error_type, 'i' as error_details; 
		else
			call Add_Manager(pid_Contract
				,pmFirstName,pmMiddleName,pmLastName,pmEFRSBNum,pmInn,pmBTAccount
				,lid_Manager);

			if (lid_Manager is not null) then
				call Add_MProcedure(lid_Manager
					,pInn,pDebtorName,pOgrn,lSnils
					,pCasenumber,pFeatures,pProcedure_type
					,lid_MProcedure);

				if (lid_MProcedure is not null) then
					call UploadProcedureInfo(lid_MProcedure, pallow_ctb, pContent_hash, pFileData);
				end if;
			end if;
		end if;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `safe_add_update_wcalendar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `safe_add_update_wcalendar`(
	pYear int,
	pDays text,
    pOKATO char(6)
	)
begin
	declare lMaxRevision bigint;
    declare lIdRegion int;
	select ifnull(max(Revision), 0) into lMaxRevision from wcalendar;
	select id_Region into lIdRegion from region where region.OKATO=pOKATO;

	insert into wcalendar
		(Year,  Days, id_Region, Revision)
	values
		(pYear, compress(pDays), lIdRegion, lMaxRevision+1)
	on duplicate key update
		Days=compress(pDays)
		, Revision=lMaxRevision+1
		, id_Region = lIdRegion
	;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Select_Debtors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `Select_Debtors`(
	  pInn VARCHAR(12)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)
)
begin
    declare lSnils VARCHAR(13);
    declare lInn VARCHAR(12);
    declare lOgrn VARCHAR(16);

    call Fix_Debtor(pInn,pOgrn,pSnils,lInn,lOgrn,lSnils);

        select id_Debtor, VerificationState, Name, INN, OGRN, SNILS
        from Debtor
        where ( (lInn   is not null and INN   is not null and   INN=lInn)
             or (lSnils is not null and SNILS is not null and SNILS=lSnils)
             or (lOgrn  is not null and OGRN  is not null and  OGRN=lOgrn))
        and (lInn   is null or Debtor.INN   is null or   lInn=Debtor.INN)
        and (lSnils is null or Debtor.SNILS is null or lSnils=Debtor.SNILS)
        and (lOgrn  is null or Debtor.OGRN  is null or  lOgrn=Debtor.OGRN)
        ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateVerifiedDebtor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `UpdateVerifiedDebtor`(
	  pid_Debtor  int
	, pINN        varchar(12)
	, pOGRN       varchar(16)
	, pSNILS      varchar(13)
	, pName       varchar(250)
	, pBankruptId BIGINT
)
begin
	declare lid_Debtor int;

	select id_Debtor into lid_Debtor from Debtor where BankruptId=pBankruptId;

	if (lid_Debtor is null || lid_Debtor=pid_Debtor) then
		select pid_Debtor into lid_Debtor;
	else
		update MProcedure set id_Debtor=lid_Debtor where id_Debtor=pid_Debtor;
		update Request set id_Debtor=lid_Debtor where id_Debtor=pid_Debtor;
		delete from Debtor where id_Debtor=pid_Debtor;
 	end if;

	update Debtor set
		TimeVerified=now()
		, VerificationState='v'
		, BankruptId=pBankruptId
		, INN=pINN
		, OGRN=pOGRN
		, SNILS=pSNILS
		, Name=pName
	where
		id_Debtor=lid_Debtor;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UploadProcedureInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `UploadProcedureInfo`(
	  pid_MProcedure int
	, pallow_ctb BIT(1)
	, pContent_hash VARCHAR(250)
	, pFileData LONGBLOB
)
begin
	declare lrevision bigint;
	declare lpublicDate datetime;
	declare lContent_hash varchar(250);
	declare lid_PData int;
	declare lallow_ctb bit(1);

	select null into lallow_ctb;
	select if(max(revision) is null, 1, max(revision) + 1), now() into lrevision, lpublicDate from PData;

	if (pFileData is not null) then
		insert into PData
		set id_MProcedure = pid_MProcedure, fileData = pFileData, ctb_allowed= pallow_ctb,
			revision= lrevision, publicDate= lpublicDate, Content_hash= pContent_hash;
	else
		select Content_hash into lContent_hash from MProcedure where id_MProcedure=pid_MProcedure;
		if (lContent_hash<>pContent_hash) then
			select * from bad_content_hash_in_throw_exception;
		end if;

		if (b'1' = pallow_ctb) then
			select id_PData, ctb_allowed into lid_PData, lallow_ctb from PData where id_MProcedure=pid_MProcedure order by revision desc limit 1;
			if (b'0' = lallow_ctb) then
				update PData
				set ctb_allowed= pallow_ctb,
					revision= lrevision, publicDate= lpublicDate, Content_hash= pContent_hash
				where id_PData=lid_PData;
			end if;
		end if;
	end if;

	if (b'1' = pallow_ctb && (pFileData is not null || b'0' = lallow_ctb)) then
		update MProcedure set publicDate= lpublicDate, revision= lrevision, Content_hash= pContent_hash
			, ctb_publicDate= lpublicDate, ctb_revision= lrevision
		where id_MProcedure=pid_MProcedure;
	elseif (pFileData is not null) then
		update MProcedure set publicDate= lpublicDate, revision= lrevision, Content_hash= pContent_hash
		where id_MProcedure=pid_MProcedure;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

