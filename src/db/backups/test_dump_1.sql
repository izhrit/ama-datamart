-- MySQL dump 10.13  Distrib 5.7.28, for Win64 (x86_64)
--
-- Host: localhost    Database: datamartdevel
-- ------------------------------------------------------
-- Server version	5.7.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_log`
--

DROP TABLE IF EXISTS `access_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_log` (
  `Details` text,
  `IP` varchar(40) DEFAULT NULL,
  `Id` int(11) DEFAULT NULL,
  `Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_Log_type` int(11) NOT NULL,
  `id_Log` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Log`),
  KEY `access_log_type` (`id_Log_type`),
  KEY `byIP` (`IP`),
  KEY `byId` (`Id`),
  KEY `byTime` (`Time`),
  CONSTRAINT `access_log_type` FOREIGN KEY (`id_Log_type`) REFERENCES `log_type` (`id_Log_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_log`
--

LOCK TABLES `access_log` WRITE;
/*!40000 ALTER TABLE `access_log` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `access_log` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `anketanp`
--

DROP TABLE IF EXISTS `anketanp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anketanp` (
  `Email` varchar(50) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `SNILS` varchar(13) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `id_AnketaNP` int(11) NOT NULL AUTO_INCREMENT,
  `id_MData` int(11) NOT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_AnketaNP`),
  KEY `refAnketaNP_MData` (`id_MData`),
  KEY `byLastName` (`lastName`),
  KEY `bySNILS` (`SNILS`),
  KEY `byINN` (`INN`),
  CONSTRAINT `refAnketaNP_MData` FOREIGN KEY (`id_MData`) REFERENCES `mdata` (`id_MData`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anketanp`
--

LOCK TABLES `anketanp` WRITE;
/*!40000 ALTER TABLE `anketanp` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `anketanp` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `id_Application` int(11) NOT NULL AUTO_INCREMENT,
  `application_data` longblob,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_MUser` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Application`),
  KEY `refApplication_MUser` (`id_MUser`),
  CONSTRAINT `refApplication_MUser` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application`
--

LOCK TABLES `application` WRITE;
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `application` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `applicationfile`
--

DROP TABLE IF EXISTS `applicationfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationfile` (
  `id_ApplicationFile` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `extension` varbinary(16) DEFAULT NULL,
  `data` longblob,
  `id_Application` int(11) NOT NULL,
  PRIMARY KEY (`id_ApplicationFile`),
  KEY `refApplicationFile_Application` (`id_Application`),
  CONSTRAINT `refApplicationFile_Application` FOREIGN KEY (`id_Application`) REFERENCES `application` (`id_Application`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicationfile`
--

LOCK TABLES `applicationfile` WRITE;
/*!40000 ALTER TABLE `applicationfile` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `applicationfile` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `applicationrtk`
--

DROP TABLE IF EXISTS `applicationrtk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicationrtk` (
  `id_ApplicationRTK` int(11) NOT NULL AUTO_INCREMENT,
  `id_MData` int(11) DEFAULT NULL,
  `creditor_inn` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id_ApplicationRTK`),
  KEY `Ref3999` (`id_MData`),
  CONSTRAINT `RefMData99` FOREIGN KEY (`id_MData`) REFERENCES `mdata` (`id_MData`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicationrtk`
--

LOCK TABLES `applicationrtk` WRITE;
/*!40000 ALTER TABLE `applicationrtk` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `applicationrtk` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `attachment`
--

DROP TABLE IF EXISTS `attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachment` (
  `Content` longblob,
  `FileName` varchar(50) DEFAULT NULL,
  `id_Attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_SentEmail` int(11) NOT NULL,
  PRIMARY KEY (`id_Attachment`),
  KEY `refAttachment_EmailToSend` (`id_SentEmail`),
  CONSTRAINT `refAttachment_EmailToSend` FOREIGN KEY (`id_SentEmail`) REFERENCES `emailtosend` (`id_SentEmail`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachment`
--

LOCK TABLES `attachment` WRITE;
/*!40000 ALTER TABLE `attachment` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `attachment` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `awardnominee`
--

DROP TABLE IF EXISTS `awardnominee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awardnominee` (
  `id_AwardNominee` int(11) NOT NULL AUTO_INCREMENT,
  `inn` varchar(13) NOT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  `SRO` text,
  `URL` text,
  PRIMARY KEY (`id_AwardNominee`),
  UNIQUE KEY `byINN` (`inn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `awardnominee`
--

LOCK TABLES `awardnominee` WRITE;
/*!40000 ALTER TABLE `awardnominee` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `awardnominee` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `awardvote`
--

DROP TABLE IF EXISTS `awardvote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awardvote` (
  `id_AwardVote` int(11) NOT NULL AUTO_INCREMENT,
  `id_AwardNominee` int(11) NOT NULL,
  `inn` varchar(13) NOT NULL,
  `VoteTime` datetime NOT NULL,
  `BulletinSignature` longblob,
  `BulletinText` longblob,
  `SRO` text,
  `email` varchar(50) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_AwardVote`),
  UNIQUE KEY `byINN` (`inn`),
  KEY `refAwardVote_AwardNominee` (`id_AwardNominee`),
  CONSTRAINT `refAwardVote_AwardNominee` FOREIGN KEY (`id_AwardNominee`) REFERENCES `awardnominee` (`id_AwardNominee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `awardvote`
--

LOCK TABLES `awardvote` WRITE;
/*!40000 ALTER TABLE `awardvote` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `awardvote` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `ccdocument`
--

DROP TABLE IF EXISTS `ccdocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ccdocument` (
  `DocumentDate` datetime NOT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `id_CCDocument` int(11) NOT NULL AUTO_INCREMENT,
  `id_CourtCase` int(11) NOT NULL,
  PRIMARY KEY (`id_CCDocument`),
  KEY `CCDocument_CourtCase` (`id_CourtCase`),
  CONSTRAINT `CCDocument_CourtCase` FOREIGN KEY (`id_CourtCase`) REFERENCES `courtcase` (`id_CourtCase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ccdocument`
--

LOCK TABLES `ccdocument` WRITE;
/*!40000 ALTER TABLE `ccdocument` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `ccdocument` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `ccevent`
--

DROP TABLE IF EXISTS `ccevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ccevent` (
  `EventTime` datetime NOT NULL,
  `EventType` char(1) NOT NULL,
  `id_CCDocument` int(11) NOT NULL,
  `id_CCEvent` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_CCEvent`),
  KEY `RefCCEvent_Document` (`id_CCDocument`),
  CONSTRAINT `RefCCEvent_Document` FOREIGN KEY (`id_CCDocument`) REFERENCES `ccdocument` (`id_CCDocument`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ccevent`
--

LOCK TABLES `ccevent` WRITE;
/*!40000 ALTER TABLE `ccevent` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `ccevent` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `ccpushedevent`
--

DROP TABLE IF EXISTS `ccpushedevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ccpushedevent` (
  `Time_dispatched` datetime NOT NULL,
  `Time_pushed` char(10) DEFAULT NULL,
  `id_CCEvent` int(11) NOT NULL,
  PRIMARY KEY (`id_CCEvent`),
  KEY `RefCCEvent_PushedEvent` (`id_CCEvent`),
  CONSTRAINT `RefCCEvent_PushedEvent` FOREIGN KEY (`id_CCEvent`) REFERENCES `ccevent` (`id_CCEvent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ccpushedevent`
--

LOCK TABLES `ccpushedevent` WRITE;
/*!40000 ALTER TABLE `ccpushedevent` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `ccpushedevent` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `contract`
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract` (
  `id_Contract` int(11) NOT NULL AUTO_INCREMENT,
  `ContractNumber` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_Contract`),
  UNIQUE KEY `byContractNumber` (`ContractNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract`
--

LOCK TABLES `contract` WRITE;
/*!40000 ALTER TABLE `contract` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `contract` VALUES (1,'1'),(2,'2');
/*!40000 ALTER TABLE `contract` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `contractuser`
--

DROP TABLE IF EXISTS `contractuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractuser` (
  `UserName` varchar(70) DEFAULT NULL,
  `id_ContractUser` int(11) NOT NULL AUTO_INCREMENT,
  `id_Contract` int(11) NOT NULL,
  `id_MUser` int(11) NOT NULL,
  PRIMARY KEY (`id_ContractUser`),
  KEY `Ref_ContractUser_Contract` (`id_Contract`),
  KEY `Ref_ContractUser_User` (`id_MUser`),
  CONSTRAINT `Ref_ContractUser_Contract` FOREIGN KEY (`id_Contract`) REFERENCES `contract` (`id_Contract`),
  CONSTRAINT `Ref_ContractUser_User` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractuser`
--

LOCK TABLES `contractuser` WRITE;
/*!40000 ALTER TABLE `contractuser` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `contractuser` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `court`
--

DROP TABLE IF EXISTS `court`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `court` (
  `Name` varchar(70) NOT NULL,
  `id_Court` int(11) NOT NULL AUTO_INCREMENT,
  `CasebookTag` varchar(50) DEFAULT NULL,
  `SearchName` varchar(30) DEFAULT NULL,
  `ShortName` varchar(40) DEFAULT NULL,
  `Address` text,
  `NumberPrefix` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_Court`),
  UNIQUE KEY `byName` (`Name`),
  UNIQUE KEY `byCasebookTag` (`CasebookTag`),
  UNIQUE KEY `bySearchName` (`SearchName`),
  UNIQUE KEY `byShortName` (`ShortName`),
  KEY `byNumberPrefix` (`NumberPrefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `court`
--

LOCK TABLES `court` WRITE;
/*!40000 ALTER TABLE `court` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `court` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `courtcase`
--

DROP TABLE IF EXISTS `courtcase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courtcase` (
  `id_CourtCase` int(11) NOT NULL AUTO_INCREMENT,
  `id_Debtor` int(11) NOT NULL,
  PRIMARY KEY (`id_CourtCase`),
  KEY `RefCourtCase_Debtor` (`id_Debtor`),
  CONSTRAINT `RefCourtCase_Debtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courtcase`
--

LOCK TABLES `courtcase` WRITE;
/*!40000 ALTER TABLE `courtcase` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `courtcase` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `courtcaseparticipant`
--

DROP TABLE IF EXISTS `courtcaseparticipant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courtcaseparticipant` (
  `id_CourtCase` int(11) NOT NULL,
  `id_MUser` int(11) NOT NULL,
  PRIMARY KEY (`id_CourtCase`,`id_MUser`),
  KEY `RefCourtCase_Participant` (`id_CourtCase`),
  KEY `RefMUser_CourtCaseParticipant` (`id_MUser`),
  CONSTRAINT `RefCourtCase_Participant` FOREIGN KEY (`id_CourtCase`) REFERENCES `courtcase` (`id_CourtCase`),
  CONSTRAINT `RefMUser_CourtCaseParticipant` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courtcaseparticipant`
--

LOCK TABLES `courtcaseparticipant` WRITE;
/*!40000 ALTER TABLE `courtcaseparticipant` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `courtcaseparticipant` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `debtor`
--

DROP TABLE IF EXISTS `debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor` (
  `INN` varchar(12) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `OGRN` varbinary(16) DEFAULT NULL,
  `SNILS` varchar(13) DEFAULT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `BankruptId` bigint(20) DEFAULT NULL,
  `TimeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TimeVerified` datetime DEFAULT NULL,
  `VerificationState` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_Debtor`),
  UNIQUE KEY `byBankruptId` (`BankruptId`),
  KEY `byINN` (`INN`),
  KEY `byOGRN` (`OGRN`),
  KEY `bySNILS` (`SNILS`),
  KEY `for_verification` (`VerificationState`,`TimeVerified`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtor`
--

LOCK TABLES `debtor` WRITE;
/*!40000 ALTER TABLE `debtor` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `debtor` VALUES ('322223234','ООО \"Новострой-Технология\"',0x323334323334,NULL,1,NULL,'2020-11-29 09:09:42',NULL,NULL),('230678','Алексашенко А А',0x36383237333934,NULL,2,NULL,'2020-11-29 09:09:42',NULL,NULL),('234788','ООО \"КАРИНА\"',NULL,'234234345634',3,NULL,'2020-11-29 09:09:42',NULL,NULL),('234788345','ООО \"Василёк\"',NULL,NULL,4,NULL,'2020-11-29 09:09:42',NULL,NULL),('234778345','ООО \"Ромашка\"',NULL,NULL,5,NULL,'2020-11-29 09:09:42',NULL,NULL);
/*!40000 ALTER TABLE `debtor` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `doctype`
--

DROP TABLE IF EXISTS `doctype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctype` (
  `id_DocType` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) NOT NULL,
  `NameAddPart` varchar(250) DEFAULT NULL,
  `ExtraParams` longblob NOT NULL,
  PRIMARY KEY (`id_DocType`),
  UNIQUE KEY `byName` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctype`
--

LOCK TABLES `doctype` WRITE;
/*!40000 ALTER TABLE `doctype` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `doctype` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `efrsb_debtor`
--

DROP TABLE IF EXISTS `efrsb_debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb_debtor` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `LegalAdress` varchar(400) DEFAULT NULL,
  `LegalCaseList` longblob,
  `Name` varchar(152) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `Revision` bigint(20) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Debtor`),
  UNIQUE KEY `byBankruptId_1` (`BankruptId`),
  KEY `Ref_Debtor_Manager` (`ArbitrManagerID`),
  KEY `byINN_2` (`INN`),
  KEY `byName_1` (`Name`),
  KEY `byOGRN_1` (`OGRN`),
  KEY `byRevision_1` (`Revision`),
  KEY `bySNILS_1` (`SNILS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `efrsb_debtor`
--

LOCK TABLES `efrsb_debtor` WRITE;
/*!40000 ALTER TABLE `efrsb_debtor` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `efrsb_debtor` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `efrsb_debtor_manager`
--

DROP TABLE IF EXISTS `efrsb_debtor_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb_debtor_manager` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `DateTime_MessageFirst` datetime DEFAULT NULL,
  `DateTime_MessageLast` datetime DEFAULT NULL,
  `Revision` bigint(20) DEFAULT NULL,
  `id_Debtor_Manager` int(11) NOT NULL,
  PRIMARY KEY (`id_Debtor_Manager`),
  KEY `Ref_Debtor_manager_Debtor` (`BankruptId`),
  KEY `Ref_Debtor_manager_Manager` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `efrsb_debtor_manager`
--

LOCK TABLES `efrsb_debtor_manager` WRITE;
/*!40000 ALTER TABLE `efrsb_debtor_manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `efrsb_debtor_manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `efrsb_manager`
--

DROP TABLE IF EXISTS `efrsb_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb_manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `CorrespondenceAddress` varchar(300) DEFAULT NULL,
  `EMail` varchar(100) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `INN` varchar(12) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `OGRNIP` varchar(15) DEFAULT NULL,
  `Phone` varchar(12) DEFAULT NULL,
  `RegNum` varchar(30) DEFAULT NULL,
  `Revision` bigint(20) NOT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `SRORegDate` datetime DEFAULT NULL,
  `SRORegNum` varchar(30) DEFAULT NULL,
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Manager`),
  UNIQUE KEY `byArbitrManagerID_1` (`ArbitrManagerID`),
  KEY `byFirstName` (`FirstName`),
  KEY `byINN_1` (`INN`),
  KEY `byLastName_1` (`LastName`),
  KEY `byMiddleName` (`MiddleName`),
  KEY `byRevision_2` (`Revision`),
  KEY `refManager_SRO` (`SRORegNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `efrsb_manager`
--

LOCK TABLES `efrsb_manager` WRITE;
/*!40000 ALTER TABLE `efrsb_manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `efrsb_manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `efrsb_sro`
--

DROP TABLE IF EXISTS `efrsb_sro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb_sro` (
  `INN` varchar(10) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `RegNum` varchar(30) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `ShortTitle` varchar(250) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `UrAdress` varchar(250) DEFAULT NULL,
  `id_SRO` int(11) NOT NULL AUTO_INCREMENT,
  `SROEmail` varchar(100) DEFAULT NULL,
  `SROPassword` varchar(100) DEFAULT NULL,
  `CEOName` varchar(250) DEFAULT NULL,
  `ExtraFields` longblob,
  PRIMARY KEY (`id_SRO`),
  UNIQUE KEY `byRegNum_1` (`RegNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `efrsb_sro`
--

LOCK TABLES `efrsb_sro` WRITE;
/*!40000 ALTER TABLE `efrsb_sro` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `efrsb_sro` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `emailtosend`
--

DROP TABLE IF EXISTS `emailtosend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailtosend` (
  `id_SentEmail` int(11) NOT NULL,
  `ErrorText` text,
  PRIMARY KEY (`id_SentEmail`),
  KEY `refEmailToSend_SentEmail` (`id_SentEmail`),
  CONSTRAINT `refEmailToSend_SentEmail` FOREIGN KEY (`id_SentEmail`) REFERENCES `sentemail` (`id_SentEmail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailtosend`
--

LOCK TABLES `emailtosend` WRITE;
/*!40000 ALTER TABLE `emailtosend` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `emailtosend` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `ArbitrManagerID` int(11) NOT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `EventTime` datetime NOT NULL,
  `MessageGuid` varchar(32) DEFAULT NULL,
  `MessageNumber` varchar(30) DEFAULT NULL,
  `MessageType` char(1) NOT NULL,
  `id_Event` int(11) NOT NULL AUTO_INCREMENT,
  `Revision` bigint(20) NOT NULL,
  `efrsb_id` int(11) NOT NULL,
  `isActive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_Event`),
  UNIQUE KEY `byRevision` (`Revision`),
  UNIQUE KEY `by_efrsb_id` (`efrsb_id`),
  UNIQUE KEY `byNumber` (`MessageNumber`),
  KEY `RefEvent_Debtor` (`BankruptId`),
  KEY `RefEvent_Manager` (`ArbitrManagerID`),
  KEY `RefEvent_Message` (`efrsb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `fausing`
--

DROP TABLE IF EXISTS `fausing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fausing` (
  `ContractNumber` varchar(40) DEFAULT NULL,
  `IP` varchar(40) NOT NULL,
  `LicenseToken` text,
  `UsingTime` datetime NOT NULL,
  `Section` char(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fausing`
--

LOCK TABLES `fausing` WRITE;
/*!40000 ALTER TABLE `fausing` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `fausing` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `gcevent`
--

DROP TABLE IF EXISTS `gcevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gcevent` (
  `EventTime` datetime NOT NULL,
  `EventType` char(1) NOT NULL,
  `id_GCEvent` int(11) NOT NULL AUTO_INCREMENT,
  `id_GoogleCalendar` int(11) NOT NULL,
  `id_MProcedure` int(11) DEFAULT NULL,
  `Body` longblob,
  `EventTimeEnd` datetime DEFAULT NULL,
  `EventSource` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_GCEvent`),
  KEY `RefGCEvent_Calendar` (`id_GoogleCalendar`),
  KEY `RefGCEvent_MProcedure` (`id_MProcedure`),
  CONSTRAINT `RefGCEvent_Calendar` FOREIGN KEY (`id_GoogleCalendar`) REFERENCES `googlecalendar` (`id_GoogleCalendar`),
  CONSTRAINT `RefGCEvent_MProcedure` FOREIGN KEY (`id_MProcedure`) REFERENCES `mprocedure` (`id_MProcedure`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gcevent`
--

LOCK TABLES `gcevent` WRITE;
/*!40000 ALTER TABLE `gcevent` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `gcevent` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `gcpushedevent`
--

DROP TABLE IF EXISTS `gcpushedevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gcpushedevent` (
  `Time_dispatched` datetime NOT NULL,
  `Time_pushed` datetime DEFAULT NULL,
  `id_GCEvent` int(11) NOT NULL,
  `ErrorText` text,
  PRIMARY KEY (`id_GCEvent`),
  KEY `RefGCPushedEvent_Event` (`id_GCEvent`),
  CONSTRAINT `RefGCPushedEvent_Event` FOREIGN KEY (`id_GCEvent`) REFERENCES `gcevent` (`id_GCEvent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gcpushedevent`
--

LOCK TABLES `gcpushedevent` WRITE;
/*!40000 ALTER TABLE `gcpushedevent` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `gcpushedevent` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `googlecalendar`
--

DROP TABLE IF EXISTS `googlecalendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `googlecalendar` (
  `id_GoogleCalendar` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `refresh_token` varchar(250) NOT NULL,
  `nextSyncToken` varchar(50) DEFAULT NULL,
  `lastUpload` datetime DEFAULT NULL,
  PRIMARY KEY (`id_GoogleCalendar`),
  KEY `RefGoogleCalendar_Manager` (`id_Manager`),
  CONSTRAINT `RefGoogleCalendar_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `googlecalendar`
--

LOCK TABLES `googlecalendar` WRITE;
/*!40000 ALTER TABLE `googlecalendar` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `googlecalendar` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `Description` text,
  `MaxAgeMinutes` int(11) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `id_JobSite` int(11) NOT NULL,
  `id_Job` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Job`),
  UNIQUE KEY `bySiteName` (`id_JobSite`,`Name`),
  KEY `Ref_Job_Site` (`id_JobSite`),
  CONSTRAINT `Ref_Job_Site` FOREIGN KEY (`id_JobSite`) REFERENCES `jobsite` (`id_JobSite`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `joblog`
--

DROP TABLE IF EXISTS `joblog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joblog` (
  `Finished` datetime DEFAULT NULL,
  `Started` datetime DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `id_JobLog` int(11) NOT NULL AUTO_INCREMENT,
  `id_Job` int(11) NOT NULL,
  PRIMARY KEY (`id_JobLog`),
  KEY `Ref_Log_Job` (`id_Job`),
  KEY `byJobStarted` (`id_Job`,`Started`),
  CONSTRAINT `Ref_Log_Job` FOREIGN KEY (`id_Job`) REFERENCES `job` (`id_Job`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joblog`
--

LOCK TABLES `joblog` WRITE;
/*!40000 ALTER TABLE `joblog` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `joblog` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `jobpart`
--

DROP TABLE IF EXISTS `jobpart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobpart` (
  `Description` text,
  `Name` varchar(100) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `id_JobPart` int(11) NOT NULL AUTO_INCREMENT,
  `id_Job` int(11) NOT NULL,
  PRIMARY KEY (`id_JobPart`),
  UNIQUE KEY `byJobName` (`id_Job`,`Name`),
  KEY `Ref_Part_Job` (`id_Job`),
  CONSTRAINT `Ref_Part_Job` FOREIGN KEY (`id_Job`) REFERENCES `job` (`id_Job`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobpart`
--

LOCK TABLES `jobpart` WRITE;
/*!40000 ALTER TABLE `jobpart` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `jobpart` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `jobpartlog`
--

DROP TABLE IF EXISTS `jobpartlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobpartlog` (
  `Finished` datetime DEFAULT NULL,
  `Number` int(11) DEFAULT NULL,
  `Results` text,
  `Started` datetime DEFAULT NULL,
  `Status` char(10) DEFAULT NULL,
  `id_JobLog` int(11) NOT NULL,
  `id_JobPartLog` int(11) NOT NULL AUTO_INCREMENT,
  `id_JobPart` int(11) NOT NULL,
  PRIMARY KEY (`id_JobPartLog`),
  KEY `Ref_JobPart_Job` (`id_JobLog`),
  KEY `Ref_Log_Part` (`id_JobPart`),
  CONSTRAINT `Ref_JobPart_Job` FOREIGN KEY (`id_JobLog`) REFERENCES `joblog` (`id_JobLog`) ON DELETE CASCADE,
  CONSTRAINT `Ref_Log_Part` FOREIGN KEY (`id_JobPart`) REFERENCES `jobpart` (`id_JobPart`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobpartlog`
--

LOCK TABLES `jobpartlog` WRITE;
/*!40000 ALTER TABLE `jobpartlog` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `jobpartlog` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `jobsite`
--

DROP TABLE IF EXISTS `jobsite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobsite` (
  `Description` text,
  `Name` varchar(100) DEFAULT NULL,
  `id_JobSite` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_JobSite`),
  UNIQUE KEY `byName` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobsite`
--

LOCK TABLES `jobsite` WRITE;
/*!40000 ALTER TABLE `jobsite` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `jobsite` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `leak`
--

DROP TABLE IF EXISTS `leak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leak` (
  `id_Leak` int(11) NOT NULL AUTO_INCREMENT,
  `id_PData` int(11) NOT NULL,
  `LeakSum` decimal(10,0) NOT NULL,
  `LeakDate` datetime DEFAULT NULL,
  `ContragentName` varchar(100) DEFAULT NULL,
  `ContragentInn` char(12) DEFAULT NULL,
  `isAccredited` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id_Leak`),
  KEY `refLeak_PData` (`id_PData`),
  CONSTRAINT `refLeak_PData` FOREIGN KEY (`id_PData`) REFERENCES `pdata` (`id_PData`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leak`
--

LOCK TABLES `leak` WRITE;
/*!40000 ALTER TABLE `leak` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `leak` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `log_type`
--

DROP TABLE IF EXISTS `log_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_type` (
  `Name` varchar(100) NOT NULL,
  `id_Log_type` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Log_type`),
  UNIQUE KEY `byName` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_type`
--

LOCK TABLES `log_type` WRITE;
/*!40000 ALTER TABLE `log_type` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `log_type` VALUES ('auth/customer',17),('auth/customer/ama',19),('auth/manager',16),('auth/manager/ama',20),('auth/partner',18),('constraint/api/customer/error',32),('constraint/api/error',33),('constraint/api/manager/error',31),('electrokk/create/ama',27),('electrokk/create/ama/error',30),('electrokk/open/ama',28),('electrokk/open/ama/error',29),('electrokk/open/ama/ok',41),('login',13),('login/admin',21),('login/ama/error',40),('login/customer',12),('login/customer/ama',14),('login/customer/ama/error',38),('login/customer/android',6),('login/customer/ios',9),('login/customer/web',3),('login/debtor',37),('login/debtor/android',36),('login/debtor/ios',35),('login/debtor/web',34),('login/manager',10),('login/manager/ama',15),('login/manager/ama/error',39),('login/manager/android',4),('login/manager/ios',7),('login/manager/web',1),('login/sro',25),('login/viewer',11),('login/viewer/android',5),('login/viewer/fa',22),('login/viewer/ios',8),('login/viewer/web',2),('lytdybr',-1),('push/unsubscribe',24),('robot/found-start-case',26),('sublevel-info',23);
/*!40000 ALTER TABLE `log_type` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `id_Contract` int(11) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  `efrsbNumber` varchar(50) DEFAULT NULL,
  `ManagerEmail` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `ExtraParams` longblob,
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `BankroTechAcc` text,
  `TimeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TimeVerified` datetime DEFAULT NULL,
  `VerificationState` char(1) DEFAULT NULL,
  `BankroTechAccVerified` bit(1) DEFAULT NULL,
  `HasIncoming` bit(1) NOT NULL DEFAULT b'0',
  `id_SRO` int(11) DEFAULT NULL,
  `ManagerEmail_Change` varchar(70) DEFAULT NULL,
  `ManagerEmail_ChangeTime` datetime DEFAULT NULL,
  `ManagerPassword_Change` varchar(30) DEFAULT NULL,
  `ManagerPassword_ChangeTime` datetime DEFAULT NULL,
  `ProtocolNum` varchar(50) DEFAULT NULL,
  `ProtocolDate` datetime DEFAULT NULL,
  `id_Contract_Change` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Manager`),
  UNIQUE KEY `byEfrsbNumber` (`efrsbNumber`),
  UNIQUE KEY `byEMailPassword` (`ManagerEmail`,`Password`),
  UNIQUE KEY `byEMail` (`ManagerEmail`),
  UNIQUE KEY `byArbitrManagerID` (`ArbitrManagerID`),
  UNIQUE KEY `byINN` (`INN`),
  UNIQUE KEY `byManagerEmail_Change` (`ManagerEmail_Change`),
  KEY `refManager_Contract` (`id_Contract`),
  KEY `Ref_Manager_SRO` (`id_SRO`),
  CONSTRAINT `Ref_Manager_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `efrsb_sro` (`id_SRO`),
  CONSTRAINT `refManager_Contract` FOREIGN KEY (`id_Contract`) REFERENCES `contract` (`id_Contract`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager`
--

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `manager` VALUES (1,1,'Иван','Иванов','Иванович','123','x@y.com','1',NULL,NULL,NULL,NULL,'2020-11-29 09:09:42',NULL,NULL,NULL,0x00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,1,'Петр','Петров','Петрович','124','z@w.com','2',NULL,NULL,NULL,NULL,'2020-11-29 09:09:42',NULL,NULL,NULL,0x00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,2,'Сидор','Сидоров','Сидорович','125','n@m.com','3',NULL,NULL,NULL,NULL,'2020-11-29 09:09:42',NULL,NULL,NULL,0x00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `managercertauth`
--

DROP TABLE IF EXISTS `managercertauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `managercertauth` (
  `Auth` varchar(32) DEFAULT NULL,
  `Body` longblob,
  `TimeStarted` datetime NOT NULL,
  `id_ManagerCertAuth` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ManagerCertAuth`),
  KEY `Ref_AuthByCert_Manager` (`id_Manager`),
  CONSTRAINT `Ref_AuthByCert_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `managercertauth`
--

LOCK TABLES `managercertauth` WRITE;
/*!40000 ALTER TABLE `managercertauth` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `managercertauth` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `managerdocument`
--

DROP TABLE IF EXISTS `managerdocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `managerdocument` (
  `Body` longblob NOT NULL,
  `DocumentType` char(1) DEFAULT NULL,
  `FileName` varchar(100) NOT NULL,
  `id_ManagerDocument` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  `PubLock` longblob,
  PRIMARY KEY (`id_ManagerDocument`),
  KEY `RefManager_Document` (`id_Manager`),
  CONSTRAINT `RefManager_Document` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `managerdocument`
--

LOCK TABLES `managerdocument` WRITE;
/*!40000 ALTER TABLE `managerdocument` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `managerdocument` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `manageruser`
--

DROP TABLE IF EXISTS `manageruser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manageruser` (
  `id_ManagerUser` int(11) NOT NULL AUTO_INCREMENT,
  `id_MUser` int(11) NOT NULL,
  `id_Manager` int(11) NOT NULL,
  `UserName` varchar(70) DEFAULT NULL,
  `DefaultViewer` bit(1) NOT NULL,
  PRIMARY KEY (`id_ManagerUser`),
  UNIQUE KEY `byMUserManager` (`id_MUser`,`id_Manager`),
  KEY `RefManagerUser_Manager` (`id_Manager`),
  KEY `RefManagerUser_MUser` (`id_MUser`),
  CONSTRAINT `RefManagerUser_MUser` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`),
  CONSTRAINT `RefManagerUser_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manageruser`
--

LOCK TABLES `manageruser` WRITE;
/*!40000 ALTER TABLE `manageruser` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `manageruser` VALUES (1,1,1,'Сергеева Светлана Михайловна (Банк Возрождение)',0x00),(2,2,1,'Селянин И О',0x01),(3,3,1,'Алексеев О А (ФНС)',0x00),(4,4,2,'Осин Я Я',0x00),(5,3,2,'Алексеев О А (ФНС)',0x00);
/*!40000 ALTER TABLE `manageruser` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mdata`
--

DROP TABLE IF EXISTS `mdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdata` (
  `ContentHash` varchar(250) DEFAULT NULL,
  `MData_Type` char(1) NOT NULL,
  `fileData` longblob NOT NULL,
  `id_Debtor` int(11) DEFAULT NULL,
  `id_MData` int(11) NOT NULL AUTO_INCREMENT,
  `id_ManagerUser` int(11) NOT NULL,
  `publicDate` datetime NOT NULL,
  `revision` int(11) NOT NULL,
  PRIMARY KEY (`id_MData`),
  KEY `refMData_Debtor` (`id_Debtor`),
  KEY `refMData_ManagerUser` (`id_ManagerUser`),
  CONSTRAINT `refMData_Debtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `refMData_ManagerUser` FOREIGN KEY (`id_ManagerUser`) REFERENCES `manageruser` (`id_ManagerUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdata`
--

LOCK TABLES `mdata` WRITE;
/*!40000 ALTER TABLE `mdata` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mdata` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mdata_attachment`
--

DROP TABLE IF EXISTS `mdata_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdata_attachment` (
  `id_MData_attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_MData` int(11) NOT NULL,
  `FileName` varchar(50) DEFAULT NULL,
  `Content` longblob,
  PRIMARY KEY (`id_MData_attachment`),
  KEY `refAttachment_MData` (`id_MData`),
  CONSTRAINT `refAttachment_MData` FOREIGN KEY (`id_MData`) REFERENCES `mdata` (`id_MData`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdata_attachment`
--

LOCK TABLES `mdata_attachment` WRITE;
/*!40000 ALTER TABLE `mdata_attachment` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mdata_attachment` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `meeting`
--

DROP TABLE IF EXISTS `meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting` (
  `id_Meeting` int(11) NOT NULL AUTO_INCREMENT,
  `id_MProcedure` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Body` longblob NOT NULL,
  `State` char(1) NOT NULL DEFAULT 'a',
  PRIMARY KEY (`id_Meeting`),
  KEY `refMeeting_MProcedure` (`id_MProcedure`),
  CONSTRAINT `refMeeting_MProcedure` FOREIGN KEY (`id_MProcedure`) REFERENCES `mprocedure` (`id_MProcedure`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meeting`
--

LOCK TABLES `meeting` WRITE;
/*!40000 ALTER TABLE `meeting` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `meeting` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `meeting_document`
--

DROP TABLE IF EXISTS `meeting_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_document` (
  `Body` longblob,
  `FileName` varchar(40) NOT NULL,
  `Meeting_document_time` datetime NOT NULL,
  `Parameters` longblob,
  `id_Meeting_document` int(11) NOT NULL AUTO_INCREMENT,
  `id_Meeting` int(11) NOT NULL,
  `DocumentName` varchar(250) NOT NULL,
  PRIMARY KEY (`id_Meeting_document`),
  KEY `refMeeting_document_Meeting` (`id_Meeting`),
  CONSTRAINT `refMeeting_document_Meeting` FOREIGN KEY (`id_Meeting`) REFERENCES `meeting` (`id_Meeting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meeting_document`
--

LOCK TABLES `meeting_document` WRITE;
/*!40000 ALTER TABLE `meeting_document` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `meeting_document` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `Body` longblob NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `MessageGUID` varchar(32) DEFAULT NULL,
  `MessageInfo_MessageType` char(1) DEFAULT NULL,
  `Number` varchar(30) DEFAULT NULL,
  `OGRN` varchar(20) DEFAULT NULL,
  `Revision` bigint(20) NOT NULL,
  `PublishDate` datetime NOT NULL,
  `SNILS` varchar(20) DEFAULT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Message` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Message`),
  UNIQUE KEY `by_efrsb_id_1` (`efrsb_id`),
  UNIQUE KEY `byRevision` (`Revision`),
  KEY `refMessage_Debtor` (`BankruptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_crm2_contract`
--

DROP TABLE IF EXISTS `mock_crm2_contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_crm2_contract` (
  `Id` varchar(40) NOT NULL,
  `ContractNumber` varchar(20) DEFAULT NULL,
  `ContractDate` datetime DEFAULT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  `AMAIsTrial` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_crm2_contract`
--

LOCK TABLES `mock_crm2_contract` WRITE;
/*!40000 ALTER TABLE `mock_crm2_contract` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `mock_crm2_contract` VALUES ('1','1',NULL,NULL,'1',NULL),('2','2',NULL,NULL,'2',NULL),('3','3',NULL,NULL,'3',NULL),('4','4',NULL,NULL,'3',1);
/*!40000 ALTER TABLE `mock_crm2_contract` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_debtor`
--

DROP TABLE IF EXISTS `mock_efrsb_debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_debtor` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `Name` varchar(152) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `Body` longblob NOT NULL,
  `LastMessageDate` datetime DEFAULT NULL,
  `Revision` bigint(20) NOT NULL,
  PRIMARY KEY (`id_Debtor`),
  UNIQUE KEY `byBankruptId` (`BankruptId`),
  KEY `byArbitrManagerID` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_debtor`
--

LOCK TABLES `mock_efrsb_debtor` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_debtor` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_debtor` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_debtor_manager`
--

DROP TABLE IF EXISTS `mock_efrsb_debtor_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_debtor_manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `id_Debtor_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `Revision` bigint(20) NOT NULL,
  PRIMARY KEY (`id_Debtor_Manager`),
  KEY `byBankruptId` (`BankruptId`),
  KEY `debtor_manager_byArbitrManagerID` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_debtor_manager`
--

LOCK TABLES `mock_efrsb_debtor_manager` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_debtor_manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_debtor_manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_email`
--

DROP TABLE IF EXISTS `mock_efrsb_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_email` (
  `address` varchar(50) NOT NULL,
  `id_Email` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email`),
  UNIQUE KEY `byAddress` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_email`
--

LOCK TABLES `mock_efrsb_email` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_email` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_email` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_email_manager`
--

DROP TABLE IF EXISTS `mock_efrsb_email_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_email_manager` (
  `id_Email` int(11) NOT NULL,
  `id_Manager` int(11) NOT NULL,
  `id_Message_Last` int(11) DEFAULT NULL,
  `id_Messages` longblob NOT NULL,
  PRIMARY KEY (`id_Email`,`id_Manager`),
  KEY `RefEmail_ManagerEmail` (`id_Email`),
  KEY `RefEmail_ManagerManager` (`id_Manager`),
  KEY `RefEmail_Manager_MessageLast` (`id_Message_Last`),
  CONSTRAINT `RefEmail_ManagerEmail` FOREIGN KEY (`id_Email`) REFERENCES `mock_efrsb_email` (`id_Email`),
  CONSTRAINT `RefEmail_ManagerManager` FOREIGN KEY (`id_Manager`) REFERENCES `mock_efrsb_manager` (`id_Manager`),
  CONSTRAINT `RefEmail_Manager_MessageLast` FOREIGN KEY (`id_Message_Last`) REFERENCES `mock_efrsb_message` (`id_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_email_manager`
--

LOCK TABLES `mock_efrsb_email_manager` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_email_manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_email_manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_event`
--

DROP TABLE IF EXISTS `mock_efrsb_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_event` (
  `ArbitrManagerID` int(11) NOT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `EventTime` datetime NOT NULL,
  `MessageInfo_MessageType` char(1) NOT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Event` int(11) NOT NULL AUTO_INCREMENT,
  `Revision` bigint(20) NOT NULL,
  `isActive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_Event`),
  KEY `Ref2539` (`ArbitrManagerID`),
  KEY `byEventTime` (`EventTime`),
  KEY `refByEfrsb_id` (`efrsb_id`),
  KEY `refEfrsb_event_Debtor` (`BankruptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_event`
--

LOCK TABLES `mock_efrsb_event` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_event` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_event` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_manager`
--

DROP TABLE IF EXISTS `mock_efrsb_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `FirstName` varchar(50) NOT NULL,
  `INN` varchar(12) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `OGRNIP` varchar(12) DEFAULT NULL,
  `RegNum` varchar(30) DEFAULT NULL,
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `Body` longblob NOT NULL,
  `DateDelete` datetime DEFAULT NULL,
  `SRORegNum` varchar(30) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `CorrespondenceAddress` varchar(300) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `SRORegDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id_Manager`),
  UNIQUE KEY `byArbitrManagerID` (`ArbitrManagerID`),
  KEY `refManager_SRO` (`SRORegNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_manager`
--

LOCK TABLES `mock_efrsb_manager` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_message`
--

DROP TABLE IF EXISTS `mock_efrsb_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_message` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `Body` longblob NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `MessageGUID` varchar(32) DEFAULT NULL,
  `MessageInfo_MessageType` char(1) DEFAULT NULL,
  `Number` varchar(30) DEFAULT NULL,
  `OGRN` varchar(20) DEFAULT NULL,
  `PublishDate` datetime NOT NULL,
  `SNILS` varchar(20) DEFAULT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Message` int(11) NOT NULL AUTO_INCREMENT,
  `Revision` bigint(20) NOT NULL,
  PRIMARY KEY (`id_Message`),
  UNIQUE KEY `by_efrsb_id` (`efrsb_id`),
  KEY `refByArbitrManagerID` (`ArbitrManagerID`),
  KEY `refByBankruptId` (`BankruptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_message`
--

LOCK TABLES `mock_efrsb_message` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_message` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_message` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_sro`
--

DROP TABLE IF EXISTS `mock_efrsb_sro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_sro` (
  `Body` longblob NOT NULL,
  `DateLastModif` datetime NOT NULL,
  `INN` varchar(10) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `RegNum` varchar(30) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `ShortTitle` varchar(250) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `UrAdress` varchar(250) DEFAULT NULL,
  `id_SRO` int(11) NOT NULL,
  PRIMARY KEY (`id_SRO`),
  UNIQUE KEY `byRegNum` (`RegNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_sro`
--

LOCK TABLES `mock_efrsb_sro` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_sro` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_sro` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mprocedure`
--

DROP TABLE IF EXISTS `mprocedure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mprocedure` (
  `id_MProcedure` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  `casenumber` varchar(60) DEFAULT NULL,
  `features` char(1) DEFAULT NULL,
  `procedure_type` char(1) NOT NULL,
  `registrationDate` datetime DEFAULT NULL,
  `publicDate` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `ctb_publicDate` datetime DEFAULT NULL,
  `ctb_revision` bigint(20) NOT NULL DEFAULT '0',
  `Content_hash` varchar(250) DEFAULT NULL,
  `ExtraParams` longblob,
  `id_Debtor` int(11) NOT NULL,
  `id_Debtor_Manager` int(11) DEFAULT NULL,
  `ctb_bankrotech_id` varchar(45) DEFAULT NULL,
  `ctb_revision_posted` bigint(20) DEFAULT NULL,
  `ctb_update_error` bit(1) NOT NULL DEFAULT b'0',
  `last_update_finish` datetime DEFAULT NULL,
  `last_update_log` longblob,
  `last_update_start` datetime DEFAULT NULL,
  `TimeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TimeVerified` datetime DEFAULT NULL,
  `VerificationState` char(1) DEFAULT NULL,
  `ctb_account_limit` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_MProcedure`),
  UNIQUE KEY `NaturalKey` (`casenumber`,`procedure_type`,`id_Manager`,`id_Debtor`),
  KEY `byCtbRevision` (`ctb_revision`),
  KEY `RefMProcedure_Manager` (`id_Manager`),
  KEY `Debtor_MProcedure` (`id_Debtor`),
  KEY `by_id_Message1` (`id_Debtor_Manager`),
  KEY `by_ctb_bankrotech_id` (`ctb_bankrotech_id`),
  CONSTRAINT `Debtor_MProcedure` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `RefMProcedure_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mprocedure`
--

LOCK TABLES `mprocedure` WRITE;
/*!40000 ALTER TABLE `mprocedure` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `mprocedure` VALUES (1,1,'А81-2868/2018',NULL,'n',NULL,'2018-05-28 00:00:00',1,NULL,0,NULL,NULL,1,NULL,NULL,NULL,0x00,NULL,NULL,NULL,'2020-11-29 09:09:42',NULL,NULL,0x00),(2,1,'А16-601/2017',NULL,'i',NULL,'2018-05-28 00:00:00',2,'2018-05-28 00:00:00',2,NULL,NULL,1,NULL,NULL,NULL,0x00,NULL,NULL,NULL,'2020-11-29 09:09:42',NULL,NULL,0x00),(3,1,'А16-936/2014',NULL,'k',NULL,'2018-05-28 00:00:00',3,NULL,0,NULL,NULL,1,NULL,NULL,NULL,0x00,NULL,NULL,NULL,'2020-11-29 09:09:42',NULL,NULL,0x00),(4,2,'А16-976/2014',NULL,'n',NULL,'2018-05-28 00:00:00',4,'2018-05-28 00:00:00',4,NULL,NULL,4,NULL,NULL,NULL,0x00,NULL,NULL,NULL,'2020-11-29 09:09:42',NULL,NULL,0x00),(5,4,'А18-936/2014',NULL,'n',NULL,'2018-05-28 00:00:00',5,NULL,0,NULL,NULL,5,NULL,NULL,NULL,0x00,NULL,NULL,NULL,'2020-11-29 09:09:42',NULL,NULL,0x00);
/*!40000 ALTER TABLE `mprocedure` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mprocedureuser`
--

DROP TABLE IF EXISTS `mprocedureuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mprocedureuser` (
  `id_MProcedure` int(11) NOT NULL,
  `id_ManagerUser` int(11) NOT NULL,
  `id_Request` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_MProcedure`,`id_ManagerUser`),
  KEY `RefMProcedureUser_MProcedure` (`id_MProcedure`),
  KEY `RefMProcedureUser_ManagerUser` (`id_ManagerUser`),
  KEY `refMProcedureUser_Request` (`id_Request`),
  CONSTRAINT `RefMProcedureUser_MProcedure` FOREIGN KEY (`id_MProcedure`) REFERENCES `mprocedure` (`id_MProcedure`),
  CONSTRAINT `RefMProcedureUser_ManagerUser` FOREIGN KEY (`id_ManagerUser`) REFERENCES `manageruser` (`id_ManagerUser`) ON DELETE CASCADE,
  CONSTRAINT `refMProcedureUser_Request` FOREIGN KEY (`id_Request`) REFERENCES `request` (`id_Request`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mprocedureuser`
--

LOCK TABLES `mprocedureuser` WRITE;
/*!40000 ALTER TABLE `mprocedureuser` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `mprocedureuser` VALUES (1,1,NULL),(2,1,NULL),(2,2,NULL),(3,1,NULL),(3,2,NULL),(3,3,NULL),(4,3,NULL),(4,4,NULL);
/*!40000 ALTER TABLE `mprocedureuser` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mrequest`
--

DROP TABLE IF EXISTS `mrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mrequest` (
  `CaseNumber` varchar(50) DEFAULT NULL,
  `DateOfApplication` datetime DEFAULT NULL,
  `DebtorINN` varchar(12) DEFAULT NULL,
  `DebtorName` varchar(250) DEFAULT NULL,
  `DebtorOGRN` varbinary(16) DEFAULT NULL,
  `DebtorSNILS` varchar(13) DEFAULT NULL,
  `id_Court` int(11) NOT NULL,
  `id_MRequest` int(11) NOT NULL AUTO_INCREMENT,
  `id_SRO` int(11) NOT NULL,
  `DateOfRequest` datetime NOT NULL,
  `DateOfResponce` datetime DEFAULT NULL,
  `DebtorCategory` char(1) NOT NULL DEFAULT 'n',
  `NextSessionDate` datetime DEFAULT NULL,
  `id_Manager` int(11) DEFAULT NULL,
  `DebtorAddress` varchar(250) DEFAULT NULL,
  `ApplicantName` varchar(250) DEFAULT NULL,
  `ApplicantINN` varchar(12) DEFAULT NULL,
  `ApplicantSNILS` varchar(13) DEFAULT NULL,
  `ApplicantOGRN` varbinary(16) DEFAULT NULL,
  `ApplicantAddress` varchar(250) DEFAULT NULL,
  `ApplicantCategory` char(1) DEFAULT NULL,
  `DateOfRequestAct` datetime DEFAULT NULL,
  `DateOfOffer` datetime DEFAULT NULL,
  `DateOfOfferTill` datetime DEFAULT NULL,
  `DateOfCreation` datetime NOT NULL,
  `CourtDecisionURL` varchar(250) DEFAULT NULL,
  `CourtDecisionAddInfo` varchar(250) DEFAULT NULL,
  `DebtorName2` varchar(250) DEFAULT NULL,
  `DebtorINN2` varchar(12) DEFAULT NULL,
  `DebtorSNILS2` varchar(13) DEFAULT NULL,
  `DebtorOGRN2` varbinary(16) DEFAULT NULL,
  `DebtorAddress2` varchar(250) DEFAULT NULL,
  `DebtorCategory2` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_MRequest`),
  KEY `Ref_Request_Court` (`id_Court`),
  KEY `Ref_Request_SRO` (`id_SRO`),
  KEY `Ref_MRequest_Manager` (`id_Manager`),
  CONSTRAINT `Ref_MRequest_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `Ref_Request_Court` FOREIGN KEY (`id_Court`) REFERENCES `court` (`id_Court`),
  CONSTRAINT `Ref_Request_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `efrsb_sro` (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mrequest`
--

LOCK TABLES `mrequest` WRITE;
/*!40000 ALTER TABLE `mrequest` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mrequest` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `muser`
--

DROP TABLE IF EXISTS `muser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muser` (
  `id_MUser` int(11) NOT NULL AUTO_INCREMENT,
  `UserEmail` varchar(70) DEFAULT NULL,
  `UserPassword` varchar(30) DEFAULT NULL,
  `UserName` varchar(70) DEFAULT NULL,
  `Confirmed` bit(1) NOT NULL DEFAULT b'0',
  `UserEmail_Change` varchar(70) DEFAULT NULL,
  `UserEmail_ChangeTime` datetime DEFAULT NULL,
  `UserPassword_Change` varchar(30) DEFAULT NULL,
  `UserPassword_ChangeTime` datetime DEFAULT NULL,
  `Monitoring` longblob,
  `UserPhone` varchar(12) DEFAULT NULL,
  `HasOutcoming` bit(1) NOT NULL DEFAULT b'0',
  `CreatedTime` datetime NOT NULL DEFAULT '2020-12-30 00:00:00',
  `Confirmation_Token` char(4) DEFAULT NULL,
  `Confirmation_Token_Cookie` char(10) DEFAULT NULL,
  `UserExecutorName` varchar(250) DEFAULT NULL,
  `UserINN` varchar(12) DEFAULT NULL,
  `UserOGRNIP` varchar(15) DEFAULT NULL,
  `UserKPP` varchar(9) DEFAULT NULL,
  `UserBIK` varchar(9) DEFAULT NULL,
  `UserCheckingAccount` varchar(20) DEFAULT NULL,
  `UserOKPO` varchar(14) DEFAULT NULL,
  `UserOfficialAddress` varchar(250) DEFAULT NULL,
  `UserMailingAddress` varchar(250) DEFAULT NULL,
  `UserContactPhone` varchar(12) DEFAULT NULL,
  `UserTelegram` varchar(12) DEFAULT NULL,
  `UserWhatsApp` varchar(12) DEFAULT NULL,
  `UserViber` varchar(12) DEFAULT NULL,
  `UserContactEmail` varchar(70) DEFAULT NULL,
  `UserPhoto` longblob,
  `UserPhotoName` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_MUser`),
  UNIQUE KEY `byUserEmail` (`UserEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `muser`
--

LOCK TABLES `muser` WRITE;
/*!40000 ALTER TABLE `muser` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `muser` VALUES (1,'aaa@oo.uu','234','Сергеева Светлана Михайловна (Банк Возрождение)',0x00,NULL,NULL,NULL,NULL,NULL,NULL,0x00,'2020-12-30 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'eee@aa.uu','567','Селянин И О',0x00,NULL,NULL,NULL,NULL,NULL,NULL,0x00,'2020-12-30 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'oo@ee.uu','891','Алексеев О А (ФНС)',0x00,NULL,NULL,NULL,NULL,NULL,NULL,0x00,'2020-12-30 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'kk@ee.uu','527','Осин Я Я',0x00,NULL,NULL,NULL,NULL,NULL,NULL,0x00,'2020-12-30 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `muser` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `pdata`
--

DROP TABLE IF EXISTS `pdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdata` (
  `id_PData` int(11) NOT NULL AUTO_INCREMENT,
  `id_MProcedure` int(11) NOT NULL,
  `publicDate` datetime NOT NULL,
  `fileData` longblob NOT NULL,
  `revision` bigint(20) NOT NULL,
  `ctb_allowed` bit(1) NOT NULL DEFAULT b'0',
  `Content_hash` varchar(250) DEFAULT NULL,
  `Processed` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id_PData`),
  UNIQUE KEY `by_id_MProcedure_revision` (`id_MProcedure`,`revision`),
  KEY `RefPData_MProcedure` (`id_MProcedure`),
  KEY `byProcessedRevision` (`Processed`,`revision`),
  CONSTRAINT `RefPData_MProcedure` FOREIGN KEY (`id_MProcedure`) REFERENCES `mprocedure` (`id_MProcedure`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdata`
--

LOCK TABLES `pdata` WRITE;
/*!40000 ALTER TABLE `pdata` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `pdata` VALUES (1,1,'2018-05-28 00:00:00',0x504B030414000000080005A0D34C884F6DA584070000582B00000C00000072656769737472792E786D6CED5ADB6E5347147D4EA5FEC3919F5A09EC73B58FA570F883FE8285685A21955095B66ADF9C8412A4A012A02A882A2629BCF4CD903831BEFEC29C5FE80FF417BAF69E7399F1B9D8094EE82520057BCF9E7DDF6B6636F96B385DBDFEC3EDAF8CEFD7BEB97BEBCEFAB58A55352BC6DAFACD3B9FDF5AFFF25AE5BB6FBFB8EA57AE071F7FB42A3A622A7A615B74C34DD1176FC5584C5AF455BC1193701BE4472DD0C7E13D7C6D8B1108F7459F8860DB04A5478CD8D7C54ED0C39F5A62C0E4237CDDE44D583660CEFADD6B95DB3F7E76E3F6DADDAF6FDC5CAB40FDCAAA789E277B2CBA2D08804DE15660C27AA76A9B96BF5A5B809DA5FE02A543714C3689019140DB838D7D6CEB918FB1C5A2172004F86B54C40B2287ED70A7024545DC5256473C15FB622F704CC777EB96E5D94DA78E5D315D723D8794BDC0F51DAF51B7EDA6CDF6EFC9657CCE1849364E4867D86E851BE1167CA2F092018788377F1D8A6E20761BD655CB759BF51AC2E2B0B58BEC6315BF43DBB425A694180410890AB7C8E9805D7E2386E1CFBC81DD85E8427E96D60937A0E50DF3431FB4A38C028A44EE02EF79A1D7871495A5339555B4E1868CFF499C74B9B84856F7390A5D43F40D31C0E769B803F9DD793926D9BBF01526851B816BD74DD3BC628857B06FC401D8848303EA0F032AC003C15B143E4AA6E882F5302AFE2303593F8692B7B4030B07ACACA748B06C4A62AA2E36601F2207CC3D89E28244860FA375AEB01162817FA4F1E0E41A1B250102CF6B2C8D60D590E316F3824F5F48F83B306C9BCC803A28D6A483739B93ABB344F6D6E6189CF64DC36C3A0DDBADA3294C73A66FD2CEF16CD377EA7EDD33D5CE61452545A1E40D654B2846F584E8EF009C60DF5B2E32EA976154E6EF2E38BF0B9B15F943C034A6A205B46EE2D38EE8B5646821ED1E2FEE047ED3B2A1C7F5EA8D2B46FA854073A1FD91AEC71C54CE219BDF275EEACB01651FB1465263EE943DAD9E927EDCE57EFC8321A42D21217C38BF0F6500C276151BB6C3C7E12650D732ADBAEB50CEF84FD36B483755AE64F763E4E74560BAAE677B92535292AAD59D9841611606F0744DDF6AF81694359BF869BA8E65CF226FCC1B57E969820964CE1EA789499935353C1D6E46064FF49AC3EDA152124614B2ECDA185D99FA04F2A8C039FC099D4FD12EBB6336A313D8E153AB9BFA18EBDFC8667B1AA1792FA9E79E412782111D065DB0C72B80666AE2472989DA6E62B0108AD338920CC623F5D4245C97C64BA113DC4022CCE13A1F199F20689C042E39EAB8D1A7069F4B27248BD270ACA80C370DCDDBAC6B49DC6A0581037A71B2719052160A835AAF9A2EA971170EEA01C78302C78E537CE4E1163ED00299C4483B7CE779536835E17AB670124F5A8CF6275142A4F828A0ECE46CE594F12B95DA050B37562081ACDAF0D990949E303F81A4A31615174AE884B5700944E7132E4CA9E3F4F35D5C45879CC4399B9536D3A2979ACBB72A34B52C41310AF86A7728CFC1B0CD994205E350D9523355A8D2F8F3DEAF3377BC9CD2D455A6A690EE010E14C65305ACF316D3A5388CB3B6C314EBD40E54E3B86AF213A82CBBC06A3B1604A05AA15BD9A5341CD97E40468D24DB2FF5D6D2B23DB3A6F45101C896C0ACE6A717F9998B0905A8F04C474E86592E70D8D0E6471C6D1849B018CA2F357EDA312CE8EB3360A25EC9BBA5005202887320510B8055068AF901408536AF187CB37AA08480DA459536C7F432CCAEE5E754C772A704CB0FB8FEF0620D2CD76A36BCAA4337DA945A0CFB7463FF800956ABBE9653F62B692F15F6CB2A9D5B20D07B70A03E24C7F202DC220469D1CB1BB411BDE767DE9A6AE3A99BC39DC5B7BE675FDAA72FCB82B4E5E4201311E632D4394AB87511CDE79CA9F92CAFA0FB9C73EEBE83E81639A0BEF2B8A15282C2F792AFC27D7AAAF1AC083AA81A8719608FDE2FD43A589DE8695717D40D1DBEDF8F794D7AA63B9E59360CD5B7B9C6ADE4E388EF597ED5B3351859390B8E2CAF20C95843858B33362BF9B41CC428BB0C95DC8592AB50CEAB2B7BB359E507FF11E7F138C9323B9125175F4C4E65D82B7EEB10B0C7A9CC1BC6FE336C7D42B77D7EB71C2B63C58C611DB8143F6F3293AE95E50E70D3DE9D3FC64DEF8AA5F62D39B2251757BAFA174D03F2D62280A30149CE80B570ECFA3A1A50F09C8F41ACD7E251E1FD74E007F4EAD2648CC7B8F1648926F1F4EEA42FEA1D80710EC6345D06C694A0CC8E26DA934F7B1780C388A193717321CD0B8C484F39217D9F01299CDE03F76FE220B0EB966F379ABE6BCA60C4747D02EADB1E1801F58E9D9D81CECFD0071F85E6C9B8D8616853198636CCCB61E8F90C433DD7762E87A197C3D0CB61E8B90F43B9C7ABFCBF66FFCE6128DD69A67478D4E4B59D4FED6364AB70EA695511BA0B9D7A161AB9ECC1A6EADA7F79B069552DABD0CFFFC360D3394300CE11E29630F32C4DA936AB50206BC199676AE6D2AA401139AF1EF467DF390C449FC93992041CF974990BB9EAB63903E3FDE86DD49777EEA47286B8896F273EF36F0A2117E18E2CA2779496528FF5A388A730A91BF12F1BF12FFDB0FA5EAC0C22ACBC43EB5412D4815E942C12A283F9CCC9B38BE8F24796D9A7B01A786551C3202A4FB9B9C61102E41F2CF11C2BAB0E0B997CA8B445526B189783AACB41D5E5A06A81415596CA7A380617F11BACD0F537504B0102000014000000080005A0D34C884F6DA584070000582B00000C000000000000000000000000000000000072656769737472792E786D6C504B050600000000010001003A000000AE0700000000,1,0x00,NULL,NULL),(2,2,'2018-05-28 00:00:00',0x504B030414000000080011A0D34C93BDEDF66E070000BB2900000C00000072656769737472792E786D6CED5ADD6E134714BEA652DF61E5AB564AECFDB7570A8B0214A917ED2B5888A61552095569ABF6CE7180801295001545540949E1A67786C489891DFB15665FA12FD057E877CEECCFACF7C74E9B044A0352B067CE9CFFF39D9913FEEA8FE62EFC78E36BED87856F6F5DBFB978BE6254F58AB6B078EDE617D717BF3A5FF9FEBB2F671B950BFE871FCC894D3112DDA0253A415BF4C46B7128864DFA2A5E8961B082E5074DAC1F06B7F1B5250658B82B7AB408B23656BA4488731D9CC47A70A7290E7879175FDB7C08DB1AD459BC75BE72E3A7CFAFDE58B8F5CDD56B0B15883F3727B6833B20065FDF9CAB29DF78F3699EE043D169823B140E967D1DA6595553371A383D999CB9FE028DFA628F141607B484B50D18D0C3B12E39203247747DF8077FB58A7846CB412B58AD405011B5E4B5291E8B2DB1E15BBAD5B05DC3704CCF72712A5A97544FC165C3B71B9653774DD33359FF0DB98DCF192549C721C90C5ACD602958864DE47B526007C1E0AF7D384EACD78D59C3B63DB706B758ACED34E758C4EF90366A8A11450D0E4414836532DA67935F897EF0331F6073C1BA909EB96D064B90F28AE9210FD291633E79227783CF3C4B278F64955DE75516D18219D2FFFB51D0E5E63451DD622F7434D1D3C4013E8F8255F0EF4C8A31F15E87AD502958F26DD3D5757D46132FA0DF801DD0868107543C1A4480068C97C97D144CD101E94E5819BB1AA2BE0721AFE90436B6595857E1609814C4445CA4C016581E30F530F40B0219AC85FB9C6103F802FF48E541C93936881D049A97D81A40AB3EFB2DA2055D7A23A6DF84622BA406C441708A3B285738B8699250DFDA048593BAA9EB9E55376D1745A1EB637593548E63EA0DCB6DB88EAE560E0B2A490A256E485B8238CA27787F15C805FD5E739251BDF4C3347F73CAF19D5AADD01E02A6434A5AE06E1B9F5645B7295D0B6EB77973D56F78860939B6E3D667B4E40B81E654E743590FD9A91C4356BF47B4549707147DF81A418DA813F2247B4AEA719DEBF10F8690968484606D721D4A0704AD2A0EAC040F833650D7D00DD7B62866FCC773EAD24C952A3EFD10F179E6EBB6ED988EA4942B71D6A68D1843616606F0B4F586516F1810E679F8A9DB96618E236F441B65E9519C0964CEF65A1F1B9156996DC543F39FCD373FBD9CDBAE7DD39B772E1B97F459CB9E37666DCB71673D57BF3C7BD1BCF88971E5CAA5BA61A375957150A061856F1214C135DFE23A54576242548C848708C679F511F85125719CE3756ED71DF69BEE85ADDEE2F6D8499C19C95FCAA6D5286C1BDDB870BA1AB51E2DEC3A1D90473BE80184160F9225AAEFA1C64C282087216710EEAAED991A88545E321DE21E14821B17D440FB084EE368736E53690F3ED6B801EE132F8AF79E2232686B296BB3A6C57EAB15380E30C959858E4D512874AA5BD56D12634FEDD46DF607398E0D27FFC82E1ADC4B3932F651AACB4FB2A6506B6A20D9C4892D69725BD90F0322D9870E6523C733A78C5EC9D40E48B8827D8998D57A831549D663E247E0B4DBA4E4420AEDB3144E81B011E26696184E3FDF4459B4C3419C705829B394F71275F9FA06F4902928063EDF217764C30D5A1C296430BAD7B21AA94291DA9FB79F8C5D267352332D325185641FA07331702B5D216F33D98ADC38AE3B54318E6C4035F26B8A7F8CC96537E5D4892901A8566856762B7147B61E10512D8EF6F37469A5A23DB6A7D45101C896C06CCA4E27B43317130A50E1D7347232CC72824387163F25E9C04082455F7EA9F103936121BD3F0626EADDBF530A202580380112530E30CA4031DF01C8506F46E32BDC3DC505542E2AB709AA9761762D3FA6692CB74AB07C9BF38F1ED7866D7875A76AE9FCE48E568B619F9E066F31C06AD6D772D2FE5C524B85F532470640AF1055F89A3B196ED56313FCB34596C2B89EBCD8C78DB10FB45A894DE51738DC1DACCA1EF9863C5F6A71BA0D35F9543F0147F988E7C7348BEF46C2C0A2A0651D8987A2C6F3304AC4248DD9637DE709BF1BF975342239C17DBA0D046BEC108D3FF35327582E79283D061D4592AE5E407DCDB46634BE1A5303A084C21D4473AA852D88B32157636C6482AAAE4D931F9AA65C408A3A5D49A38BFB5CCEC53DDBB6E6F8D9B8CB97E9BD1839A87873968BBBCE91147BC11759AADAA8DEF3AEFFEF86AE8FE82AC797D23D653895516C13264577D7CCBCE4DCF18E016399530C03938B40A97EC7ECD9925B091555D183326F2FC4097A66E78CE90A87772FC3672E4F8BF8258C9AA381D3DD646C044497B83AE4B297F3099AE7D2A382BE2400BFCD2E20653C9B7B60B2A04C2086A9FB7CEAD2070A2D2A6CEE2553499E62D076C439DBBF19B3C1E80D50FF26B67DD3351A66DD6BD8BA7446B49E9EA3354C07848ED1B0CCEC246D7284DEFA402D8FC7E98ED43C65A456D7CF466A273352736CD33AF991DA34F3B4B349D7D9A4EBBD9E74718D57F9772FFFCD4917DD69E8E5D1ABC95F4D72D7DE43B40A475A78A3BBA73BD22A54F2B8A756AA69EFF3D4CAA81A46A19DFF87A995F50F1C708210770C03ADD290A6065A0A644D39D04AD43CB62C50584ECA87F4B3EF6CDA751CD3AEBCA675E2B3AE7578973F32CF1EB955C32B8B0A065E79CCC5751822C0D9A0EA6C507536A87A670755D95596C33E388DFF2409597F03504B0102000014000000080011A0D34C93BDEDF66E070000BB2900000C000000000000000000000000000000000072656769737472792E786D6C504B050600000000010001003A000000980700000000,2,0x01,NULL,NULL),(3,3,'2018-05-28 00:00:00',0x504B0304140002000800B76EC94C04893A25F62B0000AB2E0A001500000072746B2D6578616D706C652D6C617267652E786D6CECDCDB6EDCD61580E16B17E83BB0BA6A0B6B4472CE803C7E83BEC22048DD2240ED14755BB477B2E4462964343E146EE0408ADDE4A67763CB23CB3ABE02F90A7981BE42D75A3C6DCE0C3923652CC7E9EF0089456EEEC3DA7B6D6E7E0EFCDFE3F3F59B7FB9FD3BEFCFB7FE70F7934FEFDC58091AFE8A77EBCEC79FFEFA933BBFBDB1F2A73FFE66B5B77273F0D39FAC477BD179348E37A251BC191D46AFA2D3E86CA83F462FA3B3785B2E7F3194EBA7F17DF971233A910B9F45877A518A6DCA95B11694E746F2A45C8FFF368C8EECF26BF971D31E92DB9E74E7CEDD1B2BB7FFFAAB8F6EDFBAFBFB8F3EBEB522CD5F5B8FBE9C55F769341A4A05D2A7786BE04BEF9B8DD00F7AEB6B0B14B75AFF298D1E4707DAA7E8482FC9B55DE9E3A13C36D631663D8EC6030981FCE3AD44CFF472BC11EFAC484355A593BAF6A227D1F36877D0F49BBD562708DA61BFD991A7B2EB49A92FA596DD41ABD76C773B61D80FADFFBBC96DF9FD5427B58F67DA66BC318CEFC55B32260DAF76605FE26D3F1E47A341F4B01BAC06AD56BFB32661695A6F1779CE9AF8B7B4763E8CCE756224803251F1960E7A60437E191DC7FFB0076CB852756579AB6D2FBE27ADBCB4F2D29EB42ECB68A0919879C39E79565E1F495516B19FADAE7ABFFCBEBFBCD5D534FE130DD955EBF386C42599D037D92A4A6E2EB24C9E5B58475E74E84547F2FBF37847EA1FCD5B345AF743099E7429BE3768851DDFF7AF7BD137D2BF138BE8A644EC4813CE9326A48C54BCA5F3A1AB231A49D1FD349B5E7BB28C0EA49157FA84DC78618D8D9D1A82505745D15CD681E752E591953E4BE3222B237E90DEB7257B22B190FF249D9792B6684FF20049996FE5D689F4EAD8E296959572E51B79F93DE9D8B676439A93864BB54BC96D5B2DE522697FD7E674B848C4AEDF6F76C35647B2CCF72712B148C576E8F79A9D5EA7EDBBA9680DD52C0A67DE240F745BD4F524D1DF91DD4EFAF7CA169926E0719A376FAF787E17EE563A1EDDE94E75D1CA5EBD29BFDB89C6C324B452DB7DBBB933E8F58350DA69B53BDDEB5EF183EEC20B3D9FB6F5C8826A7368DD3FD4B29A97473AFB126B99D4AC7451BC583D35F9F8D0F2F13FB6276D247B4CFC607E1E260188371AF2C076FC28DE946D3CF0834EABA97366BFFAED6E324CB754FEF423999F6703BFD56A87EDA46472255FB5E5414C6CEB5699ECC62DBF17747B8134D6EFCBBFFD56330827B7F2AC6CB64A2F124CD9EAA7DFCF7997A6EEB9E1D9B364B4CD5372AD69E9E15EC90BCA424EB236DB5DEDEA63A94F17B8853FBF6EAFE5910DC7EFA7AFF4A6BD0647C518B3F6EF4DCFF679BA9B8FF3F53CF6F415E3A56F979114CFEEC8D6AC49FC457149D3EECCB34A344EA769CD52F0B5FB1AD67D3DE97C52E9991C69D23DC7D6F989F773099A4D822D39CDB8935F78F6A27BA375E9341C384DC69B5E69B4D343CBE3B6561138D9BD6CB2E5CDACB35019D44EC36F6933AD8583FAC2E2A181B3816B7C92975BFC792990798C4A6FF379A3A9ECB5EEEBD30B271FC9D076FB37E98424D5A701B5414EAE9CBAF2CE4A1D49114BAC81269CED658D6ECFFA52DCCACB3F96CA5E0F757DC92A7A630DD92A485F5172082BC6AEFF7E9B2DA47D9BC7390F3B99560A60D1633BA9495E27AB303A19D851CCBD5014D483E491ECF8B6E139BBE9AC9BC5AD6C90032BB29FBC64E30DEFBBFB4F035B0B9223F2DADA72D742E5881AD9A84BF5E77B59DD91B5F4C4823BC45AE5B0A66F15E1985EB0126F2F9F8BAFCB6BBF341713F79C855EB10BD6EC83A571B6D371CE4CDA8AB4FD57796BB37DD0969FF461C33EDBF48193249B8F931FD6EC63CEF2B67C7F22DBDD33F3A836C36B76AC397B56290041DDAE353B00B242FBD73D3BFA7CEE8440B75AB7B6395DAFDB54D766CF6979B36DD66CB62F6CFDC937EA206805FD6EBBD1D4236771B57A5FD623F57B9C6077D5AFCD58F6D78A5CAACC174997FC6BD11270A85BC6503FAEE5CA897EB24F7C4E3A995611F8EA85964F4958BB922E904933A276EE0EC844434B79AE75C45BF56BAE3A5BEA93C51961F3C223D4CDBC5D912BCDC572A536552A32451360333B580C82B6ADFDE282BBAF8EED4476A6EF4B7DE16C4B28EF1587363748BB767B2437CFCA93EDDE7013CB0ECAA7762B195379C853B73DAFB4F4E7F4EC9A3BD83CDB7BEDA0D76887A564774A2E9CECCB5B83DA55CFC9DA4BA6E6BA7654439C1C15EC9B6FFE09C77D6CCEA6F75CB72F19D161F2959B47E1587AB19D47C17C4BC21AEF2427D3B71AE1DA6DAC7CF8B3F116C3C888CCA8CA9A1F678D491515A7C40BD5E174E3EB74A2B492F241AC7CB28B9E1AA218159C6B3BF1DFF50C1E3F48277C947DF7C75B356AF044CAE90AD2452CEBD70B9BD73DFB4ED4539DBE25E4E4EFB51B95E7CA6CB14CF7586E4C4DAA7B6D91F5E179CEB1BFEAF85A737ACD0FAF333E64A7CFA2EB6628AF2DA50FF27CD7249D71B9FA2879A18E7D639F8FFA2ACEF27A1698FF30FAFA583FA0EC53F0C0A1DFA98EEDC990B22FC6293CBCB65C642F76F1F9D45E9CEE6BFBB7E4C8D67C6A68525501CBAC7BE93EA1E63465D65728E3DFA68664146BAF47C961D5DCCF0A93951763B24F9FD93692E09FFEE98BD280FE50BC305E24AF50195CBF65AFDCE282C37BD96760B2D996BE0CA584976D14F64A5EA8E50514FB8288FD7D0C5B06BD2BA5BF8A5E0CC24ED00BBBFD5ECB4F82915D2F23752F6C4B41394634C369A69E3F43EF5DAB67D571B55EDD77BCBAEBE3D5EFC6ABDBADB08957E3D578F5BBF6EAC072BC617FB0F96160F50429DB1949BF640ED792CF4B7B6B1FC86C7D77FFE9040D1790D7C9E27635EE5DD9C965D3B63BB41F336D078D20A81CE7FF036D372F118077B8C52D41BD6BA7B4E460CE96B5A07A17DD5CDA2A70AA9CB71ECA9F913F1612AF4FC10B64A09D172604329D03F77FF71B5F01815F7844D329A53A756EC2B6A9E3AB1ADCFE84AECEDB359620E5FEF2A5DC5D031791F2E2A97723E533F687CB40F9E55726307E79189F751E7DE72CFE50A26BBFB53A0F35AC5EF495BD0B252A4F2CC94FD334C5B4316D4C1BD3C6B4316D4C1BD3FEC04D3BC0B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B431ED0FC4B4434C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4CFB7D9BB679F6CDE457AED21837C68D7163DC18377F1709C68D7163DC1837C68D7163DC1837C68D7163DC1837C68D7163DC1837FFDF36A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D692FD1B4516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B551EDE5A836A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D6963DA9836A68D6963DA9836A6CDDF3E826AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AA8D6AA3DAA836AAFD43546D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C1BD3C6B4316D4C9BBF7D04D546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD546B5516D541BD55E8A6AFF8F9DBBDB6923BB03007E4DA5BE83BB576DB521B631044BC8AFC2AB90EC0795B22AC9EEAA5DA54AD2ECEE4DEF08C1E00021AF30F30A7D92FECF198F3D833FE26C5329527F20013EE7CC99F3FDF1BB606B6B6BEBE07EF17DF1AE18970F8B71FCBE1DFDFE775BD3AF83E2A708BD2AAECBBF96C70BB111FD63715A3E2A4E47DD9DED5E6FBBDFEDED446E75602BE58BC8281E2FCE8AD3F83D29C6A3E255F1BE3C8A8F67F186D34E71DB29DE478237F1F45FF2BB26C5A4535C4682F308BE2E2E7260E4513E8CEC27F1A2C53C1B45BFBFAEEC11FBCF081BA7ACCAA346F8BC46BD75353A285E955F1537F17D3AEA75D3D776B71B09E7A18DA4778A599E348A59FC3DB23E89A8EB6995C7B921CE732D8FE2E7717EF4A66A9BEBEAC3FD083DAD5A6116DFC872492B3E8BBCC691EB2455A3D90AABCB16912FEF64341E6D4D631723E74F1E142F73C9BF4D11D1BC8F0F53271E1667B90C37E5C961EEF72AC179F955A3FD0F56F5CAEA01BA597F2D1F80CBDBFE757CBA6C1431EA50F7C155A319C7EB07E0EA59B57E526D3CA7369C5267E5E31810D7F9F1F1EACABD49699BD53BFD60F556D762C5EC4A93E6515DB8983679BECC03E6C95EC6A347F1BEDBE2FC30323E8D174CE265D777475A4AFB3C479F46E46D7B0C34239A9331DE751BD53E9ED5A9D5CE8BD19D4E6B467CA0645BCDCA365788BBEB4323E1E21CFCE4233395ACD398BBBF71821EA482A5164DE3250FADF161FCF13ACA7A598DB0180FDFE595E9ACD5A3B3C7D6AF8B316C625D8BF13AC9636BBE295C47298E67CB59BC203763F9B8DA1FDEA6165DBB98E595E53412DFA40ECCF59D57A3FA9D2B92E7EBFBF8793CEBD179E7FDD61C1A857839EDA694C9A3D664FE39C2AF72CB457F8E8A27D1BAF9CF9CE724356BA7F847DE0BA3557EC893FCDD749AB61F6D4ED1E5AF8B8885FE68866DD2B5B35991D6ABDB787DEA90E9706AF4FB9DB8F6923E29CE47C5CFB988AF9BAB4C5E3A536473BB4D532E4FBE8BD9CC4CF3694970A39AAB5EBF49C17E49ED99B7D77A0AA6C5AC5DD6F2E4F328EBF791E2328F898BBAB4CB0AF622AA54EF0F93BA4B5BA33016CD78E8A65ACF9B1BCB8BF4DDF9A2789607C751F9F88B6A895D9ABAB549153FC4C4793EDAE9EEEC0FF67ABDDDFE70672F2FB45578FB30B2B67C9FB86517636643374DAAC57159452E8B9B4EF2886A1EB46EF3C29923FE70EF5EE7CFFFED57E7DEBDEA354BDE929BE7D728E5653E138EF3FA799BE6F075047C3BDB03D3E65A2DB1F15CDA3EAA897E9EEB913FCCD7FA57D56617951B0E7A83DCCE75C02CCDF3BC4645F0A8D7DF19ECEEE5515107458A4EBD50E4CD73A337C7F9286D42A3F855356F34733A6EE4C03AC9AF117393D6C56ADF98268D64ED883A798CACAA55F2F275DBCA3B355835F85A49EA475F45857E8AA5F7D5A8BFD7DBEF3F18EE0FBA5563D4E1F3623F8F01BDDFDF8D84BBBDFD9D7E3717FB793DCA73E93ED44307B1F49FA7EE2D1F1EE6B63ACEEB7A2CC8E537873154CF72C7A716AECF066F4783FE5E6C525F768A5FD25A9F36ECA84BDE40CB934E3A8AA5668F675FA757A5E048FA2675439A379D28625A35CED2132BF288BE8DC27F7CC1A6357A968F54E9A47735EBF86A7A476E5FE7C8C7A3FD61AF5F8DA1E1979DD98707A90D377A7EFAAEA7B95BAF72C952F127296D9A2D57A9A773E337E6799DBC39A057AE804FF20AF8AF7C283AAAAEA1E5771BAC83B901CAA3ED78E0B87C1A0BD04EB7D7EDED0D76F6BAD3AFE1EE83AA9ACD54B3A79F460F3D1B750783DDFE6E95B20A994DAC76251A93F2B0CE2C76EC4177BFF7603F4E33C3E1309D697607FD9DE66C6DA6AD87EBC734E6C1CA6571695CB3795EE4F99097B458EFF331BC15324BB8FC56B1F29E35BB490DB7E390BFFA2EBFE422F5226F4469448F5B87F0E275670A10A78DE379E514CD137B9A78E9EC9A6F0B97E9AA5027AC31E35D35153BD3C25799DE9627F5BE97C7F94DE78FD168B913F290CB47BC3F4D6F0229AFD40D178D57968F3AADDAAEBEC2ADBCA0AEBD9F361A756FBB3B48AF196CDCA8FF43F059771F5D7E1D9DD5E4309FDF2FA71D52655F1FDD5325EF8E9C75E91B23757657680151337C96381FD4D69DB61B154F3FDFD6A3E84D7DCADBE016B6CEA116AE1F3FCE6F36E551279F9112224CEE5737C3BC6B5F446FFDFBEBBF154F1EF4EEF50683E1DEFDD44ED331D8DB8EA69BB6DBF2FBC9BA8BC3BA43EFFCD4BB6121174FC2AD73CB9DF2B70E302BA0695EB5C571BFE620BFFABCB95CE2E657AC95D6B618D7429AE59CB6C6D33607B58F11B58FD7CC0F1AE687F59769336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69FFBF9936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A53ED4FA3DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB4FDF711AA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5FE1C559B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB499B6FF3E42B5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA9FA36A336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D3F6DF47A836D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A976EEEBFFB0736FBB6D1C670080AF55A0EFB0CD555BC4124951B204087C15BD8AA3A455011BF121456BB8B01C37B9E99D7C904DEB40BFC2EE2BE449FACF2C97DC150F925307308ACF01A470767676E69F997F87DF85CE0EABA3F2ACBC28CFAAEFCA497955DD1FEDEDF707FDC1F6706777FFEB62F6E16EEF60AB7C96AB1C95A7E579FC8CCAE5D9E2FDBFFFDDC6C6C641F9386A5D95E751F8BAFA267E8F53DD7B51F7BC7C1D1FDE97E3EAA8A93DAF3EFD1C05CFA3605C5EC60D57A98D7C39DA189527E5A3F2A4F8AAFC4F5C7A1B2D4EAA6FA2A5075F45FF56DD336B340650DDDB8C1B8EABC7D5D168BBD7EFF57787DBBBBDE9BFFD9DBBF530DBB566773F2E9F96CF46BDE17067B053D7AC4BA663D8BA3688348649EA4D75EFB069AC3C1D0D7B7BFDBB7BFD78D8FE7EFCECED0C07DBB9EF4BEAD6B1DCFAA4601E94FFCE975ECD43503D9C7569E15A3B3C27D571EA410EEC8351EA55B76456F1C72838CB71BFD71446E993682F4AEBF0CFCAE3C2DFCBD33C9CDEFE666F7B73D0EBA7A69BC256BD93B87961B63FD68FAF57595D5C449D57453DF351F5F5ECCAB888589C560FE745D5C3A857E446529CAEA62D47C5B7D1C845F92E7F3C2F4F8B69E7EB4627D5C37A61570FF23ABF2CFE1841CB939097DC519AAD3F15517656BE4F6DA56978D77A6475547446BB38B459DCB65604EEA07C9A27FBA2FA3ECDC2CAA0EE6EF686E931C35B07F5658E470A5C1E788A4F547813F7FEAD13C8598C52E1D53434E39B46B3B2D7716DC9C2998DE4309A9BE4394A1352373F0D681EE4F595B3AE7E6BA59E4695BCB146FDBCC7377BBDDC9179F9ACF29368E9ED615A5CB184DEE7A7E42590C71D631DB5069E7E7E6856D19B3C8937DCDCDA669DE8CDBB9BF667DAD4F5122C2F4731D4342FA96AA4A422AE7C1B5375518EB7F20C4EE2F1A7313DE3E297EFFE513EBADBBFD31F0EF777B7529CA66BB0BF19A19BC6ADDBF6FC99E921A9E59C385B5979D9C5F9A5265EB7EE6413A34E13B3B437BAD6FF764AECDC315BF7EDA12DAEFBAD953D5FBC341FF1E2DA8ED9296633F7A2BB4D3A3377ED5A6B4FAC48986B52E6B571F6FB2BC7B96287FFB39B0573CACC8B35FA90F2D771BEE1B2DEF817F587AD283DADB778F7FAB5C4F06C9A90C7A9276B93C19AE476437AEB0460FB5704E0374C71EB93DCAA3477FB293D285FE6A5791985ED94352F5D9DDD5B2B7263E3B3AD82569337AD877614D6E4B9AD25DB6563BE0757EEB3D866B9E77F6D36EE619AC47813E43E5C560F0FEB9C932BBCADBEED9C4F56CCCAEA057ACB2DF8093B309F175A5D4CA7E9E91C9CB7C278B67E01AEDE55EB37D5ADF7D42DB7D4EB3819A54C9FDF75AB07F726D53DBF7DD658BBBD56ECAEB4698E9ACEC5B6C9FB655ED04ED367F92C38492FEB380EC603C6F1B08B858C5E7F0749BB212E4EBA6BA07DA1BD19F311FD2A5FAAC7D489F3E2E5A2E8EC881B7AB6D11E6C3B435CCF0FAD8A8B7BF0B3AFCCD4B3A2B5777FE5063D481D4B11ADCF12F9CBE5CDA7A9F66DEBF3623AF245E1C71CD4F64BE1227A713C4B67F1801CC6EA7EFD7EF89022BA3699754F9979BCF361D4BFF340F27EFD183F8F6733DA5F761EFDA4165A9D78319DA6D448F79C76ED50F928A29BFF37B7394E612DCA7FE5776144E587BCC9AFA6DB74F999B199E7C5C7C58585F96897DD666A8BA2F5D560D5D174CDC97476305DF26577F1101A9B299F51D3E67B37DB99693F2D295E7D86FCA48EFD94BF62A6D76BB3055332BBFEA5FDCBE8EB93F4252BAF89774D6F9775EC2486D4BC1FC6CD945E3BEAAF149D932C3ACFF2E2B857DDBF85E54CF3ED0FB1719E8FB67BDB7BC3DD7E7F67B0BFBD9B136D5DDE3D8CACEDDF678EEC9AEF186953AD429865D7A69B3CB9542B0F4F72E24CFFC5A53FDCB953FCF97FFD57DCB9533F68C97372807E9E4AD3714E43E95516BB38BEE6452A9FCC7CE9C534C9C67D3322CC5F00AFEA0FF36CFFB27EDDC5F0F687D92EE6052D046CBE01D622D9F95218358A2655E4D7E7AD9E1C27A4F41A1AC5AF3AC011E874E0C8854D959FE3CA65CA8CF59B635A35AA752F34D5636DD551C9096CD2693B05AC5E7E9D2ACDAD2F63404F23F9BE1C0D76FB7B83BBFB7BC35E1D8CA67CDEEDE7B1A4F7063B5171A7BFB73DE8E56E3F6FD679EEDD4D337410C93FA1695C3FCCB13ACE993D5272F597C358ACAFF3C4A70837A7830FA3E120F9E8D745F953CAF6E9951D63C9AFD0245D3FD62FA5B8F7557A542A8EAA6FD234A49D53441753DEC86EB9A28D7E06D04FEFD87444B75269AA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5FE7F546DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DAFEFA08D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A7F89AACDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4CDB5F1FA1DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A53ED2F51B59936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69336DA6CDB49936D366DA4C9B69FBEB23549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A536DAA4DB5A936D5A6DA549B6A53EDDF4CB5FFDBCEB9E4B671040174AD00B9C3C4AB24B0A419FE440202AFC2AB48F247016444FE21361C488EEC6CB2A33E946852A4AED073859C245535BF1E7286A21D07F1E249B04D7657775757575557BF85F7DC3C7EEA46EE2A7E14EFF5BFFF6E437F76DD1B377453377797F2E7223E72A3817C388F8FDD8D1BC607223F8D9FC5FBF2E9C20DB351FEB0BC4D1A4FE37D37D379A473E6C6F171DFFD21B3ECBB3BF9B2EF266E18B87920DFE632EBA3F850661731370E648147EED6DDC64781AEEB3EC997E1EE76C584DE6A32B508DFBA51BC37B051F936927F6D23D2ABCB8FB2C5648A280CC3AD3094E9BF74064F89F7F19EDAD42639283AA4E783B44FCC727377DB77CFC5BAF6D1E61CAB5903F7BBAC79A5567925D3C8164542B75D1E5AACB65DB39C742C9D87DFB6CED10641EE12AF456C6207325295E223EFDC17FA7C53BC945D5DF5DD0753F1BC383537125D924E4FFA4C04AED4F2EE3A13EB8B5C55B3B7CDBAE5D751ECA3DA534E7196BADD2890BD2DE81A1F7F1BBABE14891BF389EB4CDB2AC54E654B77267621BAA7475AF2C213E91B270BF927D277A7FA1B3C70EFCC39F6E2A307B2409DB43FE3A97B258173D26F86CD6EAB1345ED46AFD9D1584DDB7D77BD47BFAF6CD9E59EDC7535A896FD32E9ACEA4B835CBADEA5113736EFD1C4A9BFD2F5C3E666F0F3BFFD09363793852AD63103FD297ADE48E3A1A5A1896828513C9586A76E9EEDCDBD4F93AC8C93D1B324D4AF6C27F6A5C8F6676654DD5EAF15B5CCD259432E7362594A9AFB51A3D96A77CC2FB2269108B254A1F968BD9577DD5B49EE721DBC4D0D2C8696D1496326F2A7F4DC6A664C6E8E5454C4CA1D99B8F85662154B60F3D2DC6AB0C4FD4A22D9D033D9D05B49BE67FD4627EA36767ADD569818236B2FD43E1197EE36DA22D88EBACD46686A9F647E6EDADD7742BB92FCAFF478E3FD81D9EAD032BBA4E4F8C9409CF5C20E5E2D3C4D83FD53BFD5E8C835F530701F35DBEB952D7BB12B343E0E24CEEC5292B1E7BA94368BE8A51E83464E202A6ADEB8D0113573C8D98AF29FAF58BA234D1B765D49B865079F04B8CCF6D83A8FFADD5ED4487CA8F730C8BFECA80DD71A9FAEF5C28E75629AA9FA6395D56899E8499BF1BD48CFC47D87AECD81CF2D07FE656591984E3D257EB646263403C47B5B32E0307E2129A8194661D46935F5D4ECA7D7DE49B6E94BE5A35FC809BDEB87AD56BBD14E2493963CB0CA9BF08272904D2677762BEC463B5DA9677ABD9E5635ED961D6AA56CE6AE9F63CCDDDAC458D9E79BE7D4E2C1529A64FCA605A3DF920B8A2B27115A14A5760DCECCC5CDFCFE85F1DA4A97613FEC6D85CDAD4618352DEB0F8B3D5696A2C969DFA5397694FBF328D09228484EDEAEA9695E945A99745C3469E069F5AAF952FA66E9CC22A8313295889B25A118A4CA2793CEE3E3ECE6333FBF0D7E14A3D92198CB5991F753A085A6BBD1B9F418AEBD25E383A0B4DBE5AD795763B5E12491D9614FE35FD332B6DAA89DADB0A5CBB4D636EA597EBFFBD5FDA58CFDA564C8DC46DA384B4D33BE6F37B55A5BF1BEE438F94E0656C1DFA407924C9F15EFBAC945CF5925EF796AFE5AB03744C52BA210B6526D55BDED6D5CFFFE9479D16556E7ADF10EDB5DF1405A7A80BC2EDE36F15E6055D29D5E1EDB768273BBB5AFE5B4FE7EFC9B7BBE136D46AD56AFB3AD764A7D30DA12D3A576AB7EA1AC7A3AAC2A7B8BBA774D25976BE152DDB2A07FA980F147E47EEF6F6DD9EF5794F2F515E746E5F3B57864BD2F8749E9E416FAFC02BC3A61AE48990BFB8CA2DA7DD644F89B7216B49469CE2A3AECA50F2A316EFAAC4FBE6C4BEB3009F172FF4262F08BDEE1CA64B022B9DD93DE4A06687E8101FEC314B73AC9D5A5B9F58F544ADD04AC48A39FB28AD695F4A650F3AB798137E57DFE507E48D6E6B9ED8A70D92862B036CE12769330310B5C4340037D0C4B8B3C4C0649CE5986667541B8BBC241D70CC1CF8840AB173C15B59A4ECF60E29971B4DA01EBA36A7550AD1D536B8694F229CDF476D7D56FEE526527EB678D95E155135D1A340799721236162F45839FA6EDEDACD7D240CB41594031E7B48A139E58B7F1B8B20FF81D7E305A893EB3AE644F253B2F77E7FC2E71FA7B34DBF037EB6788C5FCE00956D0DDAFED99AA59E0C5EE170628541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAAFDFF526D98364C1BA60DD38669C3B461DA306D98364C1BA60DD38669C3B461DA306D98364C1BA60DD38669C3B461DA306D98364C1BA60DD38669C3B461DA306D98364C1BA60DD38669C3B461DA306D98364C1BA60DD38669F3BF8F40B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA836541BAA0DD5866A43B5A1DA506DA8F6374AB5977B6C2DB3C39D69AE21364EDEAC0333C1B979DF50B3F158947B6C0C52F29C64E5B1368A58853FC64F06A5BB20019717A2C73F504B03041400000008008DA1D34CE84B842274160000917508000C00000072656769737472792E786D6CEDDCDB6E5CD51900E0EB54EA3BECFAAAAD127B8E3E48617883BE8285685A21955095B66AEF9C03842AA804A828A24A480A37BD73489C181FE257D8F30ABC405FA1FF5A7B0E7B3C9EB11326A1099F9182BDF7DAEBBCFEBDD6E74CFEBB7F74F1F5BFBCFDBBE2CF97FEF0EE5BEF5C7E6DA9B9DC582A2E5D7EF39D5FBF75F9B7AF2DFDE98FBFB9B0BEF47AEFA73FB958DE298FCA9DFE56B9DDBF5AEE96DF9487E593CDF46379BF7CD2BF11973FDA8CEB87FDEBF1E356791017DE2F77D3C5487635AEECA484F1DC763C19D7FBEF6D967BF9F2C3F8F16A7E286E17519DCBEFBEB6F4F65F7FF5C6DB97DEFDFD1B6F5E5A8AE2CF5D2C3F3F29EFC3727B3332883AF5AFF51A51FBF672ABD15CBFB87286E439D77F44A1FBE5A354A7722F5D8A6BB7A38EBBF1D84E6AE3B0C6E54E2FBA20FE2B96CA2FD2E5FE56FFE65214342B7595D79DF2D3F26E79BBD76EB4D73BABCD66B7B5D15E8DA786D7AB549F472EB77B9DF576776DB5D5DA68E5FADFAE6EC7F753954C757C92CAEC6F6DF6AFF4AF459B52F7A60A3C88FECE3FEE97DBBDF2D65AF342B3D3D9585D896E69E7DA9EE5B95CC4BFA3B4A3CDF2280D4C74600C54FF5A6A742F37F97EB9DFFF7B7E203737B29E993EE776A77F254AB99FD34779517A4CA35EEA89136FE467BE989C1F5556B9C77E76E142F1CBEFFB555CB830E8FF6305E5ABB9CE5BD12FD5803E1ECEA2EAE659A6C9DDDCADDB45B95B947BF1FD51FF66E4BF7DDAA44979DF8ACE8B2AF5AFF43AADD546A371BE28BF8AFA1DE41EBD1A3DB697165C1145449AC8F85A1A8F343BCAED48FA60B09A1E16318D1E4521DFA427E2C6BD5CD84E2D87662BCD8A7171C30ADC8D2CF772EA27837E8999D1FF70703F4FD983E88BF85F55F9489927EDC1A88322CDD771EB206AB59FFB6D9836D24DDE18A5BF1315BB91AA11C545C113B947CA1B79B64C2619D477E5940A8F17E25A63A3BDD6EAACC62A6B348E2DC4F152ECB61AEBEDD5F5D56EA3BE1473417326456DDC621DA4B098E653F4FECD887651BF6FF2244B0B707FB06EBE7DC1E37BE66A0DDA9322DD619AB411ABAFC67737CB9DCDAA6B23B7EBF9E6CDDEFA46B315E574BAAB6BE78BF10F290A9FE9F941591FE74ECD6398ABBF9BD2A675B997463FFA3A0675987A9C7C3C7BE6ACC75B793DFE27C7A4AD2AC6F43F3C7D1D561DD0DF5A8E076EF43FEE5F8D30DE6C34573BED3466F96BA3BB5635B39E6AF4F4C7313E5FF41A9D4EB7D5AD52565746B376B211C7C27ACE2CA271A7B1DE5C5B6F46611B1BF167A3D36EB68E87F261DAE12C7D9ACE8C503FFD7E1E5569EA5EBD7BEEE4C5988367ACB5765E1EF52BA3843191AB553B8CAEF9EA27915F9AE0B9FB47D7F36B793B37A7B13178A5B7F36B707BDCC661F957A647FB6810CD7746F379A748AF9862F076D98EE4C33B119AD322FE687C292DBB2745CE24F5D3E120E748F8B0FE1A4E71BDAA7C95E993D8D20C624E9EE707C5CFA3D3F220E4299756DCC12F8AFCA27B9CF24AC3F0A85664FF6A31D1DAE9A68DFA6D6546C745F4CA831D6FE6340A333B7575B9D149C574CEDCA9F7727FA48ECB0D4FFD53BDDCFA1F4C74E4A88F26DEE6A7B56666AD535C9F9E38A3966CE668FF78302055F6830ECD8D3C3E73E6A5AFCDD4ED489217562F2DB81CCB96D7D6735DC6B746E93F89CC1E6EA6F915B3E8712E28CF82C12B2A3661E3B6A73FBF1D4EA407791C4F79B8B6D2263A705CE3BC538B755DCDC2F2A097B762F50BE3846923B917113F07BC5A343DE9E6F8D6B091BD9CE441F592ED6F15DF5DFFAC99E742AC91786D5DABCF85992D5A1EB67A22FF512C9BB7659D78E28C11626566B3A66F8DBB637AC2467F17A3B1F87272EE4F8CC5B17BB5893E230ACE898313EDEC0EDA79E2A29DB16CFF3919DA721CCCD32FEAB0958F6DE981836A35EF573FACE4C35C5EB793F78FADF6FA9E797BEE0A9F13B14E8959131DD09C17B54EEE8098A11BE78BBCF5F9A0D60529D4D6733BA5EAF382EACAC9633A196CDB7382EDBD3CFFE28CDA6B769A1B6BDDE576DA728EAFCE8ECB694BFD030E707DD6AF9C30EDCF8DD7D2CCF57231BD58E2423A01EED58F8E87D50E75334590CD74D68E6B07E9047FEC74595F78F587FB37CFFEE8F75C97ADA79F963386ED843198EA919CAAA8CB49FFDA8B587CED675A7CCDEE8CD5D77ECEABEFDE609BB797D655372FA8F1855ABA2FF35E75379DA5B20E45196936EE4F05F6C101232D9DB8FB6472D8EB37EA0FDCC91BF0C37CAF6AD964C3A76E1745BD6DA756EEDCC97164BDDB5C5FEEB626C2C8B96789238B9B90A9B2453D5C3CE3624D6D5A4CC4B898DA9C46ACDAD0E493E9E9FBB0FA63A784E6BB29C80EEA5ADF34EF472D6E8C3A342B5C0C51FF66B57FFE368DD6DC603BB945CDED1D3763087919D472F13BC3C2228B197BD9A7CAA3BE7806639E3299DC2E4EEE3FCBCF32F564D0384AE5F4FF964E0AFD0F0773677BA813FD6B736CE3D3489726635A11B1188A56FB7C914FB369EF99DE65713E29BACB3377BFC369375DE3B83135A8F56B67991F45513B9CCCDA64CFD9638FB6D8271CB7A777CC17B3F43CCCF1E1D1287AE4C5317D79F686F7A92AF6553EE4A60DC330449CC4FAFF1F75FD241DF3F281F5510DA8A72A76279A343CD74E11E7B9C5FE2A60FC4E38FD1702E333C8DCFA2DB867E71C88D2A29AC54027DD1BC489246353B2FE02FDFEEB81746530CE2FDB58C3C99CDF1FCB71BC65AB38FD2487918A28D3EF881260A41FEA7BD5FC3E8EC66D74F20B7C7CA18690C3C36A156C27CEAF91A218068AFC7E3F53C967B0F6A7A4F6EF23EDD1E8DB91FA5FE5BD5E6BB5B9DE5ADB58EF34AACE185E9FA4F4F5563712C696A4DD9AC6F4D347E80737F593F278B1AABE5153F5B506557F3EAADEEDB4DA549DAA53F5E7ADEACDBCC697F3AF5F5F0E523F06DF798F944E32BB2BD5F132BFB51FC5687D77FDB363803DE6C6D561BFBD189D9F59C945037CBD69AF32C037979BCD99EDFC31007CFB193AE03986B805D8FCDC219D30B55AC83AA3CD8FABB9B05950CBF2B4F930798C7C0E70FF23D4B3935E5ACFDDCE6E45EFE66F739EBBA95B8B3865A50513BDF2695E5C87830800BEC017F8025FE00B7C812FF0F592C357137C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C81AF5711BE5AE00B7C812FF005BEC017F8025FE00B7C812FF005BE5E2AF8CAE8F57AF535A22B1006C240180803613EFA08C24018080361200C8481301006C24018080361FE0618F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025F2F057CA12FF485BED017FA425FE80B7DA12FF485BED017FA7AA9E88B73700ECEC1393807E7E01C9C8373700ECEC1393807E7E01C9C8373700ECEC1393807E7E01C9C8373700ECEC1393807E7E01C9C8373700ECEC1393807E7E01C9C8373700ECEC1393807E7E01C9C8373700EFF648B7FB2C53FD982BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE8EB55A42FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025F3EEC88BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7D2D9EBEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7CF9B023FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF4B578FA025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF0E5C38EE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED0D7E2E90B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC0970F3BA22FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425F8BA72FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025F3EEC88BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7D2D9EBEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7CF9B023FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF4B578FA025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF0E5C38EE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED0D7E2E90B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC0970F3BA22FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425F8BA72FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025F3EEC88BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7D2D9EBEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7C812FF005BEC017F8025FE00B7CF9B023FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE8EBD9E82BEFCDAA4ACE3B04C0B0D3316C4E57E2313C86C7F0181EC363780C8FE1313C86C7F0181EC363780C8FE1317F33CCDF0C435FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BED017FA425FE80B7DA12FF485BE5E79FA9ABE93CBCAFD70946B9E96D86EB5B1DDCC5D703FCFBE78836CC6F5C374BE8FCB11E7FAEFA7F23653B213E663FFBDCD7C46AA95946E4759FF03504B01021400140002000800B76EC94C04893A25F62B0000AB2E0A0015002400000000000000200000000000000072746B2D6578616D706C652D6C617267652E786D6C0A0020000000000001001800F095CAC0D7FFD301108168A5D6FFD301108168A5D6FFD301504B010200001400000008008DA1D34CE84B842274160000917508000C00000000000000000000000000292C000072656769737472792E786D6C504B05060000000002000200A1000000C74200000000,3,0x00,NULL,NULL),(4,4,'2018-05-28 00:00:00',0x504B030414000000080057A5D34CB27E6ADF4F050000433100000C00000072656769737472792E786D6CED5B4B6FDB4610BEFB5710BAAFC45D2E5F81CD20402F059A5E7AE94D2097DCC4402D07B15BA7375B4E9B830DC44883D6486BC7698FBDD00FC68A6DC9407EC1F22FF40FF42F747649D992F5F0AB8E2587F1853B3B8F9DD9F986B35CE5DFC3E3C9FBCF66BED37E889ECE4DCFD6A64AB8AC97B4A8C666C3E9DAA3A9D2F7F31C39A5FBDEC4A4D814C7224917459CD64543EC88A66855E5506C8B56FA02C82FAB406FA6CF61B8288E80F0B3684822B0D581924846908B4112E8E94F5571A0C87B30AC2B2198D66035B5B97BCFE6A6A74A8FE7E79FDCAB54161616CA0B4679F6E9A30AD1755CF9F6E157DFB0C7D18C8FA66B73F37E8D45A513A9F07CA99C79AA34F3E3D7FE4C34F7C40705DE84A64D8AF57ECB6F8AB80A6B04B7D3650FBB65DD2A131D3B93950BB02BADAFC1AF43F15EBA2D0E24491237200E0D904B641CDB5111890761863FEDE3DF620BE8BB22D64443136FE0F9385D8130C51F0FC1F220E95CF9A6F805C4373C4C0C6A5AB6E3EA98D820D6A6E76CEBA067C3D34FFE299736B27978EE59B75C754B5A4D17ABE952BA0C6ECA4D6DAA85B6AA6A7828620F0CAA355E8457A9FD132C1C57C14348018823A444BA9C2EA62B9E72BC097922874B4A3AD1323E10D907CF5BC0BB04718127B039509132F3A63BDD32729F898CAC020913BB7974F7DB7BDC9E1EBA8DEBF90088E7ED970AF6504B10A15E00752EA367FA54B76278F0F041F5CB2FFAE2D0E3C4B2FC904728A0344494593A722D18DA9C311C618B3B0E24FB300D9D96C09117AA4CC8D8AF7A58B9D649E962DE026222F7EF34E8EDA957A01CA6B290754F66A09275283E05A4AD323623F6706FE6C9D385B3E37CD76522261959136B40DC9689A04ADBFB76C26A790E031CFF125B908240CA44D365ED9FE7BF6A90F31AE44E5DEB5A50AFDD6E2F2B83DD84B901D13971BEAAF27F3F03686E27C9F7A45F5C86F19FD99618D814783D0C8501ABC59CD226BA37AA21F6A0CA8BEDF4A54ADDBA0ACB6A0ECBD8137FA81A194B2254CC15F141133B59EC5500CE119FE888C8903CEF37DBF60BE6FA42FC4690FF164254CF5E66238E7CDDD75D4E998BB04D30A2AECF90631936A281CF9D287098CEF995914F0AE4DF01E493EB21FF3710DDCBDB08690F2C2BAF56F3384AA1760B5957EBFA20670E335D77B13ABC930EB59BDD11AF0F84FB21F4CE16621133108D6C8A1C6CBB28222CF24D3DB0B1E95EB93E18457D18FFFA605CB333388D428EFDAC58EC5E0AF9D2D43B887B0394B5C44167FBDF94C0153B55A9A09A371D47F29476E6847036C5DE766A48572E293F347F2F90C397CFE391CEE5F3F279784EE7FB2BBD060307E0BB3CA7768EFB302F433A1EC9D8E986A1B8DB84B320BBDE46178972771205EB37962852F1FF559FC6A4CD5983873DA53619F936C7366CDB72888BB88919A2388263902FBF8250CCFD88109BE1B068733EEB36878C409B3336079C44C98E03F2C380BB861530C402DD4494470E803EC45008FCC8F2EDC0A416BD3AF2CBC5C7CFBB807D3A08FB6302C80DE5B0AC37F13840D2741DDDE60E0A5C4E11A5C442BE01E0744C8E5D8BE93EB7E96DDF4664ED5AD6DD0CEA43EF2444873B3E32A01D73C0BE0662B6C18D71E89E231259CCA5280C4D006C1499C8B7A88E74D7E28E8F59E487C16D5F2214801D69C0923107ECABCE1FB08CFA9D1F714CDF651839D882C36E688728082D1759143383D8A6490276DB87DD02AE230D5763DC5FB06B6AC30F54D49331B8A50F2387453A32880F87D4D03590439C10716ABA24C066086FDB02B10562872176E8F7ABE2EEACB812E97325728337679FE185C8EF790235453CE2EF1BCA2DCE024C11E1061CE81CE6233F2416323173784878404372FB1F458B37CE68BF713ED957D35EAAFC5DB7FAB1FBA7F8FF0ADE7F504B0102000014000000080057A5D34CB27E6ADF4F050000433100000C000000000000000000000000000000000072656769737472792E786D6C504B050600000000010001003A000000790500000000,4,0x01,NULL,NULL),(5,5,'2018-05-28 00:00:00',0x504B0304140000000800559FD34CE9B28A46A4060000071B00000C00000072656769737472792E786D6CCD59DB6E5B45147D0E12FF30F21348A97DAEBE48EEE91FF00B565502AA44534401C19B631752E48ADE50A922C54D282FBCB94D9CA4BEFEC2CC2FF003FC026BEF3997393E173B25145A29B1F7ECD9F7BD66CFE4AFE9B27DE3BB3B5F886F77BEBA77FBEEEEF58A5DB52A6267F7D6DD4F6FEF7E7EBDF2CDD79F5D6B566E041F7ED09643B99463D59523D59317F28D9CCB4587BECAD772A1F6417ED4017DAEEEE36B57CE40F8515E10116C3D50C6C4887D23EC045DFDD09113269FE26B8F376159C09CDD7BD72B77BEFFE4E69D9D7B5FDEBCB55381FAADB67C91277B2E471D08804DAA1F58B0DEAD3A96DD6CD7366067A9BF40E9549E914D724224D00E61E305B68DC9C7C862390E1002FC1715794064D555830A1415716B5943F94C1EC9C3C0B5DCA657B76DDF69B975EC8AE89AEB05A41C065ED3F51B75C769396CFFA15EC6E78C9164E38274AA6E47EDA93E7CA2F092012788377F9DCA51201F37EC6BB6E7B5EA3584C5656B37D9C72A7E83B665472E2931082012A5FAE474C02EBF9653F5336F607721BA909FA50DD51EB4BC667EE8837694514091C85DE03D07E9FAD0A2B274A6B28A2EDCD0F13F8F92AE1737C9EA11476124E48590137C5EAA01E48FD6E598643F86AF3049ED059E53B72C6B5BC857B06FC601E8C1C109F587800AF040709FC247C99423B09E84C57F2A90F5332879433BB070CCCAC68604DBA12426EA22038E2072C2DC8B302E48A47A18AE7385CD100BFCD2C683936B6C1607083CBF636906ABA61CB788177CE985987F08C3F6C90CA883E2947470EE7372D32CA1BDB53506277DD3B05A6EC3F1EA680ACB5AE99BA4737CC76ABAF566DDB7CCCE6145254561E40D654B2846F584E80F004EB0EF0D1719F5CB342CF3B7EF39BF1B9B15FA43C034A7A205B4F6F06920C71D1D5A48BBCF8B83A0D9B21DE8F1FC7A635B245F083437DA1FEA7AC241E51CB2F917C44B7D39A1EC23D6486AC49DB027D553D28F8FB91FFF6008E96A48500FD7F7A10E80EA56B1615F3D513DA0AE6DD975CFA59CF1BF96DFD06E9A5CF1EE27C8CF4160799EEFF89A5353E2AA4D3BB182C22C0CE0E9594DBBD1B4A1ACD5C24FCB736D67157923DEA84A2F134C2073F6388D4DCAAC99E11972333278A2D75C6E0F931233A29075D746E8CAD4A7904705CEE18FE97C8A8ED81DAB159EC02E9F5AA3C4C748FF5E36DBCB10CDC7713D8F059D08223C0C46608F5600CDD4C48F1212B5DD42B0108AD33C940CC653F3D4245CD7C66BA10B4C2021E6709DCFC447081A27814B8E3A6EF6B1E073E99C64511ACE0C95AA2752DE665D8BE3562B081CD08B938D8394B25018D47AD5F2488DB771508F391E1438769CE2A30F37F52015C83846A9C3779D37855613AE670B27F6A4C3687F1E26448B0F03CA4EAE564E19BF51A923B07063051AC8AA8D261B92D063E6A79074DAA1E242099DB3162E81F07CC2C094384E3FDF465574C2495CB3D968B354F4127379AA4253EB1294B38047BB137D0EAA2E670A158C43A56F66AA50A5F8F3FEF395192FA734D32A135348F704070AE3A901D6798BC95214C655DB618A7D6907AA515C53F263A82C1B60533B3604A05AA15BD9A5241CD97E4046459CED97E9D64A657B65CDE8A302902D81D9949F7EE8672E2614A0C2AF69E46498E502870D5DBEC4D18699068BA9FE52E3AB1DC3427A7D054CCC917C540A202580B806125301B0CB40313F00A8D0D6B6E0C9EA8111026A1753DA1AD3CB30BB969FD33496BB25587ECCF5871B6B607B76ABE1575D9A68136A31ECD3C4FE1F26D8ACFA5A4ED96F25BD54D82F6D3AB740A0FBE0C4BC48CEF500DC2104E9D0CD1BB419DDE757EE9A66E3999BD560F3ADFFB02F9DCB976541DA72729089087309F31D45F5DF47F3B9EFD47CB65FD07DEEBFDC7DC7E11439A1BEF2B9A11282C1F79247E10BBAAAF15B117450354E33C01EDE5FA875B0BA48A7DD5C30370C79BE9FF39AF62CED78665908D3B7B5C66DE5E348D3B79B55DF49C1C8D6BBE0C8D51524192B4CB878C766259FAE0631DAE433654C0F347CF15D3FE999DBD640F311816C68AB39934F61C57E1C507E93438AD4408FE76F295BA5609B9E80D9DFC48DE8598F9FD758FD3852061105D3F2A56498CD13E69C84A4C7C59591F739BF24F17BC992F4A89FE822A21E86B5338A1E3F54BFE4E9E419F8A818A923D00CC271B7055F9669F6A4B30CD71FE1570BA7DFA8ECB2166321935493B6497D0861DC7D8A86EC92193B1EB1736EF3D989B9CD0F49A78C0F67317A707364C9C503EFA50C7BC577681A182288C87BE4FF7FD8FA946E917C1F3E339EAB33860DE152746DCEBCA06E5DED1F06923361FD9F07923B48A97D571CD9920B113555D12B53DE5A8813F4F0967DB8CF52590FC7E07DFCC109BAFE06504B01020000140000000800559FD34CE9B28A46A4060000071B00000C000000000000000000000000000000000072656769737472792E786D6C504B050600000000010001003A000000CE0600000000,5,0x00,NULL,NULL);
/*!40000 ALTER TABLE `pdata` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `pddocfile`
--

DROP TABLE IF EXISTS `pddocfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pddocfile` (
  `id_PDDocFile` int(11) NOT NULL AUTO_INCREMENT,
  `id_PDDocument` int(11) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `Page` int(11) NOT NULL,
  `Data` longblob NOT NULL,
  PRIMARY KEY (`id_PDDocFile`),
  KEY `RefPDDocFile_PDDocument` (`id_PDDocument`),
  CONSTRAINT `RefPDDocFile_PDDocument` FOREIGN KEY (`id_PDDocument`) REFERENCES `pddocument` (`id_PDDocument`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pddocfile`
--

LOCK TABLES `pddocfile` WRITE;
/*!40000 ALTER TABLE `pddocfile` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `pddocfile` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `pddocument`
--

DROP TABLE IF EXISTS `pddocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pddocument` (
  `id_PDDocument` int(11) NOT NULL AUTO_INCREMENT,
  `id_PreDebtor` int(11) NOT NULL,
  `id_DocType` int(11) DEFAULT NULL,
  `Name` varchar(250) NOT NULL,
  `NameAddPart` varchar(250) DEFAULT NULL,
  `Status` char(1) DEFAULT NULL,
  `StatusComment` varchar(250) DEFAULT NULL,
  `Comment` text,
  `DateLastChangingFiles` datetime DEFAULT NULL,
  PRIMARY KEY (`id_PDDocument`),
  KEY `RefPDDocument_PreDebtor` (`id_PreDebtor`),
  KEY `RefDocType_PDDocument` (`id_DocType`),
  CONSTRAINT `RefDocType_PDDocument` FOREIGN KEY (`id_DocType`) REFERENCES `doctype` (`id_DocType`) ON DELETE SET NULL,
  CONSTRAINT `RefPDDocument_PreDebtor` FOREIGN KEY (`id_PreDebtor`) REFERENCES `predebtor` (`id_PreDebtor`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pddocument`
--

LOCK TABLES `pddocument` WRITE;
/*!40000 ALTER TABLE `pddocument` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `pddocument` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `peprousing`
--

DROP TABLE IF EXISTS `peprousing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peprousing` (
  `ContractNumber` varchar(40) DEFAULT NULL,
  `IP` varchar(40) NOT NULL,
  `LicenseToken` text,
  `UsingTime` datetime NOT NULL,
  `Section` char(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peprousing`
--

LOCK TABLES `peprousing` WRITE;
/*!40000 ALTER TABLE `peprousing` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `peprousing` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `predebtor`
--

DROP TABLE IF EXISTS `predebtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `predebtor` (
  `id_PreDebtor` int(11) NOT NULL AUTO_INCREMENT,
  `id_MUser` int(11) DEFAULT NULL,
  `id_Manager` int(11) DEFAULT NULL,
  `id_Court` int(11) DEFAULT NULL,
  `id_Region` int(11) DEFAULT NULL,
  `CaseNumber` varchar(50) DEFAULT NULL,
  `DebtorName` varchar(250) NOT NULL,
  `OldDebtorName` varchar(250) DEFAULT NULL,
  `OldDebtorName2` varchar(250) DEFAULT NULL,
  `DebtorCategory` char(1) NOT NULL,
  `DebtorINN` varchar(12) DEFAULT NULL,
  `DebtorOGRN` varbinary(16) DEFAULT NULL,
  `DebtorSNILS` varchar(13) DEFAULT NULL,
  `DebtorBirthday` date DEFAULT NULL,
  `DebtorBirthAddress` varchar(250) DEFAULT NULL,
  `DebtorCabinetPassword` varchar(13) DEFAULT NULL,
  `DebtorCabinetUUID` varchar(27) DEFAULT NULL,
  `DebtorIDCardType` varchar(50) DEFAULT NULL,
  `DebtorIdCardNumber` varchar(50) DEFAULT NULL,
  `DebtorIdCardSeries` varchar(50) DEFAULT NULL,
  `DebtorRegistrationAddress` varchar(250) DEFAULT NULL,
  `DebtorAddress` varchar(250) DEFAULT NULL,
  `DebtorAddressArea` varchar(250) DEFAULT NULL,
  `DebtorAddressBlock` varchar(10) DEFAULT NULL,
  `DebtorAddressBuildStatus` char(1) DEFAULT NULL,
  `DebtorAddressCity` varchar(250) DEFAULT NULL,
  `DebtorAddressFlat` varchar(10) DEFAULT NULL,
  `DebtorAddressHouse` varchar(10) DEFAULT NULL,
  `DebtorAddressRegion` varchar(250) DEFAULT NULL,
  `DebtorAddressSettlment` varchar(250) DEFAULT NULL,
  `DebtorAddressStreet` varchar(250) DEFAULT NULL,
  `DebtorEmail` varchar(100) DEFAULT NULL,
  `DebtorPhone` varchar(12) DEFAULT NULL,
  `Note` varchar(250) DEFAULT NULL,
  `ContractNumber` varchar(50) DEFAULT NULL,
  `DateOfContract` datetime DEFAULT NULL,
  `DateOfApplication` datetime DEFAULT NULL,
  `ShowToAU` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_PreDebtor`),
  KEY `RefPreDebtor_Manager` (`id_Manager`),
  KEY `RefPreDebtor_MUser` (`id_MUser`),
  KEY `RefPreDebtor_Court` (`id_Court`),
  KEY `RefPreDebtor_region` (`id_Region`),
  CONSTRAINT `RefPreDebtor_Court` FOREIGN KEY (`id_Court`) REFERENCES `court` (`id_Court`),
  CONSTRAINT `RefPreDebtor_MUser` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`),
  CONSTRAINT `RefPreDebtor_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `RefPreDebtor_region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predebtor`
--

LOCK TABLES `predebtor` WRITE;
/*!40000 ALTER TABLE `predebtor` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `predebtor` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `procedurestart`
--

DROP TABLE IF EXISTS `procedurestart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedurestart` (
  `CaseNumber` varchar(50) DEFAULT NULL,
  `DebtorINN` varchar(12) DEFAULT NULL,
  `DebtorName` varchar(250) DEFAULT NULL,
  `DebtorOGRN` varbinary(16) DEFAULT NULL,
  `DateOfApplication` datetime DEFAULT NULL,
  `DebtorSNILS` varchar(13) DEFAULT NULL,
  `id_Contract` int(11) DEFAULT NULL,
  `id_Court` int(11) NOT NULL,
  `id_MUser` int(11) DEFAULT NULL,
  `id_Manager` int(11) DEFAULT NULL,
  `id_ProcedureStart` int(11) NOT NULL AUTO_INCREMENT,
  `TimeOfCreate` datetime NOT NULL,
  `id_MRequest` int(11) DEFAULT NULL,
  `NextSessionDate` datetime DEFAULT NULL,
  `ReadyToRegistrate` bit(1) NOT NULL DEFAULT b'0',
  `TimeOfLastChecking` datetime DEFAULT NULL,
  `DebtorCategory` char(1) NOT NULL DEFAULT 'n',
  `ShowToSRO` bit(1) NOT NULL DEFAULT b'0',
  `id_SRO` int(11) DEFAULT NULL,
  `DebtorAddress` varchar(250) DEFAULT NULL,
  `ApplicantName` varchar(250) DEFAULT NULL,
  `ApplicantINN` varchar(12) DEFAULT NULL,
  `ApplicantSNILS` varchar(13) DEFAULT NULL,
  `ApplicantOGRN` varbinary(16) DEFAULT NULL,
  `ApplicantAddress` varchar(250) DEFAULT NULL,
  `ApplicantCategory` char(1) DEFAULT NULL,
  `DateOfRequestAct` datetime DEFAULT NULL,
  `DateOfAcceptance` datetime DEFAULT NULL,
  `DateOfPrescription` datetime DEFAULT NULL,
  `efrsbPrescriptionID` varchar(32) DEFAULT NULL,
  `efrsbPrescriptionNumber` varchar(30) DEFAULT NULL,
  `efrsbPrescriptionPublishDate` datetime DEFAULT NULL,
  `AddedBySRO` bit(1) NOT NULL DEFAULT b'0',
  `CourtDecisionURL` varchar(250) DEFAULT NULL,
  `efrsbPrescriptionAddInfo` varchar(250) DEFAULT NULL,
  `CourtDecisionAddInfo` varchar(250) DEFAULT NULL,
  `PrescriptionURL` varchar(250) DEFAULT NULL,
  `PrescriptionAddInfo` varchar(250) DEFAULT NULL,
  `DebtorName2` varchar(250) DEFAULT NULL,
  `DebtorINN2` varchar(12) DEFAULT NULL,
  `DebtorSNILS2` varchar(13) DEFAULT NULL,
  `DebtorOGRN2` varbinary(16) DEFAULT NULL,
  `DebtorAddress2` varchar(250) DEFAULT NULL,
  `DebtorCategory2` char(1) DEFAULT NULL,
  `TimeOfLastCheckingEFRSB` datetime DEFAULT NULL,
  PRIMARY KEY (`id_ProcedureStart`),
  KEY `Ref_ProcedureStart_Contract` (`id_Contract`),
  KEY `Ref_ProcedureStart_Court` (`id_Court`),
  KEY `Ref_ProcedureStart_Manager` (`id_Manager`),
  KEY `Ref_ProcedureStart_User` (`id_MUser`),
  KEY `Ref_Start_Request` (`id_MRequest`),
  KEY `Ref_Start_SRO` (`id_SRO`),
  CONSTRAINT `Ref_ProcedureStart_Contract` FOREIGN KEY (`id_Contract`) REFERENCES `contract` (`id_Contract`),
  CONSTRAINT `Ref_ProcedureStart_Court` FOREIGN KEY (`id_Court`) REFERENCES `court` (`id_Court`),
  CONSTRAINT `Ref_ProcedureStart_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `Ref_ProcedureStart_User` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`),
  CONSTRAINT `Ref_Start_Request` FOREIGN KEY (`id_MRequest`) REFERENCES `mrequest` (`id_MRequest`),
  CONSTRAINT `Ref_Start_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `efrsb_sro` (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedurestart`
--

LOCK TABLES `procedurestart` WRITE;
/*!40000 ALTER TABLE `procedurestart` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `procedurestart` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `procedureusing`
--

DROP TABLE IF EXISTS `procedureusing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedureusing` (
  `ContractNumber` varchar(40) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `LicenseToken` text,
  `OGRN` varbinary(16) DEFAULT NULL,
  `SNILS` varchar(13) DEFAULT NULL,
  `UsingTime` datetime NOT NULL,
  `IP` varchar(40) NOT NULL,
  `Section` char(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedureusing`
--

LOCK TABLES `procedureusing` WRITE;
/*!40000 ALTER TABLE `procedureusing` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `procedureusing` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `procedureusing_indexed`
--

DROP TABLE IF EXISTS `procedureusing_indexed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedureusing_indexed` (
  `ContractNumber` varchar(10) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `IP` varchar(40) DEFAULT NULL,
  `OGRN` varbinary(16) DEFAULT NULL,
  `SNILS` varchar(13) DEFAULT NULL,
  `Section` char(4) DEFAULT NULL,
  `UsingTime` datetime DEFAULT NULL,
  KEY `byContractNumber` (`ContractNumber`),
  KEY `byIP` (`IP`),
  KEY `bySection` (`Section`),
  KEY `byUsingTime` (`UsingTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedureusing_indexed`
--

LOCK TABLES `procedureusing_indexed` WRITE;
/*!40000 ALTER TABLE `procedureusing_indexed` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `procedureusing_indexed` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `push_receiver`
--

DROP TABLE IF EXISTS `push_receiver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `push_receiver` (
  `Check_day_off` bit(1) NOT NULL DEFAULT b'0',
  `Destination` varchar(200) DEFAULT NULL,
  `For_Auction` char(1) DEFAULT NULL,
  `For_Committee` char(1) DEFAULT NULL,
  `For_CourtDecision` char(1) DEFAULT NULL,
  `For_MeetingPB` char(1) DEFAULT NULL,
  `For_MeetingWorker` char(1) DEFAULT NULL,
  `For_Meeting` char(1) DEFAULT NULL,
  `Time_finish` int(11) DEFAULT NULL,
  `Time_start` int(11) DEFAULT NULL,
  `Transport` char(1) DEFAULT NULL,
  `id_Push_receiver` int(11) NOT NULL AUTO_INCREMENT,
  `Timezone` int(11) DEFAULT NULL,
  `Category` char(1) NOT NULL,
  `id_Manager_Contract_MUser` int(11) NOT NULL,
  `For_Message_after` datetime DEFAULT NULL,
  `ReceiverGUID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_Push_receiver`),
  UNIQUE KEY `byCategoryId` (`Category`,`id_Manager_Contract_MUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `push_receiver`
--

LOCK TABLES `push_receiver` WRITE;
/*!40000 ALTER TABLE `push_receiver` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `push_receiver` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `push_request`
--

DROP TABLE IF EXISTS `push_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `push_request` (
  `DaysBefore` smallint(6) NOT NULL,
  `MessageNumber` varchar(30) NOT NULL,
  `id_Push_receiver` int(11) NOT NULL,
  `id_Push_request` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Push_request`),
  KEY `RefPush_request_Event` (`MessageNumber`),
  KEY `RefPush_request_Push_receiver` (`id_Push_receiver`),
  CONSTRAINT `RefPush_request_Push_receiver` FOREIGN KEY (`id_Push_receiver`) REFERENCES `push_receiver` (`id_Push_receiver`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `push_request`
--

LOCK TABLES `push_request` WRITE;
/*!40000 ALTER TABLE `push_request` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `push_request` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `pushed_event`
--

DROP TABLE IF EXISTS `pushed_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pushed_event` (
  `Time_pushed` datetime DEFAULT NULL,
  `Time_dispatched` datetime NOT NULL,
  `id_Event` int(11) NOT NULL,
  `id_Push_receiver` int(11) NOT NULL,
  `ErrorText` text,
  PRIMARY KEY (`id_Event`,`id_Push_receiver`),
  KEY `RefPushed_event_Event` (`id_Event`),
  KEY `RefPushed_event_Push_receiver` (`id_Push_receiver`),
  CONSTRAINT `RefPushed_event_Event` FOREIGN KEY (`id_Event`) REFERENCES `event` (`id_Event`),
  CONSTRAINT `RefPushed_event_Push_receiver` FOREIGN KEY (`id_Push_receiver`) REFERENCES `push_receiver` (`id_Push_receiver`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pushed_event`
--

LOCK TABLES `pushed_event` WRITE;
/*!40000 ALTER TABLE `pushed_event` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `pushed_event` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `pushed_message`
--

DROP TABLE IF EXISTS `pushed_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pushed_message` (
  `Time_dispatched` datetime NOT NULL,
  `Time_pushed` datetime DEFAULT NULL,
  `id_Message` int(11) NOT NULL,
  `id_Push_receiver` int(11) NOT NULL,
  `ErrorText` text,
  PRIMARY KEY (`id_Message`,`id_Push_receiver`),
  KEY `RefPushed_message_Message` (`id_Message`),
  KEY `RefPushed_message_receiver` (`id_Push_receiver`),
  CONSTRAINT `RefPushed_message_Message` FOREIGN KEY (`id_Message`) REFERENCES `message` (`id_Message`) ON DELETE CASCADE,
  CONSTRAINT `RefPushed_message_receiver` FOREIGN KEY (`id_Push_receiver`) REFERENCES `push_receiver` (`id_Push_receiver`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pushed_message`
--

LOCK TABLES `pushed_message` WRITE;
/*!40000 ALTER TABLE `pushed_message` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `pushed_message` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `Name` varchar(250) NOT NULL,
  `OKATO` char(6) DEFAULT NULL,
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Region`),
  UNIQUE KEY `byRegionName` (`Name`),
  UNIQUE KEY `byOKATO` (`OKATO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request` (
  `Body` longblob NOT NULL,
  `id_MUser` int(11) NOT NULL,
  `id_Request` int(11) NOT NULL AUTO_INCREMENT,
  `managerEfrsbNumber` varchar(50) DEFAULT NULL,
  `managerInn` varchar(12) DEFAULT NULL,
  `managerName` varchar(100) NOT NULL,
  `id_Debtor` int(11) NOT NULL,
  `State` char(1) NOT NULL DEFAULT 'a',
  `TimeLastChange` datetime NOT NULL,
  PRIMARY KEY (`id_Request`),
  KEY `refRequest_MUser` (`id_MUser`),
  KEY `Debtor_Request` (`id_Debtor`),
  CONSTRAINT `Debtor_Request` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `refRequest_MUser` FOREIGN KEY (`id_MUser`) REFERENCES `muser` (`id_MUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request`
--

LOCK TABLES `request` WRITE;
/*!40000 ALTER TABLE `request` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `request` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `rosreestr`
--

DROP TABLE IF EXISTS `rosreestr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rosreestr` (
  `Address` text NOT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  `Name` varchar(70) NOT NULL,
  `id_Region` int(11) NOT NULL,
  `id_Rosreestr` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Rosreestr`),
  KEY `refRosreestr_Region` (`id_Region`),
  CONSTRAINT `refRosreestr_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rosreestr`
--

LOCK TABLES `rosreestr` WRITE;
/*!40000 ALTER TABLE `rosreestr` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `rosreestr` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `sentemail`
--

DROP TABLE IF EXISTS `sentemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sentemail` (
  `Message` longblob,
  `EmailType` char(1) NOT NULL,
  `RecipientEmail` varchar(100) NOT NULL,
  `RecipientId` int(11) NOT NULL,
  `RecipientType` char(1) NOT NULL,
  `TimeSent` datetime DEFAULT NULL,
  `id_SentEmail` int(11) NOT NULL AUTO_INCREMENT,
  `SenderServer` varchar(50) DEFAULT NULL,
  `SenderUser` varchar(50) DEFAULT NULL,
  `ExtraParams` longblob,
  `TimeDispatch` datetime NOT NULL,
  PRIMARY KEY (`id_SentEmail`),
  KEY `byRecipient` (`RecipientId`,`RecipientType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sentemail`
--

LOCK TABLES `sentemail` WRITE;
/*!40000 ALTER TABLE `sentemail` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `sentemail` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `id_Session` int(11) NOT NULL AUTO_INCREMENT,
  `Token` varchar(100) DEFAULT NULL,
  `Who` char(1) NOT NULL,
  `id_Owner` int(11) NOT NULL,
  `TimeStarted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TimeLastUsed` datetime NOT NULL,
  PRIMARY KEY (`id_Session`),
  UNIQUE KEY `byWhoOwner` (`Who`,`id_Owner`),
  UNIQUE KEY `byToken` (`Token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `srodocument`
--

DROP TABLE IF EXISTS `srodocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `srodocument` (
  `Body` longblob NOT NULL,
  `DocumentType` char(1) NOT NULL,
  `FileName` varchar(100) NOT NULL,
  `PubLock` longblob,
  `id_SRODocument` int(11) NOT NULL AUTO_INCREMENT,
  `id_SRO` int(11) NOT NULL,
  PRIMARY KEY (`id_SRODocument`),
  KEY `refSRO_Document` (`id_SRO`),
  CONSTRAINT `refSRO_Document` FOREIGN KEY (`id_SRO`) REFERENCES `efrsb_sro` (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `srodocument`
--

LOCK TABLES `srodocument` WRITE;
/*!40000 ALTER TABLE `srodocument` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `srodocument` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `sub_level`
--

DROP TABLE IF EXISTS `sub_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_level` (
  `Reglament_Date` date DEFAULT NULL,
  `Reglament_Title` varchar(250) DEFAULT NULL,
  `Reglament_Url` text,
  `StartDate` date NOT NULL,
  `Sum_Common` decimal(10,2) DEFAULT NULL,
  `Sum_Employable` decimal(10,2) DEFAULT NULL,
  `Sum_Infant` decimal(10,2) DEFAULT NULL,
  `Sum_Pensioner` decimal(10,2) DEFAULT NULL,
  `id_Contract` int(11) DEFAULT NULL,
  `id_Region` int(11) NOT NULL,
  `id_Sub_level` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Sub_level`),
  KEY `RefSubLevel_Contract` (`id_Contract`),
  KEY `Ref_Sub_level_Region` (`id_Region`),
  CONSTRAINT `RefSubLevel_Contract` FOREIGN KEY (`id_Contract`) REFERENCES `contract` (`id_Contract`),
  CONSTRAINT `Ref_Sub_level_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_level`
--

LOCK TABLES `sub_level` WRITE;
/*!40000 ALTER TABLE `sub_level` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `sub_level` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `MigrationNumber` varchar(250) NOT NULL,
  `MigrationName` varchar(250) NOT NULL,
  `MigrationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MigrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `tbl_migration` VALUES ('m001','Content_hash','2020-11-29 09:09:40'),('m002','MUserChanging','2020-11-29 09:09:40'),('m003','SentEmail','2020-11-29 09:09:40'),('m004','UserIndex_Sender','2020-11-29 09:09:40'),('m005','ManagerPricedureExtraParams','2020-11-29 09:09:40'),('m006','MeetingState','2020-11-29 09:09:40'),('m007','ReduceEmailToSend','2020-11-29 09:09:40'),('m008','AddEmailErrorText','2020-11-29 09:09:40'),('m009','PDataProcessed','2020-11-29 09:09:40'),('m010','AddPDataByRevision','2020-11-29 09:09:40'),('m011','AddAward','2020-11-29 09:09:40'),('m012','AddAwardSignature','2020-11-29 09:09:40'),('m013','ModifyAward','2020-11-29 09:09:40'),('m014','AwardEfrsbNumberToINN','2020-11-29 09:09:40'),('m015','AddRequests','2020-11-29 09:09:40'),('m016','FixRequests','2020-11-29 09:09:40'),('m017','AddDebtor','2020-11-29 09:09:41'),('m018','AddEfrsb','2020-11-29 09:09:41'),('m019','AddRequestState','2020-11-29 09:09:41'),('m020','AddBankroTechAcc','2020-11-29 09:09:41'),('m021','AddBankroTechFields','2020-11-29 09:09:41'),('m022','FixVerified','2020-11-29 09:09:42'),('m023','FixMockEfrsb','2020-11-29 09:09:42'),('m024','FixDebtorVerify','2020-11-29 09:09:42'),('m025','FixMockEfrsb2','2020-11-29 09:09:42'),('m026','FixProcedureVerify','2020-11-29 09:09:42'),('m027','FixMProcedureIndex','2020-11-29 09:09:42'),('m028','AddAccessLog','2020-11-29 09:09:42'),('m029','AddBankroTechAccVerified','2020-11-29 09:09:42'),('m030','FixCtbId','2020-11-29 09:09:43'),('m031','AddCtbAccountLimit','2020-11-29 09:09:43'),('m032','AddSubLevel','2021-03-01 07:22:56'),('m033','AddFaAccVerified','2021-03-01 07:22:56'),('m034','AddManagerDateDelete','2021-03-01 07:22:56'),('m035','FixMockEfrsb3','2021-03-01 07:22:56'),('m036','AddPushReceiver','2021-03-01 07:22:56'),('m037','AddEvent','2021-03-01 07:22:56'),('m038','AddMockEfrsbSRO','2021-03-01 07:22:56'),('m039','FixEvent','2021-03-01 07:22:56'),('m040','FixSubLevelSum','2021-03-01 07:22:56'),('m041','AddMData','2021-03-01 07:22:56'),('m042','AddLogForSublevelInfo','2021-03-01 07:22:57'),('m043','FixPushReceiver','2021-03-01 07:22:57'),('m044','AddMessage','2021-03-01 07:22:57'),('m045','FixMessageEventIndexes','2021-03-01 07:22:57'),('m046','AddAnketaNP','2021-03-01 07:22:57'),('m047','AddAnketaNP_byINN','2021-03-01 07:22:57'),('m048','AddDebtorLastMessageDate','2021-03-01 07:22:57'),('m049','FixAnketaNPCascadeDelete','2021-03-01 07:22:57'),('m050','AddHasIncomingOutcoming','2021-03-01 07:22:57'),('m051','FixPushReceiver2','2021-03-01 07:22:57'),('m052','ChangePushRelations','2021-03-01 07:22:57'),('m053','AddMUserCreatedTime','2021-03-01 07:22:57'),('m054','AddApplication','2021-03-01 07:22:57'),('m055','AddMUserConfirmationTokens','2021-03-01 07:22:58'),('m056','FixManagerIdContract','2021-03-01 07:22:58'),('m057','AddPushed_message','2021-03-01 07:22:58'),('m058','AddCalendarCase','2021-03-01 07:22:58'),('m059','AddEfrsbJobs','2021-03-01 07:22:58'),('m060','AddProcedureStart','2021-03-01 07:22:58'),('m061','FixJobSiteDescription','2021-03-01 07:22:58'),('m062','AddMRequest','2021-03-01 07:22:58'),('m063','AddProcedureStartNextSessionDate','2021-03-01 07:22:58'),('m064','AddProcedureUsing','2021-03-01 07:22:58'),('m065','AddJobIndexes','2021-03-01 07:22:58'),('m066','AddJobCascadeDelete','2021-03-01 07:22:58'),('m067','AddStartCategory','2021-03-01 07:22:58'),('m068','AddCourtNames','2021-03-01 07:22:59'),('m069','AddProcedureUsingSection','2021-03-01 07:22:59'),('m070','AddApplicationRTK','2021-11-12 00:05:18'),('m071','StartsForSRO','2021-11-12 00:05:18'),('m072','AddMRequestExtraFields','2021-11-12 00:05:18'),('m073','AddReceiverGUID','2021-11-12 00:05:19'),('m074','AddIsActiveEvent','2021-11-12 00:05:19'),('m075','AddUniqueEfrsbId','2021-11-12 00:05:19'),('m076','AddPushErrorText','2021-11-12 00:05:19'),('m077','FixMockEfrsb4','2021-11-12 00:05:19'),('m078','AddCourtAddress','2021-11-12 00:05:19'),('m079','AddMRequestApplicantFields','2021-11-12 00:05:19'),('m080','DeleteProcedureStartDateOfAgree','2021-11-12 00:05:19'),('m081','AddManagerChangeEmail','2021-11-12 00:05:19'),('m082','AddMockEfrsbEmail','2021-11-12 00:05:19'),('m083','AddWcalendar','2021-11-12 00:05:19'),('m084','AddProcedureStartPrescriptionFields','2021-11-12 00:05:19'),('m085','AddFaUsing','2021-11-12 00:05:20'),('m086','AddProcedureStartAddedBySRO','2021-11-12 00:05:20'),('m087','AddSroRobotLog','2021-11-12 00:05:20'),('m088','FixUpdateEfrsbManager','2021-11-12 00:05:20'),('m089','AddProcedureUsingIndexed','2021-11-12 00:05:20'),('m090','AddJobPartStartedIndex','2021-11-12 00:05:20'),('m091','AddManagerDocument','2021-11-12 00:05:20'),('m092','FixMeetingDocument','2021-11-12 00:05:20'),('m093','AddDateOfCreationMRequest','2021-11-12 00:05:20'),('m094','FixManagerDocument','2021-11-12 00:05:20'),('m095','AddCourtDecisionURL','2021-11-12 00:05:20'),('m096','AddProtocolNumAndDate','2021-11-12 00:05:20'),('m097','AddManagerContractChange','2021-11-12 00:05:20'),('m098','AddRosreestr','2021-11-12 00:05:20'),('m099','FixEfrsbSro','2021-11-12 00:05:20'),('m100','AddPrescriptionFieldsAndAddInfoField','2021-11-12 00:05:21'),('m101','ProcedureStartMRequestDebtor2Fields','2021-11-12 00:05:21'),('m102','ProcedureStartTimeOfVerificationEFRSB','2021-11-12 00:05:21'),('m103','AddElektroKKLogs','2021-11-12 00:05:21'),('m104','AddElektroKKLogs2','2021-11-12 00:05:21'),('m105','AddApiErrorLog','2021-11-12 00:05:21'),('m106','AddFieldsGoogleCalendar','2021-11-12 00:05:21'),('m107','AddPreDebtorPDDocumentPDDocFile','2021-11-12 00:05:21'),('m108','AddMUserEscortFields','2021-11-12 00:05:21'),('m109','FixNaturalKey','2021-11-12 00:05:21'),('m110','AddLogTypesAutologinErros','2021-11-12 00:05:21'),('m111','AddLogTypesAutologinError2','2021-11-12 00:05:21');
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `vote`
--

DROP TABLE IF EXISTS `vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote` (
  `id_Vote` int(11) NOT NULL AUTO_INCREMENT,
  `id_Meeting` int(11) NOT NULL,
  `CabinetKey` varchar(20) NOT NULL,
  `Confirmation_code` varchar(10) DEFAULT NULL,
  `Answers` longblob,
  `Member` longblob,
  `CabinetTime` datetime NOT NULL,
  `Confirmation_code_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id_Vote`),
  KEY `byCabinetKey` (`CabinetKey`),
  KEY `refVote_Meeting` (`id_Meeting`),
  CONSTRAINT `refVote_Meeting` FOREIGN KEY (`id_Meeting`) REFERENCES `meeting` (`id_Meeting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote`
--

LOCK TABLES `vote` WRITE;
/*!40000 ALTER TABLE `vote` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `vote` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `vote_document`
--

DROP TABLE IF EXISTS `vote_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote_document` (
  `Body` longblob,
  `FileName` varchar(40) NOT NULL,
  `Parameters` longblob,
  `Vote_document_time` datetime NOT NULL,
  `Vote_document_type` char(1) NOT NULL,
  `id_Vote_document` int(11) NOT NULL AUTO_INCREMENT,
  `id_Vote` int(11) NOT NULL,
  PRIMARY KEY (`id_Vote_document`),
  KEY `refVote_document_Vote` (`id_Vote`),
  CONSTRAINT `refVote_document_Vote` FOREIGN KEY (`id_Vote`) REFERENCES `vote` (`id_Vote`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote_document`
--

LOCK TABLES `vote_document` WRITE;
/*!40000 ALTER TABLE `vote_document` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `vote_document` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `vote_log`
--

DROP TABLE IF EXISTS `vote_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote_log` (
  `Vote_log_time` datetime NOT NULL,
  `Vote_log_type` char(1) NOT NULL,
  `body` longblob,
  `id_Vote_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_Vote` int(11) NOT NULL,
  PRIMARY KEY (`id_Vote_log`),
  KEY `refVote_log_Vote` (`id_Vote`),
  CONSTRAINT `refVote_log_Vote` FOREIGN KEY (`id_Vote`) REFERENCES `vote` (`id_Vote`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote_log`
--

LOCK TABLES `vote_log` WRITE;
/*!40000 ALTER TABLE `vote_log` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `vote_log` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `wcalendar`
--

DROP TABLE IF EXISTS `wcalendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcalendar` (
  `id_Wcalendar` int(11) NOT NULL AUTO_INCREMENT,
  `id_Region` int(11) DEFAULT NULL,
  `Revision` int(11) NOT NULL,
  `Year` int(11) NOT NULL,
  `Days` longblob,
  PRIMARY KEY (`id_Wcalendar`),
  UNIQUE KEY `byRevision` (`Revision`),
  UNIQUE KEY `by_id_Region_Year` (`id_Region`,`Year`),
  KEY `refWcalendar_Region` (`id_Region`),
  CONSTRAINT `refWcalendar_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wcalendar`
--

LOCK TABLES `wcalendar` WRITE;
/*!40000 ALTER TABLE `wcalendar` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `wcalendar` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping routines for database 'datamartdevel'
--
/*!50003 DROP FUNCTION IF EXISTS `FaUsing_Section_readable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `FaUsing_Section_readable`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return concat(
case aSection
when 1  then 'Создать новый финанализ'
when 2  then 'Импорт/балансы/1С'
when 3  then 'Импорт/балансы/ФНС'
when 4  then 'Импорт/КМ/ПАУ'
when 5  then 'Импорт/КМ/витрина'
when 6  then 'Импорт/Сделки/Excel'
when 7  then 'Сделки/Добавить'
when 8  then 'Сделки/Импортировать банковскую выписку'
when 9  then 'Сделки/Получить-Обновить аналитику'
when 10  then 'Документ/О признаказ плохого банкротства'
when 11  then 'Документ/Анализ контрагентов'
else 'неучтённое действие'
end
,' (сч.',aSection,')'
);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getNotifyDate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `getNotifyDate`(
	notifyType CHAR(1),
    eventTime DATETIME,
    checkDayOff BIT(1)
) RETURNS datetime
BEGIN
    DECLARE dateResult DATETIME;
	CASE
	  WHEN notifyType = 'a' THEN SET dateResult = '2000-01-01';
      WHEN notifyType = 'b' THEN SET dateResult = eventTime - interval 1 day;
      WHEN notifyType = 'c' THEN SET dateResult = eventTime - interval 3 day;
      WHEN notifyType = 'd' THEN SET dateResult = eventTime - interval 5 day;
      WHEN notifyType = 'e' THEN SET dateResult = eventTime - interval 15 day;
	  ELSE SET dateResult = eventTime;
	END CASE;
    CASE
	  WHEN WEEKDAY(dateResult) = 5 && checkDayOff = 0 THEN SET dateResult = dateResult - interval 1 day;
	  WHEN WEEKDAY(dateResult) = 6 && checkDayOff = 0 THEN SET dateResult = dateResult - interval 2 day;
	  ELSE SET dateResult = dateResult;
	END CASE;
	RETURN dateResult;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `PeproUsing_Section_readable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `PeproUsing_Section_readable`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return concat(
case aSection
when 1  then 'Создать исходящее'
when 2  then 'Печатать конверт'
when 3  then 'Печатать уведомление'
when 4  then 'Печатать опись вложение'
when 5  then 'Создать реестр'
when 6  then 'Создать партию отправлений на сайте Почта России'
when 7  then 'Создать файл для загрузки на сайт Почта России'
when 8  then 'Рассылка'
when 9  then 'Добавить шаблон'
when 10 then 'Добавить входящее'
when 11 then 'Печатать журнал входящих'
else 'неучтённое действие'
end
,' (сч.',aSection,')'
);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProceduDebtor_INN_OGRN_SNILS_lp_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `ProceduDebtor_INN_OGRN_SNILS_lp_id`(
  aINN   varchar(12)
 ,aOGRN  varchar(16)
 ,aSNILS varchar(13)
) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  if (aSNILS is not null and 0<>length(aSNILS) or length(aINN)=12) then
    return null;
  else
    return concat(ifnull(aINN,''),'.',ifnull(aOGRN,''));
  end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProceduDebtor_INN_OGRN_SNILS_np_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `ProceduDebtor_INN_OGRN_SNILS_np_id`(
  aINN   varchar(12)
 ,aOGRN  varchar(16)
 ,aSNILS varchar(13)
) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  if (aSNILS is not null and 0<>length(aSNILS) or length(aINN)=12) then
    return concat(ifnull(aINN,''),'.',ifnull(aSNILS,''));
  else
    return null;
  end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProcedureType_SafeDBValueForShort` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `ProcedureType_SafeDBValueForShort`(short_name char(3)) RETURNS varchar(2) CHARSET utf8
    DETERMINISTIC
begin
  case short_name
    when 'Н'  then return 'n';
    when 'Нс' then return 'n';
    when 'КП' then return 'k';
    when 'КПс' then return 'k';
    when 'РИ' then return 'i';
    when 'РД' then return 'd';
    when 'ВУ' then return 'v';
    when 'ВУс' then return 'v';
    when 'ОД' then return 'o';
    when 'ОДс' then return 'o';
    when 'Б'  then return 'b';
    else return 1/0;
  end case;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProcedureType_SafeShortForDBValue` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `ProcedureType_SafeShortForDBValue`(db_value char(1)) RETURNS varchar(2) CHARSET utf8
    DETERMINISTIC
begin
	case db_value
		when 'n' then return 'Н';
		when 'k' then return 'КП';
		when 'i' then return 'РИ';
		when 'd' then return 'РД';
		when 'v' then return 'ВУ';
		when 'o' then return 'ОД';
		when 'b' then return 'Б';
		else return db_value;
	end case;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProcedureUsing_Section_readable` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `ProcedureUsing_Section_readable`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return if(83>convert(aSection,unsigned integer),ProcedureUsing_Section_readable_3_20(aSection),concat(
case aSection

when 83 then 'импорт банк выписки/начать'
when 84 then 'импорт банк выписки/завершить'
when 85 then 'импорт ден опер из excel/завершить'
when 86 then 'кнопка Должник'
when 87 then 'Кнопка Процедура'
when 88 then 'Подпись/загрузить'
when 89 then 'Подпись АУ/Добавить'
when 90 then 'Инструкция по добавлению подписи'
when 91 then 'Инструкция по массовому погашению'
when 92 then 'Анализ сделок/открытие заключ преднамер фикт'
when 93 then 'Финальный отчет на ЕФРСБ'
when 94 then 'Дебиторка/указать погаш сумму ссылка'
when 95 then 'Дебиторка/добавление погашения'
when 96 then 'Дебиторка/погашение/кнопка Создать доход'
when 97 then 'Дебиторка/погашение/учесть старые записи как погашения'
when 98 then 'Список процедур в excel'
when 99 then 'залоговый счет/добавление второго и последующего'
when 100 then 'Учетная группа Залоговый/открытие'
when 101 then 'Импорт объектов КМ из excel'
when 102 then 'Импорт НДС из excel'
when 103 then 'Импорт ден средств КМ из excel'
when 104 then 'Импорт дебиторской задолженности из excel'
when 105 then 'ФА ФЛ/показывать требования не включенные в реестр'
when 106 then 'Сводный анализ дебиторской задолженности'
when 107 then 'документ архива младше 30 дней'
when 108 then 'документ архива 30-180 дней'
when 109 then 'документ архива старше 180 дней'
when 110 then 'Текущее требование/зарегистрировать'
when 111 then 'Свойства должника/кнопка Печать'
when 112 then 'Дебиторка/добавление фото'
when 113 then 'Нажатие на Уведомление'
when 114 then 'Нажатие на уведомление по Мобилке'

else 'неучтённое действие'
end
,' (сч.',aSection,')'
));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProcedureUsing_Section_readable_3_19` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `ProcedureUsing_Section_readable_3_19`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
 return concat(
case aSection
when 1 then 'СК/сохранить'
when 2 then 'СК/протокол'
when 3 then 'СК/к повестке'
when 4 then 'СК/протокол открыть'
when 5 then 'РТК/создание ТК'
when 6 then 'РТК/МПТ/запланировать'
when 7 then 'РТК/печатать'
when 8 then 'Финансы/создание расхода (старая версия)'
when 9 then 'Делопроизводство/регистрация входящего'
when 10 then 'Делопроизводство/создать партию на сайте Почты'
when 11 then 'КМ/сохранить ИГ'
when 12 then 'КМ/создать объект'
when 13 then 'КМ/создать дебиторку'
when 14 then 'Планирование/сохранить задачу'
when 15 then 'Торги/сохранить лот'
when 16 then 'анализ сделок/добавить'
when 17 then 'ПГЗ/Должник/пакет'
when 18 then 'ПГЗ/Должник/печать'
when 19 then 'ПГЗ/Должник/редактировать'
when 20 then 'ПГЗ/КДЛ/пакет'
when 21 then 'ПГЗ/КДЛ/печать'
when 22 then 'ПГЗ/КДЛ/редактировать'
when 23 then 'ПГЗ/супруг/пакет'
when 24 then 'ПГЗ/супруг/печать'
when 25 then 'ПГЗ/супруг/редактировать'
when 26 then 'ЕФРСБ/подать'
when 27 then 'финанализ/фл/сформировать'
when 28 then 'ДПМ/ефрсб/опубликовать/введение/ри'
when 29 then 'ДПМ/ефрсб/опубликовать/введение/кп'
when 30 then 'ДПМ/СК/выполнено'
when 31 then 'РЖП/зарегистрировать'
when 32 then 'отчёт/превью/получатели'
when 33 then 'отчёт/превью/word'
when 34 then 'работники/уведомление'
when 35 then 'работники/уведомление/печать'
when 36 then 'работники/уведомление/редактировать'
when 37 then 'запросыпосчетам/основной/запросить'
when 38 then 'запросыпосчетам/основной/печать'
when 39 then 'запросыпосчетам/основной/редактировать'
when 40 then 'запросыпосчетам/закрыть/запросить'
when 41 then 'запросыпосчетам/закрыть/печать'
when 42 then 'запросыпосчетам/закрыть/редактировать'
else 'неучтённое действие'
end
,' (сч.',aSection,')'
);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProcedureUsing_Section_readable_3_20` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `ProcedureUsing_Section_readable_3_20`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return if(43>convert(aSection,unsigned integer),ProcedureUsing_Section_readable_3_19(aSection),concat(
case aSection

when 43 then 'Финансы/расход/прож.минимум'
when 44 then 'Финансы/расход/ефрсб'
when 45 then 'Финансы/расход/почта'
when 46 then 'Финансы/расход/коммерсант'
when 47 then 'Финансы/доход'
when 48 then 'Финансы/фото'
when 49 then 'Финансы/создать учётную группу'

when 50 then 'Финансы/запросить'
when 51 then 'Финансы/счёт/залоговый'
when 52 then 'Финансы/счёт/задатки'

when 53 then 'Работники/добавить'
when 54 then 'Работники/импортировать'
when 55 then 'Работники/собрание'

when 56 then 'Права/группа'
when 57 then 'Права/изменить уровень доступа'
when 58 then 'Права/назначить группу для процедуры'

when 59 then 'ФЛ/2НДФЛ'
when 60 then 'ФЛ/доход'
when 61 then 'ФЛ/супруг'
when 62 then 'ФЛ/иждивенец'

when 63 then 'Субсидиарка/Собрать'
when 64 then 'Субсидиарка/КДЛ'
when 65 then 'Субсидиарка/Нарушение'
when 66 then 'Субсидиарка/Привлечение'

when 67 then 'РТК/перед ликв.квотой'
when 68 then 'РТК/сокредитор'
when 69 then 'РТК/погашение'

when 70 then 'Обмен/настройка/фото'
when 71 then 'Обмен/настройка/авто'
when 72 then 'Обмен/настройка/google calendar'

when 73 then 'ПГЗ/должник/pdf'
when 74 then 'ПГЗ/супруг/pdf'
when 75 then 'ПГЗ/КДЛ/pdf'
when 76 then 'ПГЗ/банки/pdf'

when 77 then 'ефрсб/введение/мастер'
when 78 then 'ефрсб/введение/меню/подача'
when 79 then 'ефрсб/введение/редактировать'

when 80 then 'собрание дольщиков'

when 81 then 'Финансы/расход/общего вида'

when 82 then 'Документ/открытие из архива'

else 'неучтённое действие'
end
,' (сч.',aSection,')'
));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `short_fio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` FUNCTION `short_fio`(LastName text, FistName text, MiddleName text) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return concat(LastName
  	, if(FistName   IS NULL || 0=length(FistName),   '', concat(' ', substr(FistName,   1, 1), '.'))
  	, if(MiddleName IS NULL || 0=length(MiddleName), '', concat(' ', substr(MiddleName, 1, 1), '.')));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Add_Debtor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `Add_Debtor`(
	  pInn VARCHAR(12)
	, pDebtorName VARCHAR(250)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)

	, out opid_Debtor int
)
begin
	declare lVerificationState char(1);
	declare lSnils VARCHAR(13);
	declare lInn VARCHAR(12);
	declare lOgrn VARCHAR(16);

	declare exit handler for 1172 
	begin 
		select null into opid_Debtor;
		select "Debtor" as error_type, 'w' as error_details; 
	end;

	call Fix_Debtor(pInn,pOgrn,pSnils,lInn,lOgrn,lSnils);

	if (lInn is null && lOgrn is null && lSnils is null) then
		select "Debtor" as error_type, 'i' as error_details; 
	else		
		select id_Debtor, VerificationState into opid_Debtor, lVerificationState from Debtor
		where ( (lInn   is not null and INN   is not null and   INN=lInn)
		     or (lSnils is not null and SNILS is not null and SNILS=lSnils)
		     or (lOgrn  is not null and OGRN  is not null and  OGRN=lOgrn))
		and (lInn   is null or Debtor.INN   is null or   lInn=Debtor.INN)
		and (lSnils is null or Debtor.SNILS is null or lSnils=Debtor.SNILS)
		and (lOgrn  is null or Debtor.OGRN  is null or  lOgrn=Debtor.OGRN)
		;
		if (opid_Debtor is null) then
			insert into Debtor
			set Name= pDebtorName, INN= lInn, SNILS= lSnils, OGRN= lOgrn;
			select last_insert_id() into opid_Debtor;
		else
			if ('v'=lVerificationState) then 
				select null into lVerificationState;
			elseif (lVerificationState is null || 's'=lVerificationState) then 
				update Debtor
				set Name=ifnull(pDebtorName,Name), INN=ifnull(lInn,INN), OGRN=ifnull(lOgrn,OGRN), SNILS=ifnull(lSnils,SNILS)
				where id_Debtor=opid_Debtor;
			else
				select null into opid_Debtor;
				select "Debtor" as error_type, lVerificationState as error_details;
			end if;
		end if;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Add_Manager` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `Add_Manager`(
	  pid_Contract int

	, pmFirstName VARCHAR(50)
	, pmMiddleName VARCHAR(50)
	, pmLastName VARCHAR(50)
	, pmEFRSBNum VARCHAR(50)
	, pmInn VARCHAR(12)
	, pmBTAccount TEXT

	, out opid_Manager int
)
begin
	declare lVerificationState char(1);
	declare lpmBTAccount TEXT;

	select id_Manager, VerificationState,  pmBTAccount
	into opid_Manager, lVerificationState, lpmBTAccount
	from Manager
	where id_Contract=pid_Contract && (efrsbNumber=pmEFRSBNum || INN=pmInn);

	if opid_Manager is null then
		insert into Manager
		set id_Contract= pid_Contract, efrsbNumber= pmEFRSBNum,
			firstName= pmFirstName, middleName= pmMiddleName, lastName= pmLastName, INN=pmInn, BankroTechAcc=pmBTAccount;
		select last_insert_id() into opid_Manager;
	else
		if ('v'=lVerificationState) then 
			update Manager
			set BankroTechAcc=pmBTAccount
			where id_Manager=opid_Manager;
		elseif (lVerificationState is null || 's'=lVerificationState) then 
			update Manager
			set firstName= pmFirstName, middleName= pmMiddleName, lastName= pmLastName, INN=pmInn, BankroTechAcc=pmBTAccount
			where id_Manager=opid_Manager;
		else
			select null into opid_Manager;
			select "Manager" as error_type, lVerificationState as error_details;
		end if;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Add_MProcedure` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `Add_MProcedure`(
	  pid_Manager int

	, pInn VARCHAR(12)
	, pDebtorName VARCHAR(250)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)

	, pCasenumber VARCHAR(60)
	, pFeatures CHAR(1)
	, pProcedure_type CHAR(3)

	, out opid_MProcedure int
)
begin
	declare lid_Debtor int;
	declare lProcedure_type CHAR(2);
	declare lProcedure_VerificationState char(1);
	declare lDebtor_VerificationState char(1);

	declare lDebtor_Inn VARCHAR(12);
	declare lDebtor_Name VARCHAR(250);
	declare lDebtor_Ogrn VARCHAR(16);
	declare lDebtor_Snils VARCHAR(13);

	declare exit handler for 1172 
	begin 
		select null into opid_MProcedure;
		select "MProcedure" as error_type, 'w' as error_details; 
	end;

	select ProcedureType_SafeDBValueForShort(pProcedure_type) into lProcedure_type;

	select MProcedure.id_MProcedure, MProcedure.VerificationState, Debtor.VerificationState,
		   Debtor.Name,  Debtor.INN,  Debtor.OGRN,  Debtor.SNILS,  Debtor.id_Debtor
	into   opid_MProcedure,          lProcedure_VerificationState, lDebtor_VerificationState,
		   lDebtor_Name, lDebtor_Inn, lDebtor_Ogrn, lDebtor_Snils, lid_Debtor
	from MProcedure
	inner join Debtor on Debtor.id_Debtor=MProcedure.id_Debtor
	where casenumber=pCasenumber && procedure_type=lProcedure_type && id_Manager=pid_Manager
		&& (pInn is not null or pSnils is not null or pOgrn is not null)
		&& (   (pInn   is not null and Debtor.INN   is not null and   Debtor.INN=pInn)
		    or (pSnils is not null and Debtor.SNILS is not null and Debtor.SNILS=pSnils)
		    or (pOgrn  is not null and Debtor.OGRN  is not null and  Debtor.OGRN=pOgrn))
		&& (pInn   is null or Debtor.INN   is null or   pInn=Debtor.INN)
		&& (pSnils is null or Debtor.SNILS is null or pSnils=Debtor.SNILS)
		&& (pOgrn  is null or Debtor.OGRN  is null or  pOgrn=Debtor.OGRN)
		;

	if (opid_MProcedure is not null) then
		if ('v'=lProcedure_VerificationState) then 
			select null into lProcedure_VerificationState;
		elseif (lProcedure_VerificationState is null) then 
			if (lDebtor_VerificationState is not null && 's'<>lDebtor_VerificationState && 'v'<>lDebtor_VerificationState) then 
				select null into opid_MProcedure;
				select "Debtor" as error_type, lDebtor_VerificationState as error_details;
			elseif (ifnull(lDebtor_Name,'')<>ifnull(pDebtorName,'') or
			        ifnull(lDebtor_Inn,'')<>ifnull(pINN,'') or
			        ifnull(lDebtor_Ogrn,'')<>ifnull(pOgrn,'') or
			        ifnull(lDebtor_Snils,'')<>ifnull(pSnils,''))
			then
				update Debtor set INN=pINN, OGRN=pOgrn, SNILS=pSnils, Name=pDebtorName where id_Debtor=lid_Debtor;
			end if;
		elseif ('s'=lProcedure_VerificationState) then 
			select null into lProcedure_VerificationState;
		else
			select null into opid_MProcedure;
			select "MProcedure" as error_type, lProcedure_VerificationState as error_details;
		end if;
	else
		call Add_Debtor(pInn,pDebtorName,pOgrn,pSnils,lid_Debtor);
		if (lid_Debtor is not null) then
			insert into MProcedure
			set  casenumber= pCasenumber
				,procedure_type= lProcedure_type
				,id_Manager= pid_Manager
				,features= pFeatures
				,id_Debtor= lid_Debtor;
			select last_insert_id() into opid_MProcedure;

			insert into MProcedureUser(id_MProcedure,id_ManagerUser)
				select opid_MProcedure, mu.id_ManagerUser
				from ManagerUser mu
				left join MProcedureUser mpu on mpu.id_ManagerUser=mu.id_ManagerUser
			                                 && mpu.id_MProcedure=opid_MProcedure
				where mu.id_Manager=pid_Manager
				&& 0<>mu.DefaultViewer
				&& mpu.id_ManagerUser is null;
		end if;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `exit_on_error` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `exit_on_error`()
begin
	if (0<>@@error_count) then
		kill connection_id();
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Fix_Debtor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `Fix_Debtor`(
	  pInn VARCHAR(12)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)

	, out poInn VARCHAR(12)
	, out poOgrn VARCHAR(16)
	, out poSnils VARCHAR(13)
)
begin
	select replace(pSnils,' ','') into poSnils;
	select if(poSnils='00000000000' or poSnils='',null,poSnils) into poSnils;
	select replace(pInn,' ','') into poInn;
	select if(poInn='',null,poInn) into poInn;
	select replace(pOgrn,' ','') into poOgrn;
	select if(poOgrn='',null,poOgrn) into poOgrn;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SafeUpdateEfrsbManager` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `SafeUpdateEfrsbManager`(
  pid_Manager int(11),
  pArbitrManagerID int(11),
  pSRORegNum varchar(30),
  pFirstName varchar(50),
  pMiddleName varchar(50),
  pLastName varchar(50),
  pOGRNIP varchar(15),
  pINN varchar(12),
  pSRORegDate datetime,
  pRegNum varchar(30),
  pCorrespondenceAddress varchar(300),
  pRevision bigint(20)
)
BEGIN
	declare lArbitrManagerID int(11);
	declare lid_SRO int(11);

	select ArbitrManagerID into lArbitrManagerID
	from efrsb_manager where id_Manager=pid_Manager;

	if (lArbitrManagerID is null) then
		insert into efrsb_manager
		set id_Manager=pid_Manager, ArbitrManagerID=pArbitrManagerID
		, SRORegNum=pSRORegNum, SRORegDate=pSRORegDate, RegNum=pRegNum
		, FirstName=pFirstName, MiddleName=pMiddleName, LastName=pLastName
		, OGRNIP=pOGRNIP, INN=pINN
		, CorrespondenceAddress=pCorrespondenceAddress
		, Revision=pRevision
		;
	else
		update efrsb_manager
		set ArbitrManagerID=pArbitrManagerID
		, SRORegNum=pSRORegNum, SRORegDate=pSRORegDate, RegNum=pRegNum
		, FirstName=pFirstName, MiddleName=pMiddleName, LastName=pLastName
		, OGRNIP=pOGRNIP, INN=pINN
		, CorrespondenceAddress=pCorrespondenceAddress
		, Revision=pRevision
		where id_Manager=pid_Manager;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SafeUploadAnketa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `SafeUploadAnketa`(
	  pManagerINN varchar(12), pid_MUser int

	, pDebtorFirstName varchar(50), pDebtorLastName varchar(50), pDebtorMiddleName varchar(50)
	, pDebtorEmail varchar(50), pDebtorSNILS varchar(13), pDebtorINN varchar(12)

	, pFileData LONGBLOB, pContent_hash varchar(250)

	, out error_text varchar(250)
)
BEGIN
	declare lrevision bigint;
	declare lid_MData, lid_ManagerUser, lid_Manager int;

	select id_ManagerUser, m.id_Manager into lid_ManagerUser, lid_Manager
	from ManagerUser mu
	inner join Manager m on mu.id_Manager=m.id_Manager
	where INN = pManagerINN and id_MUser = pid_MUser;
	if (lid_ManagerUser is null) then
		set error_text = concat("Отсутствует связь АУ и наблюдателя (ИНН АУ: ",pManagerINN,", id_MUser=",pid_MUser,")");
	else
		select if(max(revision) is null, 1, max(revision) + 1) into lrevision from MData;

		insert into MData
		set id_ManagerUser = lid_ManagerUser, id_Debtor = null, fileData = pFileData,
		revision= lrevision, publicDate= now(), MData_Type='a', ContentHash = pContent_hash;

		select last_insert_id() into lid_MData;

		insert into AnketaNP
		set firstName = pDebtorFirstName, lastName = pDebtorLastName, middleName = pDebtorMiddleName,
		Email= pDebtorEmail, SNILS= pDebtorSNILS, INN= pDebtorINN, id_MData = lid_MData;

		set error_text = null;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SafeUploadDeals` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `SafeUploadDeals`(
	  pManagerINN varchar(12), pid_MUser int, pDebtorINN varchar(12)
	, pFileData LONGBLOB, pContent_hash varchar(250)
)
BEGIN
	declare lrevision bigint;
	declare lid_ManagerUser, lid_Debtor, lid_Manager int;

	select mu.id_ManagerUser, d.id_Debtor, m.id_Manager into lid_ManagerUser, lid_Debtor, lid_Manager
	from ManagerUser mu
	inner join Manager m on m.id_Manager=mu.id_Manager
	inner join MProcedure p on p.id_Manager=m.id_Manager
	inner join Debtor d on d.id_Debtor=p.id_Debtor
	where m.INN=pManagerINN and mu.id_MUser=pid_MUser and d.INN=pDebtorINN
	order by p.publicDate desc
	limit 1;

	if (lid_ManagerUser is null) then
		select concat("Отсутствует связь АУ и наблюдателя (ИНН АУ: ",pManagerINN,", id_MUser=",pid_MUser,")") message;
	else
		select if(max(revision) is null, 1, max(revision) + 1) into lrevision from MData;

		delete from MData
		where id_ManagerUser=lid_ManagerUser and id_Debtor=lid_Debtor and MData_Type='d';

		insert into MData
		set id_ManagerUser = lid_ManagerUser, id_Debtor = lid_Debtor, fileData = pFileData,
		revision= lrevision, publicDate= now(), MData_Type='d', ContentHash = pContent_hash;

		update Manager set HasIncoming=1 where id_Manager=lid_Manager;
		update MUser set HasOutcoming=1 where id_MUser=pid_MUser;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SafeUploadProcedureInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `SafeUploadProcedureInfo`(
	  pid_Contract int

	, pmFirstName VARCHAR(50), pmMiddleName VARCHAR(50), pmLastName VARCHAR(50)
	, pmEFRSBNum VARCHAR(50), pmInn VARCHAR(12), pmBTAccount TEXT

	, pInn VARCHAR(12), pDebtorName VARCHAR(250)
	, pOgrn VARCHAR(16), pSnils VARCHAR(13)

	, pCasenumber VARCHAR(60), pFeatures CHAR(1), pProcedure_type CHAR(2)

	, pallow_ctb BIT(1)

	, pContent_hash VARCHAR(250), pFileData LONGBLOB
)
BEGIN
	declare lid_Manager int;
	declare lid_MProcedure int;
	declare lSnils VARCHAR(13);
	declare lid_Contract int;

	select id_Contract into lid_Contract from Manager where efrsbNumber=pmEFRSBNum;

	if (lid_Contract<>pid_Contract) then
		select "Manager" as error_type, 'c' as error_details; 
	else
		select replace(pSnils,' ','') into lSnils;
		select if(lSnils='00000000000',null,lSnils) into lSnils;

		if ((''=pInn || pInn is null) && (''=pOgrn || pOgrn is null) && (''=lSnils || lSnils is null)) then
			select "Debtor" as error_type, 'i' as error_details; 
		else
			call Add_Manager(pid_Contract
				,pmFirstName,pmMiddleName,pmLastName,pmEFRSBNum,pmInn,pmBTAccount
				,lid_Manager);

			if (lid_Manager is not null) then
				call Add_MProcedure(lid_Manager
					,pInn,pDebtorName,pOgrn,lSnils
					,pCasenumber,pFeatures,pProcedure_type
					,lid_MProcedure);

				if (lid_MProcedure is not null) then
					call UploadProcedureInfo(lid_MProcedure, pallow_ctb, pContent_hash, pFileData);
				end if;
			end if;
		end if;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `safe_add_update_wcalendar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `safe_add_update_wcalendar`(
	pYear int,
	pDays text,
    pOKATO char(6)
	)
begin
	declare lMaxRevision bigint;
    declare lIdRegion int;
	select ifnull(max(Revision), 0) into lMaxRevision from wcalendar;
	select id_Region into lIdRegion from region where region.OKATO=pOKATO;

	insert into wcalendar
		(Year,  Days, id_Region, Revision)
	values
		(pYear, compress(pDays), lIdRegion, lMaxRevision+1)
	on duplicate key update
		Days=compress(pDays)
		, Revision=lMaxRevision+1
		, id_Region = lIdRegion
	;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Select_Debtors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `Select_Debtors`(
	  pInn VARCHAR(12)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)
)
begin
    declare lSnils VARCHAR(13);
    declare lInn VARCHAR(12);
    declare lOgrn VARCHAR(16);

    call Fix_Debtor(pInn,pOgrn,pSnils,lInn,lOgrn,lSnils);

        select id_Debtor, VerificationState, Name, INN, OGRN, SNILS
        from Debtor
        where ( (lInn   is not null and INN   is not null and   INN=lInn)
             or (lSnils is not null and SNILS is not null and SNILS=lSnils)
             or (lOgrn  is not null and OGRN  is not null and  OGRN=lOgrn))
        and (lInn   is null or Debtor.INN   is null or   lInn=Debtor.INN)
        and (lSnils is null or Debtor.SNILS is null or lSnils=Debtor.SNILS)
        and (lOgrn  is null or Debtor.OGRN  is null or  lOgrn=Debtor.OGRN)
        ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateVerifiedDebtor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `UpdateVerifiedDebtor`(
	  pid_Debtor  int
	, pINN        varchar(12)
	, pOGRN       varchar(16)
	, pSNILS      varchar(13)
	, pName       varchar(250)
	, pBankruptId BIGINT
)
begin
	declare lid_Debtor int;

	select id_Debtor into lid_Debtor from Debtor where BankruptId=pBankruptId;

	if (lid_Debtor is null || lid_Debtor=pid_Debtor) then
		select pid_Debtor into lid_Debtor;
	else
		update MProcedure set id_Debtor=lid_Debtor where id_Debtor=pid_Debtor;
		update Request set id_Debtor=lid_Debtor where id_Debtor=pid_Debtor;
		delete from Debtor where id_Debtor=pid_Debtor;
 	end if;

	update Debtor set
		TimeVerified=now()
		, VerificationState='v'
		, BankruptId=pBankruptId
		, INN=pINN
		, OGRN=pOGRN
		, SNILS=pSNILS
		, Name=pName
	where
		id_Debtor=lid_Debtor;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UploadProcedureInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`datamartdevel`@`localhost` PROCEDURE `UploadProcedureInfo`(
	  pid_MProcedure int
	, pallow_ctb BIT(1)
	, pContent_hash VARCHAR(250)
	, pFileData LONGBLOB
)
begin
	declare lrevision bigint;
	declare lpublicDate datetime;
	declare lContent_hash varchar(250);
	declare lid_PData int;
	declare lallow_ctb bit(1);

	select null into lallow_ctb;
	select if(max(revision) is null, 1, max(revision) + 1), now() into lrevision, lpublicDate from PData;

	if (pFileData is not null) then
		insert into PData
		set id_MProcedure = pid_MProcedure, fileData = pFileData, ctb_allowed= pallow_ctb,
			revision= lrevision, publicDate= lpublicDate, Content_hash= pContent_hash;
	else
		select Content_hash into lContent_hash from MProcedure where id_MProcedure=pid_MProcedure;
		if (lContent_hash<>pContent_hash) then
			select * from bad_content_hash_in_throw_exception;
		end if;

		if (b'1' = pallow_ctb) then
			select id_PData, ctb_allowed into lid_PData, lallow_ctb from PData where id_MProcedure=pid_MProcedure order by revision desc limit 1;
			if (b'0' = lallow_ctb) then
				update PData
				set ctb_allowed= pallow_ctb,
					revision= lrevision, publicDate= lpublicDate, Content_hash= pContent_hash
				where id_PData=lid_PData;
			end if;
		end if;
	end if;

	if (b'1' = pallow_ctb && (pFileData is not null || b'0' = lallow_ctb)) then
		update MProcedure set publicDate= lpublicDate, revision= lrevision, Content_hash= pContent_hash
			, ctb_publicDate= lpublicDate, ctb_revision= lrevision
		where id_MProcedure=pid_MProcedure;
	elseif (pFileData is not null) then
		update MProcedure set publicDate= lpublicDate, revision= lrevision, Content_hash= pContent_hash
		where id_MProcedure=pid_MProcedure;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-12  4:05:37
