
alter table Asset add `Address` text;
alter table Asset add `Description` text;
alter table Asset add `State` char(1) NOT NULL DEFAULT 's';	
alter table Asset add `ID_Object` varchar(40) DEFAULT NULL;

alter table Asset change `Title` `Title` text NOT NULL;

insert into tbl_migration set MigrationNumber='m127', MigrationName='AddAssetExreaFields';