alter table MUser add `TimeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
insert into tbl_migration set MigrationNumber='m121', MigrationName='AddMUserTimeCreated';