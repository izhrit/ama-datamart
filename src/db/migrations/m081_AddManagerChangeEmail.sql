alter table Manager add `ManagerEmail_Change` varchar(70) DEFAULT NULL;
alter table Manager add `ManagerEmail_ChangeTime` datetime DEFAULT NULL;
alter table Manager add `ManagerPassword_Change` varchar(30) DEFAULT NULL;
alter table Manager add `ManagerPassword_ChangeTime` datetime DEFAULT NULL;
alter table Manager add UNIQUE KEY `byManagerEmail_Change` (`ManagerEmail_Change`);

insert into tbl_migration set MigrationNumber='m081', MigrationName='AddManagerChangeEmail';
