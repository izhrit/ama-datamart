
CREATE TABLE `wcalendar` (
  `id_Wcalendar` int(11) NOT NULL AUTO_INCREMENT,
  `id_Region` int(11) DEFAULT NULL,
  `Revision` int(11) NOT NULL,
  `Year` int(11) NOT NULL,
  `Days` longblob,
  PRIMARY KEY (`id_Wcalendar`),
  KEY `refWcalendar_Region` (`id_Region`),
  UNIQUE KEY `by_id_Region_Year` (`id_Region`,`Year`),
  UNIQUE KEY `byRevision` (`Revision`),
  CONSTRAINT `refWcalendar_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration set MigrationNumber='m083', MigrationName='AddWcalendar';
