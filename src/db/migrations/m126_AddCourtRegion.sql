
alter table Court add `id_Region` int(11) DEFAULT NULL;
alter table Court add KEY `refCourt_Region` (`id_Region`);
alter table Court add CONSTRAINT `refCourt_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`);

insert into tbl_migration set MigrationNumber='m126', MigrationName='AddCourtRegion';