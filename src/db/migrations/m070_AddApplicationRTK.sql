CREATE TABLE `applicationrtk` (
  `id_ApplicationRTK` int(11) NOT NULL AUTO_INCREMENT,
  `id_MData` int(11) DEFAULT NULL,
  `creditor_inn` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id_ApplicationRTK`),
  KEY `Ref3999` (`id_MData`),
  CONSTRAINT `RefMData99` FOREIGN KEY (`id_MData`) REFERENCES `MData` (`id_MData`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table Application drop column debtor_inn;
alter table Application drop column creditor_inn;
alter table Application drop column debtor_name;
alter table Application drop column creditor_name;
alter table Application drop column last_date_request;


insert into tbl_migration set MigrationNumber='m070', MigrationName='AddApplicationRTK';