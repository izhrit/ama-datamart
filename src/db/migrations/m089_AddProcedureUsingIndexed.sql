
CREATE TABLE `ProcedureUsing_indexed` (
  `ContractNumber` varchar(10) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `IP` varchar(40) DEFAULT NULL,
  `OGRN` varbinary(16) DEFAULT NULL,
  `SNILS` varchar(13) DEFAULT NULL,
  `Section` char(4) DEFAULT NULL,
  `UsingTime` datetime DEFAULT NULL,
  KEY `byContractNumber` (`ContractNumber`),
  KEY `byIP` (`IP`),
  KEY `bySection` (`Section`),
  KEY `byUsingTime` (`UsingTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration set MigrationNumber='m089', MigrationName='AddProcedureUsingIndexed';