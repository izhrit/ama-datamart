ALTER TABLE ProcedureStart
ADD COLUMN efrsbPrescriptionAddInfo varchar(250) DEFAULT NULL,
ADD COLUMN CourtDecisionAddInfo varchar(250) DEFAULT NULL,
ADD COLUMN PrescriptionURL varchar(250) DEFAULT NULL,
ADD COLUMN PrescriptionAddInfo varchar(250) DEFAULT NULL;

ALTER TABLE MRequest
ADD COLUMN CourtDecisionAddInfo varchar(250) DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m100', MigrationName='AddPrescriptionFieldsAndAddInfoField';