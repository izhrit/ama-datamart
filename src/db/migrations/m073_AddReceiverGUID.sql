alter table Push_receiver add `ReceiverGUID` varchar(32) DEFAULT NULL;
update Push_receiver set ReceiverGUID = LEFT(MD5(RAND()), 32);

insert into tbl_migration set MigrationNumber='m073', MigrationName='AddReceiverGUID';
