
alter table MRequest add `DateOfRequest` datetime NOT NULL;
alter table MRequest add `DateOfResponce` datetime DEFAULT NULL;
alter table MRequest add `DebtorCategory` char(1) NOT NULL DEFAULT 'n';
alter table MRequest add `NextSessionDate` datetime DEFAULT NULL;
alter table MRequest add `id_Manager` int(11) DEFAULT NULL;

alter table MRequest add KEY `Ref_MRequest_Manager` (`id_Manager`);
alter table MRequest add CONSTRAINT `Ref_MRequest_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `Manager` (`id_Manager`);

insert into tbl_migration set MigrationNumber='m072', MigrationName='AddMRequestExtraFields';