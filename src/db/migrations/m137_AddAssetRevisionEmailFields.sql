
CREATE TABLE `Asset_proc_for_MUser` (
  `Details` longblob,
  `TimeCreated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_MProcedure` int(11) NOT NULL,
  `id_MUser` int(11) NOT NULL,
  PRIMARY KEY (`id_MProcedure`,`id_MUser`),
  KEY `Access_proc_MUser` (`id_MUser`),
  KEY `Asset_proc_MProcedure` (`id_MProcedure`),
  CONSTRAINT `Access_proc_MUser` FOREIGN KEY (`id_MUser`) REFERENCES `MUser` (`id_MUser`),
  CONSTRAINT `Asset_proc_MProcedure` FOREIGN KEY (`id_MProcedure`) REFERENCES `MProcedure` (`id_MProcedure`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table Asset add `Revision` bigint(20) DEFAULT NULL;
update Asset set Revision=0;
alter table Asset CHANGE `Revision` `Revision` bigint(20) NOT NULL;
alter table Asset add KEY `byRevision` (`Revision`);
alter table Asset change `TimeChanged` `TimeChanged` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;

alter table AssetAttachment add `Revision` bigint(20) DEFAULT NULL;
update AssetAttachment set Revision=0;
alter table AssetAttachment CHANGE `Revision` `Revision` bigint(20) NOT NULL;
alter table AssetAttachment add KEY `byRevision` (`Revision`);

insert into tbl_migration set MigrationNumber='m137', MigrationName='AddAssetRevisionEmailFields';