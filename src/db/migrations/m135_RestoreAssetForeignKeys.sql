
alter table Asset_EfrsbAssetClass add CONSTRAINT `refAsset_EfrsbAssetClass_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `Asset` (`id_Asset`) ON DELETE CASCADE;
alter table Asset_MUser add CONSTRAINT `refAsset_MUser_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `Asset` (`id_Asset`) ON DELETE CASCADE;
alter table Asset_OKPD2 add CONSTRAINT `refAsset_OKPD2_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `Asset` (`id_Asset`) ON DELETE CASCADE;
alter table AssetAttachment add CONSTRAINT `refAssetAttachment_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `Asset` (`id_Asset`) ON DELETE CASCADE;

insert into tbl_migration set MigrationNumber='m135', MigrationName='RestoreAssetForeignKeys';