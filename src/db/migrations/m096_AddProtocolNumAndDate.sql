ALTER TABLE Manager
ADD COLUMN ProtocolNum varchar(50) DEFAULT NULL,
ADD COLUMN ProtocolDate DATETIME;

insert into tbl_migration set MigrationNumber='m096', MigrationName='AddProtocolNumAndDate';