
alter table Asset_OKPD2 drop FOREIGN KEY refAsset_OKPD2_Asset;
alter table AssetAttachment drop FOREIGN KEY refAssetAttachment_Asset;
alter table Asset_EfrsbAssetClass drop FOREIGN KEY refAsset_EfrsbAssetClass_Asset;
alter table Asset_MUser drop FOREIGN KEY refAsset_MUser_Asset;

alter table Asset drop FOREIGN KEY refAsset_AssetSubGroup;
alter table Asset drop FOREIGN KEY refAsset_AssetGroup;
alter table Asset drop FOREIGN KEY refAsset_BalanceItem;
alter table Asset drop FOREIGN KEY refAsset_Region;
alter table Asset drop FOREIGN KEY refAsset_MProcedure;

alter table Asset ENGINE = MyISAM;
alter table Asset add fulltext index byDescription (Description);

ALTER TABLE Asset ADD CONSTRAINT refAsset_AssetSubGroup 
    FOREIGN KEY (id_AssetSubGroup)
    REFERENCES AssetSubGroup(id_AssetSubGroup)
;
ALTER TABLE Asset ADD CONSTRAINT refAsset_AssetGroup 
    FOREIGN KEY (id_AssetGroup)
    REFERENCES AssetGroup(id_AssetGroup)
;
ALTER TABLE Asset ADD CONSTRAINT refAsset_BalanceItem 
    FOREIGN KEY (id_BalanceItem)
    REFERENCES BalanceItem(id_BalanceItem)
;
ALTER TABLE Asset ADD CONSTRAINT refAsset_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;
ALTER TABLE Asset ADD CONSTRAINT refAsset_MProcedure 
    FOREIGN KEY (id_MProcedure)
    REFERENCES MProcedure(id_MProcedure)
;

insert into tbl_migration set MigrationNumber='m133', MigrationName='ChangeAssetEngineForFulltext';