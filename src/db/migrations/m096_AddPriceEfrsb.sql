CREATE TABLE `price_efrsb` (
  `id_pefrsb` int(11) NOT NULL,
  `date_of_set_price` date NOT NULL,
  `price_for_physical_entity` float NULL,
  `price_for_physical_entity_with_commission` float NULL,
  `price_for_legal_entity` float NULL,
  `price_for_legal_entity_with_commission` float NULL,
  PRIMARY KEY (`id_pefrsb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration set MigrationNumber='m096', MigrationName='AddPriceEfrsb';