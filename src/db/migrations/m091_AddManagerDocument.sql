
CREATE TABLE `ManagerDocument` (
  `Body` longblob NOT NULL,
  `DocumentType` char(1) DEFAULT NULL,
  `FileName` varchar(100) NOT NULL,
  `id_ManagerDocument` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  PRIMARY KEY (`id_ManagerDocument`),
  KEY `RefManager_Document` (`id_Manager`),
  CONSTRAINT `RefManager_Document` FOREIGN KEY (`id_Manager`) REFERENCES `Manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table Court add `NumberPrefix` varchar(5) DEFAULT NULL;
alter table Court add KEY `byNumberPrefix` (`NumberPrefix`);

alter table MRequest add `DateOfOfferTill` datetime DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m091', MigrationName='AddManagerDocument';