alter table mock_efrsb_event add `isActive` bit(1) NOT NULL DEFAULT b'1';
alter table event add `isActive` bit(1) NOT NULL DEFAULT b'1';
insert into log_type(id_Log_type,Name) values ( 24, 'push/unsubscribe' );
insert into tbl_migration set MigrationNumber='m074', MigrationName='AddIsActiveEvent';
