
alter table ProcedureStart add `ShowToSRO` bit(1) NOT NULL DEFAULT b'0';
alter table ProcedureStart add `id_SRO` int(11) DEFAULT NULL;
alter table ProcedureStart add KEY `Ref_Start_SRO` (`id_SRO`);
alter table ProcedureStart add CONSTRAINT `Ref_Start_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `efrsb_sro` (`id_SRO`);

insert into tbl_migration set MigrationNumber='m071', MigrationName='StartsForSRO';