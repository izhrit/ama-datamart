
insert into log_type(id_Log_type,Name) values
  ( 42, 'exposure/store/asset/start' )
, ( 43, 'exposure/store/asset/finish' )
, ( 44, 'exposure/store/document/start' )
, ( 45, 'exposure/store/document/finish' )
;

insert into tbl_migration set MigrationNumber='m136', MigrationName='AddExposureStoreLogEventTypes';