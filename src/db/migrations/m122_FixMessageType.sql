
alter table Message change column MessageInfo_MessageType MessageInfo_MessageType char(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL;
alter table mock_efrsb_message change column MessageInfo_MessageType MessageInfo_MessageType char(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL;
alter table event change column MessageType MessageType char(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m122', MigrationName='FixMessageType';