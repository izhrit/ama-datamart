CREATE TABLE `mock_efrsb_email` (
  `address` varchar(50) NOT NULL,
  `id_Email` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email`),
  UNIQUE KEY `byAddress` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mock_efrsb_email_manager` (
  `id_Email` int(11) NOT NULL,
  `id_Manager` int(11) NOT NULL,
  `id_Message_Last` int(11) DEFAULT NULL,
  `id_Messages` longblob NOT NULL,
  PRIMARY KEY (`id_Email`,`id_Manager`),
  KEY `RefEmail_ManagerEmail` (`id_Email`),
  KEY `RefEmail_ManagerManager` (`id_Manager`),
  KEY `RefEmail_Manager_MessageLast` (`id_Message_Last`),
  CONSTRAINT `RefEmail_ManagerEmail` FOREIGN KEY (`id_Email`) REFERENCES `mock_efrsb_email` (`id_Email`),
  CONSTRAINT `RefEmail_ManagerManager` FOREIGN KEY (`id_Manager`) REFERENCES `mock_efrsb_manager` (`id_Manager`),
  CONSTRAINT `RefEmail_Manager_MessageLast` FOREIGN KEY (`id_Message_Last`) REFERENCES `mock_efrsb_message` (`id_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration set MigrationNumber='m082', MigrationName='AddMockEfrsbEmail';
