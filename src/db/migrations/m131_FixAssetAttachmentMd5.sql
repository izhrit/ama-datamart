
alter table AssetAttachment change `md5hash` `md5hash` varchar(40) NOT NULL;

insert into tbl_migration set MigrationNumber='m131', MigrationName='FixAssetAttachmentMd5';