ALTER TABLE ProcedureStart
DROP COLUMN DateOfAgree;

insert into tbl_migration set MigrationNumber='m080', MigrationName='DeleteProcedureStartDateOfAgree';