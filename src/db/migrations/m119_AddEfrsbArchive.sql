alter table efrsb_debtor add `Archive` bit(1) NOT NULL DEFAULT b'0';
alter table mock_efrsb_debtor add `Archive` bit(1) NOT NULL DEFAULT b'0';
insert into tbl_migration set MigrationNumber='m119', MigrationName='AddEfrsbArchive';