
ALTER TABLE ProcedureStart
ADD COLUMN AddedBySRO bit(1) NOT NULL DEFAULT b'0';
insert into tbl_migration set MigrationNumber='m086', MigrationName='AddProcedureStartAddedBySRO';