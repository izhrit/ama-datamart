
alter table efrsb_debtor add `LastMessageDate` datetime DEFAULT NULL;
alter table JobPart add `ExtraStateFields` longblob;

insert into tbl_migration set MigrationNumber='m120', MigrationName='AddDebtorLastMessageDateJobPartState';