alter table `Meeting_document` drop column `Meeting_document_type`;
alter table `Meeting_document` add `DocumentName` VARCHAR(250) NOT NULL;

insert into tbl_migration set MigrationNumber='m092', MigrationName='FixMeetingDocument';