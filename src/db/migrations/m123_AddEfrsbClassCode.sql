
alter table EfrsbAssetClass add `Code` varchar(10) NOT NULL;

alter table Manager add `ctb_account_limit` bit(1) NOT NULL DEFAULT b'0';
update Manager set ctb_account_limit=(select max(ctb_account_limit) from MProcedure where MProcedure.id_Manager=Manager.id_Manager);
alter table MProcedure drop `ctb_account_limit`;

insert into tbl_migration set MigrationNumber='m123', MigrationName='AddEfrsbClassCode';