﻿
CREATE TABLE `FaUsing` (
  `ContractNumber` varchar(40) DEFAULT NULL,
  `IP` varchar(40) NOT NULL,
  `LicenseToken` text,
  `UsingTime` datetime NOT NULL,
  `Section` char(4) DEFAULT NULL  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration set MigrationNumber='m085', MigrationName='AddFaUsing';