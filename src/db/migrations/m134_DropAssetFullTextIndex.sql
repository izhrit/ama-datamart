
alter table Asset drop index `byDescription`;
alter table Asset ENGINE = InnoDB;

alter table Asset add CONSTRAINT `refAsset_AssetGroup` FOREIGN KEY (`id_AssetGroup`) REFERENCES `AssetGroup` (`id_AssetGroup`);
alter table Asset add CONSTRAINT `refAsset_AssetSubGroup` FOREIGN KEY (`id_AssetSubGroup`) REFERENCES `AssetSubGroup` (`id_AssetSubGroup`);
alter table Asset add CONSTRAINT `refAsset_BalanceItem` FOREIGN KEY (`id_BalanceItem`) REFERENCES `BalanceItem` (`id_BalanceItem`);
alter table Asset add CONSTRAINT `refAsset_MProcedure` FOREIGN KEY (`id_MProcedure`) REFERENCES `MProcedure` (`id_MProcedure`);
alter table Asset add CONSTRAINT `refAsset_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`);

insert into tbl_migration set MigrationNumber='m134', MigrationName='DropAssetFullTextIndex';