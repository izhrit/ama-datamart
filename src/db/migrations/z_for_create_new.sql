insert into tbl_migration set MigrationNumber='m001', MigrationName='Content_hash';
insert into tbl_migration set MigrationNumber='m002', MigrationName='MUserChanging';
insert into tbl_migration set MigrationNumber='m003', MigrationName='SentEmail';
insert into tbl_migration set MigrationNumber='m004', MigrationName='UserIndex_Sender';
insert into tbl_migration set MigrationNumber='m005', MigrationName='ManagerPricedureExtraParams';
insert into tbl_migration set MigrationNumber='m006', MigrationName='MeetingState';
insert into tbl_migration set MigrationNumber='m007', MigrationName='ReduceEmailToSend';
insert into tbl_migration set MigrationNumber='m008', MigrationName='AddEmailErrorText';
insert into tbl_migration set MigrationNumber='m009', MigrationName='PDataProcessed';
insert into tbl_migration set MigrationNumber='m010', MigrationName='AddPDataByRevision';
insert into tbl_migration set MigrationNumber='m011', MigrationName='AddAward';
insert into tbl_migration set MigrationNumber='m012', MigrationName='AddAwardSignature';
insert into tbl_migration set MigrationNumber='m013', MigrationName='ModifyAward';
insert into tbl_migration set MigrationNumber='m014', MigrationName='AwardEfrsbNumberToINN';
insert into tbl_migration set MigrationNumber='m015', MigrationName='AddRequests';
insert into tbl_migration set MigrationNumber='m016', MigrationName='FixRequests';
insert into tbl_migration set MigrationNumber='m017', MigrationName='AddDebtor';
insert into tbl_migration set MigrationNumber='m018', MigrationName='AddEfrsb';
insert into tbl_migration set MigrationNumber='m019', MigrationName='AddRequestState';
insert into tbl_migration set MigrationNumber='m020', MigrationName='AddBankroTechAcc';
insert into tbl_migration set MigrationNumber='m021', MigrationName='AddBankroTechFields';
insert into tbl_migration set MigrationNumber='m022', MigrationName='FixVerified';
insert into tbl_migration set MigrationNumber='m023', MigrationName='FixMockEfrsb';
insert into tbl_migration set MigrationNumber='m024', MigrationName='FixDebtorVerify';
insert into tbl_migration set MigrationNumber='m025', MigrationName='FixMockEfrsb2';
insert into tbl_migration set MigrationNumber='m026', MigrationName='FixProcedureVerify';
insert into tbl_migration set MigrationNumber='m027', MigrationName='FixMProcedureIndex';
insert into tbl_migration set MigrationNumber='m028', MigrationName='AddAccessLog';
insert into tbl_migration set MigrationNumber='m029', MigrationName='AddBankroTechAccVerified';
insert into tbl_migration set MigrationNumber='m030', MigrationName='FixCtbId';
insert into tbl_migration set MigrationNumber='m031', MigrationName='AddCtbAccountLimit';
insert into tbl_migration set MigrationNumber='m032', MigrationName='AddSubLevel';
insert into tbl_migration set MigrationNumber='m033', MigrationName='AddFaAccVerified';
insert into tbl_migration set MigrationNumber='m034', MigrationName='AddManagerDateDelete';
insert into tbl_migration set MigrationNumber='m035', MigrationName='FixMockEfrsb3';
insert into tbl_migration set MigrationNumber='m036', MigrationName='AddPushReceiver';
insert into tbl_migration set MigrationNumber='m037', MigrationName='AddEvent';
insert into tbl_migration set MigrationNumber='m038', MigrationName='AddMockEfrsbSRO';
insert into tbl_migration set MigrationNumber='m039', MigrationName='FixEvent';
insert into tbl_migration set MigrationNumber='m040', MigrationName='FixSubLevelSum';
insert into tbl_migration set MigrationNumber='m041', MigrationName='AddMData';
insert into tbl_migration set MigrationNumber='m042', MigrationName='AddLogForSublevelInfo';
insert into tbl_migration set MigrationNumber='m043', MigrationName='FixPushReceiver';
insert into tbl_migration set MigrationNumber='m044', MigrationName='AddMessage';
insert into tbl_migration set MigrationNumber='m045', MigrationName='FixMessageEventIndexes';
insert into tbl_migration set MigrationNumber='m046', MigrationName='AddAnketaNP';
insert into tbl_migration set MigrationNumber='m047', MigrationName='AddAnketaNP_byINN';
insert into tbl_migration set MigrationNumber='m048', MigrationName='AddDebtorLastMessageDate';
insert into tbl_migration set MigrationNumber='m049', MigrationName='FixAnketaNPCascadeDelete';
insert into tbl_migration set MigrationNumber='m050', MigrationName='AddHasIncomingOutcoming';
insert into tbl_migration set MigrationNumber='m051', MigrationName='FixPushReceiver2';
insert into tbl_migration set MigrationNumber='m052', MigrationName='ChangePushRelations';
insert into tbl_migration set MigrationNumber='m053', MigrationName='AddMUserCreatedTime';
insert into tbl_migration set MigrationNumber='m054', MigrationName='AddApplication';
insert into tbl_migration set MigrationNumber='m055', MigrationName='AddMUserConfirmationTokens';
insert into tbl_migration set MigrationNumber='m056', MigrationName='FixManagerIdContract';
insert into tbl_migration set MigrationNumber='m057', MigrationName='AddPushed_message';
insert into tbl_migration set MigrationNumber='m058', MigrationName='AddCalendarCase';
insert into tbl_migration set MigrationNumber='m059', MigrationName='AddEfrsbJobs';
insert into tbl_migration set MigrationNumber='m060', MigrationName='AddProcedureStart';
insert into tbl_migration set MigrationNumber='m061', MigrationName='FixJobSiteDescription';
insert into tbl_migration set MigrationNumber='m062', MigrationName='AddMRequest';
insert into tbl_migration set MigrationNumber='m063', MigrationName='AddProcedureStartNextSessionDate';
insert into tbl_migration set MigrationNumber='m064', MigrationName='AddProcedureUsing';
insert into tbl_migration set MigrationNumber='m065', MigrationName='AddJobIndexes';
insert into tbl_migration set MigrationNumber='m066', MigrationName='AddJobCascadeDelete';
insert into tbl_migration set MigrationNumber='m067', MigrationName='AddStartCategory';
insert into tbl_migration set MigrationNumber='m068', MigrationName='AddCourtNames';
insert into tbl_migration set MigrationNumber='m069', MigrationName='AddProcedureUsingSection';
insert into tbl_migration set MigrationNumber='m070', MigrationName='AddApplicationRTK';
insert into tbl_migration set MigrationNumber='m071', MigrationName='StartsForSRO';
insert into tbl_migration set MigrationNumber='m072', MigrationName='AddMRequestExtraFields';
insert into tbl_migration set MigrationNumber='m073', MigrationName='AddReceiverGUID';
insert into tbl_migration set MigrationNumber='m074', MigrationName='AddIsActiveEvent';
insert into tbl_migration set MigrationNumber='m075', MigrationName='AddUniqueEfrsbId';
insert into tbl_migration set MigrationNumber='m076', MigrationName='AddPushErrorText';
insert into tbl_migration set MigrationNumber='m077', MigrationName='FixMockEfrsb4';
insert into tbl_migration set MigrationNumber='m078', MigrationName='AddCourtAddress';
insert into tbl_migration set MigrationNumber='m079', MigrationName='AddMRequestApplicantFields';
insert into tbl_migration set MigrationNumber='m080', MigrationName='DeleteProcedureStartDateOfAgree';
insert into tbl_migration set MigrationNumber='m081', MigrationName='AddManagerChangeEmail';
insert into tbl_migration set MigrationNumber='m082', MigrationName='AddMockEfrsbEmail';
insert into tbl_migration set MigrationNumber='m083', MigrationName='AddWcalendar';
insert into tbl_migration set MigrationNumber='m084', MigrationName='AddProcedureStartPrescriptionFields';
insert into tbl_migration set MigrationNumber='m085', MigrationName='AddFaUsing';
insert into tbl_migration set MigrationNumber='m086', MigrationName='AddProcedureStartAddedBySRO';
insert into tbl_migration set MigrationNumber='m087', MigrationName='AddSroRobotLog';
insert into tbl_migration set MigrationNumber='m088', MigrationName='FixUpdateEfrsbManager';
insert into tbl_migration set MigrationNumber='m089', MigrationName='AddProcedureUsingIndexed';
insert into tbl_migration set MigrationNumber='m090', MigrationName='AddJobPartStartedIndex';
insert into tbl_migration set MigrationNumber='m091', MigrationName='AddManagerDocument';
insert into tbl_migration set MigrationNumber='m092', MigrationName='FixMeetingDocument';
insert into tbl_migration set MigrationNumber='m093', MigrationName='AddDateOfCreationMRequest';
insert into tbl_migration set MigrationNumber='m094', MigrationName='FixManagerDocument';
insert into tbl_migration set MigrationNumber='m095', MigrationName='AddCourtDecisionURL';
insert into tbl_migration set MigrationNumber='m096', MigrationName='AddProtocolNumAndDate';
insert into tbl_migration set MigrationNumber='m097', MigrationName='AddManagerContractChange';
insert into tbl_migration set MigrationNumber='m098', MigrationName='AddRosreestr';
insert into tbl_migration set MigrationNumber='m099', MigrationName='FixEfrsbSro';
insert into tbl_migration set MigrationNumber='m100', MigrationName='AddPrescriptionFieldsAndAddInfoField';
insert into tbl_migration set MigrationNumber='m101', MigrationName='ProcedureStartMRequestDebtor2Fields';
insert into tbl_migration set MigrationNumber='m102', MigrationName='ProcedureStartTimeOfVerificationEFRSB';
insert into tbl_migration set MigrationNumber='m103', MigrationName='AddElektroKKLogs';
insert into tbl_migration set MigrationNumber='m104', MigrationName='AddElektroKKLogs2';
insert into tbl_migration set MigrationNumber='m105', MigrationName='AddApiErrorLog';
insert into tbl_migration set MigrationNumber='m106', MigrationName='AddFieldsGoogleCalendar';
insert into tbl_migration set MigrationNumber='m107', MigrationName='AddPreDebtorPDDocumentPDDocFile';
insert into tbl_migration set MigrationNumber='m108', MigrationName='AddMUserEscortFields';
insert into tbl_migration set MigrationNumber='m109', MigrationName='FixNaturalKey';
insert into tbl_migration set MigrationNumber='m110', MigrationName='AddLogTypesAutologinErros';
insert into tbl_migration set MigrationNumber='m111', MigrationName='AddLogTypesAutologinError2';
insert into tbl_migration set MigrationNumber='m112', MigrationName='AddJobEnabled';
insert into tbl_migration set MigrationNumber='m113', MigrationName='AddJobSiteSyncTime';
insert into tbl_migration set MigrationNumber='m114', MigrationName='AddPushRequestResponse';
insert into tbl_migration set MigrationNumber='m115', MigrationName='AddAssets';
insert into tbl_migration set MigrationNumber='m116', MigrationName='fixJobPartLogNumber';
insert into tbl_migration set MigrationNumber='m117', MigrationName='AddArchivedFixCreated';
insert into tbl_migration set MigrationNumber='m118', MigrationName='AddBidding';
insert into tbl_migration set MigrationNumber='m119', MigrationName='AddEfrsbArchive';
insert into tbl_migration set MigrationNumber='m120', MigrationName='AddDebtorLastMessageDateJobPartState';
insert into tbl_migration set MigrationNumber='m121', MigrationName='AddMUserTimeCreated';
insert into tbl_migration set MigrationNumber='m122', MigrationName='FixMessageType';
insert into tbl_migration set MigrationNumber='m123', MigrationName='AddEfrsbClassCode';
insert into tbl_migration set MigrationNumber='m124', MigrationName='AddAssetRegion';
insert into tbl_migration set MigrationNumber='m125', MigrationName='EnlargeEfrsbClassName';
insert into tbl_migration set MigrationNumber='m126', MigrationName='AddCourtRegion';
insert into tbl_migration set MigrationNumber='m127', MigrationName='AddAssetExreaFields';
insert into tbl_migration set MigrationNumber='m128', MigrationName='AddAsset_ID_Object_index';
insert into tbl_migration set MigrationNumber='m129', MigrationName='AddEfrsbAssetClassCascade';
insert into tbl_migration set MigrationNumber='m130', MigrationName='AddAssetAttachmentMD5';
insert into tbl_migration set MigrationNumber='m131', MigrationName='FixAssetAttachmentMd5';
insert into tbl_migration set MigrationNumber='m132', MigrationName='AddAssetIndexes';
insert into tbl_migration set MigrationNumber='m133', MigrationName='ChangeAssetEngineForFulltext';
insert into tbl_migration set MigrationNumber='m134', MigrationName='DropAssetFullTextIndex';
insert into tbl_migration set MigrationNumber='m135', MigrationName='RestoreAssetForeignKeys';
insert into tbl_migration set MigrationNumber='m136', MigrationName='AddExposureStoreLogEventTypes';
insert into tbl_migration set MigrationNumber='m137', MigrationName='AddAssetRevisionEmailFields';
