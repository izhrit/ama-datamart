
alter table Asset add `id_Region` int(11) NOT NULL;
alter table Asset add KEY `refAsset_Region` (`id_Region`);
alter table Asset add CONSTRAINT `refAsset_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`);

insert into tbl_migration set MigrationNumber='m124', MigrationName='AddAssetRegion';