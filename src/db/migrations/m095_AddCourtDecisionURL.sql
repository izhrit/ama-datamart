ALTER TABLE MRequest
ADD COLUMN CourtDecisionURL varchar(250) DEFAULT NULL;

ALTER TABLE ProcedureStart
ADD COLUMN CourtDecisionURL varchar(250) DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m095', MigrationName='AddCourtDecisionURL';