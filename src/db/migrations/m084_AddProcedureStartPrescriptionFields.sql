﻿alter table ProcedureStart add `DateOfAcceptance` datetime DEFAULT NULL;

alter table ProcedureStart add `DateOfPrescription` datetime DEFAULT NULL;

alter table ProcedureStart add `efrsbPrescriptionID` varchar(32) DEFAULT NULL;
alter table ProcedureStart add `efrsbPrescriptionNumber` varchar(30) DEFAULT NULL;
alter table ProcedureStart add `efrsbPrescriptionPublishDate` datetime DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m084', MigrationName='AddProcedureStartPrescriptionFields';