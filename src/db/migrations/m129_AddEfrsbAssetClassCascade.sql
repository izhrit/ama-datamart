
alter table Asset_EfrsbAssetClass drop FOREIGN KEY `refAsset_EfrsbAssetClass_Asset`;
alter table Asset_EfrsbAssetClass add CONSTRAINT `refAsset_EfrsbAssetClass_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `Asset` (`id_Asset`) ON DELETE CASCADE;

insert into tbl_migration set MigrationNumber='m129', MigrationName='AddEfrsbAssetClassCascade';