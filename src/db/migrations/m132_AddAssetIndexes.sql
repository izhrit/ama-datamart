
alter table Asset change `State` `State` char(1) NOT NULL DEFAULT 'a';
update Asset set State='a' where State='s';

insert into tbl_migration set MigrationNumber='m132', MigrationName='AddAssetIndexes';