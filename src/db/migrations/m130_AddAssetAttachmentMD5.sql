
alter table Asset add `md5hash` varchar(40) DEFAULT NULL;

CREATE TABLE `AssetAttachment` (
  `Body` longblob NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `FileSize` int(11) NOT NULL,
  `id_AssetAttachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_Asset` int(11) NOT NULL,
  `md5hash` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_AssetAttachment`),
  KEY `refAssetAttachment_Asset` (`id_Asset`),
  CONSTRAINT `refAssetAttachment_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `Asset` (`id_Asset`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration set MigrationNumber='m130', MigrationName='AddAssetAttachmentMD5';