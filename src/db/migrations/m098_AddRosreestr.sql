CREATE TABLE `Rosreestr` (
  `Address` text NOT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  `Name` varchar(70) NOT NULL,
  `id_Region` int(11) NOT NULL,
  `id_Rosreestr` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Rosreestr`),
  KEY `refRosreestr_Region` (`id_Region`),
  CONSTRAINT `refRosreestr_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration set MigrationNumber='m098', MigrationName='AddRosreestr';