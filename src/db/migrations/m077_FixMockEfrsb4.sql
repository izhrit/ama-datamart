alter table mock_efrsb_debtor add `Revision` bigint(20) NOT NULL;
alter table mock_efrsb_sro change UrAddress UrAdress varchar(250) DEFAULT NULL;
alter table mock_efrsb_debtor_manager add `Revision` bigint(20) NOT NULL;
alter table mock_efrsb_manager add `Revision` bigint(20) NOT NULL;
alter table mock_efrsb_manager add `CorrespondenceAddress` varchar(300) DEFAULT NULL;
alter table mock_efrsb_manager add `SNILS` varchar(11) DEFAULT NULL;
alter table mock_efrsb_manager add `SRORegDate` datetime DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m077', MigrationName='FixMockEfrsb4';
