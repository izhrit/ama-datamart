alter table Court add `Address` text;

CREATE TABLE `PeproUsing` (
  `ContractNumber` varchar(40) DEFAULT NULL,
  `IP` varchar(40) NOT NULL,
  `LicenseToken` text,
  `UsingTime` datetime NOT NULL,
  `Section` char(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration set MigrationNumber='m078', MigrationName='AddCourtAddress';
