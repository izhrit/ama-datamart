CREATE TABLE `SRODocument` (
  `Body` longblob NOT NULL,
  `DocumentType` char(1) NOT NULL,
  `FileName` varchar(100) NOT NULL,
  `PubLock` longblob,
  `id_SRODocument` int(11) NOT NULL AUTO_INCREMENT,
  `id_SRO` int(11) NOT NULL,
  PRIMARY KEY (`id_SRODocument`),
  KEY `refSRO_Document` (`id_SRO`),
  CONSTRAINT `refSRO_Document` FOREIGN KEY (`id_SRO`) REFERENCES `efrsb_sro` (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table `efrsb_sro` add `ExtraFields` longblob;

insert into tbl_migration set MigrationNumber='m099', MigrationName='FixEfrsbSro';