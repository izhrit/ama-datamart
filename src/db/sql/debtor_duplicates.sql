﻿drop temporary table if exists duplicated_INN;
create temporary table duplicated_INN (INN varchar(12));

insert into duplicated_INN(INN)
select INN
from Debtor
where INN is not null
group by INN
having count(id_Debtor)>1
order by INN;

select duplicated_INN.INN
, group_concat(distinct Debtor.id_Debtor SEPARATOR ',') id_Debtor
, group_concat(distinct ifnull(OGRN,'null') SEPARATOR ',') OGRN
, group_concat(distinct ifnull(SNILS,'null') SEPARATOR ',') SNILS
, group_concat(distinct ifnull(id_MProcedure,'null') SEPARATOR ',') id_MProcedure
, group_concat(distinct ifnull(Name,'null') SEPARATOR ',') Name
from duplicated_INN
inner join Debtor on duplicated_INN.INN=Debtor.INN
inner join MProcedure on MProcedure.id_Debtor=Debtor.id_Debtor
group by duplicated_INN.INN
-- having duplicated_INN.INN='9102004353'
\G

select Debtor.id_Debtor, INN, OGRN, SNILS, casenumber, procedure_type ptype, id_MProcedure id_mproc, Debtor.TimeCreated, Name
from Debtor 
left join MProcedure on MProcedure.id_Debtor=Debtor.id_Debtor
where INN='9102004353';

select Debtor.id_Debtor, INN, OGRN, SNILS, casenumber, procedure_type ptype, id_MProcedure id_mproc, Debtor.TimeCreated, Name
from Debtor 
left join MProcedure on MProcedure.id_Debtor=Debtor.id_Debtor
where INN='782600312326';

select count(Debtor.id_Debtor) from Debtor left join MProcedure on Debtor.id_Debtor=MProcedure.id_Debtor where id_MProcedure is null;

drop temporary table if exists duplicated_INN;
