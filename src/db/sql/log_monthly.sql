﻿select 
 concat(year(Time),'.',lpad(month(Time),2,'0')) p
 , Name
 , count(distinct id_Log) records
 , count(distinct IP) ips 
 , count(distinct Id) Ids 
from access_log
inner join log_type on access_log.id_Log_type=log_type.id_Log_type
where Time > DATE_SUB(now(), INTERVAL 300 DAY)
group by p, Name
order by p, Name;

select concat(year(Time),'.',lpad(month(Time),2,'0')) p, Name, count(distinct id_Log) records, count(distinct IP) ips, count(distinct Id) Ids from access_log inner join log_type on access_log.id_Log_type=log_type.id_Log_type where Time > DATE_SUB(now(), INTERVAL 300 DAY) group by p, Name order by p, Name;

select 
 concat(year(Time),'.',lpad(month(Time),2,'0')) p
 , count(distinct id_Log) records
 , count(distinct IP) ips 
 , count(distinct Id) Ids 
from access_log
inner join log_type on access_log.id_Log_type=log_type.id_Log_type
where Time > DATE_SUB(now(), INTERVAL 300 DAY)
and (Name like '%ios' || Name like '%andriod')
group by p
order by p;

select concat(year(Time),'.',lpad(month(Time),2,'0')) p, count(distinct id_Log) records, count(distinct IP) ips, count(distinct Id) Ids from access_log inner join log_type on access_log.id_Log_type=log_type.id_Log_type where Time > DATE_SUB(now(), INTERVAL 300 DAY) and (Name like '%ios' || Name like '%andriod') group by p order by p;

