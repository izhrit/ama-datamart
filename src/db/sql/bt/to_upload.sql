﻿SELECT 
count(*), max(pd.publicDate), min(pd.publicDate)
FROM MProcedure mp
INNER JOIN Manager m on m.id_Manager = mp.id_Manager
INNER JOIN PData pd on mp.id_MProcedure = pd.id_MProcedure
INNER JOIN Debtor d on mp.id_Debtor = d.id_Debtor

WHERE mp.ctb_revision = pd.revision 
AND (ctb_revision_posted is null or ctb_revision_posted != mp.ctb_revision) 
AND ctb_update_error = 0

and 'v'=m.VerificationState
and 'v'=mp.VerificationState
and 'v'=d.VerificationState

and not m.id_Manager in (401,535,102986,167692,15526)

and (m.BankroTechAccVerified is null or m.BankroTechAccVerified<>0)

and (last_update_start is null && last_update_finish is null || last_update_start < last_update_finish)
and m.BankroTechAcc is not null;