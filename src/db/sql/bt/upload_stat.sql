﻿SELECT 
	    (last_update_start is not null) started
	, if(0=ctb_update_error,0,1) error
	, if(0=ctb_account_limit,0,1) lim
	, count(distinct mp.id_MProcedure) proc
	, count(distinct m.id_Manager) man 
FROM MProcedure mp 
INNER JOIN Manager m on m.id_Manager = mp.id_Manager 
INNER JOIN PData pd on mp.id_MProcedure = pd.id_MProcedure 
INNER JOIN Debtor d on mp.id_Debtor = d.id_Debtor 
WHERE mp.ctb_revision = pd.revision 
AND 'v'=m.VerificationState 
and 'v'=mp.VerificationState 
and 'v'=d.VerificationState 
and (m.BankroTechAccVerified is null or m.BankroTechAccVerified<>0) 
and m.BankroTechAcc is not null 
group by started, error, lim;
