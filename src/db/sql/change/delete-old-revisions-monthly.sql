﻿delete pd
from PData pd
inner join MProcedure on pd.id_MProcedure=MProcedure.id_MProcedure
left join Leak on Leak.id_PData=pd.id_PData
left join PData pd1 on pd1.id_MProcedure=pd.id_MProcedure 
                   and month(pd.publicDate)=month(pd1.publicDate) 
                   and pd1.publicDate>pd.publicDate
where pd.revision<>MProcedure.revision 
   && pd.revision<>MProcedure.ctb_revision
   && id_Leak is null
   && pd.publicDate > date_sub(now(), interval 60 day)
   && pd1.id_PData is not null;