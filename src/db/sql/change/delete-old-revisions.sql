﻿delete PData 
from PData 
inner join MProcedure on PData.id_MProcedure=MProcedure.id_MProcedure
left join Leak on Leak.id_PData=PData.id_PData
where PData.revision<>MProcedure.revision 
&& PData.revision<>MProcedure.ctb_revision
&& PData.publicDate < date_sub(now(), interval 30 day)
&& id_Leak is null;
