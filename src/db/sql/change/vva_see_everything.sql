insert into MUser 
set 
 UserEmail='vva@russianit.ru'
,UserPassword='1'
,UserName='vva'
,Confirmed=1;

set @vva_id_MUser= last_insert_id();

insert into ManagerUser 
 (id_MUser,      id_Manager,   UserName, DefaultViewer)
select 
  @vva_id_MUser, m.id_Manager, 'vva',    0
from Manager m;

insert into MProcedureUser
 (id_MProcedure,   id_ManagerUser)
select
  p.id_MProcedure, u.id_ManagerUser
from MProcedure p
inner join Manager m on m.id_Manager=p.id_Manager
inner join ManagerUser u on u.id_Manager=m.id_Manager
where u.id_MUser=@vva_id_MUser;