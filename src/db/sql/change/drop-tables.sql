select 'drop table Vote_log' as '';
drop table if exists Vote_log;
select 'drop table vote_log' as '';
drop table if exists vote_log;

select 'drop table Vote_document' as '';
drop table if exists Vote_document;
select 'drop table vote_document' as '';
drop table if exists vote_document;

select 'drop table Vote' as '';
drop table if exists Vote;
select 'drop table vote' as '';
drop table if exists vote;

select 'drop table Meeting' as '';
drop table if exists Meeting;
select 'drop table meeting' as '';
drop table if exists meeting;

select 'drop table Leak' as '';
drop table if exists Leak;
select 'drop table leak' as '';
drop table if exists leak;

select 'drop table PData' as '';
drop table if exists PData;
select 'drop table pdata' as '';
drop table if exists pdata;

select 'drop table MProcedureUser' as '';
drop table if exists MProcedureUser;
select 'drop table mprocedureuser' as '';
drop table if exists mprocedureuser;

select 'drop table ManagerUser' as '';
drop table if exists ManagerUser;
select 'drop table manageruser' as '';
drop table if exists manageruser;

select 'drop table MProcedure' as '';
drop table if exists MProcedure;
select 'drop table mprocedure' as '';
drop table if exists mprocedure;

select 'drop table Manager' as '';
drop table if exists Manager;
select 'drop table manager' as '';
drop table if exists manager;

select 'drop table Contract' as '';
drop table if exists Contract;
select 'drop table contract' as '';
drop table if exists contract;

select 'drop table MUser' as '';
drop table if exists MUser;
select 'drop table muser' as '';
drop table if exists muser;

select 'drop table Session' as '';
drop table if exists Session;
select 'drop table session' as '';
drop table if exists session;

select 'drop table SentEmail' as '';
drop table if exists SentEmail;
select 'drop table sentemail' as '';
drop table if exists sentemail;

select 'drop table mock_crm2_Contract' as '';
drop table if exists mock_crm2_Contract;
select 'drop table mock_crm2_contract' as '';
drop table if exists mock_crm2_contract;