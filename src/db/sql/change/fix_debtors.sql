﻿update Debtor set SNILS=replace(SNILS,' ','');

update MProcedure mp 
inner join Debtor d on d.id_Debtor=mp.id_Debtor 
inner join Debtor d1 on d1.INN=d.INN or d1.SNILS=d.SNILS or d1.OGRN=d.OGRN
set mp.id_Debtor= if (mp.id_Debtor<d1.id_Debtor,mp.id_Debtor,d1.id_Debtor)
;

update MProcedure mp 
inner join Debtor d on d.id_Debtor=mp.id_Debtor 
inner join Debtor d1 on d1.INN=d.INN or d1.SNILS=d.SNILS or d1.OGRN=d.OGRN
set 
  d.Name= ifnull(d.Name,d1.Name)
, d.INN= ifnull(d.INN,d1.INN)
, d.OGRN= ifnull(d.OGRN,d1.OGRN)
, d.SNILS= ifnull(d.SNILS,d1.SNILS)
;

delete from Debtor 
where not exists (select * from MProcedure where Debtor.id_Debtor=MProcedure.id_Debtor)
  and not exists (select * from Request where Debtor.id_Debtor=Request.id_Debtor)
;