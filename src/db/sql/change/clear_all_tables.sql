select now(), '-------------------------------' `before`;

      select 'PData' `table`, count(*) `rows` from PData
union select 'MProcedureUser', count(*) from MProcedureUser
union select 'MProcedure', count(*) from MProcedure
union select 'ManagerUser', count(*) from ManagerUser
union select 'MUser', count(*) from MUser
union select 'mock_crm2_Contract', count(*) from mock_crm2_Contract
union select 'Manager', count(*) from Manager
union select 'Contract', count(*) from Contract
union select 'Session', count(*) from Session
;

select now(), '-------------------------------' `delete..`;
delete from PData;
delete from MProcedureUser;
delete from MProcedure;
delete from ManagerUser;
delete from MUser;
delete from mock_crm2_Contract;
delete from Manager;
delete from Contract;
delete from Session;

select now(), '-------------------------------' `after`;

      select 'PData' `table`, count(*) `rows` from PData
union select 'MProcedureUser', count(*) from MProcedureUser
union select 'MProcedure', count(*) from MProcedure
union select 'ManagerUser', count(*) from ManagerUser
union select 'MUser', count(*) from MUser
union select 'mock_crm2_Contract', count(*) from mock_crm2_Contract
union select 'Manager', count(*) from Manager
union select 'Contract', count(*) from Contract
union select 'Session', count(*) from Session
;