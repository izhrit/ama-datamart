select  
	concat(m.lastName,' ',left(m.firstName,1),' ',left(m.middleName,1)) manager 
	, concat(u.UserName,' - ',u.UserEmail) viewer 
	, count(d.id_Debtor) debtors 
from Manager m
inner join ManagerUser mu on m.id_Manager=mu.id_Manager
inner join MUser u on u.id_MUser=mu.id_MUser
inner join MProcedureUser mpu on mpu.id_ManagerUser=mu.id_ManagerUser
inner join MProcedure p on p.id_MProcedure=mpu.id_MProcedure
inner join Debtor d on d.id_Debtor=p.id_Debtor
where u.UserEmail not like '%rsit%' 
&& u.UserEmail not like '%russianit%'
group by manager, viewer
order by manager
\G

select u.UserEmail email, u.UserName viewer, count(distinct p.id_Manager) managers, count(distinct r.id_Request) requests,
count(distinct p.id_MProcedure) procedures
from MUser u
left join ManagerUser mu on u.id_MUser=mu.id_MUser
left join MProcedureUser mpu on mpu.id_ManagerUser=mu.id_ManagerUser
left join MProcedure p on p.id_MProcedure=mpu.id_MProcedure
left join Request r on r.id_MUser=u.id_MUser
where u.UserEmail not like '%rsit%' && u.UserEmail not like '%russianit%' && u.UserEmail not like '%rit%' && u.UserEmail not like '%nurijane%'
group by viewer, email
order by requests desc, managers desc, procedures desc, email
limit 1000;

select u.UserEmail email, u.UserName viewer, count(distinct p.id_Manager) managers, count(distinct r.id_Request) requests, count(distinct p.id_MProcedure) procedures from MUser u left join ManagerUser mu on u.id_MUser=mu.id_MUser left join MProcedureUser mpu on mpu.id_ManagerUser=mu.id_ManagerUser left join MProcedure p on p.id_MProcedure=mpu.id_MProcedure left join Request r on r.id_MUser=u.id_MUser where u.UserEmail not like '%rsit%' && u.UserEmail not like '%russianit%' && u.UserEmail not like '%rit%' && u.UserEmail not like '%nurijane%' group by viewer, email order by requests desc, managers desc, procedures desc, email limit 1000;

select concat(m.lastName,' ',left(m.firstName,1),' ',left(m.middleName,1)) manager, concat(u.UserName,' - ',u.UserEmail) viewer, d.Name debtor from Manager m inner join ManagerUser mu on m.id_Manager=mu.id_Manager inner join MUser u on u.id_MUser=mu.id_MUser inner join MProcedureUser mpu on mpu.id_ManagerUser=mu.id_ManagerUser inner join MProcedure p on p.id_MProcedure=mpu.id_MProcedure inner join Debtor d on d.id_Debtor=p.id_Debtor where u.UserEmail not like '%rsit%' && u.UserEmail not like '%russianit%' && u.UserEmail not like '%rit%' && u.UserEmail not like '%nurijane%'  order by manager;

select count(*) from MUser u  where u.UserEmail not like '%rsit%' && u.UserEmail not like '%russianit%' && u.UserEmail not like '%rit%' && u.UserEmail not like '%nurijane%';