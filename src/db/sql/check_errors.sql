select
	mpu.id_ManagerUser
	,mpu.id_MProcedure
	,mu.UserName
	,mu.id_Manager mu_id_Manager 
	,p.id_Manager p_id_Manager
from       MProcedureUser mpu
inner join MProcedure      p on mpu.id_MProcedure =p.id_MProcedure
inner join ManagerUser    mu on mpu.id_ManagerUser=mu.id_ManagerUser
where p.id_Manager<>mu.id_Manager
\G