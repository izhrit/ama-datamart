﻿(select 
  now() `записи`
  ,now() `всего`
  ,now() `для ЦТБ`)
union
(select
  'договоров'
  ,(
    select 
      count(distinct c.id_Contract) 
    from Contract c 
    inner join Manager m on c.id_Contract=m.id_Contract
  )
  ,(
    select 
      count(distinct c.id_Contract) 
    from Contract c 
    inner join Manager m on c.id_Contract=m.id_Contract 
    inner join MProcedure p on p.id_Manager=m.id_Manager
    where 0<>ctb_revision
  )
)
union
(select
  'АУ'
  ,(
    select 
      count(distinct m.id_Manager) 
    from Contract c 
    inner join Manager m on c.id_Contract=m.id_Contract
	inner join MProcedure p on p.id_Manager=m.id_Manager
  )
  ,(
    select 
      count(distinct m.id_Manager) 
    from Contract c 
    inner join Manager m on c.id_Contract=m.id_Contract 
    inner join MProcedure p on p.id_Manager=m.id_Manager
    where 0<>ctb_revision
  )
)
union
(select
  'Процедур'
  ,(
    select 
      count(distinct p.id_MProcedure) 
    from Contract c 
    inner join Manager m on c.id_Contract=m.id_Contract
	inner join MProcedure p on p.id_Manager=m.id_Manager
  )
  ,(
    select 
      count(distinct p.id_MProcedure) 
    from Contract c 
    inner join Manager m on c.id_Contract=m.id_Contract 
    inner join MProcedure p on p.id_Manager=m.id_Manager
    where 0<>ctb_revision
  )
)
union
(select
  'Закачек'
  ,(
    select 
      count(distinct d.id_Pdata) 
    from Contract c 
    inner join Manager m on c.id_Contract=m.id_Contract
	inner join MProcedure p on p.id_Manager=m.id_Manager
	inner join PData d on p.id_MProcedure=d.id_MProcedure
  )
  ,(
    select 
      count(distinct d.id_Pdata) 
    from Contract c 
    inner join Manager m on c.id_Contract=m.id_Contract 
    inner join MProcedure p on p.id_Manager=m.id_Manager
	inner join PData d on p.id_MProcedure=d.id_MProcedure
    where 0<>d.ctb_allowed
  )
)
;

(select now() `записи`, now() `всего`, now() `для ЦТБ`) union (select 'договоров',(select count(distinct c.id_Contract) from Contract c inner join Manager m on c.id_Contract=m.id_Contract),(select count(distinct c.id_Contract)from Contract c inner join Manager m on c.id_Contract=m.id_Contract inner join MProcedure p on p.id_Manager=m.id_Manager where 0<>ctb_revision))union(select 'АУ',(select count(distinct m.id_Manager) from Contract c inner join Manager m on c.id_Contract=m.id_Contract inner join MProcedure p on p.id_Manager=m.id_Manager),(select count(distinct m.id_Manager) from Contract c inner join Manager m on c.id_Contract=m.id_Contract inner join MProcedure p on p.id_Manager=m.id_Manager where 0<>ctb_revision))union(select 'Процедур',(select count(distinct p.id_MProcedure) from Contract c inner join Manager m on c.id_Contract=m.id_Contract inner join MProcedure p on p.id_Manager=m.id_Manager),(select count(distinct p.id_MProcedure)from Contract c inner join Manager m on c.id_Contract=m.id_Contract inner join MProcedure p on p.id_Manager=m.id_Manager where 0<>ctb_revision))union(select 'Закачек'  ,(select count(distinct d.id_Pdata)from Contract c inner join Manager m on c.id_Contract=m.id_Contract inner join MProcedure p on p.id_Manager=m.id_Manager inner join PData d on p.id_MProcedure=d.id_MProcedure),(select count(distinct d.id_Pdata) from Contract c inner join Manager m on c.id_Contract=m.id_Contract inner join MProcedure p on p.id_Manager=m.id_Manager inner join PData d on p.id_MProcedure=d.id_MProcedure where 0<>d.ctb_allowed));
