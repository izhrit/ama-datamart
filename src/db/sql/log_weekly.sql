﻿select 
 date(Time) day
 , Name
 , count(distinct id_Log) records
 , count(distinct IP) ips 
 , count(distinct Id) Ids 
from access_log
inner join log_type on access_log.id_Log_type=log_type.id_Log_type
where Time > DATE_SUB(now(), INTERVAL 7 DAY)
group by day, Name
order by day, Name;

select date(Time) day, Name, count(distinct id_Log) records, count(distinct IP) ips, count(distinct Id) Ids from access_log inner join log_type on access_log.id_Log_type=log_type.id_Log_type where Time > DATE_SUB(now(), INTERVAL 7 DAY) group by day, Name order by day, Name;

select 
 date(Time) day
 , Name
 , count(distinct id_Log) records
 , count(distinct IP) ips 
 , count(distinct Id) Ids 
 , count(distinct MProcedure.id_MProcedure) procedures
 , group_concat(distinct lastName) au
 , min(publicDate)
 , max(publicDate)
from access_log
inner join log_type on access_log.id_Log_type=log_type.id_Log_type
inner join Contract on Contract.id_Contract=Id
inner join Manager on Manager.id_Contract=Contract.id_Contract
inner join MProcedure on MProcedure.id_Manager=Manager.id_Manager
where Time > DATE_SUB(now(), INTERVAL 30 DAY)
group by day, Name
order by day, Name;


select 
 date(Time) day
 , count(distinct id_Log) logins
 , count(distinct IP) ips 
 , count(distinct Id) Ids 
from access_log
inner join log_type on access_log.id_Log_type=log_type.id_Log_type
where Time > DATE_SUB(now(), INTERVAL 30 DAY)
and (Name like '%ios' || Name like '%android')
group by day
order by day;

select 
 date(Time) day
 , count(distinct id_Log) logins
 , count(distinct IP) ips 
 , count(distinct Id) Ids 
from access_log
inner join log_type on access_log.id_Log_type=log_type.id_Log_type
where Time > DATE_SUB(now(), INTERVAL 30 DAY)
and (Name like '%ama')
group by day
order by day;