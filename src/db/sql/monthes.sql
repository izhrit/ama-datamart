﻿select 
	concat(year(d.publicDate),'.',lpad(month(d.publicDate),2,'0')) `период`
	, count(distinct d.id_PData) `загрузок`
	, sum(if(1=d.ctb_allowed,1,0)) `в ЦТБ`
	, count(distinct p.id_MProcedure) `процедур`
	, count(distinct m.id_Manager) `АУ`
	, count(distinct m.id_Contract) `Договоров`
from Contract c 
inner join Manager m on c.id_Contract=m.id_Contract 
inner join MProcedure p on p.id_Manager=m.id_Manager
inner join PData d on p.id_MProcedure=d.id_MProcedure
group by `период`
;

select concat(year(d.publicDate),'.',lpad(month(d.publicDate),2,'0')) `период`, count(distinct d.id_PData) `загрузок`, sum(if(1=d.ctb_allowed,1,0)) `в ЦТБ`, count(distinct p.id_MProcedure) `процедур`, count(distinct m.id_Manager) `АУ`, count(distinct m.id_Contract) `Договоров` from Contract c inner join Manager m on c.id_Contract=m.id_Contract inner join MProcedure p on p.id_Manager=m.id_Manager inner join PData d on p.id_MProcedure=d.id_MProcedure group by `период`;
