
CREATE TABLE access_log(
    id_Log         BIGINT         AUTO_INCREMENT,
    id_Log_type    INT            NOT NULL,
    IP             VARCHAR(40),
    Time           TIMESTAMP      NOT NULL,
    Details        TEXT,
    Id             INT,
    PRIMARY KEY (id_Log)
)ENGINE=INNODB
;



CREATE TABLE AnketaNP(
    id_AnketaNP    INT            AUTO_INCREMENT,
    id_MData       INT            NOT NULL,
    firstName      VARCHAR(50),
    lastName       VARCHAR(50),
    middleName     VARCHAR(50),
    Email          VARCHAR(50),
    SNILS          VARCHAR(13),
    INN            VARCHAR(12),
    PRIMARY KEY (id_AnketaNP)
)ENGINE=INNODB
;



CREATE TABLE Application(
    id_Application      INT          AUTO_INCREMENT,
    id_MUser            INT,
    application_data    LONGBLOB,
    date_create         TIMESTAMP,
    PRIMARY KEY (id_Application)
)ENGINE=INNODB
;



CREATE TABLE ApplicationFile(
    id_ApplicationFile    INT             AUTO_INCREMENT,
    id_Application        INT             NOT NULL,
    name                  VARCHAR(250),
    extension             VARBINARY(16),
    data                  LONGBLOB,
    PRIMARY KEY (id_ApplicationFile)
)ENGINE=INNODB
;



CREATE TABLE ApplicationRTK(
    id_ApplicationRTK    INT            AUTO_INCREMENT,
    id_MData             INT,
    creditor_inn         VARCHAR(12),
    PRIMARY KEY (id_ApplicationRTK)
)ENGINE=INNODB
;



CREATE TABLE Asset(
    id_Asset               INT            AUTO_INCREMENT,
    id_MProcedure          INT            NOT NULL,
    id_BalanceItem         INT,
    id_AssetGroup          INT,
    id_AssetSubGroup       INT,
    id_Region              INT            NOT NULL,
    Title                  TEXT           NOT NULL,
    Description            TEXT,
    Address                TEXT,
    TimeExpositionStart    DATETIME       NOT NULL,
    TimeChanged            TIMESTAMP      NOT NULL,
    ExtraFields            LONGBLOB       NOT NULL,
    isAvailableToAll       BIT             DEFAULT 0 NOT NULL,
    State                  CHAR(1)         DEFAULT 'a' NOT NULL,
    ID_Object              VARCHAR(40),
    md5hash                VARCHAR(40),
    Revision               BIGINT         NOT NULL,
    PRIMARY KEY (id_Asset)
)ENGINE=INNODB
;



CREATE TABLE Asset_EfrsbAssetClass(
    id_Asset              INT    NOT NULL,
    id_EfrsbAssetClass    INT    NOT NULL,
    PRIMARY KEY (id_Asset, id_EfrsbAssetClass)
)ENGINE=INNODB
;



CREATE TABLE Asset_MUser(
    id_Asset    INT    NOT NULL,
    id_MUser    INT    NOT NULL,
    PRIMARY KEY (id_Asset, id_MUser)
)ENGINE=INNODB
;



CREATE TABLE Asset_OKPD2(
    id_Asset    INT    NOT NULL,
    id_OKPD2    INT    NOT NULL,
    PRIMARY KEY (id_Asset, id_OKPD2)
)ENGINE=INNODB
;



CREATE TABLE Asset_proc_for_MUser(
    id_MProcedure    INT          NOT NULL,
    id_MUser         INT          NOT NULL,
    TimeCreated      TIMESTAMP    NOT NULL,
    Details          LONGBLOB,
    PRIMARY KEY (id_MProcedure, id_MUser)
)ENGINE=INNODB
;



CREATE TABLE AssetAttachment(
    id_AssetAttachment    INT             AUTO_INCREMENT,
    id_Asset              INT             NOT NULL,
    FileName              VARCHAR(250)    NOT NULL,
    FileSize              INT             NOT NULL,
    md5hash               VARCHAR(40)     NOT NULL,
    Body                  LONGBLOB        NOT NULL,
    Revision              BIGINT          NOT NULL,
    PRIMARY KEY (id_AssetAttachment)
)ENGINE=INNODB
;



CREATE TABLE AssetGroup(
    id_AssetGroup    INT             AUTO_INCREMENT,
    Title            VARCHAR(100)    NOT NULL,
    PRIMARY KEY (id_AssetGroup)
)ENGINE=INNODB
;



CREATE TABLE AssetSubGroup(
    id_AssetSubGroup    INT             AUTO_INCREMENT,
    id_AssetGroup       INT             NOT NULL,
    Title               VARCHAR(100),
    PRIMARY KEY (id_AssetSubGroup)
)ENGINE=INNODB
;



CREATE TABLE Attachment(
    id_Attachment    INT            AUTO_INCREMENT,
    id_SentEmail     INT            NOT NULL,
    FileName         VARCHAR(50),
    Content          LONGBLOB,
    PRIMARY KEY (id_Attachment)
)ENGINE=INNODB
;



CREATE TABLE AwardNominee(
    id_AwardNominee    INT            AUTO_INCREMENT,
    inn                VARCHAR(13)    NOT NULL,
    firstName          VARCHAR(50),
    lastName           VARCHAR(50),
    middleName         VARCHAR(50),
    SRO                TEXT,
    URL                TEXT,
    PRIMARY KEY (id_AwardNominee)
)ENGINE=INNODB
;



CREATE TABLE AwardVote(
    id_AwardVote         INT            AUTO_INCREMENT,
    id_AwardNominee      INT            NOT NULL,
    inn                  VARCHAR(13)    NOT NULL,
    VoteTime             DATETIME       NOT NULL,
    BulletinText         LONGBLOB,
    BulletinSignature    LONGBLOB,
    firstName            VARCHAR(50),
    lastName             VARCHAR(50),
    middleName           VARCHAR(50),
    SRO                  TEXT,
    email                VARCHAR(50),
    PRIMARY KEY (id_AwardVote)
)ENGINE=INNODB
;



CREATE TABLE BalanceItem(
    id_BalanceItem    INT             AUTO_INCREMENT,
    Title             VARCHAR(200),
    code              CHAR(10)        NOT NULL,
    PRIMARY KEY (id_BalanceItem)
)ENGINE=INNODB
;



CREATE TABLE BFile(
    id_BFile       INT             AUTO_INCREMENT,
    id_BObject     INT,
    id_BLot        INT,
    id_Bidding     INT             NOT NULL,
    BFileType      CHAR(1)         NOT NULL,
    FileName       VARCHAR(250)    NOT NULL,
    Body           LONGBLOB        NOT NULL,
    Description    TEXT,
    PRIMARY KEY (id_BFile)
)ENGINE=INNODB
;



CREATE TABLE Bidding(
    id_Bidding                        INT             AUTO_INCREMENT,
    id_MProcedure                     INT             NOT NULL,
    Name                              TEXT,
    Time_accept_application_start     DATETIME,
    Time_accept_application_finish    DATETIME        NOT NULL,
    Time_start                        DATETIME,
    TimeOfCreation                    DATETIME        NOT NULL,
    BiddingType                       CHAR(1)         NOT NULL,
    ETP                               CHAR(1),
    ExternalURL                       TEXT,
    EfrsbMessageNumber                VARCHAR(50),
    PublicationMediaName              VARCHAR(100),
    PublicationMediaDate              DATETIME,
    PublicationMediaNumber            VARCHAR(50),
    PRIMARY KEY (id_Bidding)
)ENGINE=INNODB
;



CREATE TABLE BLot(
    id_BLot                   INT               AUTO_INCREMENT,
    id_Bidding                INT               NOT NULL,
    Number                    INT               NOT NULL,
    Name                      TEXT              NOT NULL,
    Description               TEXT,
    Inspection_description    TEXT,
    Address                   TEXT,
    Price_start               DECIMAL(10, 0)    NOT NULL,
    Price_step                DECIMAL(13, 2),
    Price_step_unit           CHAR(1)            DEFAULT 'r' NOT NULL,
    Price_prepayment          DECIMAL(13, 2),
    Price_prepayment_unit     CHAR(1)            DEFAULT 'r' NOT NULL,
    PRIMARY KEY (id_BLot)
)ENGINE=INNODB
;



CREATE TABLE BObject(
    id_BObject     INT        AUTO_INCREMENT,
    id_BLot        INT        NOT NULL,
    Name           TEXT       NOT NULL,
    BObjectType    CHAR(1)    NOT NULL,
    ExtraFields    TEXT,
    PRIMARY KEY (id_BObject)
)ENGINE=INNODB
;



CREATE TABLE CCDocument(
    id_CCDocument    INT             AUTO_INCREMENT,
    id_CourtCase     INT             NOT NULL,
    DocumentDate     DATETIME        NOT NULL,
    URL              VARCHAR(250),
    PRIMARY KEY (id_CCDocument)
)ENGINE=INNODB
;



CREATE TABLE CCEvent(
    id_CCEvent       INT         AUTO_INCREMENT,
    id_CCDocument    INT         NOT NULL,
    EventType        CHAR(1)     NOT NULL,
    EventTime        DATETIME    NOT NULL,
    PRIMARY KEY (id_CCEvent)
)ENGINE=INNODB
;



CREATE TABLE CCPushedEvent(
    id_CCEvent         INT         NOT NULL,
    Time_dispatched    DATETIME    NOT NULL,
    Time_pushed        CHAR(10),
    PRIMARY KEY (id_CCEvent)
)ENGINE=INNODB
;



CREATE TABLE Contract(
    id_Contract       INT            AUTO_INCREMENT,
    ContractNumber    VARCHAR(40),
    PRIMARY KEY (id_Contract)
)ENGINE=INNODB
;



CREATE TABLE ContractUser(
    id_ContractUser    INT            AUTO_INCREMENT,
    id_Contract        INT            NOT NULL,
    id_MUser           INT            NOT NULL,
    UserName           VARCHAR(70),
    PRIMARY KEY (id_ContractUser)
)ENGINE=INNODB
;



CREATE TABLE Court(
    id_Court        INT            AUTO_INCREMENT,
    id_Region       INT,
    Name            VARCHAR(70)    NOT NULL,
    SearchName      VARCHAR(30),
    ShortName       VARCHAR(40),
    CasebookTag     VARCHAR(50),
    Address         TEXT,
    NumberPrefix    VARCHAR(5),
    PRIMARY KEY (id_Court)
)ENGINE=INNODB
;



CREATE TABLE CourtCase(
    id_CourtCase    INT    AUTO_INCREMENT,
    id_Debtor       INT    NOT NULL,
    PRIMARY KEY (id_CourtCase)
)ENGINE=INNODB
;



CREATE TABLE CourtCaseParticipant(
    id_CourtCase    INT    NOT NULL,
    id_MUser        INT    NOT NULL,
    PRIMARY KEY (id_CourtCase, id_MUser)
)ENGINE=INNODB
;



CREATE TABLE Debtor(
    id_Debtor            INT             AUTO_INCREMENT,
    INN                  VARCHAR(12),
    OGRN                 VARBINARY(16),
    SNILS                VARCHAR(13),
    Name                 VARCHAR(250),
    VerificationState    CHAR(1),
    BankruptId           BIGINT,
    TimeCreated          TIMESTAMP       NOT NULL,
    TimeVerified         DATETIME,
    PRIMARY KEY (id_Debtor)
)ENGINE=INNODB
;



CREATE TABLE DocType(
    id_DocType     INT             AUTO_INCREMENT,
    Name           VARCHAR(250)    NOT NULL,
    NameAddPart    VARCHAR(250),
    ExtraParams    LONGBLOB        NOT NULL,
    PRIMARY KEY (id_DocType)
)ENGINE=INNODB
;



CREATE TABLE efrsb_debtor(
    id_Debtor          INT             AUTO_INCREMENT,
    BankruptId         BIGINT,
    ArbitrManagerID    INT,
    INN                VARCHAR(12),
    SNILS              VARCHAR(11),
    Name               VARCHAR(152),
    OGRN               VARCHAR(15),
    Revision           BIGINT,
    LegalAdress        VARCHAR(400),
    LegalCaseList      LONGBLOB,
    Archive            BIT              DEFAULT 0 NOT NULL,
    LastMessageDate    DATETIME,
    PRIMARY KEY (id_Debtor)
)ENGINE=INNODB
;



CREATE TABLE efrsb_debtor_manager(
    id_Debtor_Manager        INTEGER     NOT NULL,
    BankruptId               BIGINT,
    ArbitrManagerID          INT,
    DateTime_MessageFirst    DATETIME,
    DateTime_MessageLast     DATETIME,
    Revision                 BIGINT,
    PRIMARY KEY (id_Debtor_Manager)
)ENGINE=INNODB
;



CREATE TABLE efrsb_manager(
    id_Manager               INT             AUTO_INCREMENT,
    ArbitrManagerID          INT             NOT NULL,
    SRORegNum                VARCHAR(30),
    FirstName                VARCHAR(50)     NOT NULL,
    MiddleName               VARCHAR(50),
    LastName                 VARCHAR(50)     NOT NULL,
    OGRNIP                   VARCHAR(15),
    INN                      VARCHAR(12)     NOT NULL,
    SRORegDate               DATETIME,
    RegNum                   VARCHAR(30),
    Revision                 BIGINT          NOT NULL,
    CorrespondenceAddress    VARCHAR(300),
    SNILS                    VARCHAR(11),
    Phone                    VARCHAR(12),
    EMail                    VARCHAR(100),
    PRIMARY KEY (id_Manager)
)ENGINE=INNODB
;



CREATE TABLE efrsb_sro(
    id_SRO         INT             AUTO_INCREMENT,
    OGRN           VARCHAR(15),
    RegNum         VARCHAR(30)     NOT NULL,
    INN            VARCHAR(10)     NOT NULL,
    Name           VARCHAR(250),
    ShortTitle     VARCHAR(250),
    Title          VARCHAR(250),
    UrAdress       VARCHAR(250),
    Revision       BIGINT          NOT NULL,
    SROEmail       VARCHAR(100),
    SROPassword    VARCHAR(100),
    CEOName        VARCHAR(250),
    ExtraFields    LONGBLOB,
    PRIMARY KEY (id_SRO)
)ENGINE=INNODB
;



CREATE TABLE EfrsbAssetClass(
    id_EfrsbAssetClass           INT            AUTO_INCREMENT,
    id_EfrsbAssetClass_Parent    INT,
    Title                        TEXT           NOT NULL,
    Code                         VARCHAR(10)    NOT NULL,
    PRIMARY KEY (id_EfrsbAssetClass)
)ENGINE=INNODB
;



CREATE TABLE EmailToSend(
    id_SentEmail    INT     NOT NULL,
    ErrorText       TEXT,
    PRIMARY KEY (id_SentEmail)
)ENGINE=INNODB
;



CREATE TABLE event(
    id_Event           INT            AUTO_INCREMENT,
    efrsb_id           INT            NOT NULL,
    ArbitrManagerID    INT            NOT NULL,
    BankruptId         BIGINT,
    EventTime          DATETIME       NOT NULL,
    MessageType        CHAR(1)        NOT NULL,
    MessageNumber      VARCHAR(30),
    MessageGuid        VARCHAR(32),
    Revision           BIGINT         NOT NULL,
    isActive           BIT             DEFAULT 1 NOT NULL,
    PRIMARY KEY (id_Event)
)ENGINE=INNODB
;



CREATE TABLE FaUsing(
    ContractNumber    VARCHAR(40),
    LicenseToken      TEXT,
    UsingTime         DATETIME       NOT NULL,
    IP                VARCHAR(40)    NOT NULL,
    Section           CHAR(4)
)ENGINE=INNODB
;



CREATE TABLE GCEvent(
    id_GCEvent           INT            AUTO_INCREMENT,
    id_GoogleCalendar    INT            NOT NULL,
    id_MProcedure        INT,
    EventTime            DATETIME       NOT NULL,
    EventType            CHAR(1)        NOT NULL,
    Body                 LONGBLOB,
    EventTimeEnd         DATETIME,
    EventSource          VARCHAR(20),
    PRIMARY KEY (id_GCEvent)
)ENGINE=INNODB
;



CREATE TABLE GCPushedEvent(
    id_GCEvent         INT         NOT NULL,
    Time_dispatched    DATETIME    NOT NULL,
    Time_pushed        DATETIME,
    ErrorText          TEXT,
    PRIMARY KEY (id_GCEvent)
)ENGINE=INNODB
;



CREATE TABLE GoogleCalendar(
    id_GoogleCalendar    INT             AUTO_INCREMENT,
    id_Manager           INT             NOT NULL,
    email                VARCHAR(100),
    refresh_token        VARCHAR(250)    NOT NULL,
    nextSyncToken        VARCHAR(50),
    lastUpload           DATETIME,
    PRIMARY KEY (id_GoogleCalendar)
)ENGINE=INNODB
;



CREATE TABLE Job(
    id_Job           INT             AUTO_INCREMENT,
    id_JobSite       INT             NOT NULL,
    Name             VARCHAR(100),
    Description      TEXT,
    MaxAgeMinutes    INT             NOT NULL,
    Status           INT,
    Enabled          BIT              DEFAULT 1 NOT NULL,
    PRIMARY KEY (id_Job)
)ENGINE=INNODB
;



CREATE TABLE JobLog(
    id_JobLog    INT         AUTO_INCREMENT,
    id_Job       INT         NOT NULL,
    Started      DATETIME,
    Finished     DATETIME,
    Status       INT,
    PRIMARY KEY (id_JobLog)
)ENGINE=INNODB
;



CREATE TABLE JobPart(
    id_JobPart          INT             AUTO_INCREMENT,
    id_Job              INT             NOT NULL,
    Name                VARCHAR(100),
    Description         TEXT,
    Status              INT,
    Enabled             BIT              DEFAULT 1 NOT NULL,
    ExtraStateFields    LONGBLOB,
    PRIMARY KEY (id_JobPart)
)ENGINE=INNODB
;



CREATE TABLE JobPartLog(
    id_JobPartLog    INT         AUTO_INCREMENT,
    id_JobLog        INT         NOT NULL,
    id_JobPart       INT         NOT NULL,
    Started          DATETIME,
    Finished         DATETIME,
    Status           CHAR(10),
    Results          TEXT,
    Number           INT          DEFAULT 0 NOT NULL,
    PRIMARY KEY (id_JobPartLog)
)ENGINE=INNODB
;



CREATE TABLE JobSite(
    id_JobSite     INT             AUTO_INCREMENT,
    Name           VARCHAR(100),
    Description    TEXT,
    SyncTime       DATETIME,
    PRIMARY KEY (id_JobSite)
)ENGINE=INNODB
;



CREATE TABLE Leak(
    id_Leak           INT               AUTO_INCREMENT,
    id_PData          INT               NOT NULL,
    LeakSum           DECIMAL(10, 0)    NOT NULL,
    LeakDate          DATETIME,
    ContragentName    VARCHAR(100),
    ContragentInn     CHAR(12),
    isAccredited      BIT,
    PRIMARY KEY (id_Leak)
)ENGINE=INNODB
;



CREATE TABLE log_type(
    id_Log_type    INT             AUTO_INCREMENT,
    Name           VARCHAR(100)    NOT NULL,
    PRIMARY KEY (id_Log_type)
)ENGINE=INNODB
;



CREATE TABLE Manager(
    id_Manager                    INT             AUTO_INCREMENT,
    id_Contract                   INT,
    id_SRO                        INT,
    firstName                     VARCHAR(50),
    lastName                      VARCHAR(50),
    middleName                    VARCHAR(50),
    efrsbNumber                   VARCHAR(50),
    ManagerEmail                  VARCHAR(100),
    Password                      VARCHAR(100),
    ManagerEmail_Change           VARCHAR(70),
    ManagerEmail_ChangeTime       DATETIME,
    ManagerPassword_Change        VARCHAR(30),
    ManagerPassword_ChangeTime    DATETIME,
    ExtraParams                   LONGBLOB,
    INN                           VARCHAR(12),
    BankroTechAcc                 TEXT,
    BankroTechAccVerified         BIT,
    VerificationState             CHAR(1),
    ArbitrManagerID               INT,
    TimeCreated                   TIMESTAMP       NOT NULL,
    TimeVerified                  DATETIME,
    HasIncoming                   BIT              DEFAULT 0 NOT NULL,
    ProtocolNum                   VARCHAR(50),
    ProtocolDate                  DATETIME,
    id_Contract_Change            INT,
    ctb_account_limit             BIT              DEFAULT 0 NOT NULL,
    PRIMARY KEY (id_Manager)
)ENGINE=INNODB
;



CREATE TABLE ManagerCertAuth(
    id_ManagerCertAuth    INT            AUTO_INCREMENT,
    id_Manager            INT,
    TimeStarted           DATETIME       NOT NULL,
    Auth                  VARCHAR(32),
    Body                  LONGBLOB,
    PRIMARY KEY (id_ManagerCertAuth)
)ENGINE=INNODB
;



CREATE TABLE ManagerDocument(
    id_ManagerDocument    INT             AUTO_INCREMENT,
    id_Manager            INT             NOT NULL,
    Body                  LONGBLOB        NOT NULL,
    FileName              VARCHAR(100)    NOT NULL,
    DocumentType          CHAR(1),
    PubLock               LONGBLOB,
    PRIMARY KEY (id_ManagerDocument)
)ENGINE=INNODB
;



CREATE TABLE ManagerUser(
    id_ManagerUser    INT            AUTO_INCREMENT,
    id_MUser          INT            NOT NULL,
    id_Manager        INT            NOT NULL,
    UserName          VARCHAR(70),
    DefaultViewer     BIT            NOT NULL,
    PRIMARY KEY (id_ManagerUser)
)ENGINE=INNODB
;



CREATE TABLE MData(
    id_MData          INT             AUTO_INCREMENT,
    id_ManagerUser    INT             NOT NULL,
    id_Debtor         INT,
    publicDate        DATETIME        NOT NULL,
    fileData          LONGBLOB        NOT NULL,
    revision          INT             NOT NULL,
    ContentHash       VARCHAR(250),
    MData_Type        CHAR(1)         NOT NULL,
    PRIMARY KEY (id_MData)
)ENGINE=INNODB
;



CREATE TABLE MData_attachment(
    id_MData_attachment    INT            AUTO_INCREMENT,
    id_MData               INT            NOT NULL,
    FileName               VARCHAR(50),
    Content                LONGBLOB,
    PRIMARY KEY (id_MData_attachment)
)ENGINE=INNODB
;



CREATE TABLE Meeting(
    id_Meeting       INT         AUTO_INCREMENT,
    id_MProcedure    INT         NOT NULL,
    Date             DATETIME    NOT NULL,
    Body             LONGBLOB    NOT NULL,
    State            CHAR(1)      DEFAULT 'a' NOT NULL,
    PRIMARY KEY (id_Meeting)
)ENGINE=INNODB
;



CREATE TABLE Meeting_document(
    id_Meeting_document      INT             AUTO_INCREMENT,
    id_Meeting               INT             NOT NULL,
    Meeting_document_time    DATETIME        NOT NULL,
    DocumentName             VARCHAR(250)    NOT NULL,
    FileName                 VARCHAR(40)     NOT NULL,
    Body                     LONGBLOB,
    Parameters               LONGBLOB,
    PRIMARY KEY (id_Meeting_document)
)ENGINE=INNODB
;



CREATE TABLE Message(
    id_Message                 INT            AUTO_INCREMENT,
    efrsb_id                   INT            NOT NULL,
    ArbitrManagerID            INT,
    BankruptId                 BIGINT,
    INN                        VARCHAR(12),
    SNILS                      VARCHAR(20),
    OGRN                       VARCHAR(20),
    PublishDate                DATETIME       NOT NULL,
    Body                       LONGBLOB       NOT NULL,
    MessageInfo_MessageType    CHAR(1),
    Number                     VARCHAR(30),
    MessageGUID                VARCHAR(32),
    Revision                   BIGINT         NOT NULL,
    PRIMARY KEY (id_Message)
)ENGINE=INNODB
;



CREATE TABLE mock_crm2_Contract(
    Id                VARCHAR(40)    NOT NULL,
    ContractNumber    VARCHAR(20),
    ContractDate      DATETIME,
    Login             VARCHAR(20),
    Password          VARCHAR(20),
    AMAIsTrial        TINYINT,
    PRIMARY KEY (Id)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_debtor(
    id_Debtor          INT             AUTO_INCREMENT,
    ArbitrManagerID    INT,
    BankruptId         BIGINT          NOT NULL,
    Name               VARCHAR(152),
    INN                VARCHAR(12),
    OGRN               VARCHAR(15),
    SNILS              VARCHAR(11),
    Body               LONGBLOB        NOT NULL,
    LastMessageDate    DATETIME,
    Revision           BIGINT          NOT NULL,
    Archive            BIT              DEFAULT 0 NOT NULL,
    PRIMARY KEY (id_Debtor)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_debtor_manager(
    id_Debtor_Manager    INT       AUTO_INCREMENT,
    ArbitrManagerID      INT       NOT NULL,
    BankruptId           BIGINT    NOT NULL,
    Revision             BIGINT    NOT NULL,
    PRIMARY KEY (id_Debtor_Manager)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_email(
    id_Email    INT            AUTO_INCREMENT,
    address     VARCHAR(50)    NOT NULL,
    PRIMARY KEY (id_Email)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_email_manager(
    id_Email           INT         NOT NULL,
    id_Manager         INT         NOT NULL,
    id_Messages        LONGBLOB    NOT NULL,
    id_Message_Last    INT,
    PRIMARY KEY (id_Email, id_Manager)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_event(
    id_Event                   INT         AUTO_INCREMENT,
    ArbitrManagerID            INT         NOT NULL,
    BankruptId                 BIGINT      NOT NULL,
    efrsb_id                   INT         NOT NULL,
    EventTime                  DATETIME    NOT NULL,
    MessageInfo_MessageType    CHAR(1)     NOT NULL,
    Revision                   BIGINT      NOT NULL,
    isActive                   BIT          DEFAULT 1 NOT NULL,
    PRIMARY KEY (id_Event)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_manager(
    id_Manager               INT             AUTO_INCREMENT,
    ArbitrManagerID          INT             NOT NULL,
    SRORegNum                VARCHAR(30)     NOT NULL,
    FirstName                VARCHAR(50)     NOT NULL,
    LastName                 VARCHAR(50)     NOT NULL,
    MiddleName               VARCHAR(50),
    INN                      VARCHAR(12)     NOT NULL,
    OGRNIP                   VARCHAR(12),
    RegNum                   VARCHAR(30),
    Body                     LONGBLOB        NOT NULL,
    DateDelete               DATETIME,
    Revision                 BIGINT          NOT NULL,
    CorrespondenceAddress    VARCHAR(300),
    SNILS                    VARCHAR(11),
    SRORegDate               DATETIME,
    PRIMARY KEY (id_Manager)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_message(
    id_Message                 INT            AUTO_INCREMENT,
    efrsb_id                   INT            NOT NULL,
    ArbitrManagerID            INT,
    BankruptId                 BIGINT,
    INN                        VARCHAR(12),
    SNILS                      VARCHAR(20),
    OGRN                       VARCHAR(20),
    PublishDate                DATETIME       NOT NULL,
    Body                       LONGBLOB       NOT NULL,
    MessageInfo_MessageType    CHAR(1),
    Number                     VARCHAR(30),
    MessageGUID                VARCHAR(32),
    Revision                   BIGINT         NOT NULL,
    PRIMARY KEY (id_Message)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_sro(
    id_SRO           INT             NOT NULL,
    OGRN             VARCHAR(15),
    RegNum           VARCHAR(30)     NOT NULL,
    Body             LONGBLOB        NOT NULL,
    INN              VARCHAR(10)     NOT NULL,
    DateLastModif    DATETIME        NOT NULL,
    Name             VARCHAR(250),
    ShortTitle       VARCHAR(250),
    Title            VARCHAR(250),
    UrAdress         VARCHAR(250),
    Revision         BIGINT          NOT NULL,
    PRIMARY KEY (id_SRO)
)ENGINE=INNODB
;



CREATE TABLE MProcedure(
    id_MProcedure          INT             AUTO_INCREMENT,
    id_Manager             INT             NOT NULL,
    id_Debtor              INT             NOT NULL,
    casenumber             VARCHAR(60),
    features               CHAR(1),
    procedure_type         CHAR(1)         NOT NULL,
    registrationDate       DATETIME,
    publicDate             DATETIME,
    revision               BIGINT,
    ctb_publicDate         DATETIME,
    ctb_revision           BIGINT           DEFAULT 0 NOT NULL,
    Content_hash           VARCHAR(250),
    ExtraParams            LONGBLOB,
    ctb_bankrotech_id      VARCHAR(45),
    last_update_log        LONGBLOB,
    ctb_update_error       BIT              DEFAULT 0 NOT NULL,
    ctb_revision_posted    BIGINT,
    last_update_start      DATETIME,
    last_update_finish     DATETIME,
    VerificationState      CHAR(1),
    id_Debtor_Manager      INT,
    TimeCreated            TIMESTAMP       NOT NULL,
    TimeVerified           DATETIME,
    Archive                BIT              DEFAULT 0 NOT NULL,
    ArchivedByUser         BIT,
    PRIMARY KEY (id_MProcedure)
)ENGINE=INNODB
;



CREATE TABLE MProcedureUser(
    id_MProcedure     INT    NOT NULL,
    id_ManagerUser    INT    NOT NULL,
    id_Request        INT,
    PRIMARY KEY (id_MProcedure, id_ManagerUser)
)ENGINE=INNODB
;



CREATE TABLE MRequest(
    id_MRequest             INT             AUTO_INCREMENT,
    id_Court                INT             NOT NULL,
    id_SRO                  INT             NOT NULL,
    id_Manager              INT,
    CaseNumber              VARCHAR(50),
    DebtorCategory          CHAR(1)          DEFAULT 'n' NOT NULL,
    DebtorName              VARCHAR(250),
    DebtorINN               VARCHAR(12),
    DebtorSNILS             VARCHAR(13),
    DebtorOGRN              VARBINARY(16),
    DateOfApplication       DATETIME,
    DateOfCreation          DATETIME        NOT NULL,
    DebtorAddress           VARCHAR(250),
    DebtorCategory2         CHAR(1),
    DebtorName2             VARCHAR(250),
    DebtorINN2              VARCHAR(12),
    DebtorSNILS2            VARCHAR(13),
    DebtorOGRN2             VARBINARY(16),
    DebtorAddress2          VARCHAR(250),
    ApplicantName           VARCHAR(250),
    DateOfRequest           DATETIME        NOT NULL,
    ApplicantINN            VARCHAR(12),
    DateOfResponce          DATETIME,
    NextSessionDate         DATETIME,
    ApplicantSNILS          VARCHAR(13),
    ApplicantOGRN           VARBINARY(16),
    ApplicantAddress        VARCHAR(250),
    ApplicantCategory       CHAR(1),
    DateOfRequestAct        DATETIME,
    CourtDecisionURL        VARCHAR(250),
    CourtDecisionAddInfo    VARCHAR(250),
    DateOfOffer             DATETIME,
    DateOfOfferTill         DATETIME,
    PRIMARY KEY (id_MRequest)
)ENGINE=INNODB
;



CREATE TABLE MUser(
    id_MUser                     INT             AUTO_INCREMENT,
    Confirmation_Token_Cookie    CHAR(10),
    Confirmation_Token           CHAR(4),
    UserEmail                    VARCHAR(70),
    UserPassword                 VARCHAR(30),
    UserName                     VARCHAR(70),
    UserPhone                    VARCHAR(12),
    Confirmed                    BIT              DEFAULT 0 NOT NULL,
    UserEmail_Change             VARCHAR(70),
    UserEmail_ChangeTime         DATETIME,
    UserPassword_Change          VARCHAR(30),
    UserPassword_ChangeTime      DATETIME,
    Monitoring                   LONGBLOB,
    HasOutcoming                 BIT              DEFAULT 0 NOT NULL,
    CreatedTime                  DATETIME         DEFAULT '2020-12-30' NOT NULL,
    UserExecutorName             VARCHAR(250),
    UserINN                      VARCHAR(12),
    UserOGRNIP                   VARCHAR(15),
    UserKPP                      VARCHAR(9),
    UserBIK                      VARCHAR(9),
    UserCheckingAccount          VARCHAR(20),
    UserOKPO                     VARCHAR(14),
    UserOfficialAddress          VARCHAR(250),
    UserMailingAddress           VARCHAR(250),
    UserContactPhone             VARCHAR(12),
    UserTelegram                 VARCHAR(12),
    UserWhatsApp                 VARCHAR(12),
    UserViber                    VARCHAR(12),
    UserContactEmail             VARCHAR(70),
    UserPhoto                    LONGBLOB,
    UserPhotoName                VARCHAR(250),
    TimeCreated                  TIME            NOT NULL,
    PRIMARY KEY (id_MUser)
)ENGINE=INNODB
;



CREATE TABLE OKPD2(
    id_OKPD2           INT             AUTO_INCREMENT,
    id_OKPD2_Parent    INT,
    Title              VARCHAR(250)    NOT NULL,
    code_full          VARCHAR(50),
    code_part          CHAR(10),
    PRIMARY KEY (id_OKPD2)
)ENGINE=INNODB
;



CREATE TABLE PData(
    id_PData         INT             AUTO_INCREMENT,
    id_MProcedure    INT             NOT NULL,
    publicDate       DATETIME        NOT NULL,
    fileData         LONGBLOB        NOT NULL,
    revision         BIGINT          NOT NULL,
    ctb_allowed      BIT              DEFAULT 0 NOT NULL,
    Content_hash     VARCHAR(250),
    Processed        BIT,
    PRIMARY KEY (id_PData)
)ENGINE=INNODB
;



CREATE TABLE PDDocFile(
    id_PDDocFile     INT             AUTO_INCREMENT,
    id_PDDocument    INT             NOT NULL,
    Name             VARCHAR(250),
    Page             INT             NOT NULL,
    Data             LONGBLOB        NOT NULL,
    PRIMARY KEY (id_PDDocFile)
)ENGINE=INNODB
;



CREATE TABLE PDDocument(
    id_PDDocument            INT             AUTO_INCREMENT,
    id_PreDebtor             INT             NOT NULL,
    id_DocType               INT,
    Name                     VARCHAR(250)    NOT NULL,
    NameAddPart              VARCHAR(250),
    Status                   CHAR(1),
    StatusComment            VARCHAR(250),
    Comment                  TEXT,
    DateLastChangingFiles    DATETIME,
    PRIMARY KEY (id_PDDocument)
)ENGINE=INNODB
;



CREATE TABLE PeproUsing(
    ContractNumber    VARCHAR(40),
    LicenseToken      TEXT,
    UsingTime         DATETIME       NOT NULL,
    IP                VARCHAR(40)    NOT NULL,
    Section           CHAR(4)
)ENGINE=INNODB
;



CREATE TABLE PreDebtor(
    id_PreDebtor                 INT             AUTO_INCREMENT,
    id_MUser                     INT,
    id_Manager                   INT,
    id_Court                     INT,
    id_Region                    INT,
    CaseNumber                   VARCHAR(50),
    DebtorName                   VARCHAR(250)    NOT NULL,
    OldDebtorName                VARCHAR(250),
    OldDebtorName2               VARCHAR(250),
    DebtorCategory               CHAR(1)         NOT NULL,
    DebtorINN                    VARCHAR(12),
    DebtorOGRN                   VARBINARY(16),
    DebtorSNILS                  VARCHAR(13),
    DebtorBirthday               DATE,
    DebtorBirthAddress           VARCHAR(250),
    DebtorCabinetPassword        VARCHAR(13),
    DebtorCabinetUUID            VARCHAR(27),
    DebtorIDCardType             VARCHAR(50),
    DebtorIdCardNumber           VARCHAR(50),
    DebtorIdCardSeries           VARCHAR(50),
    DebtorRegistrationAddress    VARCHAR(250),
    DebtorAddress                VARCHAR(250),
    DebtorAddressArea            VARCHAR(250),
    DebtorAddressBlock           VARCHAR(10),
    DebtorAddressBuildStatus     CHAR(1),
    DebtorAddressCity            VARCHAR(250),
    DebtorAddressFlat            VARCHAR(10),
    DebtorAddressHouse           VARCHAR(10),
    DebtorAddressRegion          VARCHAR(250),
    DebtorAddressSettlment       VARCHAR(250),
    DebtorAddressStreet          VARCHAR(250),
    DebtorEmail                  VARCHAR(100),
    DebtorPhone                  VARCHAR(12),
    Note                         VARCHAR(250),
    ContractNumber               VARCHAR(50),
    DateOfContract               DATETIME,
    DateOfApplication            DATETIME,
    ShowToAU                     BIT              DEFAULT 0 NOT NULL,
    PRIMARY KEY (id_PreDebtor)
)ENGINE=INNODB
;



CREATE TABLE ProcedureStart(
    id_ProcedureStart               INT             AUTO_INCREMENT,
    id_Court                        INT             NOT NULL,
    id_Contract                     INT,
    id_MUser                        INT,
    id_Manager                      INT,
    id_MRequest                     INT,
    id_SRO                          INT,
    CaseNumber                      VARCHAR(50),
    DebtorName                      VARCHAR(250),
    DebtorINN                       VARCHAR(12),
    DebtorSNILS                     VARCHAR(13),
    DebtorOGRN                      VARBINARY(16),
    DebtorCategory                  CHAR(1)          DEFAULT 'n' NOT NULL,
    DebtorAddress                   VARCHAR(250),
    DebtorName2                     VARCHAR(250),
    DebtorINN2                      VARCHAR(12),
    DebtorSNILS2                    VARCHAR(13),
    DebtorOGRN2                     VARBINARY(16),
    DebtorCategory2                 CHAR(1),
    DebtorAddress2                  VARCHAR(250),
    DateOfApplication               DATETIME,
    ApplicantName                   VARCHAR(250),
    ApplicantINN                    VARCHAR(12),
    ApplicantSNILS                  VARCHAR(13),
    ApplicantOGRN                   VARBINARY(16),
    ApplicantAddress                VARCHAR(250),
    ApplicantCategory               CHAR(1),
    TimeOfCreate                    DATETIME        NOT NULL,
    NextSessionDate                 DATETIME,
    TimeOfLastChecking              DATETIME,
    ReadyToRegistrate               BIT              DEFAULT 0 NOT NULL,
    ShowToSRO                       BIT              DEFAULT 0 NOT NULL,
    AddedBySRO                      BIT              DEFAULT 0 NOT NULL,
    DateOfRequestAct                DATETIME,
    CourtDecisionURL                VARCHAR(250),
    CourtDecisionAddInfo            VARCHAR(250),
    DateOfAcceptance                DATETIME,
    DateOfPrescription              DATETIME,
    PrescriptionURL                 VARCHAR(250),
    PrescriptionAddInfo             VARCHAR(250),
    efrsbPrescriptionID             VARCHAR(32),
    efrsbPrescriptionNumber         VARCHAR(30),
    efrsbPrescriptionPublishDate    DATETIME,
    efrsbPrescriptionAddInfo        VARCHAR(250),
    TimeOfLastCheckingEFRSB         DATETIME,
    PRIMARY KEY (id_ProcedureStart)
)ENGINE=INNODB
;



CREATE TABLE ProcedureUsing(
    INN               VARCHAR(12),
    OGRN              VARBINARY(16),
    SNILS             VARCHAR(13),
    ContractNumber    VARCHAR(40),
    LicenseToken      TEXT,
    UsingTime         DATETIME       NOT NULL,
    IP                VARCHAR(40)    NOT NULL,
    Section           CHAR(4)
)ENGINE=INNODB
;



CREATE TABLE ProcedureUsing_indexed(
    INN               VARCHAR(12),
    OGRN              VARBINARY(16),
    SNILS             VARCHAR(13),
    ContractNumber    VARCHAR(10),
    UsingTime         DATETIME,
    IP                VARCHAR(40),
    Section           CHAR(4)
)ENGINE=INNODB
;



CREATE TABLE Push_receiver(
    id_Push_receiver             INT             AUTO_INCREMENT,
    For_Meeting                  CHAR(1),
    For_Committee                CHAR(1),
    For_MeetingPB                CHAR(1),
    For_MeetingWorker            CHAR(1),
    For_Auction                  CHAR(1),
    For_CourtDecision            CHAR(1),
    For_Message_after            DATETIME,
    Time_start                   INT,
    Time_finish                  INT,
    Check_day_off                BIT              DEFAULT 0 NOT NULL,
    Transport                    CHAR(1),
    Destination                  VARCHAR(200),
    Timezone                     INT,
    Category                     CHAR(1)         NOT NULL,
    id_Manager_Contract_MUser    INT             NOT NULL,
    ReceiverGUID                 VARCHAR(32),
    PRIMARY KEY (id_Push_receiver)
)ENGINE=INNODB
;



CREATE TABLE Push_request(
    id_Push_request     INT            AUTO_INCREMENT,
    MessageNumber       VARCHAR(30)    NOT NULL,
    id_Push_receiver    INT            NOT NULL,
    DaysBefore          SMALLINT       NOT NULL,
    PRIMARY KEY (id_Push_request)
)ENGINE=INNODB
;



CREATE TABLE Pushed_event(
    id_Event            INT         NOT NULL,
    id_Push_receiver    INT         NOT NULL,
    Time_dispatched     DATETIME    NOT NULL,
    Time_pushed         DATETIME,
    ErrorText           TEXT,
    SentText            TEXT,
    ResponseText        TEXT,
    PRIMARY KEY (id_Event, id_Push_receiver)
)ENGINE=INNODB
;



CREATE TABLE Pushed_message(
    id_Message          INT         NOT NULL,
    id_Push_receiver    INT         NOT NULL,
    Time_dispatched     DATETIME    NOT NULL,
    Time_pushed         DATETIME,
    ErrorText           TEXT,
    SentText            TEXT,
    ResponseText        TEXT,
    PRIMARY KEY (id_Message, id_Push_receiver)
)ENGINE=INNODB
;



CREATE TABLE region(
    id_Region    INT             AUTO_INCREMENT,
    Name         VARCHAR(250)    NOT NULL,
    OKATO        CHAR(6),
    PRIMARY KEY (id_Region)
)ENGINE=INNODB
;



CREATE TABLE Request(
    id_Request            INT             AUTO_INCREMENT,
    id_MUser              INT             NOT NULL,
    id_Debtor             INT             NOT NULL,
    managerInn            VARCHAR(12),
    managerName           VARCHAR(100)    NOT NULL,
    managerEfrsbNumber    VARCHAR(50),
    Body                  LONGBLOB        NOT NULL,
    State                 CHAR(1)          DEFAULT 'a' NOT NULL,
    TimeLastChange        DATETIME        NOT NULL,
    PRIMARY KEY (id_Request)
)ENGINE=INNODB
;



CREATE TABLE Rosreestr(
    id_Rosreestr    INT            AUTO_INCREMENT,
    id_Region       INT            NOT NULL,
    Address         TEXT           NOT NULL,
    Latitude        FLOAT,
    Longitude       FLOAT,
    Name            VARCHAR(70)    NOT NULL,
    PRIMARY KEY (id_Rosreestr)
)ENGINE=INNODB
;



CREATE TABLE SentEmail(
    id_SentEmail      INT             AUTO_INCREMENT,
    RecipientEmail    VARCHAR(100)    NOT NULL,
    RecipientId       INT             NOT NULL,
    RecipientType     CHAR(1)         NOT NULL,
    EmailType         CHAR(1)         NOT NULL,
    TimeSent          DATETIME,
    TimeDispatch      DATETIME        NOT NULL,
    Message           LONGBLOB,
    SenderServer      VARCHAR(50),
    SenderUser        VARCHAR(50),
    ExtraParams       LONGBLOB,
    PRIMARY KEY (id_SentEmail)
)ENGINE=INNODB
;



CREATE TABLE Session(
    id_Session      INT             AUTO_INCREMENT,
    Token           VARCHAR(100),
    Who             CHAR(1)         NOT NULL,
    id_Owner        INT             NOT NULL,
    TimeStarted     TIMESTAMP       NOT NULL,
    TimeLastUsed    DATETIME        NOT NULL,
    PRIMARY KEY (id_Session)
)ENGINE=INNODB
;



CREATE TABLE SRODocument(
    id_SRODocument    INT             AUTO_INCREMENT,
    id_SRO            INT             NOT NULL,
    Body              LONGBLOB        NOT NULL,
    DocumentType      CHAR(1)         NOT NULL,
    FileName          VARCHAR(100)    NOT NULL,
    PubLock           LONGBLOB,
    PRIMARY KEY (id_SRODocument)
)ENGINE=INNODB
;



CREATE TABLE sub_level(
    id_Sub_level       INT               AUTO_INCREMENT,
    id_Region          INT               NOT NULL,
    id_Contract        INT,
    StartDate          DATE              NOT NULL,
    Sum_Common         DECIMAL(10, 2),
    Sum_Employable     DECIMAL(10, 2),
    Sum_Infant         DECIMAL(10, 2),
    Sum_Pensioner      DECIMAL(10, 2),
    Reglament_Title    VARCHAR(250),
    Reglament_Date     DATE,
    Reglament_Url      TEXT,
    PRIMARY KEY (id_Sub_level)
)ENGINE=INNODB
;



CREATE TABLE tbl_migration(
    MigrationNumber    VARCHAR(250)    NOT NULL,
    MigrationName      VARCHAR(250)    NOT NULL,
    MigrationTime      TIMESTAMP       NOT NULL,
    PRIMARY KEY (MigrationNumber)
)ENGINE=INNODB
;



CREATE TABLE Vote(
    id_Vote                   INT            AUTO_INCREMENT,
    id_Meeting                INT            NOT NULL,
    CabinetKey                VARCHAR(20)    NOT NULL,
    CabinetTime               DATETIME       NOT NULL,
    Confirmation_code         VARCHAR(10),
    Confirmation_code_time    DATETIME,
    Answers                   LONGBLOB,
    Member                    LONGBLOB,
    PRIMARY KEY (id_Vote)
)ENGINE=INNODB
;



CREATE TABLE Vote_document(
    id_Vote_document      INT            AUTO_INCREMENT,
    id_Vote               INT            NOT NULL,
    Vote_document_time    DATETIME       NOT NULL,
    Vote_document_type    CHAR(1)        NOT NULL,
    FileName              VARCHAR(40)    NOT NULL,
    Body                  LONGBLOB,
    Parameters            LONGBLOB,
    PRIMARY KEY (id_Vote_document)
)ENGINE=INNODB
;



CREATE TABLE Vote_log(
    id_Vote_log      INT         AUTO_INCREMENT,
    id_Vote          INT         NOT NULL,
    Vote_log_type    CHAR(1)     NOT NULL,
    Vote_log_time    DATETIME    NOT NULL,
    body             LONGBLOB,
    PRIMARY KEY (id_Vote_log)
)ENGINE=INNODB
;



CREATE TABLE wcalendar(
    id_Wcalendar    INT         AUTO_INCREMENT,
    id_Region       INT,
    Revision        INT         NOT NULL,
    Year            INT         NOT NULL,
    Days            LONGBLOB,
    PRIMARY KEY (id_Wcalendar)
)ENGINE=INNODB
;



CREATE INDEX byTime ON access_log(Time)
;
CREATE INDEX byId ON access_log(Id)
;
CREATE INDEX byIP ON access_log(IP)
;
CREATE INDEX access_log_type ON access_log(id_Log_type)
;
CREATE INDEX byLastName ON AnketaNP(lastName)
;
CREATE INDEX bySNILS ON AnketaNP(SNILS)
;
CREATE INDEX byINN ON AnketaNP(INN)
;
CREATE INDEX refAnketaNP_MData ON AnketaNP(id_MData)
;
CREATE INDEX refApplication_MUser ON Application(id_MUser)
;
CREATE INDEX refApplicationFile_Application ON ApplicationFile(id_Application)
;
CREATE INDEX Ref3999 ON ApplicationRTK(id_MData)
;
CREATE UNIQUE INDEX byProcedure_ID_Object ON Asset(id_MProcedure, ID_Object)
;
CREATE INDEX byRevision ON Asset(Revision)
;
CREATE INDEX refAsset_AssetSubGroup ON Asset(id_AssetSubGroup)
;
CREATE INDEX refAsset_AssetGroup ON Asset(id_AssetGroup)
;
CREATE INDEX refAsset_BalanceItem ON Asset(id_BalanceItem)
;
CREATE INDEX refAsset_MProcedure ON Asset(id_MProcedure)
;
CREATE INDEX refAsset_Region ON Asset(id_Region)
;
CREATE INDEX refAsset_EfrsbAssetClass_Asset ON Asset_EfrsbAssetClass(id_Asset)
;
CREATE INDEX refAsset_EfrsbAssetClass_EfrsbAssetClass ON Asset_EfrsbAssetClass(id_EfrsbAssetClass)
;
CREATE INDEX refAsset_MUser_Asset ON Asset_MUser(id_Asset)
;
CREATE INDEX refAsset_MUser_MUser ON Asset_MUser(id_MUser)
;
CREATE INDEX refAsset_OKPD2_OKPD2 ON Asset_OKPD2(id_OKPD2)
;
CREATE INDEX refAsset_OKPD2_Asset ON Asset_OKPD2(id_Asset)
;
CREATE INDEX Asset_proc_MProcedure ON Asset_proc_for_MUser(id_MProcedure)
;
CREATE INDEX Access_proc_MUser ON Asset_proc_for_MUser(id_MUser)
;
CREATE INDEX byRevision ON AssetAttachment(Revision)
;
CREATE INDEX refAssetAttachment_Asset ON AssetAttachment(id_Asset)
;
CREATE INDEX refAssetSubGroup_AssetGroup ON AssetSubGroup(id_AssetGroup)
;
CREATE INDEX refAttachment_EmailToSend ON Attachment(id_SentEmail)
;
CREATE UNIQUE INDEX byINN ON AwardNominee(inn)
;
CREATE UNIQUE INDEX byINN ON AwardVote(inn)
;
CREATE INDEX refAwardVote_AwardNominee ON AwardVote(id_AwardNominee)
;
CREATE INDEX refBFile_BObject ON BFile(id_BObject)
;
CREATE INDEX refBFile_BLot ON BFile(id_BLot)
;
CREATE INDEX refBFile_Bidding ON BFile(id_Bidding)
;
CREATE INDEX refBidding_MProcedure ON Bidding(id_MProcedure)
;
CREATE INDEX refBLot_Bidding ON BLot(id_Bidding)
;
CREATE INDEX refBObject_BLot ON BObject(id_BLot)
;
CREATE INDEX CCDocument_CourtCase ON CCDocument(id_CourtCase)
;
CREATE INDEX RefCCEvent_Document ON CCEvent(id_CCDocument)
;
CREATE INDEX RefCCEvent_PushedEvent ON CCPushedEvent(id_CCEvent)
;
CREATE UNIQUE INDEX byContractNumber ON Contract(ContractNumber)
;
CREATE INDEX Ref_ContractUser_Contract ON ContractUser(id_Contract)
;
CREATE INDEX Ref_ContractUser_User ON ContractUser(id_MUser)
;
CREATE UNIQUE INDEX byName ON Court(Name)
;
CREATE UNIQUE INDEX byCasebookTag ON Court(CasebookTag)
;
CREATE UNIQUE INDEX byShortName ON Court(ShortName)
;
CREATE UNIQUE INDEX bySearchName ON Court(SearchName)
;
CREATE INDEX byNumberPrefix ON Court(NumberPrefix)
;
CREATE INDEX refCourt_Region ON Court(id_Region)
;
CREATE INDEX RefCourtCase_Debtor ON CourtCase(id_Debtor)
;
CREATE INDEX RefCourtCase_Participant ON CourtCaseParticipant(id_CourtCase)
;
CREATE INDEX RefMUser_CourtCaseParticipant ON CourtCaseParticipant(id_MUser)
;
CREATE INDEX byINN ON Debtor(INN)
;
CREATE INDEX byOGRN ON Debtor(OGRN)
;
CREATE INDEX bySNILS ON Debtor(SNILS)
;
CREATE UNIQUE INDEX byBankruptId ON Debtor(BankruptId)
;
CREATE INDEX for_verification ON Debtor(VerificationState, TimeVerified)
;
CREATE UNIQUE INDEX byName ON DocType(Name)
;
CREATE UNIQUE INDEX byBankruptId_1 ON efrsb_debtor(BankruptId)
;
CREATE INDEX byName_1 ON efrsb_debtor(Name)
;
CREATE INDEX byINN_2 ON efrsb_debtor(INN)
;
CREATE INDEX bySNILS_1 ON efrsb_debtor(SNILS)
;
CREATE INDEX byOGRN_1 ON efrsb_debtor(OGRN)
;
CREATE INDEX byRevision_1 ON efrsb_debtor(Revision)
;
CREATE INDEX Ref_Debtor_Manager ON efrsb_debtor(ArbitrManagerID)
;
CREATE INDEX Ref_Debtor_manager_Debtor ON efrsb_debtor_manager(BankruptId)
;
CREATE INDEX Ref_Debtor_manager_Manager ON efrsb_debtor_manager(ArbitrManagerID)
;
CREATE UNIQUE INDEX byArbitrManagerID_1 ON efrsb_manager(ArbitrManagerID)
;
CREATE INDEX byLastName_1 ON efrsb_manager(LastName)
;
CREATE INDEX byFirstName ON efrsb_manager(FirstName)
;
CREATE INDEX byMiddleName ON efrsb_manager(MiddleName)
;
CREATE INDEX byINN_1 ON efrsb_manager(INN)
;
CREATE INDEX byRevision_2 ON efrsb_manager(Revision)
;
CREATE INDEX refManager_SRO ON efrsb_manager(SRORegNum)
;
CREATE UNIQUE INDEX byRegNum_1 ON efrsb_sro(RegNum)
;
CREATE INDEX refEfrsbAssetClass_EfrsbAssetClass ON EfrsbAssetClass(id_EfrsbAssetClass_Parent)
;
CREATE INDEX refEmailToSend_SentEmail ON EmailToSend(id_SentEmail)
;
CREATE UNIQUE INDEX byNumber ON event(MessageNumber)
;
CREATE UNIQUE INDEX byRevision ON event(Revision)
;
CREATE UNIQUE INDEX by_efrsb_id ON event(efrsb_id)
;
CREATE INDEX RefEvent_Manager ON event(ArbitrManagerID)
;
CREATE INDEX RefEvent_Message ON event(efrsb_id)
;
CREATE INDEX RefEvent_Debtor ON event(BankruptId)
;
CREATE INDEX RefGCEvent_Calendar ON GCEvent(id_GoogleCalendar)
;
CREATE INDEX RefGCEvent_MProcedure ON GCEvent(id_MProcedure)
;
CREATE INDEX RefGCPushedEvent_Event ON GCPushedEvent(id_GCEvent)
;
CREATE INDEX RefGoogleCalendar_Manager ON GoogleCalendar(id_Manager)
;
CREATE UNIQUE INDEX bySiteName ON Job(id_JobSite, Name)
;
CREATE INDEX Ref_Job_Site ON Job(id_JobSite)
;
CREATE INDEX byJobStarted ON JobLog(id_Job, Started)
;
CREATE INDEX Ref_Log_Job ON JobLog(id_Job)
;
CREATE UNIQUE INDEX byJobName ON JobPart(id_Job, Name)
;
CREATE INDEX Ref_Part_Job ON JobPart(id_Job)
;
CREATE INDEX Ref_JobPart_Job ON JobPartLog(id_JobLog)
;
CREATE INDEX Ref_Log_Part ON JobPartLog(id_JobPart)
;
CREATE UNIQUE INDEX byName ON JobSite(Name)
;
CREATE INDEX refLeak_PData ON Leak(id_PData)
;
CREATE UNIQUE INDEX byName ON log_type(Name)
;
CREATE UNIQUE INDEX byEfrsbNumber ON Manager(efrsbNumber)
;
CREATE UNIQUE INDEX byEMailPassword ON Manager(ManagerEmail, Password)
;
CREATE UNIQUE INDEX byEMail ON Manager(ManagerEmail)
;
CREATE UNIQUE INDEX byArbitrManagerID ON Manager(ArbitrManagerID)
;
CREATE UNIQUE INDEX byINN ON Manager(INN)
;
CREATE UNIQUE INDEX byManagerEmail_Change ON Manager(ManagerEmail_Change)
;
CREATE INDEX refManager_Contract ON Manager(id_Contract)
;
CREATE INDEX Ref_Manager_SRO ON Manager(id_SRO)
;
CREATE INDEX Ref_AuthByCert_Manager ON ManagerCertAuth(id_Manager)
;
CREATE INDEX RefManager_Document ON ManagerDocument(id_Manager)
;
CREATE UNIQUE INDEX byMUserManager ON ManagerUser(id_MUser, id_Manager)
;
CREATE INDEX RefManagerUser_Manager ON ManagerUser(id_Manager)
;
CREATE INDEX RefManagerUser_MUser ON ManagerUser(id_MUser)
;
CREATE INDEX refMData_Debtor ON MData(id_Debtor)
;
CREATE INDEX refMData_ManagerUser ON MData(id_ManagerUser)
;
CREATE INDEX refAttachment_MData ON MData_attachment(id_MData)
;
CREATE INDEX refMeeting_MProcedure ON Meeting(id_MProcedure)
;
CREATE INDEX refMeeting_document_Meeting ON Meeting_document(id_Meeting)
;
CREATE UNIQUE INDEX by_efrsb_id_1 ON Message(efrsb_id)
;
CREATE UNIQUE INDEX byRevision ON Message(Revision)
;
CREATE INDEX refMessage_Debtor ON Message(BankruptId)
;
CREATE UNIQUE INDEX byBankruptId ON mock_efrsb_debtor(BankruptId)
;
CREATE INDEX byArbitrManagerID ON mock_efrsb_debtor(ArbitrManagerID)
;
CREATE INDEX debtor_manager_byArbitrManagerID ON mock_efrsb_debtor_manager(ArbitrManagerID)
;
CREATE INDEX byBankruptId ON mock_efrsb_debtor_manager(BankruptId)
;
CREATE UNIQUE INDEX byAddress ON mock_efrsb_email(address)
;
CREATE INDEX RefEmail_ManagerManager ON mock_efrsb_email_manager(id_Manager)
;
CREATE INDEX RefEmail_ManagerEmail ON mock_efrsb_email_manager(id_Email)
;
CREATE INDEX RefEmail_Manager_MessageLast ON mock_efrsb_email_manager(id_Message_Last)
;
CREATE INDEX byEventTime ON mock_efrsb_event(EventTime)
;
CREATE INDEX Ref2539 ON mock_efrsb_event(ArbitrManagerID)
;
CREATE INDEX refEfrsb_event_Debtor ON mock_efrsb_event(BankruptId)
;
CREATE INDEX refByEfrsb_id ON mock_efrsb_event(efrsb_id)
;
CREATE UNIQUE INDEX byArbitrManagerID ON mock_efrsb_manager(ArbitrManagerID)
;
CREATE INDEX refManager_SRO ON mock_efrsb_manager(SRORegNum)
;
CREATE UNIQUE INDEX by_efrsb_id ON mock_efrsb_message(efrsb_id)
;
CREATE INDEX refByArbitrManagerID ON mock_efrsb_message(ArbitrManagerID)
;
CREATE INDEX refByBankruptId ON mock_efrsb_message(BankruptId)
;
CREATE UNIQUE INDEX byRegNum ON mock_efrsb_sro(RegNum)
;
CREATE UNIQUE INDEX NaturalKey ON MProcedure(casenumber, procedure_type, id_Manager, id_Debtor)
;
CREATE INDEX byCtbRevision ON MProcedure(ctb_revision)
;
CREATE INDEX by_id_Message1 ON MProcedure(id_Debtor_Manager)
;
CREATE INDEX by_ctb_bankrotech_id ON MProcedure(ctb_bankrotech_id)
;
CREATE INDEX RefMProcedure_Manager ON MProcedure(id_Manager)
;
CREATE INDEX Debtor_MProcedure ON MProcedure(id_Debtor)
;
CREATE INDEX RefMProcedureUser_ManagerUser ON MProcedureUser(id_ManagerUser)
;
CREATE INDEX RefMProcedureUser_MProcedure ON MProcedureUser(id_MProcedure)
;
CREATE INDEX refMProcedureUser_Request ON MProcedureUser(id_Request)
;
CREATE INDEX Ref_Request_Court ON MRequest(id_Court)
;
CREATE INDEX Ref_Request_SRO ON MRequest(id_SRO)
;
CREATE INDEX Ref_MRequest_Manager ON MRequest(id_Manager)
;
CREATE UNIQUE INDEX byUserEmail ON MUser(UserEmail)
;
CREATE INDEX refOKPD2_OKPD2 ON OKPD2(id_OKPD2_Parent)
;
CREATE UNIQUE INDEX by_id_MProcedure_revision ON PData(id_MProcedure, revision)
;
CREATE INDEX byProcessedRevision ON PData(Processed, revision)
;
CREATE INDEX RefPData_MProcedure ON PData(id_MProcedure)
;
CREATE INDEX RefPDDocFile_PDDocument ON PDDocFile(id_PDDocument)
;
CREATE INDEX RefPDDocument_PreDebtor ON PDDocument(id_PreDebtor)
;
CREATE INDEX RefDocType_PDDocument ON PDDocument(id_DocType)
;
CREATE INDEX RefPreDebtor_MUser ON PreDebtor(id_MUser)
;
CREATE INDEX RefPreDebtor_Manager ON PreDebtor(id_Manager)
;
CREATE INDEX RefPreDebtor_Court ON PreDebtor(id_Court)
;
CREATE INDEX RefPreDebtor_region ON PreDebtor(id_Region)
;
CREATE INDEX Ref_ProcedureStart_Court ON ProcedureStart(id_Court)
;
CREATE INDEX Ref_ProcedureStart_Contract ON ProcedureStart(id_Contract)
;
CREATE INDEX Ref_ProcedureStart_User ON ProcedureStart(id_MUser)
;
CREATE INDEX Ref_ProcedureStart_Manager ON ProcedureStart(id_Manager)
;
CREATE INDEX Ref_Start_Request ON ProcedureStart(id_MRequest)
;
CREATE INDEX Ref_Start_SRO ON ProcedureStart(id_SRO)
;
CREATE INDEX bySection ON ProcedureUsing_indexed(Section)
;
CREATE INDEX byUsingTime ON ProcedureUsing_indexed(UsingTime)
;
CREATE INDEX byContractNumber ON ProcedureUsing_indexed(ContractNumber)
;
CREATE INDEX byIP ON ProcedureUsing_indexed(IP)
;
CREATE UNIQUE INDEX byCategoryId ON Push_receiver(Category, id_Manager_Contract_MUser)
;
CREATE INDEX RefPush_request_Event ON Push_request(MessageNumber)
;
CREATE INDEX RefPush_request_Push_receiver ON Push_request(id_Push_receiver)
;
CREATE INDEX RefPushed_event_Event ON Pushed_event(id_Event)
;
CREATE INDEX RefPushed_event_Push_receiver ON Pushed_event(id_Push_receiver)
;
CREATE INDEX RefPushed_message_Message ON Pushed_message(id_Message)
;
CREATE INDEX RefPushed_message_receiver ON Pushed_message(id_Push_receiver)
;
CREATE UNIQUE INDEX byRegionName ON region(Name)
;
CREATE UNIQUE INDEX byOKATO ON region(OKATO)
;
CREATE INDEX refRequest_MUser ON Request(id_MUser)
;
CREATE INDEX Debtor_Request ON Request(id_Debtor)
;
CREATE INDEX refRosreestr_Region ON Rosreestr(id_Region)
;
CREATE INDEX byRecipient ON SentEmail(RecipientId, RecipientType)
;
CREATE UNIQUE INDEX byToken ON Session(Token)
;
CREATE UNIQUE INDEX byWhoOwner ON Session(Who, id_Owner)
;
CREATE INDEX refSRO_Document ON SRODocument(id_SRO)
;
CREATE INDEX Ref_Sub_level_Region ON sub_level(id_Region)
;
CREATE INDEX RefSubLevel_Contract ON sub_level(id_Contract)
;
CREATE INDEX byCabinetKey ON Vote(CabinetKey)
;
CREATE INDEX refVote_Meeting ON Vote(id_Meeting)
;
CREATE INDEX refVote_document_Vote ON Vote_document(id_Vote)
;
CREATE INDEX refVote_log_Vote ON Vote_log(id_Vote)
;
CREATE UNIQUE INDEX by_id_Region_Year ON wcalendar(id_Region, Year)
;
CREATE UNIQUE INDEX byRevision ON wcalendar(Revision)
;
CREATE INDEX refWcalendar_Region ON wcalendar(id_Region)
;
ALTER TABLE access_log ADD CONSTRAINT access_log_type 
    FOREIGN KEY (id_Log_type)
    REFERENCES log_type(id_Log_type)
;


ALTER TABLE AnketaNP ADD CONSTRAINT refAnketaNP_MData 
    FOREIGN KEY (id_MData)
    REFERENCES MData(id_MData) ON DELETE CASCADE
;


ALTER TABLE Application ADD CONSTRAINT refApplication_MUser 
    FOREIGN KEY (id_MUser)
    REFERENCES MUser(id_MUser)
;


ALTER TABLE ApplicationFile ADD CONSTRAINT refApplicationFile_Application 
    FOREIGN KEY (id_Application)
    REFERENCES Application(id_Application)
;


ALTER TABLE ApplicationRTK ADD CONSTRAINT RefMData99 
    FOREIGN KEY (id_MData)
    REFERENCES MData(id_MData)
;


ALTER TABLE Asset ADD CONSTRAINT refAsset_AssetGroup 
    FOREIGN KEY (id_AssetGroup)
    REFERENCES AssetGroup(id_AssetGroup)
;

ALTER TABLE Asset ADD CONSTRAINT refAsset_AssetSubGroup 
    FOREIGN KEY (id_AssetSubGroup)
    REFERENCES AssetSubGroup(id_AssetSubGroup)
;

ALTER TABLE Asset ADD CONSTRAINT refAsset_BalanceItem 
    FOREIGN KEY (id_BalanceItem)
    REFERENCES BalanceItem(id_BalanceItem)
;

ALTER TABLE Asset ADD CONSTRAINT refAsset_MProcedure 
    FOREIGN KEY (id_MProcedure)
    REFERENCES MProcedure(id_MProcedure)
;

ALTER TABLE Asset ADD CONSTRAINT refAsset_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;


ALTER TABLE Asset_EfrsbAssetClass ADD CONSTRAINT refAsset_EfrsbAssetClass_Asset 
    FOREIGN KEY (id_Asset)
    REFERENCES Asset(id_Asset) ON DELETE CASCADE
;

ALTER TABLE Asset_EfrsbAssetClass ADD CONSTRAINT refAsset_EfrsbAssetClass_EfrsbAssetClass 
    FOREIGN KEY (id_EfrsbAssetClass)
    REFERENCES EfrsbAssetClass(id_EfrsbAssetClass)
;


ALTER TABLE Asset_MUser ADD CONSTRAINT refAsset_MUser_Asset 
    FOREIGN KEY (id_Asset)
    REFERENCES Asset(id_Asset) ON DELETE CASCADE
;

ALTER TABLE Asset_MUser ADD CONSTRAINT refAsset_MUser_MUser 
    FOREIGN KEY (id_MUser)
    REFERENCES MUser(id_MUser)
;


ALTER TABLE Asset_OKPD2 ADD CONSTRAINT refAsset_OKPD2_Asset 
    FOREIGN KEY (id_Asset)
    REFERENCES Asset(id_Asset) ON DELETE CASCADE
;

ALTER TABLE Asset_OKPD2 ADD CONSTRAINT refAsset_OKPD2_OKPD2 
    FOREIGN KEY (id_OKPD2)
    REFERENCES OKPD2(id_OKPD2)
;


ALTER TABLE Asset_proc_for_MUser ADD CONSTRAINT Access_proc_MUser 
    FOREIGN KEY (id_MUser)
    REFERENCES MUser(id_MUser)
;

ALTER TABLE Asset_proc_for_MUser ADD CONSTRAINT Asset_proc_MProcedure 
    FOREIGN KEY (id_MProcedure)
    REFERENCES MProcedure(id_MProcedure)
;


ALTER TABLE AssetAttachment ADD CONSTRAINT refAssetAttachment_Asset 
    FOREIGN KEY (id_Asset)
    REFERENCES Asset(id_Asset) ON DELETE CASCADE
;


ALTER TABLE AssetSubGroup ADD CONSTRAINT refAssetSubGroup_AssetGroup 
    FOREIGN KEY (id_AssetGroup)
    REFERENCES AssetGroup(id_AssetGroup)
;


ALTER TABLE Attachment ADD CONSTRAINT refAttachment_EmailToSend 
    FOREIGN KEY (id_SentEmail)
    REFERENCES EmailToSend(id_SentEmail) ON DELETE CASCADE
;


ALTER TABLE AwardVote ADD CONSTRAINT refAwardVote_AwardNominee 
    FOREIGN KEY (id_AwardNominee)
    REFERENCES AwardNominee(id_AwardNominee)
;


ALTER TABLE BFile ADD CONSTRAINT refBFile_Bidding 
    FOREIGN KEY (id_Bidding)
    REFERENCES Bidding(id_Bidding)
;

ALTER TABLE BFile ADD CONSTRAINT refBFile_BLot 
    FOREIGN KEY (id_BLot)
    REFERENCES BLot(id_BLot)
;

ALTER TABLE BFile ADD CONSTRAINT refBFile_BObject 
    FOREIGN KEY (id_BObject)
    REFERENCES BObject(id_BObject)
;


ALTER TABLE Bidding ADD CONSTRAINT refBidding_MProcedure 
    FOREIGN KEY (id_MProcedure)
    REFERENCES MProcedure(id_MProcedure)
;


ALTER TABLE BLot ADD CONSTRAINT refBLot_Bidding 
    FOREIGN KEY (id_Bidding)
    REFERENCES Bidding(id_Bidding)
;


ALTER TABLE BObject ADD CONSTRAINT refBObject_BLot 
    FOREIGN KEY (id_BLot)
    REFERENCES BLot(id_BLot)
;


ALTER TABLE CCDocument ADD CONSTRAINT CCDocument_CourtCase 
    FOREIGN KEY (id_CourtCase)
    REFERENCES CourtCase(id_CourtCase)
;


ALTER TABLE CCEvent ADD CONSTRAINT RefCCEvent_Document 
    FOREIGN KEY (id_CCDocument)
    REFERENCES CCDocument(id_CCDocument)
;


ALTER TABLE CCPushedEvent ADD CONSTRAINT RefCCEvent_PushedEvent 
    FOREIGN KEY (id_CCEvent)
    REFERENCES CCEvent(id_CCEvent)
;


ALTER TABLE ContractUser ADD CONSTRAINT Ref_ContractUser_Contract 
    FOREIGN KEY (id_Contract)
    REFERENCES Contract(id_Contract)
;

ALTER TABLE ContractUser ADD CONSTRAINT Ref_ContractUser_User 
    FOREIGN KEY (id_MUser)
    REFERENCES MUser(id_MUser)
;


ALTER TABLE Court ADD CONSTRAINT refCourt_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;


ALTER TABLE CourtCase ADD CONSTRAINT RefCourtCase_Debtor 
    FOREIGN KEY (id_Debtor)
    REFERENCES Debtor(id_Debtor)
;


ALTER TABLE CourtCaseParticipant ADD CONSTRAINT RefCourtCase_Participant 
    FOREIGN KEY (id_CourtCase)
    REFERENCES CourtCase(id_CourtCase)
;

ALTER TABLE CourtCaseParticipant ADD CONSTRAINT RefMUser_CourtCaseParticipant 
    FOREIGN KEY (id_MUser)
    REFERENCES MUser(id_MUser)
;


ALTER TABLE EfrsbAssetClass ADD CONSTRAINT refEfrsbAssetClass_EfrsbAssetClass 
    FOREIGN KEY (id_EfrsbAssetClass_Parent)
    REFERENCES EfrsbAssetClass(id_EfrsbAssetClass)
;


ALTER TABLE EmailToSend ADD CONSTRAINT refEmailToSend_SentEmail 
    FOREIGN KEY (id_SentEmail)
    REFERENCES SentEmail(id_SentEmail)
;


ALTER TABLE GCEvent ADD CONSTRAINT RefGCEvent_Calendar 
    FOREIGN KEY (id_GoogleCalendar)
    REFERENCES GoogleCalendar(id_GoogleCalendar)
;

ALTER TABLE GCEvent ADD CONSTRAINT RefGCEvent_MProcedure 
    FOREIGN KEY (id_MProcedure)
    REFERENCES MProcedure(id_MProcedure)
;


ALTER TABLE GCPushedEvent ADD CONSTRAINT RefGCPushedEvent_Event 
    FOREIGN KEY (id_GCEvent)
    REFERENCES GCEvent(id_GCEvent)
;


ALTER TABLE GoogleCalendar ADD CONSTRAINT RefGoogleCalendar_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES Manager(id_Manager)
;


ALTER TABLE Job ADD CONSTRAINT Ref_Job_Site 
    FOREIGN KEY (id_JobSite)
    REFERENCES JobSite(id_JobSite) ON DELETE CASCADE
;


ALTER TABLE JobLog ADD CONSTRAINT Ref_Log_Job 
    FOREIGN KEY (id_Job)
    REFERENCES Job(id_Job) ON DELETE CASCADE
;


ALTER TABLE JobPart ADD CONSTRAINT Ref_Part_Job 
    FOREIGN KEY (id_Job)
    REFERENCES Job(id_Job) ON DELETE CASCADE
;


ALTER TABLE JobPartLog ADD CONSTRAINT Ref_JobPart_Job 
    FOREIGN KEY (id_JobLog)
    REFERENCES JobLog(id_JobLog) ON DELETE CASCADE
;

ALTER TABLE JobPartLog ADD CONSTRAINT Ref_Log_Part 
    FOREIGN KEY (id_JobPart)
    REFERENCES JobPart(id_JobPart) ON DELETE CASCADE
;


ALTER TABLE Leak ADD CONSTRAINT refLeak_PData 
    FOREIGN KEY (id_PData)
    REFERENCES PData(id_PData)
;


ALTER TABLE Manager ADD CONSTRAINT Ref_Manager_SRO 
    FOREIGN KEY (id_SRO)
    REFERENCES efrsb_sro(id_SRO)
;

ALTER TABLE Manager ADD CONSTRAINT refManager_Contract 
    FOREIGN KEY (id_Contract)
    REFERENCES Contract(id_Contract)
;


ALTER TABLE ManagerCertAuth ADD CONSTRAINT Ref_AuthByCert_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES Manager(id_Manager)
;


ALTER TABLE ManagerDocument ADD CONSTRAINT RefManager_Document 
    FOREIGN KEY (id_Manager)
    REFERENCES Manager(id_Manager)
;


ALTER TABLE ManagerUser ADD CONSTRAINT RefManagerUser_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES Manager(id_Manager)
;

ALTER TABLE ManagerUser ADD CONSTRAINT RefManagerUser_MUser 
    FOREIGN KEY (id_MUser)
    REFERENCES MUser(id_MUser)
;


ALTER TABLE MData ADD CONSTRAINT refMData_Debtor 
    FOREIGN KEY (id_Debtor)
    REFERENCES Debtor(id_Debtor)
;

ALTER TABLE MData ADD CONSTRAINT refMData_ManagerUser 
    FOREIGN KEY (id_ManagerUser)
    REFERENCES ManagerUser(id_ManagerUser)
;


ALTER TABLE MData_attachment ADD CONSTRAINT refAttachment_MData 
    FOREIGN KEY (id_MData)
    REFERENCES MData(id_MData)
;


ALTER TABLE Meeting ADD CONSTRAINT refMeeting_MProcedure 
    FOREIGN KEY (id_MProcedure)
    REFERENCES MProcedure(id_MProcedure)
;


ALTER TABLE Meeting_document ADD CONSTRAINT refMeeting_document_Meeting 
    FOREIGN KEY (id_Meeting)
    REFERENCES Meeting(id_Meeting)
;


ALTER TABLE mock_efrsb_email_manager ADD CONSTRAINT RefEmail_Manager_MessageLast 
    FOREIGN KEY (id_Message_Last)
    REFERENCES mock_efrsb_message(id_Message)
;

ALTER TABLE mock_efrsb_email_manager ADD CONSTRAINT RefEmail_ManagerEmail 
    FOREIGN KEY (id_Email)
    REFERENCES mock_efrsb_email(id_Email)
;

ALTER TABLE mock_efrsb_email_manager ADD CONSTRAINT RefEmail_ManagerManager 
    FOREIGN KEY (id_Manager)
    REFERENCES mock_efrsb_manager(id_Manager)
;


ALTER TABLE MProcedure ADD CONSTRAINT Debtor_MProcedure 
    FOREIGN KEY (id_Debtor)
    REFERENCES Debtor(id_Debtor)
;

ALTER TABLE MProcedure ADD CONSTRAINT RefMProcedure_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES Manager(id_Manager)
;


ALTER TABLE MProcedureUser ADD CONSTRAINT RefMProcedureUser_ManagerUser 
    FOREIGN KEY (id_ManagerUser)
    REFERENCES ManagerUser(id_ManagerUser) ON DELETE CASCADE
;

ALTER TABLE MProcedureUser ADD CONSTRAINT RefMProcedureUser_MProcedure 
    FOREIGN KEY (id_MProcedure)
    REFERENCES MProcedure(id_MProcedure)
;

ALTER TABLE MProcedureUser ADD CONSTRAINT refMProcedureUser_Request 
    FOREIGN KEY (id_Request)
    REFERENCES Request(id_Request)
;


ALTER TABLE MRequest ADD CONSTRAINT Ref_MRequest_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES Manager(id_Manager)
;

ALTER TABLE MRequest ADD CONSTRAINT Ref_Request_Court 
    FOREIGN KEY (id_Court)
    REFERENCES Court(id_Court)
;

ALTER TABLE MRequest ADD CONSTRAINT Ref_Request_SRO 
    FOREIGN KEY (id_SRO)
    REFERENCES efrsb_sro(id_SRO)
;


ALTER TABLE OKPD2 ADD CONSTRAINT refOKPD2_OKPD2 
    FOREIGN KEY (id_OKPD2_Parent)
    REFERENCES OKPD2(id_OKPD2)
;


ALTER TABLE PData ADD CONSTRAINT RefPData_MProcedure 
    FOREIGN KEY (id_MProcedure)
    REFERENCES MProcedure(id_MProcedure) ON DELETE CASCADE
;


ALTER TABLE PDDocFile ADD CONSTRAINT RefPDDocFile_PDDocument 
    FOREIGN KEY (id_PDDocument)
    REFERENCES PDDocument(id_PDDocument) ON DELETE CASCADE
;


ALTER TABLE PDDocument ADD CONSTRAINT RefDocType_PDDocument 
    FOREIGN KEY (id_DocType)
    REFERENCES DocType(id_DocType) ON DELETE SET NULL
;

ALTER TABLE PDDocument ADD CONSTRAINT RefPDDocument_PreDebtor 
    FOREIGN KEY (id_PreDebtor)
    REFERENCES PreDebtor(id_PreDebtor) ON DELETE CASCADE
;


ALTER TABLE PreDebtor ADD CONSTRAINT RefPreDebtor_Court 
    FOREIGN KEY (id_Court)
    REFERENCES Court(id_Court)
;

ALTER TABLE PreDebtor ADD CONSTRAINT RefPreDebtor_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES Manager(id_Manager)
;

ALTER TABLE PreDebtor ADD CONSTRAINT RefPreDebtor_MUser 
    FOREIGN KEY (id_MUser)
    REFERENCES MUser(id_MUser)
;

ALTER TABLE PreDebtor ADD CONSTRAINT RefPreDebtor_region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;


ALTER TABLE ProcedureStart ADD CONSTRAINT Ref_ProcedureStart_Contract 
    FOREIGN KEY (id_Contract)
    REFERENCES Contract(id_Contract)
;

ALTER TABLE ProcedureStart ADD CONSTRAINT Ref_ProcedureStart_Court 
    FOREIGN KEY (id_Court)
    REFERENCES Court(id_Court)
;

ALTER TABLE ProcedureStart ADD CONSTRAINT Ref_ProcedureStart_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES Manager(id_Manager)
;

ALTER TABLE ProcedureStart ADD CONSTRAINT Ref_ProcedureStart_User 
    FOREIGN KEY (id_MUser)
    REFERENCES MUser(id_MUser)
;

ALTER TABLE ProcedureStart ADD CONSTRAINT Ref_Start_Request 
    FOREIGN KEY (id_MRequest)
    REFERENCES MRequest(id_MRequest)
;

ALTER TABLE ProcedureStart ADD CONSTRAINT Ref_Start_SRO 
    FOREIGN KEY (id_SRO)
    REFERENCES efrsb_sro(id_SRO)
;


ALTER TABLE Push_request ADD CONSTRAINT RefPush_request_Push_receiver 
    FOREIGN KEY (id_Push_receiver)
    REFERENCES Push_receiver(id_Push_receiver)
;


ALTER TABLE Pushed_event ADD CONSTRAINT RefPushed_event_Event 
    FOREIGN KEY (id_Event)
    REFERENCES event(id_Event)
;

ALTER TABLE Pushed_event ADD CONSTRAINT RefPushed_event_Push_receiver 
    FOREIGN KEY (id_Push_receiver)
    REFERENCES Push_receiver(id_Push_receiver)
;


ALTER TABLE Pushed_message ADD CONSTRAINT RefPushed_message_Message 
    FOREIGN KEY (id_Message)
    REFERENCES Message(id_Message) ON DELETE CASCADE
;

ALTER TABLE Pushed_message ADD CONSTRAINT RefPushed_message_receiver 
    FOREIGN KEY (id_Push_receiver)
    REFERENCES Push_receiver(id_Push_receiver) ON DELETE CASCADE
;


ALTER TABLE Request ADD CONSTRAINT Debtor_Request 
    FOREIGN KEY (id_Debtor)
    REFERENCES Debtor(id_Debtor)
;

ALTER TABLE Request ADD CONSTRAINT refRequest_MUser 
    FOREIGN KEY (id_MUser)
    REFERENCES MUser(id_MUser)
;


ALTER TABLE Rosreestr ADD CONSTRAINT refRosreestr_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;


ALTER TABLE SRODocument ADD CONSTRAINT refSRO_Document 
    FOREIGN KEY (id_SRO)
    REFERENCES efrsb_sro(id_SRO)
;


ALTER TABLE sub_level ADD CONSTRAINT Ref_Sub_level_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;

ALTER TABLE sub_level ADD CONSTRAINT RefSubLevel_Contract 
    FOREIGN KEY (id_Contract)
    REFERENCES Contract(id_Contract)
;


ALTER TABLE Vote ADD CONSTRAINT refVote_Meeting 
    FOREIGN KEY (id_Meeting)
    REFERENCES Meeting(id_Meeting)
;


ALTER TABLE Vote_document ADD CONSTRAINT refVote_document_Vote 
    FOREIGN KEY (id_Vote)
    REFERENCES Vote(id_Vote)
;


ALTER TABLE Vote_log ADD CONSTRAINT refVote_log_Vote 
    FOREIGN KEY (id_Vote)
    REFERENCES Vote(id_Vote)
;


ALTER TABLE wcalendar ADD CONSTRAINT refWcalendar_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;


