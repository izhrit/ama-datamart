alter table ProcedureUsing ENGINE = MyISAM;

ALTER TABLE MProcedure CHANGE TimeCreated TimeCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE Debtor CHANGE TimeCreated TimeCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE Manager CHANGE TimeCreated TimeCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE MUser CHANGE TimeCreated TimeCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE Asset_proc_for_MUser CHANGE TimeCreated TimeCreated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

alter table Message change column MessageInfo_MessageType MessageInfo_MessageType char(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL;
alter table mock_efrsb_message change column MessageInfo_MessageType MessageInfo_MessageType char(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL;
alter table event change column MessageType MessageType char(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL;

