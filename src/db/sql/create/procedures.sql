set names utf8;

delimiter //
drop procedure if exists StoreExposureDocument//
create procedure StoreExposureDocument(
	  pid_MProcedure  int
	, pid_Manager     int
	, pID_Object      varchar(40)
	, pFileName       varchar(250)
	, pBody           longblob
)
begin
	declare lid_Asset int;
	declare lid_AssetAttachment int;
	declare lRevision bigint(20);

	select Asset.id_Asset, id_AssetAttachment into lid_Asset, lid_AssetAttachment
	from Asset 
	inner join MProcedure on MProcedure.id_MProcedure=Asset.id_MProcedure
	left join AssetAttachment on AssetAttachment.id_Asset=Asset.id_Asset and FileName=pFileName
	where ID_Object=pID_Object 
	and Asset.id_MProcedure=pid_MProcedure 
	and id_Manager=pid_Manager;

	if (lid_Asset is not null) then
		select if(max(Revision) is null, 1, max(Revision) + 1) 
		into lRevision from AssetAttachment;

		if (lid_AssetAttachment is null) then
			insert into AssetAttachment set 
			id_Asset=lid_Asset, FileName=pFileName, Revision=lRevision,
			FileSize=length(pBody), md5hash=md5(pBody), Body=compress(pBody)
			;
		else
			update AssetAttachment set 
			id_Asset=lid_Asset, FileName=pFileName, Revision=lRevision,
			FileSize=length(pBody), md5hash=md5(pBody), Body=compress(pBody)
			where id_AssetAttachment=lid_AssetAttachment
			;
		end if;
	end if;
end //
delimiter ;

delimiter //
drop procedure if exists StoreExposureAsset//
create procedure StoreExposureAsset(
      pid_MProcedure int, pid_Manager int, pID_Object varchar(40)
    , pState char(1), pOKATO char(6), pAssetGroupTitle varchar(100)
    , pTitle text, pDescription text, pAddress text
    , pisAvailableToAll int, pmd5hash varchar(40)
    , pExtraFields longblob
)
begin
	declare lid_Asset int;
	declare lRevision bigint(20);
	declare lid_Region int;
	declare lid_AssetGroup int;
	declare lid_MProcedure int;

	select a.id_Asset, mp.id_MProcedure into lid_Asset, lid_MProcedure
	from MProcedure mp 
	left join Asset a on mp.id_MProcedure=a.id_MProcedure and a.ID_Object=pID_Object
	where mp.id_Manager=pid_Manager and mp.id_MProcedure=pid_MProcedure;

	if (lid_MProcedure is not null) then
		select id_Region into lid_Region from region where pOKATO=OKATO;
		select id_AssetGroup into lid_AssetGroup from AssetGroup where Title=pAssetGroupTitle;

		select if(max(Revision) is null, 1, max(Revision) + 1) into lRevision from Asset;

		if (lid_Asset is not null) then
			update Asset set
				id_AssetGroup=lid_AssetGroup, id_Region=lid_Region, Revision=lRevision, TimeExpositionStart=now()
				, Title=pTitle, Description=pDescription, Address=pAddress, State=pState
				, isAvailableToAll=pisAvailableToAll, ExtraFields=compress(pExtraFields), md5hash=pmd5hash
			where id_Asset=lid_Asset;
			select lid_Asset id_Asset;
		else
			insert into Asset set id_MProcedure=pid_MProcedure, ID_Object=pID_Object,
				id_AssetGroup=lid_AssetGroup, id_Region=lid_Region, Revision=lRevision, TimeExpositionStart=now()
				, Title=pTitle, Description=pDescription, Address=pAddress, State=pState
				, isAvailableToAll=pisAvailableToAll, ExtraFields=compress(pExtraFields), md5hash=pmd5hash
			;
			select last_insert_id() id_Asset;
		end if;
	end if;
end //
delimiter ;

delimiter //

drop procedure if exists UpdateVerifiedDebtor//
create procedure UpdateVerifiedDebtor(
	  pid_Debtor  int
	, pINN        varchar(12)
	, pOGRN       varchar(16)
	, pSNILS      varchar(13)
	, pName       varchar(250)
	, pBankruptId BIGINT
)
begin
	declare lid_Debtor int;

	select id_Debtor into lid_Debtor from Debtor where BankruptId=pBankruptId;

	if (lid_Debtor is null || lid_Debtor=pid_Debtor) then
		select pid_Debtor into lid_Debtor;
	else
		update MProcedure set id_Debtor=lid_Debtor where id_Debtor=pid_Debtor;
		update Request set id_Debtor=lid_Debtor where id_Debtor=pid_Debtor;
		delete from Debtor where id_Debtor=pid_Debtor;
 	end if;

	update Debtor set
		TimeVerified=now()
		, VerificationState='v'
		, BankruptId=pBankruptId
		, INN=pINN
		, OGRN=pOGRN
		, SNILS=pSNILS
		, Name=pName
	where
		id_Debtor=lid_Debtor;
end //

-- call UploadProcedureInfo(3,b'0','sdf','swdfewer');
drop procedure if exists UploadProcedureInfo//
create procedure UploadProcedureInfo(
	  pid_MProcedure int
	, pallow_ctb BIT(1)
	, pContent_hash VARCHAR(250)
	, pFileData LONGBLOB
)
begin
	declare lrevision bigint;
	declare lpublicDate datetime;
	declare lContent_hash varchar(250);
	declare lid_PData int;
	declare lallow_ctb bit(1);

	select null into lallow_ctb;
	select if(max(revision) is null, 1, max(revision) + 1), now() into lrevision, lpublicDate from PData;

	if (pFileData is not null) then
		insert into PData
		set id_MProcedure = pid_MProcedure, fileData = pFileData, ctb_allowed= pallow_ctb,
			revision= lrevision, publicDate= lpublicDate, Content_hash= pContent_hash;
	else
		select Content_hash into lContent_hash from MProcedure where id_MProcedure=pid_MProcedure;
		if (lContent_hash<>pContent_hash) then
			select * from bad_content_hash_in_throw_exception;
		end if;

		if (b'1' = pallow_ctb) then
			select id_PData, ctb_allowed into lid_PData, lallow_ctb from PData where id_MProcedure=pid_MProcedure order by revision desc limit 1;
			if (b'0' = lallow_ctb) then
				update PData
				set ctb_allowed= pallow_ctb,
					revision= lrevision, publicDate= lpublicDate, Content_hash= pContent_hash
				where id_PData=lid_PData;
			end if;
		end if;
	end if;

	if (b'1' = pallow_ctb && (pFileData is not null || b'0' = lallow_ctb)) then
		update MProcedure set publicDate= lpublicDate, revision= lrevision, Content_hash= pContent_hash
			, ctb_publicDate= lpublicDate, ctb_revision= lrevision
		where id_MProcedure=pid_MProcedure;
	elseif (pFileData is not null) then
		update MProcedure set publicDate= lpublicDate, revision= lrevision, Content_hash= pContent_hash
		where id_MProcedure=pid_MProcedure;
	end if;
end //

drop procedure if exists Add_Manager//
CREATE PROCEDURE Add_Manager(
	  pid_Contract int

	, pmFirstName VARCHAR(50)
	, pmMiddleName VARCHAR(50)
	, pmLastName VARCHAR(50)
	, pmEFRSBNum VARCHAR(50)
	, pmInn VARCHAR(12)
	, pmBTAccount TEXT

	, out opid_Manager int
)
begin
	declare lVerificationState char(1);
	declare lpmBTAccount TEXT;

	select id_Manager, VerificationState,  pmBTAccount
	into opid_Manager, lVerificationState, lpmBTAccount
	from Manager
	where id_Contract=pid_Contract && (efrsbNumber=pmEFRSBNum || INN=pmInn);

	if opid_Manager is null then
		insert into Manager
		set id_Contract= pid_Contract, efrsbNumber= pmEFRSBNum,
			firstName= pmFirstName, middleName= pmMiddleName, lastName= pmLastName, INN=pmInn, BankroTechAcc=pmBTAccount;
		select last_insert_id() into opid_Manager;
	else
		if ('v'=lVerificationState) then -- verified..
			update Manager
			set BankroTechAcc=pmBTAccount
			where id_Manager=opid_Manager;
		elseif (lVerificationState is null || 's'=lVerificationState) then -- not checked or skipped..
			update Manager
			set firstName= pmFirstName, middleName= pmMiddleName, lastName= pmLastName, INN=pmInn, BankroTechAcc=pmBTAccount
			where id_Manager=opid_Manager;
		else
			select null into opid_Manager;
			select "Manager" as error_type, lVerificationState as error_details;
		end if;
	end if;
end //
delimiter ;

delimiter //
drop procedure if exists Fix_Debtor//
CREATE PROCEDURE Fix_Debtor(
	  pInn VARCHAR(12)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)

	, out poInn VARCHAR(12)
	, out poOgrn VARCHAR(16)
	, out poSnils VARCHAR(13)
)
begin
	select replace(pSnils,' ','') into poSnils;
	select if(poSnils='00000000000' or poSnils='',null,poSnils) into poSnils;
	select replace(pInn,' ','') into poInn;
	select if(poInn='',null,poInn) into poInn;
	select replace(pOgrn,' ','') into poOgrn;
	select if(poOgrn='',null,poOgrn) into poOgrn;
end//
delimiter ;

delimiter //
drop procedure if exists Select_Debtors//
CREATE PROCEDURE Select_Debtors(
	  pInn VARCHAR(12)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)
)
begin
    declare lSnils VARCHAR(13);
    declare lInn VARCHAR(12);
    declare lOgrn VARCHAR(16);

    call Fix_Debtor(pInn,pOgrn,pSnils,lInn,lOgrn,lSnils);

        select id_Debtor, VerificationState, Name, INN, OGRN, SNILS
        from Debtor
        where ( (lInn   is not null and INN   is not null and   INN=lInn)
             or (lSnils is not null and SNILS is not null and SNILS=lSnils)
             or (lOgrn  is not null and OGRN  is not null and  OGRN=lOgrn))
        and (lInn   is null or Debtor.INN   is null or   lInn=Debtor.INN)
        and (lSnils is null or Debtor.SNILS is null or lSnils=Debtor.SNILS)
        and (lOgrn  is null or Debtor.OGRN  is null or  lOgrn=Debtor.OGRN)
        ;
end//
delimiter ;

delimiter //
drop procedure if exists Add_Debtor//
CREATE PROCEDURE Add_Debtor(
	  pInn VARCHAR(12)
	, pDebtorName VARCHAR(250)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)

	, out opid_Debtor int
)
begin
	declare lVerificationState char(1);
	declare lSnils VARCHAR(13);
	declare lInn VARCHAR(12);
	declare lOgrn VARCHAR(16);

	declare exit handler for 1172 -- if there are several Debtor .. 
	begin 
		select null into opid_Debtor;
		select "Debtor" as error_type, 'w' as error_details; 
	end;

	call Fix_Debtor(pInn,pOgrn,pSnils,lInn,lOgrn,lSnils);

	if (lInn is null && lOgrn is null && lSnils is null) then
		select "Debtor" as error_type, 'i' as error_details; -- Для должника указан некорректные параметры (ИНН,ОГРН,СНИЛС)
	else		
		select id_Debtor, VerificationState into opid_Debtor, lVerificationState from Debtor
		where ( (lInn   is not null and INN   is not null and   INN=lInn)
		     or (lSnils is not null and SNILS is not null and SNILS=lSnils)
		     or (lOgrn  is not null and OGRN  is not null and  OGRN=lOgrn))
		and (lInn   is null or Debtor.INN   is null or   lInn=Debtor.INN)
		and (lSnils is null or Debtor.SNILS is null or lSnils=Debtor.SNILS)
		and (lOgrn  is null or Debtor.OGRN  is null or  lOgrn=Debtor.OGRN)
		;
		if (opid_Debtor is null) then
			insert into Debtor
			set Name= pDebtorName, INN= lInn, SNILS= lSnils, OGRN= lOgrn;
			select last_insert_id() into opid_Debtor;
		else
			if ('v'=lVerificationState) then -- verified..
				select null into lVerificationState;
			elseif (lVerificationState is null || 's'=lVerificationState) then -- not checked or skipped..
				update Debtor
				set Name=ifnull(pDebtorName,Name), INN=ifnull(lInn,INN), OGRN=ifnull(lOgrn,OGRN), SNILS=ifnull(lSnils,SNILS)
				where id_Debtor=opid_Debtor;
			else
				select null into opid_Debtor;
				select "Debtor" as error_type, lVerificationState as error_details;
			end if;
		end if;
	end if;
end //

drop procedure if exists Add_MProcedure//
CREATE PROCEDURE Add_MProcedure(
	  pid_Manager int

	, pInn VARCHAR(12)
	, pDebtorName VARCHAR(250)
	, pOgrn VARCHAR(16)
	, pSnils VARCHAR(13)

	, pCasenumber VARCHAR(60)
	, pFeatures CHAR(1)
	, pProcedure_type CHAR(3)

	, out opid_MProcedure int
)
begin
	declare lid_Debtor int;
	declare lProcedure_type CHAR(2);
	declare lProcedure_VerificationState char(1);
	declare lDebtor_VerificationState char(1);

	declare lDebtor_Inn VARCHAR(12);
	declare lDebtor_Name VARCHAR(250);
	declare lDebtor_Ogrn VARCHAR(16);
	declare lDebtor_Snils VARCHAR(13);

	declare exit handler for 1172 -- if there are several MProcedure ..
	begin 
		select null into opid_MProcedure;
		select "MProcedure" as error_type, 'w' as error_details; 
	end;

	select ProcedureType_SafeDBValueForShort(pProcedure_type) into lProcedure_type;

	select MProcedure.id_MProcedure, MProcedure.VerificationState, Debtor.VerificationState,
		   Debtor.Name,  Debtor.INN,  Debtor.OGRN,  Debtor.SNILS,  Debtor.id_Debtor
	into   opid_MProcedure,          lProcedure_VerificationState, lDebtor_VerificationState,
		   lDebtor_Name, lDebtor_Inn, lDebtor_Ogrn, lDebtor_Snils, lid_Debtor
	from MProcedure
	inner join Debtor on Debtor.id_Debtor=MProcedure.id_Debtor
	where casenumber=pCasenumber && procedure_type=lProcedure_type && id_Manager=pid_Manager
		&& (pInn is not null or pSnils is not null or pOgrn is not null)
		&& (   (pInn   is not null and Debtor.INN   is not null and   Debtor.INN=pInn)
		    or (pSnils is not null and Debtor.SNILS is not null and Debtor.SNILS=pSnils)
		    or (pOgrn  is not null and Debtor.OGRN  is not null and  Debtor.OGRN=pOgrn))
		&& (pInn   is null or Debtor.INN   is null or   pInn=Debtor.INN)
		&& (pSnils is null or Debtor.SNILS is null or pSnils=Debtor.SNILS)
		&& (pOgrn  is null or Debtor.OGRN  is null or  pOgrn=Debtor.OGRN)
		;

	if (opid_MProcedure is not null) then
		if ('v'=lProcedure_VerificationState) then -- verified..
			select null into lProcedure_VerificationState;
		elseif (lProcedure_VerificationState is null) then -- not checked or skipped..
			if (lDebtor_VerificationState is not null && 's'<>lDebtor_VerificationState && 'v'<>lDebtor_VerificationState) then -- not checked or skipped..
				select null into opid_MProcedure;
				select "Debtor" as error_type, lDebtor_VerificationState as error_details;
			elseif (ifnull(lDebtor_Name,'')<>ifnull(pDebtorName,'') or
			        ifnull(lDebtor_Inn,'')<>ifnull(pINN,'') or
			        ifnull(lDebtor_Ogrn,'')<>ifnull(pOgrn,'') or
			        ifnull(lDebtor_Snils,'')<>ifnull(pSnils,''))
			then
				update Debtor set INN=pINN, OGRN=pOgrn, SNILS=pSnils, Name=pDebtorName where id_Debtor=lid_Debtor;
			end if;
		elseif ('s'=lProcedure_VerificationState) then -- not checked or skipped..
			select null into lProcedure_VerificationState;
		else
			select null into opid_MProcedure;
			select "MProcedure" as error_type, lProcedure_VerificationState as error_details;
		end if;
	else
		call Add_Debtor(pInn,pDebtorName,pOgrn,pSnils,lid_Debtor);
		if (lid_Debtor is not null) then
			insert into MProcedure
			set  casenumber= pCasenumber
				,procedure_type= lProcedure_type
				,id_Manager= pid_Manager
				,features= pFeatures
				,id_Debtor= lid_Debtor;
			select last_insert_id() into opid_MProcedure;

			insert into MProcedureUser(id_MProcedure,id_ManagerUser)
				select opid_MProcedure, mu.id_ManagerUser
				from ManagerUser mu
				left join MProcedureUser mpu on mpu.id_ManagerUser=mu.id_ManagerUser
			                                 && mpu.id_MProcedure=opid_MProcedure
				where mu.id_Manager=pid_Manager
				&& 0<>mu.DefaultViewer
				&& mpu.id_ManagerUser is null;
		end if;
	end if;
end //

drop procedure if exists SafeUploadProcedureInfo//
CREATE PROCEDURE SafeUploadProcedureInfo(
	  pid_Contract int

	, pmFirstName VARCHAR(50), pmMiddleName VARCHAR(50), pmLastName VARCHAR(50)
	, pmEFRSBNum VARCHAR(50), pmInn VARCHAR(12), pmBTAccount TEXT

	, pInn VARCHAR(12), pDebtorName VARCHAR(250)
	, pOgrn VARCHAR(16), pSnils VARCHAR(13)

	, pCasenumber VARCHAR(60), pFeatures CHAR(1), pProcedure_type CHAR(2)

	, pallow_ctb BIT(1)

	, pContent_hash VARCHAR(250), pFileData LONGBLOB
)
BEGIN
	declare lid_Manager int;
	declare lid_MProcedure int;
	declare lSnils VARCHAR(13);
	declare lid_Contract int;

	select id_Contract into lid_Contract from Manager where efrsbNumber=pmEFRSBNum;

	if (lid_Contract<>pid_Contract) then
		select "Manager" as error_type, 'c' as error_details; -- АУ прописан в другом договоре
	else
		select replace(pSnils,' ','') into lSnils;
		select if(lSnils='00000000000',null,lSnils) into lSnils;

		if ((''=pInn || pInn is null) && (''=pOgrn || pOgrn is null) && (''=lSnils || lSnils is null)) then
			select "Debtor" as error_type, 'i' as error_details; -- Для должника указан некорректные параметры (ИНН,ОГРН,СНИЛС)
		else
			call Add_Manager(pid_Contract
				,pmFirstName,pmMiddleName,pmLastName,pmEFRSBNum,pmInn,pmBTAccount
				,lid_Manager);

			if (lid_Manager is not null) then
				call Add_MProcedure(lid_Manager
					,pInn,pDebtorName,pOgrn,lSnils
					,pCasenumber,pFeatures,pProcedure_type
					,lid_MProcedure);

				if (lid_MProcedure is not null) then
					call UploadProcedureInfo(lid_MProcedure, pallow_ctb, pContent_hash, pFileData);
				end if;
			end if;
		end if;
	end if;
END //

delimiter ;
delimiter //

drop function if exists ProcedureType_SafeShortForDBValue//
create function ProcedureType_SafeShortForDBValue(db_value char(1))
returns varchar(2)
deterministic
begin
	case db_value
		when 'n' then return 'Н';
		when 'k' then return 'КП';
		when 'i' then return 'РИ';
		when 'd' then return 'РД';
		when 'v' then return 'ВУ';
		when 'o' then return 'ОД';
		when 'b' then return 'Б';
		else return db_value;
	end case;
end //

drop function if exists ProcedureType_SafeDBValueForShort//
create function ProcedureType_SafeDBValueForShort(short_name char(3))
returns varchar(2)
deterministic
begin
  case short_name
    when 'Н'  then return 'n';
    when 'Нс' then return 'n';
    when 'КП' then return 'k';
    when 'КПс' then return 'k';
    when 'РИ' then return 'i';
    when 'РД' then return 'd';
    when 'ВУ' then return 'v';
    when 'ВУс' then return 'v';
    when 'ОД' then return 'o';
    when 'ОДс' then return 'o';
    when 'Б'  then return 'b';
    else return 1/0;
  end case;
end //

delimiter ;

delimiter //
drop procedure if exists exit_on_error//
create procedure exit_on_error()
begin
	if (0<>@@error_count) then
		kill connection_id();
	end if;
end//
delimiter ;

delimiter //
drop procedure if exists safe_add_update_wcalendar//
create procedure safe_add_update_wcalendar(
	pYear int,
	pDays text,
    pOKATO char(6)
	)
begin
	declare lMaxRevision bigint;
    declare lIdRegion int;
	select ifnull(max(Revision), 0) into lMaxRevision from wcalendar;
	select id_Region into lIdRegion from region where region.OKATO=pOKATO;

	insert into wcalendar
		(Year,  Days, id_Region, Revision)
	values
		(pYear, compress(pDays), lIdRegion, lMaxRevision+1)
	on duplicate key update
		Days=compress(pDays)
		, Revision=lMaxRevision+1
		, id_Region = lIdRegion
	;
end//
delimiter ;

DELIMITER $$
DROP FUNCTION IF EXISTS getNotifyDate;
CREATE FUNCTION getNotifyDate(
	notifyType CHAR(1),
    eventTime DATETIME,
    checkDayOff BIT(1)
)
RETURNS DATETIME
BEGIN
    DECLARE dateResult DATETIME;
	CASE
	  WHEN notifyType = 'a' THEN SET dateResult = '2000-01-01';
      WHEN notifyType = 'b' THEN SET dateResult = eventTime - interval 1 day;
      WHEN notifyType = 'c' THEN SET dateResult = eventTime - interval 3 day;
      WHEN notifyType = 'd' THEN SET dateResult = eventTime - interval 5 day;
      WHEN notifyType = 'e' THEN SET dateResult = eventTime - interval 15 day;
	  ELSE SET dateResult = eventTime;
	END CASE;
    CASE
	  WHEN WEEKDAY(dateResult) = 5 && checkDayOff = 0 THEN SET dateResult = dateResult - interval 1 day;
	  WHEN WEEKDAY(dateResult) = 6 && checkDayOff = 0 THEN SET dateResult = dateResult - interval 2 day;
	  ELSE SET dateResult = dateResult;
	END CASE;
	RETURN dateResult;
END$$
DELIMITER ;

DELIMITER //
drop procedure if exists SafeUploadAnketa//
CREATE PROCEDURE SafeUploadAnketa(
	  pManagerINN varchar(12), pid_MUser int

	, pDebtorFirstName varchar(50), pDebtorLastName varchar(50), pDebtorMiddleName varchar(50)
	, pDebtorEmail varchar(50), pDebtorSNILS varchar(13), pDebtorINN varchar(12)

	, pFileData LONGBLOB, pContent_hash varchar(250)

	, out error_text varchar(250)
)
BEGIN
	declare lrevision bigint;
	declare lid_MData, lid_ManagerUser, lid_Manager int;

	select id_ManagerUser, m.id_Manager into lid_ManagerUser, lid_Manager
	from ManagerUser mu
	inner join Manager m on mu.id_Manager=m.id_Manager
	where INN = pManagerINN and id_MUser = pid_MUser;
	if (lid_ManagerUser is null) then
		set error_text = concat("Отсутствует связь АУ и наблюдателя (ИНН АУ: ",pManagerINN,", id_MUser=",pid_MUser,")");
	else
		select if(max(revision) is null, 1, max(revision) + 1) into lrevision from MData;

		insert into MData
		set id_ManagerUser = lid_ManagerUser, id_Debtor = null, fileData = pFileData,
		revision= lrevision, publicDate= now(), MData_Type='a', ContentHash = pContent_hash;

		select last_insert_id() into lid_MData;

		insert into AnketaNP
		set firstName = pDebtorFirstName, lastName = pDebtorLastName, middleName = pDebtorMiddleName,
		Email= pDebtorEmail, SNILS= pDebtorSNILS, INN= pDebtorINN, id_MData = lid_MData;

		set error_text = null;
	end if;
END//
DELIMITER ;

DELIMITER //
drop procedure if exists SafeUploadDeals//
CREATE PROCEDURE SafeUploadDeals(
	  pManagerINN varchar(12), pid_MUser int, pDebtorINN varchar(12)
	, pFileData LONGBLOB, pContent_hash varchar(250)
)
BEGIN
	declare lrevision bigint;
	declare lid_ManagerUser, lid_Debtor, lid_Manager int;

	select mu.id_ManagerUser, d.id_Debtor, m.id_Manager into lid_ManagerUser, lid_Debtor, lid_Manager
	from ManagerUser mu
	inner join Manager m on m.id_Manager=mu.id_Manager
	inner join MProcedure p on p.id_Manager=m.id_Manager
	inner join Debtor d on d.id_Debtor=p.id_Debtor
	where m.INN=pManagerINN and mu.id_MUser=pid_MUser and d.INN=pDebtorINN
	order by p.publicDate desc
	limit 1;

	if (lid_ManagerUser is null) then
		select concat("Отсутствует связь АУ и наблюдателя (ИНН АУ: ",pManagerINN,", id_MUser=",pid_MUser,")") message;
	else
		select if(max(revision) is null, 1, max(revision) + 1) into lrevision from MData;

		delete from MData
		where id_ManagerUser=lid_ManagerUser and id_Debtor=lid_Debtor and MData_Type='d';

		insert into MData
		set id_ManagerUser = lid_ManagerUser, id_Debtor = lid_Debtor, fileData = pFileData,
		revision= lrevision, publicDate= now(), MData_Type='d', ContentHash = pContent_hash;

		update Manager set HasIncoming=1 where id_Manager=lid_Manager;
		update MUser set HasOutcoming=1 where id_MUser=pid_MUser;
	end if;
END//
DELIMITER ;

DELIMITER //
drop function if exists ProceduDebtor_INN_OGRN_SNILS_lp_id//
CREATE FUNCTION `ProceduDebtor_INN_OGRN_SNILS_lp_id`(
  aINN   varchar(12)
 ,aOGRN  varchar(16)
 ,aSNILS varchar(13)
) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  if (aSNILS is not null and 0<>length(aSNILS) or length(aINN)=12) then
    return null;
  else
    return concat(ifnull(aINN,''),'.',ifnull(aOGRN,''));
  end if;
end//
DELIMITER ;

DELIMITER //
drop function if exists ProceduDebtor_INN_OGRN_SNILS_np_id//
CREATE FUNCTION `ProceduDebtor_INN_OGRN_SNILS_np_id`(
  aINN   varchar(12)
 ,aOGRN  varchar(16)
 ,aSNILS varchar(13)
) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  if (aSNILS is not null and 0<>length(aSNILS) or length(aINN)=12) then
    return concat(ifnull(aINN,''),'.',ifnull(aSNILS,''));
  else
    return null;
  end if;
end//
DELIMITER ;

DELIMITER //
drop function if exists ProcedureUsing_Section_readable_3_19//
CREATE FUNCTION `ProcedureUsing_Section_readable_3_19`(aSection char(4)) RETURNS text CHARSET utf8
 DETERMINISTIC
begin
 return concat(
case aSection
when 1 then 'СК/сохранить'
when 2 then 'СК/протокол'
when 3 then 'СК/к повестке'
when 4 then 'СК/протокол открыть'
when 5 then 'РТК/создание ТК'
when 6 then 'РТК/МПТ/запланировать'
when 7 then 'РТК/печатать'
when 8 then 'Финансы/создание расхода (старая версия)'
when 9 then 'Делопроизводство/регистрация входящего'
when 10 then 'Делопроизводство/создать партию на сайте Почты'
when 11 then 'КМ/сохранить ИГ'
when 12 then 'КМ/создать объект'
when 13 then 'КМ/создать дебиторку'
when 14 then 'Планирование/сохранить задачу'
when 15 then 'Торги/сохранить лот'
when 16 then 'анализ сделок/добавить'
when 17 then 'ПГЗ/Должник/пакет'
when 18 then 'ПГЗ/Должник/печать'
when 19 then 'ПГЗ/Должник/редактировать'
when 20 then 'ПГЗ/КДЛ/пакет'
when 21 then 'ПГЗ/КДЛ/печать'
when 22 then 'ПГЗ/КДЛ/редактировать'
when 23 then 'ПГЗ/супруг/пакет'
when 24 then 'ПГЗ/супруг/печать'
when 25 then 'ПГЗ/супруг/редактировать'
when 26 then 'ЕФРСБ/подать'
when 27 then 'финанализ/фл/сформировать'
when 28 then 'ДПМ/ефрсб/опубликовать/введение/ри'
when 29 then 'ДПМ/ефрсб/опубликовать/введение/кп'
when 30 then 'ДПМ/СК/выполнено'
when 31 then 'РЖП/зарегистрировать'
when 32 then 'отчёт/превью/получатели'
when 33 then 'отчёт/превью/word'
when 34 then 'работники/уведомление'
when 35 then 'работники/уведомление/печать'
when 36 then 'работники/уведомление/редактировать'
when 37 then 'запросыпосчетам/основной/запросить'
when 38 then 'запросыпосчетам/основной/печать'
when 39 then 'запросыпосчетам/основной/редактировать'
when 40 then 'запросыпосчетам/закрыть/запросить'
when 41 then 'запросыпосчетам/закрыть/печать'
when 42 then 'запросыпосчетам/закрыть/редактировать'
else 'неучтённое действие'
end
,' (сч.',aSection,')'
);
end//
DELIMITER ;

DELIMITER //
drop function if exists ProcedureUsing_Section_readable_3_20//
CREATE FUNCTION `ProcedureUsing_Section_readable_3_20`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return if(43>convert(aSection,unsigned integer),ProcedureUsing_Section_readable_3_19(aSection),concat(
case aSection

when 43 then 'Финансы/расход/прож.минимум'
when 44 then 'Финансы/расход/ефрсб'
when 45 then 'Финансы/расход/почта'
when 46 then 'Финансы/расход/коммерсант'
when 47 then 'Финансы/доход'
when 48 then 'Финансы/фото'
when 49 then 'Финансы/создать учётную группу'

when 50 then 'Финансы/запросить'
when 51 then 'Финансы/счёт/залоговый'
when 52 then 'Финансы/счёт/задатки'

when 53 then 'Работники/добавить'
when 54 then 'Работники/импортировать'
when 55 then 'Работники/собрание'

when 56 then 'Права/группа'
when 57 then 'Права/изменить уровень доступа'
when 58 then 'Права/назначить группу для процедуры'

when 59 then 'ФЛ/2НДФЛ'
when 60 then 'ФЛ/доход'
when 61 then 'ФЛ/супруг'
when 62 then 'ФЛ/иждивенец'

when 63 then 'Субсидиарка/Собрать'
when 64 then 'Субсидиарка/КДЛ'
when 65 then 'Субсидиарка/Нарушение'
when 66 then 'Субсидиарка/Привлечение'

when 67 then 'РТК/перед ликв.квотой'
when 68 then 'РТК/сокредитор'
when 69 then 'РТК/погашение'

when 70 then 'Обмен/настройка/фото'
when 71 then 'Обмен/настройка/авто'
when 72 then 'Обмен/настройка/google calendar'

when 73 then 'ПГЗ/должник/pdf'
when 74 then 'ПГЗ/супруг/pdf'
when 75 then 'ПГЗ/КДЛ/pdf'
when 76 then 'ПГЗ/банки/pdf'

when 77 then 'ефрсб/введение/мастер'
when 78 then 'ефрсб/введение/меню/подача'
when 79 then 'ефрсб/введение/редактировать'

when 80 then 'собрание дольщиков'

when 81 then 'Финансы/расход/общего вида'

when 82 then 'Документ/открытие из архива'

else 'неучтённое действие'
end
,' (сч.',aSection,')'
));
end//
DELIMITER ;

DELIMITER //
drop function if exists ProcedureUsing_Section_readable_3_21//
CREATE FUNCTION `ProcedureUsing_Section_readable_3_21`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return if(83>convert(aSection,unsigned integer),ProcedureUsing_Section_readable_3_20(aSection),concat(
case aSection

when 83 then 'импорт банк выписки/начать'
when 84 then 'импорт банк выписки/завершить'
when 85 then 'импорт ден опер из excel/завершить'
when 86 then 'кнопка Должник'
when 87 then 'Кнопка Процедура'
when 88 then 'Подпись/загрузить'
when 89 then 'Подпись АУ/Добавить'
when 90 then 'Инструкция по добавлению подписи'
when 91 then 'Инструкция по массовому погашению'
when 92 then 'Анализ сделок/открытие заключ преднамер фикт'
when 93 then 'Финальный отчет на ЕФРСБ'
when 94 then 'Дебиторка/указать погаш сумму ссылка'
when 95 then 'Дебиторка/добавление погашения'
when 96 then 'Дебиторка/погашение/кнопка Создать доход'
when 97 then 'Дебиторка/погашение/учесть старые записи как погашения'
when 98 then 'Список процедур в excel'
when 99 then 'залоговый счет/добавление второго и последующего'
when 100 then 'Учетная группа Залоговый/открытие'
when 101 then 'Импорт объектов КМ из excel'
when 102 then 'Импорт НДС из excel'
when 103 then 'Импорт ден средств КМ из excel'
when 104 then 'Импорт дебиторской задолженности из excel'
when 105 then 'ФА ФЛ/показывать требования не включенные в реестр'
when 106 then 'Сводный анализ дебиторской задолженности'
when 107 then 'документ архива младше 30 дней'
when 108 then 'документ архива 30-180 дней'
when 109 then 'документ архива старше 180 дней'
when 110 then 'Текущее требование/зарегистрировать'
when 111 then 'Свойства должника/кнопка Печать'
when 112 then 'Дебиторка/добавление фото'
when 113 then 'Нажатие на Уведомление'
when 114 then 'Нажатие на уведомление по Мобилке'

else 'неучтённое действие'
end
,' (сч.',aSection,')'
));
end//
DELIMITER ;

DELIMITER //
drop function if exists ProcedureUsing_Section_readable_3_22 //
CREATE FUNCTION `ProcedureUsing_Section_readable_3_22`(aSection char(4)) RETURNS text CHARSET utf8
  DETERMINISTIC
begin
  return if(115>convert(aSection,unsigned integer),ProcedureUsing_Section_readable_3_21(aSection),concat(
case aSection

when 115 then 'Свойства процедуры/добавить фото'
when 116 then 'ИГ/добавить фото'
when 117 then 'ИГ/сохранение залогодержателя'
when 118 then 'Денежные операции/изменение даты Остатка'
when 119 then 'Нормативная база/открытие'
when 120 then 'Отчет АУ/открытие через Печать документов'
when 121 then 'Элеткро КК/Создание'
when 122 then 'Импорт данных из 1с 7.7'

else 'неучтённое действие'
end
,' (сч.',aSection,')'
));
end //
DELIMITER ;

DELIMITER //
drop function if exists ProcedureUsing_Section_readable//
CREATE FUNCTION `ProcedureUsing_Section_readable`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return if(123>convert(aSection,unsigned integer),ProcedureUsing_Section_readable_3_22(aSection),concat(
case aSection

when 123 then 'Список пользователей/открытие'
when 124 then 'Список пользователей/Кнопка Создать'
when 125 then 'Новый пользователь/Сохранить'
when 126 then 'Удаление пользователя'
when 127 then 'Задача/Показать отчет по задаче'
when 128 then 'Задача/Выбор ответственного'
when 129 then 'Задача/Изменение статуса задачи'
when 130 then 'Задача/Чекбокс google календарь'
when 131 then 'Задача/просмотр истории'
when 132 then 'Задача/переход на страницу дела'
when 133 then 'Кнопка Подготовка к торгам'
when 134 then 'Подготовка к торгам/доступ/неопределенный круг лиц'
when 135 then 'Подготовка к торгам/доступ/коммерческие'
when 136 then 'Подготовка к торгам/кнопка ОК (сохранение)'
when 137 then 'Подготовка к торгам/подтверждение отправки данных'
when 138 then 'КМ/переход во вкладку Подгтовка к торгам'
when 139 then 'КМ/вкладка Подготовка к торгам/смена стадии работы'
when 140 then 'Обмен данными/Чекбокс о подготовке к торгам'
when 141 then 'Свойства ТК/вкладка Залоговые обязательства'
when 142 then 'Счета/установка статуса Заблокирован'
when 143 then 'Массовое погашение/Стена даты погашения'
when 144 then 'Получатели/открытие окна в Своем шаблоне'
when 145 then 'Электронная подпись/открытие утилиты'

else 'неучтённое действие'
end
,' (сч.',aSection,')'
));
end//
DELIMITER ;

drop function if exists `FaUsing_Section_readable`;
DELIMITER //
CREATE FUNCTION `FaUsing_Section_readable`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return concat(
case aSection
when 1  then 'Создать новый финанализ'
when 2  then 'Импорт/балансы/1С'
when 3  then 'Импорт/балансы/ФНС'
when 4  then 'Импорт/КМ/ПАУ'
when 5  then 'Импорт/КМ/витрина'
when 6  then 'Импорт/Сделки/Excel'
when 7  then 'Сделки/Добавить'
when 8  then 'Сделки/Импортировать банковскую выписку'
when 9  then 'Сделки/Получить-Обновить аналитику'
when 10  then 'Документ/О признаказ плохого банкротства'
when 11  then 'Документ/Анализ контрагентов'
else 'неучтённое действие'
end
,' (сч.',aSection,')'
);
end//
DELIMITER ;

drop function if exists `PeproUsing_Section_readable`;
DELIMITER //
CREATE FUNCTION `PeproUsing_Section_readable`(aSection char(4)) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return concat(
case aSection
when 1  then 'Создать исходящее'
when 2  then 'Печатать конверт'
when 3  then 'Печатать уведомление'
when 4  then 'Печатать опись вложение'
when 5  then 'Создать реестр'
when 6  then 'Создать партию отправлений на сайте Почта России'
when 7  then 'Создать файл для загрузки на сайт Почта России'
when 8  then 'Рассылка'
when 9  then 'Добавить шаблон'
when 10 then 'Добавить входящее'
when 11 then 'Печатать журнал входящих'
else 'неучтённое действие'
end
,' (сч.',aSection,')'
);
end//
DELIMITER ;

DELIMITER //
drop procedure if exists SafeUpdateEfrsbManager//
CREATE PROCEDURE SafeUpdateEfrsbManager(
  pid_Manager int(11),
  pArbitrManagerID int(11),
  pSRORegNum varchar(30),
  pFirstName varchar(50),
  pMiddleName varchar(50),
  pLastName varchar(50),
  pOGRNIP varchar(15),
  pINN varchar(12),
  pSRORegDate datetime,
  pRegNum varchar(30),
  pCorrespondenceAddress varchar(300),
  pRevision bigint(20)
)
BEGIN
	declare lArbitrManagerID int(11);
	declare lid_SRO int(11);

	select ArbitrManagerID into lArbitrManagerID
	from efrsb_manager where id_Manager=pid_Manager;

	if (lArbitrManagerID is null) then
		insert into efrsb_manager
		set id_Manager=pid_Manager, ArbitrManagerID=pArbitrManagerID
		, SRORegNum=pSRORegNum, SRORegDate=pSRORegDate, RegNum=pRegNum
		, FirstName=pFirstName, MiddleName=pMiddleName, LastName=pLastName
		, OGRNIP=pOGRNIP, INN=pINN
		, CorrespondenceAddress=pCorrespondenceAddress
		, Revision=pRevision
		;
	else
		update efrsb_manager
		set ArbitrManagerID=pArbitrManagerID
		, SRORegNum=pSRORegNum, SRORegDate=pSRORegDate, RegNum=pRegNum
		, FirstName=pFirstName, MiddleName=pMiddleName, LastName=pLastName
		, OGRNIP=pOGRNIP, INN=pINN
		, CorrespondenceAddress=pCorrespondenceAddress
		, Revision=pRevision
		where id_Manager=pid_Manager;
	end if;

END//
DELIMITER ;

drop function if exists `short_fio`;
DELIMITER //
CREATE FUNCTION `short_fio`(LastName text, FistName text, MiddleName text) RETURNS text CHARSET utf8
    DETERMINISTIC
begin
  return concat(LastName
  	, if(FistName   IS NULL || 0=length(FistName),   '', concat(' ', substr(FistName,   1, 1), '.'))
  	, if(MiddleName IS NULL || 0=length(MiddleName), '', concat(' ', substr(MiddleName, 1, 1), '.')));
end//
DELIMITER ;