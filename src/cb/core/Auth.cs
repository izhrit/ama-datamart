﻿using System;
using System.Collections.Generic;

using log4net;

namespace rit.cb
{
    public static class Auth
    {
        static ILog log = LogManager.GetLogger(typeof(Auth));

        public class Settings
        {
            public string auth;
            public virtual DateTime GetCurrentTime() { return DateTime.Now; }
            public virtual void Use() { }
        }

        public class Debug_settings : Settings
        {
            public DateTime start_time;
            public DateTime current_time;
            public override void Use()
            {
                current_time = start_time;
            }
            public override DateTime GetCurrentTime()
            {
                DateTime res = current_time;
                current_time = current_time.AddSeconds(1);
                return res;
            }
        }

        static IDictionary<string, Settings> settings= null;

        static Settings ParseSettings(string auth_part)
        {
            string[] options_parts = auth_part.Split(':');
            if (null == options_parts || 0 == options_parts.Length)
            {
                return null;
            }
            else
            {
                string auth = options_parts[0];
                if (1 != options_parts.Length)
                {
                    string mode = options_parts[1];
                    if ("real" != mode)
                    {
                        DateTime start_time;
                        if (!DateTime.TryParse(mode,out start_time))
                            start_time = new DateTime(2020, 9, 18);
                        return new Debug_settings() { auth = auth, start_time= start_time };
                    }
                }
                return new Settings() { auth = auth };
            }
        }

        static IDictionary<string, Settings> ReadSettings()
        {
            IDictionary<string, Settings> read_settings = new Dictionary<string, Settings>();

            System.Collections.Specialized.NameValueCollection app_settings = System.Configuration.ConfigurationManager.AppSettings;

            string app_settings_auth_string = app_settings["auth"];
            if (!string.IsNullOrEmpty(app_settings_auth_string))
            {
                string[] auth_parts = app_settings_auth_string.Split(';');
                foreach (string auth_part in auth_parts)
                {
                    Settings auth_settings = ParseSettings(auth_part);
                    if (null!= auth_settings)
                        read_settings.Add(auth_settings.auth, auth_settings);
                }
            }
            return read_settings;
        }

        static void TraceSettings()
        {
            log.Error("settings:");
            foreach (Settings auth_settings in settings.Values)
            {
                Debug_settings debug_auth_settings = auth_settings as Debug_settings;
                if (null== debug_auth_settings)
                {
                    log.ErrorFormat("    auth:\"{0}\"",auth_settings.auth);
                }
                else
                {
                    log.ErrorFormat("    auth:\"{0}\", time:{1}", auth_settings.auth, debug_auth_settings.start_time);
                }
            }
        }

        static void SafeReadSettings()
        {
            if (null == settings)
            {
                settings = ReadSettings();
                TraceSettings();
            }
        }

        public static Settings Check(string auth)
        {
            log.Debug("check:" + auth);
            SafeReadSettings();
            Settings auth_settings = null;
            if (!settings.TryGetValue(auth, out auth_settings))
                throw new Exception(string.Format("can not authorize auth:\"{0}\"",auth));
            auth_settings.Use();
            return auth_settings;
        }
    }
}
