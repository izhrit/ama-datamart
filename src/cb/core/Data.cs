﻿using System;
using System.Linq;
using System.Collections.Generic;

using log4net;

namespace rit.cb
{
    public class Debtor
    {
        public bool isCitizen = true;
        public string Name;
        public string INN;
        public string OGRN;
        public string SNILS;

        public bool isSearchableSide()
        {
            return !string.IsNullOrEmpty(Name);
        }

        public CaseBook.CaseSide CaseSide()
        {
            CaseBook.CaseSide side = new CaseBook.CaseSide();
            side.Name = Name;
            side.Inn = INN;
            side.Ogrn = OGRN;
            side.ShortName = Name;
            side.Type = 1;
            return side;
        }
    }

    public class Case_SearchConditions
    {
        static ILog log = LogManager.GetLogger(typeof(Case_SearchConditions));

        public string id;
        public Debtor debtor;
        public string CourtTag;

        public Case_SearchConditions() { }

        DateTime GetCurrentTime(int current_unix_time)
        {
            if (0 == current_unix_time)
            {
                return DateTime.Now;
            }
            else
            {
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(current_unix_time).ToLocalTime();
                return dtDateTime;
            }
        }

        public CaseBook.SearchCaseArbitrRequest Request(int current_unix_time, bool test_mode)
        {
            CaseBook.SearchCaseArbitrRequest req = new CaseBook.SearchCaseArbitrRequest();
            //req.Count = 20; // too large
            //req.Count = 5; // ok
            //req.Count = 12; // ok
            //req.Count = 16; // ok
            //req.Count = 18; // too large
            //req.Count = 17; // ok
            req.Count = 15;
            req.Sides = new CaseBook.CaseSide[] { debtor.CaseSide() };
            req.Accuracy = CaseBook.AccuracyMode.All;
            // does not work:req.CaseTypeCodes = new CaseBook.CaseType? [] { !cc.debtor.isCitizen ? CaseBook.CaseType.Bankrupt : CaseBook.CaseType.BankruptCitizen };

            if (!test_mode)
                req.IsActive= true;
            DateTime now = GetCurrentTime(current_unix_time);

            req.StartDateFrom = now.AddDays(-30);
            req.StartDateTo = now.AddDays(1);

            /*req.StartDateFrom = now.AddDays(-90);
            req.StartDateTo = now.AddDays(90);*/

            return req;
        }

        string fix_snils(string snils)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (char c in snils)
            {
                if (Char.IsDigit(c))
                    sb.Append(c);
            }
            return sb.ToString();
        }

        public bool match(CaseBook.ShortCaseInfo cinfo)
        {
            if (CourtTag != cinfo.CourtTag)
            {
                log.ErrorFormat("       (CourtTag != cinfo.CourtTag) - {0} != {1}", CourtTag, cinfo.CourtTag);
                return false;
            }
            if (debtor.isCitizen)
            {
                if (CaseBook.CaseType.BankruptCitizen != cinfo.CaseTypeCode)
                {
                    log.ErrorFormat("       debtor.isCitizen but cinfo.CaseTypeCode={0}", cinfo.CaseTypeCode);
                    return false;
                }
            }
            else
            {
                if (CaseBook.CaseType.Bankrupt != cinfo.CaseTypeCode)
                {
                    log.ErrorFormat("       !debtor.isCitizen but cinfo.CaseTypeCode={0}", cinfo.CaseTypeCode);
                    return false;
                }
            }
            if (!string.IsNullOrEmpty(debtor.SNILS))
            {
                string snils = fix_snils(cinfo.Bancruptcy.CitizenSnils);
                if (debtor.SNILS != snils)
                {
                    log.ErrorFormat("       (debtor.SNILS != cinfo.Bancruptcy.CitizenSnils) - {0} != {1}", debtor.SNILS, snils);
                    return false;
                }
                
            }
            return true;
        }
    }

    public class Side
    {
        public bool isCitizen = true;
        public string Name;
        public string INN;
        public string OGRN;
        public string SNILS;
        public string Address;

        public Side() { }
        public Side(CaseBook.CaseCardSide cbside)
        {
            isCitizen = CaseBook.LegalStatus.Person == cbside.LegalStatus.Value ||
                    CaseBook.LegalStatus.IndividualBusinessman == cbside.LegalStatus.Value;
            Name = cbside.Name;
            OGRN = cbside.Ogrn;
            INN = cbside.Inn;
            Address = cbside.Address;
        }
        public Side(CaseBook.CaseSide cbside)
        {
            isCitizen = CaseBook.LegalStatus.Person == cbside.LegalStatus.Value ||
                    CaseBook.LegalStatus.IndividualBusinessman == cbside.LegalStatus.Value;
            Name = cbside.Name;
            OGRN = cbside.Ogrn;
            INN = cbside.Inn;
            Address = cbside.Address;
        }
        public Side(Side from)
        {
            CopyFromTo(from,this);
        }

        static public void CopyFromTo(Side from, Side to)
        {
            to.isCitizen= from.isCitizen;
            to.Name = from.Name;
            to.INN = from.INN;
            to.OGRN = from.OGRN;
            to.SNILS = from.SNILS;
            to.Address = from.Address;
        }
    }

    public class Case_Details
    {
        public string CaseNumber;
        public string NextSessionDate;

        public Side [] Debtors;
        public Side Applicant;

        public void SetNextSessionDate(DateTime dt)
        {
            NextSessionDate= dt.ToString("dd.MM.yyyy HH:mm");
        }

        public Case_Details(){}
        public Case_Details(CaseBook.CaseCard card)
        {
            CaseNumber = card.Number;

            FillBySides(card.Sides);

            DateTime minNextSessionDate= DateTime.MaxValue;
            foreach (CaseBook.CaseCardInstance instance in card.Instances)
            {
                if (null != instance.NextSessionDate && instance.NextSessionDate < minNextSessionDate)
                    minNextSessionDate = instance.NextSessionDate.Value;
                if (null==Applicant && CaseNumber == instance.InstanceNumber && 
                    null!= instance.StartDocument && null!= instance.StartDocument.Declarers &&
                    0!= instance.StartDocument.Declarers.Length)
                {
                    string [] declarers = instance.StartDocument.Declarers;
                    Side debtor_reclarer = Debtors.FirstOrDefault( d => { return declarers.Contains(d.Name); });
                    if (null != debtor_reclarer)
                        Applicant = new Side(debtor_reclarer);
                }
            }
            if (minNextSessionDate < DateTime.MaxValue)
                SetNextSessionDate(minNextSessionDate);
        }

        public void FillBySides(CaseBook.CaseCardSide[] sides)
        {
            List<Side> debtors = null;
            List<Side> defendands= null;
            foreach (CaseBook.CaseCardSide cbside in sides)
            {
                switch (cbside.Category)
                {
                    case "Истец":
                    case "Заявитель":
                        Applicant = new Side(cbside);
                        break;
                    case "Должник":
                        if (null == debtors)
                            debtors = new List<Side>();
                        debtors.Add(new Side(cbside));
                        break;
                    case "Ответчик":
                        if (null == debtors)
                            defendands = new List<Side>();
                        defendands.Add(new Side(cbside));
                        break;
                }
            }
            if (null != debtors)
            {
                Debtors = debtors.ToArray();
            }
            else if (null != defendands)
            {
                Debtors = defendands.ToArray();
            }
        }
    }

    public class Case_Details_for_id : Case_Details
    {
        public string id;
        public Case_Details_for_id(string id, CaseBook.ShortCaseInfo cinfo)
        {
            this.id = id;
            CaseNumber = cinfo.Number;
            if (null != cinfo.NextSession)
                SetNextSessionDate(cinfo.NextSession.Date);

            FillBySides(cinfo.Sides);

            if (null!=cinfo.Bancruptcy)
            {
                rit.CaseBook.BancruptcyDetails b = cinfo.Bancruptcy;
                Side side = new Side(b.Debtor);
                if (!string.IsNullOrEmpty(b.CitizenSnils))
                    side.SNILS = b.CitizenSnils;
                Debtors = new Side[] { side };
            }
        }

        public void FillBySides(CaseBook.CaseSide[] sides)
        {
            List<Side> debtors = null;
            List<Side> defendands = null;
            foreach (CaseBook.CaseSide cbside in sides)
            {
                switch (cbside.Category)
                {
                    case "Истец":
                    case "Заявитель":
                        Applicant = new Side(cbside); break;
                    case "Должник":
                        if (null == debtors)
                            debtors = new List<Side>();
                        debtors.Add(new Side(cbside));
                        break;
                    case "Ответчик":
                        if (null == debtors)
                            defendands = new List<Side>();
                        defendands.Add(new Side(cbside));
                        break;
                }
            }
            if (null != debtors)
            {
                Debtors = debtors.ToArray();
            }
            else if (null != defendands)
            {
                Debtors = defendands.ToArray();
            }
        }
    }
}
