﻿namespace rit.cb
{
    public interface IService
    {
        Case_Details_for_id[] FindOutDetailsOfTheCases
            (
            string auth_token
            , Case_SearchConditions[] cases
            , int current_unix_time= 0
            );
        Case_Details [] GetCaseDetails
            (
            string auth_token
            , string [] CaseNumbers
            ); 
    }
}
