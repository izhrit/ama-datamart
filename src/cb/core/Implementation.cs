﻿using System;
using System.Collections.Generic;

using log4net;

namespace rit.cb
{
    public class ServiceImplementation : IService
    {
        static ILog log = LogManager.GetLogger(typeof(ServiceImplementation));

        public static CaseBook.WebServiceClient PrepareCaseBookService(out bool test_mode)
        {
            System.Collections.Specialized.NameValueCollection app_settings = System.Configuration.ConfigurationManager.AppSettings;

            CaseBook.WebServiceClient client = new CaseBook.WebServiceClient();

            System.ServiceModel.Security.UserNamePasswordClientCredential credentials = client.ClientCredentials.UserName;
            credentials.UserName = app_settings["casebook_UserName"];
            credentials.Password = app_settings["casebook_Password"];

            test_mode = ("true" == app_settings["test_mode"]); // false;

            log.ErrorFormat("test_mode={0}", test_mode);
            log.Error("credentials.UserName=" + credentials.UserName);
            log.Error("credentials.Password=" + new String('*', credentials.Password.Length));

            return client;
        }

        static void Trace(int i, CaseBook.ShortCaseInfo cinfo)
        {
            log.ErrorFormat("    {0}) CaseTypeCode={1}, CourtTag={2}, IsActive={3}", i, cinfo.CaseTypeCode, cinfo.CourtTag, cinfo.IsActive);
            if (CaseBook.CaseType.Bankrupt==cinfo.CaseTypeCode || CaseBook.CaseType.BankruptCitizen == cinfo.CaseTypeCode)
            {
                log.ErrorFormat("         RegistrationDate={0}", cinfo.RegistrationDate);
                foreach (CaseBook.CaseSide side in cinfo.Sides)
                    log.ErrorFormat("         side:{0}", side.Name);
                if (null != cinfo.Bancruptcy)
                    log.ErrorFormat("      cinfo.Bancruptcy.CitizenSnils={0}", cinfo.Bancruptcy.CitizenSnils);
            }
        }

        static public Case_Details_for_id FindDetailsInPagedResult(Case_SearchConditions cc, CaseBook.PagedResultOfShortCaseInfoNcCATIYq responce)
        {
            log.ErrorFormat("  responce.TotalCount={0}", responce.TotalCount);
            int j = 0;
            foreach (CaseBook.ShortCaseInfo cinfo in responce.Items)
            {
                Trace(j++, cinfo);
                if (cc.match(cinfo))
                    return new Case_Details_for_id(cc.id, cinfo);
            }
            return null;
        }

        public Case_Details_for_id FindOutDetailsOfTheCases(CaseBook.WebServiceClient client, Case_SearchConditions cc, int current_unix_time, bool test_mode)
        {
            if (null == cc.debtor || !cc.debtor.isSearchableSide())
            {
                return null;
            }
            else
            {
                CaseBook.SearchCaseArbitrRequest req = cc.Request(current_unix_time,test_mode);
                CaseBook.PagedResultOfShortCaseInfoNcCATIYq responce = null;
                try
                {
                    log.ErrorFormat("current_unix_time={0}", current_unix_time);
                    log.ErrorFormat("    request casebook SearchCaseArbitr ({0}-{1}, {2} from {3} to {4}).."
                        ,req.Page* req.Count, (req.Page+1) * req.Count+1 - 1
                        ,((null==req.IsActive? false : req.IsActive.Value) ? "active" : "any")
                        , null==req.StartDateFrom ? "?" : req.StartDateFrom.Value.ToShortDateString()
                        , null==req.StartDateTo ? "?" : req.StartDateTo.Value.ToShortDateString());
                    responce = client.SearchCaseArbitr(req);
                    log.Error("    .. got responce from casebook SearchCaseArbitr");
                }
                catch (Exception ex)
                {
                    log.Error("    .. can not SearchCaseArbitr!", ex);
                }
                if (null != responce)
                {
                    return FindDetailsInPagedResult(cc, responce);
                }
                else
                {
                    log.Error("  responce of SearchCaseArbitr is null");
                    return null;
                }
            }
        }

        public Case_Details_for_id[] FindOutDetailsOfTheCases(string auth_token, Case_SearchConditions[] cases, int current_unix_time= 0)
        {
            Auth.Check(auth_token);
            List<Case_Details_for_id> details = new List<Case_Details_for_id>();
            bool test_mode;
            CaseBook.WebServiceClient client = PrepareCaseBookService(out test_mode);
            for (int i= 0; i < cases.Length; i++)
            {
                Case_SearchConditions cc= cases[i];
                log.ErrorFormat("{0}. id={1}, debtor.Name={2}, CourtTag={3}", i,cc.id,cc.debtor.Name,cc.CourtTag);
                if (null != cc.debtor && cc.debtor.isSearchableSide())
                {
                    Case_Details_for_id cd = FindOutDetailsOfTheCases(client, cc, current_unix_time, test_mode);
                    if (null != cd)
                        details.Add(cd);
                }
            }
            return details.ToArray();
        }

        public Case_Details [] GetCaseDetails(string auth_token, string [] CaseNumbers)
        {
            Auth.Check(auth_token);
            List<Case_Details> details = new List<Case_Details>();
            bool test_mode;
            CaseBook.WebServiceClient client= PrepareCaseBookService(out test_mode);
            CaseBook.CaseCardRequest req = new CaseBook.CaseCardRequest();
            for (int i = 0; i < CaseNumbers.Length; i++)
            {
                req.CaseNumber = CaseNumbers[i];
                CaseBook.CaseCard card= client.GetCaseCard(req);
                details.Add(new Case_Details(card));
            }
            return details.ToArray();
        }
    }
}
