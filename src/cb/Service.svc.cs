﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

using log4net;

using rit.cb;

namespace rit.cbservice
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Service : IService
    {
        static ILog log = LogManager.GetLogger(typeof(Service));

        ServiceImplementation implementation = new ServiceImplementation();

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
         UriTemplate = "/FindOutDetailsOfTheCases", ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public Case_Details_for_id[] FindOutDetailsOfTheCases(string auth_token, Case_SearchConditions[] cases, int current_unix_time = 0)
        {
            log.DebugFormat("FindOutDetailsOfTheCases {{ current_unix_time={0}", current_unix_time);
            try
            {
                return implementation.FindOutDetailsOfTheCases(auth_token, cases, current_unix_time);
            }
            catch (Exception ex)
            {
                log.Error("FindOutDetailsOfTheCases throw exception:", ex);
                throw;
            }
            finally
            {
                log.Debug("FindOutDetailsOfTheCases }");
            }
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
         UriTemplate = "/GetCaseDetails", ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public Case_Details[] GetCaseDetails(string auth_token, string [] CaseNumbers)
        {
            log.Debug("GetCaseDetails {{");
            try
            {
                return implementation.GetCaseDetails(auth_token, CaseNumbers);
            }
            catch (Exception ex)
            {
                log.Error("GetCaseDetails throw exception:", ex);
                throw;
            }
            finally
            {
                log.Debug("GetCaseDetails }");
            }
        }
    }
}
