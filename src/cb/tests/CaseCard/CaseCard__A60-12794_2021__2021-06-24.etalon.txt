﻿<?xml version="1.0" encoding="utf-8"?>
<CaseCard xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
  <BankruptInfo>
    <BankruptStage>Реализация имущества гражданина</BankruptStage>
    <DocumentId>54403965-498d-4310-97cf-5efd20330b64</DocumentId>
    <RegistryCloseDate>2021-08-19T00:00:00</RegistryCloseDate>
    <Timeframe i:nil="true" />
  </BankruptInfo>
  <CaseCategory>о несостоятельности (банкротстве) физических лиц</CaseCategory>
  <CaseStage>Рассматривается в первой инстанции</CaseStage>
  <CaseType>Банкротство</CaseType>
  <CaseTypeCode>BankruptCitizen</CaseTypeCode>
  <CaseTypeMCode>Б</CaseTypeMCode>
  <ClaimSum>1887609.9600000002</ClaimSum>
  <FinishState>0</FinishState>
  <Id>6c059ab3-6242-4e6f-a33b-da7e10b6f789</Id>
  <Instances>
    <CaseCardInstance>
      <CourtName>АС Свердловской области</CourtName>
      <CourtTag>EKATERINBURG</CourtTag>
      <FinalDocument>
        <AppealDate>2021-05-28T00:00:00</AppealDate>
        <AppealState>5</AppealState>
        <ContentTypes xmlns:d5p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
          <d5p1:string>Решение о признании гражданина банкротом и введении реализации имущества</d5p1:string>
        </ContentTypes>
        <Date>2021-04-27T00:00:00</Date>
        <Id>54403965-498d-4310-97cf-5efd20330b64</Id>
        <Type>Решения и постановления</Type>
      </FinalDocument>
      <Id>4f91bde9-4510-4d28-a9e0-0a95035780e6</Id>
      <InstanceLevel>1</InstanceLevel>
      <InstanceNumber>А60-12794/2021</InstanceNumber>
      <Judge>Дурановский А. А.</Judge>
      <JudgeId>c5aa43d4-ece5-4bdf-adc5-60c4a87a1879</JudgeId>
      <NextSessionDate>2021-07-12T09:37:00</NextSessionDate>
      <NextSessionDateUtc>2021-07-12T04:37:00</NextSessionDateUtc>
      <NextSessionPlace>зал № 304</NextSessionPlace>
      <StartDate>2021-03-22T00:00:00</StartDate>
      <StartDocument>
        <ApeealedType i:nil="true" />
        <Declarers xmlns:d5p1="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
          <d5p1:string>Афлетунов Альберт Юрисович</d5p1:string>
          <d5p1:string>Афлетунов Альберт Юрисович</d5p1:string>
        </Declarers>
        <DocType>Заявление о признании гражданина (индивидуального предпринимателя) банкротом</DocType>
      </StartDocument>
    </CaseCardInstance>
  </Instances>
  <IsSimpleJustice>false</IsSimpleJustice>
  <Number>А60-12794/2021</Number>
  <RecoverySum>0</RecoverySum>
  <RegistrationDate>2021-03-22T00:00:00</RegistrationDate>
  <ResultCode>-1</ResultCode>
  <Sides>
    <CaseCardSide>
      <Address>623702, Россия, Свердловская область, г. Березовский, ул. МАЯКОВСКОГО, д. 44, кв. 2</Address>
      <Id>e059fd90-2f9c-4bcf-97e5-5500a501fd32</Id>
      <Inn>667480088137</Inn>
      <LegalStatus>Person</LegalStatus>
      <Name>Афлетунов Альберт Юрисович</Name>
      <Ogrn i:nil="true" />
      <Okpo i:nil="true" />
      <Type>Defendants</Type>
      <Category>Должник</Category>
    </CaseCardSide>
    <CaseCardSide>
      <Address>Данные скрыты</Address>
      <Id>4fb2a8f8-376b-46bd-a438-7e6561640f50</Id>
      <Inn>667221004762</Inn>
      <LegalStatus>Person</LegalStatus>
      <Name>Булатов  Роман  Сергеевич</Name>
      <Ogrn i:nil="true" />
      <Okpo i:nil="true" />
      <Type>Others</Type>
      <Category>Арбитражный управляющий</Category>
    </CaseCardSide>
    <CaseCardSide>
      <Address>Данные скрыты</Address>
      <Id>873b0715-1404-4938-b11a-b29979e5cb44</Id>
      <Inn>660402243664</Inn>
      <LegalStatus>Person</LegalStatus>
      <Name>Афлетунов Альберт Юрисович</Name>
      <Ogrn i:nil="true" />
      <Okpo i:nil="true" />
      <Type>Others</Type>
      <Category>Кредитор</Category>
    </CaseCardSide>
    <CaseCardSide>
      <Address>633011, Новосибирская область, г Бердск, ул Попова, 11</Address>
      <Id>030c4bc8-72bc-4eeb-a707-6438b72b6494</Id>
      <Inn>4401116480</Inn>
      <LegalStatus>Organization</LegalStatus>
      <Name>ФИЛИАЛ "ЦЕНТРАЛЬНЫЙ" ПУБЛИЧНОГО АКЦИОНЕРНОГО ОБЩЕСТВА "СОВКОМБАНК"</Name>
      <Ogrn>1144400000425</Ogrn>
      <Okpo>60843118</Okpo>
      <Type>Plaintiffs</Type>
      <Category>Кредитор</Category>
    </CaseCardSide>
    <CaseCardSide>
      <Address>603136, Россия, 6, г. Нижний Новгород, ул. АКАДЕМИКА САХАРОВА, д. 2 литера А</Address>
      <Id>e3799677-ed89-4e54-8ad8-36a451486067</Id>
      <Inn>7707083893</Inn>
      <LegalStatus>Organization</LegalStatus>
      <Name>ПАО СБЕРБАНК</Name>
      <Ogrn>1027700132195</Ogrn>
      <Okpo i:nil="true" />
      <Type>Plaintiffs</Type>
      <Category>Кредитор</Category>
    </CaseCardSide>
    <CaseCardSide>
      <Address>Данные скрыты</Address>
      <Id>18b7ebf6-0395-4f76-a4fe-2c35906e27d7</Id>
      <Inn>667480088137</Inn>
      <LegalStatus>Person</LegalStatus>
      <Name>Афлетунова Наталья Борисовна</Name>
      <Ogrn i:nil="true" />
      <Okpo i:nil="true" />
      <Type>Third</Type>
      <Category>Третье лицо</Category>
    </CaseCardSide>
    <CaseCardSide>
      <Address>105062, ГОРОД МОСКВА, ПЕРЕУЛОК ПОДСОСЕНСКИЙ, Д.  30 КОРПУС СТРОЕНИЕ  3</Address>
      <Id>cbf0f670-c449-4f0e-9636-52b8c588eed6</Id>
      <Inn>7710480611</Inn>
      <LegalStatus>Organization</LegalStatus>
      <Name>АССОЦИАЦИЯ "НАЦИОНАЛЬНАЯ ОРГАНИЗАЦИЯ АРБИТРАЖНЫХ УПРАВЛЯЮЩИХ"</Name>
      <Ogrn>1137799006840</Ogrn>
      <Okpo>17343218</Okpo>
      <Type>Others</Type>
      <Category>СРО</Category>
    </CaseCardSide>
  </Sides>
  <CaseResultProbabilitiesInfo i:nil="true" />
  <CaseDurationProbabilitiesInfo i:nil="true" />
</CaseCard>
