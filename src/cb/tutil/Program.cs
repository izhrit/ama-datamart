﻿using System;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;

using log4net;

using rit.cb;

namespace rit.cb
{
    class Program
    {
        static ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            log4net.Util.LogLog.QuietMode = true;
            log4net.Config.XmlConfigurator.Configure();
            Console.OutputEncoding = Encoding.UTF8;
            try
            {
                ProcessArgs(args);
                switch (method)
                {
                    case "FindOutDetailsOfTheCases": test_FindOutDetailsOfTheCases(); break;
                    case "GetCaseDetails": test_GetCaseDetails(); break;

                    case "w_CaseCardRequest": test_w_CaseCardRequest(); break;
                    case "rw_CaseCardRequest": test_rw_CaseCardRequest(); break;

                    case "w_CaseCard": test_w_CaseCard(); break;
                    case "rw_CaseCard": test_rw_CaseCard(); break;

                    case "cb_GetCaseCard_by_CaseNumber": test_cb_GetCaseCard_by_CaseNumber(); break;

                    case "CaseCard_to_CaseDetails": test_CaseCard_to_CaseDetails(); break;

                    case "w_SearchCaseArbitrRequest": test_w_SearchCaseArbitrRequest(); break;
                    case "rw_SearchCaseArbitrRequest": test_rw_SearchCaseArbitrRequest(); break;

                    case "w_PagedResultOfShortCaseInfo": test_w_PagedResultOfShortCaseInfo(); break;
                    case "rw_PagedResultOfShortCaseInfo": test_rw_PagedResultOfShortCaseInfo(); break;

                    case "w_Case_SearchConditions": test_w_Case_SearchConditions(); break;
                    case "rw_Case_SearchConditions": test_rw_Case_SearchConditions(); break;

                    case "Case_SearchConditions_to_SearchCaseArbitrRequest": test_Case_SearchConditions_to_SearchCaseArbitrRequest(); break;

                    case "cb_SearchCaseArbitr": test_cb_SearchCaseArbitr(); break;

                    case "FindDetailsInPagedResult": test_FindDetailsInPagedResult(); break;

                    case "Extract_finish_court": test_Extract_finish_court(); break;
                    case "Extract_appointment": test_Extract_appointment(); break;
                    case "Extract_start": test_Extract_start(); break;
                    case "Extract_procedure_duration": test_Extract_procedure_duration(); break;

                    default: Console.WriteLine("unknown method: \"{0}\"", method); break;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        static void test_rw_CaseCardRequest()
        {
            rit.CaseBook.CaseCardRequest req = Storer.Restore_CaseCardRequest(args_file_path);
            req.Store(Console.Out);
        }

        static void test_rw_SearchCaseArbitrRequest()
        {
            rit.CaseBook.SearchCaseArbitrRequest req = Storer.Restore_SearchCaseArbitrRequest(args_file_path);
            req.Store(Console.Out);
        }

        static void test_rw_PagedResultOfShortCaseInfo()
        {
            rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq res = Storer.Restore_PagedResultOfShortCaseInfo(args_file_path);
            res.Store(Console.Out);
        }

        static void test_rw_CaseCard()
        {
            rit.CaseBook.CaseCard card = Storer.Restore_CaseCard(args_file_path);
            card.Store(Console.Out);
        }

        static void test_w_SearchCaseArbitrRequest()
        {
            rit.CaseBook.SearchCaseArbitrRequest req = new rit.CaseBook.SearchCaseArbitrRequest();
            req.Store(Console.Out);
        }

        static void test_w_Case_SearchConditions()
        {
            Case_SearchConditions c = new Case_SearchConditions();
            c.Store(Console.Out);
        }

        static void test_rw_Case_SearchConditions()
        {
            Case_SearchConditions c = Storer.Restore_Case_SearchConditions(args_file_path);
            c.Store(Console.Out);
        }

        static void test_w_CaseCardRequest()
        {
            rit.CaseBook.CaseCardRequest req = new rit.CaseBook.CaseCardRequest();
            req.CaseNumber = "test case number";
            req.Store(Console.Out);
        }

        static void test_w_PagedResultOfShortCaseInfo()
        {
            rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq res = new rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq();
            res.Store(Console.Out);
        }

        static void test_w_CaseCard()
        {
            rit.CaseBook.CaseCard card = new rit.CaseBook.CaseCard();
            card.Store(Console.Out);
        }

        static void test_cb_GetCaseCard_by_CaseNumber()
        {
            if (string.IsNullOrEmpty(case_number))
            {
                Console.WriteLine("use non empty argument --case-number= !");
            }
            else
            {
                rit.CaseBook.CaseCardRequest req = new rit.CaseBook.CaseCardRequest();
                req.CaseNumber = case_number;

                bool test_mode;
                rit.CaseBook.WebServiceClient client = rit.cb.ServiceImplementation.PrepareCaseBookService(out test_mode);
                rit.CaseBook.CaseCard card = client.GetCaseCard(req);

                string fixed_case_number = case_number.Replace("/", "_").Replace("А", "A");
                string fname = string.Format("CaseCard__{0}__{1}.etalon.txt", fixed_case_number, DateTime.Now.ToString("yyyy-MM-dd"));
                using (FileStream fs = new FileStream(fname, FileMode.Create, FileAccess.Write))
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    card.Store(writer);
                }
            }
        }

        static void test_cb_SearchCaseArbitr()
        {
            rit.CaseBook.SearchCaseArbitrRequest req = Storer.Restore_SearchCaseArbitrRequest(args_file_path);

            Console.Out.WriteLine("StartDateFrom:{0}", req.StartDateFrom);
            Console.Out.WriteLine("StartDateTo:{0}", req.StartDateTo);
            foreach (rit.CaseBook.CaseSide side in req.Sides)
            {
                Console.Out.WriteLine("side:");
                Console.Out.WriteLine("  Name=\"{0}\"", side.Name);
            }

            bool test_mode;
            rit.CaseBook.WebServiceClient client = rit.cb.ServiceImplementation.PrepareCaseBookService(out test_mode);

            rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq res= client.SearchCaseArbitr(req);

            string base_name = Path.GetFileNameWithoutExtension(args_file_path);
            string bad_suffix = ".etalon";
            if (base_name.EndsWith(bad_suffix))
                base_name = base_name.Substring(0, base_name.Length - bad_suffix.Length);
            string fname = string.Format("psc__{0}__{1}.etalon.xml", base_name, DateTime.Now.ToString("yyyy-MM-dd"));

            using (FileStream fs = new FileStream(fname, FileMode.Create, FileAccess.Write))
            using (StreamWriter writer = new StreamWriter(fs))
            {
                res.Store(writer);
            }
        }

        static void test_CaseCard_to_CaseDetails()
        {
            rit.CaseBook.CaseCard card = Storer.Restore_CaseCard(args_file_path);
            Case_Details details = new Case_Details(card);
            Trace(details);
        }

        static void test_Case_SearchConditions_to_SearchCaseArbitrRequest()
        {
            Case_SearchConditions c = Storer.Restore_Case_SearchConditions(args_file_path);
            rit.CaseBook.SearchCaseArbitrRequest req = c.Request(current_unix_time, test_mode);
            req.Store(Console.Out);
        }

        static public void Trace(Side p)
        {
            if (null==p)
            {
                Console.WriteLine("        null");
            }
            else
            {
                Console.WriteLine("        isCitizen:{0}", p.isCitizen);
                Console.WriteLine("        Name:\"{0}\"", p.Name);
                Console.WriteLine("        INN:\"{0}\"", p.INN);
                Console.WriteLine("        OGRN:\"{0}\"", p.OGRN);
                Console.WriteLine("        SNILS:\"{0}\"", p.SNILS);
                Console.WriteLine("        Address:\"{0}\"", p.Address);
            }
        }

        static public void Trace(Side [] sides)
        {
            if (null==sides)
            {
                Console.WriteLine("        null");
            }
            else
            {
                foreach (Side s in sides)
                {
                    Console.WriteLine("    .");
                    Trace(s);
                }
            }
        }

        static public void Trace(Case_Details d)
        {
            if (null==d)
            {
                Console.WriteLine("null");
            }
            else
            {
                Case_Details_for_id with_id= d as Case_Details_for_id;
                if (null== with_id)
                {
                    Console.WriteLine("--------------------");
                }
                else
                {
                    Console.WriteLine("-----id={0}---------",with_id.id);
                }
                Console.WriteLine("    CaseNumber=\"{0}\"", d.CaseNumber);
                Console.WriteLine("    NextSessionDate=\"{0}\"", d.NextSessionDate);
                Console.WriteLine("    Debtors:");
                Trace(d.Debtors);
                Console.WriteLine("    Applicant:");
                Trace(d.Applicant);
            }
        }

        static public void Trace(Case_Details[] details)
        {
            foreach (Case_Details d in details)
                Trace(d);
        }

        public class args_GetCaseDetails
        {
            public string auth_token= null;
            public string[] CaseNumbers= null;
        }
        static void test_GetCaseDetails()
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            string args_txt = File.ReadAllText(args_file_path);
            args_GetCaseDetails args= s.Deserialize<args_GetCaseDetails>(args_txt);
            IService srv = new rit.cb.ServiceImplementation();
            Case_Details[] details= srv.GetCaseDetails(args.auth_token,args.CaseNumbers);
            Trace(details);
        }

        static void test_FindDetailsInPagedResult()
        {
            Case_SearchConditions cc = Storer.Restore_Case_SearchConditions(args_file_path);
            rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq responce = Storer.Restore_PagedResultOfShortCaseInfo(args2_file_path);
            Case_Details_for_id res = rit.cb.ServiceImplementation.FindDetailsInPagedResult(cc, responce);
            Trace(res);
        }

        static void test_FindOutDetailsOfTheCases()
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            string args_txt = File.ReadAllText(args_file_path);
            Case_SearchConditions [] cases= s.Deserialize<Case_SearchConditions[]>(args_txt);
            IService srv = new rit.cb.ServiceImplementation();
            Case_Details_for_id[] details= srv.FindOutDetailsOfTheCases(auth_token, cases, current_unix_time);
            Trace(details);
        }

        static void test_extractor(IИзвлекатель extractor)
        {
            string txt = File.ReadAllText(args_file_path);
            Машинно_читаемые_выдержки_судебного_акта extract = new Машинно_читаемые_выдержки_судебного_акта();
            extractor.Извлечь(txt, new DateTime(2001, 7, 21), extract);
            extract.Store(Console.Out);
        }

        static void test_Extract_finish_court()
        {
            test_extractor(Извлекатель.Назначения_заседание_по_итогам_процедуры);
        }

        static void test_Extract_start()
        {
            test_extractor(Извлекатель.Введения_процедуры);
        }

        static void test_Extract_procedure_duration()
        {
            test_extractor(Извлекатель.Определения_длительности_процедуры);
        }

        static void test_Extract_appointment()
        {
            Извлекатель.Утверждения_АУ_должника.варианты_ау = new ФИО []
            {
                new ФИО("Мартос","Светлана","Борисовна")
                ,new ФИО("Подоляк","Сергей","Юрьевич")
            };
            test_extractor(Извлекатель.Утверждения_АУ_должника);
        }

        static string auth_token;
        static string method;
        static string args_file_path;
        static string args2_file_path;
        static string case_number;
        static int current_unix_time = 1623998051;
        static bool test_mode = true;

        static void ProcessCurrentTimeArg(string txt)
        {
            DateTime dt = DateTime.Parse(txt);
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            current_unix_time = (int)(dt.ToUniversalTime() - epoch).TotalSeconds;
        }

        static void ProcessArgs(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                string[] ap = args[i].Split('=');
                switch (ap[0])
                {
                    case "--method": method= ap[1]; break;
                    case "--args-from-file": args_file_path= ap[1]; break;
                    case "--args2-from-file": args2_file_path = ap[1]; break;
                    case "--case-number": case_number = ap[1].Replace("_","/"); break;
                    case "--current-time": ProcessCurrentTimeArg(ap[1]); break;
                    case "--auth": auth_token= ap[1]; break;
                }
            }
        }
    }
}
