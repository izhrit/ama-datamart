﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace ama_dm_ztest_cb
{
    public static class Test_storer
    {
        public static void Store(this rit.CaseBook.CaseCardRequest req, TextWriter writer)
        {
            DataContractJsonSerializer xm = new DataContractJsonSerializer(typeof(rit.CaseBook.CaseCardRequest));
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                xm.WriteObject(ms, req);
                ms.Flush();
                string result = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                writer.WriteLine(result);
            }
        }

        public static void Store(this rit.cb.Case_SearchConditions req, TextWriter writer)
        {
            DataContractJsonSerializer xm = new DataContractJsonSerializer(typeof(rit.cb.Case_SearchConditions));
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                xm.WriteObject(ms, req);
                ms.Flush();
                string result = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                writer.WriteLine(result);
            }
        }

        public static void Store(this rit.CaseBook.SearchCaseArbitrRequest req, TextWriter writer)
        {
            DataContractSerializer xm = new DataContractSerializer(typeof(rit.CaseBook.SearchCaseArbitrRequest));
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            using (System.Xml.XmlWriter xml_writer = System.Xml.XmlWriter.Create(ms, settings))
            {
                xm.WriteObject(xml_writer, req);
                xml_writer.Flush();
                ms.Flush();
                string result = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                writer.WriteLine(result);
            }
        }

        static System.Xml.XmlWriterSettings settings = new System.Xml.XmlWriterSettings { Indent = true };

        public static void Store(this rit.CaseBook.CaseCard card, TextWriter writer)
        {
            DataContractSerializer xm = new DataContractSerializer(typeof(rit.CaseBook.CaseCard));
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            using (System.Xml.XmlWriter xml_writer = System.Xml.XmlWriter.Create(ms, settings))
            {
                xm.WriteObject(xml_writer, card);
                xml_writer.Flush();
                ms.Flush();
                string result = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                writer.WriteLine(result);
            }
        }

        public static void Store(this rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq res, TextWriter writer)
        {
            DataContractSerializer xm = new DataContractSerializer(typeof(rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq));
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            using (System.Xml.XmlWriter xml_writer= System.Xml.XmlWriter.Create(ms,settings))
            {
                xm.WriteObject(xml_writer, res);
                xml_writer.Flush();
                ms.Flush();
                string result = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                writer.WriteLine(result);
            }
        }

        public static rit.CaseBook.CaseCardRequest Restore_CaseCardRequest(string filepath)
        {
            DataContractJsonSerializer xm = new DataContractJsonSerializer(typeof(rit.CaseBook.CaseCardRequest));
            using (FileStream s = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            {
                rit.CaseBook.CaseCardRequest res = (rit.CaseBook.CaseCardRequest)xm.ReadObject(s);
                return res;
            }
        }

        public static rit.CaseBook.SearchCaseArbitrRequest Restore_SearchCaseArbitrRequest(string filepath)
        {
            DataContractSerializer xm = new DataContractSerializer(typeof(rit.CaseBook.SearchCaseArbitrRequest));
            using (FileStream s = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            {
                rit.CaseBook.SearchCaseArbitrRequest res = (rit.CaseBook.SearchCaseArbitrRequest)xm.ReadObject(s);
                return res;
            }
        }

        public static rit.cb.Case_SearchConditions Restore_Case_SearchConditions(string filepath)
        {
            DataContractJsonSerializer xm = new DataContractJsonSerializer(typeof(rit.cb.Case_SearchConditions));
            using (FileStream s = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            {
                rit.cb.Case_SearchConditions res = (rit.cb.Case_SearchConditions)xm.ReadObject(s);
                return res;
            }
        }

        public static rit.CaseBook.CaseCard Restore_CaseCard(string filepath)
        {
            DataContractSerializer xm = new DataContractSerializer(typeof(rit.CaseBook.CaseCard));
            using (FileStream s = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            {
                rit.CaseBook.CaseCard res = (rit.CaseBook.CaseCard)xm.ReadObject(s);
                return res;
            }
        }

        public static rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq Restore_PagedResultOfShortCaseInfo(string filepath)
        {
            DataContractSerializer xm = new DataContractSerializer(typeof(rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq));
            using (FileStream s = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            {
                rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq res = (rit.CaseBook.PagedResultOfShortCaseInfoNcCATIYq)xm.ReadObject(s);
                return res;
            }
        }
    }
}
