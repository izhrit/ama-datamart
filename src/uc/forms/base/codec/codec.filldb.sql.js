define([
	  'forms/base/codec/codec'
	, 'forms/base/codec/datetime/h_codec.datetime'
],
function (BaseCodec, h_codec_datetime)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.EncodeText = function (txt)
		{
			txt= txt.replace(/\r/g, '\\r');
			txt= txt.replace(/\n/g, '\\n');
			txt= txt.replace(/\"/g, '\\"');
			txt= txt.replace(/\'/g, '\\\'');
			return txt;
		}

		codec.EncodeObjectValue= function(value)
		{
			var json_txt = JSON.stringify(value, null, '  ');
			json_txt = this.EncodeText(json_txt);
			return 'compress(\'' + json_txt + '\')';
		}

		codec.EncodeStringValue= function(value,field_scheme)
		{
			switch (field_scheme)
			{
				case 'datetime': return '\'' + h_codec_datetime.ru_txt2txt_mysql().Encode(value) + '\'';
				case 'compressed': return 'compress(\'' + this.EncodeText(value) + '\')';
				default: return '\'' + this.EncodeText(value) + '\'';
			}
		}

		codec.EncodeValue= function(value,field_scheme)
		{
			var t = typeof value;
			switch (t)
			{
				case 'string': return this.EncodeStringValue(value, field_scheme);
				case 'boolean': return ((true == value) ? 1 : 0);
				case 'object': return this.EncodeObjectValue(value);
				default: return value;
			}
		}

		codec.Encode = function (data)
		{
			var db_scheme = !data['db scheme'] ? null : data['db scheme'];
			var res_txt_sql = 'set names utf8;\r\n\r\n';
			for (var table_name in data)
			{
				if ('db scheme' == table_name)
					break;
				var table_scheme = db_scheme!=null?!db_scheme[table_name] ? null : db_scheme[table_name]:null;
				if ('xml_mode' != table_name)
					res_txt_sql += "select 'insert into " + table_name + "' as '';\r\n";

				var table = data[table_name];
				var row_prefix = 'insert into `' + table_name + '`';
				for (var i = 0; i < table.length; i++)
				{
					var row = table[i];
					res_txt_sql += row_prefix;
					var j = 0;
					for (var field_name in row)
					{
						res_txt_sql += (0 != j) ? ', ' : ' set ';
						res_txt_sql += '`' + field_name + '`= '
							+ codec.EncodeValue(row[field_name], null == table_scheme ? null : table_scheme[field_name]);
						j++;
					}
					res_txt_sql += ';\r\n';
				}
				res_txt_sql += '\r\n';
			}
			return res_txt_sql;
		}

		return codec;
	}
});
