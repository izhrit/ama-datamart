define(function ()
{
	var RodRussianMonthes =
		[
			'Января'
			, 'Февраля'
			, 'Марта'
			, 'Апреля'
			, 'Мая'
			, 'Июня'
			, 'Июля'
			, 'Августа'
			, 'Сентября'
			, 'Октября'
			, 'Ноября'
			, 'Декабря'
		];
	var helper =
	{
		DatePickerOptions:
		{
			changeMonth: true
			, changeYear: true
			, dateFormat: 'dd.mm.yy'
			, autoclose: true
			, autoHide: true
		}

		, ParseRussianDate: function (txt_dd_mm_yyyy)
		{
			if (txt_dd_mm_yyyy.indexOf('T') != -1)
			{
				var parts = txt_dd_mm_yyyy.split('T');
				var txt_dd_mm_yyyy = parts[0];
			}
			if (txt_dd_mm_yyyy.indexOf('.') != -1)
			{
				var parts = txt_dd_mm_yyyy.split('.');
				var year = parts[2];
				var month = parts[1];
				var day = parts[0];
				return new Date(year, ('00' == month) ? 1 : month - 1, ('00'==day) ? 1 : day, 0, 0, 0);
			}
			else
			{
				var parts = txt_dd_mm_yyyy.split('-');
				var year = parts[0];
				var month = parts[1];
				var day = parts[2];
				return new Date(year, month - 1, day, 0, 0, 0);
			}
		}

		, ParseXsdDate: function (txt_yyyy_mm_dd)
		{
			if (txt_yyyy_mm_dd.indexOf('T') != -1)
			{
				var parts = txt_yyyy_mm_dd.split('T');
				var txt_yyyy_mm_dd = parts[0];
			}
			if (txt_yyyy_mm_dd.indexOf('.') != -1)
			{
				var parts = txt_yyyy_mm_dd.split('.');
				var year = parts[0];
				var month = parts[1];
				var day = parts[2];
				return new Date(year, month - 1, day, 0, 0, 0);
			}
			else
			{
				var parts = txt_yyyy_mm_dd.split('-');
				var year = parts[0];
				var month = parts[1];
				var day = parts[2];
				return new Date(year, month - 1, day, 0, 0, 0);
			}
		}

		, SafeGetParsedDatePart: function(txt,parse,get_date_part)
		{
			var date;
			try
			{
				date = parse(txt);
			}
			catch (ex)
			{
				return '';
			}
			return get_date_part(date);
		}

		, SafeGetRussianParsedDateYear: function (txt_dd_mm_yyyy)
		{
			var self= this;
			return this.SafeGetParsedDatePart(txt_dd_mm_yyyy,function(txt){return self.ParseRussianDate(txt)},function(date){return date.getFullYear();});
		}

		, SafeGetRussianParsedDateMonthNameRod: function (txt_dd_mm_yyyy)
		{
			var self= this;
			return this.SafeGetParsedDatePart(txt_dd_mm_yyyy, function (txt) { return self.ParseRussianDate(txt) }, function (date) { return self.RodRussianMonthNameByDate(date, txt_dd_mm_yyyy); });
		}

		, SafeGetRussianParsedDateMonthDay: function (txt_dd_mm_yyyy)
		{
			var self= this;
			return this.SafeGetParsedDatePart(txt_dd_mm_yyyy, function (txt) { return self.ParseRussianDate(txt) }
				, function (date)
			{
				if (txt_dd_mm_yyyy)
				{
					var parts = txt_dd_mm_yyyy.split('.');
					if (parts && null != parts && 0 < parts.length && '00' == parts[0])
					{
						return '';
					}
				}
				return date.getDate();
			});
		}

		, SafeGetParsedDateYear: function (txt_dd_mm_yyyy)
		{
			var self = this;
			return this.SafeGetParsedDatePart(txt_dd_mm_yyyy, function (txt) { return self.ParseXsdDate(txt) }, function (date) { return date.getFullYear(); });
		}

		, SafeGetParsedDateMonthNameRod: function (txt_dd_mm_yyyy)
		{
			var self = this;
			return this.SafeGetParsedDatePart(txt_dd_mm_yyyy, function (txt) { return self.ParseXsdDate(txt) }, function (date) { return self.RodRussianMonthNameByDate(date); });
		}

		, SafeGetParsedDateMonthDay: function (txt_dd_mm_yyyy)
		{
			var self = this;
			return this.SafeGetParsedDatePart(txt_dd_mm_yyyy, function (txt) { return self.ParseXsdDate(txt) }, function (date) { return date.getDate(); });
		}

		, RodRussianMonthNameByDate: function (d, txt_dd_mm_yyyy)
		{
			if (txt_dd_mm_yyyy)
			{
				var parts = txt_dd_mm_yyyy.split('.');
				if (parts && null != parts && 1 < parts.length && '00' == parts[1])
				{
					return '';
				}
			}
			return RodRussianMonthes[d.getMonth()];
		}

		, safeDateTime: function()
		{
			try
			{
				return app.cpw_Now();
			}
			catch (ex)
			{
				return new Date();
			}
		}

		, unixDateTimeStamp: function ()
		{
			return Math.round(+this.safeDateTime() / 1000);
		}

		, stringifyDateUTC: function(format,date)
		{
			var timezone = date.getTimezoneOffset();
			var z = timezone >= 0 ? '-' : '+';
			var pad = function (num)
			{
				var norm = Math.abs(Math.floor(num));
				return (norm < 10 ? '0' : '') + norm;
			};
			if (!Date.prototype.toISOString)
			{
				(function ()
				{

					function pad(number)
					{
						var r = String(number);
						if (r.length === 1)
						{
							r = '0' + r;
						}
						return r;
					}

					Date.prototype.toISOString = function ()
					{
						return this.getUTCFullYear()
			        + '-' + pad(this.getUTCMonth() + 1)
			        + '-' + pad(this.getUTCDate())
			        + 'T' + pad(this.getUTCHours())
			        + ':' + pad(this.getUTCMinutes())
			        + ':' + pad(this.getUTCSeconds())
			        + '.' + String((this.getUTCMilliseconds() / 1000).toFixed(3)).slice(2, 5)
			        + 'Z';
					};

				} ());
			}
			switch (format)
			{
				case 'd':
					date = date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate());
					break;
				case 'r':
					date = pad(date.getDate()) + '.' + pad(date.getMonth() + 1) + '.' + date.getFullYear();
					break;
				default:
					date = date.toISOString();
					date = date.split('.')[0] + z + pad(timezone / 60) + ':' + pad(timezone % 60);
			}
			return date;
		}

		, nowDateUTC: function (format, date)
		{
			var date= this.safeDateTime();
			return this.stringifyDateUTC(format, date);
		}

		, SafeParseDateTime: function (date)
		{
			if (!date)
				return '';
			function pad(number)
			{
				var r = String(number);
				if (r.length === 1)
					r = '0' + r;
				return r;
			}
			var date = new Date(date);
			var splitted = date.split(/[-T:]/);
			return pad(this.getUTCDate()) + '.' + pad(this.getUTCMonth() + 1) + '.' + this.getUTCFullYear()
				+ ' ' + pad(this.getUTCHours()) + ':' + pad(this.getUTCMinutes());
		}

	};
	return helper;
});
