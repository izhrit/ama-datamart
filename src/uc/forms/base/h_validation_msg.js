define([
	'forms/base/h_msgbox'
],
function (h_msgbox)
{
	var helper_res =
	{
		IfOkValidateResult_arr: function (args,validation_result, title_reject)
		{
			var invalid = false;
			var descriptions = '';
			for (var i = 0; i < validation_result.length; i++)
			{
				var validate_constraint = validation_result[i];
				if (false == validate_constraint.check_constraint_result)
					invalid = true;
				descriptions += '<li>' + validate_constraint.description + '</li>';
			}
			if (invalid)
			{
				h_msgbox.ShowModal({ title: title_reject, html: '<ul>' + descriptions + '</ul>', width: 800, id_div:'cpw-form-validation-msg' });
			}
			else
			{
				var title_warning = args.title_warning ? args.title_warning : 'Предупреждение перед сохранением содержимого формы';
				var warning_question = args.warning_question ? args.warning_question : '<div style="padding-left: 1.5em;">Сохранить содержимое формы?</div>';
				var btn_ok_title = "Да, сохранить";
				h_msgbox.ShowModal({
					title: title_warning, width: 800, id_div:'cpw-form-validation-msg'
					, html: '<ul style="padding-left: 1.5em;">' + descriptions + '</ul>' + warning_question
					, buttons: [btn_ok_title, "Нет, вернуться к редактированию"]
					, onclose: function (btn_title)
					{
						if (btn_title == btn_ok_title)
							args.on_validate_ok_func();
					}
				});
			}
		}
		,IfOkValidateResult: function (args)
		{
			var validation_result = args.validation_result;
			var title_reject = args.title_reject ? args.title_reject : 'Отказ сохранять содержимое формы из-за нарушенных ограничений';
			if (null == validation_result)
			{
				args.on_validate_ok_func();
			}
			else if ('string' == typeof validation_result)
			{
				h_msgbox.ShowModal({ title: title_reject, html: '<pre>' + validation_result + '</pre>', width: 800, id_div:'cpw-form-validation-msg' });
			}
			else if ('[object Array]' === Object.prototype.toString.call(validation_result))
			{
				this.IfOkValidateResult_arr(args, validation_result, title_reject);
			}
			else
			{
				h_msgbox.ShowModal({ title: title_reject, html: 'Валидация завершена каким то непредсказуемым образом', width: 800, id_div:'cpw-form-validation-msg' });
			}
		}
		,IfOkWithValidateResult: function (validation_result, on_validate_ok_func)
		{
			this.IfOkValidateResult({ validation_result: validation_result, on_validate_ok_func: on_validate_ok_func });
		}
		, issue: function (validation_result, description, check_constraint_result)
		{
			if (!validation_result || null == validation_result)
				validation_result = [];
			validation_result.push({
				description: description
				, check_constraint_result: !check_constraint_result ? false : check_constraint_result
			});
			return validation_result;
		}
		, error: function (validation_result, description)
		{
			return this.issue(validation_result, description);
		}
		, warning: function (validation_result, description)
		{
			return this.issue(validation_result, description, true);
		}
		, trim: function(s)
		{
			var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
			return s.replace(rtrim, '');
		}
	};
	return helper_res;
});
