﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
]
, function (fc_abstract, h_fastening_clip, h_msgbox, h_validation_msg)
{
	var fc_link_with_modal_dialog = fc_abstract();

	fc_link_with_modal_dialog.match = function (adom_item, tag_name, fc_type)
	{
		return 'A' == tag_name && 'a-modal' == fc_type;
	}

	fc_link_with_modal_dialog.render = function (options, model, model_selector, adom_item, fc_data)
	{
		var dom_item = $(adom_item);
		if (!options)
		{
			dom_item.html(dom_item.html() + '<br/>absent options for "' + model_selector + '"!');
		}
		else if (!options.controller)
		{
			dom_item.html(dom_item.html() + '<br/>absent controller for "' + model_selector + '"!');
		}
		else
		{
			var value = !model ? null : h_fastening_clip.get_model_field_value(model, model_selector);
			if (value && options.text)
				dom_item.text(options.text(value));

			if (!fc_data)
				fc_data = {};
			fc_data.value = value;

			dom_item.on(
				{
					click: function (e)
					{
						e.preventDefault();
						var editor = options.controller();
						var value = fc_data.value;
						if (value)
							editor.SetFormContent(value);
						var btnOk = !options.btn_ok_title ? 'Сохранить' : options.btn_ok_title;
						var btnClose= !options.btn_close_title ? 'Закрыть' : options.btn_close_title;
						var buttons= options.readonly ? [btnClose] : [btnOk, 'Отмена'];
						h_msgbox.ShowModal
						({
							title: !options.title ? model_selector : options.title
							, controller: editor
							, width: !options.width ? 400 : options.width
							, height: !options.height ? 400 : options.height
							, buttons: buttons
							, id_div: !options.id_div ? 'cpw-fastened-a-modal-form' : options.id_div
							, onclose: function (btn, dlg_div)
							{
								$dlg_div = $(dlg_div).closest('.ui-dialog-content');
								if (btn == btnOk)
								{
									var ChangeDomItemText = function (value) {
										if (null != options.text) {
											var txt = options.text(value);
											if (txt)
												dom_item.text(txt);
										}
									}
									var SetFcDataToDom = function () {
										fc_data.value = editor.GetFormContent();
										if(options.before_change_dom) {
											options.before_change_dom(fc_data.value, ChangeDomItemText, $dlg_div);
										} else {
											ChangeDomItemText(fc_data.value);
											$dlg_div.dialog("close");
										}
										dom_item.trigger('change');
									}
									if (!editor.Validate)
									{
										SetFcDataToDom();
									}
									else
									{
										h_validation_msg.IfOkWithValidateResult(editor.Validate(), function () { 
											SetFcDataToDom();
										});
										return false;
									}
								}
							}
						});
					}
				});
		}
		return fc_data;
	}

	fc_link_with_modal_dialog.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		return fc_data;
	}

	fc_link_with_modal_dialog.save_to_model = function (model, model_selector, dom_item, fc_data)
	{
		return h_fastening_clip.set_model_field_value(model, model_selector, fc_data.value);
	}

	fc_link_with_modal_dialog.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.a_modal_attrs = function (model_selector)
		{
			return this.fastening_attrs(model_selector, 'a-modal');
		}
	}

	return fc_link_with_modal_dialog;
});