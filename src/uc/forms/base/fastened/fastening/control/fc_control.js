define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
	, 'forms/base/h_get_selector'
]
, function (fc_abstract, h_fastening_clip,h_get_selector)
{
	var fc_control = fc_abstract();

	fc_control.match = function (adom_item, tag_name, fc_type)
	{
		return 'DIV' == tag_name && 'control' == fc_type;
	}

	fc_control.render = function (options, model, model_selector, adom_item, fc_data)
	{
		if (!options)
		{
			var dom_item = $(adom_item);
			dom_item.html(dom_item.html() + '<br/>absent options for "' + model_selector + '"!');
		}
		else if (!options.controller)
		{
			var dom_item = $(adom_item);
			dom_item.html(dom_item.html() + '<br/>absent controller for "' + model_selector + '"!');
		}
		else
		{
			var controller = options.controller();

			var value = !model ? null : h_fastening_clip.get_model_field_value(model, model_selector);
			var adom_item_selector = h_get_selector($(adom_item), true);
			if (!value)
			{
				if($(this).selector!=undefined) controller.CreateNew($(adom_item).selector);
				else controller.CreateNew(adom_item_selector);
			}
			else
			{
				controller.SetFormContent(value);
				if($(this).selector!=undefined) controller.Edit($(adom_item).selector);
				else controller.Edit(adom_item_selector);
			}

			if (!fc_data)
				fc_data = { };

			fc_data.afc_type = 'control';
			fc_data.amodel_selector = model_selector;
			fc_data.controller = controller;
		}
		return fc_data;
	}

	fc_control.load_from_model = function (model, model_selector, adom_item, fc_data)
	{
		var controller = fc_data.controller;

		var value = !model ? null : h_fastening_clip.get_model_field_value(model, model_selector);

		if (!value)
		{
			if($(this).selector!=undefined) controller.CreateNew($(adom_item).selector);
			else controller.CreateNew(h_get_selector($(adom_item)));
		}
		else
		{
			controller.SetFormContent(value);
			if($(this).selector!=undefined) controller.Edit($(adom_item).selector);
			else controller.Edit(h_get_selector($(adom_item)));
		}

		return fc_data;
	}

	fc_control.save_to_model = function (model, model_selector, dom_item, fc_data)
	{
		var value = null;
		if (fc_data && fc_data.controller)
		{
			var controller = fc_data.controller;
			value = null == controller ? null : controller.GetFormContent();
		}
		return h_fastening_clip.set_model_field_value(model, model_selector, value);
	}

	fc_control.destroy = function (adom_item, fc_data)
	{
		if (fc_data && fc_data.controller)
		{
			var controller = fc_data.controller;
			controller.Destroy();
		}
	}

	fc_control.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.control = function (model_selector)
		{
			return this.fastened_div(model_selector, 'control');
		}
	}

	return fc_control;
});