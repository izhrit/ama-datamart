﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
	, 'forms/base/h_numbering'
]
, function (fc_abstract, h_fastening_clip, h_numbering)
{
	var fc_text = fc_abstract();

	var fc_type_text = 'text';

	fc_text.match = function (adom_item, tag_name, fc_type)
	{
		return fc_type_text == fc_type;
	}

	var default_convert_value= function(value, as_text)
	{
		return (!value || null == value) ? '' : true===as_text ? value : value.toString().replace(/\"/g, '&quot;');
	}

	fc_text.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		model_selector = dom_item.attr('data-fc-selector');

		var value = h_fastening_clip.get_model_field_value(model, model_selector);
		var fixed_value = fc_data.cdata.f_convert_value(value, /*as_text=*/true);
		dom_item.text(fixed_value);

		return fc_data;
	}

	fc_text.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.text_field = function (model_selector, f_convert_value)
		{
			var res = '<span ';
			res += this.fastening_attrs(model_selector, fc_type_text);
			res += '>';

			if (!f_convert_value)
				f_convert_value = default_convert_value;

			this.store_fc_data({ cdata: { f_convert_value: f_convert_value }, fc_type: fc_type_text });

			var value = this.value();
			var fixed_value = f_convert_value(value);
			res += fixed_value;

			return res;
		}

		_template_argument.text_field_end = function ()
		{
			return '</span>';
		}

		_template_argument.txt_field = function (model_selector, f_convert_value)
		{
			var convert_value = f_convert_value;
			if ((0 === f_convert_value || f_convert_value) && ('function' != (typeof f_convert_value)))
			{
				convert_value= function (value)
				{
					return (value && null != value) ? value : f_convert_value;
				}
			}
			return this.text_field(model_selector, convert_value) + this.text_field_end();
		}

		_template_argument.txt_field_numbering = function (model_selector, n1, n2, n10)
		{
			convert_value = function (value)
			{
				var count = (value && null != value) ? value : 0;
				return h_numbering(count, n1, n2, n10);
			}
			return this.text_field(model_selector, convert_value) + this.text_field_end();
		}
	}

	return fc_text;
});