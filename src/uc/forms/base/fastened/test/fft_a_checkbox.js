﻿define([
	'forms/base/fastened/test/fft_abstract'
]
, function (fft_abstract)
{
	var a_checkbox_field_tester = fft_abstract();

	a_checkbox_field_tester.match = function (dom_item, tag_name, fc_type)
	{
		return 'A'==tag_name;
	}

	a_checkbox_field_tester.check_value = function (dom_item, value)
	{
		var txt = dom_item.text();
		return (-1 != txt.indexOf(value)) ? null : ' text "' + txt + '"! is WRONG!!!!!! should be "' + value + '"';
	}

	a_checkbox_field_tester.set_value = function (dom_item, value)
	{
		var txt = dom_item.text();
		if (-1 == txt.indexOf(value))
			dom_item.click();
		return 'set value "' + value + '"';
	}

	return a_checkbox_field_tester;
});