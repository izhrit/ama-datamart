﻿define([
	  'forms/base/fastened/test/fft_abstract'
]
, function (fft_abstract)
{
	var fastened_radio_tester = fft_abstract();

	fastened_radio_tester.match = function (dom_item, tag_name, fc_type)
	{
		return 'radio' == dom_item.attr('type');
	}

	fastened_radio_tester.check_value = function (dom_item, value)
	{
		dom_item = dom_item.filter('[value="' + value + '"]');
		if (!dom_item || null == dom_item || 0 == dom_item.length)
		{
			return 'can not find element for value "' + value + '"';
		}
		else if (dom_item.attr('checked'))
		{
			return null; // ' ok, value "' + value + '" is checked';
		}
		else
		{
			return ' value "' + value + '" is unchecked! it is WRONG!!!!!!!!';
		}
	}

	fastened_radio_tester.set_value = function (dom_item, value)
	{
		dom_item = dom_item.filter('[value="' + value + '"]');
		if (!dom_item || null == dom_item || 0 == dom_item.length)
		{
			return 'can not find element for value "' + value + '"';
		}
		else
		{
			dom_item.click();
			return 'clicked for value "' + value + '"';
		}
	}

	return fastened_radio_tester;
});