define([
	  'forms/base/codec/codec.copy'
	, 'forms/base/codec/url/codec.url.url-args'
]
, function (codec_copy, codec_url_urlargs)
{
	var urldecode= function(str)
	{
		return decodeURIComponent((str + '').replace(/\+/g, '%20'));
	}

	var FixJQGridArgs= function(args)
	{
		args.rows = parseInt(args.rows);
		args.page = parseInt(args.page);

		if (args.filters)
		{
			var filters = args.filters;
			filters = urldecode(filters);
			filters = filters.replace("\\\"", "\"");
			filters = JSON.parse(filters);
			args.filters = filters;
		}
		if (args.sidx)
			args.sidx = urldecode(args.sidx);
	}

	var ParseUrlItem = function (url,prefix)
	{
		var url_without_prefix = url.substring(prefix.length, url.length);
		var iq = url_without_prefix.indexOf('?');

		var args_part = url_without_prefix.substring(iq + 1, url_without_prefix.length);

		var queue_part = url_without_prefix.substring(1, iq);
		var queue_part_parts = queue_part.split('$');

		var args = {};
		var res = { args: args, url: url, prefix: prefix };
		var args_part_parts = args_part.split('&');
		for (var i = 0; i < args_part_parts.length; i++)
		{
			var p = args_part_parts[i].split('=');
			args[p[0]] = p[1];
		}
		FixJQGridArgs(args);
		return res;
	}

	var GetItems = function (rows_all, url, rows_options)
	{
		var parsed_url = ParseUrlItem(url, !rows_options || !rows_options.url_prefix ? '' : rows_options.url_prefix);
		if (rows_options && rows_options.CustomParseUrl)
			rows_options.CustomParseUrl(parsed_url);

		var args = parsed_url.args;
		var rows = args.rows;
		var page = args.page;

		var filtered_rows = [];
		for (var i = 0; i < rows_all.length; i++)
		{
			var row = rows_all[i];
			var prepared_row = (!rows_options || !rows_options.PrepareRow) ? row : rows_options.PrepareRow(row);
			if (!rows_options || !rows_options.RowIsOkForFilter || rows_options.RowIsOkForFilter(row, prepared_row, parsed_url))
				filtered_rows.push(prepared_row);
		}

		if (rows_options && rows_options.RowSort)
			filtered_rows.sort(rows_options.RowSort(args));

		var page_rows = [];
		for (var i = 0; i < filtered_rows.length; i++)
		{
			var r = filtered_rows[i];
			if (Math.floor(i / rows) == (page - 1))
				page_rows.push(r);
		}

		var res =
			{
				page: page // номер текущей "страницы"
				, total: Math.ceil(filtered_rows.length / rows) // количество страниц "всего"
				, records: filtered_rows.length // количество записей "всего"
				, rows: page_rows // записи текущей страницы
			};
		return res;
	}

	var JQGridSort= function(args)
	{
		var sidx = args.sidx;
		var sord = args.sord;
		return function (a, b)
		{
			var aName = '';
			var bName = '';
			if (a[sidx])
				aName = a[sidx];
			if (b[sidx])
				bName = b[sidx];
			var res = ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
			if ('desc' == sord)
				res = -res;
			return res;
		}
	}

	var std_FilterGetFiel = function (prepared_row, name)
	{
		return prepared_row[name];
	}

	var std_RowIsOkForFilter = function (row, prepared_row, parsed_url)
	{
		if (parsed_url && parsed_url.args && parsed_url.args.filters && parsed_url.args.filters.rules)
		{
			var rules = parsed_url.args.filters.rules;
			for (var i = 0; i < rules.length; i++)
			{
				var rule = rules[i];
				var rule_value = rule.data;
				var row_value = this.FilterGetField(prepared_row, rule.field);
				if (!row_value)
				{
					return false;
				}
				else
				{
					row_value = row_value.toString();
					if (0 != row_value.toUpperCase().indexOf(rule_value.toUpperCase()))
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	return function(url_prefix)
	{
		var transport = {}

		transport.rows_all = [];
		transport.options = {
			RowSort: JQGridSort
			, FilterGetField: std_FilterGetFiel
			, RowIsOkForFilter: std_RowIsOkForFilter
			, PrepareRow: function(row)
			{
				return codec_copy().Encode(row);
			}
		}

		if (url_prefix)
			transport.options.url_prefix= url_prefix;

		transport.prepare_send_abort = function (options, originalOptions, jqXHR)
		{
			var self = this;
			var send_abort=
			{
				send: function (headers, completeCallback)
				{
					var rows_all = self.rows_all;
					if ('function' == typeof rows_all)
					{
						var args = codec_url_urlargs().Decode(options.url);
						rows_all = self.rows_all(options.url,args);
					}

					var res = GetItems(rows_all, options.url, self.options);
					completeCallback(200, 'success', { text: JSON.stringify(res) });
				}
				, abort: function ()
				{
				}
			}
			return send_abort;
		}

		transport.prepare_try_to_prepare_send_abort = function ()
		{
			var self = this;
			return function (options, originalOptions, jqXHR)
			{
				if (0 == options.url.indexOf(self.options.url_prefix))
				{
					return self.prepare_send_abort(options, originalOptions, jqXHR);
				}
			}
		}

		return transport;
	}
});
