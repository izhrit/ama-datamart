define([
	  'forms/base/ajax/ajax-service-base'
]
, function (ajax_service_base)
{
	return function(url_prefix)
	{
		var service_crud= ajax_service_base(url_prefix);

		service_crud.action= function (cx)
		{
			var args= cx.url_args();
			switch (args.cmd)
			{
				case 'add':
					var result = this.create(cx.data_args(), args);
					var res = { ok: true };
					if(result) {
						if(typeof result === 'object' && !Array.isArray(result)) {
							$.extend(res, result);
						} else {
							res.id = result;
						}
					}
					cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
					return;
				case 'get':
					var row = this.read(args.id, args);
					cx.completeCallback(200, 'success', { text: JSON.stringify(row) });
					return;
				case 'update':
					var result = this.update(args.id, cx.data_args(), args);
					var res = { ok: true };
					if(result && typeof result === 'object' && !Array.isArray(result)) {
						$.extend(res, result);
					} else {
						res.id = result;
					}
					cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
					return;
				case 'delete':
					var result = this._delete(args.id, args);
					var res = { ok: true };
					if(result) {
						if(typeof result === 'object' && !Array.isArray(result)) {
							$.extend(res, result);
						} else {
							res.id = result;
						}
					}
					cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
					return;
			}
			cx.completeCallback(400, 'bad request', { text: 'unknown cmd=' + args.cmd + '!' });
			
		}

		return service_crud;
	}
});
