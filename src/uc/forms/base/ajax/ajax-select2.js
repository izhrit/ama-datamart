define([
	  'forms/base/codec/codec.copy'
	, 'forms/base/codec/url/codec.url'
	, 'forms/base/codec/url/codec.url.args'
]
, function (codec_copy, codec_url, codec_url_args)
{
	return function(url_prefix)
	{
		var transport = {}
		transport.options = {}

		if (url_prefix)
			transport.options.url_prefix= url_prefix;

		transport.query = function ()
		{
			return { results: [{ id: 1, text: "1" }, { id: 2, text: "2" }] };
		}

		transport.prepare_send_abort = function (options, originalOptions, jqXHR)
		{
			var self = this;
			var send_abort=
			{
				send: function (headers, completeCallback)
				{
					var decoded_url = codec_url().Decode(options.url);
					var args = codec_url_args().Decode(decoded_url);
					var res = self.query(args.q, args.page, args);
					completeCallback(200, 'success', { text: JSON.stringify(res) });
				}
				, abort: function ()
				{
				}
			}
			return send_abort;
		};

		transport.prepare_try_to_prepare_send_abort = function ()
		{
			var self = this;
			return function (options, originalOptions, jqXHR)
			{
				if (0 == options.url.indexOf(self.options.url_prefix))
				{
					return self.prepare_send_abort(options, originalOptions, jqXHR);
				}
			}
		}

		return transport;
	}
});
