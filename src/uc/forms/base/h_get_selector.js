define(function () {
    var get_selector = function (element) {
        var pieces = [];

        for (; element && element.tagName !== undefined; element = element.parentNode) {
            if (element.className) {
                var classes = element.className.split(' ');
                for (var i in classes) {
                    if (classes.hasOwnProperty(i) && classes[i]) {
                        pieces.unshift(classes[i]);
                        pieces.unshift('.');
                    }
                }
            }
            if (element.id && !/\s/.test(element.id)) {
                pieces.unshift(element.id);
                pieces.unshift('#');
            }
            var fc_id = element.getAttribute('fc-id');
            if (fc_id && null != fc_id)
                pieces.unshift('[fc-id="' + fc_id + '"]');
            pieces.unshift(element.tagName);
            pieces.unshift(' > ');
        }

        return pieces.slice(1).join('');
    };

    return function (dom_item, only_one) {
        if (true === only_one) {
            return get_selector(dom_item[0]);
        } else {
            return $.map(dom_item, function (el) {
                return get_selector(el);
            });
        }
    };
});