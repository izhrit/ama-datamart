define(function ()
{
	var helper ={};

	helper.check_Empty= function(txt)
	{
		return !txt || null==txt || ''==txt ;
	}

	helper.check_NotEmpty= function(txt)
	{
		return !helper.check_Empty(txt);
	}

	helper.recommend_check_NotEmpty = function (txt)
	{
		return !helper.check_Empty(txt) ? true : 'recommended';
	}

	helper.description_NotEmpty= function(element, label)
	{
		return 'Поле "' + label + '" не может быть пустым';
	}

	helper.check_EmptyOrRegExp= function(re)
	{
		return function (txt) { return helper.check_Empty(txt) ? true : re.test(txt); };
	}

	helper.check_EmptyOrNumberLength= function(number_len)
	{
		var re = new RegExp('^\\d{' + number_len + '}$');
		return helper.check_EmptyOrRegExp(re);
	}

	helper.check_NumberLength = function (number_len)
	{
		var re = new RegExp('^\\d{' + number_len + '}$');
		return function (txt) { return re.test(txt); };
	}

	helper.check_Date = function ()
	{
		var re = new RegExp('^\\d\\d.\\d\\d.\\d\\d\\d\\d$');
		return function (txt) { return helper.check_Empty(txt) || re.test(txt); };
	}

	helper.description_Date = function (element, label) 
	{
		return 'Поле "' + label + '" должно содержать дату в формате ДД.ММ.ГГГГ';
	}

	helper.Date = function () { return { check: helper.check_Date(), description: helper.description_Date }; }

	helper.description_EmptyOrNumberLength= function(number_len)
	{
		return function(element, label){return 'Поле "' + label + '" должно состоять из ' + number_len + ' цифр либо быть пустым'; }
	}

	helper.description_NumberLength = function (number_len)
	{
		return function (element, label) { return 'Поле "' + label + '" должно состоять из ' + number_len + ' цифр и не может быть пустым'; }
	}

	helper.check_Inn = function(inn)
	{
		if (!inn || inn == "") {
			return false;
		}
		else {
			if (inn.length == 10) {
				return inn[9] == String(((
					2 * inn[0] + 4 * inn[1] + 10 * inn[2] +
					3 * inn[3] + 5 * inn[4] + 9 * inn[5] +
					4 * inn[6] + 6 * inn[7] + 8 * inn[8]
				) % 11) % 10);
			}
			else if (inn.length == 12) {
				return inn[10] == String(((
					7 * inn[0] + 2 * inn[1] + 4 * inn[2] +
					10 * inn[3] + 3 * inn[4] + 5 * inn[5] +
					9 * inn[6] + 4 * inn[7] + 6 * inn[8] +
					8 * inn[9]
				) % 11) % 10) && inn[11] == String(((
					3 * inn[0] + 7 * inn[1] + 2 * inn[2] +
					4 * inn[3] + 10 * inn[4] + 3 * inn[5] +
					5 * inn[6] + 9 * inn[7] + 4 * inn[8] +
					6 * inn[9] + 8 * inn[10]
				) % 11) % 10);
			}
			else if (inn.length == 14) {
				return true;
			}
			return false;
		}
	}

	helper.description_Inn = function (element, label) 
	{ 
		return 'Поле "' + label + '" должно содержать 10/12/14 цифр и удовлетворять контрольной сумме';
	}

	helper.Inn = function () { return { check: helper.check_Inn, description: helper.description_Inn }; }

	helper.check_Ogrn = function(ogrn)
	{
		if (!ogrn || ogrn == "") {
			return false;
		}
		else {
			if (ogrn.length == 13) {
				var n13 = parseInt((parseInt(ogrn.slice(0, -1)) % 11).toString().slice(-1));
				if (n13 === parseInt(ogrn[12])) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}
	}

	helper.description_Ogrn = function (element, label) 
	{ 
		return 'Поле "' + label + '" должно содержать 13 цифр и удовлетворять контрольной сумме';
	}

	helper.Ogrn = function () { return { check: helper.check_Ogrn, description: helper.description_Ogrn }; }

	helper.check_Snils = function(snils)
	{
		if (!snils || snils == "") {
			return false;
		}
		else {
			if (snils.length == 11) {
				var sum = 0;
				for (var i = 0; i < 9; i++) {
					sum += parseInt(snils[i]) * (9 - i);
				}
				var checkDigit = 0;
				if (sum < 100) {
					checkDigit = sum;
				} else if (sum > 101) {
					checkDigit = parseInt(sum % 101);
					if (checkDigit === 100) {
						checkDigit = 0;
					}
				}
				if (checkDigit === parseInt(snils.slice(-2))) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}
	}

	helper.description_Snils = function (element, label) 
	{ 
		return 'Поле "' + label + '" должно содержать 11 цифр и удовлетворять контрольной сумме';
	}

	helper.Snils = function () { return { check: helper.check_Snils, description: helper.description_Snils }; }

	helper.NotEmpty = function () { return { check: helper.check_NotEmpty, description: helper.description_NotEmpty }; }

	helper.recommend_NotEmpty = function (descript)
	{
		return { check: helper.recommend_check_NotEmpty, description: function (element, label) { return descript; } };
	}

	helper.EmptyOrNumberLength= function(number_len) 
	{
		return { check: this.check_EmptyOrNumberLength(number_len), description: this.description_EmptyOrNumberLength(number_len) };
	}

	helper.NumberLength = function (number_len)
	{
		return { check: this.check_NumberLength(number_len), description: this.description_NumberLength(number_len) };
	}

	helper.check_PeriodDateNow = function (dateStart, dateEnd)
	{
		if (helper.check_Empty(dateStart) || helper.check_Empty(dateEnd))
			return true;
		var dateNow = new Date();
		var date_start = new Date(dateStart.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		var date_end = new Date(dateEnd.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		return date_start <= date_end && date_start <= dateNow && dateNow <= date_end;
	}

	helper.check_PeriodDate = function (dateStart, dateEnd)
	{
		if (helper.check_Empty(dateStart) || helper.check_Empty(dateEnd))
			return true;
		var date_start = new Date(dateStart.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		var date_end = new Date(dateEnd.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		return date_start <= date_end;
	}

	helper.recommend_check_PeriodDate = function (dateStart, dateEnd)
	{
		return helper.check_PeriodDate(dateStart, dateEnd) ? true : 'recommended';
	}

	helper.check_SrictPeriodDate = function (dateStart, dateEnd)
	{
		if (helper.check_Empty(dateStart) || helper.check_Empty(dateEnd))
			return true;
		var date_start = new Date(dateStart.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		var date_end = new Date(dateEnd.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		return date_start < date_end;
	}

	helper.check_BeforeDateNow = function (date)
	{
		if (helper.check_Empty(date))
			return true;
		var dateNow = new Date();
		var dateCurrent = new Date(date.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		return dateNow >= dateCurrent;
	}

	helper.recommend_check_BeforeDateNow = function (date)
	{
		return helper.check_BeforeDateNow(date) ? true : 'recommended';
	}

	helper.description_BeforeDateNow = function (element, label)
	{
		return 'Поле "' + label + '" должно содержать дату не позднее текущей даты';
	}

	helper.BeforeDateNow = function () { return { check: helper.check_BeforeDateNow, description: helper.description_BeforeDateNow }; }

	helper.check_EmptyOrBeforeDateNow = function (date)
	{
		return helper.check_Empty(date) || helper.check_BeforeDateNow(date);
	}

	helper.description_EmptyOrBeforeDateNow = function (element, label)
	{
		return 'Поле "' + label + '" должно содержать дату не позднее текущей, либо должно быть пустым';
	}

	helper.EmptyOrBeforeDateNow = function () { return { check: helper.check_EmptyOrBeforeDateNow, description: helper.description_EmptyOrBeforeDateNow }; }

	helper.recommend_check_Ciryllic = function (txt)
	{
		var re = new RegExp(/[^А-Я,Ё,а-я,ё,\s,\-]/);
		return !helper.check_Empty(txt) && re.test(txt) ? 'recommended' : true;
	}

	helper.description_Ciryllic = function (element, label)
	{
		return 'Поле "' + label + '" содержит недопустимые символы';
	}

	helper.recommend_Ciryllic = function () { return { check: helper.recommend_check_Ciryllic, description: helper.description_Ciryllic }; }

	helper.recommend_check_Latin = function (txt)
	{
		var re = new RegExp(/[^A-Z,a-z,\s,\-]/);
		return !helper.check_Empty(txt) && re.test(txt) ? 'recommended' : true;
	}

	helper.description_Latin = function (element, label)
	{
		return 'Поле "' + label + '" содержит недопустимые символы';
	}

	helper.recommend_Latin = function () { return { check: helper.recommend_check_Latin, description: helper.description_Latin }; }

	helper.check_DiffDatesMore14 = function (date1, date2)
	{
		var dateCurrent1 = new Date(date1.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		var dateCurrentYear1 = dateCurrent1.getFullYear();
		var dateCurrent2 = new Date(date2.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		var dateCurrentYear2 = dateCurrent2.getFullYear();
		var dateDiff = dateCurrentYear2 - dateCurrentYear1;
		if (dateDiff > 0 && dateDiff < 14) {
			return false;
		}
		else if (dateDiff > 14 || dateDiff < 0) {
			return true;
		}
		else {
			dateCurrent1.setYear(dateCurrentYear1 + 14);
			return dateCurrent2 >= dateCurrent1;
		}
	}

	helper.check_DocumentTypePassport = function (date)
	{
		var dateNow = new Date();
		var dateNowYear = dateNow.getFullYear();
		var dateCurrent = new Date(date.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
		var dateCurrentYear = dateCurrent.getFullYear();
		var dateDiff = dateNowYear - dateCurrentYear;
		if (dateDiff > 0 && dateDiff < 14)
		{
			return false;
		}
		else if (dateDiff > 14 || dateDiff < 0)
		{
			return true;
		}
		else
		{
			dateCurrent.setYear(dateCurrentYear + 14);
			return dateNow >= dateCurrent;
		}
	}

	helper.recommend_check_DocumentTypePassport = function (date, type)
	{
		if (helper.check_Empty(date) || helper.check_Empty(type))
			return true;
		return type.id == '103008' && !helper.check_DocumentTypePassport(date) ? 'recommended' : true;
	}

	helper.recommend_check_DocumentTypeNotPassport = function (date, type)
	{
		if (helper.check_Empty(date) || helper.check_Empty(type))
			return true;
		return type.id == '102974' && helper.check_DocumentTypePassport(date) ? 'recommended' : true;
	}

	helper.recommend_check_NationalityRus = function (data)
	{
		if (helper.check_Empty(data) || data.id == 'RUS')
			return true;
		else
			return 'recommended';
	}

	helper.recommend_check_CountrySun = function (date, item)
	{
		if (!(!item || null == item || !item.Country || null == item.Country))
		{
			var republicSun = ['AZE', 'ARM', 'BLR', 'GEO', 'KAZ', 'KGZ', 'LVA', 'LTU', 'MDA', 'RUS', 'TJK', 'TKM', 'UZB', 'UKR', 'EST'];
			var dateForSun = new Date(92, 1, 6);
			var dateCurrent = new Date(date.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
			if ((dateCurrent < dateForSun) && (republicSun.toString()).indexOf(item.Country.id) != -1)
				return 'recommended';
		}
		return true;
	}

	helper.description_NationalityRus = function (element, label) { return 'Недопустимое гражданство при прибытии гражданина РФ'; }

	helper.recommend_NationalityRus = function () { return { check: helper.recommend_check_NationalityRus, description: helper.description_NationalityRus }; }

	helper.check_Phone = function (phone) {
		if (helper.check_Empty(phone))
			return true;
		var number = phone.replace('+7', '').replace('(', '').replace(')', '').replace(/-/g, '').replace(/_/g, '');
		return number.length == 10;
	}

	helper.description_Phone = function (element, label) { return 'Неверно указан номер телефона, допустимо ровно 10 цифры'; }

	helper.checkPhone = function () { return { check: helper.check_Phone, description: helper.description_Phone }; }

	helper.recommend_check_SeriesPassport = function (series, type) {
		if (helper.check_Empty(series))
			return true;
		var re = new RegExp(/[^0-9]/);
		return series.length != 4 || re.test(series) ? 'recommended' : true;
	}

	helper.description_SeriesPassport = function (element, label) { return 'Неверно указана серия документа, удостоверяющего личность, допустимо ровно 4 цифры'; }

	helper.recommend_SeriesPassport = function () { return { check: helper.recommend_check_SeriesPassport, description: helper.description_SeriesPassport }; }

	helper.recommend_SeriesPassportType = function () {
		return {
			check: function (series, type) { return helper.check_Empty(type) || type.id != '103008' ? true : helper.recommend_check_SeriesPassport(series); },
			description: helper.description_SeriesPassport
		};
	}

	helper.recommend_check_NumberPassport = function (number, type) {
		if (helper.check_Empty(number))
			return true;
		var re = new RegExp(/[^0-9]/);
		return number.length != 6 || re.test(number) ? 'recommended' : true;
	}

	helper.description_NumberPassport = function (element, label) { return 'Неверно указан номер документа, удостоверяющего личность, допустимо ровно 6 цифр'; }

	helper.recommend_NumberPassport = function () { return { check: helper.recommend_check_NumberPassport, description: helper.description_NumberPassport }; }

	helper.recommend_NumberPassportType = function () {
		return {
			check: function (number, type) { return helper.check_Empty(type) || type.id != '103008' ? true : helper.recommend_check_NumberPassport(number); },
			description: helper.description_NumberPassport
		};
	}

	helper.recommend_check_SeriesMigration = function (series) {
		return helper.check_NotEmpty(series) && series.length != 4 ? 'recommended' : true;
	}

	helper.description_SeriesMigration = function (element, label) { return 'Серия миграционной карты должна состоять из четырех цифр'; }

	helper.recommend_SeriesMigration = function () { return { check: helper.recommend_check_SeriesMigration, description: helper.description_SeriesMigration }; }

	helper.recommend_check_NumberMigration = function (number) {
		return helper.check_NotEmpty(number) && number.length != 7 ? 'recommended' : true;
	}

	helper.description_NumberMigration = function (element, label) { return 'Номер миграционной карты должен состоять из семи цифр'; }

	helper.recommend_NumberMigration = function () { return { check: helper.recommend_check_NumberMigration, description: helper.description_NumberMigration }; }

	helper.recommend_check_SexByMiddleName = function (sex, name)
	{
		if (helper.check_Empty(sex) || helper.check_Empty(name))
			return true;
		var maleEnds = ['вич', 'ич', 'ьич', 'оглы'];
		var femaleEnds = ['вна', 'чна', 'кызы'];
		function checkEnds(ends)
		{
			for (var i = 0; i < ends.length; i++)
			{
				if (name.match(ends[i] + "$") == ends[i])
					return true;
			}
			return false;
		}
		var resultMaleEnds = checkEnds(maleEnds);
		var resultFemaleEnds = checkEnds(femaleEnds);
		if ('male' == sex && resultFemaleEnds)
			return 'recommended';
		else if ('female' == sex && resultMaleEnds)
			return 'recommended';
		else
			return true;
	}

	helper.description_SexByMiddleName = function (element, label) { return 'Пол гражданина не соответствует отчеству'; }

	helper.check_Visa_identifier = function (identifier) {
		if (!identifier || identifier == "") {
			return false;
		}
		else {
			if (identifier.length == 9) {
				var first = "А,Б,В,Г,Д,Е,Ё,Ж,З,И,Й,К,Л,М,Н,О,П,Р,С,Т,У,Ф,Х,Ц,Ч,Ш,Щ,Ъ,Ы,Ь,Э,Ю,Я,а,б,в,г,д,е,ё,ж,з,и,й,к,л,м,н,о,п,р,с,т,у,ф,х,ц,ч,ш,щ,ъ,ы,ь,э,ю,я";
				var last = "0,1,2,3,4,5,6,7,8,9";
				return -1 < first.indexOf(identifier[0]) && -1 < first.indexOf(identifier[1]) && -1 < first.indexOf(identifier[2]) &&
					-1 < last.indexOf(identifier[3]) && -1 < last.indexOf(identifier[4]) && -1 < last.indexOf(identifier[5]) &&
					-1 < last.indexOf(identifier[6]) && -1 < last.indexOf(identifier[7]) && -1 < last.indexOf(identifier[8]);
			}
			return false;
		}
	}

	helper.recommended_check_Visa_identifier = function (identifier) {
		if (helper.check_Visa_identifier(identifier))
			return true;
		else
			return 'recommended';
	}

	helper.for_Field= function(model_field_name,constraint)
	{
		constraint.fields = [model_field_name];
		return constraint;
	}

	helper.for_Fields= function(model_field_name,constraint)
	{
		constraint.fields = model_field_name;
		return constraint;
	}

	helper.get_field_value = function (model, field_name)
	{
		var name_parts = field_name.split('.');
		for (var i = 0; i < name_parts.length - 1; i++)
		{
			var name_part = name_parts[i];
			if (!model[name_part])
				return '';
			model = model[name_part];
		}
		var last_name_part = name_parts[name_parts.length - 1];
		return !model[last_name_part] ? '' : model[last_name_part];
	}

	helper.get_fields_values= function(model,fields)
	{
		var field_values = [];
		for (var i= 0; i< fields.length; i++)
		{
			field_values.push(this.get_field_value(model,fields[i]));
		}
		return field_values;
	}

	helper.build_id = function (constraints)
	{
		if (constraints)
		{
			for (var i = 0; i < constraints.length; i++)
			{
				var constraint = constraints[i];
				constraint.id = i;
			}
		}
		return constraints;
	}

	helper.build_by_fields = function (constraints)
	{
		var constraints_by_fields = {};
		if (constraints)
		{
			for (var i = 0; i < constraints.length; i++)
			{
				var constraint = constraints[i];
				constraint.id = i;
				for (var j = 0; j < constraint.fields.length; j++)
				{
					var model_field_name = constraint.fields[j];
					if (!constraints_by_fields[model_field_name])
					{
						constraints_by_fields[model_field_name] = [constraint];
					}
					else
					{
						constraints_by_fields[model_field_name].push(constraint);
					}
				}
			}
		}
		return constraints_by_fields;
	}

	helper.check_constraint = function (model, constraint, model_constraints_statuses, fields_to_validate)
	{
		if (fields_to_validate)
		{
			for (var i= 0; i<constraint.fields.length; i++)
			{
				var field = constraint.fields[i];

				if (!fields_to_validate[field])
					fields_to_validate[field] = true;
			}
		}
		var constraint_id = constraint.id;
		if (model_constraints_statuses[constraint_id] || false===model_constraints_statuses[constraint_id])
		{
			return model_constraints_statuses[constraint_id];
		}
		else
		{
			var values = this.get_fields_values(model, constraint.fields);
			var result = constraint.check.apply(null,values);
			model_constraints_statuses[constraint_id] = result;
			return result;
		}
	}

	helper.aggregate_check_result = function (aggregateted_check_result, constraint_check_result)
	{
		if (false == constraint_check_result)
		{
			return false;
		}
		else if (true == constraint_check_result)
		{
			return aggregateted_check_result;
		}
		else if (false != aggregateted_check_result)
		{
			return constraint_check_result;
		}
		else
		{
			return aggregateted_check_result;
		}
	}

	helper.check_constraints = function (model, constraints, model_constraints_statuses, fields_to_validate, model_field_name)
	{
		var aggregateted_check_result = true;
		if (constraints && null != constraints)
		{
			for (var i = 0; i < constraints.length; i++)
			{
				var constraint = constraints[i];
				var constraint_check_result = this.check_constraint(model, constraint, model_constraints_statuses, fields_to_validate);
				constraint.result = constraint_check_result;
				if (!constraint.marks || (constraint.marks.toString()).indexOf(model_field_name) > -1)
					aggregateted_check_result = this.aggregate_check_result(aggregateted_check_result, constraint_check_result);
			}
		}
		return aggregateted_check_result;
	}

	helper.combine= function(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10)
	{
		var res = c1;
		if (c2)
			res = res.concat(c2);
		if (c3)
			res = res.concat(c3);
		if (c4)
			res = res.concat(c4);
		if (c5)
			res = res.concat(c5);
		if (c6)
			res = res.concat(c6);
		if (c7)
			res = res.concat(c7);
		if (c8)
			res = res.concat(c8);
		if (c9)
			res = res.concat(c9);
		if (c10)
			res = res.concat(c10);
		return res;
	}

	return helper;
});
