@pushd %~dp0
@call %~dp0..\..\scripts\tests.bat encode_module %~dp0..\..\ forms/base/codec/test/codec.index forms/ama/spec_ama %~dp0\index.js
@call %~dp0..\..\scripts\tests.bat encode_module %~dp0..\..\ forms/base/codec/test/codec.contents forms/ama/spec_ama %~dp0\contents.js
@popd
exit /B