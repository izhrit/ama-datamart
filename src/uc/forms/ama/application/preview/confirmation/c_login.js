define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/preview/confirmation/e_login.html'
	, 'forms/base/h_msgbox'
],
	function (c_fastened, tpl, h_msgbox) {
		return function (options) {

			var controller = c_fastened(tpl, options);
			//var base_url = options.base_url;
			var base_url = '/dm/ui-backend.php';
			var base_Render = controller.Render;
			controller.Render = function (sel) {
				base_Render.call(this, sel);
				var self = this;
				$(sel + ' button.login').click(function (e) { e.preventDefault(); self.OnCtrlLogin(); });
				$(sel + ' .icon-password-toggle').click(function (e) {
					var input = $(sel + ' div.password-input-container input');
					if (input.attr('type') == 'password') {
						input.prop('type', 'text');
						$(this).prop('title', '������ ������');
					} else {
						input.prop('type', 'password');
						$(this).prop('title', '�������� ������');
					}
				});
			}

			controller.size = { width: 500, height: 500 };

			controller.OnCtrlLogin = function () {
				var login_data = this.GetFormContent();
				var self = this;
				var ajaxurl = base_url + '?action=login&as=viewer';
				var v_ajax = h_msgbox.ShowAjaxRequest("������ ����������� �� �������", ajaxurl);
				v_ajax.ajax({
					dataType: "json"
					, type: 'POST'
					, cache: false
					, data: login_data
					, success: function (responce, textStatus) {
						if (null == responce) {
							h_msgbox.ShowModal({
								width: 500
								, title: '��������� ��������������'
								, html: '�� ����� ������������ ��� ������������ ��� ������!'
							});
						}
						else if (false == responce.ok) {
							h_msgbox.ShowModal
								({
									title: '������ ��������������'
									, width: 450
									, html: '<span>�� ������� �������� �������������� �� �������:</span><br/> <center><b>\"'
										+ responce.reason + '\"</b></center>'
										+ '<small>C ��������������� ��������� ����������� � ������ ���������.</small>'
								});
						}
						else {
							document.location.href = '/dm/application_preview.php';
						}
					}
				});
			}

			return controller;
		}
	});