define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/preview/confirmation/e_confirmation.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox) 
{
	return function (email) {

		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel) {
			base_Render.call(this, sel);
			var self = this;
			$(sel + ' #text').text('Одноразовый пароль был отправле на электронную почту ' + email + ' введите его в эту форму');
			$(sel + ' #confirmation_token').focus(function (e) {
				$(sel + ' #notice').css("display", "none");
				$(sel + ' #confirmation_token').select();
			});
		}

		controller.size = { width: 400, height: 300 };

		return controller;
	}
});