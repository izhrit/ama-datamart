define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/preview/confirmation/e_set_email.html'
],
	function (c_fastened, tpl) {
		return function () {

			var controller = c_fastened(tpl);

			var base_Render = controller.Render;
			controller.Render = function (sel) {
				base_Render.call(this, sel);
				var self = this;
			}

			controller.size = { width: 400, height: 300 };

			return controller;
		}
	});