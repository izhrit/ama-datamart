﻿define(['forms/ama/application/preview/c_preview'],
	function (CreateController) {
		var form_spec =
		{
			CreateController: CreateController
			, key: 'application_preview'
			, Title: 'Требование о включении в реестр требований кредиторов'
		};
		return form_spec;
	});
