define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/preview/e_preview.html'
	, 'tpl!forms/ama/application/preview/v_application.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/application/preview/sentences/c_control'
	, 'forms/base/h_validation_msg'
	, 'forms/ama/application/preview/confirmation/c_confirmation'
	, 'forms/ama/application/preview/confirmation/c_set_email'
	, 'forms/ama/application/attachment/c_attachments'
	, 'forms/ama/application/preview/confirmation/c_login'
],
function (c_fastened, tpl, tpl_application_doc, h_msgbox, c_control, h_validation_msg, c_confirmation, c_set_email, c_attachments, c_login_form)
{
	return function (options_arg)
	{
		var controller= c_fastened(tpl);
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.Render_application();

			var self = this;

			/*$(sel + ' button#view').click(function (e) { self.View(); });
			$(sel + ' button#load_application').click(function (e) { self.LoadApplication(); });
			$(sel + ' button#save_application').click(function (e)
			{
				var update = options_arg && options_arg.id_MData;
				self.SaveFromDatamart(update, function () {window.close('', '_parent', '');});
			});
			$(sel + ' button#save').click(function (e) {
				var update = options_arg && options_arg.id_MData;
				self.SaveFromDatamart(update, function (id_MData) { if (!options.id_MData) options.id_MData = id_MData;});
			});
			self.View();*/
		}

		controller.BuildHtmlReport = function ()
		{
			return tpl_application_doc(this.model);
		}

		controller.View = function ()
		{
			this.model = this.GetFormContent();
			var self = this;
			var sel = this.fastening.selector;

			var ajaxurl = base_url + '?action=application.view-document';
			ajaxurl += options.id_MData ? ('&id_MData=' + options.id_MData) : '';
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение информации для генерации документа", ajaxurl);
			v_ajax.ajax({
				type: 'GET'
				, dataType: "json"
				, cache: false
				, success: function (responce, textStatus) {
					self.SetFormContent(responce);
					self.Render_application();
					if (options.from == 'datamart') {
						$(sel + ' #save_application').css("display", "block");
					}
					else {
						$(sel + ' #save_application').css("display", "none");
					}
				}

			});
		}

			controller.Validate = function (data)
			{
				var res = null;
				if (!data.Должник || !data.Должник.ИНН || '' == h_validation_msg.trim(data.Должник.ИНН))
					res = h_validation_msg.error(res, "ИНН должника не может быть пустым");
				if (!data.Кредитор || !data.Кредитор.ИНН || '' == h_validation_msg.trim(data.Кредитор.ИНН))
					res = h_validation_msg.error(res, "ИНН кредитора не может быть пустым");

				return res;
			}

			controller.LoadApplication = function () {
				var self = this;
				self.model = self.GetFormContent();
				if (options.from == 'datamart') {
					if (options.id_MData)
						self.SaveFromDatamart(true, function () { window.open(base_url + '?action=application.load-document&from=datamart&id_MData=' + options.id_MData); });
					else self.SaveFromDatamart(false, function (id_MData) { options.id_MData = id_MData; window.open(base_url + '?action=application.load-document&from=datamart&id_MData=' + id_MData); });
				}
				else if (self.model.Кредитор.email || '' != self.model.Кредитор.email)
					self.BeforLoad();
				else
					self.SetEmail();
			}

			controller.SaveFromDatamart = function (update,on_done)
			{
				var self = this;
				var data= self.model= self.GetFormContent();
				if (data.Приложения_Массив)
					delete data.Приложения_Массив;
				var dataStrUri = encodeURIComponent(JSON.stringify(data));
				var ajaxurl = base_url + '?action=application.save&update=' + update;
				var v_ajax = h_msgbox.ShowAjaxRequest("Сохранение данных", ajaxurl);
				v_ajax.ajax({
					dataType: "json"
					, type: 'POST'
					, data: { data: dataStrUri}
					, cache: false
					, success: function (responce, textStatus)
						{
							if (responce.status)
							{
								on_done(responce.id_MData);
							}
							else if (responce.notice == "duplicated_application")
							{
								var btnOk = 'Заменить';
								h_msgbox.ShowModal({
									title: 'Предупреждение'
									, html: 'Вы уже составляли такое заявление. <br> Замениить его текущим?'
									, buttons: [btnOk, 'Отмена']
									, id_div: "cpw-ama-application-preview-replace-application"
									, onclose: function (btn, dlg_div) {
										if (btn == btnOk) {
											self.SaveFromDatamart(true, on_done);
										}
									}
								});
							}
						}
				});
			}

			controller.BeforLoad = function () {
				var self = this;
				self.model = self.GetFormContent();
				var data =  self.model;
			
				if (data.Приложения_Массив) delete data.Приложения_Массив;
				var dataStrUri = encodeURIComponent(JSON.stringify(data));
				var ajaxurl = base_url + '?action=application.befor-load-document';
				var v_ajax = h_msgbox.ShowAjaxRequest("Передача информации для скачивания документа", ajaxurl);
				v_ajax.ajax({
					type: 'POST'
					, dataType: "json"
					, data: { data: dataStrUri }
					, cache: false
					, success: function (responce, textStatus) {
						if (responce.status)
							self.SendMail();
					}
				});
			}

			controller.SetEmail = function () {
				var self = this;
				var set_email = c_set_email();
				set_email.SetFormContent({ email: self.model.Кредитор.email });
				var btnOk = 'Подтвердить';
				h_msgbox.ShowModal({
					title: 'Изменить Email'
					, controller: set_email
					, buttons: [btnOk, 'Отмена']
					, id_div: "cpw-ama-application-preview-set-email"
					, onclose: function (btn, dlg_div) {
						if (btn == btnOk) {

							self.model.Кредитор.email = set_email.GetFormContent().email;
							self.BeforLoad();
							set_email.Destroy();
							$(dlg_div).dialog("close");
							return false;
						}
					}
				});
			}

			controller.Login = function () {
				var self = this;
				var c_login = c_login_form();
				c_login.SetFormContent({ Login: self.model.Кредитор.email });
				h_msgbox.ShowModal({
					title: 'Вход в аккаунт'
					, controller: c_login
					, buttons: ['Отмена']
				});
			}

			controller.SendMail = function () {
				var self = this;
				var ajaxurl = base_url + '?action=application.send.mail';
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка сообщения для подтверждения email", ajaxurl);
				v_ajax.ajax(
					{
						dataType: "json"
						, type: 'GET'
						, processData: false
						, cache: false
						, success: function (response, textStatus)
						{
							if (response.status)
								self.Confirmation(response.id_MData);
							else if (response.notice == 'duplicated_email')
							{
								var btnEnter = 'Войти';
								var btnEdit = 'Изменить';
								h_msgbox.ShowModal({
									title: 'Предупреждение'
									, html: 'Такой Email уже зарегистрирован на Витрине данных ПАУ. <br> Войдите в аккаунт или измениите email'
									, buttons: [btnEdit, btnEnter, 'Отмена']
									, id_div: "cpw-ama-application-preview-set-email"
									, onclose: function (btn, dlg_div) {
										if (btn == btnEdit) {
											$("#cpw-ama-application-preview-set-email").dialog("close");
											self.SetEmail();
											return false;
										}
										if (btn == btnEnter) {
											$("#cpw-ama-application-preview-set-email").dialog("close");
											self.Login();
											return false;
										}
									}
								});
							}
						}
					});
			}

			controller.Confirmation = function (id_MData) {
				var self = this;
				var confirmation = c_confirmation(self.model.Кредитор.email);
				var btnOk = 'Подтвердить';
				var btnEdit = 'Изменить Email';
				h_msgbox.ShowModal({
					title: 'Скачать заявление'
					, controller: confirmation
					, buttons: [btnOk, btnEdit, 'Отмена']
					, id_div: "cpw-ama-application-preview-confirm"
					, onclose: function (btn, dlg_div) {
						if (btn == btnOk) {
							var token = confirmation.GetFormContent().confirmation_token;
							var ajaxurl = base_url + '?action=application.confirmation&token=' + token;
							var v_ajax = h_msgbox.ShowAjaxRequest("Проверка пароля на серевре", ajaxurl);
							v_ajax.ajax(
								{
									dataType: "json"
									, type: 'GET'
									, async: false
									, processData: false
									, cache: false
									, success: function (response, textStatus) {
										if (response.status) {
											confirmation.Destroy();
											$(dlg_div).dialog("close");
											window.open(base_url + '?action=application.load-document&id_MData=' + id_MData);
											document.location.href = '/dm/ui.php?enable_viewers2&section=outcome&token=' + token;
										}
										else {
											$(confirmation.fastening.selector + ' #notice').css("display", "block");
										}
									}
								});
							return false;
						}
						else if (btn == btnEdit) {
							self.SetEmail();
							confirmation.Destroy();
							$(dlg_div).dialog("close");
							return false;
						}
					}
				});
			}

			controller.Render_application = function ()
			{
				//var self = this;
				var sel = this.fastening.selector;
				$(sel + ' div.cpw-ama-application-preview > div.content').html(tpl_application_doc(this.model));
				/*$(sel + ' #header1').click(function (e) { self.Edit('header1', 500, 200); });
				$(sel + ' #header2').click(function (e) { self.Edit('header2', 800, 800); });
				$(sel + ' #first_p').click(function (e) { self.Edit('first_p', 800, 500); });
				$(sel + ' #second_p').click(function (e) { self.Edit('second_p', 800, 350); });
				$(sel + ' #third_p').click(function (e) { self.Edit('third_p', 800, 300); });
				$(sel + ' #fourth_p').click(function (e) { self.Edit('fourth_p', 800, 400); });
				$(sel + ' #fifth_p').click(function (e) { self.Edit('fifth_p', 800, 300); });
				$(sel + ' #sixth_p').click(function (e) { self.Edit('sixth_p', 800, 400); });
				$(sel + ' #sign').click(function (e) { self.Edit('sign', 800, 250); });
				$(sel + ' #attachments').click(function (e) { self.Attachments(); });*/
			}

			controller.Edit = function (type_c, width, height)
			{
				var self = this;
				var c_sentence= c_control(type_c, width, height, self.model);

				c_sentence.SetFormContent(self.model);
				var btnOk = 'Сохранить';
				h_msgbox.ShowModal
					({
						title: 'Редактировать'
						, controller: c_sentence
						, width: 800
						, height: 800
						, buttons: [btnOk, 'Отмена']
						, id_div: "cpw-form-ama-application-sentence"
						, onclose: function (btn, dlg_div)
						{
							if (btn == btnOk)
							{
								self.model = c_sentence.GetFormContent();
								if (type_c == 'first_p') 
									self.model.Решение.Тип = $(c_sentence.fastening.selector + ' #type').val();
								self.Render_application();
								c_sentence.Destroy();
								$(dlg_div).dialog("close");
								return false;
							}
						}
					});
			}

			controller.Attachments = function () {
				var self = this;
				var attachments = c_attachments({ base_url: base_url });

				attachments.transfer_id_to_main = function (id) {
					self.model = self.GetFormContent();
					self.model.id_application = id;
				}
				attachments.get_info_from_main = function () {
					self.model = self.GetFormContent();
					var ans = {
						id_application: self.model.id_application ? self.model.id_application : '',
						inn_creditor: self.model.Кредитор.ИНН ? self.model.Кредитор.ИНН : '',
						inn_debtor: self.model.Должник ? (self.model.Должник.ИНН ? self.model.Должник.ИНН : '') : ''
					}
					return ans;
				}
				attachments.del_attachments = function (id) {
					var fc_dom_item = $(sel + ' [model-selector="Приложения"]');
					model = [];
					c_attachments.fastening.set_fc_model_value(fc_dom_item, model);
					c_attachments.fastening.on_after_render();
				}

				attachments.SetFormContent(self.model.Приложения_Массив);
				var btnOk = 'Сохранить';
				h_msgbox.ShowModal
					({
						title: 'Редактировать'
						, controller: attachments
						, width: 800
						, height: 800
						, buttons: [btnOk, 'Отмена']
						, id_div: "cpw-form-ama-application-sentence"
						, onclose: function (btn, dlg_div) {
							if (btn == btnOk) {
								attachments.model = attachments.GetFormContent();
								self.model.Приложения_Массив = attachments.model;
								self.model.Приложения = '';
								for (i = 0; i < attachments.model.length; i++) {
									self.model.Приложения += ('<li>' + attachments.model[i].Наименование + '</li>');
								}
								self.Render_application();
								attachments.Destroy();
								$(dlg_div).dialog("close");
								return false;
							}
						}
					});
			}

			return controller;
		}
	});