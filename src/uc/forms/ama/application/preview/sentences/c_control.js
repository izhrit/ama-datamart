define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/preview/sentences/e_header1.html'
	, 'tpl!forms/ama/application/preview/sentences/e_header2.html'
	, 'tpl!forms/ama/application/preview/sentences/e_first_p.html'
	, 'tpl!forms/ama/application/preview/sentences/e_second_p.html'
	, 'tpl!forms/ama/application/preview/sentences/e_third_p.html'
	, 'tpl!forms/ama/application/preview/sentences/e_fourth_p.html'
	, 'tpl!forms/ama/application/preview/sentences/e_fifth_p.html'
	, 'tpl!forms/ama/application/preview/sentences/e_sixth_p.html'
	, 'tpl!forms/ama/application/preview/sentences/e_sign.html'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
],
	function (c_fastened, tpl_header1, tpl_header2, tpl_first_p, tpl_second_p, tpl_third_p, tpl_fourth_p, tpl_fifth_p, tpl_sixth_p, tpl_sign, h_msgbox, h_validation_msg) {
		return function (options_arg, width, height, model) {
			var tpl;
			switch (options_arg) {
				case 'header1':
					tpl = tpl_header1;
					break;
				case 'header2':
					tpl = tpl_header2;
					break;
				case 'first_p':
					tpl = tpl_first_p;
					break;
				case 'second_p':
					tpl = tpl_second_p;
					break;
				case 'third_p':
					tpl = tpl_third_p;
					break;
				case 'fourth_p':
					tpl = tpl_fourth_p;
					break;
				case 'fifth_p':
					tpl = tpl_fifth_p;
					break;
				case 'sixth_p':
					tpl = tpl_sixth_p;
					break;
				case 'sign':
					tpl = tpl_sign;
					break;
			}

			var controller = c_fastened(tpl);

			var base_Render = controller.Render;
			controller.Render = function (sel) {
				base_Render.call(this, sel);
				var self = this;
				if (options_arg == 'first_p') {
					$(sel + ' #type').val(self.GetFormContent().Решение.Тип);
				}
				if (options_arg == 'fourth_p') {
					$(sel + ' #mytextarea1').val(model.Основание);
					$(sel + ' #mytextarea1').tinymce({
						menubar: false
						, statusbar: false
						, language: "ru"
					});
				}

			}

			controller.size = { width: width, height: height };

			return controller;
		}
	});