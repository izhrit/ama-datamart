define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/main/e_application.html'
	, 'forms/ama/application/attachment/c_attachments'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
	, 'forms/ama/application/steps/s1_debtor/c_app_s1_debtor'
	, 'forms/ama/application/steps/s2_creditor/c_app_s2_creditor'
	, 'forms/ama/application/steps/s3_circumstances/c_app_s3_circumstances'
],
function (c_fastened, tpl, c_attachments, h_msgbox, h_validation_msg, c_app_s1_debtor, c_app_s2_creditor, c_app_s3_circumstances)
{
	return function (options_arg)
	{
		var from = (options_arg && options_arg.from) ? options_arg.from : 'application';
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var options = {
			from: from,
			field_spec:
			{
				 О_должнике: { controller: function () { return c_app_s1_debtor({ base_url: base_url, from: from }); } }
				,О_кредиторе: { controller: function () { return c_app_s2_creditor({ base_url: base_url, from: from }); } }
				,Обстоятельства: { controller: function () { return c_app_s3_circumstances({ base_url: base_url, from: from }); } }
				,Приложения: { controller: function () { return c_attachments({ base_url: base_url, from: from }); } }
			}
		};
			
		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' #cpw-application-tabs').on( "tabsactivate", function( event, ui ) { self.OnTabActivate(event,ui); } );

			$(sel + ' button.to-creditor').click(function () { $(sel + ' a[href="#cpw-application-tab-creditor"]').click(); });
			$(sel + ' button.to-circumstances').click(function () { $(sel + ' a[href="#cpw-application-tab-circumstances"]').click(); });
			$(sel + ' button.to-attachments').click(function () { $(sel + ' a[href="#cpw-application-tab-attachments"]').click(); });

			this.fastening.get_fc_controller('О_должнике').OnActivate();

			var control_attachments = this.fastening.get_fc_controller('Приложения');
			control_attachments.transfer_id_to_main = function (id) {
				self.model = self.GetFormContent();
				self.model.id_application = id;
			}
			control_attachments.get_info_from_main = function () {
				self.model = self.GetFormContent();
				var ans = {
					id_application: self.model.id_application ? self.model.id_application : '',
					inn_creditor: self.model.Кредитор.ИНН ? self.model.Кредитор.ИНН : '',
					inn_debtor: self.model.Должник ? (self.model.Должник.ИНН ? self.model.Должник.ИНН : '') : ''
				}
				return ans;
			}
			control_attachments.del_attachments = function (id) {
				var fc_dom_item = $(sel + ' [data-fc-selector="Приложения"]');
				model = [];
				control_attachments.fastening.set_fc_model_value(fc_dom_item, model);
				control_attachments.fastening.on_after_render();
			}

			$(sel + ' div.row button#view').click(function (e) { self.View(); });
			$(sel + ' div.row button#save').click(function (e) { self.Save(); });
		}

		controller.OnTabActivate = function (e, ui)
		{
			switch (ui.newPanel.attr('id'))
			{
				case 'cpw-application-tab-debtor':
					this.fastening.get_fc_controller('О_должнике').OnActivate();
					break;
				case 'cpw-application-tab-creditor':
					this.fastening.get_fc_controller('О_кредиторе').OnActivate();
					break;
			}
		}

		controller.View = function ()
		{
			var m= this.model = this.GetFormContent();
			var self = this;

			var data = {
				Основание: tinyMCE.get('mytextarea').getContent()
				, Должник: m.Должник
				, Кредитор: m.Кредитор
				, АУ: m.АУ
				, Решение: m.Решение
				, Приложения: []
				, id_application: !m.id_application ? '' : m.id_application
			}
			var Приложения= self.model.Приложения;
			for (i = 0; i < Приложения.length; i++)
			{
				var a= Приложения[i];
				data.Приложения.push({ Наименование: a.Наименование, id: a.id });
			}
				
			h_validation_msg.IfOkValidateResult({
				validation_result: self.Validate(data)
				, title_reject: 'Невозможно сформировать заявление потому что'
				, on_validate_ok_func: function () {
					var ajaxurl = base_url + '?action=application.befor-view-document&from=' + from;
					var v_ajax = h_msgbox.ShowAjaxRequest("Передача информации для генерации документа", ajaxurl);
					v_ajax.ajax({
						type: 'POST'
						, data: data
						, cache: false
						, success: function (responce, textStatus)
						{
							document.location.href = '/dm/application_preview.php';
						}
					});
				}
			});
		}

		return controller;
	}
});