﻿define(['forms/ama/application/main/c_application'],
	function (CreateController) {
		var form_spec =
		{
			CreateController: CreateController
			, key: 'application'
			, Title: 'Требование о включении в реестр требований кредиторов'
		};
		return form_spec;
	});
