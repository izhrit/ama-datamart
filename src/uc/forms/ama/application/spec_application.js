define(function ()
{
	return {

		controller: {
			"application_main": {
				path: 'forms/ama/application/main/c_application'
				, title: 'главная форма подачи заявления о требовании кредитора'
			}
			,"application_debtor": {
				path: 'forms/ama/application/debtor/c_debtor'
				, title: 'свойства должника в форме подачи заявления о требовании кредитора'
			}
			,"application_creditor": {
				path: 'forms/ama/application/creditor/c_creditor'
				, title: 'свойства кредитора в форме подачи заявления о требовании кредитора'
			}
			,"app_s1_debtor": {
				path: 'forms/ama/application/steps/s1_debtor/c_app_s1_debtor'
				, title: 'первый шаг подачи заявления в включении в реестр требований кредитора'
			}
			,"app_s2_creditor": {
				path: 'forms/ama/application/steps/s2_creditor/c_app_s2_creditor'
				, title: 'второй шаг подачи заявления в включении в реестр требований кредитора'
			}
			,"app_s3_circumstances": {
				path: 'forms/ama/application/steps/s3_circumstances/c_app_s3_circumstances'
				, title: 'третий шаг подачи заявления в включении в реестр требований кредитора'
			}
			,"application_attachments": {
				path: 'forms/ama/application/attachment/c_attachments'
				, title: 'прилагаемые материалы к заявлению о требовании кредитора'
			}
			,"application_preview": {
				path: 'forms/ama/application/preview/c_preview'
				, title: 'превью заявления о требовании кредитора'
			}
		}

		, content: {
			"application_debtor-01sav": {
				path: 'txt!forms/ama/application/debtor/tests/contents/01sav.json.etalon.txt'
			}
			,"application_s1_debtor-01sel": {
				path: 'txt!forms/ama/application/steps/s1_debtor/tests/contents/01sel.json.etalon.txt'
			}
			,"application_creditor-01sav": {
				path: 'txt!forms/ama/application/creditor/tests/contents/01sav.json.etalon.txt'
			}
			,"application_s2_creditor-01sel": {
				path: 'txt!forms/ama/application/steps/s2_creditor/tests/contents/01sel.json.etalon.txt'
			}
			,"application_s3_circumstances-01sav": {
				path: 'txt!forms/ama/application/steps/s3_circumstances/tests/contents/01sav.json.etalon.txt'
			}
			,"application-01sav": {
				path: 'txt!forms/ama/application/main/tests/contents/01sav.json.etalon.txt'
			}
		}
	};

});