﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/creditor/e_creditor.html'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_validation_msg)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 920, height: 540 };

		controller.Validate = function (model)
		{
			var vr= [];
			var block = function (msg) { vr.push({check_constraint_result:false,description:msg}); }

			var model = this.GetFormContent();

			if ('' == h_validation_msg.trim(model.Наименование.Полное))
				block("Необходимо указать полное наименование кредитора");
			if ('' == h_validation_msg.trim(model.Наименование.Краткое))
				block("Необходимо указать сокращённое наименование кредитора");

			if ('' == h_validation_msg.trim(model.ИНН))
				block("Необходимо указать ИНН кредитора");
			if ('' == h_validation_msg.trim(model.Руководитель))
				block("Необходимо указать руководителя кредитора");

			if ('' == h_validation_msg.trim(model.Адрес.Юридический))
				block("Необходимо указать юридической адрес кредитора");
			if ('' == h_validation_msg.trim(model.Адрес.Почтовый))
				block("Необходимо указать почтовый адрес кредитора");

			return 0==vr.length ? null : vr;
		}

		return controller;
	}
});