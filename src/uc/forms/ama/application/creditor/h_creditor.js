﻿define([
	'forms/base/h_msgbox'
	, 'forms/ama/application/creditor/c_creditor'
	, 'forms/base/h_validation_msg'
],
function (h_msgbox, c_cust_creditor, h_validation_msg)
{
	var helper = {};

	helper.ModalEditCreditor= function (base_url, application_creditor, on_done)
	{
		var c_creditor = c_cust_creditor({ base_url: base_url });
		c_creditor.SetFormContent(application_creditor);
		var btnOk = 'Сохранить информацию о кредиторе';
		h_msgbox.ShowModal
			({
				title: 'Информация о кредиторе (заявителе требования)'
				, controller: c_creditor
				, width: 900
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-application-creditor"
				, onclose: function (btn, dlg_div)
				{
					if (btn == btnOk)
					{
						h_validation_msg.IfOkValidateResult(
						{
							validation_result: c_creditor.Validate()
							, title_reject: 'Невозможно сохранить данные кредитора потому что'
							, on_validate_ok_func: function () {
								var creditor = c_creditor.GetFormContent();
								on_done(creditor);
								c_creditor.Destroy();
								$(dlg_div).dialog("close");
							}
						});
						return false;
					}
				}
			});
	}

	return helper;
});