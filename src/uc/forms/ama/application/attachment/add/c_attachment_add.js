﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/attachment/add/e_attachment_add.html'
	, 'forms/ama/datamart/manager/committee/question/variants/c_question_variants'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
],
	function (c_fastened, tpl, c_question_variants, h_msgbox, h_validation_msg) {
		return function (options) {
			var controller = c_fastened(tpl, options);

			controller.size = { width: 600, height: 300 };

			var base_Render = controller.Render;
			controller.Render = function (sel) {
				base_Render.call(this, sel);

				var self = this;
				if (options && options.Файл)
					$(sel + ' #attachment').attr(defaultvalue,options.Файл);
				$(sel + ' #attachment').change(function () { var files = this.files; self.isAdd(files); });
			}

			controller.isAdd = function (files) {
				var sel = this.fastening.selector;
				var self = this;
				//проверка размера файла и загружен ли он(надо напсисать на это фалидацию)
				var maxFileSize = 52428800000;
				var file = $(sel + ' #attachment');
				var flag = false;
				if (file.prop('files').length) {
					if (file.prop('files')[0].size <= maxFileSize) {
						flag = true;
					}
				}
				//создание модели и селектора
				this.model = this.GetFormContent();
				var sel = this.fastening.selector;
				//сохранение файла в модель
				if (flag) self.model.Файл = file.prop('files')[0];
				else self.model.Файл = '';
				//заполнение наименования названием файла, если оно еще не заполненно
				if (!$(sel + ' textarea[id="name"]').val()) {
					$(sel + ' textarea[id="name"]').val(files[0].name);
					this.model.Наименование = files[0].name;
				}
				this.SetFormContent(this.model);
			}

			controller.Validate = function () {
				var res = null;
				var model = this.GetFormContent();
				if (!model.Наименование || '' == model.Наименование)
					res = h_validation_msg.error(res, "Наименование не может быть пустым");
				//if (!model.Файл)
				//	res = h_validation_msg.error(res, "Не выбран файл");
				return res;
			}

			return controller;
		}
	});