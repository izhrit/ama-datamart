﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/attachment/e_attachments.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/application/attachment/add/c_attachment_add'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_msgbox, c_attachment_add, h_validation_msg) 
{
	return function (options_arg) 
	{
		var controller = c_fastened(tpl, options_arg);
		var from = ((options_arg && options_arg.from) ? options_arg.from : 'datamart');

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.add').click(function () { self.OnAdd(); });

			var on_delete = function (e) { self.OnDelete(e); }
			var on_edit_button = function (e) { self.OnEdit(e); }
			var on_edit = function (e) {
				if (!$(e.target).hasClass('delete') && !$(e.target).parent().hasClass('delete') &&
					!$(e.target).hasClass('edit') && !$(e.target).parent().hasClass('edit')) {
					self.OnEdit(e);
				}
			}
			this.fastening.on_after_render = function () {
				$(sel + ' button.delete').unbind('click', on_delete).click(on_delete);
				$(sel + ' button.edit').unbind('click', on_edit_button).click(on_edit_button);
				$(sel + ' [fc-type="array-item"]').unbind('click', on_edit).click(on_edit);
			}
			this.fastening.on_after_render();
		}

			controller.OnDelete = function (e) {
				e.stopPropagation();
				var i_item = parseInt($(e.target).parents('[data-fc-type="array-item"]').attr('data-array-index'));
				var self = this;
				var fc_dom_item = $(this.fastening.selector);
				var model = this.fastening.get_fc_model_value(fc_dom_item);
				var item = model[i_item];
				var btnOk = 'Да, удалить';
				h_msgbox.ShowModal
					({
						title: 'Подтверждение удаления приложения №' + (i_item + 1)
						, html: 'Вы действительно хотите удалить документ <br/>'
							+ '№' + (i_item + 1) + ' <span style="font-weight: 600;">' + item.Наименование + '</span>?'
						, width: 500, height: 250
						, buttons: [btnOk, 'Нет, не удалять']
						, onclose: function (btn) {
							var del = function () {
								model.splice(i_item, 1);
								self.fastening.set_fc_model_value(fc_dom_item, model);
								$(self.fastening.selector).trigger('model_change');
							}
							if (btn == btnOk) {
								if (!item.id)
									del();
								else
								{
									var base_url = '/dm/ui-backend.php';
									var ajaxurl = base_url + '?action=application.file&file_action=del&id_file=' + item.id;
									var v_ajax = h_msgbox.ShowAjaxRequest("Удалени файла с сервера", ajaxurl);
									v_ajax.ajax({
										processData: false,
										contentType: false,
										type: 'GET',
										success: function (respose) {
											del();
										}
									});
								}
							}
						}
					});
			}

			controller.OnEdit = function (e) {
				var t = $(e.target);
				if ('array-item' != t.attr('data-fc-type'))
					t = t.parents('[data-fc-type="array-item"]');
				var i_item = parseInt(t.attr('data-array-index'));

				var fc_dom_item = $(this.fastening.selector);
				var model = this.fastening.get_fc_model_value(fc_dom_item);
				var item = model[i_item];
				var c_add = c_attachment_add();
				c_add.SetFormContent(item);
				this.DoEdit(i_item, c_add);
			}

			controller.DoEdit = function (i_item, c_add) {
				var self = this;
				var btnOk = 'Обновить';
				h_msgbox.ShowModal
					({
						title: 'Редактирование параметров приложеня №' + (i_item + 1)
						, controller: c_add
						, buttons: [btnOk, 'Отмена']
						, id_div: "cpw-ama-aplication-attachment-add"
						, onclose: function (btn, dlg_div) {
							if (btn == btnOk) {
								h_validation_msg.IfOkWithValidateResult(c_add.Validate(), function () {
									var fc_dom_item = $(self.fastening.selector);
									var model = self.fastening.get_fc_model_value(fc_dom_item);
									var attacment = c_add.GetFormContent();
									var on_done = function () {
										model[i_item] = attacment;
										self.fastening.set_fc_model_value(fc_dom_item, model);
										$(self.fastening.selector).trigger('model_change');
										c_add.Destroy();
										$(dlg_div).dialog("close");
									}
									//берем файл из формы и готовим к отправке
									if (!attacment.Файл)
										on_done();
									else
									{
										var File = new FormData();
										File.append('file', attacment.Файл);

										//отправляем файл через ajax и получаем id файла
										var base_url = '/dm/ui-backend.php';
										var ajaxurl = encodeURI(base_url + '?action=application.file&file_action=edit'
											+ '&name=' + attacment.Наименование
											+ '&id_file=' + attacment.id);
										var v_ajax = h_msgbox.ShowAjaxRequest("Передача файла на сервер", ajaxurl);
										v_ajax.ajax({
											data: File,
											processData: false,
											contentType: false,
											type: 'POST',
											success: function () {
												on_done();
											}
										});
									}

								})
								return false;
							}
						}
					});
			}

			controller.OnAdd = function ()
			{
				var self = this;
				var sel = this.fastening.selector;
				var c_add = c_attachment_add();
				var btnOk = 'Сохранить документ';
				h_msgbox.ShowModal
				({
					title: 'Приложить документ'
					, controller: c_add
					, buttons: [btnOk, 'Отмена']
					, id_div: "cpw-ama-aplication-attachment-add"
					, onclose: function (btn, dlg_div) {
						if (btn == btnOk) {
							h_validation_msg.IfOkWithValidateResult(c_add.Validate(), function () {
								//готовим модель в которую будем добавлять новую строку с файлом
								var fc_dom_item = $(sel + ' [model-selector="Приложения"]');
								var model = self.fastening.get_fc_model_value(fc_dom_item);
								if (!model)
									model = [];

								var attachment = c_add.GetFormContent();//получение модели
								var on_done = function (data) //функция добавления строки
								{
									attachment.id = data.id_file;
									//добавляем название файла и id файла в основную форму
									model.push(attachment);
									self.fastening.set_fc_model_value(fc_dom_item, model);
									self.fastening.on_after_render();
									$(sel).trigger('model_change');
									c_add.Destroy();
									$(dlg_div).dialog("close");
								}
								if (!attachment.Файл) {
									on_done({ id_file: ''});
								}
								else {
									//берем файл из формы и готовим к отправке
									var File = new FormData();
									File.append('file', attachment.Файл);
									//отправляем файл через ajax и получаем id файла
									var base_url = '/dm/ui-backend.php';
									var ajaxurl = encodeURI(base_url + '?action=application.file&file_action=add'
										+ '&name=' + attachment.Наименование);
									var v_ajax = h_msgbox.ShowAjaxRequest("Передача файла на сервер", ajaxurl);
									v_ajax.ajax({
										data: File,
										dataType: 'json',
										processData: false,
										contentType: false,
										type: 'POST',
										success: function (data) {
											on_done(data);
										}
									});
								}
							});
							return false;
						}
					}
				});
			}

			return controller;
		}
	});