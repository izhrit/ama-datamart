﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/debtor/edit_sentences/second/e_sentence_second.html'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
],
	function (c_fastened, tpl, h_msgbox, h_validation_msg) {
		return function (options_arg) {
			var controller = c_fastened(tpl);

			controller.size = { width: 800, height: 400 };

			controller.Validate = function () {
				var res = null;
				var model = this.GetFormContent();
				if ('' == h_validation_msg.trim(model.АУ.Наименование))
					res = h_validation_msg.error(res, "Наименование суда не может быть пустым");
				if ('' == h_validation_msg.trim(model.АУ.СРО))
					res = h_validation_msg.error(res, "Наименование суда не может быть пустым");
				if ('' == h_validation_msg.trim(model.Решение.Сообщение.Дата))
					res = h_validation_msg.error(res, "Наименование суда не может быть пустым");
				if ('' == h_validation_msg.trim(model.Решение.Сообщение.Номер))
					res = h_validation_msg.error(res, "Наименование суда не может быть пустым");
				if ('' == h_validation_msg.trim(model.Решение.Процедура))
					res = h_validation_msg.error(res, "Наименование суда не может быть пустым");
				//return res;
				return null;
			}

			return controller;
		}
	});