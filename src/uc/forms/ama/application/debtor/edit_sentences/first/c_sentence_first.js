﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/debtor/edit_sentences/first/e_sentence_first.html'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
],
	function (c_fastened, tpl, h_msgbox, h_validation_msg) {
		return function (options_arg) {
			var controller = c_fastened(tpl);

			controller.size = { width: 800, height: 300 };

			var base_Render = controller.Render;
			controller.Render = function (sel) {
				base_Render.call(this, sel);
				var self = this;
				$(sel + ' #type').val(self.GetFormContent().Решение.Тип);

			}

			var check_date_dd_mm_yyyyy = function (str) {
				var regexp = /^(\d{2})\.(\d{2})\.(\d{4})$/;
				return regexp.test(str);
			}

			controller.Validate = function () {
				var res = null;
				var model = this.GetFormContent();
				var sel = this.fastening.selector;
				model.Решение.Тип = $(sel + ' #type').val();
				if (model.Решение.Дата && !check_date_dd_mm_yyyyy(model.Решение.Дата))
					res = h_validation_msg.error(res, "Дата принятия решения должна быть указана в форме ДД.ММ.ГГГГ");
				return res;
			}

			return controller;
		}
	});