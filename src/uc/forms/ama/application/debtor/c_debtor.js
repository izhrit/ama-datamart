﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/debtor/e_debtor.html'
	, 'forms/base/h_validation_msg'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_validation_msg, h_msgbox)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 920, height: 768 };

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' select[data-fc-selector="Должник.Тип"]').change(function () { self.OnChangeType(); });
			$(sel + ' button.kad-arbitr').click(function () { self.OnOpenKadArbitr(); });
			$(sel + ' button.egrul').click(function () { self.OnOpenEgrul(); });
			$(sel + ' button.efrsb').click(function () { self.OnOpenEfrsb(); });
		}

		var base_GetFormContent= controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var res= base_GetFormContent.call(this);
			var d= res.Должник;
			switch (d.Тип)
			{
				case 'гражданин':
					delete d.Наименование;
					break;
				case 'организация':
					delete d.Фамилия;
					delete d.Имя;
					delete d.Отчество;
					break;
			}
			return res;
		}

		controller.OnChangeType = function ()
		{
			var sel= this.fastening.selector;
			var t= $(sel + ' select[data-fc-selector="Должник.Тип"]').val();
			$(sel + ' div.cpw-ama-aplication-debtor').attr('data-debtor-type',t);
		}

		controller.OnOpenKadArbitr = function ()
		{
			var sel= this.fastening.selector;
			var Номер_дела= $(sel + ' input[data-fc-selector="Судебный_акт_о_введении_процедуры.Номер_дела"]').val();
			if (Номер_дела && null != Номер_дела && '' != Номер_дела)
			{
				var url = 'https://kad.arbitr.ru/Card?number=' + encodeURI(Номер_дела);
				window.open(url);
			}
			else
			{
				h_msgbox.ShowModal({ 
					title: 'Невозможно открыть карточку судебного дела', width: 600
					, html:'Чтобы открыть карточку судебного дела, необходимо указать номер дела должника!'
				});
			}
		}

		controller.OnOpenEgrul = function ()
		{
			var sel= this.fastening.selector;
			
			var inn= $(sel + ' input[data-fc-selector="Должник.ИНН"]').val();
			if (!inn || null == inn || '' == inn)
			{
				h_msgbox.ShowModal({ 
					title: 'Невозможно открыть выписку ЕГРЮЛ', width: 600
					, html:'Чтобы открыть выписку ЕГРЮЛ, необходимо заполнить поле ИНН!'
				});
			}
			else
			{
				var url= 'https://probili.ru/request/egrul/?inn=' + inn;
				var ogrn= $(sel + ' input[data-fc-selector="Должник.ОГРН"]').val();
				if (''!=ogrn)
					url+= '&ogrn=' + ogrn;
				window.open(url);
			}
		}

		controller.OnOpenEfrsb = function ()
		{
			var sel = this.fastening.selector;
			var MessageGUID = $(sel + ' input[data-fc-selector="Сообщение_на_ефрсб_о_введении_процедуры.MessageGUID"]').val();
			if (MessageGUID && null != MessageGUID && '' != MessageGUID)
			{
				var url = "https://old.bankrot.fedresurs.ru/MessageWindow.aspx?ID=" + MessageGUID;
				window.open(url, "Сообщение", "toolbar=no,location=no,status=no,menubar=no,resizable=yes,directories=no,scrollbars=yes,width=1000,height=600");
			}
			else
			{
				h_msgbox.ShowModal({ 
					title: 'Невозможно открыть сообщение ЕФРСБ', width: 600
					, html:'Чтобы открыть сообщение ЕФРСБ, необходимо заполнить поле ID!'
				});
			}
		}

		controller.Validate = function ()
		{
			var vr= [];
			var block = function (msg) { vr.push({check_constraint_result:false,description:msg}); }
			var warn = function (msg) { vr.push({description:msg}); }

			var model = this.GetFormContent();

			var должник= model.Должник;
			if (должник.Наименование && '' == h_validation_msg.trim(должник.Наименование))
				block("Неободимо указать наименование должника");
			if (должник.Фамилия && '' == h_validation_msg.trim(должник.Фамилия))
				block("Неободимо указать фамилию должника");
			if ('' == h_validation_msg.trim(должник.Адрес))
				block("Неободимо указать адрес должника");

			var АУ= model.АУ;
			if ('' == h_validation_msg.trim(АУ.Фамилия))
				block("Неободимо указать фамилию арбитражного управляющего");

			if ('' == h_validation_msg.trim(АУ.Адрес))
				block("Неободимо указать адрес арбитражного управляющего");

			if ('' == h_validation_msg.trim(АУ.Имя))
				warn("Желательно указать имя арбитражного управляющего");
			if ('' == h_validation_msg.trim(АУ.Отчество))
				warn("Желательно указать отчество арбитражного управляющего");

			return 0==vr.length ? null : vr;
		}

		return controller;
	}
});