﻿define([
	'forms/base/h_msgbox'
	, 'forms/ama/application/debtor/c_debtor'
	, 'forms/ama/application/debtor/edit_sentences/first/c_sentence_first'
	, 'forms/ama/application/debtor/edit_sentences/second/c_sentence_second'
	, 'forms/base/h_validation_msg'
],
function (h_msgbox, c_cust_debtor, c_sentence_first, c_sentence_second, h_validation_msg)
{
	var helper = {};

	helper.ModalEditDebtor = function (base_url, application_debtor, on_done)
	{
		var c_debtor = c_cust_debtor({ base_url: base_url });
		c_debtor.SetFormContent(application_debtor);
		var btnOk = 'Сохранить информацию о должнике';
		h_msgbox.ShowModal
		({
			title: 'Информация о должнике'
			, controller: c_debtor
			, buttons: [btnOk, 'Отмена']
			, id_div: "cpw-ama-aplication-debtor"
			, onclose: function (btn, dlg_div)
			{
				if (btn == btnOk)
				{
					h_validation_msg.IfOkValidateResult({
						validation_result: c_debtor.Validate()
						, title_reject: 'Невозможно сохранить данные должника потому что'
						, on_validate_ok_func: function ()
						{
							var debtor = c_debtor.GetFormContent();
							on_done(debtor);
							c_debtor.Destroy();
							$(dlg_div).dialog("close");
						}
					});
					return false;
				}
			}
		});
	}

	helper.LoadDebtor = function (BankruptId, base_url_debtor, on_done)
	{
		var self = this;
		var ajaxurl = base_url_debtor + '&BankruptId=' + BankruptId;
		var v_ajax = h_msgbox.ShowAjaxRequest("Загрузка данных о должнике BankruptId=" + BankruptId + " с сервера", ajaxurl);
		v_ajax.ajax(
		{
			dataType: "json"
			, type: 'GET'
			, processData: false
			, cache: false
			, success: function (debtor_info, textStatus)
			{
				var Должник = {
					Наименование: di.DebtorName,
					ИНН: di.DebtorINN,
					ОГРН: di.DebtorOGRN,
					СНИЛС: di.DebtorSNILS,
					BankruptId: di.BankruptId,
					Адрес: di.DebtorAdress
				}
				var АУ = {
					Наименование: di.ManagerName,
					Адрес: di.ManagerAdress,
					СРО: di.SROName,
					ИНН: di.ManagerINN,
					Number: di.ManagerNumber,
					Фамилия: di.ManagerLastName,
					Имя: di.ManagerFirstName,
					Отчество: di.ManagerMiddleName,
				}
				var Решение =
				{
					Процедура: di.ProcedureName,
					Номер: di.DecisionNumber,
					Дата: self.DateFormatter(di.DecisionDate),
					Суд: { Наименование: di.CourtName, Адрес: di.CourtAdress },
					Тип: di.DecisionType,
					Сообщение: { Дата: di.MessageDate, Номер: di.MessageNumber }
				}
				on_done(Должник, АУ, Решение);
			}
		});
	}

		helper.ModalEditFirstSentence = function (base_url, first_sentence, on_done) {
			var c_first = c_sentence_first({ base_url: base_url });
			c_first.SetFormContent(first_sentence);
			var btnOk = 'Сохранить';
			h_msgbox.ShowModal
				({
					title: 'Редактировать'
					, controller: c_first
					, width: 900
					, height: 300
					, buttons: [btnOk, 'Отмена']
					, id_div: "cpw-ama-aplication-sentence-first"
					, onclose: function (btn, dlg_div) {
						if (btn == btnOk) {
							h_validation_msg.IfOkValidateResult({
								validation_result: c_first.Validate()
								, title_reject: 'Невозможно сохранить данные должника потому что'
								, on_validate_ok_func: function () {
									var first_sentence = c_first.GetFormContent();
									on_done(first_sentence);
									c_first.Destroy();
									$(dlg_div).dialog("close");
								}
							});
							return false;
						}

					}
				});
		}

		helper.ModalEditSecondSentence = function (base_url, second_sentence, on_done) {
			var c_second = c_sentence_second({ base_url: base_url });
			c_second.SetFormContent(second_sentence);
			var btnOk = 'Сохранить';
			h_msgbox.ShowModal
				({
					title: 'Редактировать'
					, controller: c_second
					, width: 900
					, height: 300
					, buttons: [btnOk, 'Отмена']
					, id_div: "cpw-ama-aplication-sentence-second"
					, onclose: function (btn, dlg_div) {
						if (btn == btnOk) {
							h_validation_msg.IfOkValidateResult({
								validation_result: c_second.Validate()
								, title_reject: 'Невозможно сохранить данные должника потому что'
								, on_validate_ok_func: function () {
									c_second.Validate();
									var second_sentence = c_second.GetFormContent();
									on_done(second_sentence);
									c_second.Destroy();
									$(dlg_div).dialog("close");

								}
							});
							return false;
						}
					}
				});
		}

		helper.DateFormatter = function (date) {
			if (!date || date == '')
				return '';
			else {
				var arr = date.split('-');
				var ans = '';
				for (i = arr.length - 1; i > 0; i--)
					ans += (arr[i] + '.');
				ans += arr[0];
				return ans;
			}
		}

	return helper;
});