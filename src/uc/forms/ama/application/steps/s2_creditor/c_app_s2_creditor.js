define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/steps/s2_creditor/e_app_s2_creditor.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/application/creditor/c_creditor'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_msgbox, c_creditor, h_validation_msg)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_select2_creditor= base_url + '?action=application.creditor.select2'

		var options = {
			field_spec:
			{
				Выбранный_кредитор: { ajax: { url: base_url_select2_creditor, dataType: 'json' } }
			}
		};

		var controller = c_fastened(tpl, options);

		var penalty_name= ['Пени','Штрафы','Неустойки','Проценты'];

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self= this;

			$(sel + ' input[data-fc-selector="Долг.Санкции.Одной_суммой"]').change(function () { self.OnChangePenaltyVariant(); });
			$(sel + ' input[data-fc-selector="Банковские_реквизиты.Структурировано"]').change(function () { self.OnBankAccountStructured(); });

			$(sel + ' input[data-fc-selector="Банковские_реквизиты.Структурировано"]').change(function () { self.OnBankAccountStructured(); });

			$(sel + ' button.creditor').click(function () { self.OnCreditor(); });

			for (var i = 0; i < penalty_name.length; i++)
				$(sel + ' input[data-fc-selector="Долг.Санкции.' + penalty_name[i] + '"]').change(function () { self.SafeUpdatePenaltySum(); })

			this.SafeUpdatePenaltySum();
		}

		controller.OnActivate = function ()
		{
			var sel= this.fastening.selector;
			var select2= $(sel + ' [data-fc-selector="Выбранный_кредитор"]');
			if (null==select2.select2('data'))
				select2.select2('open');
		}

		controller.OnChangePenaltyVariant = function ()
		{
			var sel= this.fastening.selector;
			var checked= $(sel + ' input[data-fc-selector="Долг.Санкции.Одной_суммой"]').attr('checked');
			var root_item= $(sel + ' div.cpw-application-step2-creditor');
			var sum_item= $(sel + ' input[data-fc-selector="Долг.Санкции.Сумма"]');
			if ('checked' == checked)
			{
				root_item.attr('data-penalties-mode','short');
				sum_item.attr('readonly',null);
			}
			else
			{
				root_item.attr('data-penalties-mode','expanded');
				sum_item.attr('readonly','readonly');
				this.SafeUpdatePenaltySum();
			}
		}

		controller.SafeUpdatePenaltySum = function ()
		{
			var sel= this.fastening.selector;
			var checked= $(sel + ' input[data-fc-selector="Долг.Санкции.Одной_суммой"]').attr('checked');
			if (!checked)
			{
				var sum= 0;
				for (var i = 0; i < penalty_name.length; i++)
				{
					var value= parseFloat($(sel + ' input[data-fc-selector="Долг.Санкции.' + penalty_name[i] + '"]').val());
					if (value && null!=value && !isNaN(value))
						sum+= value;
				}
				$(sel + ' input[data-fc-selector="Долг.Санкции.Сумма"]').val(sum);
			}
		}

		controller.OnBankAccountStructured = function ()
		{
			var sel= this.fastening.selector;
			var checked= $(sel + ' input[data-fc-selector="Банковские_реквизиты.Структурировано"]').attr('checked');
			$(sel + ' div.cpw-application-step2-creditor').attr('data-bank-account-mode','checked'==checked?'structured':'unstructured');
		}

		controller.OnCreditor = function ()
		{
			var self= this;
			var sel = this.fastening.selector;
			var cc_creditor = c_creditor();
			var select2= $(sel + ' [data-fc-selector="Выбранный_кредитор"]');
			var selection= select2.select2('data');
			var m = {};
			if (null!=selection)
				m= selection.data;
			cc_creditor.SetFormContent(m);
			var btnOk = 'Сохранить информацию о кредиторе-заявителе';
			h_msgbox.ShowModal
			({
				title: 'Информация о кредиторе-заявителе'
				, controller: cc_creditor
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-ama-aplication-creditor"
				, onclose: function (btn, dlg_div)
				{
					if (btn == btnOk)
					{
						h_validation_msg.IfOkValidateResult({
							validation_result: cc_creditor.Validate()
							, title_reject: 'Невозможно сохранить данные заявителя, потому что'
							, on_validate_ok_func: function ()
							{
								m= cc_creditor.GetFormContent();
								select2.select2('data', data_for_select2(m));
								cc_creditor.Destroy();
								$(dlg_div).dialog("close");
							}
						});
						return false;
					}
				}
			});
		}

		var data_for_select2 = function (Кредитор)
		{
			return (null == Кредитор)
				? null
				: {id:'?',text:Кредитор.Наименование.Полное,data:Кредитор}
		}

		var codec = {
			Encode: function (data)
			{
				var res = {};
				if (data.Выбранный_кредитор && null!=data.Выбранный_кредитор)
					res.Кредитор= data.Выбранный_кредитор.data;
				res.Контактная_информация= data.Контактная_информация;
				res.Долг= data.Долг;
				var s= res.Долг.Санкции;
				if (!s.Одной_суммой)
				{
					delete s.Сумма;
				}
				else
				{
					delete s.Пени;
					delete s.Штрафы;
					delete s.Неустойки;
					delete s.Проценты;
				}
				res.Банковские_реквизиты= data.Банковские_реквизиты;
				var b= res.Банковские_реквизиты;
				if (b.Структурировано)
				{
					delete b.Текстом;
				}
				else
				{
					delete b.Банк;
					delete b.Рассчётный_счёт;
				}
				return res;
			}
			, Decode: function (data)
			{
				if ('string' == typeof data)
					data= JSON.parse(data);
				data.Выбранный_кредитор = (!data.Кредитор) ? null : data_for_select2(data.Кредитор);
				return data;
			}
		};

		controller.UseCodec(codec);

		return controller;
	}
});