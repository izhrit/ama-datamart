define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/steps/s3_circumstances/e_app_s3_circumstances.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			if (!this.model || null == this.model)
				this.model = {Описание:"<p>Задолженность возникла в результате договорных отношений.</p>"};

			base_Render.call(this,sel);

			$(sel + ' #circumstances-text').tinymce({
				menubar: false
				, statusbar: false
				, language: "ru"
			});
		}

		var base_Destroy= controller.Destroy;
		controller.Destroy = function ()
		{
			var sel= this.fastening.selector;
			base_Destroy.call(this);
			tinymce.remove(sel + " #circumstances-text");
		}

		return controller;
	}
});