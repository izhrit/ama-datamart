include ..\in.lib.txt quiet

wait_text "Коммерсантъ"
shot_check_png ..\..\shots\00new.png

play_stored_lines dm_application_s1_debtor_fields_1

shot_check_png ..\..\shots\01sel.png
wait_click_text "о должнике"
shot_check_png ..\..\shots\01sel_form.png
wait_click_text "Отмена"
shot_check_png ..\..\shots\01sel.png
wait_click_text "о сообщении"
shot_check_png ..\..\shots\01sel_form.png
wait_click_text "Отмена"
shot_check_png ..\..\shots\01sel.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01sel.json.result.txt

wait_click_text "Редактировать"
shot_check_png ..\..\shots\01sel.png
wait_click_text "о должнике"
shot_check_png ..\..\shots\01sel_form.png
wait_click_text "Отмена"
shot_check_png ..\..\shots\01sel.png
wait_click_text "о сообщении"
shot_check_png ..\..\shots\01sel_form.png
wait_click_text "Отмена"
shot_check_png ..\..\shots\01sel.png

exit