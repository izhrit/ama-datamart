define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/application/steps/s1_debtor/e_app_s1_debtor.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/application/debtor/c_debtor'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_msgbox, c_debtor, h_validation_msg)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_select2_debtor = base_url + '?action=efrsb.debtor.select2'
		var base_url_debtor = base_url + '?action=application.debtor';

		var options = {
			field_spec:
			{
				Выбранный_должник: { ajax: { url: base_url_select2_debtor, dataType: 'json' } }
			}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self= this;

			$(sel + ' button.debtor')
				.button({ icons: { primary: 'ui-icon-pencil' } })
				.click(function () { self.OnDebtor(); })
			;
			$(sel + ' button.message')
				.button({ icons: { primary: 'ui-icon-pencil' } })
				.click(function () { self.OnDebtor(); })
			;

			$(sel + ' [data-fc-selector="Выбранный_должник"]').on('select2-selected', function (e) { self.OnSelectDebtor(); })

			this.UpdateMessage();
		}

		controller.OnActivate = function ()
		{
			var sel= this.fastening.selector;
			var select2= $(sel + ' [data-fc-selector="Выбранный_должник"]');
			if (null==select2.select2('data'))
				select2.select2('open');
		}

		controller.OnSelectDebtor = function ()
		{
			var self= this;
			var sel = this.fastening.selector;
			var select2= $(sel + ' [data-fc-selector="Выбранный_должник"]');
			var selected_data= select2.select2('data');
			var BankruptId= selected_data.id;
			var ajaxurl = base_url_debtor + '&BankruptId=' + BankruptId;
			var v_ajax = h_msgbox.ShowAjaxRequest("Загрузка данных о должнике BankruptId=" + BankruptId + " с сервера", ajaxurl);
			v_ajax.ajax(
			{
				dataType: "json", type: 'GET', processData: false, cache: false
				, success: function (di)
				{
					if (!self.model)
						self.model = {};
					CopyHiddenFields(di,self.model);
					select2.select2('data', data_for_select2(di.Должник));
					self.UpdateMessage();
				}
			});
		}

		var CopyHiddenFields = function (from, to)
		{
			to.Судебный_акт_о_введении_процедуры= from.Судебный_акт_о_введении_процедуры;
			to.Должник= from.Должник;
			to.Суд= from.Суд;
			to.АУ= from.АУ;
			to.Сообщение_на_ефрсб_о_введении_процедуры= from.Сообщение_на_ефрсб_о_введении_процедуры;
		}

		controller.UpdateMessage = function ()
		{
			if (this.model && null!=this.model)
			{
				var m= this.model.Сообщение_на_ефрсб_о_введении_процедуры;
				if (m && null != m)
				{
					var sel = this.fastening.selector;
					$(sel + ' a.efrsb_message').attr('href','https://old.bankrot.fedresurs.ru/MessageWindow.aspx?ID=' + m.MessageGUID);
					$(sel + ' a.efrsb_message span.Number').text(m.Number);
					$(sel + ' a.efrsb_message span.PublishDate').text(m.PublishDate);
				}
			}
		}

		controller.OnDebtor = function ()
		{
			var self= this;
			var sel = this.fastening.selector;
			var cc_debtor = c_debtor();
			var m = {};
			if (this.model)
				CopyHiddenFields(self.model,m);
			cc_debtor.SetFormContent(m);
			var btnOk = 'Сохранить информацию о должнике';
			h_msgbox.ShowModal
			({
				title: 'Информация о должнике'
				, controller: cc_debtor
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-ama-aplication-debtor"
				, onclose: function (btn, dlg_div)
				{
					if (btn == btnOk)
					{
						h_validation_msg.IfOkValidateResult({
							validation_result: cc_debtor.Validate()
							, title_reject: 'Невозможно сохранить данные должника потому что'
							, on_validate_ok_func: function ()
							{
								m= cc_debtor.GetFormContent();
								CopyHiddenFields(m,self.model);
								var select2= $(sel + ' [data-fc-selector="Выбранный_должник"]');
								select2.select2('data', data_for_select2(m.Должник));
								self.UpdateMessage();
								cc_debtor.Destroy();
								$(dlg_div).dialog("close");
							}
						});
						return false;
					}
				}
			});
		}

		var data_for_select2 = function (d)
		{
			var text= 'организация'==d.Тип ? d.Наименование : 
				d.Фамилия + ' ' + d.Имя + ' ' + d.Отчество;
			return {id:'?',text:text};
		}

		var codec = {
			Encode: function (data)
			{
				var res = {};
				res.Должник= data.Должник;
				res.Судебный_акт_о_введении_процедуры= data.Судебный_акт_о_введении_процедуры;
				res.Суд= data.Суд;
				res.АУ= data.АУ;
				res.Сообщение_на_ефрсб_о_введении_процедуры= data.Сообщение_на_ефрсб_о_введении_процедуры;
				res.Публикация_в_СМИ= data.Публикация_в_СМИ;
				return res;
			}
			, Decode: function (data)
			{
				if ('string' == typeof data)
					data= JSON.parse(data);
				data.Выбранный_должник = (!data.Должник) ? null : data_for_select2(data.Должник);
				return data;
			}
		}

		controller.UseCodec(codec);

		return controller;
	}
});