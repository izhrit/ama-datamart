﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/cabinetcc/main/e_committee_cabinet.html'
	, 'forms/ama/cabinetcc/root/c_com_cab_root'
	, 'forms/ama/cabinetcc/signing/c_com_cab_signing'
	, 'forms/ama/cabinetcc/question/main/c_committee_question'
	, 'forms/base/codec/codec.copy'
	, 'forms/ama/datamart/base/h_questions'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, c_com_cab_root, c_com_cab_signing, c_committee_question, codec_copy, h_questions, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_code_url = controller.base_url + '?action=meeting.cabinetcc';

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			this.OnTop();
		}

		controller.BindButtons= function()
		{
			var sel = this.fastening.selector;
			var self = this;
			$(sel + ' button.go-sign').click(function () { self.OnStartSign(); });
			$(sel + ' button.top').click(function () { self.OnTop(); });
			$(sel + ' tr.q').click(function (e)
			{
				var i_item = parseInt($(e.target).parents('[data-fc-type="array-item"]').attr('data-array-index'));
				self.OnQuestion(i_item);
			});
			$(sel + ' button.next').click(function () { self.OnNext(); });
			$(sel + ' button.prev').click(function () { self.OnPrev(); });
			$(sel + ' button.logout').click(function () { self.OnLogout(); });
		}

		controller.ChangePageController= function(create_controller, on_after_change)
		{
			var sel = this.fastening.selector;
			var self = this;
			var after_destroy= function()
			{
				self.page_controller = create_controller();
				self.page_controller.CreateNew(sel + ' > div.cpw-ama-committee-cabinet-form > div.workplace');
				self.BindButtons();
				if (on_after_change)
					on_after_change();
			}
			if (!this.page_controller || null == this.page_controller)
			{
				after_destroy();
			}
			else
			{
				if (!this.iquestion && 0 != this.iquestion && "0" != this.iquestion)
				{
					this.page_controller.Destroy();
					after_destroy();
				}
				else
				{
					var question_model = this.page_controller.GetFormContent();
					this.page_controller.Destroy();
					var iquestion = this.iquestion;
					delete this.iquestion;
					if (!question_model.Ответ)
					{
						after_destroy();
					}
					else
					{
						this.Сохранить_ответ(iquestion, question_model, after_destroy);
					}
				}
			}
		}

		controller.Сохранить_ответ = function (iquestion, question_model, after)
		{
			var вопрос = this.fastening.model.Вопросы[iquestion];
			var ответ = this.Ответ_для(вопрос);
			if (!ответ)
			{
				ответ = { На_вопрос: { Номер: вопрос.Номер, Формулировка: вопрос.На_голосование.Формулировка} };
				if (!this.fastening.model.Ответы || null == this.fastening.model.Ответы)
					this.fastening.model.Ответы = [];
				this.fastening.model.Ответы.push(ответ);
			}
			if (ответ.Ответ == question_model.Ответ)
			{
				after();
			}
			else
			{
				var url = this.base_code_url + '&cmd=store-answer&id_Vote=' + this.model.id_Vote;
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка выбранного ответа на сервер", url);
				var self = this;
				v_ajax.ajax({
					dataType: "json"
					, type: 'POST'
					, data: { На_вопрос: ответ.На_вопрос, Ответ: question_model.Ответ }
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							self.Дополнить_документы_и_журнал(data);
							ответ.Ответ = question_model.Ответ;
							after();
						}
					}
				});
			}
		}

		controller.Ответ_для= function(вопрос)
		{
			var model = this.fastening.model;
			if (model.Ответы && null != model.Ответы)
			{
				for (var j = 0; j < model.Ответы.length; j++)
				{
					var ответ = model.Ответы[j];
					if (ответ.На_вопрос.Номер == вопрос.Номер)
					{
						return ответ;
					}
				}
			}
		}

		controller.Результат_для = function (вопрос)
		{
			var model = this.fastening.model;
			if (model.Результаты && null != model.Результаты)
			{
				for (var j = 0; j < model.Результаты.length; j++)
				{
					var результат = model.Результаты[j];
					if (результат.По_вопросу.Номер == вопрос.Номер)
					{
						return результат;
					}
				}
			}
		}

		controller.Вопрос_для = function (на_вопрос)
		{
			var вопросы = this.model.Вопросы;
			for (var i = 0; i < вопросы.length; i++)
			{
				var вопрос = вопросы[i];
				if (на_вопрос.Номер == вопрос.Номер)
					return вопрос;
			}
			return null;
		}

		controller.PrepareTopModel= function()
		{
			var model = this.fastening.model;
			if (!model || null == model)
				model = {};
			var cc = codec_copy();

			var top_model = {
				Вопросы: []
				, Документы: model.Документы
				, Журнал: model.Журнал
				, Состояние: model.Состояние
			};
			if (model && model.Кабинет && model.Кабинет.Участник && model.Кабинет.Участник.Нет_в_списках)
				top_model.Нет_в_списках = model.Кабинет.Участник.Нет_в_списках;
			if (model.Вопросы && null != model.Вопросы)
			{
				for (var i = 0; i < model.Вопросы.length; i++)
				{
					var вопрос = cc.Copy(model.Вопросы[i]);
					var ответ_на_вопрос = this.Ответ_для(вопрос);
					if (ответ_на_вопрос)
					{
						вопрос.Ответ = ответ_на_вопрос.Ответ;
						if (ответ_на_вопрос.Подписано)
							вопрос.Подписано = ответ_на_вопрос.Подписано;
					}
					var результат_по_вопросу = this.Результат_для(вопрос);
					if (результат_по_вопросу)
					{
						if (результат_по_вопросу.Решение_НЕ_принято)
						{
							вопрос.Результат = 'решение не принято';
						}
						else
						{
							вопрос.Результат = 'принято решение : «' + результат_по_вопросу.Принято_решение + '»';
						}
					}
					top_model.Вопросы.push(вопрос);
				}
			}
			return top_model;
		}

		controller.OnNext= function()
		{
			var Вопросы = this.fastening.model.Вопросы;
			if ((Вопросы.length - 1) != this.iquestion)
			{
				this.OnQuestion(this.iquestion + 1);
			}
			else if (!this.Все_вопросы_подписаны())
			{
				this.OnSign();
			}
			else
			{
				this.OnTop();
			}
		}

		controller.OnPrev = function ()
		{
			if (0 == this.iquestion)
			{
				this.OnTop();
			}
			else
			{
				this.OnQuestion(this.iquestion-1);
			}
		}

		controller.OnTop = function ()
		{
			var self = this;
			this.ChangePageController(function ()
			{
				var controller = c_com_cab_root({ base_url: self.base_url });
				var top_model = self.PrepareTopModel();
				controller.SetFormContent(top_model);
				return controller;
			});
		}

		controller.приготовить_данные_для_бюллетеня= function()
		{
			var ccopy = codec_copy();
			var бюллетень = {
				id_Vote: this.model.id_Vote
				, Должник: ccopy.Copy(this.model.Кабинет.Должник)
				, Дата_заседания: this.model.Кабинет.Дата_заседания
				, Участник: ccopy.Copy(this.model.Кабинет.Участник)
				, Вопросы: []
			};

			var вопросы = this.model.Вопросы;
			var ответы = this.model.Ответы;
			if (ответы && null != ответы)
			{
				for (var i = 0; i < ответы.length; i++)
				{
					var ответ_на_вопрос = ответы[i];
					if (!ответ_на_вопрос.Подписано || null == ответ_на_вопрос.Подписано)
					{
						var вопрос = this.Вопрос_для(ответ_на_вопрос.На_вопрос);
						var в_бюллетень = {
							Номер: вопрос.Номер
							, На_голосование: {
								Формулировка: вопрос.На_голосование.Формулировка
								, Варианты: ccopy.Copy(вопрос.На_голосование.Варианты ? вопрос.На_голосование.Варианты : h_questions.Варианты_ответов_формы1)
							}
							, Выбран_вариант: ответ_на_вопрос.Ответ
						};
						бюллетень.Вопросы.push(в_бюллетень);
					}
				}
			}

			return бюллетень;
		}

		controller.Дополнить_документы_и_журнал= function(дж)
		{
			if (дж.Документы && null != дж.Документы)
			{
				for (var i = 0; i < дж.Документы.length; i++)
				{
					var документ = дж.Документы[i];
					if (!this.model.Документы || null==this.model.Документы)
						this.model.Документы= [];
					this.model.Документы.push(документ);
				}
			}
			if (дж.Журнал && null != дж.Журнал)
			{
				for (var i = 0; i < дж.Журнал.length; i++)
				{
					var запись = дж.Журнал[i];
					if (!this.model.Журнал || null == this.model.Журнал)
						this.model.Журнал = [];
					this.model.Журнал.push(запись);
				}
			}
		}

		controller.OnOkSigned = function (бюллетень,signed)
		{
			for (var i= 0; i<бюллетень.Вопросы.length; i++)
			{
				var вопрос = бюллетень.Вопросы[i];
				var ответ_на_вопрос = this.Ответ_для(вопрос);
				ответ_на_вопрос.Подписано = signed.Подписано;
			}
			this.Дополнить_документы_и_журнал(signed);
		}

		controller.OnStartSign = function ()
		{
			var Вопросы= this.model.Вопросы;
			if (Вопросы && null!=Вопросы)
			{
				for (var i= 0; i<Вопросы.length; i++)
				{
					var вопрос = Вопросы[i];
					var ответ_на_вопрос = this.Ответ_для(вопрос);
					if (!ответ_на_вопрос || null==ответ_на_вопрос ||
						!ответ_на_вопрос.Подписано || null == ответ_на_вопрос.Подписано)
					{
						this.OnQuestion(i);
						break;
					}
				}
			}
		}

		controller.OnSign = function ()
		{
			var self = this;
			var бюллетень = self.приготовить_данные_для_бюллетеня();
			var checked = 0 != $(self.fastening.selector + ' input[type="radio"]:checked').length;
			if (self.model && 'Голосование'!=self.model.Состояние)
			{
				h_msgbox.ShowModal({
					title: 'Подписание неуместно', width: 350
					, html: 'Голосование в настоящее время закрыто.'
				});
			}
			else if ((!бюллетень.Вопросы || null == бюллетень.Вопросы || 0 == бюллетень.Вопросы.length) && !checked)
			{
				h_msgbox.ShowModal({
					title: 'Данные для подписания отсутствуют', width: 500
					, html:'Укажите ответы на вопросы, которые хотите подписать.'
				});
			}
			else
			{
				var ccopy = codec_copy();
				this.ChangePageController(function ()
				{
					бюллетень = self.приготовить_данные_для_бюллетеня();
					var controller = c_com_cab_signing({ base_url: self.base_url });
					controller.OnOkSigned = function (signed)
					{
						self.OnOkSigned(бюллетень, signed);
						self.OnTop();
					}
					controller.Дополнить_документы_и_журнал = function (дж)
					{
						self.Дополнить_документы_и_журнал(дж);
					}
					controller.SetFormContent(бюллетень);
					return controller;
				});
			}
		}

		controller.Все_вопросы_подписаны= function()
		{
			var вопросы = this.model.Вопросы;
			if (вопросы && null != вопросы)
			{
				for (var i = 0; i < вопросы.length; i++)
				{
					var вопрос = вопросы[i];
					var ответ = this.Ответ_для(вопрос);
					if (!ответ || null == ответ || !ответ.Подписано || null == ответ.Подписано)
						return false;
				}
			}
			return true;
		}

		controller.OnQuestion= function(iquestion)
		{
			var self = this;
			var fastening = this.fastening;
			var sel = fastening.selector;
			var model = fastening.model;
			var Вопросы= model.Вопросы;
			var вопрос = Вопросы[iquestion];
			var question_model= {
				Номер: вопрос.Номер
				, В_повестке: вопрос.В_повестке
				, На_голосование: { Формулировка: вопрос.На_голосование.Формулировка }
			};
			if (вопрос.На_голосование.Варианты)
				question_model.На_голосование.Варианты = вопрос.На_голосование.Варианты;
			var ответ = this.Ответ_для(вопрос);
			if (ответ)
			{
				question_model.Ответ = ответ.Ответ;
				if (ответ.Подписано)
					question_model.Подписано = ответ.Подписано;
			}
			this.ChangePageController
			(
				function ()
				{
					var controller = c_committee_question();
					controller.SetFormContent(question_model);
					return controller;
				}
				, function ()
				{
					self.iquestion = iquestion;
					var to_top_title = 'Вернуться на главную страницу кабинета';
					var b_prev = $(sel + ' button.prev');
					var b_next = $(sel + ' button.next');
					if (0 == iquestion)
					{
						b_prev.text(to_top_title);
					}
					if ((Вопросы.length - 1) == iquestion)
					{
						b_next.text(!self.Все_вопросы_подписаны() ? 'Перейти к подписанию' : to_top_title);
					}
				}
			);
		}

		controller.OnLogout= function()
		{
			window.location = 'ui.php';
		}

		return controller;
	}
});