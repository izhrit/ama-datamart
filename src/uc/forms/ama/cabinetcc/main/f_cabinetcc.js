define(['forms/ama/cabinetcc/main/c_committee_cabinet'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'cabinetcc'
		, Title: 'Кабинет голосования комитета кредиторов на витрине данных ПАУ'
	};
	return form_spec;
});

