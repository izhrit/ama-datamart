﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/cabinetcc/root/e_com_cab_root.html'
	, 'forms/ama/cabinetcc/documents/c_com_cab_documents'
	, 'forms/ama/cabinetcc/questions/c_com_cab_questions'
	, 'forms/ama/cabinetcc/log/c_com_cab_log'
],
function (c_fastened, tpl, c_com_cab_documents, c_com_cab_questions, c_com_cab_log)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var options = {
			field_spec:
				{
					Документы: { controller: function () { return c_com_cab_documents({ base_url: base_url }); } }
					, Вопросы: { controller: c_com_cab_questions }
					, Журнал: { controller: c_com_cab_log }
				}
		};

		var controller = c_fastened(tpl, options);

		return controller;
	}
});