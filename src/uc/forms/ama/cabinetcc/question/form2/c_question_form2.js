﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/cabinetcc/question/form2/e_question_form2.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' input[type="radio"]').change(function () { self.OnChangeRadio(); });
			$(sel + ' input[type="text"]').change(function () { self.OnChangeText(); });
			$(sel + ' table tr').click(function (e) { self.OnClickRow(e); });

			this.UpdateUI();
		}

		controller.UpdateText= function()
		{
			var sel = this.fastening.selector;
			var variant = parseInt($(sel + ' input[checked="checked"]').attr('value'));
			if (this.fastening.model
				&& this.fastening.model.Предложенные_варианты
				&& variant != this.fastening.model.Предложенные_варианты.length)
			{
				$(sel + ' input[type="text"]').val('');
			}
		}

		controller.OnChangeRadio = function ()
		{
			this.UpdateText();
			this.UpdateUI();
		}

		controller.OnChangeText = function ()
		{
			var text = $(sel + ' input[type="text"]').val();
			if ('' != text)
			{
				var sel = this.fastening.selector;
				var checked_radio = $(sel + ' table.new-variant input[type="radio"]');
				checked_radio.attr('checked', 'checked');
				$(sel + ' table tr').removeClass('selected');
				checked_radio.parents('tr').addClass('selected');
			}
		}

		controller.OnClickRow = function (e)
		{
			var sel = this.fastening.selector;
			var radio = $(e.target).parents('tr').find('input[type="radio"]');
			if (0 != radio.length)
			{
				radio.attr('checked', 'checked');
				this.UpdateText();
				this.UpdateUI();
			}
		}

		controller.UpdateUI = function ()
		{
			var sel = this.fastening.selector;
			var checked_radio = $(sel + ' input[type="radio"]:checked');
			var selected_value = checked_radio.val();
			$(sel + ' table tr').removeClass('selected');
			checked_radio.parents('tr').addClass('selected');
		}

		return controller;
	}
});
