wait_text "Варианты"
shot_check_png ..\..\shots\00new.png

je $($('table.new-variant tr')[0]).addClass('hovered');
shot_check_png ..\..\shots\00new_hovered.png
je $($('table.new-variant tr')[0]).removeClass('hovered');

exit