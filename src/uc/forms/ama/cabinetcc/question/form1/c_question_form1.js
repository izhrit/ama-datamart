﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/cabinetcc/question/form1/e_question_form1.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' input[type="radio"]').change(function () { self.OnChangeRadio(); });
		}

		controller.OnChangeRadio = function ()
		{
			var content_form = this.GetFormContent();
		}

		return controller;
	}
});
