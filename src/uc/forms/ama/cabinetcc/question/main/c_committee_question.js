﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/cabinetcc/question/main/e_committee_question.html'
	, 'forms/ama/cabinetcc/question/form1/c_question_form1'
	, 'forms/ama/cabinetcc/question/form2/c_question_form2'
	, 'forms/ama/datamart/base/h_questions'
],
function (c_fastened, tpl, c_question_form1, c_question_form2, h_questions)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var question_model = {};
			var Ответ = this.fastening.model.Ответ;
			if (this.fastening.model.Ответ)
				question_model.Ответ = Ответ;
			if (this.fastening.model.Подписано)
				question_model.Подписано = this.fastening.model.Подписано;
			if (this.fastening.model.На_голосование.Варианты)
			{
				this.c_answer = c_question_form2();
				question_model.Предложенные_варианты = this.fastening.model.На_голосование.Варианты;
				if (Ответ)
				{
					var ivariant = question_model.Предложенные_варианты.indexOf(Ответ);
					if (-1 != ivariant)
					{
						question_model.Выбранный_вариант = ivariant;
					}
					else
					{
						question_model.Свой_вариант = Ответ;
						question_model.Выбранный_вариант = question_model.Предложенные_варианты.length;
					}
				}
			}
			else
			{
				var form1_ответы = h_questions.Варианты_ответов_формы1;
				if (!Ответ || -1 != form1_ответы.indexOf(Ответ))
				{
					this.c_answer = c_question_form1();
				}
				else
				{
					this.c_answer = c_question_form2();
					question_model.Выбранный_вариант = 3;
					question_model.Свой_вариант = Ответ;
					question_model.Предложенные_варианты = form1_ответы;
				}
			}
			this.c_answer.SetFormContent(question_model);
			this.c_answer.CreateNew(sel + ' div.answer');
		}

		controller.GetFormContent = function ()
		{
			var res = this.c_answer.GetFormContent();
			if (!res.Предложенные_варианты)
			{
				return res;
			}
			else
			{
				var iвариант = parseInt(res.Выбранный_вариант);
				return { Ответ: (iвариант < res.Предложенные_варианты.length) ? res.Предложенные_варианты[iвариант] : res.Свой_вариант };
			}
		}

		return controller;
	}
});