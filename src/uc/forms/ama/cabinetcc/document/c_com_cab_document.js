﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/cabinetcc/document/v_com_cab_document.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);
		}

		controller.SetFormContent= function(data)
		{
			this.model = data;
		}

		return controller;
	}
});