﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/cabinetcc/questions/e_com_cab_questions.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);
		return controller;
	}
});