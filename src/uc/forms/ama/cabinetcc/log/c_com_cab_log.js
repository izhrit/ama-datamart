﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/cabinetcc/log/e_com_cab_log.html'
	, 'forms/ama/datamart/base/h_vote_log'
],
function (c_fastened, tpl, h_vote_log)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		controller.UseCodec({
			Decode: function (content)
			{
				var rows =
				("string" != typeof content) ? content :
				('[' != content.charAt(0) && '{' != content.charAt(0)) ? null :
				JSON.parse(content);

				var res = [];
				for (var i = 0; i < rows.length; i++)
				{
					var row = rows[i];
					res.push(row.Время ? row : h_vote_log.Запись_о_событии(row));
				}
				return res;
			}
		});

		return controller;
	}
});