define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var cabinetcc_specs = {

		controller: {

		  "dm_committee_cabinet_root": { path: 'forms/ama/cabinetcc/root/c_com_cab_root' }
		, "dm_committee_cabinet":      { path: 'forms/ama/cabinetcc/main/c_committee_cabinet' }
		, "dm_committee_login":        { path: 'forms/ama/cabinetcc/login/c_committee_cabinet_login' }
		, "dm_question_form1":         { path: 'forms/ama/cabinetcc/question/form1/c_question_form1' }
		, "dm_question_form2":         { path: 'forms/ama/cabinetcc/question/form2/c_question_form2' }
		, "cabinetcc_question":        { path: 'forms/ama/cabinetcc/question/main/c_committee_question' }
		, "cabinetcc_signing":         { path: 'forms/ama/cabinetcc/signing/c_com_cab_signing' }
		, "cabinetcc_document":        { path: 'forms/ama/cabinetcc/document/c_com_cab_document' }
		, "cabinetcc_documents":       { path: 'forms/ama/cabinetcc/documents/c_com_cab_documents' }
		, "cabinetcc_log":             { path: 'forms/ama/cabinetcc/log/c_com_cab_log' }
		, "cabinetcc_questions":       { path: 'forms/ama/cabinetcc/questions/c_com_cab_questions' }
		, "cabinetcc_root":            { path: 'forms/ama/cabinetcc/root/c_com_cab_root' }

		}

		, content: {
		  "cabinetcc-example": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example.json.txt' }
		, "cabinetcc-example1q": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example1q.json.txt' }
		, "cabinetcc-example1q1s": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example1q1s.json.txt' }
		, "cabinetcc-example2q": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example2q.json.txt' }
		, "cabinetcc-example2q1s": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example2q1s.json.txt' }
		, "cabinetcc-example2q2s": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example2q2s.json.txt' }

		, "cabinetcc-example-c": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example_c.json.txt' }
		, "cabinetcc-example-d": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example_d.json.txt' }
		, "cabinetcc-example-e": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example_e.json.txt' }
		, "cabinetcc-example-f": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example_f.json.txt' }
		, "cabinetcc-example-g": { path: 'txt!forms/ama/cabinetcc/main/tests/contents/example_g.json.txt' }

		, "cabinetcc-question-form1": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form1.json.txt' }
		, "cabinetcc-question-form1-yes": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form1.yes.json.txt' }
		, "cabinetcc-question-form1-yes-signed": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form1.yes.signed.json.txt' }
		, "cabinetcc-question-form1-no": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form1.no.json.txt' }
		, "cabinetcc-question-form1-no-signed": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form1.no.signed.json.txt' }
		, "cabinetcc-question-form1-pass": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form1.pass.json.txt' }
		, "cabinetcc-question-form1-pass-signed": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form1.pass.signed.json.txt' }
		, "cabinetcc-question-form1-error": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form1.error.json.txt' }
		, "cabinetcc-question-form1-error-signed": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form1.error.signed.json.txt' }

		, "cabinetcc-question-form2": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form2.json.txt' }
		, "cabinetcc-question-form2-v1": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form2.v1.json.txt' }
		, "cabinetcc-question-form2-v2": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form2.v2.json.txt' }
		, "cabinetcc-question-form2-v2-signed": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form2.v2.signed.json.txt' }
		, "cabinetcc-question-form2-v3": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form2.v3.json.txt' }
		, "cabinetcc-question-form2-vnew": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form2.vnew.json.txt' }
		, "cabinetcc-question-form2-vnew-signed": { path: 'txt!forms/ama/cabinetcc/question/main/tests/contents/example.form2.vnew.signed.json.txt' }

		, "committee-signing-example": { path: 'txt!forms/ama/cabinetcc/signing/tests/contents/example.json.txt' }

		, "cabinetcc-login-example1": { path: 'txt!forms/ama/cabinetcc/login/tests/contents/example1.json.txt' }
		, "cabinetcc-login-example2": { path: 'txt!forms/ama/cabinetcc/login/tests/contents/example2.json.txt' }
		, "cabinetcc-login-example3": { path: 'txt!forms/ama/cabinetcc/login/tests/contents/example3.json.txt' }

		, "cabinetcc-document-text": { path: 'txt!forms/ama/cabinetcc/document/tests/contents/example.txt' }
		, "cabinetcc-document-email": { path: 'txt!forms/ama/cabinetcc/document/tests/contents/example.json.txt' }

		, "cabinetcc-documents-example": { path: 'txt!forms/ama/cabinetcc/documents/tests/contents/example.json.txt' }
		, "cabinetcc-log-example": { path: 'txt!forms/ama/cabinetcc/log/tests/contents/example.json.txt' }
		, "cabinetcc-questions-example": { path: 'txt!forms/ama/cabinetcc/questions/tests/contents/example.json.txt' }
		, "cabinetcc-root-example": { path: 'txt!forms/ama/cabinetcc/root/tests/contents/example.json.txt' }
		}

	};

	return h_spec.combine(cabinetcc_specs);
});