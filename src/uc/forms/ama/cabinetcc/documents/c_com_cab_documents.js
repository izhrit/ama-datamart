﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/cabinetcc/documents/e_com_cab_documents.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/base/h_vote_document'
	, 'forms/ama/datamart/base/h_SentEmail'
	, 'forms/ama/cabinetcc/document/c_com_cab_document'
],
function (c_fastened, tpl, h_msgbox, h_vote_document, h_SentEmail, c_com_cab_document)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_document_url = controller.base_url + '?action=meeting.document';
		controller.base_download_url = !app.ExternalWindowOpen ? (controller.base_url + '?action=meeting.download') : 'ui-backend.php?action=meeting.download';

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' td.doc a').click(function (e) { self.OnDocument(e); });
			$(sel + ' td.fname a').each(function (index)
			{
				var a = $(this);
				var tr = a.parents('tr');
				var i = tr.attr('data-array-index');
				if (i)
				{
					var doc = self.model[i];
					var url = self.base_download_url
						+ '&id_SentEmail=' + (!doc.id_SentEmail ? '' : doc.id_SentEmail)
						+ '&id_Vote_document=' + (!doc.id_Vote_document ? '' : doc.id_Vote_document);
					if (!app.ExternalWindowOpen) {
						a.attr('href', url);
						a.attr('target', '_blank');
					}
					else {
						a.attr('href', '#');
						a.attr('onclick', "window.external.OpenDatamartUrl('/" + url + "')");
					}
				}
			});
		}

		controller.OnDocument= function(e)
		{
			var a = $(e.target);
			var tr = a.parents('tr');
			var i = tr.attr('data-array-index');
			var doc= this.model[i];
			var url = this.base_document_url
				+ '&id_SentEmail=' + (!doc.id_SentEmail ? '' : doc.id_SentEmail)
				+ '&id_Vote_document=' + (!doc.id_Vote_document ? '' : doc.id_Vote_document);
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение текста документа с сервера", url);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.ShowDocument(data, a.text(), tr.find('td.reg span').text());
					}
				}
			});
		}

		controller.ShowDocument= function(text, title, date)
		{
			var com_cab_document = c_com_cab_document();
			com_cab_document.SetFormContent(text);
			h_msgbox.ShowModal({
				title: 'Текстовое отображение документа "' + title + '" от ' + date
				, width: 950, height: 700
				, id_div: 'id-cabinetcc-Vote-document'
				, controller: com_cab_document
				, buttons:['Закрыть текстовое отображение документа']
			});
		}

		controller.UseCodec({
			Decode: function (content)
			{
				var rows =
				("string" != typeof content) ? content :
				('[' != content.charAt(0) && '{' != content.charAt(0)) ? null :
				JSON.parse(content);

				var res = [];
				for (var i = 0; i < rows.length; i++)
				{
					var row = rows[i];
					res.push(
						row.id_Vote_document ? h_vote_document.Запись_о_документе(row) :
						row.id_SentEmail ? h_SentEmail.Запись_о_документе(row) : row
					);
				}
				return res;
			}
		});

		return controller;
	}
});