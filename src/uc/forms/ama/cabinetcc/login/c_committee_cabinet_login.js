﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/cabinetcc/login/e_committee_cabinet_login.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/cabinetcc/main/c_committee_cabinet'
],
function (c_fastened, tpl, h_msgbox, c_committee_cabinet)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_code_url = controller.base_url + '?action=meeting.cabinetcc';

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			$(sel + ' div.login input').focus();

			var self = this;
			$(sel + ' div.login button').click(function () { self.OnLogin(); });

			this.OnLogin();
		}

		controller.OnLogin= function()
		{
			var sel = this.fastening.selector;
			var key = $(sel + ' div.login input').val();

			if ('' != key)
			{
				var url = this.base_code_url + '&cmd=login&key=' + key;
				var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных кабинета с сервера", url);
				v_ajax.ajax({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							$(sel + ' div.login').hide();
							var committee_cabinet = c_committee_cabinet();
							committee_cabinet.SetFormContent(data);
							committee_cabinet.Edit(sel + ' div.cabinet');
						}
					}
				});
			}
		}

		return controller;
	}
});