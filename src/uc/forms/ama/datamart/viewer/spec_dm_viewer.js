define(function ()
{
	return {

		controller: {
			"dm_viewer_main": {
				path: 'forms/ama/datamart/viewer/main/c_dm_viewer_main'
				, title: 'главная форма наблюдателя'
			}
			,"dm_viewer_debtors": {
				path: 'forms/ama/datamart/viewer/debtors/c_dm_viewer_debtors'
				, title: 'список должников (сопровождение)'
			}
			,"dm_viewer_escorts": {
				path: 'forms/ama/datamart/viewer/escorts/c_dm_viewer_escorts'
				, title: 'форма со списком должников и с полной информацией о должнике'
			}
			, "dm_viewer_procedures": {
				path: 'forms/ama/datamart/viewer/procedures/c_dm_viewer_procedures'
				, title: 'процедуры наблюдателя'
				, keywords: 'запросы на доступ'
			}
			, "dm_viewer_register":
			{
				path: 'forms/ama/datamart/viewer/register/c_dm_viewer_register'
				, keywords: 'запросы на регистрацию'
				, title: 'запрос на регистрацию наблюдателя'
			}
			, "dm_viewer_request":
			{
				path: 'forms/ama/datamart/viewer/request/c_dm_viewer_request'
				, keywords: 'запрос доступ процедуры должники'
				, title: 'запрос на доступ наблюдателя к информации по процедуре'
			}
			, "dm_viewer_profile": {
				path: 'forms/ama/datamart/viewer/profile/c_dm_viewer_profile'
				, title: 'профиль наблюдателя'
			}
			, "dm_viewer_leaks": {
				path: 'forms/ama/datamart/viewer/leaks/c_dm_viewer_leaks'
				, title: 'привлечённые специалисты'
			}
			, "dm_viewer_outcome": {
				path: 'forms/ama/datamart/viewer/outcome/c_dm_viewer_outcome'
				, title: 'исходящие для наблбюдателей'
			}
		}

		, content: {
			"viewer_profile-01sav": {
				path: 'txt!forms/ama/datamart/viewer/profile/tests/contents/01sav.json.etalon.txt'
			}
			, "viewer_profile-changed_email": {
				path: 'txt!forms/ama/datamart/viewer/profile/tests/contents/changed_email.json.txt'
			}
			, "viewer_request-01sav": {
				path: 'txt!forms/ama/datamart/viewer/request/tests/contents/01sav.json.etalon.txt'
				, title: 'запрос на доступ наблюдателя к информации по процедуре юр.лица'
			}
			, "viewer_request-np": {
				path: 'txt!forms/ama/datamart/viewer/request/tests/contents/np.json.txt'
				, title: 'запрос на доступ наблюдателя к информации по процедуре физ.лица'
			}
			, "viewer_register-01sav": {
				path: 'txt!forms/ama/datamart/viewer/register/tests/contents/01sav.json.etalon.txt'
			}
		}

	};

});