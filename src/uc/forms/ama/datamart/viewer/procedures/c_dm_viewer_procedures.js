﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/viewer/procedures/e_dm_viewer_procedures.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/base/h_procedure_types'
	, 'forms/ama/datamart/base/h_debtorName'
	, 'forms/ama/datamart/viewer/request/c_dm_viewer_request'
	, 'forms/ama/datamart/base/h_Request_state'
	, 'forms/base/h_validation_msg'
	, 'forms/ama/datamart/viewer/procedures/h_dm_viewer_add_request'
],
function (c_fastened, tpl, h_msgbox, h_procedure_types, h_debtorName, c_dm_viewer_request, h_Request_state, h_validation_msg, h_dm_viewer_add_request)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();

			var self = this;
			if (!app.disable_viewers_registration)
			{
				this.AddButtonToPagerLeft('cpw-ama-dm-viewer-procedures-pager',
					'Запросить доступ к информации', function () { self.OnRegisterRequest(); });
			}
		}

		controller.grid_row_buttons = function ()
		{
			var self = this;
			if (app.disable_viewers_registration)
			{
				return [
					{ "class": "procedure", text: "Открыть витрину с информацией по процедуре", click: function () { self.OnProcedure(); } }
				];
			}
			else
			{
				return [
					{ "class": "procedure", text: "Открыть витрину с информацией по процедуре", click: function () { self.OnProcedure(); } }
					, { "class": "delete", text: "Отказаться от доступа к информации по процедуре", click: function () { self.OnDeleteRequest(); } }
					, { "class": "state", text: "Уточнить текущее состояние доступа к информации", click: function () { self.OnStateRequest(); } }
				];
			}
		};

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_grid_url = controller.base_url + '?action=procedure.jqgrid';
		controller.base_crud_url = controller.base_url + '?action=request.crud';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_MUser=' + this.model.id_MUser;
			return url;
		}

		var  procedureFormatter= function(cellvalue, options, rowObject)
		{
			return !rowObject.procedure_type || null == rowObject.procedure_type
				? '' : h_procedure_types.SafeShortForDBValue(rowObject.procedure_type);
		}

		var debtorNameFormatter = function (cellvalue, options, rowObject)
		{
			return h_debtorName.beautify_debtorName(rowObject.debtorName);
		}

		var statusFormatter = function (cellvalue, options, rowObject)
		{
			return !rowObject.state || null == rowObject.state 
				? 'открыто' : h_Request_state.byCode[rowObject.state].Состояние.доступа;
		}

		var dateFormat = function(cellvalue, options, rowObject)
		{
			if(rowObject.publicDate && rowObject.publicDate.length > 0)
			{
				var dateAndTimeSplit = rowObject.publicDate.split(' ');
				if (2!=dateAndTimeSplit.length)
					dateAndTimeSplit = rowObject.publicDate.split('T');
				var dateSplit = dateAndTimeSplit[0].split('-');
				var timeSplit = dateAndTimeSplit[1].split(':')

				var date = dateSplit[2] + '.' + dateSplit[1] + '.' + dateSplit[0] + ' ' 
				+ timeSplit[0] + ':' + timeSplit[1];

				return date
			}

			return '';
		}

		controller.colModel =
		[
			  { name: 'id_MProcedure', hidden: true }
			, { name: 'id_Request', hidden: true }
			, { name: 'Пояснения', hidden: true }
			, { name: 'КогоПредставляетЗаявитель', hidden: true }
			, { label: 'Тип пр.', name: 'procedure_type', width: 30, align: 'left', formatter: procedureFormatter }
			, { label: 'Должник', name: 'debtorName', align: 'left', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] }, formatter: debtorNameFormatter }
			, { label: 'Арбитражный управляющий', name: 'Manager', width: 100 }
			, { label: 'Состояние', name: 'state', width: 50, search: false, formatter: statusFormatter }
			, { label: 'На дату', name: 'publicDate', align: 'left', width: 50, search: false, formatter: dateFormat }
			, { label: ' ', name: 'myac', width: 20, align: 'right', search: false }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = self.PrepareUrl();
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка страницы списка процедур...'
				, recordtext: 'Показано процедур {1} из {2}'
				, emptyText: 'Нет процедур для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				// , rowList: [5, 10, 15]
				, pager: '#cpw-ama-dm-viewer-procedures-pager'
				, viewrecords: true
				, autowidth: true
				, multiselect: false
				, multiboxonly: false
				, height: 'auto'
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true 
				, onSelectRow: function (id, s, e) 
				{ 
					self.onSelectRow_if_no_menu(id, s, e, function () { self.OnProcedure(); });
				}
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка процедур", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnProcedure = function ()
		{
			if (!this.ShowProcedure)
			{
				h_msgbox.ShowModal({
					title: "Информация о процедуре"
					, html: 'Просмотр информации, размещённой по процедуре не реализован'
				});
			}
			else
			{
				var sel = this.fastening.selector;
				var grid = $(sel + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');
				var rowdata = grid.jqGrid('getRowData', selrow);
				this.ShowProcedure(h_procedure_types.PrepareSelect2(rowdata));
			}
		}

		controller.OnStateRequest = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			
			if (!rowdata.id_Request)
			{
				h_msgbox.ShowModal({
					title: 'Статус запроса доступа к информации о процедуре', width:600
					, html: 'Доступ к информации о процедуре над должником <center><b>'
						 + rowdata.debtorName
						+ '</b></center><div style="text-align:center;"><br/>открыт!</div>'
				});
			}
			else
			{
				var self = this;
				var ajaxurl = this.base_crud_url + '&cmd=get&id_MUser=' + this.model.id_MUser + '&id=' + rowdata.id_Request;
				var v_ajax = h_msgbox.ShowAjaxRequest("Получение параметров запроса с сервера", ajaxurl);
				v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							self.EditRequest(rowdata.id_Request, data);
						}
					}
				});
			}
		}

		controller.EditRequest= function(id_Request,request)
		{
			var self = this;
			var c_request = c_dm_viewer_request({ base_url: this.base_url });
			request.Выбранная_процедура = { id: request.Должник.BankruptId, text: request.Должник.Наименование };
			c_request.SetFormContent(request);
			var btnOk = 'Изменить запрос на доступ';
			h_msgbox.ShowModal({
				title: 'Запрос на доступ к данным процедуры'
				, controller: c_request
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-request"
				, onclose: function (btn, dlg_div)
				{
					if (btn == btnOk) {
						h_validation_msg.IfOkWithValidateResult(c_request.Validate(), function () {
							var request_data = c_request.GetFormContent();
							self.UpdateRequest(id_Request, request_data, function () {
								c_request.Destroy();
								$(dlg_div).dialog("close");
							});
						});
						return false;
					}
				}
			});
		}

		controller.UpdateRequest = function (id_Request, request, on_done)
		{
			var sel = this.fastening.selector;
			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=update&id=' + id_Request;
			if (this.model)
				ajaxurl += '&id_MUser=' + this.model.id_MUser;
			var v_ajax = h_msgbox.ShowAjaxRequest("Обновление запроса данных на сервере", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'POST'
				, data: request
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						on_done();
						self.ReloadGrid();
						$(sel).trigger('model_change');
					}
				}
			});
		}

		controller.OnDeleteRequest = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;
			var btnOk = 'Да, отказаться';
			h_msgbox.ShowModal({
				title: 'Подтверждение отказа от информации'
				, html: "Вы действительно хотите отказаться от информации по процедуре<br/>"
					+ "<center><b>" + rowdata.debtorName + "</b>?</center>"
				, buttons: [btnOk, 'Отмена']
				, width:500
				, id_div: "cpw-form-ama-datamart-viewer-proc-delete-confirm"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						self.DeleteRequest(rowdata);
					}
				}
			});
		}

		controller.DeleteRequest = function (rowdata)
		{
			var sel = this.fastening.selector;
			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=delete&id=' + rowdata.id_Request;
			if (this.model)
				ajaxurl += '&id_MUser=' + this.model.id_MUser;
			var v_ajax = h_msgbox.ShowAjaxRequest("Удаление запроса данных на сервере", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.ReloadGrid();
						$(sel).trigger('model_change');
					}
				}
			});
		}

		controller.OnRegisterRequest= function()
		{
			var self = this;
			var sel = this.fastening.selector;
			h_dm_viewer_add_request.OnRegisterRequest(this.base_url, this.model.id_MUser, function () { 
				self.ReloadGrid();
				$(sel).trigger('model_change');
			});
		}

		return controller;
	}
});