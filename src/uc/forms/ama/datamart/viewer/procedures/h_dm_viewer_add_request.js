﻿define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/viewer/request/c_dm_viewer_request'
	, 'forms/base/h_validation_msg'
],
function (h_msgbox, c_dm_viewer_request, h_validation_msg)
{
	var helper = {};

	helper.OnRegisterRequest= function(base_url, id_MUser, on_done)
	{
		var self = this;
		var ajaxurl = base_url + '?action=request.fields';
		var v_ajax = h_msgbox.ShowAjaxRequest("Получение параметров запроса с сервера", ajaxurl);
		v_ajax.ajax
		({
			dataType: "json", type: 'GET', cache: false
			, success: function (data)
			{
				self.NewRegisterRequest(data.КогоПредставляетЗаявитель, data.Пояснения, base_url, id_MUser, on_done);
			}
		});
	}

	helper.NewRegisterRequest = function (КогоПредставляетЗаявитель, Пояснения, base_url, id_MUser, on_done)
	{
		var self = this;
		var c_request = c_dm_viewer_request({ base_url: base_url });
		c_request.SetFormContent({ Пояснения: Пояснения, Процедура: {КогоПредставляетЗаявитель: КогоПредставляетЗаявитель}});
		var btnOk = 'Запросить доступ к процедуре';
		h_msgbox.ShowModal({
			title: 'Запрос на доступ к данным процедуры'
			, controller: c_request
			, buttons: [btnOk, 'Отмена']
			, id_div: "cpw-form-ama-datamart-add-request"
			, onclose: function (btn, dlg_div)
			{
				if (btn == btnOk)
				{
					h_validation_msg.IfOkWithValidateResult(c_request.Validate(), function ()
					{
						var request= c_request.GetFormContent();
						request.Должник.Наименование = request.Выбранная_процедура.data.Должник.Наименование;
						self.RegisterRequest(request, base_url, id_MUser, function ()
						{
							c_request.Destroy();
							$(dlg_div).dialog("close");
							on_done();
						});
					});
					return false;
				}
			}
		});
	}

	helper.RegisterRequest= function(request, base_url, id_MUser, on_done)
	{
		var ajaxurl = base_url + '?action=request.crud&cmd=add&id_MUser=' + id_MUser;
		var v_ajax = h_msgbox.ShowAjaxRequest("Регистрация запроса данных на сервере", ajaxurl);
		v_ajax.ajax
		({
			dataType: "json", type: 'POST', cache: false, data: request
			, success: function (data)
			{
				if (data.ok)
				{
					on_done();
				}
				else
				{
					h_msgbox.ShowModal({ title: 'Ошибка запроса', html: data.reason });
				}
			}
		});
	}

	return helper;
});