﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/viewer/profile/e_dm_viewer_profile.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 640 }; //, height: 300

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			$(sel + ' [data-fc-selector="Имя"]').select();
		}

		return controller;
	}
});