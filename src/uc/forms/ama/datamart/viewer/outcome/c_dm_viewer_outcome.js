﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/viewer/outcome/e_dm_viewer_outcome.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();

			var self= this;
			if (!app.disable_viewers2)
				this.AddButtonToPagerLeft('cpw-ama-datamart-outcome-pager', 'Добавить заявление РТК', function () { window.open(app.base_application_url); });

			this.AddButtonToPagerLeft('cpw-ama-datamart-outcome-pager', 'Обновить', function () { self.ReloadGrid(); });

			if (app.download_MData)
				window.open(base_url + '?action=application.load-document&id_MData=' + app.download_MData);
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_grid_url = controller.base_url + '?action=viewer.outcome.jqgrid';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_MUser=' + this.model.id_MUser;
			return url;
		}

		var MData_variants= [
			{db_value:'d',column_title:'сделки из ФА',base_url:'deals/view_deals.php?'}
			,{db_value:'a',column_title:'анкета физ.лица',base_url:'anketanp/view_anketanp.php?'}
			, { db_value: 'k', column_title: 'заявление в РТК', base_url: 'application_preview.php?' }
		];
		var MData_variant_by_db_value = {};
		var MData_variant_by_column_title= {};
		for (var i = 0; i < MData_variants.length; i++)
		{
			var v= MData_variants[i];
			MData_variant_by_db_value[v.db_value]= v;
			MData_variant_by_column_title[v.column_title]= v;
		}


		var formatterMData_Type= function (cellvalue, options, rowObject)
		{
			return !MData_variant_by_db_value[rowObject.MData_Type]
				? rowObject.MData_Type
				: MData_variant_by_db_value[rowObject.MData_Type].column_title;
		}

		var Type_search_values= { 
			'': 'вся'
			, 'd': 'сделки из ФА'
			, 'a': 'анкета физ.лица'
			, 'k': 'заявление в РТК'
		};

		controller.colModel =
		[
			  { name: 'id_MData', hidden: true }
			, { label: 'Информация',	name: 'MData_Type', width: 100, formatter: formatterMData_Type, stype: 'select', searchoptions: {sopt:['eq'], value:Type_search_values } }
			, { label: 'Кому',			name: 'ToManager', width: 250}
			, { label: 'По должнику',	name: 'ForDebtor', width: 250}
			, { label: 'На дату',		name: 'publicDate', width: 100}
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = self.PrepareUrl();
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка списка исходящих...'
				, recordtext: 'Показано исходящих {1} из {2}'
				, emptyText: 'Нет исходящих для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, pager: '#cpw-ama-datamart-outcome-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, onSelectRow: function () { self.OnOpen(); }
				, ondblClickRow: function () { /*nothing*/ }
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка исходящих", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.PrepareBaseUrlForMData = function (MData_Type)
		{
			return !MData_variant_by_column_title[MData_Type]
				? null
				: MData_variant_by_column_title[MData_Type].base_url;
		}

		controller.OnOpen = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var id_MData= rowdata.id_MData;
			var MData_Type= rowdata.MData_Type;

			var base_url= this.PrepareBaseUrlForMData(MData_Type);
			if (null == base_url)
			{
				alert('Непредусмотренный тип исходящего!\r\n   (id_MData=' + id_MData + ', MData_Type=' + MData_Type + ')');
			}
			else
			{
				window.open(base_url + 'id_MData=' + id_MData);
			}
		}

		return controller;
	}
});