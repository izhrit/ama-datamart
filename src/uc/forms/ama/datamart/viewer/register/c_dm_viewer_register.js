﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/viewer/register/e_dm_viewer_register.html'
	, 'forms/ama/datamart/viewer/request/c_dm_viewer_request'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, c_dm_viewer_request, h_validation_msg)
{
	return function (options_arg)
	{
		var base_url = (options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart';

		var options = {
			field_spec:
			{
				Запрос_на_доступ: { controller: function () { return c_dm_viewer_request({ register: true, base_url: base_url }); } }
			}
		};

		var controller = c_fastened(tpl, options);

		controller.size = { width: 700, height: 705 };

		var fix_phone_number = function (txt) {
			return !txt || null == txt ? '' : txt.replace(/[^0-9.]/g, "");
		}

		controller.Validate = function () {
			var res = null;
			var model = this.GetFormContent();
			if ('' == h_validation_msg.trim(model.Заявитель.Имя))
				res = h_validation_msg.error(res, "Имя НЕ может быть пустым");
			if (h_validation_msg.trim(model.Заявитель.Email).length < 5)
				res = h_validation_msg.error(res, "Адрес электронной почты НЕ может быть короче 5 символов");
			else if (-1 == model.Заявитель.Email.indexOf('@'))
				res = h_validation_msg.error(res, "Адрес электронной почты должен содержать символ '@'");
			if (11 != fix_phone_number(model.Заявитель.Телефон).length)
				res = h_validation_msg.error(res, "В номере телефона должно быть 11 цифр");
			var procedure = model.Запрос_на_доступ.Выбранная_процедура;
			if ( !procedure || null == procedure)
				res = h_validation_msg.error(res, "Необходимо выбрать процедуру на которую вы хотите подписаться");
			if ('' == h_validation_msg.trim(model.Запрос_на_доступ.Процедура.КогоПредставляетЗаявитель))
				res = h_validation_msg.error(res, "Укажите кого вы представляете в процедуре");
			if ('' == h_validation_msg.trim(model.Запрос_на_доступ.Пояснения))
				res = h_validation_msg.error(res, "Пояснение к запросу НЕ может быть пустым");
			return res;
		}

		return controller;
	}
});
