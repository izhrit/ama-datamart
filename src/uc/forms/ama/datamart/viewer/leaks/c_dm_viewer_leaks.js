﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/viewer/leaks/e_dm_viewer_leaks.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/base/h_procedure_types'
],
function (c_fastened, tpl, h_msgbox, h_procedure_types)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_grid_url = controller.base_url + '?action=leaks.jqgrid';

		var procedureFormatter= function(cellvalue, options, rowObject)
		{
			return h_procedure_types.SafeShortForDBValue(rowObject.procedure_type);
		}

		var accreditedFormatter= function(cellvalue, options, rowObject)
		{
			return rowObject.isAccredited ? 'Да' : 'Нет';
		}

		controller.colModel =
		[
			  { name: 'id_Leak', hidden: true }
			, { label: 'АУ', name: 'Manager', width: 200, searchoptions: { sopt: ['bw'] } }
			, { label: 'Процедура', name: 'Procedure', width: 80 }
			, { label: 'Сумма', name: 'LeakSum',editrules: {required: false}, searchoptions: {sopt: ['eq']} }
			, { label: 'Дата', name: 'LeakDate', width: 100 }
			, { label: 'Получатель', name: 'ContragentName', width: 300, searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] } }
			, { label: 'Аккредитован?', name: 'isAccredited',width: 100, search: true, sortable: false, stype: 'select', 
				searchoptions: {sopt:['eq'], value:{ '': '', '1': 'Да', '0': 'Нет' } }, formatter: accreditedFormatter }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = this.base_grid_url;
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка привлечённых специалистов..'
				, recordtext: 'Привлечённые специалисты {0} - {1} из {2}'
				, emptyText: 'Нет привлечённых специалистов для отображения'
				, rownumbers: false
				, rowNum: 10
				, rowList: [5, 10, 15]
				, pager: '#cpw-cpw-ama-dm-viewer-leaks-pager'
				, viewrecords: true
				, autowidth: true
				, multiselect: false
				, multiboxonly: false
				, height: '270'
				, ignoreCase: true
				, shrinkToFit: true
				//, onSelectAll: function () { self.OnSelect(); }
				//, onSelectRow: function () { self.OnSelect(); }
				, ondblClickRow: function () { self.OnProcedure(); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка привлчечённых специалистов", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.ReloadGrid = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.trigger('reloadGrid');
		}

		return controller;
	}
});