﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/viewer/main/e_dm_viewer_main.html'

	, 'forms/ama/datamart/procedure/selectable/c_dm_sel_procedure'
	, 'forms/ama/datamart/viewer/procedures/c_dm_viewer_procedures'
	, 'forms/ama/datamart/viewer/leaks/c_dm_viewer_leaks'
	, 'forms/ama/datamart/viewer/outcome/c_dm_viewer_outcome'
	, 'forms/ama/datamart/viewer/escorts/c_dm_viewer_escorts'

	, 'forms/ama/datamart/common/schedule/c_dm_schedule'
	, 'forms/ama/datamart/viewer/profile/c_dm_viewer_profile'
	, 'forms/ama/datamart/common/push/profile/h_dm_push_profile'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/common/news/c_news'

	, 'tpl!forms/ama/datamart/viewer/main/v_redirect_to_km_from_fa.html'
	, 'forms/ama/datamart/viewer/procedures/h_dm_viewer_add_request'
	, 'forms/mpl/asset/grid/c_mpl_asset_grid'
],
function (c_fastened, tpl

	, c_dm_sel_procedure
	, c_dm_viewer_procedures
	, c_dm_viewer_leaks
	, c_dm_viewer_outcome
	, c_dm_viewer_escorts

	, c_dm_viewer_schedule
	, c_dm_viewer_profile
	, h_dm_push_profile
	, h_msgbox
	, c_news

	, v_redirect_to_km_from_fa
	, h_dm_viewer_add_request
	, c_mpl_asset_grid
	)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var options = {
			field_spec:
				{
					Витрина: { controller: function () { return c_dm_sel_procedure({ base_url: base_url }); }, render_on_activate:true  }
					, Процедуры: { controller: function () { return c_dm_viewer_procedures({ base_url: base_url }); }, render_on_activate:true  }
					, Привлечённые: { controller: function () { return c_dm_viewer_leaks({ base_url: base_url }); }, render_on_activate:true  }
					, Календарь: { controller: function () { return c_dm_viewer_schedule({ base_url: base_url }); }, render_on_activate:true  }
					, Новости: { controller: function () { return c_news({ base_url: base_url }); }, render_on_activate:true  }
					, Исходящие: { controller: function () { return c_dm_viewer_outcome({ base_url: base_url }); }, render_on_activate:true  }
					, Сопровождение: { controller: function () { return c_dm_viewer_escorts({ base_url: base_url }); }, render_on_activate:true  }
					, Конкурсная_масса: { controller: function () { return c_mpl_asset_grid({ base_url: base_url }); }, render_on_activate:true  }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.base_url = base_url;
		controller.base_crud_url = controller.base_url + '?action=viewer.crud';

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			if(!this.model || !this.model.isEscort)
			{
				delete options.field_spec.Сопровождение
			}
			base_Render.call(this, sel);

			var self = this;

			if(!this.model || !this.model.isEscort)
			{
				$(sel + ' [aria-controls="cpw-ama-datamart-viewer-main-escort"]').hide();
			}

			var c_procedures = this.fastening.get_fc_controller('Процедуры');
			c_procedures.ShowProcedure = function (id_text) { self.ShowProcedure(id_text); }

			$(sel + " .jquery-menu").menu();
			$(sel + " .profile-dropdown").click(function(){
				$(this).find("ul.jquery-menu").toggle();
			})

			//close menu
			$(document).on("click", function (e) {
				/*menu buttons*/
				if ($(e.target).hasClass("open-profile")) self.OnProfile();
				if ($(e.target).hasClass("notifications")) self.OnNotifications();
				if($(e.target).hasClass("logout")) { if (self.OnLogout) self.OnLogout(); }

				if(!$(e.target).hasClass("profile-dropdown") && !$(e.target).parent().hasClass("profile-dropdown")) {
					$(sel + " .profile-dropdown ul.jquery-menu").hide();
				}
			});

			$(sel + ' ul.ui-tabs-nav > li > a').click(function(){ $(window).resize(); });

			this.ProcessSection();

			$(sel + ' div#cpw-ama-datamart-viewer-main-schd button.add-procedure').click(function () { self.OnAddProcedure(); });
			$(sel + ' div#cpw-ama-datamart-viewer-main-news button.add-procedure').click(function () { self.OnAddProcedure(); });
			$(sel + ' div#cpw-ama-datamart-viewer-main-proc button.add-procedure').click(function () { self.OnAddProcedure(); });
			$(sel + ' div#cpw-ama-datamart-viewer-main-dmrt button.add-procedure').click(function () { self.OnAddProcedure(); });
			$(sel + ' div[data-fc-selector="Процедуры"]').on('model_change', function (){ self.ReloadNewsSchedule(); });
			$(sel + ' div#cpw-ama-datamart-viewer-main-tabs').on('tabsactivate', function (event, ui) { self.OnActivateTab(event, ui); });
		}

		controller.ProcessSection = function ()
		{
			if (app.open_section && 'false' != app.open_section)
			{
				var sel= this.fastening.selector;
				switch (app.open_section)
				{
					case "km":
						if (app.debtor_inn && app.debtor_inn != "")
						{
							this.Direct_to_km(app.debtor_inn);
							app.debtor_inn = "";
						}
						break;
					case "outcome":
						$(sel + ' #cpw-ama-datamart-viewer-main-tabs > ul > li > a[href="#cpw-ama-datamart-viewer-main-outm"]').click();
						break;
				}
				app.open_section = "false";
			}
		}

		controller.OnActivateTab = function (event, ui)
		{
			var tab_id= ui.newPanel.attr('id');
			if (this.need_reload && this.need_reload[tab_id])
			{
				this.need_reload[tab_id]= false;
				switch (tab_id)
				{
					case 'cpw-ama-datamart-viewer-main-schd':
						this.ReloadSchedule();
						break;
				}
			}
			
		}

		controller.ReloadSchedule = function ()
		{
			var fastening= this.fastening;
			var c_schedule= fastening.get_fc_controller('Календарь');
			if (c_schedule.model && null!=c_schedule.model)
				c_schedule.Reload();
		}

		controller.ReloadNewsSchedule = function ()
		{
			var fastening= this.fastening;
			var sel = fastening.selector;
			var c_news= fastening.get_fc_controller('Новости');
			if (c_news.model && null!=c_news.model)
				c_news.ReloadGrid();
			if ('false' == $(sel + ' div#cpw-ama-datamart-viewer-main-schd').attr('aria-hidden'))
			{
				this.ReloadSchedule();
			}
			else
			{
				if (!this.need_reload)
					this.need_reload = {};
				this.need_reload['cpw-ama-datamart-viewer-main-schd']= true;
			}
		}

		controller.OnAddProcedure = function ()
		{
			var self= this;
			var fastening= this.fastening;
			h_dm_viewer_add_request.OnRegisterRequest(this.base_url, this.model.id_MUser, function () {
				self.ReloadNewsSchedule();
				var c_procedures= fastening.get_fc_controller('Процедуры');
				if (c_procedures.model && null!=c_procedures.model)
					c_procedures.ReloadGrid();
			});
		}

		controller.Direct_to_km = function (inn)
		{
			var self = this;
			var sel = this.fastening.selector;
			var id_text = '';
			var ajaxurl = this.base_url + '?action=procedure.find.select2&INN=' + inn;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение информации о процедуре с сервера", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus) {
					if (data.id)
					{
						var id_text = data;
						$(sel + ' #cpw-ama-datamart-viewer-main-tabs > ul > li > a[href="#cpw-ama-datamart-viewer-main-dmrt"]').click();
						var c_datamart = self.fastening.get_fc_controller('Витрина');
						var on_done = function () { $(sel + ' #cpw-ama-datamart-viewer-main-dmrt > div > div > div > div > div > div >a[isection="2"]').click(); };
						c_datamart.SelectProcedure(id_text,on_done);
						
					}
					else
					{
						var reason = data.text ? data.text + ' отсутствуют' : 'отсутствуют на Витрине данных';
						h_msgbox.ShowModal({
							title: 'Информация о процедуре над должником с ИНН:' + inn,
							width: 800, height: 400,
							html: v_redirect_to_km_from_fa({reason:reason})
						});
					}
				}
			});
		}

		controller.ShowProcedure = function (id_text)
		{
			var sel = this.fastening.selector;
			$(sel + ' #cpw-ama-datamart-viewer-main-tabs > ul > li > a[href="#cpw-ama-datamart-viewer-main-dmrt"]').click();
			var c_datamart = this.fastening.get_fc_controller('Витрина');
			c_datamart.SelectProcedure(id_text);
		}

		controller.OkToSave= function(viewer)
		{
			var email = viewer.Email;
			var title = "Проверка перед сохранением";
			if (5>email.length)
			{
				h_msgbox.ShowModal({ title: title, width:410, html: "Длина E-mail должна быть не менее 5 символов!" });
				return false;
			}
			if (-1==email.indexOf('@'))
			{
				h_msgbox.ShowModal({ title: title, width: 410, html: "E-mail должен содержать символ '@'!" });
				return false;
			}
			var name = viewer.Имя.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
			if ('' == name)
			{
				h_msgbox.ShowModal({ title: title, width: 410, html: "Имя не может быть пустым!" });
				return false;
			}
			return true;
		}

		controller.OnProfile= function ()
		{
			var c_profile = c_dm_viewer_profile();
			var self = this;
			var profile = { Имя: this.model.Имя, Email: this.model.Email };
			if (this.model.Email_Change)
				profile.Email_Change = this.model.Email_Change;
			c_profile.SetFormContent(profile);
			var btnOk = 'Сохранить свойства пользователя витрины';
			h_msgbox.ShowModal
			({
				title: 'Настройка свойств пользователя витрины'
				, controller: c_profile
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-viewer"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var viewer = c_profile.GetFormContent();
						if (!self.OkToSave(viewer))
							return false;
						self.UpdateProfile(viewer);
					}
				}
			});
		}

		controller.OnNotifications = function ()
		{
			h_dm_push_profile.ModalEditPushProfile(base_url, this.model.id_MUser, 'v');
		}

		controller.UpdateProfile= function(profile)
		{
			var sel = this.fastening.selector;
			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=update&id=' + this.model.id_MUser;
			var v_ajax = h_msgbox.ShowAjaxRequest("Сохранение настроек пользователя на сервере", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'POST'
				, data: profile
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok)
					{
						self.model.Имя = profile.Имя;
						if (self.model.Email != profile.Email)
							self.model.Email_Change = profile.Email;
						$(sel + ' div > header div.profile-dropdown > span').text(profile.Имя);
					}
					else
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка сохранения профиля наблюдателя'
							, width: 450
							, html: '<span>Не удалось сохранить профиль наблюдателя по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
				}
			});
		}

		return controller;
	}
});