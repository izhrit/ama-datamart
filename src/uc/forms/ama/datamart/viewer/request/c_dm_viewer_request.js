﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/viewer/request/e_dm_viewer_request.html'
	, 'forms/ama/datamart/base/h_procedure_types'
	, 'forms/ama/datamart/base/h_Request_state'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_procedure_types, h_Request_state, h_validation_msg)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_select2 = base_url + '?action=efrsb.debtor.select2'

		var options = {
			field_spec:
				{
					Выбранная_процедура: { ajax: { url: base_url_select2, dataType: 'json' } }
				}
			, h_Request_state: h_Request_state
		};

		var controller = c_fastened(tpl, options);

		controller.size = { width: 700, height: 555 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			if (this.model && null != this.model && this.model.Выбранная_процедура && null!=this.model.Выбранная_процедура)
			{
				$(sel + ' [data-fc-selector="Выбранная_процедура"]').select2( "readonly", true );
				$(sel + ' div.row.small-help-text').css('display','none');
			}
			else
			{
				var self = this;
				$(sel + ' [data-fc-selector="Выбранная_процедура"]').on('select2-selected', function (e) { self.OnSelectProcedure(); });
			}
		}

		controller.OnSelectProcedure= function()
		{
			var sel = this.fastening.selector;
			var selected_data = $(sel + ' [data-fc-selector="Выбранная_процедура"]').select2('data');

			var proc = selected_data.data;
			var txtАУ = proc.АУ.Фамилия + ' ' + proc.АУ.Имя + ' ' + proc.АУ.Отчество;
			$(sel + ' [data-fc-selector="Процедура.Управляющий"]').val(txtАУ);
			$(sel + ' [data-fc-selector="Должник.ИНН"]').val(h_procedure_types.SafeTextForDBValue(proc.Должник.ИНН));
			$(sel + ' [data-fc-selector="Должник.ОГРН"]').val(h_procedure_types.SafeTextForDBValue(proc.Должник.ОГРН));
			$(sel + ' [data-fc-selector="Должник.СНИЛС"]').val(h_procedure_types.SafeTextForDBValue(proc.Должник.СНИЛС));
		}

		controller.Validate = function () {
			var res = null;
			var model = this.GetFormContent();
			var procedure = model.Выбранная_процедура;
			if (!procedure || null == procedure)
				res = h_validation_msg.error(res, "Необходимо выбрать процедуру на которую вы хотите подписаться");
			if ('' == h_validation_msg.trim(model.Процедура.КогоПредставляетЗаявитель))
				res = h_validation_msg.error(res, "Укажите кого вы представляете в процедуре");
			if ('' == h_validation_msg.trim(model.Пояснения))
				res = h_validation_msg.error(res, "Пояснение к запросу НЕ может быть пустым");
			return res;
		}

		return controller;
	}
});
