include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Кого вы представляете"
shot_check_png ..\..\shots\00new.png

play_stored_lines ama_datamart_viewer_request_fields_1

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01sav.json.result.txt

wait_click_text "Редактировать модель"
shot_check_png ..\..\shots\01sav.png

exit