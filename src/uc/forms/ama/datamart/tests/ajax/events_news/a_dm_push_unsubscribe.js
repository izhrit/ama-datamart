define([
	'forms/base/ajax/ajax-service-base'
],
function (ajax_service_base) {
	var service = ajax_service_base('ama/datamart?action=push.unsubscribe');

	service.action = function (cx) {
		var res = { "success": true }
		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});