define([
	'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
],
function (ajax_service_base, db, ql)
{
	var service = ajax_service_base('ama/datamart?action=push.test');

	service.action_receivers= function(cx, args)
	{
		var q= args.q;

		var prep_data= function(pr)
		{
			if (!pr || pr==null)
			{
				return pr;
			}
			else
			{
				return {
					id_Push_receiver: pr.id_Push_receiver
					,Transport: pr.Transport
					,Destination: 'a'!=pr.Transport ? '' : pr.Destination
				};
			}
		}

		var managers= ql.select(function (r) { return {
			id: 'm-' + r.m.id_Manager
			,text: r.m.lastName + ' ' + r.m.firstName + ' ' + r.m.middleName
			,data: prep_data(r.pr)
		};})
		.from(db.Manager, 'm')
		.left_join(db.Push_receiver,'pr',function(r){return 'm'==r.pr.Category && r.m.id_Manager==r.pr.id_Manager_Contract_MUser;})
		.where(function(r){return -1!=(r.m.lastName + ' ' + r.m.firstName + ' ' + r.m.middleName).indexOf(q);})
		.exec();

		var viewers= ql.select(function (r) { return {
			id: 'v-' + r.v.id_MUser
			,text: r.v.UserName
			,data: prep_data(r.pr)
		};})
		.from(db.MUser, 'v')
		.left_join(db.Push_receiver,'pr',function(r){return 'v'==r.pr.Category && r.v.id_MUser==r.pr.id_Manager_Contract_MUser;})
		.where(function(r){return -1!=(r.v.UserName).indexOf(q);})
		.exec();

		var contracts= ql.select(function (r) { return {
			id: 'c-' + r.c.id_Contract
			,text: r.c.ContractNumber
			,data: prep_data(r.pr)
		};})
		.from(db.Contract, 'c')
		.left_join(db.Push_receiver,'pr',function(r){return 'c'==r.pr.Category && r.c.id_Contract==r.pr.id_Manager_Contract_MUser;})
		.where(function(r){return -1!=(r.c.ContractNumber).indexOf(q);})
		.exec();

		var items= [];
		for (var i= 0; i<managers.length; i++)
			items.push(managers[i]);
		for (var i= 0; i<viewers.length; i++)
			items.push(viewers[i]);
		for (var i= 0; i<contracts.length; i++)
			items.push(contracts[i]);

		cx.completeCallback(200, 'success', { text: JSON.stringify({results:items}) });
	}

	service.action = function (cx)
	{
		var args= cx.url_args();
		switch (args.cmd)
		{
			case 'receivers': this.action_receivers(cx, args);
		}
	}

	return service;
});