define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
]
, function (ajax_jqGrid, db, ql)
{
	var transport = ajax_jqGrid();

	transport.options.url_prefix = 'ama/datamart?action=push.jqgrid';

	transport.rows_all = function (url, args)
	{
		if (!args.admin)
		{
			return receiver_rows;
		}
		else
		{
			var events= ql.select(function (r) { return {
				id_Push:'Pushed_event-' + r.pe.id_Event + '-' + r.pe.id_Push_receiver
				,Time_dispatched:ql.my.safe_ru_legal_time(r.pe,'Time_dispatched')
				,Time_pushed:ql.my.safe_ru_legal_time(r.pe,'Time_pushed')
				,Category:r.pr.Category
				,info:'о событии'
				,msg_Number:(null==r.msg ? '' : r.msg.Number)
				,msg_Type:(null==r.msg ? '' : r.msg.MessageInfo_MessageType)
				,error:r.pe.ErrorText
				,Transport:r.pr.Transport
				,receiver: 
					(null!=r.c) ? ('аб. ' + r.c.ContractNumber) : 
					(null!=r.m) ? ('АУ ' + r.m.lastName + ' ' + r.m.firstName.charAt(0) + r.m.middleName.charAt(0) ) :
					(null!=r.v) ? ('наб. ' + r.v.UserName ) :'?'
			};})
			.from(db.Pushed_event,'pe')
			.inner_join(db.Push_receiver, 'pr', function (r) { return r.pe.id_Push_receiver==r.pr.id_Push_receiver; })
			.inner_join(db.event, 'e', function (r) { return r.e.id_Event==r.pe.id_Event; })
			.left_join(db.Message, 'msg', function (r) { return r.msg.efrsb_id==r.e.efrsb_id; })
			.left_join(db.Contract, 'c', function (r) { return r.pr.id_Manager_Contract_MUser==r.c.id_Contract && 'c'==r.pr.Category; })
			.left_join(db.Manager, 'm', function (r) { return r.pr.id_Manager_Contract_MUser==r.m.id_Manager && 'm'==r.pr.Category; })
			.left_join(db.MUser, 'v', function (r) { return r.pr.id_Manager_Contract_MUser==r.v.id_MUser && 'v'==r.pr.Category; })
			.exec();
			var messages= ql.select(function (r) { return {
				id_Push:'Pushed_message-' + r.pm.id_Message + '-' + r.pm.id_Push_receiver
				,Time_dispatched: ql.my.safe_ru_legal_time(r.pm,'Time_dispatched')
				,Time_pushed: ql.my.safe_ru_legal_time(r.pm,'Time_pushed')
				,Category:r.pr.Category
				,info:'об объявлении'
				,msg_Number:(null==r.msg ? '' : r.msg.Number)
				,msg_Type:(null==r.msg ? '' : r.msg.MessageInfo_MessageType)
				,error:r.pm.ErrorText
				,Transport:r.pr.Transport
				,receiver: 
					(null!=r.c) ? ('аб. ' + r.c.ContractNumber) : 
					(null!=r.m) ? ('АУ ' + r.m.lastName + ' ' + r.m.firstName.charAt(0) + r.m.middleName.charAt(0) ) :
					(null!=r.v) ? ('наб. ' + r.v.UserName ) :'?'
			};})
			.from(db.Pushed_message,'pm')
			.inner_join(db.Push_receiver, 'pr', function (r) { return r.pm.id_Push_receiver==r.pr.id_Push_receiver; })
			.left_join(db.Message, 'msg', function (r) { return r.msg.id_Message==r.pm.id_Message; })
			.left_join(db.Contract, 'c', function (r) { return r.pr.id_Manager_Contract_MUser==r.c.id_Contract && 'c'==r.pr.Category; })
			.left_join(db.Manager, 'm', function (r) { return r.pr.id_Manager_Contract_MUser==r.m.id_Manager && 'm'==r.pr.Category; })
			.left_join(db.MUser, 'v', function (r) { return r.pr.id_Manager_Contract_MUser==r.v.id_MUser && 'v'==r.pr.Category; })
			.exec();
			return ql.union(events,messages);
		}
	}

	var receiver_rows= [
		{
			EventTime: "27.11.2020 12:00"
			, message: "Торги: Ларионов КК"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "Суд: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "СК: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "Суд: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "СК: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "Суд: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "СК: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "Суд: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "СК: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "Суд: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "СК: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "Суд: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "СК: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "Суд: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
		, {
			EventTime: "27.11.2020 12:00"
			, message: "СК: Сапарова ЕН"
			, Time_dispatched: "29.11.2020 12:30"
			, Time_pushed: "29.11.2020 13:30"
		}
	];

	return transport.prepare_try_to_prepare_send_abort();
});