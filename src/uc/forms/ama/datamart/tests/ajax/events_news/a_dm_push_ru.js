﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var crud_service = ajax_CRUD('ama/datamart?action=push.crud');

	var Time2DB = function (time) { if (time) { return parseInt(time.split(':')[0]); } }

	var DBint2Time = function (time) { if (time) { return ('0' + time).slice(-2) + ':00' } }

	function swap(json) {
		var ret = {};
		for (var key in json) {
			ret[json[key]] = key;
		}
		return ret;
	}

	var Уведомлять2DB = {
		'никогда': 'a',
		'за_1_день': 'b',
		'за_3_дня': 'c',
		'за_5_дней': 'd',
		'за_15_дней': 'e',
	}
	var Bool2DB = {
		'true': '1',
		'false': '0'
	}

	var Directions2DB = {
		'в_мобильное_приложение_на_телефон': 'a',
		'смс_сообщениями_на_телефон': 'b',
		'на_адрес_электронной_почты': 'c',
		'никуда': 'd'
	}

	crud_service.read = function (id, args)
	{
		var r= ql.find_first_or_null(db.Push_receiver, function (pr) { return id == pr.id_Manager_Contract_MUser && pr.Category==args.Category; });
		if (null == r)
		{
			return r;
		}
		else
		{
			var res= {
				id_Push_receiver: r.id_Push_receiver
				, О_событиях: {
					Уведомлять_об_объявлениях_на_ефрсб: (r.For_Message_after && null!=r.For_Message_after)
					, О_проведении_торгов: { Уведомлять: swap(Уведомлять2DB)[r.For_Auction] }
					, О_собраниях: { Уведомлять: swap(Уведомлять2DB)[r.For_Meeting] }
					, О_судебном_заседании: { Уведомлять: swap(Уведомлять2DB)[r.For_CourtDecision] }
				}
				, Присылать_уведомления: { 
					с: DBint2Time(r.Time_start)
					, до: DBint2Time(r.Time_finish)
					, Часовой_пояс: 'мск' + ((r.Timezone<0) ? r.Timezone : ('+' + r.Timezone))
				}
				, Учитывать_выходные: swap(Bool2DB)[r.Check_day_off] === 'true' ? true : false
				, Направлять: swap(Directions2DB)[r.Transport]
				, ПунктНазначения: r.Destination
			};
			return res;
		}
	}

	crud_service.update = function (id, push_settings, args)
	{
		var r= null==push_settings.id_Push_receiver 
			? ql.insert_with_next_id(db.Push_receiver, 'id_Push_receiver', { Category: args.Category, id_Manager_Contract_MUser: id })
			: ql.find_first_or_null(db.Push_receiver, function (pr) { return push_settings.id_Push_receiver == pr.id_Push_receiver; });

		var efrsb= push_settings.О_событиях;
		r.For_Meeting = Уведомлять2DB[efrsb.О_собраниях.Уведомлять];
		r.For_MeetingPB = Уведомлять2DB[efrsb.О_собраниях.Уведомлять];
		r.For_Committee = Уведомлять2DB[efrsb.О_собраниях.Уведомлять];
		r.For_MeetingWorker= Уведомлять2DB[efrsb.О_собраниях.Уведомлять];
		r.For_Auction= Уведомлять2DB[efrsb.О_проведении_торгов.Уведомлять];
		r.For_CourtDecision= Уведомлять2DB[efrsb.О_судебном_заседании.Уведомлять];
		r.For_Message_after = !efrsb.Уведомлять_об_объявлениях_на_ефрсб ? null : "2019-10-13T10:01";
		r.ReceiverGUID = "bbf6a11418e4af96b009682b8ce74409";

		r.Check_day_off= Bool2DB[push_settings.Учитывать_выходные];

		var время= push_settings.Присылать_уведомления;
		r.Time_start= Time2DB(время.с);
		r.Time_finish= Time2DB(время.до);
		r.Timezone= 'мск-1'==время.Часовой_пояс ? -1 : parseInt(время.Часовой_пояс.substring('мск+'.length));

		r.Transport= Directions2DB[push_settings.Направлять];
		r.Destination= push_settings.ПунктНазначения;
	}

	return crud_service;
});