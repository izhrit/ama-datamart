﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/ama/datamart/tests/d_datamart_read'
],
function (ajax_service_base, d_datamart, d_datamart_read)
{
	var service= ajax_service_base('ama/datamart_testdb');

	var read= function()
	{
		return d_datamart;
	}

	var store = function (data)
	{
		d_datamart_read.content = data;
		return { ok: true };
	}

	service.action= function (cx)
	{
		var res= null;
		switch (cx.url_args().cmd)
		{
			case 'read': res = read(); break;
			case 'store': res = store(cx.data_args()); break;
		}
		var txt_res = JSON.stringify(res);
		cx.completeCallback(200, 'success', { text: txt_res });
	}

	return service;
});