﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
],
function (ajax_service_base, db, ql)
{
	var service= ajax_service_base('ama/datamart?action=job-monitor');

	service.action= function (cx)
	{
		var res= [
			{
				"title":"datamart"
				,"description":"Витрина данных ПАУ, расположена на сайте rsit.ru"
				,"jobs":[
					{
						"title":"nightly"
						,"description":"работы, которые должны выполняться каждую ночь"
						,"max_age_minutes":1440
						,"status":"ok"
						,"last":{
							"started":"2020-01-01T12:00:00"
							,"finished":"2020-01-01T12:01:00"
							,"status":"ok"
						}
						,"parts":[
							{
								"title":"verify_manager"
								,"description":"верифицировать вновь добавленных АУ по ЕФРСБ"
								,"status":"ok"
								,"last":{
									 "started":"2020-01-01T12:00:00"
									,"finished":"2020-01-01T12:01:00"
									,"status":"ok"
									,"results":"синхронизировано 100 АУ из 300, 5 подтверждено"
								}
							}
						]
					}
				]
			}
		];
		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});