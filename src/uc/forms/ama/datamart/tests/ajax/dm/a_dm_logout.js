﻿define([
	  'forms/base/ajax/ajax-service-base'
],function (ajax_service_base)
{
	var service= ajax_service_base('ama/datamart?action=logout');

	service.action= function (cx)
	{
		cx.completeCallback(200, 'success', { text: '' });
	}

	return service;
});