﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
	, 'forms/ama/datamart/base/h_cryptoapi'
],
function (ajax_service_base, db, ql, h_ctyptoapi)
{
	var service= ajax_service_base('ama/datamart?action=login');

	var GetTokenToSign = function (data)
	{
		var subject = h_ctyptoapi.GetDNFields(data.Subject);
		var Manager = ql.find_first_or_null(db.Manager, function (m) { return m.INN == subject.ИНН; });

		var efrsb_manager = ql.find_first_or_null(db.efrsb_manager, function (m) { return m.INN == subject.ИНН; });

		if (null != Manager)
		{
			return { token: 'exists' };
		}
		else if (null != efrsb_manager)
		{
			var token = {
				token: 'test_auth_token_to_sign_for_id_Manager_from_efrsb_' + efrsb_manager.id_Manager
			}
			return token;
		}
		else
		{
			return null;
		}
	}

	// здесь проверки не происходит, т.к. браузер блокирует запросы к другим доменным именам
	var AuthByTokenSignature = function (data) {
		var signature = data.base64_encoded_signature;
		var parts = signature.split('_');
		
		var id_Manager = parts[parts.length - 1];

		var Manager = ql.find_first_or_null(db.Manager, function (m) { return m.id_Manager == id_Manager; });

		var efrsb_manager = ql.find_first_or_null(db.efrsb_manager, function (m) { return m.id_Manager == id_Manager; });

		if (null != Manager) {
			var auth_info = {
				id_Manager: id_Manager
				, ArbitrManagerID: Manager.ArbitrManagerID

				, Фамилия: Manager.LastName
				, Имя: Manager.FirstName
				, Отчество: Manager.MiddleName
				, ИНН: Manager.INN
				, efrsbNumber: Manager.RegNum
			}
		}
		else if (null != efrsb_manager) {
			var auth_info = {
				id_Manager: id_Manager
				, ArbitrManagerID: efrsb_manager.ArbitrManagerID

				, Фамилия: efrsb_manager.LastName
				, Имя: efrsb_manager.FirstName
				, Отчество: efrsb_manager.MiddleName
				, ИНН: efrsb_manager.INN
				, efrsbNumber: efrsb_manager.RegNum
			}
		} else {
			return null;
		}

		return auth_info;
	}

	var login_manager= function(cx,args, certcmd, cmd)
	{
		if ('login' == certcmd)
		{
			switch (cmd) {
				case 'get-token-to-sign':
					var token = GetTokenToSign(cx.originalOptions.data);
					cx.completeCallback(200, 'success', { text: JSON.stringify({ token: token }) });
					return;
				case 'auth-by-token-signature':
					var res = AuthByTokenSignature(cx.originalOptions.data);
					cx.completeCallback(200, 'success', { text: JSON.stringify(res, null, '\t') });
					return;
				case 'logout':
					cx.completeCallback(200, 'success', { text: JSON.stringify({ ok: true }, null, '\t') });
					return;
			}
		}
		else
		{
			var Manager = ql.find_first_or_null(db.Manager, function (r) {
				return r.ManagerEmail == args.Login && r.Password == args.Password;
			});
			return null == Manager ? null : {
				firstName: Manager.firstName
				, lastName: Manager.lastName
				, middleName: Manager.middleName
				, efrsbNumber: Manager.efrsbNumber
				, INN: Manager.INN
				, id_Manager: Manager.id_Manager
				, HasIncoming: Manager.HasIncoming
				, id_Contract: Manager.id_Contract
			};
		}
	}

	var login_viewer = function (args, passwordcmd)
	{
		if ('change' == passwordcmd)
		{
			var MUser = ql.find_first_or_null(db.MUser, function (r) { return r.UserEmail == args.Login; });
			if (null != MUser)
				MUser.UserPassword = 'new_password';
			return args.Login;
		}
		else
		{
			var MUser = ql.find_first_or_null(db.MUser, function (r)
			{
				return r.UserEmail == args.Login && r.UserPassword == args.Password;
			});
			if(MUser)
			{
				var ContractUser = ql.find_first_or_null(db.ContractUser, function (r)
				{
					return r.id_MUser == MUser.id_MUser;
				});
			}
			
			return null == MUser ? null : {
				Имя: MUser.UserName
				, id_MUser: MUser.id_MUser
				, Email: MUser.UserEmail
				, HasOutcoming: MUser.HasOutcoming
				, isEscort: ContractUser ? 1 : 0
			};
		}
	}

	var login_customer = function (args)
	{
		var mock_crm2_Contract = ql.find_first_or_null(db.mock_crm2_Contract, function (r)
		{
			return r.ContractNumber == args.Login && r.Password == args.Password;
		});
		if (!mock_crm2_Contract || null == mock_crm2_Contract)
		{
			return null;
		}
		else
		{
			var Contract = ql.find_first_or_null(db.Contract, function (r)
			{
				return r.ContractNumber == mock_crm2_Contract.ContractNumber;
			});
			if (null == Contract)
			{
				Contract = ql.insert_with_next_id(db.Contract, 'id_Contract', {
					ContractNumber: mock_crm2_Contract.ContractNumber
				});
			}
			return { 
				id_Contract: Contract.id_Contract
				, ContractNumber: Contract.ContractNumber
			};
		}
	}

	var login_sro= function (args)
	{
		var sro = ql.find_first_or_null(db.efrsb_sro, function (r)
		{
			return r.SROEmail == args.Login && r.SROPassword == args.Password;
		});
		return null == sro ? null : {
			  Name: sro.Name
			, Title: sro.Title
			, ShortTitle: sro.ShortTitle
			, INN: sro.INN
			, RegNum: sro.RegNum
			, id_SRO: sro.id_SRO
		};
	}

	var login_admin = function (args)
	{
		if ('1' == args.Login && '1' == args.Password)
		{
			return {id_Admin:1};
		}
		else
		{
			return null;
		}
	}

	service.action= function (cx)
	{
		var args= cx.url_args();
		var res= null;
		switch (args.as)
		{
			case 'viewer': res = login_viewer(cx.data_args(), args.passwordcmd); break;
			case 'manager': res = login_manager(cx,cx.data_args(), args.certcmd, args.cmd); break;
			case 'customer': res = login_customer(cx.data_args(), args.passwordcmd); break;
			case 'sro': res = login_sro(cx.data_args(), args.passwordcmd); break;
			case 'admin': res = login_admin(cx.data_args(), args.passwordcmd); break;
		}
		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});