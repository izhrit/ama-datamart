﻿define
([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-select2'
	, 'forms/base/ql'
	, 'forms/ama/datamart/base/h_procedure_types'
]
, function (db, ajax_select2, ql, h_procedure_types)
{
	var service = ajax_select2('ama/datamart?action=meeting.select2');

	service.query = function (q, page, args)
	{
		var maxSize = 15;

		var rows = ql.select(function (r) { return {
				id: r.cm.id_Meeting
				, text: r.cm.Date + " (" + h_procedure_types.PrepareText(r) + ')'
			};})
			.from(db.Meeting, 'cm')
			.inner_join(db.MProcedure, 'mp', function (r) { return (r.cm.id_MProcedure == r.mp.id_MProcedure); })
			.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
			.inner_join(db.Manager, 'm', function (r) { return (r.mp.id_Manager == r.m.id_Manager); })
			.where(function (r)
			{
				return (!args.id_Manager || args.id_Manager == r.mp.id_Manager)
					&& (-1 != r.d.Name.indexOf(q))
			})
			.limit(100)
			.exec();

		var more = rows.length == maxSize && i < db.MProcedure.length;

		return { results: rows, pagination: { more: more } };
	}

	return service;
});
