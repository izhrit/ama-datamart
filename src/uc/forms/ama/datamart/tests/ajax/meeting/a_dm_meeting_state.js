﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/ama/datamart/tests/ajax/meeting/h_dm_meeting'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/ama/datamart/base/h_SentEmail'
	, 'forms/base/h_times'
],
function (ajax_service_base, ql, db, h_dm_meeting, codec_copy, h_codec_datetime, h_SentEmail, h_times)
{
	var service = ajax_service_base('ama/datamart?action=meeting.state');

	var start = function (args)
	{
		var dt_codec = h_codec_datetime.ru_txt2dt();

		var Meeting = ql.find_first_or_null(db.Meeting, function (m) { return args.id_Meeting == m.id_Meeting; });
		for (var i = 0; i < Meeting.Body.Участники.length; i++)
		{
			var участник = Meeting.Body.Участники[i];
			var время = dt_codec.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()));
			var vote = ql.insert_with_next_id(db.Vote, 'id_Vote', {
				id_Meeting: Meeting.id_Meeting
				, CabinetKey: '12345678901234567890'
				, CabinetTime: время
				, Member: codec_copy().Copy(участник)
			});
			var row_SentEmail = h_SentEmail.PrepareRow('о заседании КК', {
				RecipientId: vote.id_Vote
				, RecipientEmail: участник.email
				, Message: 'кароче, дело к ночи'
				, Body: 'скоро будет заседание!'
				, TimeDispatch: время
			});
			ql.insert_with_next_id(db.SentEmail, 'id_SentEmail', row_SentEmail);
		}
		Meeting.State = 'b';
		Meeting.Body.Состояние = 'Голосование';

		var Голосование = h_dm_meeting.Голосование(args.id_Meeting);

		return Голосование;
	}

	var pause = function (args)
	{
		var Meeting = ql.find_first_or_null(db.Meeting, function (m) { return args.id_Meeting == m.id_Meeting; });
		Meeting.State = 'c';
		Meeting.Body.Состояние = 'Приостановлено';
		return {ok:true};
	}

	var took_place = function (args, data_decoded_as_args)
	{
		var dt_codec = h_codec_datetime.ru_txt2dt();
		var время = dt_codec.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()));

		var Meeting = ql.find_first_or_null(db.Meeting, function (m) { return args.id_Meeting == m.id_Meeting; });

		Meeting.Результаты = data_decoded_as_args.результаты;
		ql.from(db.Vote, 'v')
		.where(function (r) { return r.v.id_Meeting == args.id_Meeting })
		.map(function (r)
		{
			var vote = r.v;
			var row_SentEmail = h_SentEmail.PrepareRow('об итогах КК', {
				RecipientId: vote.id_Vote
				, RecipientEmail: vote.Member.email
				, Message: 'подвели результаты'
				, Body: 'результаты отличные!'
				, TimeDispatch: время
			});
			ql.insert_with_next_id(db.SentEmail, 'id_SentEmail', row_SentEmail);
			var row_log = { id_Vote: vote.id_Vote, Vote_log_type: 'r', Vote_log_time: время };
			ql.insert_with_next_id(db.Vote_log, 'id_Vote_log', row_log);
		})

		return { ok: true };
	}

	var did_not_took_place = function (args, data_decoded_as_args)
	{
		var dt_codec = h_codec_datetime.ru_txt2dt();
		var время = dt_codec.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()));

		var Meeting = ql.find_first_or_null(db.Meeting, function (m) { return args.id_Meeting == m.id_Meeting; });

		Meeting.Заседание_не_состоялось = { Пояснения: data_decoded_as_args.Пояснения };
		ql.from(db.Vote, 'v')
		.where(function (r) { return r.v.id_Meeting == args.id_Meeting })
		.map(function (r)
		{
			var vote = r.v;
			var row_SentEmail = h_SentEmail.PrepareRow('об итогах КК', {
				RecipientId: vote.id_Vote
				, RecipientEmail: vote.Member.email
				, Message: 'заседание не состоялось'
				, Body: 'заседание не состоялось, пояснение:' + data_decoded_as_args.Пояснения
				, TimeDispatch: время
			});
			ql.insert_with_next_id(db.SentEmail, 'id_SentEmail', row_SentEmail);
			var row_log = { id_Vote: vote.id_Vote, Vote_log_type: 'r', Vote_log_time: время };
			ql.insert_with_next_id(db.Vote_log, 'id_Vote_log', row_log);
		})

		return { ok: true };
	}

	service.action= function (cx)
	{
		var args= cx.url_args();
		var res = null;
		switch (args.state)
		{
			case 'b': res = start(args); break;
			case 'c': res = pause(args); break;
			case 'e': res = took_place(args, cx.data_args()); break;
			case 'f': res = did_not_took_place(args, cx.data_args()); break;
			default:
				completeCallback(400, 'Bad Request', '');
				return;
		}

		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});