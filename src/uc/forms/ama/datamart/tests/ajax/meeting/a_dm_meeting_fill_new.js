﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/h_times'
],
function (ajax_service_base, ql, db, h_codec_datetime, h_times)
{
	var service = ajax_service_base('ama/datamart?action=meeting.fill-new');

	service.action= function (cx)
	{
		var rows = ql.from(db.MProcedure, 'mp')
		.where(function (r) { return r.mp.id_MProcedure == cx.url_args().id_MProcedure; })
		.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
		.inner_join(db.Manager, 'm', function (r) { return r.mp.id_Manager == r.m.id_Manager; })
		.left_join(db.Meeting, 'c', function (r) { return r.c.id_MProcedure == r.mp.id_MProcedure; })
		.limit(1)
		.rows;

		var row = rows[0];

		var res = {
			Должник: {
				Наименование: row.d.Name
				, ИНН: row.d.INN
				, ОГРН: row.d.OGRN
				, СНИЛС: row.d.SNILS
			}
			,Время_заседания: '18:00'
		};

		var dtnow = h_times.safeDateTime();
		var dt_codec = h_codec_datetime.ru_date_txt2dt();
		var addDays = function (d, days)
		{
			var date = new Date(d.valueOf());
			date.setDate(date.getDate() + days);
			return date;
		}
		var dtзаседания = addDays(dtnow, 15);
		res.Дата_заседания = dt_codec.Decode(h_codec_datetime.Date2dt(dtзаседания));

		if (row.m.ExtraParams)
		{
			var p = row.m.ExtraParams;
			res.Телефон_АУ = p.Телефон;
			res.Ознакомление = {
				С_материалами: { 
					Адрес: p.Адрес.Рабочий
					, Время: { Начало: p.Приёмное_время.С, Конец: p.Приёмное_время.До }
					, С_даты: dt_codec.Decode(h_codec_datetime.Date2dt(addDays(dtзаседания, -8)))
				}
				, С_результатами: { 
					Адрес: p.Адрес.Рабочий
					, Время: { Начало: p.Приёмное_время.С, Конец: p.Приёмное_время.До }
					, До_даты: dt_codec.Decode(h_codec_datetime.Date2dt(addDays(dtзаседания, +7)))
				}
			}
		}

		if (row.mp.ExtraParams)
			res.Должник.Местонахождение = row.mp.ExtraParams.Адрес;

		if (row.c && null != row.c)
			res.Участники = row.c.Body.Участники;

		res.Offer_accepted = '' + !ql.from(db.Meeting, 'c')
			.inner_join(db.MProcedure, 'mp', function (r) { return r.c.id_MProcedure == r.mp.id_MProcedure; })
			.where(function (r) { return r.mp.id_Manager == row.m.id_Manager; })
			.empty();

		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});