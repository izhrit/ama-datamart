﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/base/h_procedure_types'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/ama/datamart/tests/ajax/meeting/h_dm_meeting'
],
function (ajax_service_base, h_procedure_types, ql, db, h_dm_meeting)
{
	var service = ajax_service_base('ama/datamart?action=meeting.nearest');

	service.action= function (cx)
	{
		var rows = ql.select(function (r) { return {
			selected_data: { id: r.cm.id_Meeting, text: r.cm.Date + " (" + h_procedure_types.PrepareText(r) + ')' }
			, meeting_info: h_dm_meeting.Meeting(r)
		};})
		.from(db.Meeting, 'cm')
		.inner_join(db.MProcedure, 'mp', function (r) { return (r.cm.id_MProcedure == r.mp.id_MProcedure); })
		.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
		.inner_join(db.Manager, 'm', function (r) { return (r.mp.id_Manager == r.m.id_Manager); })
		.where(function (r) { return (cx.url_args().id_Manager == r.mp.id_Manager); })
		.exec();

		var text = (0 >= rows.length) ? null : JSON.stringify(rows[0]);
		cx.completeCallback(200, 'success', { text: text });
	}

	return service;
});