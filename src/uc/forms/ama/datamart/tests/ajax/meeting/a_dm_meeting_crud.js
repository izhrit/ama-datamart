﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/ajax/meeting/h_dm_meeting'
]
, function (db, ajax_CRUD, ql, h_dm_meeting)
{
	var service= ajax_CRUD('ama/datamart?action=meeting.crud');

	service.create = function (meeting)
	{
		return ql.insert_with_next_id(db.Meeting, 'id_Meeting', {
			id_MProcedure: meeting.id_MProcedure
			, Date: meeting.Дата_заседания
			, Body: meeting
		}).id_Meeting;
	}

	service.update = function (id, meeting)
	{
		var Meeting = ql.find_first_or_null(db.Meeting, function (r) { return r.id_Meeting == id; });
		Meeting.Body= meeting;
		Meeting.Date = meeting.Дата_заседания;
	}

	service._delete = function (ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id = id_arr[i];
			db.Meeting = ql._delete(db.Meeting, function (r) { return r.id_Meeting == id; });
		}
	}

	service.read = function (id)
	{
		var rows= ql.select(function (r) { return h_dm_meeting.Meeting(r); })
			.from(db.Meeting, 'cm')
			.inner_join(db.MProcedure, 'mp', function (r) { return r.cm.id_MProcedure == r.mp.id_MProcedure; })
			.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
			.where(function (r) { return id == r.cm.id_Meeting; })
			.exec();
		return rows[0];
	}

	return service;
});