﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
],
function (ajax_service_base, db, ql)
{
	var service = ajax_service_base('ama/datamart?action=meeting.document');

	var document_count = 0;

	var get_document= function(args)
	{
		document_count++;
		if (''!=args.id_SentEmail)
		{
			var row_SentEmail = ql.find_first_or_null(db.SentEmail, function (r) { return r.id_SentEmail == args.id_SentEmail; });
			return null == row_SentEmail ? 'Запись о письме №' + args.id_SentEmail + 'НЕ найдена!' : row_SentEmail.ExtraParams;
		}
		else if ('' != args.id_Vote_document)
		{
			var row_Vote_document = ql.find_first_or_null(db.Vote_document, function (r) { return r.id_Vote_document == args.id_Vote_document; });
			return null == row_Vote_document ? 'Запись о документе №' + args.id_Vote_document + ' НЕ найдена!' : row_Vote_document.Body;
		}
		else
		{
			var txt = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus id lorem at mi feugiat dictum. Donec viverra nulla velit, nec sagittis diam placerat a. Aliquam pharetra condimentum sapien. Sed volutpat dui mauris, at interdum ligula tincidunt a. Suspendisse non maximus dolor. In tincidunt imperdiet tempor. Sed ac nulla at lacus pretium faucibus. Duis mauris odio, rutrum sed pharetra quis, convallis at urna. Vivamus at lectus ut felis varius commodo rhoncus ut ligula. Fusce neque nisl, ultrices nec enim ut, convallis luctus sapien. Mauris ut ipsum eros. Pellentesque vehicula tristique neque, vel rutrum massa convallis vitae.';
			txt = args.id_SentEmail + args.id_Vote_document + ' : ' + txt + txt + txt + txt;
			txt = (1 == (document_count % 2)) ? txt : { Subject:"Тестовая тема чтобы посмотреть как будет смотреться на экране", Body: txt };
			return txt;
		}
	}

	service.action= function(cx)
	{
		cx.completeCallback(200, 'success', { text: JSON.stringify(get_document(cx.url_args())) });
	}

	return service;
});