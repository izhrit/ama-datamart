﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/codec/codec.copy'
	, 'forms/ama/datamart/base/h_SentEmail'
	, 'forms/ama/datamart/base/h_vote_document'
	, 'forms/base/h_times'
	, 'forms/ama/datamart/tests/ajax/meeting/h_dm_meeting'
],
function (ajax_service_base, db, ql, h_codec_datetime, codec_copy, h_SentEmail, h_vote_document, h_times, h_dm_meeting)
{
	var service = ajax_service_base('ama/datamart?action=meeting.cabinetcc');

	var store_answer = function (время, a, id_Vote)
	{
		var row = { id_Vote: id_Vote, Vote_log_type: 'a', Vote_log_time: время };
		if (a.На_вопрос)
			row.body = a.На_вопрос.Номер;
		ql.insert_with_next_id(db.Vote_log, 'id_Vote_log', row);

		var vote = ql.find_first_or_null(db.Vote, function (v) { return id_Vote == v.id_Vote });
		if (!vote.Answers)
			vote.Answers = [];
		var answer = { На_вопрос: { Номер: a.На_вопрос.Номер }, Ответ: a.Ответ };
		vote.Answers.push(answer);

		return { Журнал: [row] };
	}

	var throw_code= function(время, c, id_Vote)
	{
		var Участник = c.data.Участник;
		var row_Vote_log_sms = { id_Vote: id_Vote, Vote_log_type: 'c', Vote_log_time: время, body: Участник.телефон };
		var row_Vote_log_email = { id_Vote: id_Vote, Vote_log_type: 'b', Vote_log_time: время, body: Участник.email };
		ql.insert_with_next_ids(db.Vote_log, 'id_Vote_log', [row_Vote_log_sms, row_Vote_log_email]);

		var row_Vote_document = h_vote_document.PrepareRow('Бюллетень', {
			id_Vote: id_Vote
			, Vote_document_time: время
			, Body: c.text
		});
		ql.insert_with_next_id(db.Vote_document, 'id_Vote_document', row_Vote_document);

		var row_SentEmail = h_SentEmail.PrepareRow('о начале подписания КК', { 
			 RecipientId: id_Vote 
			, RecipientEmail: Участник.email
			, Message: 'кароче, дело к ночи'
			, TimeDispatch: время
			, Body: 'Заказан код подтверждения'
		});
		ql.insert_with_next_id(db.SentEmail, 'id_SentEmail', row_SentEmail);

		return {
			sms: { number: Участник.телефон, time: время }
			, test_code: '3141'
			, email: { address: Участник.email, time: время }
			, Журнал: [row_Vote_log_sms, row_Vote_log_email]
			, Документы: [row_SentEmail, row_Vote_document]
		};
	}

	var catch_code= function(время, c, id_Vote)
	{
		switch (c.code)
		{
			case '1':
				return { ok: false, why: 'Указанный код подтверждения не соответствует высланному!' };
			case '2':
				return { ok: false, why: 'С момента отправки кода подтверждения прошло слишком много времени!' };
			default:
				var vote = ql.find_first_or_null(db.Vote, function (v) { return id_Vote == v.id_Vote });
				if (vote.Answers)
				{
					for (var i = 0; i < vote.Answers.length; i++)
						vote.Answers[i].Подписано = время;
				}
				var Участник = c.data.Участник;
				var row_Vote_document_txt_sig = h_vote_document.PrepareRow('Подпись', {
					id_Vote: id_Vote
					, Vote_document_time: время
					, Body: 'собственноручно подписал!'
				});
				ql.insert_with_next_id(db.Vote_document, 'id_Vote_document', row_Vote_document_txt_sig);
				var row_Vote_document_sig = h_vote_document.PrepareRow('ПодписьОператора', {
					id_Vote: id_Vote
					, Vote_document_time: время
					, Body: 'да, действительно, подписал!'
				});
				ql.insert_with_next_id(db.Vote_document, 'id_Vote_document', row_Vote_document_sig);
				var row_SentEmail = h_SentEmail.PrepareRow('о подписании КК', {
					RecipientId: id_Vote
					, RecipientEmail: Участник.email
					, Message: 'кароче, дело к ночи'
					, TimeDispatch: время
					, Body: 'Бюллетени подписаны!'
				});
				ql.insert_with_next_id(db.SentEmail, 'id_SentEmail', row_SentEmail);
				var row_log = { id_Vote: id_Vote, Vote_log_type: 's', Vote_log_time: время };
				ql.insert_with_next_id(db.Vote_log, 'id_Vote_log', row_log);
				var Документы = [row_Vote_document_txt_sig,row_Vote_document_sig,row_SentEmail];
				return { ok: true, Подписано: время, Документы: Документы, Журнал: [row_log] };
		}
	}

	var login= function(key)
	{
		var ccopy = codec_copy();
		var rows= ql.select(function (r)
			{
				var res = {
					id_Vote: r.v.id_Vote
					,Кабинет: {
						Участник: ccopy.Copy(r.v.Member)
						, Дата_заседания: r.m.Date
						, Должник: {
							Наименование: r.d.Name
							, Местонахождение: !r.m.Body.Должник ? '' : r.m.Body.Должник.Местонахождение
						}
					}
					, Вопросы: ccopy.Copy(r.m.Body.Вопросы)
					, Ответы: ccopy.Copy(r.v.Answers)
					, Результаты: ccopy.Copy(r.m.Результаты)
					, Состояние: r.m.Body.Состояние
				};
				for (var i = 0; i < res.Вопросы.length; i++)
				{
					res.Вопросы[i].Номер = '' + (i + 1);
				}
				return res;
			})
			.from(db.Vote, 'v')
			.where(function (r) { return key == (r.v.CabinetKey + r.v.id_Vote); })
			.inner_join(db.Meeting, 'm', function (r) { return r.v.id_Meeting == r.m.id_Meeting; })
			.inner_join(db.MProcedure, 'mp', function (r) { return r.mp.id_MProcedure == r.m.id_MProcedure; })
			.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
			.exec();
		var cabonetcc = rows[0];

		h_dm_meeting.Load_ЖурналДокументы(cabonetcc);

		return (null == rows && 0 == rows.length) ? null : cabonetcc;
	}

	service.action = function (cx)
	{
		var dt_codec = h_codec_datetime.ru_txt2dt();
		var время = dt_codec.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()));
		var args= cx.url_args();
		var res = null;
		switch (args.cmd)
		{
			case 'login': res = login(args.key); break;
			case 'store-answer': res = store_answer(время, cx.data_args(), args.id_Vote); break;
			case 'throw-code': res = throw_code(время, cx.data_args(), args.id_Vote); break;
			case 'catch-code': res = catch_code(время, cx.data_args(), args.id_Vote); break;
			default:
				completeCallback(400, 'Bad Request', '');
				return;
		}
		var text = JSON.stringify(res);
		cx.completeCallback(200, 'success', { text: text });
	}

	return service;
});