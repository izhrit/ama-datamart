﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/ql'
	, 'forms/ama/datamart/base/h_SentEmail'
	, 'forms/ama/datamart/base/h_vote_log'
	, 'forms/ama/datamart/base/h_vote_document'
]
, function (db, codec_copy, ql, h_SentEmail, h_vote_log, h_vote_document)
{
	var helper = {};

	helper.Голосование= function(id_Meeting)
	{
		var res= ql.from(db.Vote, 'v')
		.where(function (r) { return id_Meeting == r.v.id_Meeting; })
		.map(function (rrr)
		{
			var n = rrr.v.Member;
			var res= {
				ФИО: n.Фамилия + ' ' + n.Имя + ' ' + n.Отчество
				, email: n.email
				, Уведомлен: rrr.v.CabinetTime
				, id_Vote: rrr.v.id_Vote
			};
			if (rrr.v.Answers && rrr.v.Answers.length > 0 && rrr.v.Answers[0].Подписано)
			{
				res.Ответов = rrr.v.Answers.length;
				res.Проголосовал = rrr.v.Answers[0].Подписано;
				res.Ответы = rrr.v.Answers;
			}
			return res;
		});
		return res;
	}

	helper.Meeting= function(r)
	{
		var meeting = codec_copy().Copy(r.cm.Body);
		var date = r.cm.Date;
		if (-1 != date.indexOf(' '))
			date= date.split(' ')[0];
		meeting.Дата_заседания = date;
		meeting.Голосование = this.Голосование(r.cm.id_Meeting);
		if (!meeting.Должник)
			meeting.Должник = {};
		var Должник = meeting.Должник;
		Должник.Наименование = r.d.Name;
		Должник.ИНН = r.d.INN;
		Должник.ОГРН = r.d.OGRN;
		Должник.СНИЛС = r.d.SNILS;
		return meeting;
	}

	helper.Load_ЖурналДокументы= function(vote_cabinet)
	{
		vote_cabinet.Журнал = ql.from(db.Vote_log, 'vl')
			.where(function (r) { return vote_cabinet.id_Vote == r.vl.id_Vote; })
			.map(function (r) { return r.vl; });
		var emails = ql.from(db.SentEmail, 'se')
			.where(function (r) { return h_SentEmail.IsFor(r.se, 'Vote', vote_cabinet.id_Vote); })
			.map(function (r) { return r.se; });
		var documents = ql.from(db.Vote_document, 'd')
			.where(function (r) { return vote_cabinet.id_Vote == r.d.id_Vote; })
			.map(function (r) { return r.d; });
		vote_cabinet.Документы = ql.union(emails, documents);
	}

	return helper;
});