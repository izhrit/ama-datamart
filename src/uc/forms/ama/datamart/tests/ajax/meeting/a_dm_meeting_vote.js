﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/ama/datamart/tests/ajax/meeting/h_dm_meeting'
],
function (ajax_service_base, ql, db, h_dm_meeting)
{
	var service = ajax_service_base('ama/datamart?action=meeting.vote');

	service.action= function (cx)
	{
		var args= cx.url_args();
		var items = ql.from(db.Vote, 'v')
		.where(function (r) { return (args.id_Vote == r.v.id_Vote); })
		.inner_join(db.Meeting, "m", function (r) { return r.v.id_meeting = r.m.id_Meeting; })
		.map(function (r)
		{
			var res= {
				ФИО: r.v.Member.Фамилия + ' ' + r.v.Member.Имя + ' ' + r.v.Member.Отчество
				, email: r.v.Member.email
				, телефон: r.v.Member.телефон
				, Вопросы: []
				, id_Vote: args.id_Vote
			};
			for (var i = 0; i < r.m.Body.Вопросы.length; i++)
			{
				var в = r.m.Body.Вопросы[i];
				var вопрос = {
					Номер: !в.Номер ? (i + 1) : в.Номер
					, На_голосование: { Формулировка: в.На_голосование.Формулировка }
					, В_повестке: в.В_повестке
				};
				var ответ = ql.find_first_or_null(r.v.Answers, function (rr) { return в.Номер == rr.На_вопрос.Номер; });
				if (null != ответ)
				{
					вопрос.Ответ = ответ.Ответ;
					if (ответ.Подписано)
						вопрос.Подписано = ответ.Подписано;
				}
				res.Вопросы.push(вопрос);
			}
			return res;
		});

		var vote= null;
		if (items.length>0)
		{
			vote = items[0];
			h_dm_meeting.Load_ЖурналДокументы(vote);
		}

		var text = (0 >= items.length) ? null : JSON.stringify(vote);
		cx.completeCallback(200, 'success', { text: text });
	}

	return service;
});