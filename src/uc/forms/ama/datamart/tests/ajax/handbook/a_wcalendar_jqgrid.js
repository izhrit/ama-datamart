define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
]
, function (ajax_jqGrid, db, ql)
{
	var service = ajax_jqGrid('ama/datamart?action=wcalendar.jqgrid');

	service.rows_all = function ()
	{
		var rows= ql.select(function (r) { return {
			id_Wcalendar:r.wcl.id_Wcalendar
			,Year:r.wcl.Year
			,Region:r.rg.Name
		};})
		.from(db.wcalendar,'wcl')
		.inner_join(db.region, 'rg', function (r) { return r.rg.id_Region==r.wcl.id_Region; })
		.exec();
		return rows;
	}

	return service;
}
);