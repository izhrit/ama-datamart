﻿define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
]
, function (ajax_jqGrid, db, ql)
{
	var service = ajax_jqGrid('ama/datamart?action=pefrsb.jqgrid');

	service.rows_all = function ()
	{
		var rows= ql.select(function (r) { return {
			id_pefrsb:r.p.id_pefrsb
			,Физлицо:r.p.price_for_physical_entity
			,Физлицо_с_комиссией:r.p.price_for_physical_entity_with_commission
			,Дата:r.p.date_of_set_price
			,Юрлицо:r.p.price_for_legal_entity
			,Юрлицо_с_комиссией:r.p.price_for_legal_entity_with_commission
		};})
		.from(db.price_efrsb,'p')
		.exec();
		return rows;
	}

	return service;
}
);