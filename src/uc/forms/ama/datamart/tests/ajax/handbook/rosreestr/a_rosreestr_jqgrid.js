﻿define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
]
, function (ajax_jqGrid, db, ql)
{
	var service = ajax_jqGrid('ama/datamart?action=rosreestr.jqgrid');

	service.rows_all = function ()
	{
		var rows= ql.select(function (r) { return {
			id_Rosreestr:r.r.id_Rosreestr
			,Name:r.r.Name
		};})
		.from(db.Rosreestr,'r')
		.exec();
		return rows;
	}

	return service;
}
);