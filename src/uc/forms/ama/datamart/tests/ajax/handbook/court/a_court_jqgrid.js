﻿define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
]
, function (ajax_jqGrid, db, ql)
{
	var service = ajax_jqGrid('ama/datamart?action=court.jqgrid');

	service.rows_all = function ()
	{
		var rows= ql.select(function (r) { return {
			id_Court:r.c.id_Court
			,Name:r.c.Name
		};})
		.from(db.Court,'c')
		.exec();
		return rows;
	}

	return service;
}
);