define([
	  'forms/base/ql'
	, 'forms/ama/datamart/tests/d_datamart'
]
, function (ql, db)
{
	var transport = {}

	transport.options= {
		url_prefix : 'ama/datamart?action=wcalendar.list'
	}

	transport.get_list = function ()
	{
		var years = ql.from(db.Wcalendar, 'wcl')
			.map(function (r) {
				return {
					Year: r.wcl.Year
				};
			});
		return years;
	}

	transport.prepare_send_abort = function (options, originalOptions, jqXHR)
	{
		var self = this;
		var send_abort=
		{
			send: function (headers, completeCallback)
			{
				var res = self.get_list();
				completeCallback(200, 'success', { text: JSON.stringify(res) });
			}
			, abort: function ()
			{
			}
		}
		return send_abort;
	};

	transport.prepare_try_to_prepare_send_abort = function ()
	{
		var self = this;
		return function (options, originalOptions, jqXHR)
		{
			if (0 == options.url.indexOf(self.options.url_prefix))
			{
				return self.prepare_send_abort(options, originalOptions, jqXHR);
			}
		}
	}

	return transport.prepare_try_to_prepare_send_abort();
});
