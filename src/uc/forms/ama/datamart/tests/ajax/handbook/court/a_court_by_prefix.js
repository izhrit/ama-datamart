﻿define
([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-service-base'
	, 'forms/base/ql'
	, 'forms/base/codec/codec.copy'
]
, function (db, ajax_service_base, ql, codec_copy)
{
	var service= ajax_service_base('ama/datamart?action=court.by-prefix');

	service.action= function (cx)
	{
		var args= cx.url_args();
		var number= args.number;

		var rows= ql.select(function (r) {
			return {
				id: r.c.id_Court
				,text: r.c.Name
			};
		})
		.from(db.Court,'c')
		.where(function (r) { return 0==number.indexOf(r.c.NumberPrefix);})
		.exec();
		var res= rows[0];

		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});
