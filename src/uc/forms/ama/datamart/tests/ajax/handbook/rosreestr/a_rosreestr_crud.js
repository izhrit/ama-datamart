define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/datamart?action=rosreestr.crud');

	var copy_to_db_row = function (rosreestr, rosreestr_row)
	{
		rosreestr_row.Name = rosreestr.Name;
		rosreestr_row.Address = rosreestr.Address;
		rosreestr_row.Latitude = rosreestr.Latitude;
		rosreestr_row.Longitude = rosreestr.Longitude;
		
		
		var region = ql.find_first_or_null(db.region, function (r) { return r.OKATO == rosreestr.Region; });
		rosreestr_row.id_Region = region.id_Region;
		
	}

	service.create = function (rosreestr)
	{
		var rosreestr_row= {};
		copy_to_db_row(rosreestr, rosreestr_row);
		ql.insert_with_next_id(db.Rosreestr, 'id_Rosreestr', rosreestr_row);
	}

	service.update = function (id_Rosreestr, rosreestr)
	{
		var rosreestr_row = ql.find_first_or_null(db.Rosreestr, function (r) { return r.id_Rosreestr == id_Rosreestr; });
		copy_to_db_row(rosreestr, rosreestr_row);
	}

	service.read= function(id_Rosreestr)
	{
		var rows = ql.select(function (r) { return {
			id_Rosreestr: r.r.id_Rosreestr
			, Region: r.rg.OKATO
			, Name: r.r.Name
			, Address: r.r.Address
			, Latitude: r.r.Latitude
			, Longitude: r.r.Longitude
		};})
		.from(db.Rosreestr, 'r')
		.inner_join(db.region, 'rg', function (r) { return r.r.id_Region == r.rg.id_Region; })
		.where(function(r) { return r.r.id_Rosreestr==id_Rosreestr; })
		.exec();
		return rows[0];
	}

	service._delete= function(ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id_Rosreestr = id_arr[i];
			db.Rosreestr = ql._delete(db.Rosreestr, function (r) { return id_Rosreestr == r.id_Rosreestr; });
		}
	}

	return service;
});