﻿define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
	, 'forms/base/codec/datetime/s_codec.mysql_txt2txt_ru_legal'
]
, function (ajax_jqGrid, db, ql, s_codec_mysql_txt2txt_ru_legal)
{
	var service = ajax_jqGrid('ama/datamart?action=sl.jqgrid');

	service.rows_all = function ()
	{
		var rows= ql.select(function (r) { return {
			id_Sub_level:r.sl.id_Sub_level

			,Регион:r.rg.Name
			,Дата:s_codec_mysql_txt2txt_ru_legal.Encode(r.sl.StartDate)

			,Общий:r.sl.Sum_Common
			,Трудоспособных:r.sl.Sum_Employable
			,Пенсионеров:r.sl.Sum_Pensioner
			,Несовершеннолетних:r.sl.Sum_Infant

			,Публичность:(!r.sl.id_Contract || null==r.sl.id_Contract)?1:0
		};})
		.from(db.sub_level,'sl')
		.inner_join(db.region, 'rg', function (r) { return r.sl.id_Region==r.rg.id_Region; })
		.exec();
		return rows;
	}

	return service;
}
);