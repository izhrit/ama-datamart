﻿define
([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-select2'
	, 'forms/base/ql'
]
, function (db, ajax_select2, ql)
{
	var service= ajax_select2('ama/datamart?action=court.select2');

	service.query = function (q)
	{
		var rows=  ql.select(function (r) { return {
			id: r.c.id_Court
			,text: r.c.Name
		};})
		.from(db.Court, 'c')
		.where(function (r) { return -1!=r.c.Name.indexOf(q);})
		.exec();
		return { results: rows };
	}

	return service;
});
