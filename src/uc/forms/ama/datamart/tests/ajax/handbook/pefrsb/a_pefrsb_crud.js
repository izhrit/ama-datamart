﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/datamart?action=pefrsb.crud');

	var copy_to_db_row = function (pefrsb, pefrsb_row)
	{
		pefrsb_row.price_for_physical_entity = pefrsb.price_for_physical_entity;
		pefrsb_row.date_of_set_price = pefrsb.date_of_set_price;
		pefrsb_row.price_for_legal_entity = pefrsb.price_for_legal_entity;
	}

	service.create = function (pefrsb)
	{
		var pefrsb_row= {};
		copy_to_db_row(pefrsb, pefrsb_row);
		ql.insert_with_next_id(db.price_efrsb, 'id_pefrsb', pefrsb_row);
	}

	service.update = function (id_pefrsb, pefrsb)
	{
		var pefrsb_row = ql.find_first_or_null(db.price_efrsb, function (r) { return r.id_pefrsb == id_pefrsb; });
		copy_to_db_row(pefrsb, pefrsb_row);
	}

	service.read= function(id_pefrsb)
	{
		var rows = ql.select(function (r) {
			return {
				id_pefrsb: r.pe.id_pefrsb
				, Сумма_за_сообщение: {
                    Физлицо: {
						  Задан: r.pe.price_for_physical_entity ? true : false
						, Значение: r.pe.price_for_physical_entity
                    }
					, Юрлицо: {
						Задан: r.pe.price_for_legal_entity ? true : false
						, Значение: r.pe.price_for_legal_entity
					}
				}
				, Дата: r.pe.date_of_set_price
			};
		})
		.from(db.price_efrsb,'pe')
		.where(function(r) { return r.pe.id_pefrsb==id_pefrsb; })
		.exec();
		return rows[0];
	}

	service._delete= function(ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id_pefrsb= id_arr[i];
			db.price_efrsb = ql._delete(db.price_efrsb, function (c) { return id_pefrsb == c.id_pefrsb; });
		}
	}

	return service;
});