﻿define([
	'forms/base/ajax/ajax-service-base'
],
function (ajax_service_base)
{
	var service = ajax_service_base('ama/datamart?action=api-GetSubLevelInformation');

	service.action= function (cx)
	{
		var region = cx.url_args().region;
		var text = '';
		switch (region) {
			case '94 000':
				text = {
					Common: '10010', Infant: '10030', Employable: '10020', Pensioner: '10040'
					, Reglaments: [{
						title: 'Приказ №1 по округу', date: '2019-11-01', url: 'http//google.com'
					}]
				};
				break;
			default:
				text = {
					Common: null, Infant: null, Employable: null, Pensioner: null
					, Reglaments: []
				};
				break;

		}
		cx.completeCallback(200, 'success', { text: JSON.stringify(text) });
	}

	return service;
});