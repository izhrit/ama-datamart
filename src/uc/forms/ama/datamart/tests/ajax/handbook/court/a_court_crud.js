﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/datamart?action=court.crud');

	var copy_to_db_row= function (court, court_row)
	{
		court_row.Name= court.Name;
		court_row.ShortName= court.ShortName;
		court_row.SearchName= court.SearchName;
		court_row.CasebookTag= court.CasebookTag;
		court_row.Address= court.Address;
		court_row.NumberPrefix= court.NumberPrefix;
	}

	service.create= function(court)
	{
		var court_row= {};
		copy_to_db_row(court, court_row);
		ql.insert_with_next_id(db.Court, 'id_Court', court_row);
	}

	service.update = function (id_Court, court)
	{
		var court_row= ql.find_first_or_null(db.Court, function (r) { return r.id_Court== id_Court; });
		copy_to_db_row(court, court_row);
	}

	service.read= function(id_Court)
	{
		var rows = ql.select(function (r) { return {
			id_Court: r.c.id_Court
			, Name: r.c.Name
			, CasebookTag: r.c.CasebookTag
			, ShortName: r.c.ShortName
			, SearchName: r.c.SearchName
			, Address: r.c.Address
			, NumberPrefix: r.c.NumberPrefix
		};})
		.from(db.Court,'c')
		.where(function(r) { return r.c.id_Court==id_Court; })
		.exec();
		return rows[0];
	}

	service._delete= function(ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id_Court= id_arr[i];
			db.Court= ql._delete(db.Court, function (c) { return id_Court==c.id_Court; });
		}
	}

	return service;
});