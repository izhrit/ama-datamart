﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/base/codec/datetime/s_codec.mysql_txt2txt_ru_legal'
]
, function (db, ajax_CRUD, ql, s_codec_mysql_txt2txt_ru_legal)
{
	var service = ajax_CRUD('ama/datamart?action=sl.crud');

	var copy_to_db_record = function (sl_record, sl)
	{
		sl.StartDate= s_codec_mysql_txt2txt_ru_legal.Decode(sl_record.Дата);

		var v= sl_record.Прожиточный_минимум;
		sl.Sum_Common= !v.Подушевой.Задан ? null : v.Подушевой.Значение;
		sl.Sum_Employable= !v.Трудоспособных.Задан ? null : v.Трудоспособных.Значение;
		sl.Sum_Pensioner= !v.Пенсионеров.Задан ? null : v.Пенсионеров.Значение;
		sl.Sum_Infant= !v.Несовершеннолетних.Задан ? null : v.Несовершеннолетних.Значение;

		var ract= sl_record.Регламентирующий_акт;
		sl.Reglament_Title= ract.Название;
		sl.Reglament_Date= s_codec_mysql_txt2txt_ru_legal.Decode(ract.Дата);
		sl.Reglament_Url= ract.Ссылка;

		var region = ql.find_first_or_null(db.region, function (r) { return r.OKATO==sl_record.Регион; });
		sl.id_Region= region.id_Region;
	}

	service.create= function(sl_record)
	{
		var sl = {id_Region:1};
		copy_to_db_record(sl_record, sl);
		sl.id_Contract= sl_record.id_Contract;
		ql.insert_with_next_id(db.sub_level, 'id_Sub_level', sl);
	}

	service.update = function (id_Sub_level, sl_record)
	{
		var sl = ql.find_first_or_null(db.sub_level, function (r) { return r.id_Sub_level == id_Sub_level; });
		copy_to_db_record(sl_record, sl);
	}

	service.read= function(id_Sub_level)
	{
		var rows = ql.select(function (r) { return {
			Дата: s_codec_mysql_txt2txt_ru_legal.Encode(r.sl.StartDate)
			,Регион: r.rg.OKATO
			, Прожиточный_минимум: {
				Подушевой: { Задан: (r.sl.Sum_Common && null!=r.sl.Sum_Common), Значение:r.sl.Sum_Common }
				,Трудоспособных: { Задан: (r.sl.Sum_Employable && null!=r.sl.Sum_Employable), Значение:r.sl.Sum_Employable }
				,Пенсионеров: { Задан: (r.sl.Sum_Pensioner && null!=r.sl.Sum_Pensioner), Значение:r.sl.Sum_Pensioner }
				,Несовершеннолетних: { Задан: (r.sl.Sum_Infant && null!=r.sl.Sum_Infant), Значение:r.sl.Sum_Infant }
			}
			, Регламентирующий_акт: {
				Название: r.sl.Reglament_Title
				,Дата: s_codec_mysql_txt2txt_ru_legal.Encode(r.sl.Reglament_Date)
				,Ссылка: r.sl.Reglament_Url
			}
			,ContractNumber: !r.c || null==r.c ? null : r.c.ContractNumber
			,id_Sub_level:r.sl.id_Sub_level
		};})
		.from(db.sub_level,'sl')
		.inner_join(db.region, 'rg', function (r) { return r.sl.id_Region==r.rg.id_Region; })
		.left_join(db.Contract, 'c', function (r) {return r.sl.id_Contract==r.c.id_Contract; })
		.where(function (r) { return r.sl.id_Sub_level == id_Sub_level; })
		.exec();

		return rows[0];
	}

	service._delete= function(ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id_Sub_level = id_arr[i];
			db.sub_level = ql._delete(db.sub_level, function (sl) { return id_Sub_level == sl.id_Sub_level; });
		}
	}

	return service;
});