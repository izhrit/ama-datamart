﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/datamart?action=wcalendar.crud');

	service.create = function (wcl_record)
	{
		ql.insert_with_next_id(db.wcalendar, 'id_Wcalendar', wcl_record);
	}

	service.update = function (year, wcl_record)
	{
		var Wcl = ql.find_first_or_null(db.wcalendar, function (r) { return r.Year == year; });
		if (null != Wcl) {
			Wcl.Days = wcl_record;
		}
	}

	service.read= function(id)
	{
		var res = ql.from(db.wcalendar, 'wcl')
		.where(function (r) { return r.wcl.id_Wcalendar == id; })
		.map(function (r) { return { Days: JSON.stringify(r.wcl.Days) }; })
		[0];
		return res;
	}

	service.delete = function (year)
	{
		db.wcalendar = ql.delete(db.wcalendar, function (r) { return r.Year == year; });
	}

	return service;
});