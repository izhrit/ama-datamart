﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'txt!forms/ama/datamart/common/cust_query/result/tests/contents/example1.json.txt'
],
function (ajax_service_base, txt_result)
{
	var service= ajax_service_base('ama/datamart?action=custom_query.execute');

	service.action= function (cx)
	{
		cx.completeCallback(200, 'success', { text: txt_result });
	}

	return service;
});