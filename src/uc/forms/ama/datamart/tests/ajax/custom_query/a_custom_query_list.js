﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'txt!forms/ama/datamart/common/cust_query/list/tests/contents/example2.json.txt'
],
function (ajax_service_base, txt_list)
{
	var service= ajax_service_base('ama/datamart?action=custom_query.list');

	var find_folder_by_name = function (folders, foldername)
	{
		for (var i = 0; i < folders.length; i++)
		{
			var folder= folders[i];
			if (foldername==folder.foldername)
				return folder;
		}
		return null;
	}

	service.action= function (cx)
	{
		var args= cx.url_args();
		var path= args.path;

		if ('/' == path)
		{
			cx.completeCallback(200, 'success', { text: txt_list });
		}
		else
		{
			var parts= path.split('/')
			var cur_folder= JSON.parse(txt_list);
			for (var i = 0; i < parts.length; i++)
			{
				var fname= parts[i];
				if ('' != fname)
					cur_folder = find_folder_by_name(cur_folder.folders, fname);
			}
			cx.completeCallback(200, 'success', { text: JSON.stringify(cur_folder.content, null, '\t') });
		}
	}

	return service;
});