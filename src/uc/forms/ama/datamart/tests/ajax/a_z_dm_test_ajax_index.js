define
(
	[
		  'forms/base/ajax/ajax-collector'

		, 'forms/ama/datamart/tests/ajax/viewer/a_dm_viewer_jqgrid'
		, 'forms/ama/datamart/tests/ajax/viewer/a_dm_viewer_crud'
		, 'forms/ama/datamart/tests/ajax/viewer/a_dm_viewer_register'
		, 'forms/ama/datamart/tests/ajax/viewer/a_dm_viewer_select2'
		, 'forms/ama/datamart/tests/ajax/viewer/a_dm_viewer_outcome_jqgrid'

		, 'forms/ama/datamart/tests/ajax/start/a_dm_start_jqgrid'
		, 'forms/ama/datamart/tests/ajax/start/a_dm_start_crud'
		, 'forms/ama/datamart/tests/ajax/start/a_dm_start_check'
		, 'forms/ama/datamart/tests/ajax/start/a_dm_start_agree'
		, 'forms/ama/datamart/tests/ajax/start/a_dm_start_new'

		, 'forms/ama/datamart/tests/ajax/consent/a_dm_consent_jqgrid'
		, 'forms/ama/datamart/tests/ajax/consent/a_dm_consent_crud'
		, 'forms/ama/datamart/tests/ajax/consent/a_dm_consent_check'

		, 'forms/ama/datamart/tests/ajax/mrequest/a_dm_mrequest_jqgrid'
		, 'forms/ama/datamart/tests/ajax/mrequest/a_dm_mrequest_crud'
		, 'forms/ama/datamart/tests/ajax/mrequest/a_dm_consent_select2'
		, 'forms/ama/datamart/tests/ajax/mrequest/a_dm_mrequest_typeahead'
		, 'forms/ama/datamart/tests/ajax/mrequest/a_dm_manager_select2'

		, 'forms/ama/datamart/tests/ajax/debtor/a_dm_debtor_select2'
		, 'forms/ama/datamart/tests/ajax/debtor/a_dm_debtor_params'
		, 'forms/ama/datamart/tests/ajax/debtor/a_dm_debtor_address_typeahead'
		, 'forms/ama/datamart/tests/ajax/debtor/a_dm_debtor_region_typeahead'
		, 'forms/ama/datamart/tests/ajax/debtor/a_dm_debtor_jqgrid'
		, 'forms/ama/datamart/tests/ajax/debtor/a_dm_debtor_crud'
		, 'forms/ama/datamart/tests/ajax/debtor/docs/a_dm_debtor_docs_jqgrid'
		, 'forms/ama/datamart/tests/ajax/debtor/docs/a_dm_debtor_docs_crud'
		, 'forms/ama/datamart/tests/ajax/debtor/docs/a_dm_debtor_docs_page_crud'
		, 'forms/ama/datamart/tests/ajax/debtor/docs/a_dm_debtor_docs_doc'
		, 'forms/ama/datamart/tests/ajax/debtor/docs/a_dm_debtor_docs_status_crud'
		, 'forms/ama/datamart/tests/ajax/debtor/docs/a_dm_debtor_docs_status'
		, 'forms/ama/datamart/tests/ajax/debtor/docs/a_dm_debtor_docs_select2'
		, 'forms/ama/datamart/tests/ajax/debtor/docs/a_dm_debtor_docs_default_doc'
		, 'forms/ama/datamart/tests/ajax/debtor/cabinet/a_dm_debtor_docs'
		, 'forms/ama/datamart/tests/ajax/debtor/cabinet/a_dm_debtor_info'
		, 'forms/ama/datamart/tests/ajax/debtor/cabinet/a_dm_debtor_escort_info'
		, 'forms/ama/datamart/tests/ajax/debtor/cabinet/a_dm_debtor_company_info'

		, 'forms/ama/datamart/tests/ajax/procedure/a_dm_procedure_jqgrid'
		, 'forms/ama/datamart/tests/ajax/procedure/a_dm_procedure_select2'

		, 'forms/ama/datamart/tests/ajax/procedure/a_dm_proc_viewers_ru'
		, 'forms/ama/datamart/tests/ajax/procedure/a_dm_procedure_ru'
		, 'forms/ama/datamart/tests/ajax/procedure/a_dm_procedure_info'
		, 'forms/ama/datamart/tests/ajax/procedure/a_dm_procedure_dbinfo'
		, 'forms/ama/datamart/tests/ajax/procedure/a_dm_procedure_msg_prep'
		, 'forms/ama/datamart/tests/ajax/procedure/a_dm_procedure_params_preview'

		, 'forms/ama/datamart/tests/ajax/request/a_dm_request_crud'
		, 'forms/ama/datamart/tests/ajax/request/a_dm_request_jqgrid'
		, 'forms/ama/datamart/tests/ajax/request/a_dm_request_resolve'
		, 'forms/ama/datamart/tests/ajax/request/a_dm_request_fields'

		, 'forms/ama/datamart/tests/ajax/meeting/a_dm_meeting_fill_new'
		, 'forms/ama/datamart/tests/ajax/meeting/a_dm_meeting_crud'
		, 'forms/ama/datamart/tests/ajax/meeting/a_dm_meeting_nearest'
		, 'forms/ama/datamart/tests/ajax/meeting/a_dm_meeting_select2'
		, 'forms/ama/datamart/tests/ajax/meeting/a_dm_meeting_vote'
		, 'forms/ama/datamart/tests/ajax/meeting/a_dm_meeting_state'
		, 'forms/ama/datamart/tests/ajax/meeting/a_dm_meeting_cabinetc'
		, 'forms/ama/datamart/tests/ajax/meeting/a_dm_meeting_document'

		, 'forms/ama/datamart/tests/ajax/dm/a_dm_login'
		, 'forms/ama/datamart/tests/ajax/dm/a_dm_logout'
		, 'forms/ama/datamart/tests/ajax/dm/a_dm_db'
		, 'forms/ama/datamart/tests/ajax/dm/a_dm_job_monitor'
		, 'forms/ama/datamart/tests/ajax/dm/a_dm_debtor_login'

		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_jqgrid'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_select2'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_efrsb_manager_select2'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_income_jqgrid'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_crud'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_password'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_cert_login'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_sro_crud'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_document_crud'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_notifications_jqgrid'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_notifications_info'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_info_crud'
		, 'forms/ama/datamart/tests/ajax/manager/a_dm_manager_contract'
		

		, 'forms/ama/datamart/tests/ajax/efrsb/a_dm_efrsb_jqgrid'
		, 'forms/ama/datamart/tests/ajax/efrsb/a_dm_efrsb_debtor_select2'

		, 'forms/ama/datamart/tests/ajax/leaks/a_dm_leaks_jqgrid'

		, 'forms/ama/datamart/tests/ajax/award/a_award_nominees'
		, 'forms/ama/datamart/tests/ajax/award/a_award_nominee'
		, 'forms/ama/datamart/tests/ajax/award/a_award_votes'
		, 'forms/ama/datamart/tests/ajax/award/a_award_vote'
		, 'forms/ama/datamart/tests/ajax/award/a_award_votes_jqgrid'
		, 'forms/ama/datamart/tests/ajax/award/a_award_manager_select2'
		, 'forms/ama/datamart/tests/ajax/award/a_award_manager'
		, 'forms/ama/datamart/tests/ajax/award/a_award_results'
		, 'forms/ama/datamart/tests/ajax/award/a_award_login'

		, 'forms/ama/datamart/tests/ajax/handbook/a_request'
		, 'forms/ama/datamart/tests/ajax/handbook/a_sl_crud'
		, 'forms/ama/datamart/tests/ajax/handbook/a_sl_jqgrid'
		, 'forms/ama/datamart/tests/ajax/handbook/a_wcalendar_crud'
		, 'forms/ama/datamart/tests/ajax/handbook/a_wcalendar_list'
		, 'forms/ama/datamart/tests/ajax/handbook/a_wcalendar_jqgrid'

		, 'forms/ama/datamart/tests/ajax/handbook/court/a_court_jqgrid'
		, 'forms/ama/datamart/tests/ajax/handbook/court/a_court_crud'
		, 'forms/ama/datamart/tests/ajax/handbook/court/a_court_select2'
		, 'forms/ama/datamart/tests/ajax/handbook/court/a_court_by_prefix'

		, 'forms/ama/datamart/tests/ajax/handbook/rosreestr/a_rosreestr_jqgrid'
		, 'forms/ama/datamart/tests/ajax/handbook/rosreestr/a_rosreestr_crud'

		, 'forms/ama/datamart/tests/ajax/events_news/a_dm_news_jqgrid'
		, 'forms/ama/datamart/tests/ajax/events_news/a_dm_schedule_info'
		, 'forms/ama/datamart/tests/ajax/events_news/a_dm_push_jqgrid'
		, 'forms/ama/datamart/tests/ajax/events_news/a_dm_push_ru'
		, 'forms/ama/datamart/tests/ajax/events_news/a_dm_push_get'
		, 'forms/ama/datamart/tests/ajax/events_news/a_dm_push_test'
		, 'forms/ama/datamart/tests/ajax/events_news/a_dm_push_unsubscribe'

		, 'forms/ama/datamart/tests/ajax/custom_query/a_custom_query_list'
		, 'forms/ama/datamart/tests/ajax/custom_query/a_custom_query_execute'

		, 'forms/ama/datamart/tests/ajax/application/a_application_debtor'
		, 'forms/ama/datamart/tests/ajax/application/a_application_creditor_select2'

		, 'forms/ama/datamart/tests/ajax/sro/a_dm_sro_props_crud'
		, 'forms/ama/datamart/tests/ajax/sro/a_dm_sro_document_crud'

		, 'forms/ama/datamart/tests/ajax/email/a_email_jqgrid'
		, 'forms/ama/datamart/tests/ajax/email/a_email_get'

		, 'forms/ama/datamart/tests/ajax/access_log/a_access_log_jqgrid'
		, 'forms/ama/datamart/tests/ajax/access_log/a_access_log_get'
		
		, 'forms/ama/datamart/tests/ajax/access_log/a_access_log_explain'
		
		, 'forms/ama/datamart/tests/ajax/google_calendar/a_dm_google_calendar_control'

		, 'forms/ama/datamart/tests/ajax/biddings/a_ext_km_select2'
		, 'forms/ama/datamart/tests/ajax/biddings/a_ext_etp_select2'

		, 'forms/ama/datamart/tests/ajax/last_result/a_dm_last_result_jqgrid'
	],
	function (ajax_collector)
	{
		return ajax_collector(Array.prototype.slice.call(arguments, 1));
	}
);