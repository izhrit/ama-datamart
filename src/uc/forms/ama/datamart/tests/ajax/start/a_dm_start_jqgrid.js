﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
], function (db, ajax_jqGrid, ql)
{
	var service= ajax_jqGrid('ama/datamart?action=start.jqgrid');
	service.rows_all = function (url,args)
	{
		var rows= ql.select(function (r) { return {
				id_ProcedureStart : r.s.id_ProcedureStart
				,debtorName: r.s.DebtorName
				,debtorName2: r.s.DebtorName2
				,debtorINN: r.s.DebtorINN
				,debtorOGRN: r.s.DebtorOGRN
				,debtorSNILS: r.s.DebtorSNILS
				,DebtorCategory: r.s.DebtorCategory
				,caseNumber: r.s.CaseNumber
				,DateOfApplication: r.s.DateOfApplication
				,TimeOfLastChecking: r.s.TimeOfLastChecking
				,NextSessionDate: r.s.NextSessionDate
				,Court: r.c.ShortName
				,Manager: (!r.m || null==r.m) ? '' : (r.m.lastName + ' ' + r.m.firstName.charAt(0) + '.' + r.m.middleName.charAt(0) + '.')
				,ReadyToRegistrate: r.s.ReadyToRegistrate
				,id_MRequest: r.s.id_MRequest
				,addedBySRO: r.s.AddedBySRO
			};})
			.from(db.ProcedureStart, 's')
			.inner_join(db.Court, 'c', function (r) { return r.s.id_Court==r.c.id_Court; })
			.left_join(db.Manager, 'm', function (r) { return r.m.id_Manager==r.s.id_Manager; })
			.where(function (r) 
				{
					return (r.s.id_Contract==args.id_Contract || args.id_Contract=='undefined' || r.s.AddedBySRO==1)
						&& (!args.id_Manager || r.m && null!=r.m && (args.id_Manager==r.s.id_Manager));
				}
			)
			.exec();
		return rows;
	}

	return service;
});