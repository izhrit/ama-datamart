define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/ajax/start/h_a_dm_start'
	, 'forms/base/codec/datetime/h_codec.datetime'
]
, function (db, ajax_CRUD, ql, h_dm_start, h_codec_datetime)
{
	var service = ajax_CRUD('ama/datamart?action=start.crud');

	var getDebtorObject = function (r, num) {
		var DebtorCategory = 'DebtorCategory' + (num || '');
		var DebtorName = 'DebtorName' + (num || '');
		var DebtorSNILS = 'DebtorSNILS' + (num || '');
		var DebtorINN = 'DebtorINN' + (num || '');
		var DebtorOGRN = 'DebtorOGRN' + (num || '');
		var DebtorAddress = 'DebtorAddress' + (num || '');

		var Должник = { ИНН: r.ps[DebtorINN] || '', ОГРН: r.ps[DebtorOGRN] || '', Адрес: r.ps[DebtorAddress] || '' };
		switch (r.ps[DebtorCategory])
		{
			case 'l':
				Должник.Тип= "Юр_лицо";
				Должник.Юр_лицо= { Наименование: r.ps[DebtorName] };
				break;
			case 'n':
				var p= r.ps[DebtorName].split(' ');
				Должник.Тип= "Физ_лицо";
				Должник.Физ_лицо= { Фамилия: p[0], Имя: p[1], Отчество: p[2], СНИЛС: r.ps[DebtorSNILS] || '' };
				break;
		}

		return Должник;
	}

	service.create= function(start,args)
	{
		var row_fields= h_dm_start.fill_row_by_start(start);
		row_fields.id_Contract= args.id_Contract || null;
		var start = ql.insert_with_next_id(db.ProcedureStart, 'id_ProcedureStart', row_fields);

		return {
			data: {
				id: start.id_ProcedureStart,
			}
		}
	};

	service.update = function (id_ProcedureStart, start)
	{
		var row_ProcedureStart = ql.find_first_or_null(db.ProcedureStart, function (ps) { return id_ProcedureStart==ps.id_ProcedureStart; });
		h_dm_start.fill_row_by_start(start, row_ProcedureStart);
	};

	service.read= function(id_ProcedureStart)
	{
		var rows= ql.select(function(r)
			{
				var Заявитель= { ИНН: r.ps.ApplicantINN, ОГРН: r.ps.ApplicantOGRN, Адрес: r.ps.ApplicantAddress  };
				var start= {
					id_ProcedureStart: r.ps.id_ProcedureStart
					, Заявитель: Заявитель
					, Заявление_на_банкротство: {
						Подано: (r.ps.DateOfApplication && null!=r.ps.DateOfApplication && ''!=r.ps.DateOfApplication)
						, Дата_подачи: !r.ps.DateOfApplication || null==r.ps.DateOfApplication || ''==r.ps.DateOfApplication ? ''
							: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.DateOfApplication)
						, В_суд: ((null == r.c) ? null : { id: r.c.id_Court, text:r.c.Name })
						, Дата_принятия: !r.ps.DateOfAcceptance || null==r.ps.DateOfAcceptance || ''==r.ps.DateOfAcceptance ? ''
						: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.DateOfAcceptance)
						, Дата_рассмотрения: !r.ps.NextSessionDate || null==r.ps.NextSessionDate || ''==r.ps.NextSessionDate ? ''
							: h_codec_datetime.mysql_txt2txt_ru_legal().Encode(r.ps.NextSessionDate)
					}
					,Номер_судебного_дела:r.ps.CaseNumber
					,Запрос: {
						Ссылка: r.ps.CourtDecisionURL || ''
						, Время: {
							акта: !r.ps.DateOfRequestAct || null == r.ps.DateOfRequestAct || '' == r.ps.DateOfRequestAct ? ''
							: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.DateOfRequestAct)
						}
						, Дополнительная_информация: r.ps.CourtDecisionAddInfo || ''
					}
					,Время_сверки:r.ps.TimeOfLastChecking
					,Показывать_для_регистрации_в_ПАУ:(0!=r.ps.ReadyToRegistrate)
					,Показывать_СРО:(1==r.ps.ShowToSRO)
					,Назначение: {
						Ссылка: r.ps.PrescriptionURL || ''
						, Время: {
							акта: !r.ps.DateOfPrescription || null==r.ps.DateOfPrescription || ''==r.ps.DateOfPrescription ? ''
							: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.DateOfPrescription)
						}
						, Дополнительная_информация: r.ps.PrescriptionAddInfo || ''
					}
					,ЕФРСБ: {
						Номер:r.ps.efrsbPrescriptionNumber || ''
						,Дата_публикации:!r.ps.efrsbPrescriptionPublishDate || null==r.ps.efrsbPrescriptionPublishDate || ''==r.ps.efrsbPrescriptionPublishDate ? '' : h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.efrsbPrescriptionPublishDate)
						,ID:r.ps.efrsbPrescriptionID || ''
						,Дополнительная_информация:r.ps.efrsbPrescriptionAddInfo || ''
					}
					,isChoosedInRequest: (r.mr && r.mr.id_Manager==r.ps.id_Manager && r.mr.DateOfResponce) || false
				};

				if(r.ps.DebtorName2) {
					start.Должник = { Тип: 'Супруги', Супруги: { Должник1: getDebtorObject(r), Должник2: getDebtorObject(r, 2) } };
				} else {
					start.Должник = getDebtorObject(r);
				}

				switch (r.ps.ApplicantCategory)
				{
					case 'l':
						Заявитель.Тип= "Юр_лицо";
						Заявитель.Юр_лицо= { Наименование: r.ps.ApplicantName };
						break;
					case 'n':
						var p= r.ps.ApplicantName.split(' ');
						Заявитель.Тип= "Физ_лицо";
						Заявитель.Физ_лицо= { Фамилия: p[0], Имя: p[1], Отчество: p[2], СНИЛС: r.ps.ApplicantSNILS };
						break;
				}
				if(!r.ps.ApplicantName) {
					start.Заявитель = {Заявитель_должник: true}
				}
				if (!r.m || null == r.m)
				{
					start.АУ = null;
				}
				else
				{
					start.АУ = {
						id: r.m.id_Manager
						,text: (r.m.lastName + ' ' + r.m.firstName.charAt(0) + '.' + r.m.middleName.charAt(0) + '.')
					}
				}

				return start;
			})
			.from(db.ProcedureStart, 'ps')
			.inner_join(db.Court, 'c', function (r) { return r.ps.id_Court==r.c.id_Court; })
			.left_join(db.Manager, 'm', function (r) { return r.m.id_Manager==r.ps.id_Manager; })
			.left_join(db.MRequest, 'mr', function(r) { return r.ps.id_MRequest==r.mr.id_MRequest; })
			.where(function (r) { return id_ProcedureStart == r.ps.id_ProcedureStart; })
			.exec();
		
		var start= rows[0];
		return start;
	};

	service._delete= function(ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id = id_arr[i];
			db.ProcedureStart = ql._delete(db.ProcedureStart, function (r) { return id == r.id_ProcedureStart; });
		}
	};

	return service;
});