define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/base/h_times'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/ama/datamart/tests/ajax/start/h_a_dm_start'
]
, function (db, ajax_CRUD, ql, h_times, h_codec_datetime, h_dm_start)
{
	var service = ajax_CRUD('ama/datamart?action=start.check');

	var actualize = function (start,args)
	{
		var date= h_times.safeDateTime();
		var dt= h_codec_datetime.Date2dt(date);
		var Время_сверки= h_codec_datetime.ru_legal_txt2dt().Decode(dt);

		var start_row= null;
		if (start.id_ProcedureStart)
		{
			start_row = ql.find_first_or_null(db.ProcedureStart, function (ps) { return ps.id_ProcedureStart==start.id_ProcedureStart;});
			h_dm_start.fill_row_by_start(start,start_row);
		}
		else
		{
			start_row= h_dm_start.fill_row_by_start(start);
			start_row.id_Contract= args.id_Contract;
			ql.insert_with_next_id(db.ProcedureStart, 'id_ProcedureStart', start_row);
		}
		start_row.TimeOfLastChecking= Время_сверки;
		return { Время_сверки: Время_сверки, id_ProcedureStart: start_row.id_ProcedureStart };
	}

	var search = function (start,args)
	{
		var date= h_times.safeDateTime();
		var dt= h_codec_datetime.Date2dt(date);
		var Время_сверки= h_codec_datetime.ru_legal_txt2dt().Decode(dt);
		var start_row= null;
		if (start.id_ProcedureStart)
		{
			start_row = ql.find_first_or_null(db.ProcedureStart, function (ps) { return ps.id_ProcedureStart==start.id_ProcedureStart;});
			h_dm_start.fill_row_by_start(start,start_row);
		}
		else
		{
			start_row= h_dm_start.fill_row_by_start(start);
			start_row.id_Contract= args.id_Contract;
			ql.insert_with_next_id(db.ProcedureStart, 'id_ProcedureStart', start_row);
		}
		start_row.TimeOfLastChecking= Время_сверки;
		if ('Абайдуллин Валерий Кимович' == start_row.DebtorName)
		{
			start_row.CaseNumber= 'Ё34/2021';
			start_row.NextSessionDate= '2021-02-12';
		}
		var res= { 
			Время_сверки: Время_сверки
			,Номер_судебного_дела: start_row.CaseNumber
			,Дата_рассмотрения: start_row.NextSessionDate
			,id_ProcedureStart: start_row.id_ProcedureStart
		};
		return res;
	}

	service.action = function (cx)
	{
		var args= cx.url_args();
		var res = null;
		switch (args.cmd)
		{
			case 'actualize': res = actualize(cx.data_args(),args); break;
			case 'search': res = search(cx.data_args(),args); break;
			default:
				completeCallback(400, 'Bad Request', '');
				return;
		}
		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});