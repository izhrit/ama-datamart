define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/h_times'
]
, function (db, ajax_CRUD, ql, copy, h_codec_datetime, h_times)
{
	var service = ajax_CRUD('ama/datamart?action=start.agree');

	service.action = function (cx)
	{
		var args= cx.url_args();
		var agree= cx.data_args();

		var id_Manager= (agree.id_Manager!=="null") ? agree.id_Manager : args.id_Manager;
		var id_Contract= args.id_Contract || null;
		
		var rows= ql.select(function(r)
			{
				return r.ps.id_ProcedureStart
			})
			.from(db.ProcedureStart, 'ps')
			.where(function (r) { return args.id == r.ps.id_MRequest && id_Contract == r.ps.id_Contract && id_Manager == r.ps.id_Manager; })
			.exec();

		if(0!==rows.length) {
			cx.completeCallback(200, 'success', { text: '{ "ok": false, "message": "Согласие уже подано!" }' });
		}

		var row_MRequest= ql.find_first_or_null(db.MRequest, function (mr) { return mr.id_MRequest==args.id; });

		if(row_MRequest) {
			var row_fields= copy().CopyObject(row_MRequest);
			row_fields.id_Manager= id_Manager;
			row_fields.id_Contract= id_Contract;
			row_fields.ShowToSRO= 1;
			var dnow= h_times.safeDateTime();
			row_fields.TimeOfCreate= h_codec_datetime.Date2ru(dnow);
			ql.insert_with_next_id(db.ProcedureStart, 'id_ProcedureStart', row_fields);
		}
		cx.completeCallback(200, 'success', { text: '{ "ok": true }' });
	}

	return service;
});