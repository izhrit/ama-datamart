define([
	'forms/base/codec/datetime/h_codec.datetime'
],
function (h_codec_datetime)
{
	var helper = {};

	helper.fill_row_by_start = function (start, row_fields)
	{
		var fillDebtorCitizenFields = function(Должник, num) {
			num = !num ? '' : num;
			row_fields['DebtorCategory' + num] = 'n';
			var Физ_лицо = Должник.Физ_лицо;
			row_fields['DebtorName' + num] = !Физ_лицо ? null : Физ_лицо.Фамилия + ' ' + Физ_лицо.Имя + ' ' + Физ_лицо.Отчество;
			row_fields['DebtorSNILS' + num] = !Физ_лицо ? null : Физ_лицо.СНИЛС;
		}
		var fillDebtorCommonFields = function(Должник, num) {
			num = !num ? '' : num;
			row_fields['DebtorINN' + num] = Должник.ИНН;
			row_fields['DebtorOGRN' + num] = Должник.ОГРН;
			row_fields['DebtorAddress' + num] = Должник.Адрес;
		}

		if (!row_fields)
			row_fields = {};

		var Должник= start.Должник;
		switch (Должник.Тип)
		{
			case 'Юр_лицо':
				row_fields.DebtorCategory= 'l';
				row_fields.DebtorName= Должник.Юр_лицо.Наименование;
				fillDebtorCommonFields(Должник);
				fillDebtorCitizenFields({}, 2);
				fillDebtorCommonFields({}, 2);
				break;
			case 'Физ_лицо':
				fillDebtorCitizenFields(Должник);
				fillDebtorCommonFields(Должник);
				fillDebtorCitizenFields({}, 2);
				fillDebtorCommonFields({}, 2);
				break;
			case 'Супруги':
				fillDebtorCitizenFields(Должник.Супруги.Должник1);
				fillDebtorCommonFields(Должник.Супруги.Должник1);

				fillDebtorCitizenFields(Должник.Супруги.Должник2, 2);
				fillDebtorCommonFields(Должник.Супруги.Должник2, 2)
				break;
		}
		if(start.Заявитель) {
			var Заявитель= start.Заявитель
			switch (Заявитель.Тип)
			{
				case 'Юр_лицо':
					row_fields.ApplicantCategory= 'l';
					row_fields.ApplicantName= Заявитель.Юр_лицо.Наименование;
					break;
				case 'Физ_лицо':
					row_fields.ApplicantCategory= 'n';
					var Физ_лицо= Заявитель.Физ_лицо;
					row_fields.ApplicantName= Физ_лицо.Фамилия + ' ' + Физ_лицо.Имя + ' ' + Физ_лицо.Отчество;
					row_fields.ApplicantSNILS= Физ_лицо.СНИЛС;
					break;
			}
			row_fields.ApplicantINN= Заявитель.ИНН;
			row_fields.ApplicantOGRN= Заявитель.ОГРН;
			row_fields.ApplicantAddress= Заявитель.Адрес;
		} else {
			row_fields.ApplicantCategory= null;
			row_fields.ApplicantName= null;
			row_fields.ApplicantSNILS= null;
			row_fields.ApplicantINN= null;
			row_fields.ApplicantOGRN= null;
			row_fields.ApplicantAddress= null;
		}

		row_fields.CaseNumber= start.Номер_судебного_дела;

		var Заявитель= start.Заявитель;

		var заявление= start.Заявление_на_банкротство;
		if (заявление.Подано)
		{
			row_fields.DateOfApplication= !заявление.Дата_подачи || null==заявление.Дата_подачи || ''==заявление.Дата_подачи 
				? null : h_codec_datetime.mysql_txt2txt_ru_date().Decode(заявление.Дата_подачи);
			row_fields.id_Court= (!заявление.В_суд || null==заявление.В_суд) ? null : заявление.В_суд.id;
		}
		row_fields.DateOfAcceptance
		= !заявление.Дата_принятия || null==заявление.Дата_принятия || ''==заявление.Дата_принятия 
		? null : h_codec_datetime.mysql_txt2txt_ru_date().Decode(заявление.Дата_принятия);

		row_fields.id_Manager= (!start.АУ || null==start.АУ) ? null : start.АУ.id;
		row_fields.ReadyToRegistrate= (start.Показывать_для_регистрации_в_ПАУ && 'false'!=start.Показывать_для_регистрации_в_ПАУ)?1:0;
		row_fields.ShowToSRO= (start.Показывать_СРО && 'false'!=start.Показывать_СРО)?1:0;

		row_fields.NextSessionDate= !заявление.Дата_рассмотрения || null==заявление.Дата_рассмотрения || ''==заявление.Дата_рассмотрения 
				? null : h_codec_datetime.mysql_txt2txt_ru_legal().Decode(заявление.Дата_рассмотрения);

		row_fields.id_MRequest= (!start.id_MRequest || null==start.id_MRequest) ? null : start.id_MRequest;

		var назначение= start.Назначение;
		if(назначение.Время) {
			if(назначение.Ссылка) {
				row_fields.PrescriptionURL = назначение.Ссылка;
				row_fields.PrescriptionAddInfo = назначение.Дополнительная_информация;
				row_fields.DateOfPrescription = !назначение.Время.акта || null == назначение.Время.акта || '' == назначение.Время.акта ? ''
				: h_codec_datetime.mysql_txt2txt_ru_date().Decode(назначение.Время.акта)
			}
		}

		var запрос= start.Запрос;
		if(запрос.Время) {
			if(запрос.Ссылка) {
				row_fields.CourtDecisionURL = запрос.Ссылка;
				row_fields.CourtDecisionAddInfo = запрос.Дополнительная_информация;
				row_fields.DateOfRequestAct = !запрос.Время.акта || null == запрос.Время.акта || '' == запрос.Время.акта ? ''
				: h_codec_datetime.mysql_txt2txt_ru_date().Decode(запрос.Время.акта)
			}
		}

		var efrsb= start.ЕФРСБ;
		row_fields.efrsbPrescriptionNumber= !efrsb.Номер || null==efrsb.Номер || ''==efrsb.Номер
		? null : efrsb.Номер;
		row_fields.efrsbPrescriptionPublishDate= !efrsb.Дата_публикации || null==efrsb.Дата_публикации || ''==efrsb.Дата_публикации 
		? null : h_codec_datetime.mysql_txt2txt_ru_legal().Decode(efrsb.Дата_публикации);
		row_fields.efrsbPrescriptionID= !efrsb.ID || null==efrsb.ID || ''==efrsb.ID
		? null : efrsb.ID;
		row_fields.efrsbPrescriptionAddInfo= !efrsb.Дополнительная_информация || null==efrsb.Дополнительная_информация || ''==efrsb.Дополнительная_информация
		? null : efrsb.Дополнительная_информация;

		return row_fields;
	}

	return helper;
});