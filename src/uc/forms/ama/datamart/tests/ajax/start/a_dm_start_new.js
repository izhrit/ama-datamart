define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/datamart?action=start.new');

	var findSettings = function (args)
	{

		var rows= ql.select(function(r)
			{
				return {
					id_ProcedureStart: r.ps.id_ProcedureStart,
					showToSRO: r.ps.ShowToSRO
				}
			})
			.from(db.ProcedureStart, 'ps')
			.where(function (r) { 
				return (args.id_Contract && r.ps.id_Contract==args.id_Contract)
						||  (args.id_Manager && r.ps.id_Manager==args.id_Manager); 
			})
			.exec();

		var lastPS = null;
		if(0!=rows.length) {
			lastPS = rows[0];
			for(var i = 0; i < rows.length; i++) {
				if(lastPS.id_ProcedureStart < rows[i].id_ProcedureStart)
					lastPS = rows[i];
			}
		}

		return { Показывать_СРО: (lastPS && lastPS.showToSRO) || 0 }
	}

	service.action = function (cx)
	{
		var args= cx.url_args();
		var res = null;
		res = findSettings(args);
		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});