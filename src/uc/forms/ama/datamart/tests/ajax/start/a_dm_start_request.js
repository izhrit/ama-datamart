define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/ajax/mrequest/a_dm_mrequest_crud'
]
, function (db, ajax_CRUD, ql, a_dm_mrequest_crud)
{
	var service = ajax_CRUD('ama/datamart?action=start.request');

	service.create = function (request, args) {
		function get_id_SRO(id_ProcedureStart)
		{
			var rows = ql.select(function (r) {
				return {
					id_SRO: r.m.id_SRO
				}
			})
				.from(db.Manager, 'm')
				.inner_join(db.ProcedureStart, 'ps', function (r) { return r.ps.id_Manager == r.m.id_Manager; })
				.where(function (r) {
					return id_ProcedureStart == r.ps.id_ProcedureStart
				})
				.exec();
			return rows[0].id_SRO
		}

		var row_MRequest = ql.find_first_or_null(db.MRequest, function (mr) { return request.Запрос.Ссылка==mr.CourtDecisionURL; });
		if(!row_MRequest){
			a_dm_mrequest_crud.create(request, {id_SRO: get_id_SRO(request.id_ProcedureStart)})
		}
	}

	service.action = function (cx)
	{
		var args= cx.url_args();
		var res = null;
		res = this.create(JSON.parse(cx.options.data), args);
		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});