﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/ql'
], function (db, ajax_jqGrid, codec_copy, ql)
{
	var service= ajax_jqGrid('ama/datamart?action=procedure.jqgrid');
	var ccodec_copy = codec_copy();
	service.rows_all = function (url,args)
	{
		var prepare_procedure_row = function (r)
		{
			var row = ccodec_copy.Copy(r.mp, row);
			row.debtorName = r.d.Name;
			row.debtorInn = r.d.INN;
			row.debtorOgrn = r.d.OGRN;
			row.debtorSnils = r.d.SNILS;
			row.publicDate= (r.pd && null!=r.pd) ? r.pd.publicDate : '2021-11-29T00:00:00';
			row.Archived= 0==r.mp.ArchivedByUser ? 0 : 1==r.mp.ArchivedByUser ? 1 : !r.mp.Archive ? 0 : 1;
			row.Manager = r.m.lastName
				+ (!r.m.firstName ? '' : (' ' + r.m.firstName.charAt(0).toUpperCase() + '.'))
				+ (!r.m.middleName ? '' : (' ' + r.m.middleName.charAt(0).toUpperCase() + '.'));
			row.Viewers = ql.select(function (rr) { return rr.mu.UserName; })
				.from(db.MProcedureUser, 'mpu')
				.inner_join(db.ManagerUser, 'mu', function (rr) { return rr.mpu.id_ManagerUser == rr.mu.id_ManagerUser; })
				.where(function (rr) { return rr.mpu.id_MProcedure == r.mp.id_MProcedure; })
				.exec();
			return row;
		}
		if (!args.id_MUser)
		{
			var procedure_rows = ql.select(prepare_procedure_row)
				.from(db.MProcedure, 'mp')
				.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
				.inner_join(db.Manager, 'm', function (r) { return (r.mp.id_Manager == r.m.id_Manager); })
				.left_join(db.PData, 'pd', function (r) { return (r.mp.id_MProcedure == r.pd.id_MProcedure); })
				.where(function (r)
				{
					if (!((r.mp.id_Manager == args.id_Manager) || (r.m.id_Contract == args.id_Contract)))
						return false;
					if ('true' == args.show_archive)
						return true;
					if (1==r.mp.ArchivedByUser)
						return false;
					return !r.mp.Archive;
				})
				.exec();
			var efrsb_procedure_rows = ql.select(function (r) {
				return {
					  id_Debtor_Manager: r.edm.id_Debtor_manager
					, Manager: r.m.LastName
						+ (!r.m.FirstName ? '' : (' ' + r.m.FirstName.charAt(0).toUpperCase() + '.'))
						+ (!r.m.MiddleName ? '' : (' ' + r.m.MiddleName.charAt(0).toUpperCase() + '.'))
					, debtorName: r.d.Name
					, debtorInn: r.d.INN
					, debtorOgrn: r.d.OGRN
					, debtorSnils: r.d.SNILS
			};})
			.from(db.efrsb_debtor_manager, 'edm')
			.inner_join(db.efrsb_debtor, 'd', function (r) { return r.edm.BankruptId == r.d.BankruptId; })
			.inner_join(db.efrsb_manager, 'm', function (r) { return (r.edm.ArbitrManagerID == r.m.ArbitrManagerID); })
			.where(function (r) { return r.edm.ArbitrManagerID == 3 })
			.exec();
			
			return ql.union(procedure_rows, efrsb_procedure_rows);
		}
		else
		{
			var procedure_rows = ql.select(prepare_procedure_row)
				.from(db.MProcedure, 'mp')
				.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
				.inner_join(db.Manager, 'm', function (r) { return (r.mp.id_Manager == r.m.id_Manager); })
				.inner_join(db.MProcedureUser, 'mpu', function (r) { return (r.mpu.id_MProcedure == r.mp.id_MProcedure); })
				.inner_join(db.ManagerUser, 'mu', function (r) { return (r.mu.id_ManagerUser == r.mpu.id_ManagerUser); })
				.where(function (r) { return r.mu.id_MUser == args.id_MUser; })
				.exec();
			var request_rows = ql.select(function (r) { return {
					id_Request: r.rq.id_Request
					, Manager: r.rq.managerName
					, debtorName: r.d.Name
					, state: r.rq.State
				};})
				.from(db.Request, 'rq')
				.inner_join(db.Debtor, 'd', function (r) { return r.rq.id_Debtor == r.d.id_Debtor; })
				.where(function (r) { return r.rq.id_MUser == args.id_MUser; })
				.exec();
			return ql.union(procedure_rows, request_rows);
		}
	}

	return service;
});