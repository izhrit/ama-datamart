﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'txt!forms/ama/datamart/procedure/section/registry/tests/rtk-example1.xml'
	, 'txt!forms/ama/datamart/procedure/section/registry/tests/rtk-example0.xml'
	, 'txt!forms/ama/datamart/procedure/section/report/tests/report.xml'
],
function (ajax_service_base, rtk_example1, rtk_example0, report)
{
	var service= ajax_service_base('ama/datamart?action=procedure.info');

	service.action= function (cx)
	{
		var id_Procedure = parseInt(cx.url_args().id_MProcedure);
		var rtk = (0 == (id_Procedure % 2)) ? rtk_example1 : rtk_example0;

		var splitter = '<!--/-->';
		var text = '';
		switch (id_Procedure)
		{
			case 1: text = rtk_example0; break;
			case 2: text = rtk_example1 + splitter; break;
			case 3: text = rtk_example0 + splitter + report; break;
			case 4: text = splitter + report; break;
			case 5: break;
		}

		cx.completeCallback(200, 'success', { text: text });
	}

	return service;
});