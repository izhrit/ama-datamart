﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/datamart?action=proc-viewers.ru');

	service.update = function (id, procedure)
	{
		db.MProcedureUser = ql._delete(db.MProcedureUser, function (r) { return r.id_MProcedure == id; });
		for (var i = 0; i < procedure.Доступ_у_наблюдателей.length; i++)
		{
			db.MProcedureUser.push({
				id_MProcedure: id
				, id_ManagerUser: procedure.Доступ_у_наблюдателей[i].id
			});
		}
	}

	service.read= function(id)
	{
		var rows= ql.select(function(rr){return {
				НаименованиеДолжника: rr.d.Name
				, ТипПроцедуры: rr.mp.procedure_type
				, id_MProcedure: rr.mp.id_MProcedure
				, Доступ_у_наблюдателей: ql.select(function (r) { return {
						id: r.mu.id_ManagerUser
						, text: r.mu.UserName
					};})
					.from(db.MProcedureUser, 'mpu')
					.where(function (r) { return r.mpu.id_MProcedure == rr.mp.id_MProcedure; })
					.inner_join(db.ManagerUser, 'mu', function (r) { return r.mpu.id_ManagerUser == r.mu.id_ManagerUser; })
					.exec()
			};})
			.from(db.MProcedure, 'mp')
			.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
			.where(function (r) { return id == r.mp.id_MProcedure; })
			.exec();
		return rows[0];
	}

	return service;
});