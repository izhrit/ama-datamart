﻿define
([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-select2'
	, 'forms/base/ql'
	, 'forms/ama/datamart/base/h_procedure_types'
]
, function (db, ajax_select2, ql, h_procedure_types)
{
	var service= ajax_select2('ama/datamart?action=procedure.select2');

	service.query = function (q, page, args)
	{
		var items = null;
		if (args.id_Manager)
		{
			items = ql.from(db.MProcedure, 'mp')
				.inner_join(db.Manager, 'm', function (r) { return (r.mp.id_Manager == r.m.id_Manager); })
				.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
				.where(function (r) { return (args.id_Manager == r.mp.id_Manager) && (-1 != r.d.Name.indexOf(q)) });


			var procedure_items = ql.from(db.MProcedure, 'mp')
				.inner_join(db.Manager, 'm', function (r) { return (r.mp.id_Manager == r.m.id_Manager); })
				.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
				.where(function (r) { return (args.id_Manager == r.mp.id_Manager) && (-1 != r.d.Name.indexOf(q)) });
			var efrsb_procedure_items = ql.from(db.efrsb_debtor_manager, 'edm')
				.inner_join(db.efrsb_debtor, 'd', function (r) { return r.edm.BankruptId == r.d.BankruptId; })
				.inner_join(db.efrsb_manager, 'm', function (r) { return r.edm.ArbitrManagerID == r.m.ArbitrManagerID; })
				.where(function (r) { return (3 == r.edm.ArbitrManagerID) && (-1 != r.d.Name.indexOf(q)); })
			items = ql.union(procedure_items, efrsb_procedure_items);
		}
		else if (args.id_Contract)
		{
			items = ql.from(db.MProcedure, 'mp')
				.inner_join(db.Manager, 'm', function (r) { return (r.mp.id_Manager == r.m.id_Manager); })
				.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
				.where(function (r) { return (args.id_Contract == r.m.id_Contract) && (-1 != r.d.Name.indexOf(q)) });
		}
		else if (args.id_MUser)
		{
			var procedure_items = ql.from(db.MProcedure, 'mp')
				.inner_join(db.Manager, 'm', function (r) { return (r.mp.id_Manager == r.m.id_Manager); })
				.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
				.inner_join(db.MProcedureUser, 'mpu', function (r) { return (r.mpu.id_MProcedure == r.mp.id_MProcedure); })
				.inner_join(db.ManagerUser, 'mu', function (r) { return (r.mu.id_ManagerUser == r.mpu.id_ManagerUser); })
				.where(function (r) { return (args.id_MUser == r.mu.id_MUser) && (-1 != r.d.Name.indexOf(q)); });
			var request_items = ql.from(db.Request, 'rq')
				.inner_join(db.Debtor, 'd', function (r) { return r.rq.id_Debtor == r.d.id_Debtor; })
				.where(function (r) { return (args.id_MUser == r.rq.id_MUser) && (-1 != r.d.Name.indexOf(q)); })
			items = ql.union(procedure_items, request_items);
		}
		else // используется в тестах формы прав наблбдателя данных ему конкретным АУ (там id_Manager берётся из сессии)
		{
			items = ql.from(db.MProcedure, 'mp')
				.inner_join(db.Manager, 'm', function (r) { return (r.mp.id_Manager == r.m.id_Manager); })
				.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
				.where(function (r) { return (-1 != r.d.Name.indexOf(q)) });
		}
		items = items.limit(15).map(function (r) { return h_procedure_types.PrepareSelect2(r); });
		return { results: items };
	}

	return service;
});
