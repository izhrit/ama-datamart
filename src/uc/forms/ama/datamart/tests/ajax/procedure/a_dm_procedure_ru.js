﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service= ajax_CRUD('ama/datamart?action=procedure.ru');


	/*
	Archive       |false|false|false|false|false|true |true |true |true |true
	ArchivedByUser|null |null |false|false|true |null |null |false|false|true
	В_архив       |false|true |false|true |false|false|true |false|true |false
	--------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
	ArchivedByUser|null |true |false|true |false|false|null |false|true |false

	*/

	service.update = function (id, procedure)
	{
		var MProcedure = ql.find_first_or_null(db.MProcedure, function (r) { return id == r.id_MProcedure; });
		if (!MProcedure.ExtraParams)
			MProcedure.ExtraParams = {};
		MProcedure.ExtraParams.Адрес = procedure.Должник.Адрес;
		var new_ArchivedByUser= (true==procedure.В_архив || 'true'==procedure.В_архив) ? 1 : 0;
		var MProcedure_Archive= MProcedure.Archive ? 1 : 0;
		var MProcedure_ArchivedByUser_is_null= (1!=MProcedure.ArchivedByUser && 0!=MProcedure.ArchivedByUser);
		if (!MProcedure_ArchivedByUser_is_null || new_ArchivedByUser!=MProcedure_Archive)
			MProcedure.ArchivedByUser= new_ArchivedByUser;
	}

	service.read= function(id)
	{
		var rows= ql.select(function (rr) { return {
				Должник:{
					Наименование: rr.d.Name
					, ИНН: rr.d.INN
					, ОГРН: rr.d.OGRN
					, СНИЛС: rr.d.SNILS
					, Адрес: (!rr.mp.ExtraParams || null == rr.mp.ExtraParams) ? '' : rr.mp.ExtraParams.Адрес
				}
				, НомерДела: rr.mp.caseNumber
				, ТипПроцедуры: rr.mp.procedure_type
				, id_MProcedure: rr.mp.id_MProcedure
				, В_архив: (1==rr.mp.ArchivedByUser || 1==rr.mp.Archive)
			};})
			.from(db.MProcedure, 'mp')
			.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
			.where(function (r) { return id == r.mp.id_MProcedure; })
			.exec();
		return rows[0];
	}

	return service;
});