﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
],
function (ajax_service_base, db, ql)
{
	var service = ajax_service_base('ama/datamart?action=procedure.dbinfo');

	service.action= function (cx)
	{
		var id_Procedure = parseInt(cx.url_args().id_MProcedure);

		var rows= ql.select(function (r) { return {
			m_firstName: r.m.firstName
			, m_lastName: r.m.lastName
			, m_middleName: r.m.middleName
			, m_efrsbNumber: r.m.efrsbNumber
			, m_INN: r.m.INN

			, p_debtorName: r.d.Name
			, p_debtorInn: r.d.INN
			, p_debtorOgrn: r.d.OGRN
			, p_debtorSnils: r.d.SNILS

			, p_procedure_type: r.mp.procedure_type
			, p_caseNumber: r.mp.caseNumber

			, p_publicDate: '03.03.2013 00:04:15'
			, p_ctb_allowed: 0
		};})
		.from(db.MProcedure, 'mp')
		.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
		.inner_join(db.Manager, 'm', function (r) { return r.m.id_Manager = r.mp.id_Manager; })
		.inner_join(db.Contract, 'c', function (r) { return r.m.id_Contract = r.c.id_Contract; })
		.where(function (r) { return r.mp.id_MProcedure == id_Procedure; })
		.exec();

		var dbinfo = rows[0];

		cx.completeCallback(200, 'success', { text: JSON.stringify(dbinfo, null, '\t') });
	}

	return service;
});