﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
], function (db, ajax_jqGrid, ql)
{
	var service= ajax_jqGrid('ama/datamart?action=request.jqgrid');

	service.rows_all = function (url,args)
	{
		return ql.select(function (r) { return {
				id_Request: r.rq.id_Request
				, Имя: r.mu.UserName
				, email: r.mu.UserEmail
				, Представляет: r.rq.Body.Процедура.КогоПредставляетЗаявитель
				, В_процедуре: r.d.Name
				, TimeLastChange: r.rq.TimeLastChange
				, state: r.rq.State
			};})
			.from(db.Request, 'rq')
			.inner_join(db.Debtor, 'd', function (r) { return r.rq.id_Debtor == r.d.id_Debtor; })
			.inner_join(db.MProcedure, 'mp', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
			.inner_join(db.Manager, 'm', function (r) { return r.m.id_Manager == r.mp.id_Manager; })
			.inner_join(db.MUser, 'mu', function (r) { return r.mu.id_MUser == r.rq.id_MUser; })
			.where(function (r) { return r.m.id_Manager == args.id_Manager; })
			.exec();
	}

	return service;
});