﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/h_times'
],
function (ajax_service_base, ql, db, h_codec_datetime, h_times)
{
	var service = ajax_service_base('ama/datamart?action=request.resolve');

	var approve = function (request)
	{
		var r = ql.from(db.Request, 'rq')
		.where(function (r) { return r.rq.id_Request == request.id_Request; })
		.inner_join(db.Debtor, 'd', function (r) { return r.rq.id_Debtor == r.d.id_Debtor; })
		.inner_join(db.MUser, 'mu', function (r) { return r.mu.id_MUser == request.id_MUser; })
		.inner_join(db.MProcedure, 'mp', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
		.inner_join(db.Manager, 'm', function (r) { return r.m.id_Manager == r.mp.id_Manager; })
		.rows[0];

		ql.insert_with_next_id(db.ManagerUser, 'id_ManagerUser',
		{
			id_MUser: request.id_MUser
			, id_Manager: r.m.id_Manager
			, UserName: r.mu.UserName
		});

		db.MProcedureUser.push({
			  id_MProcedure: r.mp.id_MProcedure
			, id_Request: request.id_Request
			, id_ManagerUser: r.mu.id_ManagerUser
		});
	}

	service.action= function (cx)
	{
		var args= cx.url_args();
		var state = 'a';
		switch (args.decision)
		{
			case 'reject': state = 'r'; break;
			case 'approve': state = 'p'; break;
		}

		var request = ql.find_first_or_null(db.Request, function (rq) { return rq.id_Request == args.id_Request; });
		request.State = state;
		request.TimeLastChange = h_codec_datetime.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()), h_codec_datetime.format.mysql);

		if ('approve' == args.decision)
			approve(request);

		cx.completeCallback(200, 'success', { text: JSON.stringify({ ok: true }) });
	}

	return service;
});