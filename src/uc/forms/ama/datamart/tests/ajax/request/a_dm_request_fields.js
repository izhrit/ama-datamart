﻿define([
	  'forms/base/ajax/ajax-service-base'
],
function (ajax_service_base)
{
	var service = ajax_service_base('ama/datamart?action=request.fields');

	service.action= function (cx)
	{
		var res = {
			КогоПредставляетЗаявитель: 'высшие силы'
			, Пояснения: 'чесное блахародное'
		};

		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});