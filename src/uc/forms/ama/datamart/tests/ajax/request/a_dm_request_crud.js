define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/h_times'
]
, function (db, ajax_CRUD, ql, h_codec_datetime, h_times)
{
	var service = ajax_CRUD('ama/datamart?action=request.crud');

	service.create= function(request, args)
	{
		var debtor = ql.find_first_or_null(db.Debtor, function (d) { 
			return d.INN && d.INN == request.Должник.ИНН
				|| d.OGRN && d.OGRN == request.Должник.ОГРН
				|| d.SNILS && d.SNILS == request.Должник.СНИЛС;
		});
		if (null == debtor)
		{
			debtor= ql.insert_with_next_id(db.Debtor, 'id_Debtor', {
				Name: request.Должник.Наименование
				, INN: request.Должник.ИНН
				, OGRN: request.Должник.ОГРН
				, SNILS: request.Должник.СНИЛС
			});
		}
		var TimeLastChange = h_codec_datetime.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()), h_codec_datetime.format.mysql);
		ql.insert_with_next_id(db.Request, 'id_Request', {
			id_MUser: args.id_MUser
			, id_Debtor: debtor.id_Debtor
			, managerName: request.Процедура.Управляющий
			, Body: {
				Процедура: {
					КогоПредставляетЗаявитель: request.Процедура.КогоПредставляетЗаявитель
				}
				, Пояснения: request.Пояснения
			}
			, State: 'a'
			, TimeLastChange: TimeLastChange
		});
	}

	service.update = function (id, request, args)
	{
		var r = ql.find_first_or_null(db.Request, function (r) { return r.id_Request == id; });
		r.Body.Пояснения = request.Пояснения;
		r.Body.Процедура.КогоПредставляетЗаявитель = request.Процедура.КогоПредставляетЗаявитель;
	}

	service.read= function(id)
	{
		var rows = ql.select(function (r) { return {
				Должник: {
					Наименование: r.d.Name
					, ИНН: r.d.INN
					, ОГРН: r.d.OGRN
					, СНИЛС: r.d.SNILS
					, BankruptId: r.d.BankruptId
				}
				, Процедура: {
					Управляющий: r.rq.managerName
					, КогоПредставляетЗаявитель: r.rq.Body.Процедура.КогоПредставляетЗаявитель
				}
				, Заявитель: {
					Имя: r.mu.UserName
					,email: r.mu.UserEmail
				}
				, Пояснения: r.rq.Body.Пояснения
				, TimeLastChange: h_codec_datetime.ru_legal_txt2txt_mysql().Decode(r.rq.TimeLastChange)
				, State: r.rq.State
		};})
		.from(db.Debtor, 'd')
		.inner_join(db.Request, 'rq', function (r) { return r.rq.id_Debtor == r.d.id_Debtor; })
		.inner_join(db.MUser, 'mu', function (r) { return r.mu.id_MUser == r.rq.id_MUser; })
		.where(function (r) { return r.rq.id_Request == id; })
		.exec();
		return rows[0];
	}

	service._delete= function(ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id = id_arr[i];
			db.Request = ql._delete(db.Request, function (r) { return id == r.id_Request; });
		}
	}

	return service;
});