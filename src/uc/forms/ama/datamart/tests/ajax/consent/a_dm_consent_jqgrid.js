﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
	, 'forms/base/codec/datetime/h_codec.datetime'
], function (db, ajax_jqGrid, ql, h_codec_datetime)
{
	var service= ajax_jqGrid('ama/datamart?action=consent.jqgrid');
	service.rows_all = function (url,args)
	{
		var rows= ql.select(function (r) { return {
				id_ProcedureStart : r.s.id_ProcedureStart
				,DebtorCategory: r.s.DebtorCategory
				,debtorName: r.s.DebtorName
				,debtorName2: r.s.DebtorName2
				,DateOfApplication: !r.s.DateOfApplication || null == r.s.DateOfApplication || '' == r.s.DateOfApplication ? ''
					: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.s.DateOfApplication)
				,Court: r.c.ShortName
				,Manager: (!r.m || null==r.m) ? '' : (r.m.lastName + ' ' + r.m.firstName.charAt(0) + '.' + r.m.middleName.charAt(0) + '.')
				,caseNumber: r.s.CaseNumber
				,NextSessionDate:  !r.s.NextSessionDate || null==r.s.NextSessionDate || ''==r.s.NextSessionDate ? ''
				: h_codec_datetime.mysql_txt2txt_ru_legal().Encode(r.s.NextSessionDate)
				,addedBySRO: r.s.AddedBySRO
				,id_MRequest: r.s.id_MRequest
			};})
			.from(db.ProcedureStart, 's')
			.inner_join(db.Court, 'c', function (r) { return r.s.id_Court==r.c.id_Court; })
			.left_join(db.Manager, 'm', function (r) { return r.m.id_Manager==r.s.id_Manager; })
			.where(function (r) 
				{
					return r.s.id_Manager && r.m.id_SRO==args.id_SRO && (r.s.ShowToSRO===1 || r.s.AddedBySRO==1)
				}
			)
			.exec();
		return rows;
	}

	return service;
});