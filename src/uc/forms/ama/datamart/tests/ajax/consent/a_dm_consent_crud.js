define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/ajax/consent/h_a_dm_consent'
	, 'forms/base/codec/datetime/h_codec.datetime'
]
, function (db, ajax_CRUD, ql, h_a_dm_consent, h_codec_datetime)
{
	var service = ajax_CRUD('ama/datamart?action=consent.crud');

	var getDebtorObject = function (r, num) {
		var DebtorCategory = 'DebtorCategory' + (num || '');
		var DebtorName = 'DebtorName' + (num || '');
		var DebtorSNILS = 'DebtorSNILS' + (num || '');
		var DebtorINN = 'DebtorINN' + (num || '');
		var DebtorOGRN = 'DebtorOGRN' + (num || '');
		var DebtorAddress = 'DebtorAddress' + (num || '');

		var Должник = { ИНН: r.ps[DebtorINN] || '', ОГРН: r.ps[DebtorOGRN] || '', Адрес: r.ps[DebtorAddress] || '' };
		switch (r.ps[DebtorCategory])
		{
			case 'l':
				Должник.Тип= "Юр_лицо";
				Должник.Юр_лицо= { Наименование: r.ps[DebtorName] };
				break;
			case 'n':
				var p= r.ps[DebtorName].split(' ');
				Должник.Тип= "Физ_лицо";
				Должник.Физ_лицо= { Фамилия: p[0], Имя: p[1], Отчество: p[2], СНИЛС: r.ps[DebtorSNILS] || '' };
				break;
		}

		return Должник;
	}

	service.create= function(consent,args)
	{
		var names = consent.АУ.text.split(' ');
		if (consent.АУ.id) {
			var rows = ql.find_first_or_null(db.Manager, function (m) { return consent.АУ.id == m.ArbitrManagerID; });
			
			if (!rows) {
				var res = ql.insert_with_next_id(db.Manager, 'id_Manager', {
					  lastName: names[0]
					, middleName: names[2]
					, firstName: names[1]
					, id_SRO: 2
				});
				res.ArbitrManagerID = res.id_Manager;
				consent.АУ.id = res.id_Manager;
			} else {
				consent.АУ.id = rows.id_Manager;
			}
		}
		var row_fields= h_a_dm_consent.fill_row_by_consent(consent);
		row_fields.AddedBySRO= 1;
		ql.insert_with_next_id(db.ProcedureStart, 'id_ProcedureStart', row_fields);
	}

	service.update = function (id_ProcedureStart, consent)
	{
		var names = consent.АУ.text.split(' ');
		if (consent.АУ.id) {
			var rows = ql.find_first_or_null(db.Manager, function (m) { return consent.АУ.id == m.ArbitrManagerID; });
			if (!rows) {
				var res = ql.insert_with_next_id(db.Manager, 'id_Manager', {
					  lastName: names[0]
					, middleName: names[2]
					, firstName: names[1]
					, id_SRO: 2
				});
				res.ArbitrManagerID = res.id_Manager;
				consent.АУ.id = res.id_Manager;
			} else {
				consent.АУ.id = rows.id_Manager;
			}
		}
		var row_ProcedureStart = ql.find_first_or_null(db.ProcedureStart, function (ps) { return id_ProcedureStart==ps.id_ProcedureStart; });
		h_a_dm_consent.fill_row_by_consent(consent, row_ProcedureStart);
	};

	service.read= function(id_ProcedureStart)
	{
		var rows= ql.select(function(r)
			{
				var Заявитель= { ИНН: r.ps.ApplicantINN, ОГРН: r.ps.ApplicantOGRN, Адрес: r.ps.ApplicantAddress  };
				var consent= {
					id_ProcedureStart: r.ps.id_ProcedureStart
					, Заявитель: Заявитель
					, Заявление_на_банкротство: {
						Подано: (r.ps.DateOfApplication && null!=r.ps.DateOfApplication && ''!=r.ps.DateOfApplication)
						, Дата_подачи: !r.ps.DateOfApplication || null==r.ps.DateOfApplication || ''==r.ps.DateOfApplication ? ''
							: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.DateOfApplication)
						, В_суд: ((null == r.c) ? null : { id: r.c.id_Court, text:r.c.Name })
						, Дата_принятия: !r.ps.DateOfAcceptance || null==r.ps.DateOfAcceptance || ''==r.ps.DateOfAcceptance ? ''
						: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.DateOfAcceptance)
						, Дата_рассмотрения: !r.ps.NextSessionDate || null==r.ps.NextSessionDate || ''==r.ps.NextSessionDate ? ''
							: h_codec_datetime.mysql_txt2txt_ru_legal().Encode(r.ps.NextSessionDate)
					}
					,Номер_судебного_дела:r.ps.CaseNumber
					,Запрос: {
						Ссылка: r.ps.CourtDecisionURL || ''
						, Время: {
							акта: !r.ps.DateOfRequestAct || null == r.ps.DateOfRequestAct || '' == r.ps.DateOfRequestAct ? ''
							: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.DateOfRequestAct)
						}
						, Дополнительная_информация: r.ps.CourtDecisionAddInfo || ''
					}
					,Запрос_дополнительная_информация: {
						Дополнительная_информация: r.ps.CourtDecisionAddInfo || ''
					}
					,Время_сверки:r.ps.TimeOfLastChecking
					,Показывать_для_регистрации_в_ПАУ:(0!=r.ps.ReadyToRegistrate)
					,Назначение: {
						Ссылка: r.ps.PrescriptionURL || ''
						, Время: {
							акта: !r.ps.DateOfPrescription || null==r.ps.DateOfPrescription || ''==r.ps.DateOfPrescription ? ''
							: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.DateOfPrescription)
						}
						, Дополнительная_информация: r.ps.PrescriptionAddInfo || ''
					}
					,Назначение_дополнительная_информация: {
						Дополнительная_информация: r.ps.PrescriptionAddInfo || ''
					}
					,ЕФРСБ: {
						Номер:r.ps.efrsbPrescriptionNumber || ''
						,Дата_публикации:!r.ps.efrsbPrescriptionPublishDate || null==r.ps.efrsbPrescriptionPublishDate || ''==r.ps.efrsbPrescriptionPublishDate ? '' : h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.efrsbPrescriptionPublishDate)
						,ID:r.ps.efrsbPrescriptionID || ''
						,Дополнительная_информация:r.ps.efrsbPrescriptionAddInfo || ''
					}
					,ЕФРСБ_дополнительная_информация: {
						Дополнительная_информация: r.ps.efrsbPrescriptionAddInfo || ''
					}
				};

				if(r.ps.DebtorName2) {
					consent.Должник = { Тип: 'Супруги', Супруги: { Должник1: getDebtorObject(r), Должник2: getDebtorObject(r, 2) } };
				} else {
					consent.Должник = getDebtorObject(r);
				}
				
				switch (r.ps.ApplicantCategory)
				{
					case 'l':
						Заявитель.Тип= "Юр_лицо";
						Заявитель.Юр_лицо= { Наименование: r.ps.ApplicantName };
						break;
					case 'n':
						var p= r.ps.ApplicantName.split(' ');
						Заявитель.Тип= "Физ_лицо";
						Заявитель.Физ_лицо= { Фамилия: p[0], Имя: p[1], Отчество: p[2], СНИЛС: r.ps.ApplicantSNILS };
						break;
				}
				if(!r.ps.ApplicantName) {
					consent.Заявитель = {Заявитель_должник: true}
				}
				if (!r.m || null == r.m)
				{
					consent.АУ = null;
				}
				else
				{
					consent.АУ = {
						id: r.m.ArbitrManagerID
						,text: (r.m.lastName + ' ' + r.m.firstName.charAt(0) + '.' + r.m.middleName.charAt(0) + '.')
					}
				}

				return consent;
			})
			.from(db.ProcedureStart, 'ps')
			.inner_join(db.Court, 'c', function (r) { return r.ps.id_Court==r.c.id_Court; })
			.left_join(db.Manager, 'm', function (r) { return r.m.id_Manager==r.ps.id_Manager; })
			.where(function (r) { return id_ProcedureStart == r.ps.id_ProcedureStart; })
			.exec();
	
		var consent= rows[0];
		return consent;
	};

	service._delete= function(ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id = id_arr[i];
			db.ProcedureStart = ql._delete(db.ProcedureStart, function (r) { return id == r.id_ProcedureStart; });
		}
	};

	return service;
});