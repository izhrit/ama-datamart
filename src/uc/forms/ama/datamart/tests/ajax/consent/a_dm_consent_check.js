define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/base/h_times'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/ama/datamart/tests/ajax/consent/h_a_dm_consent'
]
, function (db, ajax_CRUD, ql, h_times, h_codec_datetime, h_a_dm_consent)
{
	var service = ajax_CRUD('ama/datamart?action=consent.check');

	var actualize = function (consent,args)
	{
		var date= h_times.safeDateTime();
		var dt= h_codec_datetime.Date2dt(date);
		var Время_сверки= h_codec_datetime.ru_legal_txt2dt().Decode(dt);

		var consent_row= null;
		var names = consent.АУ.text.split(' ');
		if (consent.АУ.id) {
			var rows = ql.find_first_or_null(db.Manager, function (m) { return consent.АУ.id == m.ArbitrManagerID; });
			if (!rows) {
				var res = ql.insert_with_next_id(db.Manager, 'id_Manager', {
					lastName: names[0]
					, middleName: names[2]
					, firstName: names[1]
					, id_SRO: 2
				});
				consent.АУ.id = res.id_Manager;
			} else {
				consent.АУ.id = rows.id_Manager;
			}
		}
		if (consent.id_ProcedureStart)
		{
			consent_row = ql.find_first_or_null(db.ProcedureStart, function (ps) { return ps.id_ProcedureStart==consent.id_ProcedureStart;});
			h_a_dm_consent.fill_row_by_consent(consent,consent_row);
		}
		else
		{
			consent_row= h_a_dm_consent.fill_row_by_consent(consent);
			consent_row.AddedBySRO= 1;
			ql.insert_with_next_id(db.ProcedureStart, 'id_ProcedureStart', consent_row);
		}
		consent_row.TimeOfLastChecking= Время_сверки;
		return { Время_сверки: Время_сверки, id_ProcedureStart: consent_row.id_ProcedureStart };
	}

	var search = function (consent,args)
	{

		var date= h_times.safeDateTime();
		var dt= h_codec_datetime.Date2dt(date);
		var Время_сверки= h_codec_datetime.ru_legal_txt2dt().Decode(dt);
		var consent_row = null;
		var names = consent.АУ.text.split(' ');
		if (consent.АУ.id) {
			var rows = ql.find_first_or_null(db.Manager, function (m) { return consent.АУ.id == m.ArbitrManagerID; });
			if (!rows) {
				var res = ql.insert_with_next_id(db.Manager, 'id_Manager', {
					lastName: names[0]
					, middleName: names[2]
					, firstName: names[1]
					, id_SRO: 2
				});
				consent.АУ.id = res.id_Manager;
			} else {
				consent.АУ.id = rows.id_Manager;
			}
		}
		if (consent.id_ProcedureStart)
		{
			consent_row = ql.find_first_or_null(db.ProcedureStart, function (ps) { return ps.id_ProcedureStart==consent.id_ProcedureStart;});
			h_a_dm_consent.fill_row_by_consent(consent,consent_row);
		}
		else
		{
			consent_row= h_a_dm_consent.fill_row_by_consent(consent);
			consent_row.AddedBySRO= 1;
			ql.insert_with_next_id(db.ProcedureStart, 'id_ProcedureStart', consent_row);
		}
		consent_row.TimeOfLastChecking= Время_сверки;
		if ('Жизнеспособный Изъян Фрейдович' == consent_row.DebtorName)
		{
			consent_row.CaseNumber= 'К86/2021';
			consent_row.NextSessionDate= '2021-03-16';
		}
		var res= { 
			Время_сверки: Время_сверки
			,Номер_судебного_дела: consent_row.CaseNumber
			,Дата_рассмотрения: consent_row.NextSessionDate
			,id_ProcedureStart: consent_row.id_ProcedureStart
		};
		return res;
	}

	service.action = function (cx)
	{
		var args= cx.url_args();
		var res = null;
		switch (args.cmd)
		{
			case 'actualize': res = actualize(cx.data_args(),args); break;
			case 'search': res = search(cx.data_args(),args); break;
			default:
				completeCallback(400, 'Bad Request', '');
				return;
		}
		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});