﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
	, 'forms/ama/datamart/base/h_procedure_types'
	, 'forms/base/codec/codec.copy'
], function (db, ajax_jqGrid, ql, h_procedure_types, codec_copy)
{
	var service = ajax_jqGrid('ama/datamart?action=leaks.jqgrid');

	service.rows_all = function ()
	{
		var ccodec_copy = codec_copy();
		return ql.select(function (r) {
			var row = ccodec_copy.Copy(r.le, row);
			row.LeakSum = r.le.LeakSum;
			row.LeakDate = r.le.LeakDate;
			row.Contragent = r.le.ContragentName;
			row.isAccredited = r.le.isAccredited;

			row.Procedure = h_procedure_types.SafeShortForDBValue(r.mp.procedure_type);

			row.Manager = r.m.lastName
						+ (!r.m.firstName ? '' : (' ' + r.m.firstName.charAt(0).toUpperCase() + '.'))
						+ (!r.m.middleName ? '' : (' ' + r.m.middleName.charAt(0).toUpperCase() + '.'));

			return row;
		})
		.from(db.Leak, "le")
		.inner_join(db.PData, 'pd', function (r) { return (r.le.id_PData == r.pd.id_PData); })
		.inner_join(db.MProcedure, 'mp', function (r) { return (r.pd.id_MProcedure == r.mp.id_MProcedure); })
		.inner_join(db.Manager, 'm', function (r) { return (r.mp.id_Manager == r.m.id_Manager); })
		.where(function (r) { return (r.le.id_PData == r.pd.id_PData); })
		.exec();
	}

	return service;
});