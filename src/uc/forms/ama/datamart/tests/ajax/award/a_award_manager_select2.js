﻿define([
	'forms/base/ajax/ajax-select2'
]
, function (ajax_select2)
{
	var service= ajax_select2('ama/datamart?action=award.managers');

	var h_select2_variants = [
		{
			id: 'вариант 1', text: 'Сидинеев С С'
			, data: {
				Имя: 'Сиденей', Фамилия: 'Сиденеев', Отчество: 'Сиденеевич'
				, inn: '326'
				, СРО: 'Ата та СРО'
			}
		}
		, {
			id: 'вариант 2', text: 'Алиев А А'
			, data: {
				Имя: 'Алий', Фамилия: 'Алиев', Отчество: 'Алиевич'
				, inn: '323'
				, СРО: 'Восточное СРО'
			}
		}
		, {
			id: 'вариант 3', text: 'Коровьев К К'
			, data: {
				Имя: 'Коров', Фамилия: 'Коровьев', Отчество: 'Коровьевич'
				, inn: '327'
				, СРО: 'Независимые АУ'
			}
		}
	];

	service.query = function (q, page)
	{
		var maxSize = 6;
		var items = [];
		var i = 0;
		for (; i < h_select2_variants.length && items.length < maxSize; i++)
		{
			var item = h_select2_variants[i];
			if (-1 != item.text.indexOf(q))
				items.push(h_select2_variants[i]);
		}
		var more = items.length == maxSize && i < h_select2_variants.length;
		return { results: items, pagination: { more: more } };
	}

	return service;
});