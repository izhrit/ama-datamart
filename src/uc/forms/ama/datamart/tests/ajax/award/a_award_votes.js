﻿define([
	'forms/base/ajax/ajax-service-base'
],
function (ajax_service_base)
{
	var service = ajax_service_base('ama/datamart?action=award.votes');

	service.action= function (cx)
	{
		cx.completeCallback(200, 'success', { text: JSON.stringify(0, null, '\t') });
	}

	return service;
});