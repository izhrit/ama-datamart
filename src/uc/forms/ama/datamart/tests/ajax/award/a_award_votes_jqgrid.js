﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
], function (db, ajax_jqGrid, ql)
{
	var service= ajax_jqGrid('ama/datamart?action=award.jqgrid');

	service.rows_all = function ()
	{
		return ql.select(function (r) { return {
			id_AwardVote: r.v.id_AwardVote
			, АУ: r.v.lastName + ' ' + r.v.firstName + ' ' + r.v.middleName
			, СРО: r.v.SRO
			, VoteTime: r.v.VoteTime
			, За: r.n.lastName + ' ' + r.n.firstName + ' ' + r.n.middleName
		};})
		.from(db.AwardVote, "v")
		.inner_join(db.AwardNominee, 'n', function (r) { return (r.v.id_AwardNominee == r.n.id_AwardNominee); })
		.exec();
	}

	return service;
});