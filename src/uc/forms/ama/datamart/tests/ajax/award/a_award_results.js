﻿define([
	'forms/base/ajax/ajax-service-base'
],
function (ajax_service_base)
{
	var service = ajax_service_base('ama/datamart?action=award.results');

	service.action = function (cx)
	{
		var nominees = [
			{
				"Номинант": "иванов и и"
				, "СРО": "Отцы демократии"
				, "Голосов": 100
			}
			, {
				"Номинант": "петров п п"
				, "СРО": "Ошибки эволюции"
				, "Голосов": 200
			}
			, {
					"Номинант": "сидоров с с"
				, "СРО":"Гиганты мысли"
				, "Голосов": 50
			}
		];

		cx.completeCallback(200, 'success', { text: JSON.stringify(nominees, null, '\t') });
	}

	return service;
});