﻿define([
	  'forms/base/ajax/ajax-service-base'
],
function (ajax_service_base)
{
	var service = ajax_service_base('ama/datamart?action=award.login');

	service.action= function (cx)
	{
		var args= cx.data_args();
		cx.completeCallback(200, 'success', { text: JSON.stringify({ ok: (args.password=== args.login) }) });
	}

	return service;
});
