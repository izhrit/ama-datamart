﻿define([
	'forms/base/ajax/ajax-service-base'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/d_datamart'
],
function (ajax_service_base, ql, db)
{
	var service = ajax_service_base('ama/datamart?action=award.manager');

	service.action= function (cx)
	{
		var inn = cx.url_args().inn;

		var managers = ql.select(function (r) { return {
			inn: r.v.inn
			, VoteTime: r.v.VoteTime
			, Signed: (null != r.v.BulletinSignature)
		};})
		.from(db.AwardVote, 'v')
		.where(function (r) { return r.v.inn == inn; })
		.exec();

		var manager = 0==managers.length ? null : managers[0];

		cx.completeCallback(200, 'success', { text: JSON.stringify(manager, null, '\t') });
	}

	return service;
});