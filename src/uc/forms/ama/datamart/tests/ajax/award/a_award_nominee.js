﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
],
function (ajax_service_base, db, ql)
{
	var service = ajax_service_base('ama/datamart?action=award.nominee');

	service.action= function (cx)
	{
		var id_AwardNominee = parseInt(cx.url_args().id_AwardNominee);

		var nominees = ql.select(function (r) { return {
			Имя: r.n.firstName
			, Фамилия: r.n.lastName
			, Отчество: r.n.middleName
			, СРО: r.n.SRO
		};})
		.from(db.AwardNominee, 'n')
		.where(function (r) { return r.n.id_AwardNominee == id_AwardNominee; })
		.exec();

		var nominee= nominees[0];

		cx.completeCallback(200, 'success', { text: JSON.stringify(nominee, null, '\t') });
	}

	return service;
});