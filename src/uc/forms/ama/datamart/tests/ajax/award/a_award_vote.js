﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
],
function (ajax_service_base, db, ql)
{
	var service = ajax_service_base('ama/datamart?action=award.vote');

	service.action= function (cx)
	{
		var args= cx.data_args();

		var nominee= ql.find_first_or_null(db.AwardNominee,function(n){
			return n.inn==args.ПроголосовалЗа.ИНН
		});

		ql.insert_with_next_id(db.AwardVote, 'id_AwardVote', {
			inn: args.Голосующий.ИНН
			, VoteTime: new Date()
			, id_AwardNominee: nominee.id_AwardNominee
		});

		cx.completeCallback(200, 'success', { text: JSON.stringify({}, null, '\t') });
	}

	return service;
});