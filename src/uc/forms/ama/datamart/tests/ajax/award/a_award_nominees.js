﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
],
function (ajax_service_base, db, ql)
{
	var service = ajax_service_base('ama/datamart?action=award.nominees');

	service.action= function (cx)
	{
		var nominees = ql.select(function (r) { return {
			Имя: r.n.firstName
			, Фамилия: r.n.lastName
			, Отчество: r.n.middleName
			, СРО: r.n.SRO
			, ИНН: r.n.inn
			, URL: r.n.URL
			, id_AwardNominee: r.n.id_AwardNominee
		};})
		.from(db.AwardNominee, 'n')
		.exec();

		cx.completeCallback(200, 'success', { text: JSON.stringify(nominees, null, '\t') });
	}

	return service;
});