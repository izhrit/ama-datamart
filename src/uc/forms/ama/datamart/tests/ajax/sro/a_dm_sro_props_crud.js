﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql) {
	var service = ajax_CRUD('ama/datamart?action=sro.props.crud');

	service.update = function (id, request, args) {
		var e = ql.find_first_or_null(db.efrsb_sro, function (e) { return e.id_SRO == id; });
		e.ExtraFields = request;
	}

	service.read = function (id) {

		var rows = ql
			.select(function (r) {
				return JSON.stringify(r.s.ExtraFields);
			})
			.from(db.efrsb_sro, 's')
			.where(function (r) { return r.s.id_SRO == id; })
			.exec();
		return rows[0];
	}

	return service;
});