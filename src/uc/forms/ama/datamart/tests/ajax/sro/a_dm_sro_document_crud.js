﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/datamart?action=sro.document.ru');
	service.read = function(id)
	{
		var rows= ql
			.select(function (r) {
				var res= {
					id_SRODocument: r.d.id_SRODocument
					, FileName: r.d.FileName
					, DocumentType: r.d.DocumentType
				};
				return res;
			})
			.from(db.SRODocument, 'd')
			.inner_join(db.efrsb_sro, 'e', function (r) { return r.d.id_SRO == r.e.id_SRO; })
			.where(function (r) { return r.e.id_SRO == id; })
			.exec();
		return rows;
	}

	service.create = function (id, args) {
		return 3;
	}

	service._delete = function (id) {
		db.SRODocument = ql._delete(db.SRODocument, function (d) { return d.id_SRODocument == id; });
	}

	return service;
});