﻿define
(
	[
		'forms/ama/datamart/tests/d_datamart'
		, 'forms/base/ajax/ajax-CRUD'
		, 'forms/base/ql'
		, 'forms/ama/datamart/base/h_procedure_types'
		, 'forms/ama/datamart/tests/ajax/viewer/h_a_dm_viewer'
	]
	, function (db, ajax_CRUD, ql, h_procedure_types, h_a_dm_viewer)
	{
		var service = ajax_CRUD('ama/datamart?action=viewer.crud');

		var insert_MProcedureUsers= function(id_ManagerUser,Доступ_к_процедурам)
		{
			if (Доступ_к_процедурам)
			{
				for (var i = 0; i < Доступ_к_процедурам.length; i++)
				{
					db.MProcedureUser.push({
						id_ManagerUser: id_ManagerUser
						, id_MProcedure: Доступ_к_процедурам[i].id
					});
				}
			}
		};

		var insert_MUser = function (viewer)
		{
			return ql.insert_with_next_id(db.MUser, 'id_MUser', {
				  UserEmail: viewer.Email
				, UserName: viewer.Имя
			});
		};

		var insert_or_find_MUser= function(viewer)
		{
			var MUser = ql.find_first_or_null(db.MUser, function (r) { return r.UserEmail == viewer.Email; });
			if (null == MUser)
				MUser = insert_MUser(viewer);
			return MUser;
		};

		service.create_as_customer = function (viewer, args)
		{
			var MUser = insert_or_find_MUser(viewer.Аккаунт);
			var row_fields= h_a_dm_viewer.fill_row_by_viewer(viewer);
			$.extend(MUser, row_fields);
			var result = ql.insert_with_next_id(db.ContractUser, 'id_ContractUser', {
				UserName: viewer.Аккаунт.Имя
				, id_MUser: MUser.id_MUser
				, id_Contract: args.id_Contract
			});
		}

		service.create_as_manager = function(viewer, args)
		{
			var MUser = insert_or_find_MUser(viewer);
			var ManagerUser = ql.insert_with_next_id(db.ManagerUser, 'id_ManagerUser', {
				UserName: viewer.Имя
				, DefaultViewer: viewer.Давать_доступ_ко_всем_процедурам_по_умолчанию
				, id_MUser: MUser.id_MUser
				, id_Manager: viewer.id_Manager
			});
			if (viewer.Доступ_к_процедурам && null != viewer.Доступ_к_процедурам)
				insert_MProcedureUsers(ManagerUser.id_ManagerUser, viewer.Доступ_к_процедурам);
		}

		service.create= function(viewer,args)
		{
			if(args.id_Contract)
			{
				return this.create_as_customer(viewer,args)
			}
			else
			{
				return this.create_as_manager(viewer,args)
			}
		};

		service.update_as_customer = function (id, viewer)
		{
			var ContractUser = ql.find_first_or_null(db.ContractUser, function (r) { return r.id_ContractUser == id; });
			if (null != ContractUser)
			{
				var MUser = ql.find_first_or_null(db.MUser, function (r) { return r.id_MUser == ContractUser.id_MUser; });
				if (!MUser.Confirmed && MUser.UserEmail != viewer.Email)
				{
					/* to do удалить если надо старый MUser и надо ли? */
					MUser = insert_or_find_MUser(viewer.Аккаунт);
				}
				var row_fields= h_a_dm_viewer.fill_row_by_viewer(viewer, MUser);
				$.extend(MUser, row_fields);
				ContractUser.UserName = viewer.Аккаунт.Имя;
				ContractUser.id_MUser = MUser.id_MUser;
			}
		}

		service.update_as_managerOrViewer = function (id, viewer)
		{
			if(args.id_Contract) {
				return this.create_as_customer(viewer,args)
			}else {
				return this.create_as_manager(viewer,args)
			}
		};

		service.update_as_customer = function (id, viewer) {
			var ContractUser = ql.find_first_or_null(db.ContractUser, function (r) { return r.id_ContractUser == id; });
			if (null != ContractUser)
			{
				var MUser = ql.find_first_or_null(db.MUser, function (r) { return r.id_MUser == ContractUser.id_MUser; });
				if (!MUser.Confirmed && MUser.UserEmail != viewer.Email)
				{
					/* to do удалить если надо старый MUser и надо ли? */
					MUser = insert_or_find_MUser(viewer.Аккаунт);
				}
				var row_fields= h_a_dm_viewer.fill_row_by_viewer(viewer, MUser);
				$.extend(MUser, row_fields);
				ContractUser.UserName = viewer.Аккаунт.Имя;
				ContractUser.id_MUser = MUser.id_MUser;
			}
		}

		service.update_as_managerOrViewer = function (id, viewer) {
			if (!viewer.Имя) // viewer сохраняет сам себя
			{
				var MUser = ql.find_first_or_null(db.MUser, function (r) { return r.id_MUser == id; });
				MUser.UserName = viewer.Имя;
			}
			else // АУ меняет свойства viewer а
			{
				var ManagerUser = ql.find_first_or_null(db.ManagerUser, function (r) { return r.id_ManagerUser == id; });
				if (null != ManagerUser)
				{
					var MUser = ql.find_first_or_null(db.MUser, function (r) { return r.id_MUser == ManagerUser.id_MUser; });
					if (!MUser.Confirmed && MUser.UserEmail != viewer.Email)
					{
						/* to do удалить если надо старый MUser и надо ли? */
						MUser = insert_or_find_MUser(viewer);
						ManagerUser.id_MUser = MUser.id_MUser;
					}
					ManagerUser.UserName = viewer.Имя;
					ManagerUser.DefaultViewer = viewer.Давать_доступ_ко_всем_процедурам_по_умолчанию;
					db.MProcedureUser = ql._delete(db.MProcedureUser, function (r) { return r.id_ManagerUser == ManagerUser.id_ManagerUser; });
					insert_MProcedureUsers(ManagerUser.id_ManagerUser, viewer.Доступ_к_процедурам);
				}
			}
		}

		service.update = function (id, viewer, args)
		{
			if(args.id_Contract)
			{
				this.update_as_customer(id, viewer)
			}
			else
			{
				this.update_as_managerOrViewer(id, viewer)
			}
		};

		service.read_as_customer = function(id)
		{
			var rows= ql.select(function(r)
			{
				return {
					id_ContractUser:id,
					Аккаунт: {
						Email: r.mu.UserEmail,
						Имя: r.cu.UserName,
						Confirmed: r.mu.Confirmed
					},
					Для_контактов: {
						Контактный_телефон: r.mu.UserContactPhone,
						Telegram: r.mu.UserTelegram,
						WhatsApp: r.mu.UserWhatsApp,
						Viber: r.mu.UserViber,
						Email: r.mu.UserContactEmail
					},
					Для_договора: {
						Исполнитель: r.mu.UserExecutorName,
						ИНН: r.mu.UserINN,
						ОГРНИП: r.mu.UserOGRNIP,
						КПП: r.mu.UserKPP,
						БИК: r.mu.UserBIK,
						Расчетный_счет: r.mu.UserCheckingAccount,
						ОКПО: r.mu.UserOKPO,
						Юридический_адрес: r.mu.UserOfficialAddress,
						Почтовый_адрес: r.mu.UserMailingAddress,
						Телефон: r.mu.UserPhone
					},
					Лица: {
						URL: null == r.mu.UserPhoto ? null : 'forms/ama/datamart/tests/assets/img/photo_vozrozhdeniya.jpg'
						, Название_фото: r.mu.UserPhotoName
					}
				};
			})
			.from(db.ContractUser, 'cu')
			.inner_join(db.MUser, 'mu', function(r) { return r.cu.id_MUser==r.mu.id_MUser})
			.where(function (r) { return id == r.cu.id_ContractUser; })
			.exec();
			return rows[0];
		}

		service.read_as_manager = function (id)
		{
			if(args.id_Contract) {
				this.update_as_customer(id, viewer)
			}else {
				this.update_as_managerOrViewer(id, viewer)
			}
		};

		service.read_as_customer = function(id) {
			var rows= ql.select(function(r)
			{
				return {
					id_ContractUser:id,
					Аккаунт: {
						Email: r.mu.UserEmail,
						Имя: r.cu.UserName,
						Confirmed: r.mu.Confirmed
					},
					Для_контактов: {
						Контактный_телефон: r.mu.UserContactPhone,
						Telegram: r.mu.UserTelegram,
						WhatsApp: r.mu.UserWhatsApp,
						Viber: r.mu.UserViber,
						Email: r.mu.UserContactEmail
					},
					Для_договора: {
						Исполнитель: r.mu.UserExecutorName,
						ИНН: r.mu.UserINN,
						ОГРНИП: r.mu.UserOGRNIP,
						КПП: r.mu.UserKPP,
						БИК: r.mu.UserBIK,
						Расчетный_счет: r.mu.UserCheckingAccount,
						ОКПО: r.mu.UserOKPO,
						Юридический_адрес: r.mu.UserOfficialAddress,
						Почтовый_адрес: r.mu.UserMailingAddress,
						Телефон: r.mu.UserPhone
					},
					Лица: {
						URL: null == r.mu.UserPhoto ? null : 'forms/ama/datamart/tests/assets/img/photo_vozrozhdeniya.jpg'
						, Название_фото: r.mu.UserPhotoName
					}
				};
			})
			.from(db.ContractUser, 'cu')
			.inner_join(db.MUser, 'mu', function(r) { return r.cu.id_MUser==r.mu.id_MUser})
			.where(function (r) { return id == r.cu.id_ContractUser; })
			.exec();
			return rows[0];
		}

		service.read_as_manager = function (id) {
			var rows= ql.select(function(rr){return {
				Имя: rr.mu.UserName
				, ИзвестныйКак: rr.u.UserName
				, Email: rr.u.UserEmail
				, Confirmed: rr.u.Confirmed || 1!=ql.from(db.ManagerUser, 'mu').where(function (rrr) { return rr.mu.id_MUser == rrr.mu.id_MUser; }).rows.length
				, id_ManagerUser: rr.mu.id_ManagerUser
				, Давать_доступ_ко_всем_процедурам_по_умолчанию: rr.mu.DefaultViewer
				, Доступ_к_процедурам: ql.select(function (r) { return h_procedure_types.PrepareSelect2(r); })
					.from(db.MProcedureUser, 'mpu')
					.inner_join(db.MProcedure, 'mp', function (r) { return r.mpu.id_MProcedure == r.mp.id_MProcedure && r.mp.id_Manager == rr.mu.id_Manager; })
					.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
					.where(function (r) { return r.mpu.id_ManagerUser == rr.mu.id_ManagerUser; })
					.exec()
			};})
			.from(db.ManagerUser, 'mu')
			.where(function (r) { return id == r.mu.id_ManagerUser; })
			.inner_join(db.MUser, 'u', function (r) { return r.mu.id_MUser == r.u.id_MUser; })
			.exec();
			return rows[0];
		}

		service.read= function(id, args)
		{
			if(args.id_Contract)
			{
				return this.read_as_customer(id)
			}
			else
			{
				return this.read_as_manager(id)
			}
		};

		service.delete_as_customer = function(ids) {
			var id_arr = ids.split(',');
			for (var i = 0; i < id_arr.length; i++)
			{
				var id = id_arr[i];
				var f = function (r) { return id == r.id_ContractUser; };
				db.ContractUser = ql._delete(db.ContractUser, f);
			}
		}

		service.delete_as_manager = function(ids) {
			var id_arr = ids.split(',');
			for (var i = 0; i < id_arr.length; i++)
			{
				var id = id_arr[i];
				var f = function (r) { return id == r.id_ManagerUser; };
				db.ManagerUser = ql._delete(db.ManagerUser, f);
				db.MProcedureUser = ql._delete(db.MProcedureUser, f);
			}
		}

		service._delete= function(ids,args)
		{
			if(args.id_Contract)
			{
				return this.delete_as_customer(ids)
			}
			else
			{
				return this.delete_as_manager(ids)
			}
		};

		return service;
	}
);