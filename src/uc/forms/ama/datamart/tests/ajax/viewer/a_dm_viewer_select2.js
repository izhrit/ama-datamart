﻿define
([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-select2'
]
, function (d_datamart, ajax_select2)
{
	var service = ajax_select2('ama/datamart?action=viewer.select2');

	service.query = function (q, page, args)
	{
		var maxSize = 15;

		var items = [];
		var i = 0;
		for (; i < d_datamart.ManagerUser.length && items.length < maxSize; i++)
		{
			var item = d_datamart.ManagerUser[i];
			if ((!args.id_Manager || args.id_Manager == item.id_Manager) && -1 != item.UserName.indexOf(q))
				items.push({id:item.id_ManagerUser,text:item.UserName});
		}
		var more = items.length == maxSize && i < d_datamart.ManagerUser.length;

		return { results: items, pagination: { more: more } };
	}

	return service;
});
