﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/h_times'
],
function (ajax_service_base, db, ql, h_codec_datetime, h_times)
{
	var service= ajax_service_base('ama/datamart?action=viewer.register');

	var insert_or_Find_Debtor= function(Должник)
	{
		var debtor = ql.find_first_or_null(db.Debtor, function (d)
		{
			return Должник.ИНН && null != Должник.ИНН && d.INN == Должник.ИНН
				|| Должник.ОГРН && null != Должник.ОГРН && d.OGRN == Должник.ОГРН
				|| Должник.СНИЛС && null != Должник.СНИЛС && d.SNILS == Должник.СНИЛС;
		});

		if (null == debtor)
		{
			debtor = ql.insert_with_next_id(db.Debtor, 'id_Debtor', {
				Name: Должник.Наименование
				, INN: Должник.ИНН
				, OGRN: Должник.ОГРН
				, SNILS: Должник.СНИЛС
			});
		}

		return debtor;
	}

	service.action= function (cx)
	{
		var data_decoded_as_args = cx.data_args();

		var Заявитель = data_decoded_as_args.Заявитель;
		var Запрос = data_decoded_as_args.Запрос_на_доступ;
		var Должник = Запрос.Должник;

		var muser = ql.insert_with_next_id(db.MUser, 'id_MUser', {
			UserEmail: Заявитель.Email
			, UserName: Заявитель.Имя
			, UserPhone: Заявитель.Телефон
			, UserPassword: 'reg_password'
		});

		var debtor = insert_or_Find_Debtor(Должник);

		var TimeLastChange = h_codec_datetime.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()), h_codec_datetime.format.mysql);
		var request = ql.insert_with_next_id(db.Request, 'id_Request', {
			id_MUser: muser.id_MUser
			, id_Debtor: debtor.id_Debtor
			, managerName: Запрос.Процедура.Управляющий
			, State: 'a'
			, TimeLastChange: TimeLastChange
			, Body: {
				Процедура: {
					КогоПредставляетЗаявитель: Запрос.Процедура.КогоПредставляетЗаявитель
				}
				, Пояснения: Запрос.Пояснения
			}
		});

		cx.completeCallback(200, 'success', { text: JSON.stringify({ ok: true }, null, '\t') });
	}

	return service;
});