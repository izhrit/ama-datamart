﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/ql'
]
, function (db, ajax_jqGrid, codec_copy, ql)
{
	var service= ajax_jqGrid('ama/datamart?action=viewer.jqgrid');

	service.jqgrid_for_manager = function (args)
	{
		var ccodec_copy = codec_copy();
		var rows = [];
		for (var i = 0; i< db.ManagerUser.length; i++)
		{
			var ManagerUser = db.ManagerUser[i];
			if (ManagerUser.id_Manager == args.id_Manager)
			{
				var row = {};
				rows.push(row);

				ccodec_copy.CopyFieldsTo(ManagerUser, row);
				row.Procedures = ql.select(function (r){return r.d.Name})
					.from(db.MProcedureUser, 'mpu')
					.where(function (r) { return r.mpu.id_ManagerUser == ManagerUser.id_ManagerUser; })
					.inner_join(db.MProcedure, 'mp', function (r) { return r.mpu.id_MProcedure == r.mp.id_MProcedure && r.mp.id_Manager == ManagerUser.id_Manager; })
					.inner_join(db.Debtor, 'd', function (r) { return r.mp.id_Debtor == r.d.id_Debtor; })
					.exec()
					.join(', ');
			}
		}
		return rows;
	}

	service.jqgrid_for_customer = function (args)
	{
		var rows= ql.select(function (r) { return {
			id_ContractUser : r.cu.id_ContractUser
			,UserName: r.cu.UserName
		};})
		.from(db.ContractUser, 'cu')
		.where(function (r) 
			{
				return r.cu.id_Contract==args.id_Contract;
			}
		)
		.exec();
		return rows;
	}

	service.rows_all = function (url,args)
	{
		if(args.id_Contract)
		{
			return this.jqgrid_for_customer(args)
		}
		else
		{
			return this.jqgrid_for_manager(args)
		}
	}

	return service;
}
);