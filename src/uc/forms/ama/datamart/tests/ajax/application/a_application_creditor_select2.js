﻿define
([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-select2'
	, 'forms/base/ql'
]
, function (db, ajax_select2, ql)
{
	var service = ajax_select2('ama/datamart?action=application.creditor.select2');

	service.query = function (q)
	{
		var maxSize = 15;

		var creditors= [
			{
				id:1
				,text:'ПАУ Сбербанк'
				,data:{
	"Наименование": {
		"Краткое": "Сбер",
		"Полное": "ПАУ Сбербанк"
	},
	"ИНН": "0944375521",
	"ОГРН": "1102110604804",
	"Руководитель": "Артёмов Артём Артёмович",
	"Адрес": {
		"Юридический": "Рязань, Королёва 34-76",
		"Почтовый": "Тамбов, Зёленая 11"
	}
}
			}
			, {
				id:1
				,text:'ПАО Внешторгбанк'
				,data: {
	"Наименование": {
		"Краткое": "ВТБ",
		"Полное": "ПАО Внешторгбанк"
	},
	"ИНН": "1644647342",
	"ОГРН": "1052520669850",
	"Руководитель": "Парамонович Парамон Парамонович",
	"Адрес": {
		"Юридический": "Ульяновск, проспект Кирова 3",
		"Почтовый": "Нижний новгород, ул Подлесная 56-34"
	}
}
			}
		];

		var items= [];
		for (var i = 0; i < creditors.length; i++)
		{
			var item= creditors[i];
			if (-1!=item.text.indexOf(q))
				items.push(item);
		}

		return { results: items };
	}

	return service;
});
