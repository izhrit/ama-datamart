﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
	, 'forms/base/codec/datetime/h_codec.datetime'
], function (db, ajax_jqGrid, ql, h_codec_datetime)
{
var service= ajax_jqGrid('ama/datamart?action=mrequest.jqgrid');

service.rows_all = function (url,args)
{
	if(args.id_Contract) {
		return this.jqgridForContractOrManager(args)
	}else {
		return this.jqgridForSRO(args)
	}
}

service.jqgridForContractOrManager = function(args) {
	var getConsents = function(id_MRequest) {
		var consents= ql.select(function (r) { return {
							id_Contract: r.ps.id_Contract,
							dateOfApplication:   r.ps.DateOfApplication,
							managerName: (r.m.lastName + ' ' + r.m.firstName.charAt(0) + '.' + r.m.middleName.charAt(0) + '.')
						};})
						.from(db.ProcedureStart, 'ps')
						.inner_join(db.Manager, 'm', function (r) { return r.ps.id_Manager == r.m.id_Manager; })
						.where(function (r) { return r.ps.id_MRequest == id_MRequest; })
						.exec();
		var result = {
			amount: consents.length,
			ourPosition: null,
			managerName: null
		}
		consents.sort(function(a, b){
			if(!a.DateOfApplication || !b.DateOfApplication) {
				return 1;
			}
			var time1 = new Date(a.DateOfApplication.replace('T', ' '));
			var time2 = new Date(b.DateOfApplication.replace('T', ' '));
			return time1-time2
		});
		for(var i = 0; i < consents.length; i++) {
			if(consents[i].id_Contract == args.id_Contract) {
				result.ourPosition = i+1; 
				result.managerName = consents[i].managerName; 
				break;
			}
		}
		return result
	}

	var rows= ql.select(function (r) { return {
					id_MRequest: r.mr.id_MRequest
					, DebtorCategory: r.mr.DebtorCategory
					, debtorName: r.mr.DebtorName
					, debtorName2: r.mr.DebtorName2
					, Court: r.c.ShortName
					, DateOfRequestAct: !r.mr.DateOfRequestAct || null == r.mr.DateOfRequestAct || '' == r.mr.DateOfRequestAct ? ''
					: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.mr.DateOfRequestAct)
					, details: null
					, consents: getConsents(r.mr.id_MRequest)
			};})
			.from(db.MRequest, 'mr')
			.inner_join(db.Manager, 'm',  function(r) { return r.mr.id_SRO == r.m.id_SRO; })
			.inner_join(db.Court, 'c', function (r) { return r.mr.id_Court == r.c.id_Court; })
			.where(function(r) {
					return ((args.id_Contract && r.m.id_Contract == args.id_Contract) ||
					(args.id_Manager && r.m.id_Manager == args.id_Manager)) &&
					r.mr.DateOfOffer 
			})
			.order((function(a, b){
				if(!a.mr.DateOfCreation || !b.mr.DateOfCreation) {
					return 1;
				}
				var time1 = new Date(a.mr.DateOfCreation.replace('T', ' '));
				var time2 = new Date(b.mr.DateOfCreation.replace('T', ' '));
				return time2-time1
			}))
			.exec();
	return rows;
}

service.jqgridForSRO = function(args) {

	var customFilterOpt = null
	if(args.customFilter)
		customFilterOpt = JSON.parse(decodeURIComponent(args.customFilter))

	var isExistConsent = function(id_MRequest) {
		var rows = ql.select(function (r) { return {
			id_ProcedureStart: r.ps.id_ProcedureStart
		};})
		.from(db.ProcedureStart, 'ps')
		.where(function (r) {
			return r.ps.id_MRequest == id_MRequest
		})
		.exec();
		if(rows.length) return true
		return false
	}

	var customFilter = function(r) {
		return (customFilterOpt.DateOfOffer.Include ? r.mr.DateOfOffer : true) &&
		(customFilterOpt.DateOfOffer.Exclude ? !r.mr.DateOfOffer : true) &&
		(customFilterOpt.DateOfResponce.Include ? r.mr.DateOfResponce : true) &&
		(customFilterOpt.DateOfResponce.Exclude ? !r.mr.DateOfResponce : true) &&
		(customFilterOpt.Consents.Include ? isExistConsent(r.mr.id_MRequest)===true : true) &&
		(customFilterOpt.Consents.Exclude ? isExistConsent(r.mr.id_MRequest)===false : true)
	}

	var rows= ql.select(function (r) { return {
		id_MRequest: r.mr.id_MRequest
		, debtorName: r.mr.DebtorName
		, debtorName2: r.mr.DebtorName2
		, DebtorCategory: r.mr.DebtorCategory
		, Court: r.c.ShortName
		, DateOfCreation: !r.mr.DateOfCreation || null == r.mr.DateOfCreation || '' == r.mr.DateOfCreation ? ''
		: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.mr.DateOfCreation)
		, DateOfOffer: !r.mr.DateOfOffer || null == r.mr.DateOfOffer || '' == r.mr.DateOfOffer ? ''
					: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.mr.DateOfOffer)
		, Manager: (!r.m || null == r.m) ? null : r.m.lastName + ' ' + r.m.firstName.charAt(0) + '.' + r.m.middleName.charAt(0) + '.'
		, DateOfResponce: !r.mr.DateOfResponce || null == r.mr.DateOfResponce || '' == r.mr.DateOfResponce ? ''
		: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.mr.DateOfResponce)
		, NextSessionDate: !r.mr.NextSessionDate || null==r.mr.NextSessionDate || ''==r.mr.NextSessionDate ? ''
		: h_codec_datetime.mysql_txt2txt_ru_legal().Encode(r.mr.NextSessionDate)
	};})
	.from(db.MRequest, 'mr')
	.inner_join(db.Court, 'c', function (r) { return r.mr.id_Court == r.c.id_Court; })
	.left_join(db.Manager, 'm', function (r) { return r.mr.id_Manager == r.m.id_Manager; })
	.where(function (r) {
		return r.mr.id_SRO == args.id_SRO &&
		(!customFilterOpt || (customFilterOpt && customFilter(r)));
	})
	.order((function(a, b){
		if(!a.mr.DateOfCreation || !b.mr.DateOfCreation) {
			return 1;
		}
		var time1 = new Date(a.mr.DateOfCreation.replace('T', ' '));
		var time2 = new Date(b.mr.DateOfCreation.replace('T', ' '));
		return time2-time1
	}))
	.exec();
	return rows;
}

return service;
});