﻿﻿define([
	'forms/base/codec/datetime/h_codec.datetime'
],
function (h_codec_datetime) {
	var helper = {};

	helper.getManagerSelectFields = function (r) {
		return {
			"АУ": {
				"id": r.m.id_Manager,
				"text": r.m.lastName + ' ' + r.m.firstName + ' ' + r.m.middleName
			},
			"id_ProcedureStart": r.ps.id_ProcedureStart,
			"Заявление_на_банкротство": {
				"Дата_подачи": !r.ps.DateOfApplication || null == r.ps.DateOfApplication || '' == r.ps.DateOfApplication ? ''
					: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.DateOfApplication)
			},
			"Номер_судебного_дела": r.ps.CaseNumber
		}
	}

	helper.fill_row_by_request = function (request, row_fields)
	{
		var fillDebtorCitizenFields = function(Должник, num) {
			num = !num ? '' : num;
			row_fields['DebtorCategory' + num] = 'n';
			var Физ_лицо = Должник.Физ_лицо;
			row_fields['DebtorName' + num] = !Физ_лицо ? null : Физ_лицо.Фамилия + ' ' + Физ_лицо.Имя + ' ' + Физ_лицо.Отчество;
			row_fields['DebtorSNILS' + num] = !Физ_лицо ? null : Физ_лицо.СНИЛС;
		}
		var fillDebtorCommonFields = function(Должник, num) {
			num = !num ? '' : num;
			row_fields['DebtorINN' + num] = Должник.ИНН;
			row_fields['DebtorOGRN' + num] = Должник.ОГРН;
			row_fields['DebtorAddress' + num] = Должник.Адрес;
		}

		if (!row_fields)
			row_fields = {};

		var Должник= request.Должник;
		switch (Должник.Тип)
		{
			case 'Юр_лицо':
				row_fields.DebtorCategory= 'l';
				row_fields.DebtorName= Должник.Юр_лицо.Наименование;
				fillDebtorCommonFields(Должник);
				fillDebtorCitizenFields({}, 2);
				fillDebtorCommonFields({}, 2);
				break;
			case 'Физ_лицо':
				fillDebtorCitizenFields(Должник);
				fillDebtorCommonFields(Должник);
				fillDebtorCitizenFields({}, 2);
				fillDebtorCommonFields({}, 2);
				break;
			case 'Супруги':
				fillDebtorCitizenFields(Должник.Супруги.Должник1);
				fillDebtorCommonFields(Должник.Супруги.Должник1);

				fillDebtorCitizenFields(Должник.Супруги.Должник2, 2);
				fillDebtorCommonFields(Должник.Супруги.Должник2, 2)
				break;
		}

		if(request.Заявитель) {
			var Заявитель= request.Заявитель
			if(request.Заявитель.Заявитель_должник===false) {
				row_fields.ApplicantCategory= null;
				row_fields.ApplicantName= null;
				row_fields.ApplicantSNILS= null;
				row_fields.ApplicantINN= null;
				row_fields.ApplicantOGRN= null;
				row_fields.ApplicantAddress= null;
			} else {
				switch (Заявитель.Тип)
				{
					case 'Юр_лицо':
						row_fields.ApplicantCategory= 'l';
						row_fields.ApplicantName= Заявитель.Юр_лицо.Наименование;
						break;
					case 'Физ_лицо':
						row_fields.ApplicantCategory= 'n';
						var Физ_лицо= Заявитель.Физ_лицо;
						row_fields.ApplicantName= Физ_лицо.Фамилия + ' ' + Физ_лицо.Имя + ' ' + Физ_лицо.Отчество;
						row_fields.ApplicantSNILS= Физ_лицо.СНИЛС;
						break;
				}
				row_fields.ApplicantINN= Заявитель.ИНН;
				row_fields.ApplicantOGRN= Заявитель.ОГРН;
				row_fields.ApplicantAddress= Заявитель.Адрес;
			}
		}

		row_fields.CaseNumber= request.Номер_судебного_дела;

		var заявление= request.Заявление_на_банкротство;
		if (заявление.В_суд)
		{
			row_fields.id_Court= (!заявление.В_суд || null==заявление.В_суд) ? null : заявление.В_суд.id;
		}
		row_fields.NextSessionDate= !заявление.Дата_рассмотрения || null==заявление.Дата_рассмотрения || ''==заявление.Дата_рассмотрения 
				? null : h_codec_datetime.mysql_txt2txt_ru_legal().Decode(заявление.Дата_рассмотрения);

		var запрос= request.Запрос;
		if(запрос.Время) {

			row_fields.DateOfRequest= !запрос.Время.получения || null == запрос.Время.получения || '' == запрос.Время.получения ? ''
			: h_codec_datetime.mysql_txt2txt_ru_date().Decode(запрос.Время.получения)
			row_fields.DateOfOffer= !запрос.Время.опроса || null == запрос.Время.опроса || '' == запрос.Время.опроса ? ''
			: h_codec_datetime.mysql_txt2txt_ru_date().Decode(запрос.Время.опроса)
			row_fields.DateOfResponce= !запрос.Время.ответа || null ==запрос.Время.ответа || '' == запрос.Время.ответа ? ''
			: h_codec_datetime.mysql_txt2txt_ru_date().Decode(запрос.Время.ответа)
			if(запрос.Ссылка) {
				row_fields.CourtDecisionURL= запрос.Ссылка;
				row_fields.DateOfRequestAct= !запрос.Время.акта || null == запрос.Время.акта || '' == запрос.Время.акта ? ''
				: h_codec_datetime.mysql_txt2txt_ru_date().Decode(запрос.Время.акта)
				row_fields.CourtDecisionAddInfo= запрос.Дополнительная_информация
			}
		}
		
		return row_fields;
	}

	return helper;
});