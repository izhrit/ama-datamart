﻿define
([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/ajax/mrequest/h_a_dm_mrequest'
	, 'forms/base/ql'
]
	, function (db, ajax_service_base, h_a_dm_mrequest, ql)
{
		var service = ajax_service_base('ama/datamart?action=mrequest.select2');

		var substringMatcher = function (records) {
			return function findMatches(q, cb) {
				var matches, substrRegex;

				matches = [];

				substrRegex = new RegExp(q, 'i');

				$.each(records, function (i, record) {
					if (substrRegex.test(record.АУ.text)) {
						matches.push(record);
					}
				});
				cb(matches);
			};
		};

		service.action = function (cx) {
			var args = cx.url_args(); // аргументы командной строки
			var rows = ql.select(function (r) { return h_a_dm_mrequest.getManagerSelectFields(r) })
				.from(db.ProcedureStart, 'ps')
				.inner_join(db.Manager, 'm', function (r) { return r.ps.id_Manager == r.m.id_Manager; })
				.where(function (r) {
					return (!r.ps.id_MRequest) &&
					((!args.id_SRO || r.m.id_SRO == args.id_SRO) &&
					(r.ps.ShowToSRO == 1 || r.ps.AddedBySRO == 1));
				})
				.exec();
			if (!args.query) {
				cx.completeCallback(200, 'success', { text: JSON.stringify(rows) });
            }
			var res = []
			substringMatcher(rows)(args.query, function (matches) { res = matches })

			cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
		}

		

	return service;
});
