define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/ama/datamart/tests/ajax/mrequest/h_a_dm_mrequest'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/h_times'
]
	, function (db, ajax_CRUD, ql, h_a_dm_mrequest, h_codec_datetime, h_times) {
		var service = ajax_CRUD('ama/datamart?action=mrequest.crud');

		var getDebtorObject = function (r, num) {
			var DebtorCategory = 'DebtorCategory' + (num || '');
			var DebtorName = 'DebtorName' + (num || '');
			var DebtorSNILS = 'DebtorSNILS' + (num || '');
			var DebtorINN = 'DebtorINN' + (num || '');
			var DebtorOGRN = 'DebtorOGRN' + (num || '');
			var DebtorAddress = 'DebtorAddress' + (num || '');
	
			var Должник = { ИНН: r.mr[DebtorINN] || '', ОГРН: r.mr[DebtorOGRN] || '', Адрес: r.mr[DebtorAddress] || '' };
			switch (r.mr[DebtorCategory])
			{
				case 'l':
					Должник.Тип= "Юр_лицо";
					Должник.Юр_лицо= { Наименование: r.mr[DebtorName] };
					break;
				case 'n':
					var p= r.mr[DebtorName].split(' ');
					Должник.Тип= "Физ_лицо";
					Должник.Физ_лицо= { Фамилия: p[0], Имя: p[1], Отчество: p[2], СНИЛС: r.mr[DebtorSNILS] || '' };
					break;
			}
	
			return Должник;
		}

		service.create = function (request, args) {
			var id_SRO = parseInt(args.id_SRO);
			var row_fields= h_a_dm_mrequest.fill_row_by_request(request);

			var В_отчет = null;
			if (request.Согласия_АУ && request.Согласия_АУ.В_отчет) {
				В_отчет = request.Согласия_АУ.В_отчет;
	
				var r = ql.find_first_or_null(db.ProcedureStart, function (r) { return r.id_ProcedureStart == В_отчет; });
				if (r) { row_fields.id_Manager = r.id_Manager; r.id_SRO = id_SRO }
			}

			row_fields.id_SRO = id_SRO;
			row_fields.DateOfCreation = h_times.stringifyDateUTC(null, h_times.safeDateTime())
	
			mrequest = ql.insert_with_next_id(db.MRequest, 'id_MRequest', row_fields);

			if (request.Согласия_АУ && request.Согласия_АУ.Согласия && mrequest) { // сопостовляем всем выбранным согласиям созданное дело
				for (var i = 0; i < request.Согласия_АУ.Согласия.length; i++) {
					var id_ProcedureStart = request.Согласия_АУ.Согласия[i].id_ProcedureStart;

					var r = ql.find_first_or_null(db.ProcedureStart, function (r) { return r.id_ProcedureStart == id_ProcedureStart; });
					if (r) { r.id_MRequest = mrequest.id_MRequest }
				}
			}
			return {
				data: {
					id: mrequest.id_MRequest
				}
			}
		}

		service.update = function (id, request, args) {
			var id_Manager = null;
			var В_отчет = null;
			if (request.Согласия_АУ) {
				if (request.Согласия_АУ.В_отчет) { // берем  id АУ из выбранного согласия
					var В_отчет = request.Согласия_АУ.В_отчет;
					var r = ql.find_first_or_null(db.ProcedureStart, function (r) { return r.id_ProcedureStart == В_отчет; });
					if (r) { id_Manager = r.id_Manager; r.id_SRO = args.id_SRO; }
				}
				var r = ql.find_all_or_null(db.ProcedureStart, function (r) { return r.id_MRequest == id; });
				if (request.Согласия_АУ.Согласия) { // сопоставляем всем выбранным согласиям редактируемое дело
					if(r) {
						for(var i = 0; i < r.length; i++) {
							var isConsentInNewConsents = false
							for (var j = 0; j < request.Согласия_АУ.Согласия.length; j++) {
								var id_ProcedureStart = request.Согласия_АУ.Согласия[j].id_ProcedureStart;
								if(r[i].id_ProcedureStart == id_ProcedureStart)
									isConsentInNewConsents = true
							}
							if(!isConsentInNewConsents) {
								var idConsentToNull = r[i].id_ProcedureStart
								var c = ql.find_first_or_null(db.ProcedureStart, function (r) { return r.id_ProcedureStart == idConsentToNull; });
								if (c) { c.id_MRequest = null }
							}
						}
					}

					for (var i = 0; i < request.Согласия_АУ.Согласия.length; i++) {
						var id_ProcedureStart = request.Согласия_АУ.Согласия[i].id_ProcedureStart;
						var r = ql.find_first_or_null(db.ProcedureStart, function (r) { return r.id_ProcedureStart == id_ProcedureStart; });
						if (r) { r.id_MRequest = id }
					}

				}else {
					if(r) {
						for(var i = 0; i < r.length; i++) {
							r[i].id_MRequest = null
						}
					}
				}
			}
			var row_MRequest = ql.find_first_or_null(db.MRequest, function (mr) { return id==mr.id_MRequest; });
			row_MRequest.id_Manager= id_Manager;
			h_a_dm_mrequest.fill_row_by_request(request, row_MRequest);
		}

		service.read = function (id, args) {
			var getAUs = function (id_MRequest) {
				var rows = ql.select(function (r) {
					$ps = {
						"АУ": {
							"id": r.m.id_Manager,
							"text": r.m.lastName + ' ' + r.m.firstName + ' ' + r.m.middleName
						},
						"id_ProcedureStart": r.ps.id_ProcedureStart,
						"Заявление_на_банкротство": {
							"Дата_подачи": !r.ps.DateOfApplication || null == r.ps.DateOfApplication || '' == r.ps.DateOfApplication ? ''
								: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.ps.DateOfApplication)
						},
						"Номер_судебного_дела": r.ps.CaseNumber
					}
					if(r.ps.id_SRO) $ps.В_ответе = r.ps.id_ProcedureStart;
					return $ps;
				})
					.from(db.ProcedureStart, 'ps')
					.left_join(db.Manager, 'm', function (r) { return r.ps.id_Manager == r.m.id_Manager; })
					.where(function (r) {
						return id_MRequest == r.ps.id_MRequest
					})
					.exec();
				return rows
			}

			var rows = ql.select(function (r) {
				var Заявитель= { ИНН: r.mr.ApplicantINN, ОГРН: r.mr.ApplicantOGRN, Адрес: r.mr.ApplicantAddress  };
				var request = {
					"id_MRequest": r.mr.id_MRequest
					, "id_SRO": r.mr.id_SRO
					, "Заявитель": Заявитель
					, "Заявление_на_банкротство": {
						В_суд: ((null == r.c) ? null : { id: r.c.id_Court, text:r.c.Name })
						, Дата_рассмотрения: !r.mr.NextSessionDate || null==r.mr.NextSessionDate || ''==r.mr.NextSessionDate ? ''
							: h_codec_datetime.mysql_txt2txt_ru_legal().Encode(r.mr.NextSessionDate)
					}
					,"Номер_судебного_дела":r.mr.CaseNumber
					, "Запрос": {
						"Ссылка": r.mr.CourtDecisionURL
						, "Время": {
							"получен": r.mr.DateOfRequest ? true : false,
							"получения": !r.mr.DateOfRequest || null == r.mr.DateOfRequest || '' == r.mr.DateOfRequest ? ''
								: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.mr.DateOfRequest),
							"опрошен_ау": r.mr.DateOfOffer ? true : false,
							"опроса": !r.mr.DateOfOffer || null == r.mr.DateOfOffer || '' == r.mr.DateOfOffer ? ''
							: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.mr.DateOfOffer),
							"отвечен": r.mr.DateOfResponce ? true : false,
							"ответа": !r.mr.DateOfResponce || null == r.mr.DateOfResponce || '' == r.mr.DateOfResponce ? ''
								: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.mr.DateOfResponce),
							"акта": !r.mr.DateOfRequestAct || null == r.mr.DateOfRequestAct || '' == r.mr.DateOfRequestAct ? ''
							: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.mr.DateOfRequestAct)
						}
						,'Дополнительная_информация': r.mr.CourtDecisionAddInfo
					}
					,"Запрос_дополнительная_информация": {
						'Дополнительная_информация': r.mr.CourtDecisionAddInfo
					}
					, "Согласия_АУ": {
						"Согласия": getAUs(r.mr.id_MRequest),
						"В_отчет": !r.mr ? null : r.mr.id_Manager
					}
				}

				if(r.mr.DebtorName2) {
					request.Должник = { Тип: 'Супруги', Супруги: { Должник1: getDebtorObject(r), Должник2: getDebtorObject(r, 2) } };
				} else {
					request.Должник = getDebtorObject(r);
				}

				switch (r.mr.ApplicantCategory)
				{
					case 'l':
						Заявитель.Тип= "Юр_лицо";
						Заявитель.Юр_лицо= { Наименование: r.mr.ApplicantName };
						break;
					case 'n':
						var p= r.mr.ApplicantName.split(' ');
						Заявитель.Тип= "Физ_лицо";
						Заявитель.Физ_лицо= { Фамилия: p[0], Имя: p[1], Отчество: p[2], СНИЛС: r.mr.ApplicantSNILS };
						break;						
				}
				if(!r.mr.ApplicantName) {
					request.Заявитель = {Заявитель_должник: true}
				}
				return request
			})
				.from(db.MRequest, 'mr')
				.inner_join(db.Court, 'c', function (r) { return r.mr.id_Court == r.c.id_Court })
				.left_join(db.ProcedureStart, 'ps', function (r) { return r.mr.id_Manager == r.ps.id_Manager })
				.where(function (r) { return r.mr.id_MRequest == id && (!args.id_SRO || r.mr.id_SRO == args.id_SRO); })
				.exec();
			return rows[0];
		}

		service._delete = function (ids) {
			var id_arr = ids.split(',');
			for (var i = 0; i < id_arr.length; i++) {
				var id = id_arr[i];
				db.MRequest = ql._delete(db.MRequest, function (r) { return id == r.id_MRequest; });
			}
		}

		return service;
	});