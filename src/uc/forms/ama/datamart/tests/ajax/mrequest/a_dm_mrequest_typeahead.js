﻿define
([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/ajax/mrequest/h_a_dm_mrequest'
	, 'forms/base/ql'
	, 'forms/base/codec/datetime/h_codec.datetime'
]
	, function (db, ajax_service_base, h_a_dm_mrequest, ql, h_codec_datetime)
{
		var service = ajax_service_base('ama/datamart?action=mrequest.typeahead');

		var substringMatcher = function (records) {
			return function findMatches(q, cb) {
				var matches, substrRegex;

				matches = [];

				substrRegex = new RegExp(q, 'i');
				
				$.each(records, function (i, record) {
					if (substrRegex.test(record.name)) {
						matches.push(record);
					}
				});
				cb(matches);
			};
		};

		var getAUs = function (idSRO, DebtorName) {
			var rows = ql.select(function (r) { return h_a_dm_mrequest.getManagerSelectFields(r) })
				.from(db.ProcedureStart, 'ps')
				.inner_join(db.Manager, 'm', function (r) { return r.ps.id_Manager == r.m.id_Manager; })
				.where(function (r) {
					return (!idSRO || (r.m.id_SRO == idSRO  && (r.ps.ShowToSRO==1 || r.ps.AddedBySRO==1))) && (r.ps.DebtorName == DebtorName)
				})
				.exec();
			return rows
		}
		
		var getDebtorObject = function (r, num) {
			var DebtorCategory = 'DebtorCategory' + (num || '');
			var DebtorName = 'DebtorName' + (num || '');
			var DebtorSNILS = 'DebtorSNILS' + (num || '');
			var DebtorINN = 'DebtorINN' + (num || '');
			var DebtorOGRN = 'DebtorOGRN' + (num || '');
			var DebtorAddress = 'DebtorAddress' + (num || '');

			var Должник = { ИНН: r.ps[DebtorINN] || '', ОГРН: r.ps[DebtorOGRN] || '', Адрес: r.ps[DebtorAddress] || '' };
			switch (r.ps[DebtorCategory])
			{
				case 'l':
					Должник.Тип= "Юр_лицо";
					Должник.Юр_лицо= { Наименование: r.ps[DebtorName] };
					break;
				case 'n':
					var p= r.ps[DebtorName].split(' ');
					Должник.Тип= "Физ_лицо";
					Должник.Физ_лицо= { Фамилия: p[0], Имя: p[1], Отчество: p[2], СНИЛС: r.ps[DebtorSNILS] || '' };
					break;
			}
	
			return Должник;
		}

		service.action = function (cx) {
			var args = cx.url_args(); // аргументы командной строки
			var rows = ql.select(function (r) {
				var Заявитель= { ИНН: r.ps.ApplicantINN, ОГРН: r.ps.ApplicantOGRN, Адрес: r.ps.ApplicantAddress  };
				var request = {
					Заявитель: Заявитель
					, Заявление_на_банкротство: {
						В_суд: ((null == r.c) ? null : { id: r.c.id_Court, text:r.c.Name })
						, Дата_рассмотрения: !r.ps.NextSessionDate || null==r.ps.NextSessionDate || ''==r.ps.NextSessionDate ? ''
							: h_codec_datetime.mysql_txt2txt_ru_legal().Encode(r.ps.NextSessionDate)
					}
					, Номер_судебного_дела: r.ps.CaseNumber
					, Запрос: {
						Ссылка: r.ps.CourtDecisionURL
						, Время: {
							получен: ''
							, получения: ''
							, опрошен_ау: ''
							, опроса: ''
							, отвечен: ''
							, ответа: ''
							, акта: !r.ps.DateOfRequestAct || null==r.ps.DateOfRequestAct || ''==r.ps.DateOfRequestAct ? ''
							: h_codec_datetime.mysql_txt2txt_ru_legal().Encode(r.ps.DateOfRequestAct)
						}
						, Дополнительная_информация: r.ps.CourtDecisionAddInfo
					}
					, Запрос_дополнительная_информация: {
						Дополнительная_информация:  r.ps.CourtDecisionAddInfo
					}
					, Согласия_АУ: {
						Согласия: getAUs(args.id_SRO, r.ps.DebtorName),
						В_отчет: r.ps.id_Manager
					}
				}

				if(args.isMarriedCouple) {
					request.Должник = { Тип: 'Супруги' };
					request.Должник.Супруги = { Должник1: getDebtorObject(r), Должник2: getDebtorObject(r, 2) };
				} else {
					request.Должник = getDebtorObject(r);
				}

				switch (r.ps.ApplicantCategory)
				{
					case 'l':
						Заявитель.Тип= "Юр_лицо";
						Заявитель.Юр_лицо= { Наименование: r.ps.ApplicantName };
						break;
					case 'n':
						var p= r.ps.ApplicantName.split(' ');
						Заявитель.Тип= "Физ_лицо";
						Заявитель.Физ_лицо= { Фамилия: p[0], Имя: p[1], Отчество: p[2], СНИЛС: r.ps.ApplicantSNILS };
						break;						
				}
				if(!r.ps.ApplicantCategory) {
					request.Заявитель = {Заявитель_должник: true}
				}

				return {
					'name': r.ps.DebtorName
					, 'category': r.ps.DebtorCategory

					, 'inn': r.ps.DebtorINN
					, 'ogrn': r.ps.DebtorOGRN
					, 'snils': r.ps.DebtorSNILS

					, 'caseNumber': r.ps.CaseNumber
					, 'courtName': r.c.Name
					
					, 'Request': request
				};
			})
				.from(db.ProcedureStart, 'ps')
				.inner_join(db.Court, 'c', function (r) { return r.ps.id_Court == r.c.id_Court; })
				.inner_join(db.Manager, 'm', function(r) { return r.ps.id_Manager == r.m.id_Manager })
				.where(function (r) {
					return (((r.ps.AddedBySRO==1 || r.ps.ShowToSRO==1) && (r.m.id_SRO == args.id_SRO)) || !args.id_SRO)
						&& (r.ps.DebtorCategory == args.debtorCategory)
						&& (args.isMarriedCouple ? r.ps.DebtorName2 : !r.ps.DebtorName2);
				})
				.exec();
			var res = []
			substringMatcher(rows)(args.query, function (matches) { res = matches })

			cx.completeCallback(200, 'success', { text: JSON.stringify(res) });

		}

	return service;
});
