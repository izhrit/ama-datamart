﻿define(['forms/base/ajax/ajax-jqGrid']
, function (ajax_jqGrid)
{
	var service = ajax_jqGrid('ama/datamart?action=efrsb.jqgrid');

	service.rows_all = function ()
	{
		return [
			 { PublishDate: '02.02.2012 11:00', MessageType: 'a', Number: '2' }
			,{ PublishDate: '03.03.2013', MessageType: 'b', Number:'3' }
			,{ PublishDate: '04.04.2014', MessageType: 'c', Number:'4' }
		];
	}

	return service;
});