﻿define
([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-select2'
	, 'forms/base/ql'
]
, function (db, ajax_select2, ql)
{
	var service = ajax_select2('ama/datamart?action=efrsb.debtor.select2');

	service.query = function (q)
	{
		var maxSize = 15;

		var items= ql.select(function (r) { return {
			id: r.d.BankruptId
			,text: r.d.Name
			,data: {
				Должник: {
					Наименование: r.d.Name
					, ИНН: r.d.INN
					, ОГРН: r.d.OGRN
					, СНИЛС: r.d.SNILS
				}
				, АУ: {
					Фамилия: r.m.LastName
					, Имя: r.m.FirstName
					, Отчество: r.m.MiddleName
					, НомерЕФРСБ: r.m.RegNum
				}
			}
		};})
		.from(db.mock_efrsb_debtor, 'd')
		.left_join(db.mock_efrsb_manager, 'm', function (r) { return r.d.ArbitrManagerID == r.m.ArbitrManagerID; })
		.where(function (r)
		{
			return -1 != r.d.Name.indexOf(q)
		})
		.limit(maxSize)
		.exec();

		return { results: items };
	}

	return service;
});
