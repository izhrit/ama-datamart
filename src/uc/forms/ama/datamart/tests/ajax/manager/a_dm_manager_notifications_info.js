﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
],
function (ajax_service_base, db, ql)
{
	var service = ajax_service_base('ama/datamart?action=manager.notifications.info');

	service.action= function (cx)
	{
		var args = cx.url_args();
		var id = args.id;
		
		var res = null;
		var EmailInfo = ql.find_first_or_null(db.SentEmail, function (e) { return e.id_SentEmail == id; });
		var EmailInfo = ql.select(function (r) {
			return {
				  firstName: r.m.firstName
				, lastName: r.m.lastName
				, middleName: r.m.middleName
				, EmailType: r.se.EmailType
				, TimeDispatch: r.se.TimeDispatch
				, TimeSent: r.se.TimeSent
				, RecipientEmail: r.se.RecipientEmail
				, Body: r.se.ExtraParams.Body
				, Subject: r.se.ExtraParams.Subject
			};
		})
		.from(db.SentEmail, 'se')
		.inner_join(db.Manager, "m", function (r) { return r.se.RecipientId == r.m.id_Manager;})
		.where(function (r) { return r.se.id_SentEmail == id; })
		.exec();

		var EmailInfo = 0 == EmailInfo.length ? null : EmailInfo[0];

		cx.completeCallback(200, 'success', { text: JSON.stringify(EmailInfo) });
		
	}

	return service;
});