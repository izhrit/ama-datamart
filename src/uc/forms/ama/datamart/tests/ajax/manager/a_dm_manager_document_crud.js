﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/datamart?action=manager.document.ru');
	service.read = function(id)
	{
		var rows= ql
			.select(function (r) {
				var res= {
					id_ManagerDocument: r.d.id_ManagerDocument
					, FileName: r.d.FileName
					, DocumentType: r.d.DocumentType
				};
				return res;
			})
			.from(db.ManagerDocument, 'd')
			.inner_join(db.Manager, 'm', function (r) { return r.d.id_Manager == r.m.id_Manager; })
			.where(function (r) { return r.m.id_Manager == id; })
			.exec();
		return rows;
	}

	service.create = function (id, args) {
		return 3;
	}

	service._delete = function (id) {
		db.ManagerDocument = ql._delete(db.ManagerDocument, function (d) { return d.id_ManagerDocument == id; });
	}

	return service;
});