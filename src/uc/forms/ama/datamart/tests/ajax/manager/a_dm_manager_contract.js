﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
],
function (ajax_service_base, db, ql)
{
	var service = ajax_service_base('ama/datamart?action=manager.contract');

	service.action= function (cx)
	{
		var args= cx.url_args();
		var res = null;
		var reason = null;

		var Manager = ql.find_first_or_null(db.Manager, function (m) { return m.id_Manager == args.id_Manager; });

		if (null!=Manager)
		{
			switch (args.cmd)
			{
				case 'unregister':
					Manager.id_Contract = null;
					res = true;
					break;
				case 'decline':
					Manager.id_Contract = null;
					Manager.id_Contract_Change = null;
					res = true;
					break;
				case 'register':
						var Manager_post = cx.data_args();
						if (Manager_post.manager_contract_password) {
							var Contract = ql.find_first_or_null(db.mock_crm2_Contract, function (c) { return c.ContractNumber == Manager_post.manager_new_contract && c.Password == Manager_post.manager_contract_password; });
							if (Contract && null != Contract) {
								Manager.id_Contract = Contract.Id
								Manager.id_Contract_Change = null
								res = true
							} else {
								res = false;
								reason = 'Неверный номер договора и/или пароль'
							}
						} else if (args.category == 'contract') {
							var Contract = ql.find_first_or_null(db.mock_crm2_Contract, function (c) { return c.ContractNumber == Manager_post.manager_new_contract });
							if (Contract && null != Contract) {
								Manager.id_Contract = Contract.Id
								Manager.id_Contract_Change = null
								res = true
							} else {
								res = false;
								reason = 'Данный договор отсутствует в базе данных'
							}
						}else {
							var Contract = ql.find_first_or_null(db.mock_crm2_Contract, function (c) { return c.ContractNumber == Manager_post.manager_new_contract });
							if (Contract && null != Contract) {
								Manager.id_Contract = null
								Manager.id_Contract_Change = Contract.Id
								res = true
							} else {
								res = false;
								reason = 'Данный договор отсутствует в базе данных'
							}
						}
					break;
			}
		}
		cx.completeCallback(200, 'success', { text: JSON.stringify({ ok: res, reason: reason }) });
	}

	return service;
});