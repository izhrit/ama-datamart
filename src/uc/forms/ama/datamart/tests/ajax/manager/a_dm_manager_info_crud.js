﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
	, 'forms/base/codec/datetime/h_codec.datetime'
]
, function (db, ajax_CRUD, ql, h_codec_datetime)
{
	var service = ajax_CRUD('ama/datamart?action=manager.info');
	service.read = function(id)
	{
		var rows= ql
			.select(function (r) {
				var res= {
					id_Manager: r.m.id_Manager
					, Протокол: {
						Номер: r.m.ProtocolNum,
						Дата: !r.m.ProtocolDate || null == r.m.ProtocolDate || '' == r.m.ProtocolDate ? ''
						: h_codec_datetime.mysql_txt2txt_ru_date().Encode(r.m.ProtocolDate)
					}
				};
				return res;
			})
			.from(db.Manager, 'm')
			.where(function (r) { return r.m.id_Manager == id; })
			.exec();
		return rows[0];
	}

	service.update = function (id, manager_info) {
		var row_Manager = ql.find_first_or_null(db.Manager, function (m) { return id==m.id_Manager; });
		row_Manager.ProtocolNum = manager_info.Протокол.Номер;
		row_Manager.ProtocolDate = manager_info.Протокол.Дата;
	}

	return service;
});