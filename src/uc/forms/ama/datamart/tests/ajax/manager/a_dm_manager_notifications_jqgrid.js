﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
], function (db, ajax_jqGrid, ql)
{
	var service = ajax_jqGrid('ama/datamart?action=manager.notifications.jqgrid');

	service.rows_all = function (url, args)
	{
		return ql.select(function (r) { return {
			  id_SentEmail: r.se.id_SentEmail
			, RecepientName: (!r.m || null == r.m) ? '' : (r.m.lastName + ' ' + r.m.firstName.charAt(0) + '.' + r.m.middleName.charAt(0) + '.')
			, RecipientEmail: r.se.RecipientEmail
			, EmailType: r.se.EmailType
			, TimeDispatch: r.se.TimeDispatch
			, TimeSent: r.se.TimeSent
		};})
		.from(db.SentEmail,"se")
		.inner_join(db.Manager, "m", function (r) { return r.se.RecipientId==r.m.id_Manager; })
		.where(function (r) { return r.se.RecipientType == 'm' && r.se.EmailType == 'y' && r.m.id_SRO == args.id_SRO; }) 
		.exec();
	}

	return service;
});
