﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/datamart?action=manager.sro.ru');

	service.action = function (cx) {
		var args = cx.url_args();
		var self = this;
		
		var res = null;
		var Manager = ql.find_first_or_null(db.Manager, function (m) { return m.ArbitrManagerID == args.id; });
		if (null != Manager) {
			switch (args.cmd) {
				case 'update':
					Manager.ManagerEmail_Change = args.email;
					Manager.ManagerPassword_Change = args.id;
					res = { ok: true };
					break;
				case 'get':
					res = read(args.id);
					break;
			}
		}
		var txt_res = JSON.stringify(res);
		cx.completeCallback(200, 'success', { text: txt_res });
	}

	var read = function(id)
	{
		var rows= ql
			.select(function (r) {
				var res= {
					id_Manager: r.m.id_Manager
					, firstName: r.m.firstName
					, lastName: r.m.lastName
					, middleName: r.m.middleName
					, efrsbNumber: r.m.efrsbNumber
					, INN: r.m.INN
					, Email: r.m.ManagerEmail
					, EmailChange: r.m.ManagerEmail_Change
					, EfrsbEmail: (!r.e || null == r.e) ? '' : r.e.EMail
					, ArbitrManagerID: r.m.ArbitrManagerID
					, PasswordEnabled: (r.m.Password && null != r.m.Password)
					, PasswordSent: (r.m.ManagerEmail_Change && null != r.m.ManagerEmail_Change && r.m.ManagerPassword_Change && null != r.m.ManagerPassword_Change)
				};
				return res;
			})
			.from(db.Manager, 'm')
			.left_join(db.efrsb_manager, 'e', function (r) { return r.m.ArbitrManagerID == r.e.ArbitrManagerID; })
			.where(function (r) { return r.m.ArbitrManagerID == id; })
			.exec();
		return rows[0];
	}

	return service;
});