﻿define([
	'forms/base/ajax/ajax-select2'
	,'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
]
, function (ajax_select2, db, ql)
{
	var service= ajax_select2('ama/datamart?action=efrsb-manager.select2');

	service.query = function (q, page, args)
	{
		var items= ql.select(function (r) { return {
			id: r.m.id_Manager
			,text: r.m.LastName + ' ' + r.m.FirstName + ' ' + r.m.MiddleName
		};})

		.from(db.efrsb_manager, 'm')
		.where(function (r) 
			{ 
				return args.RegNum ? args.RegNum==r.m.SRORegNum 
					: true ;
			}
		)
		.exec();
		return { results: items };
	}

	return service;
});