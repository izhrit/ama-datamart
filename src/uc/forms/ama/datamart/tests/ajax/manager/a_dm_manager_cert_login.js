﻿define([
	'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
	, 'forms/ama/datamart/base/h_cryptoapi'
]
, function (ajax_service_base, db, ql, h_cryptoapi)
{
	var service = ajax_service_base("ama/datamart?action=manager.cert.login");

	service.GetTokenToSign = function (data) {
		var subject = h_cryptoapi.GetDNFields(data.Subject);
		var Manager = ql.find_first_or_null(db.Manager, function (m) { return m.INN == subject.ИНН; });

		var efrsb_manager = ql.find_first_or_null(db.efrsb_manager, function (m) { return m.INN == subject.ИНН; });

		if (null != Manager) {
			var token = {
				token: 'exists'
			}
			return token;
		}
		else if (null != efrsb_manager) {
			var token = {
				token: 'test_auth_token_to_sign_for_id_Manager_from_efrsb_' + efrsb_manager.id_Manager
			}
			return token;
		} else {
			return null;
		}
	}

	// здесь проверки не происходит, т.к. браузер блокирует запросы к другим доменным именам
	service.AuthByTokenSignature = function (data) {
		var signature = data.base64_encoded_signature;
		var parts = signature.split('_');

		var id_Manager = parts[parts.length - 1];

		var Manager = ql.find_first_or_null(db.Manager, function (m) { return m.id_Manager == id_Manager; });

		var efrsb_manager = ql.find_first_or_null(db.efrsb_manager, function (m) { return m.id_Manager == id_Manager; });

		if (null != Manager) {
			var auth_info = {
				id_Manager: id_Manager
				, ArbitrManagerID: Manager.ArbitrManagerID

				, Фамилия: Manager.LastName
				, Имя: Manager.FirstName
				, Отчество: Manager.MiddleName
				, ИНН: Manager.INN
				, efrsbNumber: Manager.RegNum
			}
		}
		else if (null != efrsb_manager) {
			var auth_info = {
				id_Manager: id_Manager
				, ArbitrManagerID: efrsb_manager.ArbitrManagerID

				, Фамилия: efrsb_manager.LastName
				, Имя: efrsb_manager.FirstName
				, Отчество: efrsb_manager.MiddleName
				, ИНН: efrsb_manager.INN
				, efrsbNumber: efrsb_manager.RegNum
			}
		} else {
			return null;
		}
		return auth_info;
	}

	service.action = function (cx) {
		var parsed_args = cx.url_args();
		switch (parsed_args.cmd) {
			case 'get-token-to-sign':
				var token = this.GetTokenToSign(cx.originalOptions.data);
				cx.completeCallback(200, 'success', { text: JSON.stringify({ token: token }) });
				return;
			case 'auth-by-token-signature':
				var res = this.AuthByTokenSignature(cx.originalOptions.data);
				cx.completeCallback(200, 'success', { text: JSON.stringify(res, null, '\t') });
				return;
			case 'logout':
				cx.completeCallback(200, 'success', { text: JSON.stringify({ ok: true }, null, '\t') });
				return;
		}
		var error_msg = 'unknown cmd=' + parsed_args.cmd + '!';
		completeCallback(400, 'bad request', { text: error_msg });
	};

	return service;
});