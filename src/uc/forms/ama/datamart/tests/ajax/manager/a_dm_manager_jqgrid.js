﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
], function (db, ajax_jqGrid, ql)
{
	var service = ajax_jqGrid('ama/datamart?action=manager.jqgrid');

	service.rows_all = function (url,args)
	{
		return ql.select(function (r) { return {
				id_Manager : r.m.id_Manager
				, firstName: r.m.firstName
				, lastName: r.m.lastName
				, middleName: r.m.middleName
				, efrsbNumber: r.m.efrsbNumber
				, ArbitrManagerID: r.m.ArbitrManagerID
				, INN: r.m.INN
				, registration: r.m.id_Contract ? 'Прописан' : 'На прописку'
			};})
			.from(db.Manager, 'm')
			.where(function (r)
				{ 
					return args.id_Contract 
						? r.m.id_Contract == args.id_Contract || r.m.id_Contract_Change == args.id_Contract
						: r.m.id_SRO == args.id_SRO;
				})
			.exec();
	}

	return service;
});