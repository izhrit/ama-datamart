﻿define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
],
function (ajax_service_base, db, ql)
{
	var service = ajax_service_base('ama/datamart?action=manager.password');

	service.action= function (cx)
	{
		var args= cx.url_args();
		var res = null;
		var Manager = ql.find_first_or_null(db.Manager, function (m) { return m.id_Manager == args.id_Manager; });
		if (null!=Manager)
		{
			switch (args.cmd)
			{
				case 'change':
					Manager.ManagerEmail = args.email;
					Manager.Password = args.id_Manager;
					res = true;
					break;
				case 'block':
					Manager.ManagerEmail = args.email;
					Manager.Password = null;
					res = true;
					break;
			}
		}
		cx.completeCallback(200, 'success', { text: JSON.stringify({ ok: res }) });
	}

	return service;
});