﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/datamart?action=manager.ru');

	service.update = function (id, manager)
	{
		var Manager = ql.find_first_or_null(db.Manager, function (m) { return id == m.id_Manager; });
		if (null != Manager)
		{
			Manager.ManagerEmail = manager.Email;
			if (!Manager.ExtraParams)
				Manager.ExtraParams = {};
			Manager.ExtraParams.Телефон = manager.Телефон;
			Manager.ExtraParams.Приёмное_время = manager.Приёмное_время;
			Manager.ExtraParams.Адрес = manager.Адрес;
		}
	}

	service.read= function(id)
	{
		var rows= ql
			.select(function (r) {
				var res= {
					id_Manager: r.m.id_Manager
					, firstName: r.m.firstName
					, ContractNumber: !r.m.id_Contract_Change ? r.m.id_Contract ? r.c.ContractNumber : null : null
					, ContractNumberToChange: r.m.id_Contract_Change ? r.c.ContractNumber : null
					, lastName: r.m.lastName
					, middleName: r.m.middleName
					, efrsbNumber: r.m.efrsbNumber
					, INN: r.m.INN
					, Email: r.m.ManagerEmail
					, PasswordEnabled: (r.m.Password && null != r.m.Password)
				};
				if (r.m.ExtraParams)
				{
					res.Телефон = r.m.ExtraParams.Телефон;
					res.Приёмное_время = r.m.ExtraParams.Приёмное_время;
					res.Адрес = r.m.ExtraParams.Адрес;
				}
				return res;
			})
			.from(db.Manager, 'm')
			.left_join(db.Contract, 'c', function (r) { return r.m.id_Contract == r.c.id_Contract || r.m.id_Contract_Change == r.c.id_Contract; })
			.where(function (r) { return r.m.id_Manager == id; })
			.exec();
		return rows[0];
	}

	return service;
});