﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
], function (db, ajax_jqGrid, ql)
{
	var service = ajax_jqGrid('ama/datamart?action=manager.income.jqgrid');

	service.rows_all = function (url, args)
	{
		return ql.select(function (r) { return {
			  id_MData: r.md.id_MData
			, MData_Type: r.md.MData_Type
			, FromUser: r.mu.UserName
			, ForDebtor: r.d.Name
			, publicDate: r.md.publicDate
		};})
		.from(db.MData,"md")
		.inner_join(db.ManagerUser, "mu", function (r) { return r.md.id_ManagerUser==r.mu.id_ManagerUser; })
		.left_join(db.Debtor, "d", function (r) { return r.d.id_Debtor==r.md.id_Debtor; })
		.where(function (r) { return r.mu.id_Manager==args.id_Manager; })
		.exec();
	}

	return service;
});