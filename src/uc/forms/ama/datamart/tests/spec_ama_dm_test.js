define(function ()
{

	return {

		content: {
			"dm_logged_manager0": {
				path: 'txt!forms/ama/datamart/tests/logged/logged_manager0.json.txt'
				, title: 'залогиненный АУ 1 - Иванов ИИ'
			}
			, "dm_logged_manager1": {
				path: 'txt!forms/ama/datamart/tests/logged/logged_manager1.json.txt'
				, title: 'залогиненный АУ 2 - Петров ПП'
			}
			, "dm_logged_customer0": {
				path: 'txt!forms/ama/datamart/tests/logged/logged_customer0.json.txt'
				, title: 'залогиненный абонент 1'
			}
			, "dm_logged_viewer0": {
				path: 'txt!forms/ama/datamart/tests/logged/logged_viewer0.json.txt'
				, title: 'залогиненный наблюдатель 1 - a@o.u: Сергеева СМ'
			}
			, "dm_logged_admin0": {
				path: 'txt!forms/ama/datamart/tests/logged/logged_admin0.json.txt'
				, title: 'залогиненный администратор 1 - admin: Христофор'
			}
			, "dm_logged_sro0": {
				path: 'txt!forms/ama/datamart/tests/logged/logged_sro0.json.txt'
				, title: 'залогиненный СРО 1 - s@s.ru: МЦПУ'
			}
			, "dm_logged_debtor0": {
				path: 'txt!forms/ama/datamart/tests/logged/logged_debtor0.json.txt'
				, title: 'залогиненный должник 5 - sofiya71@s.ru: Вергунова СП'
			}
			, "dm_logged_debtor1": {
				path: 'txt!forms/ama/datamart/tests/logged/logged_debtor1.json.txt'
				, title: 'должник не установивший пароль 6 : Аносов КД'
			}
		}

	};

});