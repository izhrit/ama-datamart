﻿define([
	'txt!forms/ama/datamart/tests/datamart.json.txt'
	, 'forms/ama/datamart/tests/d_datamart_read'
],
function (content_ini_txt, d_datamart_read)
{
	d_datamart_read.content= JSON.parse(content_ini_txt);
	return d_datamart_read.content;
});