define([
	  'forms/base/codec/codec'
	, 'forms/ama/datamart/procedure/section/base/h_section_filenames'
	, 'forms/ama/datamart/tests/constants/h_constants_codec'
],
	function (BaseCodec, h_section_filenames, h_constants_codec)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.Encode = function (data)
		{
			var res = '<?php\r\n\r\n';

			res += h_constants_codec.PhpCommentsHeader('codec.SentEmail.php');

			res += '$filename_in_zip= array(\r\n';

			for (var i = 0; i < h_section_filenames.length; i++)
			{
				res += 0==i ? '  ' : ' ,';
				res += "'" + h_section_filenames[i] + "'\r\n";
			}

			res += ');\r\n';

			return res;
		}

		return codec;
	}
});
