define([
	  'forms/base/codec/codec'
	, 'forms/ama/datamart/base/h_vote_log'
	, 'forms/ama/datamart/tests/constants/h_constants_codec'
],
function (BaseCodec, h_vote_log, h_constants_codec)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.Encode = function (data)
		{
			var res = '<?php\r\n\r\n';

			res += h_constants_codec.PhpCommentsHeader('codec.Vote_log.php');

			var Vote_log_type_description_byCode = {};
			var Vote_log_type_description_byDescr = {};
			res += '$Vote_log_type_description= array(\r\n';
			for (var i = 0; i < h_vote_log.vote_log_type_array.length; i++)
			{
				var d = h_vote_log.vote_log_type_array[i];

				if (Vote_log_type_description_byCode[d.code])
					throw 'ununique Vote_log_type ' + d.code;
				if (Vote_log_type_description_byDescr[d.descr])
					throw 'ununique descr for Vote_log_type ' + d.descr;

				res += (0 == i) ? '   ' : '  ,';
				res += 'array(';
				res += "'code'=>'" + d.code + "'";
				res += ", 'descr'=>'" + d.descr + "'";
				res += ')\r\n';

				Vote_log_type_description_byCode[d.code] = d;
				Vote_log_type_description_byDescr[d.descr] = d;
			}
			res += ');\r\n\r\n';

			return res;
		}

		return codec;
	}
});
