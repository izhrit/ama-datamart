define([
	  'forms/base/codec/codec'
	, 'forms/ama/datamart/base/h_vote_document'
	, 'forms/ama/datamart/tests/constants/h_constants_codec'
],
function (BaseCodec, h_vote_document, h_constants_codec)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.Encode = function (data)
		{
			var res = '<?php\r\n\r\n';

			res += h_constants_codec.PhpCommentsHeader('codec.Vote_document.php');

			var Vote_document_type_description_byCode = {};
			var Vote_document_type_description_byDescr = {};
			res += '$Vote_document_type_description= array(\r\n';
			for (var i = 0; i < h_vote_document.Vote_document_type_description.length; i++)
			{
				var d = h_vote_document.Vote_document_type_description[i];

				if (Vote_document_type_description_byCode[d.code])
					throw 'ununique Vote_document_type ' + d.code;
				if (Vote_document_type_description_byDescr[d.descr])
					throw 'ununique descr for Vote_document_type ' + d.descr;

				res += (0 == i) ? '   ' : '  ,';
				res += 'array(';
				res += "'code'=>'" + d.code + "'";
				res += ", 'descr'=>'" + d.code + "'";
				res += ", 'Наименование_документа'=>'" + d.Наименование_документа + "'";
				res += ", 'FileName_prefix'=>'" + d.FileName_prefix + "'";
				res += ", 'FileName_extension'=>'" + d.FileName_extension + "'";
				res += ')\r\n';

				Vote_document_type_description_byCode[d.code] = d;
				Vote_document_type_description_byDescr[d.descr] = d;
			}
			res += ');\r\n\r\n';

			return res;
		}

		return codec;
	}
});
