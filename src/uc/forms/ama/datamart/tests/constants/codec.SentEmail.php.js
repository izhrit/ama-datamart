define([
	  'forms/base/codec/codec'
	, 'forms/ama/datamart/base/h_SentEmail'
	, 'forms/ama/datamart/tests/constants/h_constants_codec'
],
function (BaseCodec, h_SentEmail, h_constants_codec)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.Encode = function (data)
		{
			var res = '<?php\r\n\r\n';

			res += h_constants_codec.PhpCommentsHeader('codec.SentEmail.php');

			res+= 'global $EmailType_descriptions, $RecipientType_description;\r\n';

			var RecipientType_description_byCode = {};
			var RecipientType_description_byTable = {};
			res += '$RecipientType_description= array(\r\n';
			for (var i = 0; i < h_SentEmail.RecipientType_description.length; i++)
			{
				var d = h_SentEmail.RecipientType_description[i];
				if (RecipientType_description_byTable[d.table])
					throw 'ununique table RecipientType ' + d.table;
				if (RecipientType_description_byCode[d.code])
					throw 'ununique RecipientType ' + d.code;

				res += (0 == i) ? '   ' : '  ,';
				res += 'array(';
				res += "'table'=>'" + d.table + "'";
				res += ", 'code'=>'" + d.code + "'";
				res += ')\r\n';

				RecipientType_description_byTable[d.table] = d;
				RecipientType_description_byCode[d.code] = d;
			}
			res += ');\r\n\r\n';

			var EmailType_description_byCode = {};
			var EmailType_description_byDescr = {};
			res += '$EmailType_descriptions= array(\r\n';
			for (var i = 0; i < h_SentEmail.EmailType_descriptions.length; i++)
			{
				var d = h_SentEmail.EmailType_descriptions[i];

				if (!RecipientType_description_byTable[d.table])
					throw 'EmailType for unknown table ' + d.table;
				if (EmailType_description_byCode[d.code])
					throw 'ununique EmailType ' + d.code;
				if (EmailType_description_byDescr[d.descr])
					throw 'ununique descr for EmailType ' + d.descr;

				res += (0 == i) ? '   ' : '  ,';
				res += 'array(';
				res += "'table'=>'" + d.table + "'";
				res += ", 'code'=>'" + d.code + "'";
				res += ", 'descr'=>'" + d.descr + "'";
				if (d.Наименование_документа)
					res += ", 'Наименование_документа'=>'" + d.Наименование_документа + "'";
				if (d.FileName_prefix)
					res += ", 'FileName_prefix'=>'" + d.FileName_prefix + "'"
				res += ')\r\n';

				EmailType_description_byCode[d.code] = d;
				EmailType_description_byDescr[d.descr] = d;
			}
			res += ');\r\n';

			return res;
		}

		return codec;
	}
});
