define([
	 'forms/base/codec/codec'
	,'forms/ama/biddings/efrsb_classes/base/h_efrsb_classes'
],
function (BaseCodec, h_efrsb_classes)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.Encode= function ()
		{
			var res= 'set names utf8;\r\n';
			var variants_by_code_len= {
				'2':  {id_Parent: 'null',   get_id:'set @id_l1=last_insert_id();\r\n'}
				,'4': {id_Parent: '@id_l1', get_id:'set @id_l2=last_insert_id();\r\n'}
				,'7': {id_Parent: '@id_l2', get_id:''}
			};
			for (var i= 0; i< h_efrsb_classes.classes_arr_arr.length; i++)
			{
				var c= h_efrsb_classes.classes_arr_arr[i];
				var code= c[0];
				var name= c[1];
				var v= variants_by_code_len[code.length+''];
				res+= 'insert into EfrsbAssetClass set ';
				res+= 'id_EfrsbAssetClass_Parent=' + v.id_Parent + ', ';
				res+= 'Code="' + code + '", ';
				res+= 'Title=\'' + name + '\';\r\n';
				res+= v.get_id;
			}
			return res;
		}

		return codec;
	}
});
