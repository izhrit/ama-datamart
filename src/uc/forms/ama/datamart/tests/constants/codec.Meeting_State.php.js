define([
	  'forms/base/codec/codec'
	, 'forms/ama/datamart/base/h_Meeting_state'
	, 'forms/ama/datamart/tests/constants/h_constants_codec'
],
function (BaseCodec, h_Meeting_state, h_constants_codec)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.Encode = function (data)
		{
			var res = '<?php\r\n\r\n';

			res += h_constants_codec.PhpCommentsHeader('codec.Meeting_State.php');

			var Meeting_State_description_byCode = {};
			var Meeting_State_description_byDescr = {};
			res += '$Meeting_State_descriptions= array(\r\n';
			for (var i = 0; i < h_Meeting_state.Meeting_State_descriptions.length; i++)
			{
				var d = h_Meeting_state.Meeting_State_descriptions[i];

				if (Meeting_State_description_byCode[d.code])
					throw 'ununique Meeting_State ' + d.code;
				if (Meeting_State_description_byDescr[d.descr])
					throw 'ununique descr for Meeting_State ' + d.descr;

				res += (0 == i) ? '   ' : '  ,';
				res += 'array(';
				res += "'code'=>'" + d.code + "'";
				res += ", 'descr'=>'" + d.descr + "'";
				res += ')\r\n';

				Meeting_State_description_byCode[d.code] = d;
				Meeting_State_description_byDescr[d.descr] = d;
			}
			res += ');\r\n';

			return res;
		}

		return codec;
	}
});
