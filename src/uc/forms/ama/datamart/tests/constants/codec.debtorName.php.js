define([
	  'forms/base/codec/codec'
	, 'forms/ama/datamart/base/h_debtorName'
	, 'forms/ama/datamart/tests/constants/h_constants_codec'
],
function (BaseCodec, h_debtorName, h_constants_codec)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.Encode = function (data)
		{
			var res = '<?php\r\n\r\n';

			res += h_constants_codec.PhpCommentsHeader('codec.debtorName.php');

			res += '$debtorName_beauty_prefixes= array(\r\n';
			for (var i = 0; i < h_debtorName.changes.length; i++)
			{
				var c = h_debtorName.changes[i];
				res += (0 == i) ? '   ' : '  ,';
				res += 'array(';
				res += "'prefix'=>'" + c.prefix + "'";
				res += ", 'change_to'=>'" + c.change_to + "'";
				res += ')\r\n';
			}
			res += ');\r\n';

			return res;
		}

		return codec;
	}
});
