define([
	'forms/base/h_spec'
],
function (h_spec)
{
	var dm_handbook_specs = {

		controller:
		{
			"dm_handbook":
			{
				path: 'forms/ama/datamart/handbook/main/c_dm_handbook'
				,title: 'Справочники'
			}

			,"dm_sl_record":
			{
				path: 'forms/ama/datamart/handbook/sl/record/c_sl_record'
				, title: 'Запись о прожиточном минимуме'
			}

			, "dm_sl_request":
			{
				path: 'forms/ama/datamart/handbook/sl/request/c_sl_request'
				, title: 'Запрос инофрмации о прожиточном минимуме'
			}

			,"dm_sl_table":
			{
				path: 'forms/ama/datamart/handbook/sl/table/c_sl_table'
				, title: 'Записи о прожиточных минимумах'
			}
			, "dm_wcalebdar_month":
			{
				path: 'forms/ama/datamart/handbook/wcalendar/month/c_month'
				, title: 'Отображение месяца рабочего календаря'
			}
			, "dm_wcalendar_year":
			{
				path: 'forms/ama/datamart/handbook/wcalendar/year/c_year'
				, title: 'Отображение рабочего календаря на год'
			}
			, "dm_wcalendar_year_editor":
			{
				path: 'forms/ama/datamart/handbook/wcalendar/year_editor/c_year_editor'
				, title: 'Редактор рабочего календаря на год'
			}
			,"dm_court":
			{
				path: 'forms/ama/datamart/handbook/court/record/c_court'
				, title: 'Информация об арбитражном суде'
			}
			,"dm_court_table":
			{
				path: 'forms/ama/datamart/handbook/court/table/c_court_table'
				, title: 'Информация об арбитражных судах'
			}
			, "dm_rosreestr_table":
			{
				path: 'forms/ama/datamart/handbook/rosreestr/table/c_rosreestr_table'
				, title: 'Информация о подразделениях росреестра'
			}
			, "dm_rosreestr":
			{
				path: 'forms/ama/datamart/handbook/rosreestr/record/c_rosreestr'
				, title: 'Информация о подразделении росреестра'
			}
		}

		, content:
		{
			"sl-record-01sav": { path: 'txt!forms/ama/datamart/handbook/sl/record/tests/contents/01sav.json.etalon.txt' }
			, "wcalendar-month1": { path: 'txt!forms/ama/datamart/handbook/wcalendar/month/tests/contents/example1.json.txt' }
			, "wcalendar-year1": { path: 'txt!forms/ama/datamart/handbook/wcalendar/year/tests/contents/example1.json.txt' }
			, "wcalendar-year-editor1": { path: 'txt!forms/ama/datamart/handbook/wcalendar/year_editor/tests/contents/example1.json.txt' }
			, "court-01sav": { path: 'txt!forms/ama/datamart/handbook/court/record/tests/contents/01sav.json.etalon.txt' }
			, "rosreestr-01sav": { path: 'txt!forms/ama/datamart/handbook/rosreestr/record/tests/contents/01sav.json.etalon.txt' }
		}
	};

	return h_spec.combine(dm_handbook_specs);
});