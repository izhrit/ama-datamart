define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/handbook/wcalendar/new_year/e_new_year.html'
	, 'forms/ama/datamart/base/h_region'
],
function (c_fastened, tpl, h_region)
{
	return function (options_arg)
	{
		var admin_mode = (null == options_arg) ? true : options_arg.admin_mode;
		var new_year = (null == options_arg) ? '' : options_arg.new_year;
		var pr_copy = (null == options_arg) ? false : options_arg.pr_copy;

		var controller = c_fastened(tpl, {regions:h_region, admin_mode:admin_mode, new_year:new_year, pr_copy:pr_copy });

		controller.size = {width:550, height:580};

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' select[data-fc-selector="Регион"]').select2();
		}

		return controller;
	}
});