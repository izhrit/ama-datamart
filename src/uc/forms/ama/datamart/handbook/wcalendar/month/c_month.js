define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/handbook/wcalendar/month/e_month.html'
],
function (c_fastened, tpl)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self = this;
			var admin_mode = (null == options_arg) ? false : options_arg.admin_mode;
			if (admin_mode)
			{
				$(sel + ' td').css('cursor', 'pointer');
				$(sel + ' td').click(function (e) { self.OnDayClick(e); });
			}
		}

		controller.OnDayClick = function (e)
		{
			var sel = this.fastening.selector;
			var tditem = $(e.target);

			var change = tditem.attr('data-change');
			change == 1 ? tditem.attr('data-change', 0) : tditem.attr('data-change', 1);

			var count_change = $('td[data-change=1]');
			count_change.length ? $('#del_changes').show() : $('#del_changes').hide();
			if (tditem.hasClass('holiday'))
			{
				tditem.removeClass('holiday')
				tditem.addClass('work')
			}
			else if (tditem.hasClass('work'))
			{
				tditem.removeClass('work')
				tditem.addClass('holiday')
			}
		}

		controller.GetFormContent = function ()
		{
			var result = [];

			var sel = this.fastening.selector;
			$(sel+' td').each(function () {
				var text = $(this).text();
				if (!text) return;
				var isWorkDay = $(this).hasClass('work');
				var day = $(this).data('day');
				var obj = {
					'день': day,
					'рабочий': String(isWorkDay)
				}
				result.push(obj);
			});

			return result;
		}

		return controller;
	}
});