define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/handbook/wcalendar/year/e_year.html'
	, 'forms/ama/datamart/handbook/wcalendar/month/c_month'
],
function (c_fastened, tpl, c_month)
{
	return function (options_arg)
	{
		var options = {
			field_spec:
			{
				Январь: { controller: function () { return c_month(options_arg); } }
				, Февраль: { controller: function () { return c_month(options_arg); } }
				, Март: { controller: function () { return c_month(options_arg); } }
				, Апрель: { controller: function () { return c_month(options_arg); } }
				, Май: { controller: function () { return c_month(options_arg); } }
				, Июнь: { controller: function () { return c_month(options_arg); } }
				, Июль: { controller: function () { return c_month(options_arg); } }
				, Август: { controller: function () { return c_month(options_arg); } }
				, Сентябрь: { controller: function () { return c_month(options_arg); } }
				, Октябрь: { controller: function () { return c_month(options_arg); } }
				, Ноябрь: { controller: function () { return c_month(options_arg); } }
				, Декабрь: { controller: function () { return c_month(options_arg); } }
			}
		};

		var controller = c_fastened(tpl, options);

		return controller;
	}
});