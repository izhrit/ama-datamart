define([
	'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/handbook/wcalendar/year_editor/e_year_editor.html'
	, 'forms/ama/datamart/handbook/wcalendar/year/c_year'
	, 'forms/ama/datamart/handbook/wcalendar/new_year/c_new_year'
	, 'forms/base/h_msgbox'
],
	function (c_fastened, tpl, c_year, c_new_year, h_msgbox) {
		return function (options_arg) {

			var controller = c_fastened(tpl);

			controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
			controller.base_crud_url = controller.base_url + '?action=wcalendar.crud';
			controller.base_list_url = controller.base_url + '?action=wcalendar.list';

			controller.base_grid_url = controller.base_url + '?action=wcalendar.jqgrid';
			controller.old_year;
			controller.admin_mode;
			controller.old_id_row;

			var base_Render = controller.Render;
			controller.Render = function (sel) {
				base_Render.call(this, sel);
				this.RenderGrid();

				var self = this;
				self.admin_mode = this.model && null != this.model && null != this.model.id_Admin;
				$(sel + ' select').focus(function (e) {
					controller.old_selected_year = e.target.value;
				});

				if (!controller.admin_mode) {
					$(sel + ' #delete').hide();
					$(sel + ' #save').hide();
					$(sel + ' #add').hide();
					$(sel + ' #copy').hide();
					$(sel + ' #del_changes').hide();
				}

				$(sel + ' #save').click(function (e) { self.OnSaveYear(e); });
				$(sel + ' #delete').click(function (e) { self.OnDeleteYear(e); });
				$(sel + " #add").click(function (e) { self.ShowYearForAddOrCopy(e, false); });
				$(sel + " #copy").click(function (e) { self.ShowYearForAddOrCopy(e, true); });
				$(sel + " #del_changes").click(function (e) { self.OnDelChanges(e); });
			}

			controller.ReloadGrid= function()
			{
				var sel = this.fastening.selector;
				var grid = $(sel + ' table.grid');
				grid.trigger('reloadGrid');
			}

			controller.OnSelectYear = function (id) {
				if(!controller.old_id_row){
					controller.old_id_row = id;
				}
				var sel = this.fastening.selector;
				var isNewYear = $(sel + ' #next_year').val();
				var self = this;
				var has_change = false;
				if (self.cc_year && null != self.cc_year && self.edited_year_before_change != null) {
					var current_year = self.cc_year.GetFormContent();
					var year_before_change = self.edited_year_before_change;
					if (typeof year_before_change === "string") year_before_change = JSON.parse(year_before_change);

					has_change = JSON.stringify(year_before_change) != JSON.stringify(current_year);
				}
				var btnOk = 'ОК';
				if (has_change || isNewYear) {
					h_msgbox.ShowModal
						({
							title: 'Изменения не сохранены.'
							, html: 'Перейти без сохранения?'
							, buttons: [btnOk, 'Отмена']
							, id_div: "cpw-form-ama-datamart-wcalendar"
							, onclose: function (btn) {
								if (btn == btnOk) {
									controller.old_id_row = id;
									self.OnSelect();
								}
							}
						});
				} else {
					controller.old_id_row = id;
					self.OnSelect();
				}
			}

			controller.OnSelect = function () {
				var sel = this.fastening.selector;
				$(sel + ' #next_year').val('');
				var grid = $(sel + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');
				var rowdata = grid.jqGrid('getRowData', selrow);

				var self = this;
				var id_Wcalendar = rowdata.id_Wcalendar;
				var ajaxurl_get = this.base_crud_url + '&cmd=get&id=' + id_Wcalendar;

				var v_ajax = h_msgbox.ShowAjaxRequest("Получение рабочего календаря с сервера", ajaxurl_get);
				v_ajax.ajax
					({
						dataType: "json", type: 'GET', cache: false
						, success: function (wcalendar, textStatus) {
							if (self.cc_year) {
								self.cc_year.Destroy();
								self.cc_year = null;
								delete self.cc_year;
							}

							self.cc_year = c_year({ admin_mode: controller.admin_mode });
							self.edited_year_before_change = wcalendar.Days;
							self.cc_year.SetFormContent(wcalendar.Days);
							self.cc_year.Edit(sel + ' div.year');
						}
					});
			}

			controller.OnDelChanges = function () {
				var sel = this.fastening.selector;
				var self = this;

				if (self.cc_year && null != self.cc_year) {
					self.cc_year.Destroy();
					$(sel + ' div.year').html();
					delete self.cc_year;
				}

				if (self.edited_year_before_change && null != self.edited_year_before_change) {
					self.cc_year = c_year({ admin_mode: controller.admin_mode });
					self.cc_year.SetFormContent(self.edited_year_before_change);
					self.cc_year.Edit(sel + ' div.year');
				}
				$(sel + ' #del_changes').hide();
			}

			controller.OnSaveYear = function () {
				self = this;
				if (!controller.admin_mode) {
					h_msgbox.ShowModal({ title: "Ошибка", html: "Недостаточно прав для сохранения" });
					return false;
				}
				var sel = this.fastening.selector;
				var is_new_year = $(sel + ' #next_year').val();

				var has_change = false;
				if (this.cc_year && null != this.cc_year && self.edited_year_before_change != null) {
					var current_year = this.cc_year.GetFormContent();
					var year_before_change = self.edited_year_before_change;
					if (typeof year_before_change === "string") year_before_change = JSON.parse(year_before_change);

					has_change = JSON.stringify(year_before_change) != JSON.stringify(current_year);
				}

				if (!is_new_year && !(controller.old_id_row && has_change)) {
					h_msgbox.ShowModal({ title: "Ошибка", html: "Нет данных для сохранения!" });
					return false;
				}
				var self = this;

				var btnOk = 'ОК';
				h_msgbox.ShowModal
					({
						title: 'Сохранение'
						, html: 'Сохранить запись?'
						, buttons: [btnOk, 'Отмена']
						, id_div: "cpw-form-ama-datamart-wcalendar"
						, onclose: function (btn) {
							if (btn == btnOk) {
								if (is_new_year) {
									self.OnAddYear();
								} else {
									self.OnUpdateYear();
								}
							}
						}
					});
			}


			controller.OnAddYear = function () {
				if (!controller.admin_mode) {
					h_msgbox.ShowModal({ title: "Ошибка", html: "Недостаточно прав для добавления" });
					return false;
				}
				var sel = this.fastening.selector;
				var self = this;
				var cc_year = self.cc_year;
				var days = cc_year.GetFormContent();
				var year = $(sel + ' #next_year').val();
				var kod_okato = $(sel + ' #kod_okato').val();
				var data_for_add = {
					"Year": year,
					"Days": days,
					"Kod_okato": kod_okato
				}

				var ajaxurl_create = self.base_crud_url + '&cmd=add';
				v_ajax = h_msgbox.ShowAjaxRequest("Добавление записи на сервере", ajaxurl_create);
				v_ajax.ajax
					({
						dataType: "json", type: 'POST', cache: false, data: data_for_add
						, success: function (responce) {
							if (responce) {
								$(sel + ' #del_changes').hide();
								$(sel + ' #next_year').val('');
								self.edited_year_before_change = JSON.stringify(days);
								h_msgbox.ShowModal({ width: 200, html: "Запись добавлена!" });
								self.ReloadGrid();
								$(sel).trigger('model_change');
								$(sel + ' .year').html('');
							}
						}
					});
			}

			controller.OnUpdateYear = function () {
				if (!controller.admin_mode) {
					h_msgbox.ShowModal({ title: "Ошибка", html: "Недостаточно прав для сохранения" });
					return false;
				}
				var sel = this.fastening.selector;
				var self = this;
				var cc_year = self.cc_year;
				var grid = $(sel + ' table.grid');
				var selrow = controller.old_id_row;
				var rowdata = grid.jqGrid('getRowData', selrow);

				var self = this;
				var id_Wcalendar = rowdata.id_Wcalendar;

				var ajaxurl_update = self.base_crud_url + '&cmd=update&id=' + id_Wcalendar;
				var data_for_update = cc_year.GetFormContent();

				v_ajax = h_msgbox.ShowAjaxRequest("Сохранение записи на сервере", ajaxurl_update);
				v_ajax.ajax
					({
						dataType: "json", type: 'POST', cache: false, data: data_for_update
						, success: function (responce) {
							$(sel + ' #del_changes').hide();
							self.edited_year_before_change = JSON.stringify(data_for_update);
							h_msgbox.ShowModal({ width: 200, html: "Запись сохранена!" });
							self.ReloadGrid();
							$(sel).trigger('model_change');
						}
					});
			}


			controller.OnDeleteYear = function () {
				if (!controller.admin_mode) {
					h_msgbox.ShowModal({ title: "Ошибка", html: "Недостаточно прав для удаления" });
					return false;
				}
				var sel = this.fastening.selector;
				var self = this;
				var isNewYear = $(sel + ' #next_year').val();
				if (isNewYear) {
					h_msgbox.ShowModal({ width: 200, html: "Год еще не сохранен!" });
					return false;
				}
				var grid = $(sel + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');

				if (!selrow) {
					h_msgbox.ShowModal({ html: "Не выбрана запись для удаления!" });
					return false;
				}

				var btnOk = 'ОК';
				h_msgbox.ShowModal
					({
						title: 'Удаление'
						, html: 'Удалить запись?'
						, buttons: [btnOk, 'Отмена']
						, id_div: "cpw-form-ama-datamart-wcalendar"
						, onclose: function (btn) {
							if (btn == btnOk) {
								self.DeleteYear(sel);
							}
						}
					});
			}

			controller.DeleteYear = function (sel) {
				var grid = $(sel + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');
				var rowdata = grid.jqGrid('getRowData', selrow);
				var id_Wcalendar = rowdata.id_Wcalendar;
				self = this;
				var ajaxurl_delete = self.base_crud_url + '&cmd=delete&id=' + id_Wcalendar;
				v_ajax = h_msgbox.ShowAjaxRequest("Удаление записи на сервере", ajaxurl_delete);
				v_ajax.ajax
					({
						dataType: "json", type: 'POST', cache: false
						, success: function (responce) {
							self.edited_year_before_change = null;
							$(sel + ' #del_changes').hide();
							h_msgbox.ShowModal({ width: 200, html: "Запись удалена!" });
							self.ReloadGrid();
							$(sel).trigger('model_change');
							$(sel + ' .year').html('');
						}
					});
			}

			controller.ShowYearForAddOrCopy = function (e, pr_copy) {
				if (!controller.admin_mode) {
					h_msgbox.ShowModal({ title: "Ошибка", html: "Недостаточно прав для добавления" });
					return false;
				}
				var sel = this.fastening.selector;
				var self = this;
				var isNewYear = $(sel + ' #next_year').val();

				if (pr_copy && !self.cc_year){
					h_msgbox.ShowModal({ title: "Ошибка", html: "Выберите год для копирования" });
					return false;
				}

				var has_change = false;
				if (self.cc_year && null != self.cc_year && self.edited_year_before_change != null) {
					var current_year = self.cc_year.GetFormContent();
					var year_before_change = self.edited_year_before_change;
					if (typeof year_before_change === "string") year_before_change = JSON.parse(year_before_change);

					var has_change = JSON.stringify(year_before_change) != JSON.stringify(current_year);
				}
				title_mes = pr_copy ? 'Копирование года' : 'Добавление'

				if (has_change || isNewYear) {
					var btnOk = 'ОК';
					h_msgbox.ShowModal
						({
							title: title_mes
							, html: 'Перейти без сохранения?'
							, buttons: [btnOk, 'Отмена']
							, id_div: "cpw-form-ama-datamart-wcalendar"
							, onclose: function (btn) {
								if (btn == btnOk) {
									self.OnDelChanges();
									self.YearForAddOrCopy(pr_copy);
								}
							}
						});
				} else {
					self.YearForAddOrCopy(pr_copy);
				}
			}

			controller.YearForAddOrCopy = function (pr_copy) {
				var self = this;
				var sel = this.fastening.selector;
				$(sel + ' #next_year').val('');

				if(pr_copy){
					var grid = $(sel + ' table.grid');
					var selrow = grid.jqGrid('getGridParam', 'selrow');
					var rowdata = grid.jqGrid('getRowData', selrow);
					new_year = rowdata.Year;
					days_for_copy = self.cc_year.GetFormContent();
					title_mes = 'Копирование записи';
				} else {
					var currentTime = new Date();
					new_year = currentTime.getFullYear();
					title_mes = 'Добавление записи';
				}

				var btnOk = 'ОК';
				var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;
				var cc_new_year = c_new_year({base_url:this.base_url,admin_mode:admin_mode,new_year:new_year,pr_copy:pr_copy});
				h_msgbox.ShowModal
				({
					title: title_mes
					, controller: cc_new_year
					, width:100
					, id_div:'cpw-form-wcl-record-add'
					, buttons:[btnOk, 'Отмена']
					, onclose: function(btn)
					{
						if (btn==btnOk)
						{
							var wcl_new_year= cc_new_year.GetFormContent();
							if(!pr_copy){
								if (!wcl_new_year.Год) {
									h_msgbox.ShowModal({ html: "Необходимо ввести год!" });
									return false;
								}
								if (!wcl_new_year.Год.match(/^\d+$/)) {
									h_msgbox.ShowModal({ html: "Год должен состоять из цифр!" });
									return false;
								}
								days = null;
								year = wcl_new_year.Год;
							}else{
								days = days_for_copy;
								year = new_year;
							}
							kod_okato = wcl_new_year.Код_ОКАТО;
							self.CreateYear(year, kod_okato, days);
						}
					}
				});
			}

			controller.CreateYear = function (new_year, okato, days_for_copy) {
				var self = this;
				var sel = this.fastening.selector;
				var arr_reg = okato.split(',');

				self.base_list_url = self.base_url + '?action=wcalendar.list';
				var pr = false;
				var ajaxurl_get = self.base_list_url;
				var v_ajax = h_msgbox.ShowAjaxRequest("Получение списка с годами", ajaxurl_get);
				v_ajax.ajax
					({
						dataType: "json", type: 'GET', cache: false
						, success: function (responce) {
							responce.map(function (item) {
								if(item.Year == new_year && item.okato == arr_reg[0]) {
									pr = true;
									return;
								}
							});
							if (pr) {
								h_msgbox.ShowModal({ html: "Введённый год для данного региона уже есть в списке!" });
								return false;
							}else{
								var obj_new_year = {};
								if(days_for_copy){
									obj_new_year = days_for_copy;
								 }else {
									var arrayMonths = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
									for (var i = 1; i <= 12; i++) {
										var month = '0' + i;
										month = month.substring(month.length - 2)
										var date = '01.' + month + '.' + new_year;
										var obj_day = {
											'день': date,
											'рабочий': true
										}
										var arr_month = [obj_day];
										var month_string = arrayMonths[i - 1];
										obj_new_year[month_string] = arr_month;
									}
								}
								obj_new_year['Регион'] = arr_reg;

								if (self.cc_year) {
									self.cc_year.Destroy();
									self.cc_year = null;
									delete self.cc_year;
								}
								$(sel + ' #next_year').val(new_year);
								$(sel + ' #kod_okato').val(arr_reg[0]);
								var cc_year = self.cc_year = c_year({ admin_mode: self.admin_mode });
								cc_year.SetFormContent(obj_new_year);
								self.edited_year_before_change = JSON.stringify(obj_new_year);
								cc_year.Edit(sel + ' div.year');
								$(sel + ' #years option').attr('selected', false);
							}

						}
					});
			}

			controller.colModel =
				[
					{ name: 'id_Wcalendar', hidden: true }

				, { label: 'Год', name: 'Year', width: 1, sortable: true, search: true, defaultSearch: 'cn', defaultValue: 'test', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] }  }
				, { label: 'Регион', name: 'Region', width: 2, sortable: true, defaultSearch: 'cn', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] } }

				];

			controller.RenderGrid = function () {
				var sel = this.fastening.selector;
				var self = this;

				var url = this.base_grid_url;
				var grid = $(sel + ' table.grid');
				grid.jqGrid
					({
						datatype: 'json'
						, url: url
						, colModel: self.colModel
						, gridview: true
						, loadtext: 'Загрузка...'
						, emptyText: 'Нет записей для просмотра'
						, pgtext: "{0} из {1}"
						, rownumbers: false
						, rowNum: 6
						, pager: '#wcl-grid-pager'
						, viewrecords: true
						, autowidth: true
						, height: 'auto'
						, multiselect: false
						, multiboxonly: true
						, shrinkToFit: true
						, ignoreCase: true
						, searchOnEnter: true
						, onSelectRow: function (id, s, e) { self.OnSelectYear(id); }
						, loadComplete: function () { self.RenderRowActions(grid); }
						, loadError: function (jqXHR, textStatus, errorThrown) {
							h_msgbox.ShowAjaxError("Загрузка списка годов", url, jqXHR.responseText, textStatus, errorThrown)
						}
					});
				grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
			}

			return controller;
		}

});