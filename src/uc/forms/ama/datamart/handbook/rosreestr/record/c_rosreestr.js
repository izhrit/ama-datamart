define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/handbook/rosreestr/record/e_rosreestr.html'
	, 'forms/ama/datamart/base/h_region'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_region, h_validation_msg)
{
	return function (options_arg)
	{
		var admin_mode= (null==options_arg) ? true : options_arg.admin_mode;

		var controller = c_fastened(tpl, { regions: h_region, admin_mode:admin_mode });

		controller.size = { width: 580, height: 455 };

		var base_Render = controller.Render;

		controller.Render = function (sel) {
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' select[data-fc-selector="Region"]').select2();
		}

		controller.Validate = function () {
			var res = null;
			var model = this.GetFormContent();

			if ('' == h_validation_msg.trim(model.Region))
				res = h_validation_msg.error(res, "Регион не может быть пустым");

			if ('' == model.Name)
				res = h_validation_msg.error(res, "Официальное наименование не может быть пустым");

			if ('' == model.Address)
				res = h_validation_msg.error(res, "Адрес не может быть пустым");

			if (!/[+-]?([0-9]*[.])?[0-9]+/.test(model.Latitude))
				res = h_validation_msg.error(res, "Широта должна содержать только число");

			if (!/[+-]?([0-9]*[.])?[0-9]+/.test(model.Longitude))
				res = h_validation_msg.error(res, "Долгот должна содержать только число");

			return res;
		}

		return controller;
	}
});