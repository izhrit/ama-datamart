define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/handbook/sl/request/e_sl_request.html'
	, 'forms/ama/datamart/base/h_region'
	, 'forms/base/h_msgbox'
],
	function (c_fastened, tpl, h_region, h_msgbox)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var controller = c_fastened(tpl, {regions:h_region });
		var dateFormat = function (date)
		{
			return date.substring(6, 10) + '-' + date.substring(3, 5) + '-' + date.substring(0, 2)
		};
		controller.size = {width:550, height:580};

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' select[model-selector="Регион"]').select2();

			this.FixKvartalByDate();

			$(sel + ' .kvartal').change(function () { self.OnChangeKvartal(); });
			$(sel + ' [model-selector="Дата"]').change(function () { self.FixKvartalByDate(); });

			$(sel + ' button.request').click(function () { self.OnRequest(); });

			this.FixKvartalByDate();

			$(sel + ' .kvartal').change(function () { self.OnChangeKvartal(); });
			$(sel + ' [data-fc-selector="Дата"]').change(function () { self.FixKvartalByDate(); });
		}

		controller.OnChangeKvartal = function ()
		{
			var sel= this.fastening.selector;
			var dt= $(sel + ' .kvartal').val();
			$(sel + ' [model-selector="Дата"]').val(dt);
		}

		controller.FixKvartalByDate = function ()
		{
			var sel= this.fastening.selector;
			var dt= $(sel + ' [model-selector="Дата"]').val();
			$(sel + " .kvartal > option").each(function()
			{
				if (dt == this.value)
				{
					$(this).attr('selected','selected');
					return false;
				}
			});
		}

		controller.OnRequest = function () {
			var self = this;
			var sel = this.fastening.selector;
			var params = self.GetFormContent();
			var date = dateFormat(params.Дата);
			var ajaxurl = encodeURI(base_url + '?action=api-GetSubLevelInformation&date=' + date + '&region=' + params.Регион);
			v_ajax = h_msgbox.ShowAjaxRequest("Запрос записей на сервере", ajaxurl);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (responce) {
						var reglament = '';
						responce.Reglaments.forEach(function (item, i, arr) {
							reglament = reglament + (i + 1) + ') ' + responce.Reglaments[i].title + '<br>'
						});
						$(sel + " div#common").text(responce.Common == null ? 'не найдено' : responce.Common + ' руб.');
						$(sel + " div#employable").text(responce.Employable == null ? 'не найдено' : responce.Employable + ' руб.');
						$(sel + " div#infant").text(responce.Infant == null ? 'не найдено' : responce.Infant + ' руб.');
						$(sel + " div#pensioner").text(responce.Pensioner == null ? 'не найдено' : responce.Pensioner + ' руб.');
						$(sel + " div#reglament").html(reglament);
						$(sel + " div.list").css('display','block');
					}
				});
		}

		controller.OnChangeKvartal = function () {
			var sel = this.fastening.selector;
			var dt = $(sel + ' .kvartal').val();
			$(sel + ' [data-fc-selector="Дата"]').val(dt);
		}

		controller.FixKvartalByDate = function () {
			var sel = this.fastening.selector;
			var dt = $(sel + ' [data-fc-selector="Дата"]').val();
			$(sel + " .kvartal > option").each(function () {
				if (dt == this.value) {
					$(this).attr('selected', 'selected');
					return false;
				}
			});
		}

		return controller;
	}
});