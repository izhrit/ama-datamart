define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/handbook/sl/record/e_sl_record.html'
	, 'forms/ama/datamart/base/h_region'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_region,h_validation_msg)
{
	return function (options_arg)
	{
		var admin_mode = (null == options_arg) ? true : options_arg.admin_mode;

		var controller = c_fastened(tpl, {regions:h_region, admin_mode:admin_mode });

		controller.size = {width:550, height:580};

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' button.gotourl').click(function () { self.OnGotoUrl(); });
			$(sel + ' select[data-fc-selector="Регион"]').select2();

			this.FixKvartalByDate();

			$(sel + ' .kvartal').change(function () { self.OnChangeKvartal(); });
			$(sel + ' [data-fc-selector="Дата"]').change(function () { self.FixKvartalByDate(); });
			$(sel + ' input[data-fc-selector="Прожиточный_минимум.Подушевой.Значение"]').change(function () { self.OnChangeMoney("Прожиточный_минимум.Подушевой.Значение"); });
			$(sel + ' input[data-fc-selector="Прожиточный_минимум.Трудоспособных.Значение"]').change(function () { self.OnChangeMoney("Прожиточный_минимум.Трудоспособных.Значение"); });
			$(sel + ' input[data-fc-selector="Прожиточный_минимум.Пенсионеров.Значение"]').change(function () { self.OnChangeMoney("Прожиточный_минимум.Пенсионеров.Значение"); });
			$(sel + ' input[data-fc-selector="Прожиточный_минимум.Несовершеннолетних.Значение"]').change(function () { self.OnChangeMoney("Прожиточный_минимум.Несовершеннолетних.Значение"); });

		}
		controller.OnChangeMoney = function (input) {
			var sel = this.fastening.selector;
			var data = $(sel + ' input[data-fc-selector="'+input+'"]').val();
			data = data.replace(/\D/, '.');
			var regxp1 = /(\d{1,})\.(\d{1,2})/g;
			var regxp2 = /(\d{1,})/g;
			var matches1 = data.match(regxp1);
			var matches2 = data.match(regxp2);
			$(sel + ' input[data-fc-selector="' + input + '"]').val(matches1 ? matches1[0] : (matches2 ? matches2[0]+'.00' : ''));
		}

		controller.OnChangeKvartal = function ()
		{
			var sel= this.fastening.selector;
			var dt= $(sel + ' .kvartal').val();
			$(sel + ' [data-fc-selector="Дата"]').val(dt);
		}

		controller.FixKvartalByDate = function ()
		{
			var sel= this.fastening.selector;
			var dt= $(sel + ' [data-fc-selector="Дата"]').val();
			$(sel + " .kvartal > option").each(function()
			{
				if (dt == this.value)
				{
					$(this).attr('selected','selected');
					return false;
				}
			});
		}

		controller.OnGotoUrl = function ()
		{
			var sel= this.fastening.selector;
			var url= $(sel + ' input[data-fc-selector="Регламентирующий_акт.Ссылка"]').val();
			if (url && null != url && '' != null)
			{
				if (0!=url.indexOf('http'))
					url= 'http://' + url;
				window.open(url);
			}
		}

		var check_money = function (str) {
			var regexp = /^(\d{1,})\.(\d{1,2})$/;
			return regexp.test(str);
		}

		var check_date_dd_mm_yyyyy = function (str) {
			var regexp = /^(\d{2})\.(\d{2})\.(\d{4})$/;
			return regexp.test(str);
		}

		controller.Validate = function () {
			var res = null;
			var model = this.GetFormContent();
			var error_money = "Значение прожиточного минимума должно быть указано по типу 100.59 (где 100 - рубли, 59 - копейки)";
			if (!check_date_dd_mm_yyyyy(model.Дата))
				res = h_validation_msg.error(res, "Дата должна быть указана в форме ДД.ММ.ГГГГ");

			if ('' == h_validation_msg.trim(model.Регион))
				res = h_validation_msg.error(res, "Регион не может быть пустым");

			if (model.Прожиточный_минимум.Несовершеннолетних.Задан && !check_money(model.Прожиточный_минимум.Несовершеннолетних.Значение))
				res = h_validation_msg.error(res, error_money);
			else if (model.Прожиточный_минимум.Пенсионеров.Задан && !check_money(model.Прожиточный_минимум.Пенсионеров.Значение))
				res = h_validation_msg.error(res, error_money);
			else if (model.Прожиточный_минимум.Подушевой.Задан && !check_money(model.Прожиточный_минимум.Подушевой.Значение))
				res = h_validation_msg.error(res, error_money);
			else if (model.Прожиточный_минимум.Трудоспособных.Задан && !check_money(model.Прожиточный_минимум.Трудоспособных.Значение))
				res = h_validation_msg.error(res, error_money);

			return res;
		}

		return controller;
	}
});