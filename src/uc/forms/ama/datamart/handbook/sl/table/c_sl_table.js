﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/handbook/sl/table/e_sl_table.html'
	, 'forms/ama/datamart/handbook/sl/record/c_sl_record'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/ama/datamart/handbook/sl/table/e_sl_table_delete_confirm.html'
	, 'forms/base/h_numbering'
	, 'forms/base/h_number_format'
	, 'forms/ama/datamart/handbook/sl/request/c_sl_request'
	, 'forms/base/h_validation_msg'
	, 'forms/base/codec/codec.copy'
],
function (c_fastened, tpl, c_sl_record, h_msgbox, tpl_remove_confirm, h_numbering, h_number_format, c_sl_request, h_validation_msg, codec_copy)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		controller.base_crud_url = controller.base_url + '?action=sl.crud';
		controller.base_grid_url = controller.base_url + '?action=sl.jqgrid';

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);
			this.RenderGrid();

			var self = this;
			this.AddButtonToPagerLeft('cpw-form-sl-grid-pager', 'Добавить запись', function () { self.OnAdd(); });
			this.AddButtonToPagerLeft('cpw-form-sl-grid-pager', 'Запросить', function () { self.OnRequest(); });

			if (app.open_section && app.open_section != "false") {
				switch (app.open_section) {
					case "sl":
						if (app.region && app.region != "") {
							$(sel + ' input#gs_Регион').val(app.region);
							app.region = '';
						}
				}
			}
		}

		var moneySumFormatter = function (cellvalue, options, rowObject)
		{
			return (cellvalue == null || cellvalue == "") ? "" : h_number_format(cellvalue, 2, ' ,', ' ');
		}

		var publicFormatter = function (cellvalue, options, rowObject)
		{
			return 0==cellvalue ? '' : 'всем';
		}

		controller.colModel =
		[
			  { name: 'id_Sub_level', hidden: true }

			, { label: 'Регион', name: 'Регион', width: 30, defaultSearch: 'cn',defaultValue: 'test', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] } }
			, { label: 'C даты', name: 'Дата', align: 'center', width: 10, defaultSearch: 'cn', search: false }

			, { label: 'Подушевой', name: 'Общий', align: 'right', width: 10, formatter: moneySumFormatter, search: false }
			, { label: 'Трудосп.', name: 'Трудоспособных', width: 10, align: 'right', formatter: moneySumFormatter, search: false }
			, { label: 'Пенсионер.', name: 'Пенсионеров', width: 10, align: 'right', formatter: moneySumFormatter, search: false }
			, { label: 'Несоверш.', name: 'Несовершеннолетних', width: 10, align: 'right', formatter: moneySumFormatter, search: false }

			, { label: 'Публично?', name: 'Публичность', width:10, align:'center', formatter: publicFormatter, searchoptions: { sopt:['eq'], value: ':;1:для всех;0:приватно'}, stype: 'select' }

			, { label: ' ', name: 'myac', width: 5, align:'right', search: false, sortable:false }
		];

		controller.PrepareUrl = function ()
		{
			return (app.region && app.region != "") ? encodeURI(this.base_grid_url + '&region=' + app.region) : this.base_grid_url;
		}

		controller.RenderGrid= function()
		{
			var sel = this.fastening.selector;
			var self= this;

			var url= self.PrepareUrl();
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Записи {0} - {1} из {2}'
				, emptyText: 'Нет записей для просмотра'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				// , rowList: [5, 10, 15]
				, pager: '#cpw-form-sl-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, multiSort: true
				, onSelectRow: function (id, s, e) 
				{ 
					self.onSelectRow_if_no_menu(id, s, e, function () { self.OnEdit(); });
				}
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка прожиточныех минимумов", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.grid_row_buttons = function ()
		{
			var self = this;
			return [
				{ "class": "edit-sl-record", text: "Редактировать", click: function () { self.OnEdit(); } }
				,{ "class": "delete-sl-record", text: "Удалить", click: function () { self.OnDelete(); }  }
			];
		};

		controller.OnRequest = function ()
		{
			var cc_sl_request = c_sl_request({ base_url: this.base_url});
			h_msgbox.ShowModal
				({
					title: 'Запросить прожиточный минимум'
					, controller: cc_sl_request
					, width: 400
					, id_div: 'cpw-form-sl-request'
					, buttons: ['Отмена']
					, onclose: function (btn) {
					}
				});
		}

		controller.OnEdit= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			$(sel + " ul.jquery-menu").hide();

			var self= this;
			var id_Sub_level= rowdata.id_Sub_level;
			var ajaxurl_get= self.base_crud_url + '&cmd=get&id=' + id_Sub_level;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение записи с сервера", ajaxurl_get);
			v_ajax.ajax
			({
				dataType: "json", type: 'GET', cache: false
				, success: function (sl_record, textStatus)
				{
					if (null == sl_record)
					{
						v_ajax.ShowAjaxError(sl_record, textStatus);
					}
					else
					{
						self.DoEdit(id_Sub_level,sl_record);
					}
				}
			});
		}

		var areEqual = function(obj1, obj2)
		{
			var obj1Keys = Object.keys(obj1);
			var obj2Keys = Object.keys(obj2);

			if (obj1Keys.length !== obj2Keys.length)
				return false;

			for (var i= 0; i<obj1Keys.length; i++)
			{
				var objKey= obj1Keys[i];
				if (obj1[objKey] !== obj2[objKey])
				{
					if (("object"!= typeof obj1[objKey]) || ("object"!=typeof obj2[objKey]))
					{
						return false;
					}
					else
					{
						if (!areEqual(obj1[objKey], obj2[objKey]))
							return false;
					}
				}
			}
			return true;
		};

		controller.DoEdit = function (id_Sub_level,sl_record)
		{
			var sel= this.fastening.selector;
			var self= this;

			var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;
			var public_record= !sl_record.ContractNumber || null==sl_record.ContractNumber;

			var cc_sl_record= c_sl_record({base_url:this.base_url,admin_mode:admin_mode});
			var old_sl_record= codec_copy().Copy(sl_record);
			cc_sl_record.SetFormContent(sl_record);
			var btnOk= 'Сохранить запись о прожиточном минимуме';
			var btnPub= 'Создать запись по этому образцу для всех ..';
			var btnCancel= 'Отмена';
			var titleEdit= 'Редактирование записи';
			var buttons= admin_mode	? (public_record ? [btnOk, btnCancel] : [btnPub, btnCancel])
									: (public_record ? ['Закрыть'] : [btnOk, btnCancel]);
			var title= admin_mode	? (public_record ? titleEdit : 'Модерирование записи')
									: (public_record ? 'Просмотр записи' : titleEdit);
			h_msgbox.ShowModal
			({
				  title:title, controller: cc_sl_record, id_div:'cpw-form-sl-record-edit', buttons:buttons
				, onclose: function(btn)
				{
					if (btn == btnPub)
					{
						delete sl_record.ContractNumber;
						self.OnAdd(sl_record);
					}
					else if (btn == btnOk)
					{
						var new_sl_record= cc_sl_record.GetFormContent();
						if (!areEqual(old_sl_record, new_sl_record))
						{
							var ajaxurl_update = self.base_crud_url + '&cmd=update&id=' + id_Sub_level;
							v_ajax = h_msgbox.ShowAjaxRequest("Сохранение записи на сервере", ajaxurl_update);
							v_ajax.ajax({
								dataType: "json", type: 'POST', cache: false, data: new_sl_record
								, success: function (responce)
								{
									if (responce.ok)
									{
										self.ReloadGrid();
										$(sel).trigger('model_change');
									}
								}
							});
						}
					}
				}
			});
		}

		controller.OnAdd= function(seed)
		{
			var sel = this.fastening.selector;
			var self= this;
			var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;
			var cc_sl_record = c_sl_record({base_url:this.base_url,admin_mode:admin_mode});
			if (!seed)
				seed= {id_Contract:this.model.id_Contract};
			cc_sl_record.SetFormContent(seed);
			var btnOk= 'Сохранить запись о прожиточном минимуме';
			h_msgbox.ShowModal
			({
				  title:'Добавление записи'
				, controller: cc_sl_record
				, width:400
				, id_div:'cpw-form-sl-record-add'
				, buttons:[btnOk, 'Отмена']
				, onclose: function (btn, dlg_div)
				{
					if (btn==btnOk)
					{
						h_validation_msg.IfOkWithValidateResult(cc_sl_record.Validate(), function ()
						{
							var ajaxurl_create = self.base_crud_url + '&cmd=add';
							v_ajax = h_msgbox.ShowAjaxRequest("Сохранение записи на сервере", ajaxurl_create);
							var sl_record = cc_sl_record.GetFormContent();
							v_ajax.ajax
								({
									dataType: "json", type: 'POST', cache: false, data: sl_record
									, success: function (responce) {
										if (responce) {
											self.ReloadGrid();
											$(sel).trigger('model_change');
										}
									}
								});
							cc_sl_record.Destroy();
							$(dlg_div).dialog("close");
						});
						return false;
					}
				}
			});
		}

		controller.ReloadGrid= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.trigger('reloadGrid');
		}

		controller.OnDelete = function ()
		{
			var self = this;
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var admin_mode = this.model && null != this.model && null != this.model.id_Admin;
			var public_record = ("всем" == rowdata.Публичность);

			var can_not_title = 'Отсутствие прав на удаление!';
			if (admin_mode && !public_record)
			{
				h_msgbox.ShowModal({ title: can_not_title, html: "Администратор не может удалять записи пользователей!", width:400 });
			}
			else if (admin_mode && !public_record || !admin_mode && public_record)
			{
				h_msgbox.ShowModal({ title: can_not_title, html: "Пользователь не может удалять опубликованные записи!", width:400 });
			}
			else
			{
				var rows_to_delete= [rowdata];
				var to_delete = [rowdata.id_Sub_level];
				var btnOk= 'Да, удалить ' + to_delete.length + h_numbering(to_delete.length,' запись',' записи',' записей');
				h_msgbox.ShowModal
				({
					html: tpl_remove_confirm(rows_to_delete), title: 'Подтверждение удаления'
					, width: '500', buttons: [btnOk, 'Нет']
					, onclose: function (bname)
					{
						if (btnOk == bname)
						{
							var ajaxurl_delete= self.base_crud_url + '&cmd=delete&id=' + to_delete.join(',');
							v_ajax = h_msgbox.ShowAjaxRequest("Удаление записей на сервере", ajaxurl_delete);
							v_ajax.ajax({
								dataType: "json", type: 'POST', cache: false
								, success: function (responce)
								{
									if (responce)
									{
										self.ReloadGrid();
										$(sel).trigger('model_change');
									}
								}
							});
						}
					}
				});
			}
		}

		return controller;
	}
});