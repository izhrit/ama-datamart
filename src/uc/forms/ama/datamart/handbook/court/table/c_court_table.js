﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/handbook/court/table/e_court_table.html'
	, 'forms/ama/datamart/handbook/court/record/c_court'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/ama/datamart/handbook/court/table/e_court_table_delete_confirm.html'
	, 'forms/base/h_numbering'
	, 'forms/base/h_validation_msg'
	, 'forms/base/codec/codec.copy'
],
function (c_fastened, tpl, c_court, h_msgbox, tpl_remove_confirm, h_numbering, h_validation_msg, codec_copy)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		controller.base_crud_url = controller.base_url + '?action=court.crud';
		controller.base_grid_url = controller.base_url + '?action=court.jqgrid';

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);
			this.RenderGrid();

			var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;
			if (admin_mode)
			{
				var self = this;
				this.AddButtonToPagerLeft('cpw-form-court-grid-pager', 'Добавить', function () { self.OnAdd(); });
			}
		}

		controller.colModel =
		[
			  { name: 'id_Court', hidden: true }
			, { label: 'Наименование', name: 'Name', width: 100 }
			, { label: ' ', name: 'myac', width: 5, align:'right', search: false, sortable:false }
		];

		controller.PrepareUrl = function ()
		{
			return this.base_grid_url;
		}

		controller.RenderGrid= function()
		{
			var sel = this.fastening.selector;
			var self= this;

			var url= self.PrepareUrl();
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Записи {0} - {1} из {2}'
				, emptyText: 'Нет записей для просмотра'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				// , rowList: [5, 10, 15]
				, pager: '#cpw-form-court-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, multiSort: true
				, onSelectRow: function (id, s, e) 
				{ 
					self.onSelectRow_if_no_menu(id, s, e, function () { self.OnEdit(); });
				}
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка судов", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.grid_row_buttons = function ()
		{
			var self = this;
			var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;
			if (!admin_mode)
			{
				return [ { "class": "edit-court-record", text: "Детали..", click: function () { self.OnEdit(); } }];
			}
			else
			{
				return [
					{ "class": "edit-court-record", text: "Редактировать..", click: function () { self.OnEdit(); } }
					,{ "class": "delete-court-record", text: "Удалить", click: function () { self.OnDelete(); }  }
				];
			}
		};

		controller.OnEdit= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			$(sel + " ul.jquery-menu").hide();

			var self= this;
			var id_Court= rowdata.id_Court;
			var ajaxurl_get= self.base_crud_url + '&cmd=get&id=' + id_Court;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение записи с сервера", ajaxurl_get);
			v_ajax.ajax
			({
				dataType: "json", type: 'GET', cache: false
				, success: function (court, textStatus)
				{
					if (null == court)
					{
						v_ajax.ShowAjaxError(court, textStatus);
					}
					else
					{
						self.DoEdit(id_Court,court);
					}
				}
			});
		}

		var areEqual = function(obj1, obj2)
		{
			var obj1Keys = Object.keys(obj1);
			var obj2Keys = Object.keys(obj2);

			if (obj1Keys.length !== obj2Keys.length)
			{
				return false;
			}

			for (var i= 0; i<obj1Keys.length; i++)
			{
				var objKey= obj1Keys[i];
				if (obj1[objKey] != obj2[objKey])
				{
					if (("object"!= typeof obj1[objKey]) || ("object"!=typeof obj2[objKey]))
					{
						return false;
					}
					else
					{
						if (!areEqual(obj1[objKey], obj2[objKey]))
							return false;
					}
				}
			}
			return true;
		};

		controller.DoEdit = function (id_Court,court)
		{
			var sel= this.fastening.selector;
			var self= this;

			var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;

			var cc_court= c_court({admin_mode:admin_mode});
			var old_court= codec_copy().Copy(court);
			cc_court.SetFormContent(court);
			var btnOk= 'Сохранить запись об арбитражном суде';
			var buttons= !admin_mode ? ['Закрыть'] : [btnOk, 'Отмена'];
			var title= !admin_mode	? 'Просмотр записи о суде'
									: 'Редактирование записи об арбитражном суде';
			h_msgbox.ShowModal
			({
				  title:title, controller: cc_court, id_div:'cpw-form-court-edit', buttons:buttons
				, onclose: function(btn)
				{
					if (btn == btnOk)
					{
						var new_court= cc_court.GetFormContent();
						if (!areEqual(old_court, new_court))
						{
							var ajaxurl_update = self.base_crud_url + '&cmd=update&id=' + id_Court;
							v_ajax = h_msgbox.ShowAjaxRequest("Сохранение записи на сервере", ajaxurl_update);
							v_ajax.ajax({
								dataType: "json", type: 'POST', cache: false, data: new_court
								, success: function (responce)
								{
									if (responce.ok)
									{
										self.ReloadGrid();
										$(sel).trigger('model_change');
									}
								}
							});
						}
					}
				}
			});
		}

		controller.OnAdd= function(seed)
		{
			var sel = this.fastening.selector;
			var self= this;
			var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;
			var cc_court= c_court({base_url:this.base_url,admin_mode:admin_mode});
			if (!seed)
				seed= {id_Contract:this.model.id_Contract};
			cc_court.SetFormContent(seed);
			var btnOk= 'Сохранить запись об арбитражном суде';
			h_msgbox.ShowModal
			({
				  title:'Добавление записи об арбитражном суде'
				, controller: cc_court
				, width:400
				, id_div:'cpw-form-court-record-add'
				, buttons:[btnOk, 'Отмена']
				, onclose: function (btn, dlg_div)
				{
					if (btn==btnOk)
					{
						h_validation_msg.IfOkWithValidateResult(cc_court.Validate(), function ()
						{
							var ajaxurl_create = self.base_crud_url + '&cmd=add';
							v_ajax = h_msgbox.ShowAjaxRequest("Сохранение записи на сервере", ajaxurl_create);
							var court= cc_court.GetFormContent();
							v_ajax.ajax
								({
									dataType: "json", type: 'POST', cache: false, data: court
									, success: function (responce) {
										if (responce) {
											self.ReloadGrid();
											$(sel).trigger('model_change');
										}
									}
								});
							cc_court.Destroy();
							$(dlg_div).dialog("close");
						});
						return false;

					}
				}
			});
		}

		controller.ReloadGrid= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.trigger('reloadGrid');
		}

		controller.OnDelete = function ()
		{
			var self = this;
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var rows_to_delete= [rowdata];
			var to_delete = [rowdata.id_Court];
			var btnOk= 'Да, удалить ' + to_delete.length + h_numbering(to_delete.length,' запись',' записи',' записей');
			h_msgbox.ShowModal
			({
				html: tpl_remove_confirm(rows_to_delete), title: 'Подтверждение удаления'
				, width: '500', buttons: [btnOk, 'Нет']
				, onclose: function (bname)
				{
					if (btnOk == bname)
					{
						var ajaxurl_delete= self.base_crud_url + '&cmd=delete&id=' + to_delete.join(',');
						v_ajax = h_msgbox.ShowAjaxRequest("Удаление записей на сервере", ajaxurl_delete);
						v_ajax.ajax({
							dataType: "json", type: 'POST', cache: false
							, success: function (responce)
							{
								if (responce)
								{
									self.ReloadGrid();
									$(sel).trigger('model_change');
								}
							}
						});
					}
				}
			});
		}

		return controller;
	}
});