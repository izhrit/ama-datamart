include ..\in.lib.txt quiet

wait_text "Тэг"
shot_check_png ..\..\shots\00new.png

play_stored_lines court_fields_1

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01sav.json.result.txt

wait_click_text "Редактировать модель"
shot_check_png ..\..\shots\01sav.png

exit