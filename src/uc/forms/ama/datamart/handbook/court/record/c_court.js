define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/handbook/court/record/e_court.html'
],
function (c_fastened, tpl)
{
	return function (options_arg)
	{
		var admin_mode= (null==options_arg) ? true : options_arg.admin_mode;

		var controller = c_fastened(tpl, { admin_mode:admin_mode });

		controller.size = {width:550, height:510};

		var base_Render= controller.Render;
		controller.Render = function(sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' button.show-location-on-map').click(function() { self.OnShowLocationOnMap(); });
		}

		controller.OnShowLocationOnMap= function() 
		{
			var sel= this.fastening.selector;
			var address= $(sel + ' #cpw-court-address-textarea').val();
			var url= 'https://yandex.ru/maps/?text=' + encodeURI(address);
			window.open(url);
		}

		return controller;
	}
});