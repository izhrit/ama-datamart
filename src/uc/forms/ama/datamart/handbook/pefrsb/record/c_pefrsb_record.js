define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/handbook/pefrsb/record/e_pefrsb_record.html'
	, 'forms/ama/datamart/base/h_region'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_region,h_validation_msg)
{
	return function (options_arg)
	{
		var admin_mode = (null == options_arg) ? true : options_arg.admin_mode;

		var controller = c_fastened(tpl, {regions:h_region, admin_mode:admin_mode });

		controller.size = {width:650, height:350};

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' button.gotourl').click(function () { self.OnGotoUrl(); });

			$(sel + ' input[data-fc-selector="Сумма_за_сообщение.Физлицо.Значение"]').change(function () { self.OnChangeMoney("Сумма_за_сообщение.Физлицо.Значение"); });
			$(sel + ' input[data-fc-selector="Сумма_за_сообщение.Физлицо_с_комиссией.Значение"]').change(function () { self.OnChangeMoney("Сумма_за_сообщение.Физлицо_с_комиссией.Значение"); });
			$(sel + ' input[data-fc-selector="Сумма_за_сообщение.Юрлицо.Значение"]').change(function () { self.OnChangeMoney("Сумма_за_сообщение.Юрлицо.Значение"); });
			$(sel + ' input[data-fc-selector="Сумма_за_сообщение.Юрлицо_с_комиссией.Значение"]').change(function () { self.OnChangeMoney("Сумма_за_сообщение.Юрлицо_с_комиссией.Значение"); });
		}
		controller.OnChangeMoney = function (input) {
			var sel = this.fastening.selector;
			var data = $(sel + ' input[data-fc-selector="'+input+'"]').val();
			data = data.replace(/\D/, '.');
			var regxp1 = /(\d{1,})\.(\d{1,2})/g;
			var regxp2 = /(\d{1,})/g;
			var matches1 = data.match(regxp1);
			var matches2 = data.match(regxp2);
			$(sel + ' input[data-fc-selector="' + input + '"]').val(matches1 ? matches1[0] : (matches2 ? matches2[0]+'.00' : ''));
		}



		var check_money = function (str) {
			var regexp = /^(\d{1,})\.(\d{1,2})$/;
			return regexp.test(str);
		}

		var check_date_dd_mm_yyyyy = function (str) {
			var regexp = /^(\d{2})\.(\d{2})\.(\d{4})$/;
			return regexp.test(str);
		}

		controller.Validate = function () {
			var res = null;
			var model = this.GetFormContent();
			var error_money = "Значение цены должно быть указано по типу 100.59 (где 100 - рубли, 59 - копейки)";
			if (!check_date_dd_mm_yyyyy(model.Дата))
				res = h_validation_msg.error(res, "Дата должна быть указана в форме ДД.ММ.ГГГГ");

			if (model.Сумма_за_сообщение.Физлицо.Задан && !check_money(model.Сумма_за_сообщение.Физлицо.Значение))
				res = h_validation_msg.error(res, error_money);
			else if (model.Сумма_за_сообщение.Юрлицо.Задан && !check_money(model.Сумма_за_сообщение.Юрлицо.Значение))
				res = h_validation_msg.error(res, error_money);
			else if (model.Сумма_за_сообщение.Физлицо_с_комиссией.Задан && !check_money(model.Сумма_за_сообщение.Физлицо_с_комиссией.Значение))
				res = h_validation_msg.error(res, error_money);
			else if (model.Сумма_за_сообщение.Юрлицо_с_комиссией.Задан && !check_money(model.Сумма_за_сообщение.Юрлицо_с_комиссией.Значение))
				res = h_validation_msg.error(res, error_money);	
			return res;
		}

		return controller;
	}
});