﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/handbook/pefrsb/table/e_pefrsb_table.html'
	, 'forms/ama/datamart/handbook/pefrsb/record/c_pefrsb_record'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/ama/datamart/handbook/pefrsb/table/e_pefrsb_table_delete_confirm.html'
	, 'forms/base/h_numbering'
	, 'forms/base/h_number_format'
	, 'forms/base/h_validation_msg'
	, 'forms/base/codec/codec.copy'
],
function (c_fastened, tpl, c_pefrsb_record, h_msgbox, tpl_remove_confirm, h_numbering, h_number_format, h_validation_msg, codec_copy)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		controller.base_crud_url = controller.base_url + '?action=pefrsb.crud';
		controller.base_grid_url = controller.base_url + '?action=pefrsb.jqgrid';

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);
			this.RenderGrid();

			var self = this;
			this.AddButtonToPagerLeft('cpw-form-pefrsb-grid-pager', 'Добавить запись', function () { self.OnAdd(); });

			
		}

		var moneySumFormatter = function (cellvalue, options, rowObject)
		{
			return (cellvalue == null || cellvalue == "") ? "" : h_number_format(cellvalue, 2, ' ,', ' ');
		}

		controller.colModel =
		[
			  { name: 'id_pefrsb', hidden: true }

			, { label: 'Начиная с даты', name: 'Дата', align: 'left', width: 9, defaultSearch: 'cn', search: false }

			, { label: 'за физических лиц', name: 'Физлицо', width: 5, align: 'right', formatter: moneySumFormatter, search: false }
			, { label: 'за физических лиц с учётом комиссии банка', name: 'Физлицо_с_комиссией', width: 10, align: 'right', formatter: moneySumFormatter, search: false }
			, { label: 'за юридических лиц', name: 'Юрлицо', align: 'right', width: 5, formatter: moneySumFormatter, search: false }
			, { label: 'за юридических лиц с учётом комиссии банка', name: 'Юрлицо_с_комиссией', align: 'right', width: 10, formatter: moneySumFormatter, search: false }			

			, { label: ' ', name: 'myac', width: 3, align:'right', search: false, sortable:false }
		];

		controller.PrepareUrl = function ()
		{
			return (app.price_efrsb && app.price_efrsb != "") ? encodeURI(this.base_grid_url + '&price_efrsb=' + app.price_efrsb) : this.base_grid_url;
		}

		controller.RenderGrid= function()
		{
			var sel = this.fastening.selector;
			var self= this;

			var url= self.PrepareUrl();
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Записи {0} - {1} из {2}'
				, emptyText: 'Нет записей для просмотра'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				// , rowList: [5, 10, 15]
				, pager: '#cpw-form-pefrsb-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, multiSort: true
				, onSelectRow: function (id, s, e) 
				{ 
					self.onSelectRow_if_no_menu(id, s, e, function () { self.OnRead(); });
				}
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка cтоимости публикации сообщения", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.grid_row_buttons = function ()
		{
			var self = this;
			return [
				{ "class": "edit-pefrsb-record", text: "Редактировать", click: function () { self.OnEdit(); } }
				,{ "class": "delete-pefrsb-record", text: "Удалить", click: function () { self.OnDelete(); }  }
			];
		};
		controller.OnRead = function () {
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			$(sel + " ul.jquery-menu").hide();

			var self = this;
			var id_pefrsb = rowdata.id_pefrsb;
			var ajaxurl_get = self.base_crud_url + '&cmd=get&id=' + id_pefrsb;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение записи с сервера", ajaxurl_get);
			v_ajax.ajax
			({
				dataType: "json", type: 'GET', cache: false
				, success: function (pefrsb_record, textStatus)
				{
					if (null == pefrsb_record)
					{
						v_ajax.ShowAjaxError(pefrsb_record, textStatus);
					}
					else
					{
						self.DoRead(id_pefrsb, pefrsb_record);
					}
				}
			});
		}

		controller.OnEdit= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			$(sel + " ul.jquery-menu").hide();

			var self= this;
			var id_pefrsb= rowdata.id_pefrsb;
			var ajaxurl_get= self.base_crud_url + '&cmd=get&id=' + id_pefrsb;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение записи с сервера", ajaxurl_get);
			v_ajax.ajax
			({
				dataType: "json", type: 'GET', cache: false
				, success: function (pefrsb_record, textStatus)
				{
					if (null == pefrsb_record)
					{
						v_ajax.ShowAjaxError(pefrsb_record, textStatus);
					}
					else
					{
						self.DoEdit(id_pefrsb,pefrsb_record);
					}
				}
			});
		}

		var areEqual = function(obj1, obj2)
		{
			var obj1Keys = Object.keys(obj1);
			var obj2Keys = Object.keys(obj2);

			if (obj1Keys.length !== obj2Keys.length)
				return false;

			for (var i= 0; i<obj1Keys.length; i++)
			{
				var objKey= obj1Keys[i];
				if (obj1[objKey] !== obj2[objKey])
				{
					if (("object"!= typeof obj1[objKey]) || ("object"!=typeof obj2[objKey]))
					{
						return false;
					}
					else
					{
						if (!areEqual(obj1[objKey], obj2[objKey]))
							return false;
					}
				}
			}
			return true;
		};

		controller.DoEdit = function (id_pefrsb, pefrsb_record) {
			var sel = this.fastening.selector;
			var self = this;

			var admin_mode = this.model && null != this.model && null != this.model.id_Admin;

			var cc_pefrsb_record = c_pefrsb_record({ base_url: this.base_url, admin_mode: admin_mode });
			var old_pefrsb_record = codec_copy().Copy(pefrsb_record);
			cc_pefrsb_record.SetFormContent(pefrsb_record);
			var btnOk = 'Сохранить запись о цене ЕФРСБ';
			var buttons = [btnOk, 'Отмена'];
			var title = 'Редактирование записи цены ЕФРСБ';

			h_msgbox.ShowModal
				({
					title: title, controller: cc_pefrsb_record, id_div: 'cpw-form-pefrsb-record-edit', buttons: buttons
					, onclose: function (btn) {
						if (btn == btnOk) {
							h_validation_msg.IfOkWithValidateResult(cc_pefrsb_record.Validate(), function () {
								var new_pefrsb_record = cc_pefrsb_record.GetFormContent();
								if (!areEqual(old_pefrsb_record, new_pefrsb_record)) {
									var ajaxurl_update = self.base_crud_url + '&cmd=update&id=' + id_pefrsb;
									v_ajax = h_msgbox.ShowAjaxRequest("Сохранение записи на сервере", ajaxurl_update);
									v_ajax.ajax({
										dataType: "json", type: 'POST', cache: false, data: new_pefrsb_record
										, success: function (responce) {
											if (responce.ok) {
												self.ReloadGrid();
												$(sel).trigger('model_change');
											}
										}
									});
								}
							});
							return false;
						}
					}
				});
		}

		controller.DoRead = function (id_pefrsb, pefrsb_record) {
			var sel = this.fastening.selector;
			var self = this;

			var admin_mode = this.model && null != this.model && null != this.model.id_Admin;

			var cc_pefrsb_record = c_pefrsb_record({ base_url: this.base_url, admin_mode: admin_mode });
			var old_pefrsb_record = codec_copy().Copy(pefrsb_record);
			cc_pefrsb_record.SetFormContent(pefrsb_record);
			var btnOk = 'Сохранить запись о цене ЕФРСБ';
			var buttons = ['Закрыть'];
			var title = 'Просмотр записи о цене ЕФРСБ';

			h_msgbox.ShowModal
				({
					title: title, controller: cc_pefrsb_record, id_div: 'cpw-form-pefrsb-record-edit', buttons: buttons
					, onclose: function (btn) {
						
					}
				});
		}

		controller.OnAdd= function(seed)
		{
			var sel = this.fastening.selector;
			var self= this;
			var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;
			var cc_pefrsb_record = c_pefrsb_record({base_url:this.base_url,admin_mode:admin_mode});
			if (!seed)
				seed= {id_pefrsb:this.model.id_pefrsb};
			cc_pefrsb_record.SetFormContent(seed);
			var btnOk= 'Сохранить запись о цене ЕФРСБ';
			h_msgbox.ShowModal
			({
				  title:'Добавление записи'
				, controller: cc_pefrsb_record
				, width:400
				, id_div:'cpw-form-sl-record-add'
				, buttons:[btnOk, 'Отмена']
				, onclose: function (btn, dlg_div)
				{
					if (btn==btnOk)
					{
						h_validation_msg.IfOkWithValidateResult(cc_pefrsb_record.Validate(), function ()
						{
							var ajaxurl_create = self.base_crud_url + '&cmd=add';
							v_ajax = h_msgbox.ShowAjaxRequest("Сохранение записи на сервере", ajaxurl_create);
							var pefrsb_record = cc_pefrsb_record.GetFormContent();
							v_ajax.ajax
								({
									dataType: "json", type: 'POST', cache: false, data: pefrsb_record
									, success: function (responce) {
										if (responce) {
											self.ReloadGrid();
											$(sel).trigger('model_change');
										}
									}
								});
							cc_pefrsb_record.Destroy();
							$(dlg_div).dialog("close");
						});
						return false;
					}
				}
			});
		}

		controller.ReloadGrid= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.trigger('reloadGrid');
		}

		controller.OnDelete = function ()
		{
			var self = this;
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var admin_mode = this.model && null != this.model && null != this.model.id_Admin;
			var public_record = ("всем" == rowdata.Публичность);

			var can_not_title = 'Отсутствие прав на удаление!';
			if (admin_mode && !public_record)
			{
				h_msgbox.ShowModal({ title: can_not_title, html: "Администратор не может удалять записи пользователей!", width:400 });
			}
			else if (admin_mode && !public_record || !admin_mode && public_record)
			{
				h_msgbox.ShowModal({ title: can_not_title, html: "Пользователь не может удалять опубликованные записи!", width:400 });
			}
			else
			{
				var rows_to_delete= [rowdata];
				var to_delete = [rowdata.id_pefrsb];
				var btnOk= 'Да, удалить ' + to_delete.length + h_numbering(to_delete.length,' запись',' записи',' записей');
				h_msgbox.ShowModal
				({
					html: tpl_remove_confirm(rows_to_delete), title: 'Подтверждение удаления'
					, width: '500', buttons: [btnOk, 'Нет']
					, onclose: function (bname)
					{
						if (btnOk == bname)
						{
							var ajaxurl_delete= self.base_crud_url + '&cmd=delete&id=' + to_delete.join(',');
							v_ajax = h_msgbox.ShowAjaxRequest("Удаление записей на сервере", ajaxurl_delete);
							v_ajax.ajax({
								dataType: "json", type: 'POST', cache: false
								, success: function (responce)
								{
									if (responce)
									{
										self.ReloadGrid();
										$(sel).trigger('model_change');
									}
								}
							});
						}
					}
				});
			}
		}

		return controller;
	}
});