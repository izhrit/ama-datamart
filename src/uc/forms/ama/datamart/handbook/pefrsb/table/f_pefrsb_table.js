define(['forms/pefrsb/table/c_pefrsb_table'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'table'
		, Title: 'Таблица цен ЕФРСБ'
	};
	return form_spec;
});
