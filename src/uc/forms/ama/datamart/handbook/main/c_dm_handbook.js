﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/handbook/main/e_dm_handbook.html'
	, 'forms/ama/datamart/handbook/sl/table/c_sl_table'
	, 'forms/ama/datamart/handbook/court/table/c_court_table'
	, 'forms/ama/datamart/handbook/wcalendar/year_editor/c_year_editor'
	, 'forms/ama/datamart/handbook/rosreestr/table/c_rosreestr_table'
],
function (c_fastened, tpl, c_sl_table, c_court_table, c_year_editor, c_rosreestr_table)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var options = {
			field_spec:
				{
					  Прожиточный_минимум: { controller: function () { return c_sl_table({ base_url: base_url }); }, render_on_activate:true }
					, Арбитражные_суды: { controller: function () { return c_court_table({ base_url: base_url }); }, render_on_activate:true }
					, Производственный_календарь: { controller: function () { return c_year_editor({ base_url: base_url }); }, render_on_activate: true }
					, Подразделения_росреестра: { controller: function () { return c_rosreestr_table({ base_url: base_url }); }, render_on_activate: true }
				}
		};

		var controller = c_fastened(tpl, options);

		return controller;
	}
});