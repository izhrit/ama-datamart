define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/login/admin/e_admin_login.html'
	, 'forms/ama/datamart/login/form/c_login_form'
	, 'tpl!forms/ama/datamart/login/form/e_login_form_admin.html'
],
function (c_fastened, tpl, c_login_form, e_login_form_admin)
{
	return function(options_arg)
	{
		var options = {
			field_spec:
				{
					Администратор: { controller: function () { return c_login_form(e_login_form_admin); } }
				}
		};

		var base_base_url = (options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart';
		var base_url = base_base_url + '?action=login';

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.RenderAdmin();

			$(sel + ' #cpw-ama-datamart-login-1 input[name="Login"]').focus();
		}

		controller.RenderAdmin= function()
		{
			var self = this;
			var datamart = this.fastening.get_fc_controller('Администратор');
			datamart.service_url = base_url + '&as=admin';
			datamart.base_url = base_base_url;
			datamart.OnLogin = function (login_data)
			{
				if (self.OnLogin)
					self.OnLogin({ Администратор: login_data });
			}
		}

		return controller;
	}
});
