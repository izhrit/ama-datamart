define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/login/main/e_login.html'
	, 'forms/ama/datamart/login/form/c_login_form'
	, 'tpl!forms/ama/datamart/login/form/e_login_form_customer.html'
	, 'tpl!forms/ama/datamart/login/form/e_login_form_manager.html'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/ama/datamart/login/main/v_login_manager_not_email.html'
	, 'tpl!forms/ama/datamart/login/form/e_login_form_viewer.html'
	, 'tpl!forms/ama/datamart/login/main/validation_error/v_login_viewer_from_fa.html'
	, 'tpl!forms/ama/datamart/login/form/e_login_form_sro.html'
],
function (c_fastened, tpl, c_login_form, e_login_form_customer, e_login_form_manager, h_msgbox, v_login_manager_not_email, e_login_form_viewer, v_login_viewer_from_fa, e_login_form_sro)
{
	return function(options_arg)
	{
		var options = {
			field_spec:
				{
					КабинетАУ: { controller: function () { return c_login_form(e_login_form_manager); } }
					, Витрина: { controller: function () { return c_login_form(e_login_form_viewer); } }
					, КабинетАбонента: { controller: function () { return c_login_form(e_login_form_customer); } }
					, КабинетСРО: { controller: function () { return c_login_form(e_login_form_sro); } }
				}
		};

		var base_base_url = (options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart';
		var base_url = base_base_url + '?action=login';

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.RenderManager();
			this.RenderViewer();
			this.RenderCustomer();
			this.RenderSRO();

			$(sel + ' #cpw-ama-datamart-login-2 input[name="Login"]').focus();

			var self= this;
			$(sel + ' div#cpw-ama-datamart-login-tabs').on("tabsactivate", function (event, ui) { self.OnTabActivate(event, ui); });

			if (app.open_section == "cannot_be_opened_from_fa")
			{
				h_msgbox.ShowModal({
					title: 'Пояснение о невозможности входа',
					width: 800, height: 200,
					html: v_login_viewer_from_fa()
				});
			}
		}

		controller.OnTabActivate= function(event, ui)
		{
			var sel = this.fastening.selector;
			$(sel + ' ' + ui.newPanel.selector + ' input[name="Login"]').focus();
		}

		controller.RenderManager= function()
		{
			var self = this;
			var cabinet = this.fastening.get_fc_controller('КабинетАУ')
			cabinet.service_url = base_url + '&as=manager&platform=web';
			cabinet.base_url = base_base_url;
			cabinet.OnLogin = function (login_data)
			{
				if (self.OnLogin)
					self.OnLogin({ КабинетАУ: login_data });
			}
			var base_OnCtrlLogin = cabinet.OnCtrlLogin;
			cabinet.OnCtrlLogin = function ()
			{
				var login_data = this.GetFormContent();
				if (login_data && login_data.Login && null != login_data.Login && 0 != login_data.Login.length)
				{
					if (-1 == login_data.Login.indexOf('@'))
					{
						h_msgbox.ShowModal({ title: 'Неверные данные', width:550, html: v_login_manager_not_email() });
						return;
					}
				}
				base_OnCtrlLogin.call(this);
			}
		}

		controller.RenderViewer= function()
		{
			var self = this;
			var datamart = this.fastening.get_fc_controller('Витрина');
			datamart.service_url = base_url + '&as=viewer&platform=web';
			datamart.base_url = base_base_url;
			datamart.OnLogin = function (login_data)
			{
				if (self.OnLogin)
					self.OnLogin({ Витрина: login_data });
			}
		}

		controller.RenderSRO= function()
		{
			var self = this;
			var cabinent = this.fastening.get_fc_controller('КабинетСРО');
			cabinent.service_url = base_url + '&as=sro&platform=web';
			cabinent.OnLogin = function (login_data)
			{
				if (self.OnLogin)
					self.OnLogin({ КабинетСРО: login_data });
			}
		}

		controller.RenderCustomer= function()
		{
			var self = this;
			var cabinent = this.fastening.get_fc_controller('КабинетАбонента');
			cabinent.service_url = base_url + '&as=customer&platform=web';
			cabinent.OnLogin = function (login_data)
			{
				if (self.OnLogin)
					self.OnLogin({ КабинетАбонента: login_data });
			}
		}

		controller.AutoLogin = function (category, id)
		{
			var category_model_field_name=
			{
				  'manager': 'КабинетАУ'
				, 'customer': 'КабинетАбонента'
				, 'viewer': 'Витрина'
				, 'sro': 'КабинетСРО'
			};
			var model_field_name= category_model_field_name[category];
			var cabinet = this.fastening.get_fc_controller(model_field_name);
			cabinet.AutoLogin({id:id,category:category});
		}

		return controller;
	}
});
