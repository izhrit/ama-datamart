﻿define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/base/h_cryptoapi'

	, 'tpl!forms/ama/datamart/login/form/v_choosed_cert_without_inn.html'
	, 'tpl!forms/ama/datamart/login/form/v_au_not_found.html'
	, 'tpl!forms/ama/datamart/login/form/v_auth_signature_bad.html'
	, 'tpl!forms/ama/datamart/login/form/v_capicom_undefined.html'
	, 'tpl!forms/ama/datamart/login/form/v_cert_canceled_by_user.html'

	, 'forms/ama/datamart/login/form/c_login_by_cert'
],
function (h_msgbox, h_cryptoapi
	, v_choosed_cert_without_inn, v_au_not_found, v_auth_signature_bad, v_capicom_undefined, v_cert_canceled_by_user, c_login_by_cert)
{
	var helper = {};

	helper.Login= function (base_url, on_logged)
	{
		try
		{
			var self = this;
			app.cryptoapi.ChooseCertificateAnd(function (cert)
			{
				self.LoginByCert(cert,base_url, on_logged);
			}
			);
		}
		catch (ex)
		{
			var description= ex.description ? ex.description : ex.message;
			if (!description || null==description)
				throw ex;
			if (-1 != description.indexOf('ActiveXObject is not defined'))
			{
				h_msgbox.ShowModal({width:460, title:'Аутентификация неудачна',html: v_capicom_undefined()});
			}
			else if (-1 != description.indexOf('cancelled by the user'))
			{
				h_msgbox.ShowModal({width:460, title:'Аутентификация неудачна',html: v_cert_canceled_by_user()});
			}
			else
			{
				throw ex;
			}
		}
	}

	helper.LoginByCert = function (cert,base_url, on_logged)
	{
		var dn_fields = h_cryptoapi.GetDNFields(cert.SubjectName);
		if (!dn_fields.ИНН)
		{
			h_msgbox.ShowModal({ width: 460, title: 'Аутентификация невозможна', html: v_choosed_cert_without_inn(dn_fields) });
		}
		else
		{
			var self = this;
			var url = base_url + '?action=manager.cert.login&cmd=get-token-to-sign';
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение токена авторизации на подпись", url);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: { SerialNumber: cert.SerialNumber, Issuer: cert.IssuerName, Subject: cert.SubjectName }
				, success: function (data, textStatus)
				{	
					if (null == data || !data.token)
					{
						h_msgbox.ShowModal({ width: 460, title: 'Аутентификация неудачна', html: v_au_not_found(dn_fields) });
					}
					else
					{
						self.LoginByCertToken(cert, data.token, base_url, on_logged);
					}
				}
			});
		}
	}

	helper.LoginByCertToken = function (cert, token, base_url, on_logged)
	{
		var self= this;

		var cc_login_by_cert= c_login_by_cert();
		cc_login_by_cert.SetFormContent(token);

		var btn_sign= 'Подписать запрос на доступ и войти в систему';
		h_msgbox.ShowModal({
			title:'Подписание запроса на доступ к информации'
			,buttons:[btn_sign,'Закрыть']
			,id_div:'cpw-form-orpau-auth-token',width:600,height:700
			,controller:cc_login_by_cert
			,onclose: function (btn)
			{
				if (btn_sign == btn)
				{
					var token_variant= token.token;
					var base64_encoded_signature = app.cryptoapi.SignBase64(token_variant,/*detached=*/true, cert);
					self.LoginByCertTokenSignature(cert, token_variant, base64_encoded_signature, base_url, on_logged);
				}
			}
		});
	}

	helper.LoginByCertTokenSignature = function (cert, token_variant, base64_encoded_signature, base_url, on_logged)
	{
		var url = base_url + '?action=manager.cert.login&cmd=auth-by-token-signature';
		var v_ajax = h_msgbox.ShowAjaxRequest("Отправка подписи токена авторизации на сервер", url);
		v_ajax.ajax
		({
			dataType: "json"
			, type: 'POST'
			, cache: false
			, data: { base64_encoded_signature: base64_encoded_signature }
			, success: function (auth_info, textStatus)
			{
				if (null == auth_info || !auth_info.id_Manager)
				{
					h_msgbox.ShowModal({ width: 460, title: 'Аутентификация неудачна', html: v_auth_signature_bad() });
				}
				else
				{
					on_logged(auth_info);
				}
			}
		});
	}

	return helper;
});