define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/login/form/e_login_form.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/viewer/register/c_dm_viewer_register'
	, 'forms/base/h_validation_msg'
	, 'forms/ama/datamart/login/form/h_login_by_cert'
],
	function (c_fastened, tpl, h_msgbox, c_dm_viewer_register, h_validation_msg, h_login_by_cert)
{
	return function(tpl_param)
	{
		var controller = c_fastened(tpl_param ? tpl_param : tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.login').click(function (e) { e.preventDefault(); self.OnCtrlLogin(); });
			$(sel + ' button.certificate-login').click(function (e) { e.preventDefault(); self.OnLoginByCert(); });

			$(sel + ' button.register').click(function (e) { e.preventDefault(); self.OnCtrlRegister(); });

			$(sel + ' div.forgot-password').click(function (e) { e.preventDefault(); self.OnCtrlPassword(); });
			$(sel + ' .icon-password-toggle').click(function (e) {
				var input = $(sel + ' div.password-input-container input');
				if(input.attr('type') == 'password'){
					input.prop('type', 'text');
					$(this).prop('title', 'Скрыть пароль');
				} else {
					input.prop('type', 'password');
					$(this).prop('title', 'Показать пароль');
				}
			});
		}

		controller.OnLoginByCert = function ()
		{
			var self = this;

			h_login_by_cert.Login(this.base_url
				, function(auth_info)
				{
					self.OnLoggedCert(auth_info);
				}
			);
		}

		controller.OnLoggedCert = function (auth_info) {
			var self = this;
			if (self.OnLogin)
				self.OnLogin(auth_info);
		}

		controller.OnCtrlLogin= function()
		{
			var login_data = this.GetFormContent();
			var self = this;
			var v_ajax= h_msgbox.ShowAjaxRequest("Запрос авторизации на сервере", this.service_url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: login_data
				, success: function (responce, textStatus)
				{
					if (null==responce)
					{
						h_msgbox.ShowModal({
							width: 500
							, title: 'Неудачная аутентификация'
							, html: 'Вы ввели неправильное имя пользователя или пароль!'
						});
					}
					else if (false == responce.ok)
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка аутентификации'
							, width: 450
							, html: '<span>Не удалось провести аутентификацию по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
					else
					{
						if (self.OnLogin)
							self.OnLogin(responce);
					}
				}
			});
		}

		controller.AutoLogin = function (login_data)
		{
			login_data.Login = '';
			login_data.Password= '';
			var self = this;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос авто авторизации на сервере", this.service_url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: login_data
				, success: function (responce, textStatus)
				{
					if (null != responce && false != responce.ok)
					{
						if (self.OnLogin)
							self.OnLogin(responce);
					}
				}
			});
		}

		controller.OnCtrlPassword= function()
		{
			var login_data = this.GetFormContent();
			if (!login_data.Login || null == login_data.Login || '' == login_data.Login)
			{
				h_msgbox.ShowModal({
					width: 500
					, title: 'Запрос на смену пароля'
					, html: 'Укажите E-mail для которого хотите запросить новый пароль.'
				});
			}
			else
			{
				var self = this;
				var v_ajax = h_msgbox.ShowAjaxRequest("Запрос смены пароля на сервере", this.service_url + '&passwordcmd=change');
				v_ajax.ajax({
					dataType: "json"
					, type: 'POST'
					, cache: false
					, data: login_data
					, success: function (data, textStatus)
					{
						h_msgbox.ShowModal({
							width: 600
							, title: 'Запрос на смену пароля'
							, html: 'Сервер обработал запрос на смену пароля для логина "'
								+ login_data.Login
								+ '".<br/> Если такая учётная запись существует, на этот адрес выслан новый пароль'
						});
					}
				});
			}
		}

		controller.OnCtrlRegister= function()
		{
			var self = this;
			var c_register = c_dm_viewer_register({ base_url: this.base_url });
			c_register.SetFormContent({});
			var btnOk = 'Подать заявку на регистрацию';
			setTimeout(function ()
			{
				h_msgbox.ShowModal({
					title: 'Регистрация пользователя Витрины данных ПАУ'
					, controller: c_register
					, buttons: [btnOk, 'Отмена']
					, id_div: "cpw-form-ama-datamart-register"
					, onclose: function (btn, dlg_div)
					{
						if (btn == btnOk)
						{
							h_validation_msg.IfOkWithValidateResult(c_register.Validate(), function () {
								var register_data = c_register.GetFormContent();
								self.Register(register_data, function () {
									c_register.Destroy();
									$(dlg_div).dialog("close");
								});
							});
							return false;
						}
					}
				});
			}, 200);
		}

		controller.Register = function (register_data,on_done)
		{
			register_data.Запрос_на_доступ.Должник.Наименование = register_data.Запрос_на_доступ.Выбранная_процедура.data.Должник.Наименование;
			var self = this;
			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса на доступ на сервер", this.base_url + '?action=viewer.register');
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: register_data
				, success: function (data, textStatus)
				{
					if (!data.ok)
					{
						h_msgbox.ShowModal({ title: 'Ошибка регистрации', html: data.reason });
					}
					else
					{
						on_done();
						h_msgbox.ShowModal({
							width: 600
							, title: 'Запрос на доступ передан на сервер'
							, html: 'Сервер обработал запрос на доступ.<br/>'
								+ 'Письмо о предоставлении или непредоставлении доступа<br/>'
								+ 'будет выслано Вам на адрес электронной почты <b>"' + register_data.Заявитель.Email + '"</b>.'
						});
					}
				}
			});
		}

		return controller;
	}
});
