define(function ()
{

	return {

		controller: {
			"dm_sro_cabinet": {
				path: 'forms/ama/datamart/sro/main/c_sro_cabinet'
				, title: 'главная форма СРО АУ'
			}
			, "dm_sro_consent": {
				path: 'forms/ama/datamart/sro/consent/c_consent'
				, title: 'согласие АУ'
			}
			, "dm_sro_request": {
				path: 'forms/ama/datamart/sro/request/c_request_wrapper'
				, title: 'запрос кандидатур АУ'
			}
			, "dm_sro_requests": {
				path: 'forms/ama/datamart/sro/requests/c_dm_requests'
				, title: 'запросы кандидатур АУ'
			}
			, "dm_sro_requests_params": {
				path: 'forms/ama/datamart/sro/requests_params/c_dm_requests_params'
				, title: 'настройки отображения запросов'
			}
			, "dm_sro_consents": {
				path: 'forms/ama/datamart/sro/consents/c_dm_consents'
				, title: 'согласия АУ'
			}
			, "dm_sro_managers": {
				path: 'forms/ama/datamart/sro/managers/c_sro_managers'
				, title: 'Список АУ СРО'
			}
			, "dm_sro_manager": {
				path: 'forms/ama/datamart/sro/manager/c_cust_manager'
				, title: 'Доступ к витрине через СРО'
			}
			, "dm_sro_manager_document": {
				path: 'forms/ama/datamart/sro/manager_document/c_sro_manager_document'
				, title: 'Документы АУ'
			}
			, "dm_sro_manager_info": {
				path: 'forms/ama/datamart/sro/manager_info/c_sro_manager_info'
				, title: 'Информация об АУ'
			}
			, "dm_sro_manager_document_add": {
				path: 'forms/ama/datamart/sro/manager_document/add/c_sro_manager_document_add'
				, title: 'Добавить документ АУ'
			}
			, "dm_sro_notifications": {
				path: 'forms/ama/datamart/sro/notifications/c_sro_notifications'
				, title: 'Уведомления АУ'
			}
			, "dm_sro_props": {
				path: 'forms/ama/datamart/sro/props/c_sro_props'
				, title: 'Реквизиты СРО'
			}
		},
		content: {
			"dm_request_01sav": {
				path: 'txt!forms/ama/datamart/sro/request/tests/contents/01sav.json.etalon.txt'
				, title: 'запрос кандидатур АУ'
			}
			, "dm_request_consent_01sav": {
				path: 'txt!forms/ama/datamart/sro/consent/tests/contents/01sav.json.etalon.txt'
				, title: 'согласие АУ'
			}
			, "dm_request_consent_08sav-married": {
				path: 'txt!forms/ama/datamart/sro/consent/tests/contents/08sav-married.json.etalon.txt'
				, title: 'согласие АУ (супруги)'
			}
			, "dm_request_06sav-married": {
				path: 'txt!forms/ama/datamart/sro/request/tests/contents/06sav-married.json.etalon.txt'
				, title: 'согласие АУ (супруги)'
			}
			, "dm_requests_params_00checkInclude": {
				path: 'txt!forms/ama/datamart/sro/requests_params/tests/contents/00checkInclude.json.etalon.txt'
				, title: 'фильтр запросов'
			}
			, "dm_sro_manager_info_01sav": {
				path: 'txt!forms/ama/datamart/sro/manager_info/tests/contents/01sav.json.etalon.txt'
				, title: 'информация об ау'
			}
		}

	};

});