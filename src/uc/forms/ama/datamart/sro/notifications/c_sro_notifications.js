﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/sro/notifications/e_sro_notifications.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/notifications/c_sro_notification_info.js'
	, 'forms/ama/datamart/base/h_email_types'
],
function (c_fastened, tpl, h_msgbox, c_sro_notification_info, h_email_types)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_grid_url = controller.base_url + '?action=manager.notifications.jqgrid';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_SRO=' + this.model.id_SRO;
			return url;
		}

		var emailTypeFormatter = function (cellvalue, options, rowObject)
		{
			return h_email_types[rowObject.EmailType];
		}
		var selectableEmailType = function ()
		{
			var res= ':Все уведомления';
			var names= h_email_types;
			for (var name in names)
			{
				res+= ';';
				res+= name + ':' + names[name];
			}
			return res;
		}

		controller.colModel =
		[
			  { name: 'id_SentEmail', hidden: true }
			, { label: 'Кому', name: 'RecepientName', width: 235 }
			, { label: 'email', name: 'RecipientEmail', width: 235 }
			, { label: 'О чём', name: 'EmailType', width: 275, stype: 'select'
				, formatter: emailTypeFormatter
				, searchoptions:
					{
						sopt: ['eq']
						, value: selectableEmailType()
						, dataInit: function (el)
						{
							$("option:contains('Все уведомления')", el).attr("selected", "selected");
							$(el).trigger('change');
						}
					}
				, sortable: false
			}
			, { label: 'На отправку', name: 'TimeDispatch', width: 275, search: false }
			, { label: 'Отправлено', name: 'TimeSent', width: 275, search: false }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url= self.PrepareUrl();
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показано уведомлений {1} из {2}'
				, emptyText: 'Нет уведомлений для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, rowList: [15, 30, 100, 300, 500]
				, pager: '#cpw-cpw-ama-dm-sro-notifications-pager'
				, viewrecords: true
				, height: 'auto'
				, autowidth: true
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, onSelectRow: function (id, s, e) {
					self.onSelectRow_if_no_menu(id, s, e, function () { self.OnNotificationInfo(); });
				}
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка АУ", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnNotificationInfo= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			self = this;
			var id_SentEmail = rowdata.id_SentEmail;
			
			var ajaxurl = this.base_url + '?action=manager.notifications.info&id=' + id_SentEmail;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос информации об уведомлении с сервера", ajaxurl);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus) {
					if (null == data) {
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else {
						var c_notification_info = c_sro_notification_info({ base_url: this.base_url, sel: sel });
						var mimeBtn = 'Квитанция об отправке'
						data.EmailType = h_email_types[data.EmailType];
						c_notification_info.SetFormContent(data);
						h_msgbox.ShowModal
							({
								title: 'Уведомление АУ'
								, controller: c_notification_info
								, buttons: [mimeBtn, 'Закрыть']
								, id_div: "cpw-form-ama-datamart-notification-info"
								, onclose: function (btn, dlg_div) {
									if (btn == mimeBtn) {
										var model = c_notification_info.GetFormContent();
										if (null != model.TimeSent && '' != model.TimeSent) {
											var mime_dwnld_url = self.base_url + '?action=manager.notifications.mime.dwnld&id=' + id_SentEmail;
											window.open(mime_dwnld_url);
										} else {
											h_msgbox.ShowModal({
												title: 'Невозможно открыть квитанцию об отправке', width: 600
												, html: 'Сообщение еще не отправлено!'
											});
										}

										return false;
									}
								}
							});

					}
				}
			});
		}


		return controller;
	}
});