﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/notifications/e_sro_notification_info.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 860, height: 590 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');


		return controller;
	}
});