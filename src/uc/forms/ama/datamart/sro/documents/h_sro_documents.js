﻿define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/documents/c_sro_documents'
	, 'forms/ama/datamart/base/h_sro_document_types'
],
function (h_msgbox, c_sro_documents, h_sro_document_types)
{
	return {
		ModalEditSroDocuments: function (base_url, sro)
		{
			var ajaxurl = base_url + '?action=sro.document.ru&cmd=get&id=' + sro.id_SRO;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос информации об документах СРО с сервера", ajaxurl);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.EditDocument(base_url, sro, data);
					}
				}
			});
		}

		, EditDocument: function (base_url, sro, documents)
		{
			var c_documents = c_sro_documents({ base_url: base_url, id_SRO: sro.id_SRO });

			documents.forEach(function (item) { 
				var dt= item.DocumentType;
				item.DocumentType = h_sro_document_types[dt];
				if (!item.DocumentType)
					item.DocumentType= 'Документ неизвестного типа "' + dt + '"';
			});

			c_documents.SetFormContent(documents);

			h_msgbox.ShowModal
			({
				title: 'Документы СРО ' + sro.ShortTitle
				, controller: c_documents
				, buttons: ['Закрыть']
				, id_div: "cpw-form-ama-datamart-manager-documents"
			});
		}
	};
});