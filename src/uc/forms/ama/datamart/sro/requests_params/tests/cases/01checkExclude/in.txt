include ..\..\..\..\..\..\..\wbt.lib.txt quiet

wait_text "В таблице запросов отображать"

shot_check_png ..\..\shots\00checkInclude.png

js wbt.SetModelFieldValue("DateOfOffer.Exclude", "checked");
js wbt.SetModelFieldValue("Consents.Exclude", "checked");
js wbt.SetModelFieldValue("DateOfResponce.Exclude", "checked");

shot_check_png ..\..\shots\01checkExclude.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01checkExclude.json.result.txt
wait_click_full_text "Редактировать модель в элементе управления"

wait_text "В таблице запросов отображать"

js wbt.CheckModelFieldValue("DateOfOffer.Exclude", "checked");
js wbt.CheckModelFieldValue("Consents.Exclude", "checked");
js wbt.CheckModelFieldValue("DateOfResponce.Exclude", "checked");

exit