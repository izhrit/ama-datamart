define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/requests_params/e_dm_requests_params.html'
], function (c_fastened, tpl) {
	return function(options_arg) {

		var options = {};

		var controller = c_fastened(tpl, options);

		controller.size = { width: 389, height: 260 };

		var base_Render= controller.Render;

		controller.Render = function (sel)
		{
			base_Render.call(this,sel);
			var self= this;

			if(!this.model) $(sel + ' [data-fc-selector="All"]').attr("checked", true);

			$(sel + ' [data-fc-selector="All"]').change(function () {
				if(!$(this).attr('checked')) $(this).attr("checked", true);
				$(sel + ' .only_block input').attr("checked", false);
			})
			$(sel + ' [data-fc-selector="DateOfOffer.Include"]').change(function () {
				self.OnCheckCheckbox('DateOfOffer.Exclude',$(this).attr('checked'));
			})
			$(sel + ' [data-fc-selector="DateOfOffer.Exclude"]').change(function () {
				self.OnCheckCheckbox('DateOfOffer.Include',$(this).attr('checked'));
			})

			$(sel + ' [data-fc-selector="Consents.Include"]').change(function () {
				self.OnCheckCheckbox('Consents.Exclude',$(this).attr('checked'));
			})
			$(sel + ' [data-fc-selector="Consents.Exclude"]').change(function () {
				self.OnCheckCheckbox('Consents.Include',$(this).attr('checked'));
			})

			$(sel + ' [data-fc-selector="DateOfResponce.Include"]').change(function () {
				self.OnCheckCheckbox('DateOfResponce.Exclude',$(this).attr('checked'));
			})
			$(sel + ' [data-fc-selector="DateOfResponce.Exclude"]').change(function () {
				self.OnCheckCheckbox('DateOfResponce.Include',$(this).attr('checked'));
			})
		}

		controller.OnCheckCheckbox = function (reverseElName, isChecked) {
			var sel = this.fastening.selector;
			if(isChecked) {
				$(sel + ' [data-fc-selector="All"]').attr("checked", false);
				$(sel + ' [data-fc-selector="' + reverseElName + '"]').attr("checked", false);
			} else {
				this.CheckAllCheckboxes();
			}
		}

		controller.CheckAllCheckboxes = function () {
			var sel = this.fastening.selector;
			var checkboxes = [
				'DateOfOffer.Include', 'DateOfOffer.Exclude',
				'Consents.Include', 'Consents.Exclude',
				'DateOfResponce.Include', 'DateOfResponce.Exclude'
			];
			var uncheckedCount = 0;
			for(var i = 0; i < checkboxes.length; i++) {
				if(!$(sel + ' [data-fc-selector="' + checkboxes[i] +'"]').attr('checked')) uncheckedCount++;
			}
			if(uncheckedCount == checkboxes.length)
				$(sel + ' [data-fc-selector="All"]').attr("checked", true);
		}
		return controller;
	}
});