﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/sro/consents/e_dm_consents.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/consent/h_consent'
],
function (c_fastened, tpl, h_msgbox, h_consent)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_grid_url = controller.base_url + '?action=consent.jqgrid';

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;

			this.consents_helper = h_consent
			({ 
				base_url: this.base_url
				, selector: sel
				, id_SRO: this.model.id_SRO
				, on_change: function ()
				{ 
					self.ReloadGrid();
					$(sel).trigger('model_change');
				}
			});

			this.RenderGrid();

			this.AddButtonToPagerLeft('cpw-cpw-ama-dm-consents-pager', 'Обновить', function () { self.ReloadGrid(); });
			this.AddButtonToPagerLeft('cpw-cpw-ama-dm-consents-pager', 'Зарегистрировать согласие', function () { self.consents_helper.OnAdd(); });
		}

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_SRO=' + this.model.id_SRO;
			return url;
		}

		controller.colModel = function ()
		{
			return [
				  { name: 'id_ProcedureStart', hidden: true }
				, { label: 'Должник', name: 'debtorName', align: 'left',formatter: this.consents_helper.debtorNameFormatter, width: 150 }
				, { label: 'Подано', name: 'DateOfApplication', width: 30, search: false}
				, { label: 'в суд', name: 'Court', width: 60 }
				, { label: 'Согласный АУ', name: 'Manager', width: 60, formatter: this.consents_helper.managerFormatter }
				, { label: 'Номер дела', name: 'caseNumber', width: 45 }
				, { label: 'Рассмотрение', name: 'NextSessionDate', width: 45, search: false }
				, { label: 'id_MRequest', name: 'id_MRequest', width: 1, hidden: true, search: false }
				, { label: 'addedBySRO', name: 'addedBySRO', width: 1, hidden: true, search: false }
				, { label: ' ', name: 'myac', width: 20, align: 'right', search: false, sortable:false}
			];
		}
		

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = self.PrepareUrl();
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel()
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показано согласий {1} из {2}'
				, emptyText: 'Нет согласий для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, rowList: [15, 30, 60]
				, pager: '#cpw-cpw-ama-dm-consents-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true 
				, onSelectRow: function (id, s, e) 
				{ 
					self.onSelectRow_if_no_menu(id, s, e, function () { self.consents_helper.OnEdit(); });
				}
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка согласий", url, jqXHR.responseText, textStatus, errorThrown)
				}
				,rowattr: function (rowData) {
					// если не создано СРО или привязано к запросу
					if(!rowData.addedBySRO || rowData.id_MRequest) return { "class": "jqgrid-not-editable-row" };
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.grid_row_buttons = function ()
		{
			var self = this;
			return [
				  { "class": "edit-procedure-start", text: "Редактировать", click: function () { self.consents_helper.OnEdit(); } }
				, { "class": "delete-procedure-start", text: "Удалить", click: function () { self.consents_helper.OnRemove(); } }
			];
		};

		return controller;
	}
});