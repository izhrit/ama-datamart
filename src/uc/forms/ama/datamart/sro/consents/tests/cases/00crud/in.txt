include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\consent\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "Искать"

wait_text "Показано согласий 2 из 2"
shot_check_png ..\..\shots\initial.png

wait_click_text "Зарегистрировать согласие"
shot_check_png ..\..\shots\add.png


# Начало заполнения формы создания согласия

js wbt.SetModelFieldValue("Должник.Тип", "Супруги");

js $('.cpw-ama-datamart-consent .cpw-ama-datamart-married_couple .debtor1').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_1
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js $('.cpw-ama-datamart-consent .cpw-ama-datamart-married_couple .debtor2').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_2
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Москвы');

wait_click_text "Заявителями являются должники (кликните, чтобы поменять)"
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-consent-applicant"] '
	wait_text "Информация о заявителе"
	play_stored_lines ama_datamart_applicant_fields_1
	wait_click_text "Сохранить информацию о заявителе"
	wait_text "Тростников С.М."
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-consent .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
play_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

js $(".cpw-ama-datamart-consent .appointment").click();
wait_text "Судебный акт о назначении кандидатуры АУ"
play_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о назначении"
wait_text_disappeared "Судебный акт о назначении кандидатуры АУ"

js $(".cpw-ama-datamart-consent .efrsb_report").click();
wait_text "Сообщение на ЕФРСБ о введении процедуры"
play_stored_lines ama_datamart_efrsb_report_fields_1
wait_click_text "Сохранить информацию о сообщении"
wait_text_disappeared "Сообщение на ЕФРСБ о введении процедуры"

js wbt.SetModelFieldValue("АУ", "Иванов Иван Иванович");

# Конец заполнения формы создания согласия

shot_check_png ..\..\shots\add_filled.png
wait_click_text "Отмена"


wait_click_text "Зарегистрировать согласие"
shot_check_png ..\..\shots\add.png


# Начало заполнения формы создания согласия

js wbt.SetModelFieldValue("Должник.Тип", "Супруги");

js $('.cpw-ama-datamart-consent .cpw-ama-datamart-married_couple .debtor1').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_1
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js $('.cpw-ama-datamart-consent .cpw-ama-datamart-married_couple .debtor2').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_2
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Москвы');

wait_click_text "Заявителями являются должники (кликните, чтобы поменять)"
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-consent-applicant"] '
	wait_text "Информация о заявителе"
	play_stored_lines ama_datamart_applicant_fields_1
	wait_click_text "Сохранить информацию о заявителе"
	wait_text "Тростников С.М."
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-consent .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
play_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

js $(".cpw-ama-datamart-consent .appointment").click();
wait_text "Судебный акт о назначении кандидатуры АУ"
play_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о назначении"
wait_text_disappeared "Судебный акт о назначении кандидатуры АУ"

js $(".cpw-ama-datamart-consent .efrsb_report").click();
wait_text "Сообщение на ЕФРСБ о введении процедуры"
play_stored_lines ama_datamart_efrsb_report_fields_1
wait_click_text "Сохранить информацию о сообщении"
wait_text_disappeared "Сообщение на ЕФРСБ о введении процедуры"

js wbt.SetModelFieldValue("АУ", "Иванов Иван Иванович");

# Конец заполнения формы создания согласия

shot_check_png ..\..\shots\add_filled.png
wait_click_full_text "Сохранить"


shot_check_png ..\..\shots\added.png
wait_text "Показано согласий 3 из 3"

je $("td[title='Щербаков К.А., Уварова Д.Р.'] ~ td > div.actions-button").click();
wait_click_text "Редактировать"
shot_check_png ..\..\shots\edit_added.png


# Начало проверки заполнения формы создания согласия

js $('.cpw-ama-datamart-consent .cpw-ama-datamart-married_couple .debtor1').click();
wait_text "Информация о должнике"
check_stored_lines ama_datamart_debtor_physical_fields_1
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Отмена")').click()
wait_text_disappeared "Информация о должнике"

js $('.cpw-ama-datamart-consent .cpw-ama-datamart-married_couple .debtor2').click();
wait_text "Информация о должнике"
check_stored_lines ama_datamart_debtor_physical_fields_2
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Отмена")').click()
wait_text_disappeared "Информация о должнике"

wait_click_text  "Тростников С.М."
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-consent-applicant"] '
	wait_text "Информация о заявителе"
	check_stored_lines ama_datamart_applicant_fields_1
	js $('[aria-describedby="cpw-form-ama-consent-applicant"] .ui-dialog-buttonpane .ui-button-text:contains("Отмена")').click()
	wait_text "Тростников С.М."
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-consent .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
check_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

js $(".cpw-ama-datamart-consent .appointment").click();
wait_text "Судебный акт о назначении кандидатуры АУ"
check_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о назначении"
wait_text_disappeared "Судебный акт о назначении кандидатуры АУ"

js $(".cpw-ama-datamart-consent .efrsb_report").click();
wait_text "Сообщение на ЕФРСБ о введении процедуры"
check_stored_lines ama_datamart_efrsb_report_fields_1
wait_click_text "Сохранить информацию о сообщении"
wait_text_disappeared "Сообщение на ЕФРСБ о введении процедуры"

js wbt.CheckModelFieldValue("АУ", "Иванов И.И.");

# Конец проверки заполнения формы создания согласия


# Начало заполнения формы создания согласия

play_stored_lines ama_datamart_consent_fields_2

wait_click_text "Тростников С.М."
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-consent-applicant"] '
	wait_text "Информация о заявителе"
	js wbt.SetModelFieldValue("Заявитель_должник", "checked");
	shot_check_png ..\..\shots\debtor_is_applicant.png
	wait_click_text "Сохранить информацию о заявителе"
	wait_text "Заявителем является должник (кликните, чтобы поменять)"
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-consent .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
play_stored_lines ama_datamart_court_decision_fields_2
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

js $(".cpw-ama-datamart-consent .appointment").click();
wait_text "Судебный акт о назначении кандидатуры АУ"
play_stored_lines ama_datamart_court_decision_fields_2
wait_click_text "Сохранить информацию о назначении"
wait_text_disappeared "Судебный акт о назначении кандидатуры АУ"

js $(".cpw-ama-datamart-consent .efrsb_report").click();
wait_text "Сообщение на ЕФРСБ о введении процедуры"
play_stored_lines ama_datamart_efrsb_report_fields_2
wait_click_text "Сохранить информацию о сообщении"
wait_text_disappeared "Сообщение на ЕФРСБ о введении процедуры"

# Конец заполнения формы создания согласия

wait_click_full_text "Сохранить"


wait_text "Показано согласий 3 из 3"
shot_check_png ..\..\shots\edited.png

wait_click_text "Трамп"
shot_check_png ..\..\shots\edit_edited.png
wait_click_text "Отмена"

wait_text "Показано согласий 3 из 3"

je $("td[title='Трамп Д.Ф.'] ~ td > div.actions-button").click();
wait_click_text "Удалить"
shot_check_png ..\..\shots\delete_edited.png
wait_click_text "Отмена"

wait_text "Показано согласий 3 из 3"

je $("td[title='Трамп Д.Ф.'] ~ td > div.actions-button").click();
wait_click_text "Удалить"
shot_check_png ..\..\shots\delete_edited.png
wait_click_text "Да, удалить"

wait_text "Показано согласий 2 из 2"
shot_check_png ..\..\shots\initial.png

wait_click_text "Калимуллина"
shot_check_png ..\..\shots\edit_record_of_abonent.png
wait_click_text "Закрыть"

exit