define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/props/e_sro_list_old_team.html'
	, 'forms/ama/datamart/sro/props/c_sro_old_team'
	, 'forms/ama/datamart/base/h_region'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/props/h_sro_old_team'
],
function (c_fastened, tpl, c_sro_old_team, h_region, h_msgbox, h_sro_old_team)
{
	return function (options_arg)
	{
		var admin_mode = (null == options_arg) ? true : options_arg.admin_mode;

		var controller = c_fastened(tpl, {regions:h_region, admin_mode:admin_mode });

		controller.size = {width:1000, height:525};

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			var base_url = controller.base_url;

			base_Render.call(this,sel);

			var self= this;
			$("#add-team-button").click(function () {				
				self.AddNewTeam(base_url, self.model);
			})
			
			$(".edit-team-button").click(function () {
				var editnumder = $(this).attr('value');
				var edit_flag = true;			
				self.EditTeam(base_url, editnumder, self.model, edit_flag);
			})

			$(".delete-team-button").click(function () {
				var editnumder = $(this).attr('value');
				self.DeleteTeam(base_url, editnumder, self.model);
			})

		}		

		controller.AddNewTeam = function (base_url, model) {
			h_sro_old_team.AddOldTeam(base_url, this.model.id_SRO, model);
		}

		controller.EditTeam = function (base_url, editnumder, model, edit_flag) {
			h_sro_old_team.EditExistOldTeam(base_url, this.model.id_SRO, editnumder, model, edit_flag);
		}

		controller.DeleteTeam = function (base_url, editnumder, model) {
			h_sro_old_team.DeleteOldTeam(base_url, this.model.id_SRO, editnumder, model);
		}

		return controller;
	}
});

