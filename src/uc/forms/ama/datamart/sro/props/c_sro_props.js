﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/props/e_sro_props.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 980, height: 510 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			if (this.model)
				this.members = this.model.Комитет.Конкурсный.Участники

			$(sel + ' input[data-fc-selector="Телефон"]').inputmask("+9(999) 999-99-99");

			$(sel + ' table.contest-table input[name="Председатель"]').click(function () {
				$(this).parents('.member').find('input[name="Секретарь"]').prop('checked', false)
			})
			$(sel + ' table.contest-table input[name="Секретарь"]').click(function () {
				$(this).parents('.member').find('input[name="Председатель"]').prop('checked', false)
			})

			if (!this.members)
				this.members = [];
			this.FillData();
			this.AddRowEvent();
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		controller.SetMembersToModel = function () {
			var self = this;
			var sel = this.fastening.selector;
			var new_members = [];
			
			$(sel + ' table.contest-table tr.member').each(function () {
				var new_member = {
					"Фамилия": $(this).find('.lastname').val(),
					"Имя": $(this).find('.firstname').val(),
					"Отчество": $(this).find('.middlename').val(),
					"Роль": $(this).find('.radio-item input[type="radio"]:checked').attr('name')
				};
				if (!new_member.Роль) { delete new_member.Роль }

				if (new_member.Фамилия != '') {
					new_members.push(new_member);
				}
				
			})
			this.model.Комитет.Конкурсный.Участники = new_members;
		}

		controller.FillData = function () {
			var self = this;
			var sel = this.fastening.selector;

			var table = $(sel + ' table.contest-table');
			for (var i = 0; i < self.members.length; i++) {
				var row_member = $(sel + ' tr.member-' + i);
				row_member.find('.firstname').val(self.members[i].Имя)
				row_member.find('.lastname').val(self.members[i].Фамилия)
				row_member.find('.middlename').val(self.members[i].Отчество)

				if (self.members[i].Роль) {
					row_member.find('input[name="' + self.members[i].Роль + '"][value="role-' + i + '"]').prop("checked", true);
				}

				if (i == self.members.length - 1) {
					var j = i + 2;
					this.AppendRow(table, j);
				}
			}
		}

		controller.AddRowEvent = function () {
			var self = this;
			var sel = this.fastening.selector;
			var table = $(sel + ' table.contest-table');

			$(sel + ' #cpw-ama-datamart-sro-props-tab-contest tr.member:last-child input.lastname').on('input', function () {
				if ($(this).val().length == 1) {
					$(this).parents('tr.member').removeClass('member-next')
					var trClass = $(this).parents('tr.member').attr('class').split('-')
					
					self.AppendRow(table, +trClass[trClass.length - 1] + 1)
					$(this).unbind()
					self.AddRowEvent();
				}
			});
		}

		controller.AppendRow = function (table, num) {
			table.append('<tr class="member member-next member-' + num + '"><td></td><td>' + num + ')</td> <td><input type="text" class="member-info	lastname"/></td> <td><input type="text" class="member-info firstname"/></td> <td><input type="text" class="member-info middlename"/></td> <td class="radio-item"><label><input type="radio" name="Председатель" value="role-' + num + '"><span> - Председатель</span></label></td> <td class="radio-item"><label><input type="radio" name="Секретарь" value="role-' + num +'"><span> - Секретарь</span></label></td> </tr>');
		}
		return controller;
	}
});