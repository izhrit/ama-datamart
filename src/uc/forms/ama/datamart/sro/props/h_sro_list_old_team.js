define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/props/c_sro_list_old_team'
],
function (h_msgbox, c_sro_list_old_team)
{
	return {
		SroListOldTeam: function (base_url, id_SRO, modal)
		{
			var old_props = $.extend(true, {}, modal);
			var self= this;
			var admin_mode= this.modal && null!=this.modal && null!=this.modal.id_Admin;
			var ee_sro_list_old_team = c_sro_list_old_team({base_url:this.base_url,admin_mode:admin_mode});
			modal.Старые_составы_конкурсного_комитета.sort(function(a,b){
				var dateA = a.Дата.split('.');
				var dateB = b.Дата.split('.');
				var parsDateA = new Date(dateA[2], dateA[1], dateA[0]);
				var parsDateB = new Date(dateB[2], dateB[1], dateB[0]);
				return parsDateB - parsDateA;
			})
			ee_sro_list_old_team.SetFormContent(modal);
			ee_sro_list_old_team.base_url = base_url;
			var btnOk= 'Сохранить';

			h_msgbox.ShowModal
			({
				  title:'Старые составы конкурсного комитета'
				, controller: ee_sro_list_old_team
				, id_div:'cpw-form-sro-list-old-team'
				, buttons:[btnOk, 'Отмена']
			});
		}
	};
});