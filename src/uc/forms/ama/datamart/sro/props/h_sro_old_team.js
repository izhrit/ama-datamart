define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/props/c_sro_old_team'
	, 'forms/ama/datamart/sro/props/c_sro_delete_old_team'
],
function (h_msgbox, c_sro_old_team, c_sro_delete_old_team)
{
	return {		
		EditExistOldTeam: function (base_url, id_SRO, edit_numder, model, edit_flag)
		{
			var old_props = $.extend(true, {}, model);
			var props_with_change = $.extend(true, {}, model);
			var self= this;
			var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;
			var ee_sro_old_team = c_sro_old_team({base_url:this.base_url,admin_mode:admin_mode});
			ee_sro_old_team.SetFormContent(model.Старые_составы_конкурсного_комитета[edit_numder]);
			ee_sro_old_team.base_url = base_url;
			ee_sro_old_team.copy_current_team = model.Комитет.Конкурсный;
			ee_sro_old_team.flag = edit_flag;
			var btnOk= 'Сохранить изменение старого состава';

			h_msgbox.ShowModal
			({
				  title:'Изменение старого состава конкурсного комитета'
				, controller: ee_sro_old_team
				, width:400
				, id_div:'cpw-ama-datamart-sro-old-team'
				, buttons:[btnOk, 'Отмена']
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						ee_sro_old_team.SetOldTeamToModel()
						props_with_change.Старые_составы_конкурсного_комитета[edit_numder] = ee_sro_old_team.GetFormContent();
						var is_changed = JSON.stringify(old_props) !== JSON.stringify(props_with_change)
						if (is_changed)
							self.UpdateExistOldTeam(base_url, id_SRO, props_with_change);
						location.reload();													
					}
				}
			});
		}

		, AddOldTeam: function (base_url, id_SRO, model)
		{
			var old_props = $.extend(true, {}, model);
			var change_props = $.extend(true, {}, model);
			var self= this;
			var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;
			var ee_sro_old_team = c_sro_old_team({base_url:this.base_url,admin_mode:admin_mode});
			ee_sro_old_team.SetFormContent();
			ee_sro_old_team.base_url = base_url;
			ee_sro_old_team.copy_current_team = model.Комитет.Конкурсный;
			var btnOk= 'Сохранить старый состав';

			h_msgbox.ShowModal
			({
				  title:'Добавление старого состава конкурсного комитета'
				, controller: ee_sro_old_team
				, width:400
				, id_div:'cpw-ama-datamart-sro-old-team'
				, buttons:[btnOk, 'Отмена']
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						ee_sro_old_team.SetOldTeamToModel()

						var new_props = ee_sro_old_team.GetFormContent();

						if(new_props['Наименование'] != '' && new_props['Дата'] != ''){
							change_props.Старые_составы_конкурсного_комитета.push(new_props);
						}
						var is_changed = JSON.stringify(old_props) !== JSON.stringify(change_props)
						if (is_changed)
							self.UpdateExistOldTeam(base_url, id_SRO, change_props);
						location.reload();
					}
				}
			});
		}

		, DeleteOldTeam: function (base_url, id_SRO, edit_numder, model)
		{
			var old_props = $.extend(true, {}, model);
			var change_props = $.extend(true, {}, model);
			var self= this;

			var admin_mode= this.model && null!=this.model && null!=this.model.id_Admin;
			var ee_sro_delete_old_team = c_sro_delete_old_team({base_url:this.base_url,admin_mode:admin_mode});
			ee_sro_delete_old_team.SetFormContent(model.Старые_составы_конкурсного_комитета[edit_numder]);
			var btnOk= 'Удалить';

			h_msgbox.ShowModal
			({
				  title:'Подтверждение удаления записи'
				, controller: ee_sro_delete_old_team
				, width:400
				, id_div:'cpw-ama-datamart-sro-old-team'
				, buttons:[btnOk, 'Отмена']
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						change_props.Старые_составы_конкурсного_комитета.splice(edit_numder, 1);
						var is_changed = JSON.stringify(old_props) !== JSON.stringify(change_props)
						if (is_changed)
							self.UpdateExistOldTeam(base_url, id_SRO, change_props);	
					}
					location.reload();
				}
			});
		}

		, UpdateExistOldTeam: function (base_url, id_SRO, model)
		{
			var self = this;
			var ajaxurl = base_url + '?action=sro.props.crud&cmd=update&id=' + id_SRO;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача информации о составе СРО на сервер", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: model
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok)
					{
					}
					else
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка сохранения информации о старом составе СРО'
							, width: 400
							, html: '<span>Не удалось сохранить информацию о старом составе СРО по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
				}
			});
		}
	};
});