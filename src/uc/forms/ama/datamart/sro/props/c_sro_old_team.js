define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/props/e_sro_old_team.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.size = {width:975, height:500};
		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);
			var self = this;
			if (this.model)				
				this.members = this.model.Участники
		
			$(sel + ' table.contest-table input[name="Председатель"]').click(function () {
				$(this).parents('.member').find('input[name="Секретарь"]').prop('checked', false)
			})
			$(sel + ' table.contest-table input[name="Секретарь"]').click(function () {
				$(this).parents('.member').find('input[name="Председатель"]').prop('checked', false)
			})

			if (!this.members)
				this.members = [];
			this.FillData();			
			$(".button-copy").click(function () {			
				self.FillCopyData(controller.copy_current_team);
				self.AddCopyRowEvent();
			})
			this.AddRowEvent();							
		}

		controller.SetOldTeamToModel = function () {
			var self = this;
			var sel = this.fastening.selector;
			var new_old_teams = [];

			var date = $(sel + ' div.data-row');						
			var name = $(sel + ' div.name-row');			
			
			var new_old_team = [];
			var pushed_old_team = [];

			$(sel + ' table.contest-table tr.member').each(function (index) {
				new_old_team[index] = {
					"Фамилия": $(this).find('.lastname').val(),
					"Имя": $(this).find('.firstname').val(),
					"Отчество": $(this).find('.middlename').val(),
					"Роль": $(this).find('.radio-item input[type="radio"]:checked').attr('name')
				};
				if (!new_old_team[index].Роль) { delete new_old_team[index].Роль }
				if (new_old_team[index].Фамилия != '') {
					pushed_old_team.push(new_old_team[index]);
				}				
			})
			var new_old_teams = {
				"Наименование": name.find('.name-charter').val(),
				"Дата": date.find('.end-date').val(),
				"Участники": pushed_old_team
			};					
			this.model = new_old_teams;
		}

		controller.FillCopyData = function (copy_member) {
			var copy_members = copy_member.Участники;			
			var sel = this.fastening.selector;
			$(sel + ' table.contest-table tr.member').remove();			
			var table = $(sel + ' table.contest-table');			
			for (var i = 0; i < copy_members.length+1; i++) {
				this.AppendCopyRow(table, i);
			}														
			for (var i = 0; i < copy_members.length; i++) {				
				var row_member = $(sel + ' tr.member-' + i);
				row_member.find('.firstname').val(copy_members[i].Имя)
				row_member.find('.lastname').val(copy_members[i].Фамилия)
				row_member.find('.middlename').val(copy_members[i].Отчество)
				if (copy_members[i].Роль) {
					row_member.find('input[name="' + copy_members[i].Роль + '"][value="role-' + i + '"]').prop("checked", true);
				}			
			}
		}

		controller.FillData = function () {
			var self = this;
			var sel = this.fastening.selector;			
			var table = $(sel + ' table.contest-table');
			for (var i = 0; i < self.members.length; i++) {
				var row_member = $(sel + ' tr.member-' + i);
				row_member.find('.firstname').val(self.members[i].Имя)
				row_member.find('.lastname').val(self.members[i].Фамилия)
				row_member.find('.middlename').val(self.members[i].Отчество)
				if (self.members[i].Роль) {
					row_member.find('input[name="' + self.members[i].Роль + '"][value="role-' + i + '"]').prop("checked", true);
				}
				if (i == self.members.length - 1) {
					var j = i + 2;
					this.AppendRow(table, j);
				}
			}
		}

		controller.AddRowEvent = function () {
			var self = this;
			var sel = this.fastening.selector;
			var table = $(sel + ' table.contest-table');
			$(sel + ' #cpw-ama-datamart-sro-props-old-team-tab-contest tr.member:last-child input.lastname').on('input', function () {
				if ($(this).val().length == 1) {
					var trClass = $(this).parents('tr.member').attr('class').split('-')					
					self.AppendRow(table, +trClass[trClass.length - 1] + 1)
					$(this).unbind()
					self.AddRowEvent();
				}
			});
		}

		controller.AddCopyRowEvent = function () {
			var self = this;
			var sel = this.fastening.selector;
			var table = $(sel + ' table.contest-table');
			$(sel + ' #cpw-ama-datamart-sro-props-old-team-tab-contest tr.member:last-child input.lastname').on('input', function () {
				if ($(this).val().length == 1) {
					var trClass = $(this).parents('tr.member').attr('class').split('-')
					self.AppendCopyRow(table, +trClass[trClass.length - 1] + 1)
					$(this).unbind()
					self.AddCopyRowEvent();
				}
			});
		}

		controller.AppendRow = function (table, num) {
			var flag = controller.flag;
			if (flag == true) {
				var num_mod = num;
			} else {
				var num_mod = num + 1;
			}
			table.append('<tr class="member member-' + num + '"><td></td><td>' + num_mod + ')</td> <td><input type="text" class="member-info	lastname"/></td> <td><input type="text" class="member-info firstname"/></td> <td><input type="text" class="member-info middlename"/></td> <td class="radio-item"><label><input type="radio" name="Председатель" value="role-' + num + '"><span> - Председатель</span></label></td> <td class="radio-item"><label><input type="radio" name="Секретарь" value="role-' + num +'"><span> - Секретарь</span></label></td> </tr>');
		}

		controller.AppendCopyRow = function (table, num) {
			var num_mod = num + 1;
			table.append('<tr class="member member-' + num + '"><td></td><td>' + num_mod + ')</td> <td><input type="text" class="member-info	lastname"/></td> <td><input type="text" class="member-info firstname"/></td> <td><input type="text" class="member-info middlename"/></td> <td class="radio-item"><label><input type="radio" name="Председатель" value="role-' + num + '"><span> - Председатель</span></label></td> <td class="radio-item"><label><input type="radio" name="Секретарь" value="role-' + num +'"><span> - Секретарь</span></label></td> </tr>');
		}
		
		return controller;
	}
});