﻿define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/props/c_sro_props'
],
function (h_msgbox, c_sro_props)
{
	return {
		ModalEditSroProps: function (base_url, id_SRO)
		{
			var ajaxurl = base_url + '?action=sro.props.crud&cmd=get&id=' + id_SRO;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос информации об АУ с сервера", ajaxurl);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						var props = JSON.parse(data);
						self.EditProps(base_url, id_SRO, props);
					}
				}
			});
		}


		, EditProps: function (base_url, id_SRO, props)
		{
			var old_props = $.extend(true, {}, props);
			var c_props = c_sro_props({ base_url: base_url });
			var self = this;
			c_props.SetFormContent(props);
			var btnOk = 'Сохранить реквизиты СРО';
			h_msgbox.ShowModal
			({
				title: 'Реквизиты СРО ' + props.Наименование
				, controller: c_props
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-sro-props"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						c_props.SetMembersToModel()
						var new_props = c_props.GetFormContent();
						var is_changed = JSON.stringify(old_props) !== JSON.stringify(new_props)
						if (is_changed)
							self.UpdateProps(base_url, id_SRO, props);
							
						
					}
				}
			});
		}

		, UpdateProps: function (base_url, id_SRO, props)
		{
			var self = this;
			var ajaxurl = base_url + '?action=sro.props.crud&cmd=update&id=' + id_SRO;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача информации об реквизитах СРО на сервер", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: props
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok)
					{
					}
					else
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка сохранения информации о реквизитах'
							, width: 400
							, html: '<span>Не удалось сохранить информацию о реквизитах по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
				}
			});
		}
	};
});