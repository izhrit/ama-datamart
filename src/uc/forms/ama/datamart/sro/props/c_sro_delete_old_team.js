define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/props/e_sro_delete_old_team.html'
	, 'forms/ama/datamart/base/h_region'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_region, h_msgbox)
{
	return function (options_arg)
	{
		var admin_mode = (null == options_arg) ? true : options_arg.admin_mode;

		var controller = c_fastened(tpl, {regions:h_region, admin_mode:admin_mode });

		controller.size = {width:450, height:150};

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			var base_url = controller.base_url;
			base_Render.call(this,sel);			
		}

		return controller;
	}
});