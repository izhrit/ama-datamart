﻿define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/manager_info/c_sro_manager_info'
	, 'forms/base/h_validation_msg'
],
	function (h_msgbox, c_sro_manager_info, h_validation_msg)
{
	return {
		ModalEditManagerInfo: function(base_url, id_Manager)
		{
			if (!id_Manager || id_Manager == '')
			{
				h_msgbox.ShowModal
					({ 
						title: 'Работа с информацией недоступна!', buttons: ['Закрыть'], width: 410
						,html: 'Для того, чтобы менять информацию об АУ, необходимо выслать ему пароль доступа!'
					});
				return;
			}
			var ajaxurl = base_url + '?action=manager.info&cmd=get&id=' + id_Manager;
				var v_ajax = h_msgbox.ShowAjaxRequest("Запрос информации об АУ с сервера", ajaxurl);
				var self = this;
				v_ajax.ajax({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							self.EditInfo(base_url, data);
						}
					}
				});
		}
		, EditInfo: function (base_url, data)
		{
			var self = this
			var c_manager_info = c_sro_manager_info({ base_url: base_url, id_Manager: data.id_Manager });

			c_manager_info.SetFormContent(data);

			var btnOk= "Сохранить";
			h_msgbox.ShowModal
			({
				title: 'Информация об АУ'
				, controller: c_manager_info
				, buttons: [btnOk,"Отмена"]
				, id_div: "cpw-form-ama-datamart-manager-info"
				, onclose: function (btn, dlg_div)
				{
					if (btnOk == btn)
					{
						h_validation_msg.IfOkWithValidateResult(c_manager_info.Validate(), function () { 
							var manager_info= c_manager_info.GetFormContent();
							self.Update(base_url, manager_info);
							$(dlg_div).dialog("close");
						});
						return false;
					}
				}
			});
		}
		, Update: function(base_url, manager_info) {
			var ajaxurl = base_url + '?action=manager.info&cmd=update&id=' + manager_info.id_Manager;
            var v_ajax = h_msgbox.ShowAjaxRequest("Отправка информации об АУ на сервер", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'POST'
				, data: manager_info
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else {		
						h_msgbox.ShowModal
						({ 
							title: 'Информация успешно обновлена!', buttons: ['Закрыть'], width: 410
						});			
					}
				}
			});
		}
	};
});