define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/manager_info/e_sro_manager_info.html'
], function(c_fastened, tpl) {
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;

		controller.size = { width: 640, height: 165 };
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
		}

		var trim = function (txt)
		{
			return txt.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
		}

		controller.Validate = function() {
			var manager_info = this.GetFormContent();
			var vr= [];
			var block = function (msg) { vr.push({ check_constraint_result: false, description: msg }); }
			if(trim(manager_info.Протокол.Номер) !== '' && trim(manager_info.Протокол.Дата) == '') {
				block('Необходимо указать дату протокола о включении в СРО!');
			}
			if(trim(manager_info.Протокол.Номер) == '' && trim(manager_info.Протокол.Дата) !== '') {
				block('Необходимо указать номер протокола о включении в СРО!');
			}
			return 0==vr.length ? null : vr;
		}

		return controller;
	}
});