﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/sro/managers/e_sro_managers.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/manager/h_cust_manager'
	, 'forms/ama/datamart/sro/manager_document/h_sro_manager_document'
	, 'forms/ama/datamart/sro/manager_info/h_sro_manager_info'
],
function (c_fastened, tpl, h_msgbox, h_cust_manager, h_sro_manager_document, h_sro_manager_info)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_grid_url = controller.base_url + '?action=manager.jqgrid';
		controller.base_crud_url = controller.base_url + '?action=manager.ru';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_SRO=' + this.model.id_SRO;
			return url;
		}

		controller.grid_row_buttons = function () {
			var self = this;
			return [
				 { "class": "settings-au", text: "Доступ к витрине", click: function () { self.OnManager(); } }
				,{ "class": "documents-au", text: "Документы АУ", click: function () { self.OnDocuments(); } }
				,{ "class": "info-au", text: "Информация об АУ", click: function () { self.OnManagerInfo(); } }
			];
		};

		controller.colModel =
		[
			  { name: 'id_Manager', hidden: true }
			, { name: 'ArbitrManagerID', hidden: true }
			, { label: '№ на ЕФРСБ', name: 'efrsbNumber', width: 135 }
			, { label: 'Фамилия', name: 'lastName', width: 275 }
			, { label: 'Имя', name: 'firstName', width: 275 }
			, { label: 'Отчество', name: 'middleName', width: 275 }
			, { label: 'Пароль доступа', name: 'status', width: 275 }
			, { label: ' ', name: 'myac', width: 70, align: 'right', search: false }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url= self.PrepareUrl();
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показано АУ {1} из {2}'
				, emptyText: 'Нет АУ для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, rowList: [15, 30, 100, 300, 500]
				, pager: '#cpw-cpw-ama-dm-sro-managers-pager'
				, viewrecords: true
				, height: 'auto'
				, autowidth: true
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, onSelectRow: function (id, s, e) {
					self.onSelectRow_if_no_menu(id, s, e, function () { self.OnManager(); });
				}
				, loadComplete: function () {
					self.RenderRowActions(grid);
					var dlcc_name = 'data-load-complete-count';
					var dlcc = grid.attr(dlcc_name);
					grid.attr(dlcc_name, !dlcc || null == dlcc ? 1 : parseInt(dlcc) + 1);
				}
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка АУ", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
			grid.jqGrid('setLabel','efrsbNumber','',{'text-align':'left'});
			grid.jqGrid('setLabel','lastName','',{'text-align':'left'});
			grid.jqGrid('setLabel','firstName','',{'text-align':'left'});
			grid.jqGrid('setLabel','middleName','',{'text-align':'left'});
		}

		controller.OnManager= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			self = this;

			h_cust_manager.ModalEditManagerProfile(this.base_url, sel, rowdata.ArbitrManagerID);
		}

		controller.OnDocuments = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			h_sro_manager_document.ModalEditManagerDocuments(this.base_url, rowdata);
		}

		controller.OnManagerInfo = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			h_sro_manager_info.ModalEditManagerInfo(this.base_url, rowdata.id_Manager)
		}

		return controller;
	}
});