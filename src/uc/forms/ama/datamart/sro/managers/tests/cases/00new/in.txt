include ..\..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Иванов"
shot_check_png ..\..\shots\00new.png
wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\00new.json.result.txt
wait_click_full_text "Редактировать модель в элементе управления"
wait_text "Иванов"
wait_click_text "Иванов"
wait_text "Доступ к витрине"
shot_check_png ..\..\shots\00form.png
wait_click_text "Закрыть"
je $("td[title='Иванов'] ~ td > div.actions-button").click();
wait_click_text "Документы"
wait_text "Выписка"
shot_check_png ..\..\shots\01form.png
wait_click_text "Добавить документ"
wait_text "Добавление документа"
shot_check_png ..\..\shots\02form.png
wait_click_full_text "Отмена"
wait_click_full_text "Закрыть"
je $("td[title='Иванов'] ~ td > div.actions-button").click();
wait_click_text "Информация об АУ"
wait_text "Протокол о включении в состав"
shot_check_png ..\..\shots\03form.png
wait_click_text "Отмена"
exit