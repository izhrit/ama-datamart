﻿define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/manager/c_cust_manager'
],
function (h_msgbox, c_cust_manager)
{
	return {
		ModalEditManagerProfile: function (base_url, sel, ArbitrManagerID)
		{
			var ajaxurl = base_url + '?action=manager.sro.ru&cmd=get&id=' + ArbitrManagerID;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос информации об АУ с сервера", ajaxurl);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.EditProfile(base_url, sel, ArbitrManagerID, data);
					}
				}
			});
		}


		, EditProfile: function (base_url, sel, ArbitrManagerID, manager)
		{
			var c_manager = c_cust_manager({ base_url: base_url, sel: sel });
			var self = this;
			c_manager.SetFormContent(manager);
			var btnOk = 'Сохранить свойства АУ';
			h_msgbox.ShowModal
			({
				title: 'Доступ к витрине'
				, controller: c_manager
				, buttons: ['Закрыть']
				, id_div: "cpw-form-ama-datamart-manager"
			});
		}
	};
});