﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/manager/e_cust_manager.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 680, height: 440 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			var self = this;
			this.PasswordSent
			this.PasswordEnabled
			this.PasswordStatus = false

			this.PasswordSent = (this.model && this.model.PasswordSent && true == this.model.PasswordSent);
			this.PasswordEnabled = (this.model && this.model.PasswordEnabled && true == this.model.PasswordEnabled);
			this.ArbitrManagerID = !this.model ? null : this.model.ArbitrManagerID;

			if (!this.PasswordSent)
				this.PasswordSent = false;
			if (!this.PasswordEnabled)
				this.PasswordEnabled = false;

			if (this.PasswordSent) {
				this.PasswordStatus = 'wait'
			}
			else {
				this.PasswordStatus = this.PasswordEnabled;
			}

			$(sel + ' button.password').click(function () { self.OnChangePassword(); });
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.selGrid= !options_arg ? '' : options_arg.sel;

		controller.OkToSave = function (manager) {
			if (!manager || !manager.Email || null == manager.Email || '' == manager.Email)
				return false;
			var email = manager.Email;
			var title = "Проверка перед сохранением";
			if (5 > email.length) {
				h_msgbox.ShowModal({ title: title, width: 400, html: "Длина E-mail должна быть не менее 5 символов!" });
				return false;
			}
			if (-1 == email.indexOf('@')) {
				h_msgbox.ShowModal({ title: title, width: 400, html: "E-mail должен содержать символ '@'!" });
				return false;
			}
			return true;
		}

		controller.OnChangePassword= function()
		{
			var self = this;
			if (this.PasswordStatus === true) {
				return false;
			}
			this.model = this.GetFormContent();

			if (self.OkToSave(this.model)) {
				var self = this;
				var sel = this.fastening.selector;
				var btnOk = this.PasswordEnabled
					? 'Да, хочу сменить пароль АУ'
					: 'Да, хочу предоставить пароль АУ';
				h_msgbox.ShowModal({
					width: 520
					, title: "Подтверждение операции с паролем АУ"
					, html: true == this.PasswordEnabled
						? '<span>Вы уверены что хотите сменить пароль АУ</span><br/> и выслать его на адрес <strong>' + this.model.Email + '</strong>?'
						: '<span>Вы уверены что хотите предоставить пароль АУ</span><br/> и выслать его на адрес <strong>' + this.model.Email + '</strong>?<br/>E-mail будет использоваться для информирования о запросах.'
					, buttons: [btnOk, 'Нет, отменить']
					, id_div: "cpw-form-ama-datamart-manager-form-confirm"
					, onclose: function (btn) {
						if (btn == btnOk) {
							self.ChangePassword();
						}
					}
				});
			} else {
				return false;
			}
		}

		controller.ChangePassword = function ()
		{
			var selGrid = this.selGrid
			var sel = this.fastening.selector;
			var manager = this.model;
			var url = this.base_url + '?action=manager.sro.ru&cmd=update&id=' + manager.ArbitrManagerID;
			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса на создание пароля", url);

			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: manager
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);	
					}
					else if (true == responce.ok)
					{
						$(sel + ' div.cpw-ama-datamart-sro-manager')
							.attr('password_status', 'wait');

						var grid = $(selGrid + ' table.grid');
						var selrow = grid.jqGrid('getGridParam', 'selrow');
						var rowdata = grid.jqGrid('getRowData', selrow);
						rowdata.status = "Отправлен";
						grid.jqGrid('setRowData', selrow, rowdata);
						h_msgbox.ShowModal({width: 200, html: '<center>Пароль выслан</center>'});
					}
					else
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка сохранения пароля'
							, width: 450
							, html: '<span>Не удалось сменить пароль и email АУ по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>' 
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
				}
			});
		}

		return controller;
	}
});