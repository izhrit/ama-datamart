﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/sro/requests/e_dm_requests.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/request/h_request'
	, 'forms/ama/datamart/sro/requests_params/c_dm_requests_params'
],
	function (c_fastened, tpl, h_msgbox, h_request, c_requests_params)
{
	return function (options_arg)
	{
		var options = {
			field_spec:
				{
					'Фильтрация':
					{
						controller: c_requests_params
						, title: 'Настройки отображения запросов'
						, btn_ok_title: 'Применить'
					}
				}
		};

		var controller = c_fastened(tpl, options);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_grid_url = controller.base_url + '?action=mrequest.jqgrid';
		controller.base_crud_url = controller.base_url + '?action=mrequest.crud';

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			this.PrepareToBaseRender();
			base_Render.call(this, sel);
			var self = this;
			this.requests_helper = h_request
				({
					base_url: this.base_url
					, id_SRO: this.model && this.model.id_SRO
					, selector: sel
					, on_change: function () {
						self.ReloadGrid();
						$(sel).trigger('model_change');
					}
				});

			this.RenderGrid();

			this.AddButtonToPagerLeft('cpw-cpw-ama-dm-requests-pager', 'Обновить', function () { self.ReloadGrid(); });
			this.AddButtonToPagerLeft('cpw-cpw-ama-dm-requests-pager', 'Зарегистрировать запрос', function () { self.requests_helper.OnAdd(); });

			$(sel + ' .show-hide-search-inputs').click(function () { self.OnShowFilterToolBar(); });
		}

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_SRO=' + this.model.id_SRO;
			return url;
		}

		controller.colModel = function ()
		{
			return [
				  { name: 'id_MRequest', hidden: true}
				, { label: 'Должник', name: 'debtorName', formatter: this.requests_helper.debtorNameFormatter, align: 'left', width: 140 }
				, { label: 'Cуд', name: 'Court', width: 60 }
				, { label: 'Получено', name: 'DateOfCreation', width: 40, search: false}
				, { label: 'Запрошено', name: 'DateOfOffer', width: 40, search: false}
				, { label: 'Кандидатура АУ', name: 'Manager', width: 60 }
				, { label: 'Отвечено', name: 'DateOfResponce', width: 45, search: false}
				, { label: 'Рассмотрение', name: 'NextSessionDate', width: 50, search: false }
				, { label: ' ', name: 'myac', width: 25, align: 'right', search: false, sortable:false}
			];
		}
		

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = self.PrepareUrl();
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel()
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показано запросов {1} из {2}'
				, emptyText: 'Нет запросов для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, rowList: [15, 30, 60]
				, pager: '#cpw-cpw-ama-dm-requests-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true 
				, onSelectRow: function (id, s, e) 
				{ 
					self.onSelectRow_if_no_menu(id, s, e, function () { self.requests_helper.OnEdit(); });
				}
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка запросов", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.grid_row_buttons = function ()
		{
			var self = this;
			return [
				{ "class": "edit-request", text: "Редактировать", click: function () { self.requests_helper.OnEdit(); } }
				, { "class": "delete-request", text: "Удалить", click: function () { self.requests_helper.OnRemove(); } }
			];
		};

		controller.OnShowFilterToolBar = function () {
			var sel = this.fastening.selector;
			var filter_toolbar = $(sel + ' .filter-toolbar');
			var display = filter_toolbar.css('display');
			filter_toolbar.css('display', ('none' == display) ? 'block' : 'none');
		}

		controller.PrepareToBaseRender = function () {
			var self = this;
			var fspec= this.options.field_spec;

			fspec.Фильтрация.text = function (data)
			{
				self.AddFilter(data);
				return 'Параметры отображения запросов';
			}
		}

		controller.AddFilter = function (data) {
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.setGridParam({ postData: { customFilter: encodeURIComponent(JSON.stringify(data)) } });
			this.ReloadGrid();
		}

		return controller;
	}
});