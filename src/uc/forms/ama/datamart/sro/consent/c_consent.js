﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/consent/e_consent.html'
	, 'tpl!forms/ama/datamart/common/start/v_start_new_info.html'
	, 'tpl!forms/ama/datamart/common/start/v_start_check_pause.html'
	, 'forms/ama/datamart/common/applicant/c_applicant'
	, 'forms/ama/datamart/common/applicant/h_applicant'
	, 'forms/ama/datamart/common/court_decision/c_dm_court_decision'
	, 'forms/ama/datamart/common/court_decision/h_dm_court_decision'
	, 'forms/ama/datamart/common/efrsb_report/c_dm_efrsb_report'
	, 'forms/ama/datamart/common/efrsb_report/h_dm_efrsb_report'
	, 'forms/ama/datamart/common/married_couple/c_married_couple'
	, 'forms/base/h_times'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/h_validation_msg'
	, 'forms/base/h_msgbox'	
	, 'forms/base/h_constraints'
],
	function (
		  c_fastened, tpl
		, v_start_new_info, v_start_check_pause
		, c_applicant, h_applicant, c_court_decision, h_court_decision, c_efrsb_report,h_efrsb_report,c_married_couple
		, h_times, h_codec_datetime, h_validation_msg, h_msgbox, h_constraints
) {
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_court_select2 = base_url + '?action=court.select2'
		var base_url_manager_select2 = base_url + '?action=efrsb-manager.select2'
		if (options_arg && options_arg.id_SRO)
			base_url_manager_select2 += '&id_SRO=' + options_arg.id_SRO;

		var options = {
			readonly: options_arg && options_arg.readonly
			, ApplicantText: h_applicant.Text
			, field_spec:
				{
					
					"АУ": { ajax: { url: base_url_manager_select2, dataType: 'json' } }
					, 'Должник.Супруги':
					{
						controller: function() { return c_married_couple({ readonly: options_arg && options_arg.readonly }) }
						, title: 'Информация о должнике'
						, buttons: options_arg && options_arg.readonly ? ["Закрыть"] : null
					}
					, "Заявление_на_банкротство.В_суд":
					{ 
						placeholder:'Арбитражный суд (обязательно выберите из списка!)'
						,ajax: { url: base_url_court_select2, dataType: 'json' } 
					}
					, 'Заявитель':
					{
						controller: c_applicant
						, title: 'Информация о заявителе'
						, id_div: 'cpw-form-ama-consent-applicant'
						, btn_ok_title: 'Сохранить информацию о заявителе'
						, btn_close_title: 'Закрыть информацию о заявителе'
						, text: function(){}
					}
					, 'Запрос':
					{
						  controller: c_court_decision
						, title: 'Судебный акт о запросе кандидатур АУ'
						, id_div: 
						'cpw-form-ama-consent-court-decision-request-au'
						, btn_ok_title: 'Сохранить информацию о запросе кандидатур АУ судом'
						, text: function(){}
					}
					, 'Запрос_дополнительная_информация':
					{
						  controller: function() { return c_court_decision({ onlyAddInfo: true, readonly: true }) }
						, title: 'Дополнительная информация'
						, readonly: true
						, id_div: 'cpw-form-ama-consent-court-decision-request-au-add-info'	
						, text: function(){ return 'дополнительная информация от АУ'; }
					}
					, 'Назначение':
					{
						  controller: c_court_decision
						, title: 'Судебный акт о назначении кандидатуры АУ'
						, id_div: 'cpw-form-ama-consent-court-decision-appoint-au'
						, btn_ok_title: 'Сохранить информацию о назначении кандидатуры АУ судом'
						, btn_close_title: 'Закрыть информацию о назначении кандидатуры АУ судом'
						, text: function(){}
					}
					, 'Назначение_дополнительная_информация':
					{
						  controller: function() {return c_court_decision({ onlyAddInfo: true, readonly: true }) }
						, title: 'Дополнительная информация'
						, readonly: true
						, id_div: 'cpw-form-ama-consent-court-decision-appoint-au-add-info'
						, text: function(){ return 'дополнительная информация'; }
					}
					, 'ЕФРСБ':
					{
						  controller: c_efrsb_report
						, title: 'Сообщение на ЕФРСБ о введении процедуры'
						, id_div: 'cpw-form-ama-consent-efrsb-report'
						, btn_ok_title: 'Сохранить информацию о сообщении на ЕФРСБ о введении процедуры'
						, btn_close_title: 'Закрыть информацию о сообщении на ЕФРСБ о введении процедуры'
						, text: function(){}
					}
					, 'ЕФРСБ_дополнительная_информация':
					{
						  controller: function() {return c_efrsb_report({ onlyAddInfo: true, readonly: true }) }
						, title: 'Дополнительная информация'
						, readonly: true
						, id_div: 'cpw-form-ama-consent-efrsb-report-add-info'
						, text: function(){ return 'дополнительная информация'; }
					}
				}
		};

		var controller = c_fastened(tpl, options);

		controller.id_SRO = (options_arg && options_arg.id_SRO) ? options_arg.id_SRO : null;

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			this.readonly = options_arg && options_arg.readonly;
			this.PrepareToBaseRender(sel);
			base_Render.call(this,sel);
			var self = this;
			this.PrepareForm();

			$(sel + ' select.type').change(function () { self.OnSelectType(); });
			$(sel + ' button.open').click(function () { self.OnOpen(); });
			$(sel + ' button.actualize-check-time').click(function () { self.OnActualizeCheckTime(); });
			$(sel + ' button.check-kad-arbitr').click(function () { self.OnCheckKadArbitr(); });
			$(sel + ' button.court-by-number').click(function () { self.OnSelectCourtByCaseNumber(); });
			$(sel + ' input[data-fc-selector="Заявление_на_банкротство.Подано"]').change(function () { self.OnChangeПодано(); });
			$(sel + ' input[data-fc-selector="Заявление_на_банкротство.Дата_подачи"]').change(function () { self.OnChangeДата_подачи(); });
			$(sel + ' .make-request').click(function() { self.OnMakeRequest(); })
		}

		var base_GetFormContent= controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var consent= base_GetFormContent.call(this);

			var Должник= consent.Должник;
			switch (Должник.Тип)
			{
				case 'Физ_лицо':
					if (Должник.Юр_лицо)
						delete Должник.Юр_лицо;
					if (Должник.Супруги)
						delete Должник.Супруги;
					break;
				case 'Юр_лицо':
					if (Должник.Физ_лицо)
						delete Должник.Физ_лицо;
					if (Должник.Супруги)
						delete Должник.Супруги;
					break;
				case 'Супруги':
					consent.Должник = {
						Тип: Должник.Тип,
						Супруги: Должник.Супруги
					}
					break;
			}

			if(!consent.Заявитель)
				consent.Заявитель = h_applicant.GetDefaultObject();

			consent.Запрос = h_court_decision.GetDefaultObject(consent.Запрос);
			
			delete consent.Запрос_дополнительная_информация;

			consent.Назначение = h_court_decision.GetDefaultObject(consent.Назначение);
			
			delete consent.Назначение_дополнительная_информация;

			consent.ЕФРСБ = h_efrsb_report.GetDefaultObject(consent.ЕФРСБ);

			delete consent.ЕФРСБ_дополнительная_информация;

			return consent;
		}

		controller.SetReadOnly = function ()
		{
			var sel= this.fastening.selector;
			$(sel + ' div.select2').select2('enable', false);
			$(sel + ' input[type="text"]').prop('disabled', true);
			$(sel + ' input[type="checkbox"]').prop('disabled', true);
			$(sel + ' select').prop('disabled', true);
			$(sel + ' .check-kad-arbitr').prop('disabled', true);
			$(sel + ' .actualize-check-time').prop('disabled', true);
			$(sel + ' .check-kad-arbitr-help').attr('title', null);
			$(sel + ' .decision').hide();
			$(sel + ' .appointment').hide();
			$(sel + ' .efrsb_report').hide();
		}

		controller.OnSelectCourtByCaseNumber = function ()
		{
			if (options_arg && options_arg.readonly)
			{
				h_msgbox.ShowModal({
					title:'Отказ искать суд по номеру',width:400, id_div:'cpw-msg-can-not-find-court-by-number-ro'
					,html:'Функция поиска суда по номеру дела недоступна в режиме "только для чтения"..'
				});
			}
			else
			{
				var sel= this.fastening.selector;
				var number= $(sel + ' [data-fc-selector="Номер_судебного_дела"]').val();
				if ('' == number)
				{
					h_msgbox.ShowModal({
						title:'Отказ искать суд по номеру',width:400, id_div:'cpw-msg-can-not-find-court-by-empty-number'
						,html:'Укажите непустой номер дела, чтобы найти по нему суд..'
					});
				}
				else
				{
					var ajaxurl= base_url + '?action=court.by-prefix&number=' + encodeURI(number);
					var v_ajax = h_msgbox.ShowAjaxRequest("Запрос суда по номеру дела", ajaxurl);
					v_ajax.ajax
					({
						  dataType: "json", type: 'GET'
						, success: function (data, textStatus)
						{
							if (null != data)
							{
								$(sel + ' [data-fc-selector="Заявление_на_банкротство.В_суд"]').select2('data',data);
							}
							else
							{
								h_msgbox.ShowModal({
									title:'Отказ искать суд по номеру',width:600, id_div:'cpw-msg-can-not-find-court-by-number'
									,html:'Не удалось определить суд по номеру дела <b>' + number + '</b>'
								});
							}
						}
					});
				}
			}
		}

		controller.OnSelectType = function ()
		{
			var sel= this.fastening.selector;
			var type= $(sel + ' select.type').val();
			var root_item= $(sel + ' div.cpw-ama-datamart-consent');
			switch (type) {
				case 'Юр_лицо': 
					root_item.removeClass('citizen marriage_couple').addClass('organization'); 
					$(sel + ' [data-fc-selector="Должник.Юр_лицо.Наименование"]').focus(); 
					this.SetSingularApplicant();
					break;
				case 'Физ_лицо': 
					root_item.removeClass('organization marriage_couple').addClass('citizen'); 
					$(sel + ' [data-fc-selector="Должник.Физ_лицо.Фамилия"]').focus();
					this.SetSingularApplicant();
					break;
				case 'Супруги':
					root_item.removeClass('citizen organization').addClass('marriage_couple');
					this.SetPluralApplicant();
					break;
			}
		}

		var trim = function (txt)
		{
			return txt.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
		}

		controller.OnOpen = function ()
		{
			var sel= this.fastening.selector;
			var case_number= $(sel + ' input[data-fc-selector="Номер_судебного_дела"]').val();
			if ('' != trim(case_number))
			{
				window.open('https://kad.arbitr.ru/Card?number=' + case_number);
			}
			else
			{
				h_msgbox.ShowModal({
					title:'Отказ открыть страницу'
					,html:'<p>Чтобы открыть страницу дела на kad.arbitr.ru,</p><p style="text-align:right"> нужно указать номер дела..</p>'
					,width:400, id_div:'cpw-msg-can-not-open-kad-arbitr'
				});
			}
		}

		controller.MakeRequest = function () {}

		controller.OnMakeRequest = function()
		{
			var consent = this.GetFormContent()
			this.MakeRequest(consent)
		}

		controller.OnChangeПодано = function ()
		{
			var sel= this.fastening.selector;
			var checked= $(sel + ' input[data-fc-selector="Заявление_на_банкротство.Подано"]').attr('checked');
			if ('checked' != checked)
			{
				$(sel + ' input[data-fc-selector="Заявление_на_банкротство.Дата_подачи"]').val('');
			}
			else
			{
				var txt= $(sel + ' input[data-fc-selector="Заявление_на_банкротство.Дата_подачи"]').val();
				if ('' == txt)
				{
					var dnow= h_times.safeDateTime();
					$(sel + ' input[data-fc-selector="Заявление_на_банкротство.Дата_подачи"]').val(h_codec_datetime.Date2ru_date(dnow));
				}
			}
		}

		controller.OnChangeДата_подачи = function ()
		{
			var sel= this.fastening.selector;
			var txt= $(sel + ' input[data-fc-selector="Заявление_на_банкротство.Дата_подачи"]').val();
			if ('' == txt)
			{
				$(sel + ' input[data-fc-selector="Заявление_на_банкротство.Подано"]').attr('checked',null);
			}
			else
			{
				$(sel + ' input[data-fc-selector="Заявление_на_банкротство.Подано"]').attr('checked','checked');
			}
		}

		controller.Validate = function ()
		{
			var consent= this.GetFormContent();
			var vr= [];
			var block = function (msg) { vr.push({check_constraint_result:false,description:msg}); }
			switch (consent.Должник.Тип) {
				case 'Юр_лицо':
					if ('' == trim(consent.Должник.Юр_лицо.Наименование))
						block('Необходимо указать наименование должника!');
					break;
				case 'Физ_лицо':
					if ('' == trim(consent.Должник.Физ_лицо.Фамилия)) {
						block('Необходимо указать фамилию должника!');
					} else if(h_constraints.recommend_Ciryllic().check(consent.Должник.Физ_лицо.Фамилия)==='recommended') {
						block(h_constraints.recommend_Ciryllic().description(null, 'Фамилия'));
					}
					if(consent.Должник.Физ_лицо.Имя && h_constraints.recommend_Ciryllic().check(consent.Должник.Физ_лицо.Имя)==='recommended')
						block(h_constraints.recommend_Ciryllic().description(null, 'Имя'));
					if(consent.Должник.Физ_лицо.Отчество && h_constraints.recommend_Ciryllic().check(consent.Должник.Физ_лицо.Отчество)==='recommended')
						block(h_constraints.recommend_Ciryllic().description(null, 'Отчество'));
					if(consent.Должник.Физ_лицо.СНИЛС && !h_constraints.Snils().check(consent.Должник.Физ_лицо.СНИЛС))
						block(h_constraints.Snils().description(null, 'СНИЛС'));
					break;
				case 'Супруги':
					var cc_married_couple = this.fastening.get_fc_controller('Должник.Супруги');
					if(cc_married_couple.fastening) {
						var married_couple_result = cc_married_couple.Validate();
						vr = married_couple_result ? vr.concat(married_couple_result) : vr ;
					}
					break;
			}
			if(consent.Должник.ИНН && !h_constraints.Inn().check(consent.Должник.ИНН))
				block(h_constraints.Inn().description(null, 'ИНН'));
			if(consent.Должник.ОГРН && !h_constraints.Ogrn().check(consent.Должник.ОГРН))
				block(h_constraints.Ogrn().description(null, 'ОГРН'));
			var t= consent.Заявление_на_банкротство.Дата_подачи;
			if (null == consent.Заявление_на_банкротство.В_суд)
				block('Необходимо выбрать суд, в который было подано заявление о банкротстве!');
			if (null == consent.АУ)
				block('Необходимо выбрать АУ, который согласен провести процедуру!');
			if (''!=t && !(/^(0[1-9]|1\d|2\d|3[01])\.(0[1-9]|1[0-2])\.(19|20)\d{2}$/.test(t)))
				block('Дата подачи заявления (' + t + ') должно быть записано в формате дд.мм.гггг!');
			var t= consent.Заявление_на_банкротство.Дата_рассмотрения;
			if (''!=t && !(/^(0[1-9]|1\d|2\d|3[01])\.(0[1-9]|1[0-2])\.(19|20)\d{2} (20|21|22|23|[0-1]?\d{1}):([0-5]?\d{1})$/.test(t)))
				block('Время рассмотрения заявления должно быть записано в формате дд.мм.гггг чч:мм!');
			return 0==vr.length ? null : vr;
		}

		controller.OnActualizeCheckTime = function ()
		{
			var sel= this.fastening.selector;
			var self= this;
			h_validation_msg.IfOkWithValidateResult(this.Validate(), function ()
			{ 
				self.ActualizeCheckTime(function (data)
				{ 
					$(sel + ' input[data-fc-selector="Время_сверки"]').val(data.Время_сверки);
					if (!self.model)
						self.model = {id_ProcedureStart: data.id_ProcedureStart};
				});
			});
		}
		controller.ActualizeCheckTime = function (on_Время_сверки)
		{
			var dnow= h_times.safeDateTime();
			var Время_сверки= h_codec_datetime.Date2ru_legal(dnow);
			on_Время_сверки({ Время_сверки: Время_сверки });
		}

		controller.CheckKadArbitr = function (on_kad_arbitr_info)
		{
			var dnow= h_times.safeDateTime();

			var kad_arbitr_info = {
				Время_сверки: h_codec_datetime.Date2ru_legal(dnow)
			};

			on_kad_arbitr_info(kad_arbitr_info);
		}

		var check_fields= [
			{field_name:'Номер_судебного_дела', selector:'Номер_судебного_дела', title:'Номер судебного дела'}
			,{field_name:'Дата_рассмотрения', selector:'Заявление_на_банкротство.Дата_рассмотрения', title:'Дата рассмотрения'}
		];

		var show_and_get_changed_values = function (sel, kad_arbitr_info)
		{
			var changed_values= [];
			for (var i = 0; i < check_fields.length; i++)
			{
				var cf= check_fields[i];
				var iinput=  $(sel + ' input[data-fc-selector="'+ cf.selector + '"]');
				var old_value= iinput.val();
				if ('' == old_value)
				{
					var new_value= (!kad_arbitr_info[cf.field_name] || null==kad_arbitr_info[cf.field_name]) ? '' : kad_arbitr_info[cf.field_name];
					if (new_value != old_value)
					{
						iinput.val(new_value);
						changed_values.push({name:cf.title,value:new_value});
					}
				}
			}
			return changed_values;
		}

		controller.OnCheckKadArbitr = function ()
		{
			var self= this;
			var sel= this.fastening.selector;

			h_validation_msg.IfOkWithValidateResult(this.Validate(), function ()
			{ 
				var txt_Время_сверки= $(sel + ' input[data-fc-selector="Время_сверки"]').val();
				var dВремя_сверки= ''==txt_Время_сверки ? new Date(2000,1,1) : h_codec_datetime.ru_legal2Date(txt_Время_сверки);
				var dnow= h_times.safeDateTime();
				var span_seconds= (dnow.getTime() - dВремя_сверки.getTime())/1000;
				var min_span_seconds= 15 * 60; // не чаще 15 минут..

				if (span_seconds < min_span_seconds)
				{
					h_msgbox.ShowModal({
						title:'Отказ выполнять сверку'
						, html: v_start_check_pause({Время_сверки:txt_Время_сверки,Текущее_время:h_codec_datetime.Date2ru(dnow)})
						,width:500, id_div:'cpw-msg-can-not-check-kad-arbitr'
					});
				}
				else
				{
					self.CheckKadArbitr(function (kad_arbitr_info)
					{
						$(sel + ' input[data-fc-selector="Время_сверки"]').val(kad_arbitr_info.Время_сверки);

						var changed_values= show_and_get_changed_values(sel, kad_arbitr_info);

						if (!self.model)
							self.model = {id_ProcedureStart: kad_arbitr_info.id_ProcedureStart};

						h_msgbox.ShowModal({
							title:(0 == changed_values.length) ? 'Нет новой информации' : 'Обнаружена новая информация'
							,width:400, id_div:'cpw-msg-new-info-from-kad-arbitr'
							,html: v_start_new_info(changed_values)
						});
					});
				}
			});
		}

		controller.nameFormatter= function(person)
		{
			switch(person.Тип)
			{
				case 'Юр_лицо':
					return person.Юр_лицо.Наименование
				case 'Физ_лицо':
					var name= person.Физ_лицо.Фамилия + ' '
					if(person.Физ_лицо.Имя) { name+= person.Физ_лицо.Имя[0] + '.' }
					if(person.Физ_лицо.Отчество) { name+= person.Физ_лицо.Отчество[0]+'.' }
					return name
			}
		}

		controller.PrepareForm = function()
		{
			var sel = this.fastening.selector;
			if(this.readonly)
			{
				this.SetReadOnly();
				if(!this.model || !this.model.Заявитель || this.model.Заявитель.Заявитель_должник)
				{
					$(sel + ' .applicant').hide();
					$(sel + ' .applicant-is-debtor').show();
				}
			}

			if(!this.model || !this.model.id_ProcedureStart || (options_arg && options_arg.hideMakeRequestBtn))
				$(sel + ' .make-request').hide();

			$(sel + ' input[data-fc-selector="Заявление_на_банкротство.Дата_рассмотрения"]').inputmask("99.99.9999 99:99"); 
			$(sel).tooltip();
		}

		controller.PrepareToBaseRender = function (sel)
		{
			var self = this;
			var fspec= this.options.field_spec;

			fspec.Заявитель.controller = function() { return c_applicant({
				readonly: self.readonly
				, GetCurrentType: self.GetCurrentType.bind(self)
				, GetMarriedCouple: self.GetMarriedCouple.bind(self)
			}) }
			fspec.Заявитель.buttons = this.readonly ? ["Закрыть"] : null
			fspec.Заявитель.text = function (data)
			{
				if(data.Заявитель_должник) {
					return (self.GetCurrentType() === 'Супруги' ? h_applicant.Text.plural : h_applicant.Text.singular) + ' (кликните, чтобы поменять)'
				}
				return self.nameFormatter(data);
			}

			fspec.Запрос.text = function (data){
				self.OnChangeDecision(data)
				return '(кликните, чтобы поменять)';
			}
			fspec.Назначение.text = function (data) {
				self.OnChangeAppointment(data);
				return '(кликните, чтобы поменять)';
			}
			fspec.ЕФРСБ.text = function(data) {
				self.OnChangeEFRSB(data);
				return '(кликните, чтобы поменять)';
			}
		}

		controller.OnChangeDecision = function (decision) {
			var sel = this.fastening.selector;
			$openDecision = $(sel + ' .open-decision');
			$decisionAddInfo = $(sel + ' .decision-add-info');
			if(decision.Ссылка && decision.Ссылка !== '') {
				$openDecision.attr("href", decision.Ссылка);
				$openDecision.attr("target", "_blank");
				$openDecision.html('судебным актом от ' + decision.Время.акта);
				$(sel + ' .no-decision').hide();
				$openDecision.show();
			} else {
				$openDecision.attr("href", "#");
				$openDecision.attr("target", "_self");
				$openDecision.html('');
				$openDecision.hide();
				$(sel + ' .no-decision').show();
			}
			if(this.readonly && decision.Дополнительная_информация  && decision.Дополнительная_информация !== '') {
				$decisionAddInfo.show();
			} else {
				$decisionAddInfo.hide();
			}
		}

		controller.OnChangeAppointment = function (appointment) {
			var sel = this.fastening.selector;
			$openAppointment = $(sel + ' .open-appointment');
			$appointmentAddInfo = $(sel + ' .appointment-add-info');
			if(appointment.Ссылка && appointment.Ссылка !== '') {
				$openAppointment.attr("href", appointment.Ссылка);
				$openAppointment.attr("target","_blank");
				$openAppointment.html('судебным актом от ' + appointment.Время.акта);
				$(sel + ' .no-appointment').hide();
				$openAppointment.show();
			} else {
				$openAppointment.attr("href", "#");
				$openAppointment.attr("target","_self");
				$openAppointment.html('');
				$openAppointment.hide();
				$(sel + ' .no-appointment').show();
			}
			if(this.readonly && appointment.Дополнительная_информация && appointment.Дополнительная_информация !== '') {
				$appointmentAddInfo.show();
			} else {
				$appointmentAddInfo.hide();
			}
		}

		controller.OnChangeEFRSB = function(efrsb) {
			var sel = this.fastening.selector;
			$openEfrsbReport = $(sel + ' .open-efrsb_report');
			$efrsbReportAddInfo = $(sel + ' .efrsb_report-add-info');
			if(efrsb.ID && efrsb.ID !== '') {
				$openEfrsbReport
				.attr("href", "https://old.bankrot.fedresurs.ru/MessageWindow.aspx?ID=" + efrsb.ID)
				.attr("target","_blank")
				.html("на ЕФРСБ о введении")
				.html('№' + efrsb.Номер + ' от ' + efrsb.Дата_публикации)
				.show();
				$(sel + ' .no-efrsb_report').hide();
			} else {
				$openEfrsbReport.attr("href", "#");
				$openEfrsbReport.attr("target","_self");
				$(sel + ' .label-efrsb_report').html("на ЕФРСБ сообщение");
				$openEfrsbReport.html('');
				$openEfrsbReport.hide();
				$(sel + ' .no-efrsb_report').show();
			}
			if(this.readonly && efrsb.Дополнительная_информация && efrsb.Дополнительная_информация !== '') {
				$efrsbReportAddInfo.show();
			} else {
				$efrsbReportAddInfo.hide();
			}
		}

		controller.GetCurrentType = function() {
			var sel = this.fastening.selector;
			return $(sel + ' [data-fc-selector="Должник.Тип"]').val();
		}

		controller.GetMarriedCouple = function() {
			var model = this.GetFormContent();
			return model.Должник
		}

		controller.SetSingularApplicant = function() {
			var sel = this.fastening.selector;
			if(!this.model || !this.model.Заявитель || this.model.Заявитель.Заявитель_должник) {
				$(sel + ' .applicant').html(h_applicant.Text.singular + ' (кликните, чтобы поменять)')
				$(sel + ' .applicant-is-debtor').html(h_applicant.Text.singular)
			}
		}

		controller.SetPluralApplicant = function() {
			var sel = this.fastening.selector;
			if(!this.model || !this.model.Заявитель || this.model.Заявитель.Заявитель_должник) {
				$(sel + ' .applicant').html(h_applicant.Text.plural + ' (кликните, чтобы поменять)')
				$(sel + ' .applicant-is-debtor').html(h_applicant.Text.plural)
			}
		}

		return controller;
	}
});