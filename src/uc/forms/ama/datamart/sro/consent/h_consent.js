define([
    'forms/base/h_msgbox'
  , 'tpl!forms/ama/datamart/common/start/v_start_delete_confirm.html'
  , 'forms/ama/datamart/sro/consent/c_consent'
  , 'forms/base/h_validation_msg'
  , 'forms/ama/datamart/base/h_debtorName'
  , 'forms/ama/datamart/sro/request/h_request'
  , 'forms/base/h_times'
  , 'forms/base/codec/codec.copy'
],
function (h_msgbox, v_start_delete_confirm, c_consent, h_validation_msg, h_debtorName, h_request, h_times, codec_copy)
{
	return function (options_arg)
	{
		var helper = {};

		if (options_arg)
		{
			if (options_arg.on_change)
				helper.on_change = options_arg.on_change;
			if (options_arg.selector)
                helper.selector = options_arg.selector;
            if (options_arg.id_SRO)
				helper.id_SRO = options_arg.id_SRO;
		}
		helper.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		helper.base_crud_url = helper.base_url + '?action=consent.crud';
		helper.base_check_url = helper.base_url + '?action=consent.check';
		helper.base_request_url = helper.base_url + '?action=mrequest.based_on_ps' + (helper.id_SRO ? '&id_SRO=' + helper.id_SRO : '');

		helper.width = 920;
		helper.minHeight = 540;

		helper.requestHelper = h_request({base_url: helper.base_url, id_SRO: helper.id_SRO});

		helper.OnAdd = function ()
		{
			var self= this;
			var cc_consent = this.prepare_controller_consent();
			var btnOk= "Сохранить";
			h_msgbox.ShowModal({
				title:'Регистрация согласия на предоставление кандидатуры АУ'
				,controller:cc_consent
				,width: self.width
				,minHeight: self.minHeight
				,buttons:[btnOk,"Отмена"]
				, onclose: function (btn, dlg_div)
				{
					if (btnOk == btn)
					{
						h_validation_msg.IfOkWithValidateResult(cc_consent.Validate(), function () { 
							var consent= cc_consent.GetFormContent();
							self.Add(consent);
							$(dlg_div).dialog("close");
						});
						return false;
					}
				}
			});
		}

		helper.Add = function (consent)
		{
			var self= this;
			var ajaxurl = this.base_crud_url + 
				(!consent.id_ProcedureStart 
					? '&cmd=add' + ( this.id_SRO ? '&id_SRO='+this.id_SRO : '' )
					: '&cmd=update&id=' + consent.id_ProcedureStart);
            var v_ajax = h_msgbox.ShowAjaxRequest("Отправка согласия на сервер", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'POST'
				, data: consent
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else {							
						if(self.on_change)
							self.on_change();
					}

				}
			});
		}

		helper.prepare_controller_consent = function (readonly)
		{
			var self= this;
            var cc_consent = c_consent({base_url:this.base_url,readonly:readonly});
			cc_consent.ActualizeCheckTime = function (on_Время_сверки)
			{
				self.ActualizeCheckTime(cc_consent, on_Время_сверки);
			}
			cc_consent.CheckKadArbitr = function (on_kad_arbitr_info)
			{
				self.CheckKadArbitr(cc_consent,on_kad_arbitr_info);
			}
			cc_consent.MakeRequest = function (consent)
			{
				self.requestHelper.OnAdd(self.ConsentToMrequest(consent));
			}
			return cc_consent;
		}

		helper.CheckKadArbitr = function (cc_consent, on_kad_arbitr_info)
		{
			var self= this;
			var consent = cc_consent.GetFormContent();
			var ajaxurl = this.base_check_url + '&cmd=search'+( this.id_SRO ? '&id_SRO='+this.id_SRO : '' );
			var v_ajax = h_msgbox.ShowAjaxRequest("Проверка дела на сервере", ajaxurl, 'cpw-id-find-out-consent-form');
			v_ajax.ajax
			({
				dataType: "json", type: 'POST', data: consent
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						if (self.on_change)
							self.on_change();
						on_kad_arbitr_info(data);
					}
				}
			});
		}

		helper.ActualizeCheckTime = function (cc_consent, on_Время_сверки)
		{
			var self= this;
			var consent = cc_consent.GetFormContent();
			var ajaxurl = this.base_check_url + '&cmd=actualize' + ( this.id_SRO ? '&id_SRO='+this.id_SRO : '' );
			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка времени сверки на сервер", ajaxurl, 'cpw-id-actualize-check-time-form');
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'POST'
				, data: consent
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						if (self.on_change)
							self.on_change();
						on_Время_сверки(data);
					}
				}
			});
		}

		helper.OnEdit= function ()
		{
			var grid = $(this.selector + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			$(this.selector + " ul.jquery-menu").hide();
			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=get' + (this.id_SRO ? '&id_SRO='+ this.id_SRO:'') + '&id=' + rowdata.id_ProcedureStart;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных согласия с сервера", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						// если не создано СРО и привязано к запросу
						var readonly = (rowdata.addedBySRO==0) || rowdata.id_MRequest || null;
						self.DoEdit(rowdata.id_ProcedureStart, data, readonly);
					}
				}
			});
		}

		helper.DoEdit= function (id_ProcedureStart,consent,readonly)
		{
			var self= this;
			var cc_consent= this.prepare_controller_consent(readonly);
			cc_consent.SetFormContent(consent);
			var btnOk= "Сохранить";
			h_msgbox.ShowModal({
				title:'Согласие на предоставление кандидатуры АУ'
				,controller:cc_consent
				,buttons:readonly?["Закрыть"]:[btnOk,"Отмена"]
				,width: self.width
				,minHeight: self.minHeight
				, onclose: function (btn, dlg_div)
				{
					if (btnOk == btn)
					{
						h_validation_msg.IfOkWithValidateResult(cc_consent.Validate(), function () { 
							var edited_consent= cc_consent.GetFormContent();
							self.Add(edited_consent);
							$(dlg_div).dialog("close");
						});
						return false;
					}
				}
			});
		}

		helper.OnRemove = function ()
		{
			var grid = $(this.selector + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;
			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal
			({
				title: 'Подтверждение удаления' // заявлений
				, html: v_start_delete_confirm(rowdata)
				, width: 600
				, id_div: "cpw-form-ama-datamart-sro-procedure-start-delete-confirm"
				, buttons: [btnOk, 'Отмена']
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						self.Remove(rowdata.id_ProcedureStart);
					}
				}
			});
		}

		helper.Remove = function (id_ProcedureStart)
		{
			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=delete&id=' + id_ProcedureStart;
			var v_ajax = h_msgbox.ShowAjaxRequest("Удаления данных о заявлении с сервера", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
                        if(data.ok && self.on_change) {
							self.on_change();
						}else{
							h_msgbox.ShowModal({
								title:'Отказ выполнять удаление согласия'
								, html: '<p>' + data.message +'</p>'
								,width:500, id_div:'cpw-msg-can-not-delete-consent'
							});
						}
					}
				}
			});
		}

		helper.ConsentToMrequest = function(consent) {
			var mrequest = codec_copy().Copy(consent);
			var currentDate = h_times.stringifyDateUTC('r', h_times.safeDateTime());
			mrequest.Запрос = {
				Ссылка: consent.Запрос.Ссылка,
				Время: {
					акта: consent.Запрос.Время.акта,
					получен: true,
					получения: currentDate,
					опрошен_ау: true,
					опроса: currentDate
				},
				Дополнительная_информация: consent.Запрос.Дополнительная_информация
			};
			mrequest.Согласия_АУ = {
				В_отчет: consent.id_ProcedureStart,
				Согласия: [{
					АУ: {
						id: consent.АУ.id_Manager,
						text: consent.АУ.text
					},
					id_ProcedureStart: consent.id_ProcedureStart,
					Заявление_на_банкротство: {
						Дата_подачи: consent.Заявление_на_банкротство.Дата_подачи
					}
				}]
			};
			delete mrequest.id_ProcedureStart;
			delete mrequest.АУ;

			return mrequest;
		}

		helper.managerFormatter = function (cellvalue, options, rowObject)
		{
			var res= rowObject.Manager;
			if (null==res)
				res= '';
			if (rowObject.ReadyToRegistrate)
				res= 'назначен. ' + res;
			return res;
		}

		helper.debtorNameFormatter= function(cellvalue, options, rowObject)
		{
			var getShortName = function(parts) {
				var res= parts[0];
				if (1 < parts.length)
				{
					for (var i = 1; i < parts.length; i++)
					{
						if(parts[i]) {
							if(i==1) res+= ' ';
							res += parts[i].charAt(0) + '.';
						}
					}
				}
				return res
			}
			if ('n' != rowObject.DebtorCategory)
			{
				return h_debtorName.beautify_debtorName(rowObject.debtorName);
			}
			else
			{
				var res = getShortName(rowObject.debtorName.split(' '));
				if(rowObject.debtorName2)
					res += ', ' +getShortName(rowObject.debtorName2.split(' '));
				return res;
			}
		}

		return helper;
	}
});