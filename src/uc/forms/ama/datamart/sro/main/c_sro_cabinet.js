﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/main/e_sro_cabinet.html'
	, 'forms/ama/datamart/sro/requests/c_dm_requests'
	, 'forms/ama/datamart/sro/consents/c_dm_consents'
	, 'forms/ama/datamart/sro/managers/c_sro_managers'
	, 'forms/ama/datamart/sro/notifications/c_sro_notifications'
	, 'forms/ama/datamart/sro/props/h_sro_props'
	, 'forms/ama/datamart/sro/documents/h_sro_documents'
],
function (c_fastened, tpl, c_dm_requests, c_dm_consents, c_sro_managers, c_sro_notifications, h_sro_props, h_sro_documents)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'ama/datamart' : options_arg.base_url;

		var options = {
			field_spec:
				{
					Запросы: { controller: function () { return c_dm_requests({ base_url: base_url }); }, render_on_activate:true }
					, Заявления: { controller: function () { return c_dm_consents({ base_url: base_url }); }, render_on_activate:true }
					, АУ: { controller: function () { return c_sro_managers({ base_url: base_url }); }, render_on_activate:true }
					, УведомленияАУ: { controller: function () { return c_sro_notifications({ base_url: base_url }); }, render_on_activate: true }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' div > header button.logout').click(function () { if (self.OnLogout) self.OnLogout(); });

			$(sel + " .jquery-menu").menu();

			$(sel + " .profile-dropdown").click(function (e)
			{
				e.preventDefault();
				$(this).find("ul.jquery-menu").toggle();
			})

			//close menu
			$(document).on("click", function (e)
			{
				var target = $(e.target);
				/*menu buttons*/
				if (target.hasClass("open-profile")) self.OnProfile();
				if (target.hasClass("logout")) { if (self.OnLogout) { self.OnLogout(); return false; } }
				if (target.hasClass("notifications")) self.OnNotifications();
				if (target.hasClass("open-props")) self.OnProps();
				if (target.hasClass("open-docs")) self.OnDocuments();

				if (!target.hasClass("profile-dropdown") &&
					!target.parent().hasClass("profile-dropdown") &&
					!target.parent().parent().hasClass("profile-dropdown"))
				{
					$(sel + " .profile-dropdown ul.jquery-menu").hide();
				}
			});

			$(sel + ' ul.ui-tabs-nav > li > a').click(function () {  });
			$(sel + ' div#cpw-ama-datamart-sro-main-tabs').on('tabsactivate', function (event, ui) { self.OnActivateTab(event, ui); });
		}

		controller.OnActivateTab = function (event, ui)
		{
			var tab_id= ui.newPanel.attr('id');
			switch (tab_id)
			{
				case 'cpw-ama-datamart-sro-main-reqt':
				this.ReloadRequests();
				break;
				case 'cpw-ama-datamart-sro-main-strt':
				this.ReloadConsents();
				break;
			}
		}

		controller.ReloadRequests = function(){
			var fastening= this.fastening;
			var c_requests= fastening.get_fc_controller('Запросы');
			if(c_requests.fastening)
				c_requests.ReloadGrid();
		}

		controller.ReloadConsents = function(){
			var fastening= this.fastening;
			var c_consents= fastening.get_fc_controller('Заявления');
			if(c_consents.fastening)
				c_consents.ReloadGrid();
		}

		controller.OnProps = function () {
			h_sro_props.ModalEditSroProps(base_url, this.model.id_SRO);
		}

		controller.OnDocuments = function () {
			h_sro_documents.ModalEditSroDocuments(base_url, this.model);
		}

		return controller;
	}
});