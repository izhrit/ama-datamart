include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet
je app.cpw_Now= function(){return new Date(2017,1,1,1,1,1); }

wait_text "kad.arbitr"
shot_check_png ..\..\shots\06sav.png


# Начало проверки созданного согласия

js wbt.CheckModelFieldValue("Должник.Тип", "Супруги");

js $('.cpw-ama-datamart-request .cpw-ama-datamart-married_couple .debtor1').click();
wait_text "Информация о должнике"
check_stored_lines ama_datamart_debtor_physical_fields_1
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Отмена")').click()
wait_text_disappeared "Информация о должнике"

js $('.cpw-ama-datamart-request .cpw-ama-datamart-married_couple .debtor2').click();
wait_text "Информация о должнике"
check_stored_lines ama_datamart_debtor_physical_fields_2
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Отмена")').click()
wait_text_disappeared "Информация о должнике"

js wbt.CheckModelFieldValue("Заявление_на_банкротство.В_суд", 'Москвы');

wait_click_text "Тростников С.М."
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-request-applicant"] '
	wait_text "Информация о заявителе"
	check_stored_lines ama_datamart_applicant_fields_1
	wait_click_text "Сохранить информацию о заявителе"
	wait_text "Тростников С.М."
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-request .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
check_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

# Конец проверки созданного согласия


# Начало заполнения формы создания согласия

js wbt.SetModelFieldValue("Должник.Тип", "Супруги");

js $('.cpw-ama-datamart-request .cpw-ama-datamart-married_couple .debtor1').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_2
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js $('.cpw-ama-datamart-request .cpw-ama-datamart-married_couple .debtor2').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_1
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

wait_text "Заявителями являются должники (кликните, чтобы поменять)"

js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Удмуртской');

js $(".cpw-ama-datamart-request .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
play_stored_lines ama_datamart_court_decision_fields_2
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

# Конец заполнения формы создания согласия

shot_check_png ..\..\shots\07edt.png


wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\07edt-married.json.result.txt
wait_click_full_text "Редактировать модель в элементе управления"
wait_text "kad.arbitr"

js $('.cpw-ama-datamart-request .cpw-ama-datamart-married_couple .debtor1').click();
wait_text "Информация о должнике"
check_stored_lines ama_datamart_debtor_physical_fields_2
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Отмена")').click()
wait_text_disappeared "Информация о должнике"

js $('.cpw-ama-datamart-request .cpw-ama-datamart-married_couple .debtor2').click();
wait_text "Информация о должнике"
check_stored_lines ama_datamart_debtor_physical_fields_1
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Отмена")').click()
wait_text_disappeared "Информация о должнике"

wait_text "Заявителями являются должники (кликните, чтобы поменять)"

js $(".cpw-ama-datamart-request .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
check_stored_lines ama_datamart_court_decision_fields_2
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

shot_check_png ..\..\shots\07edt.png

exit