include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet
je app.cpw_Now= function(){return new Date(2020, 4, 24, 17, 36, 0, 0 );}
execute_javascript_stored_lines add_wbt_std_functions

wait_text "Должник:"
shot_check_png ..\..\shots\00new.png


# Начало заполнения формы создания запроса

play_stored_lines request_fields_1

# Конец заполнения формы создания запроса


wait_click_full_text "Подготовить"
wait_text "Ответ"
wait_click_text "Ответ"
wait_text "Документ-ответ успешно сформирован"
shot_check_png ..\..\shots\03dwnld_responce.png
wait_click_text "OK"
wait_text_disappeared "Документ-ответ успешно сформирован"

wait_click_full_text "Подготовить"
wait_text "Протокол"
wait_click_text "Протокол"
wait_text "Документ-протокол успешно сформирован"
shot_check_png ..\..\shots\03dwnld_protocol.png
wait_click_text "OK"
wait_text_disappeared "Документ-протокол успешно сформирован"

wait_click_full_text "Подготовить"
wait_text "Выписка"
wait_click_text "Выписка"
wait_text "Выписка успешно сформирована и скачана"
shot_check_png ..\..\shots\03dwnld_discharge.png
wait_click_text "OK"
wait_text_disappeared "Выписка успешно сформирована и скачана"

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\03dwnld.json.result.txt
exit