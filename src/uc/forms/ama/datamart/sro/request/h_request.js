﻿define([
	'forms/base/h_msgbox'
	, 'tpl!forms/ama/datamart/sro/request/v_dm_request_delete_confirm.html'
	, 'forms/ama/datamart/sro/request/c_request_wrapper'
	, 'forms/base/h_validation_msg'
	, 'forms/ama/datamart/base/h_debtorName'
],
	function (h_msgbox, v_dm_request_delete_confirm, c_request, h_validation_msg, h_debtorName) {
		return function (options_arg) {
			var helper = {};

			if (options_arg) {
				if (options_arg.on_change)
					helper.on_change = options_arg.on_change;
				if (options_arg.selector)
					helper.selector = options_arg.selector;
				if (options_arg.id_SRO)
					helper.id_SRO = options_arg.id_SRO
			}
			helper.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
			helper.base_crud_url = helper.base_url + '?action=mrequest.crud' + (helper.id_SRO ? '&id_SRO=' + helper.id_SRO : '');
			helper.base_dwnld_url = helper.base_url + '?action=mrequest.dwnl_doc';

			helper.width = 920;
			helper.minHeight = 540;

			helper.OnAdd = function (request) {
				var self = this;
				var cc_request = this.prepare_controller_request();
				if(request)
					cc_request.SetFormContent(request);
				var btnOk = "Сохранить";
				h_msgbox.ShowModal({
					title: 'Регистрация запроса кандидатур АУ из суда'
					, controller: cc_request
					, id_div: "cpw-form-ama-datamart-sro-request-create-form"
					, width: self.width
					, minHeight: self.minHeight
					, buttons: [btnOk, "Отмена"]
					, onclose: function (btn, dlg_div) {
						if (btnOk == btn) {
							h_validation_msg.IfOkWithValidateResult(cc_request.Validate(), function () {
								var request = cc_request.GetFormContent();
								self.Add(request);
								$(dlg_div).dialog("close");
							});
							return false;
						}
					}
				});
			}

			helper.Add = function (request) {
				var self = this;
				var ajaxurl = this.base_crud_url +
					(!request.id_MRequest
						? '&cmd=add&'
						: '&cmd=update&id=' + request.id_MRequest);
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка заявления на сервер", ajaxurl);
				v_ajax.ajax
					({
						dataType: "json"
						, type: 'POST'
						, data: request
						, success: function (data, textStatus) {
							if (null == data) {
								v_ajax.ShowAjaxError(data, textStatus);
							}
							else {
								if (self.on_change)
									self.on_change();
							}
						}
					});
			}

			helper.prepare_controller_request = function (id_MRequest, updateRequest) {
				var self= this;
				var сc_request = c_request({ base_url: this.base_url, id_SRO: this.id_SRO, id_MRequest: id_MRequest || null, updateRequest: updateRequest || null });
				сc_request.DownloadResponce = function (on_DownloadResponce) {
					self.DownloadResponce(сc_request, on_DownloadResponce);
				}
				сc_request.DownloadProtocol = function (on_DownloadProtocol) {
					self.DownloadProtocol(сc_request, on_DownloadProtocol);
				}
				сc_request.DownloadDischarge = function (on_DownloadDischarge) {
					self.DownloadDischarge(сc_request, on_DownloadDischarge);
				}
				сc_request.DownloadConsent = function () {
					self.DownloadConsent(сc_request);
				}
				return сc_request;
			}

			helper.OnEdit = function () {
				var grid = $(this.selector + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');
				var rowdata = grid.jqGrid('getRowData', selrow);
				$(this.selector + " ul.jquery-menu").hide();

				var self = this;
				var ajaxurl = this.base_crud_url + '&cmd=get&id=' + rowdata.id_MRequest;
				var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных запроса кандидатур с сервера", ajaxurl);
				v_ajax.ajax
					({
						dataType: "json"
						, type: 'GET'
						, cache: false
						, success: function (data, textStatus) {
							if (null == data) {
								v_ajax.ShowAjaxError(data, textStatus);
							}
							else {
								self.DoEdit(rowdata.id_MRequest, data);
							}
						}
					});
			}

			helper.DoEdit = function (id_MRequest, request) {
				var self = this;
				var cc_request = this.prepare_controller_request(id_MRequest, this.Update.bind(this));
				cc_request.SetFormContent(request);
				var btnOk = "Сохранить";
				h_msgbox.ShowModal({
					title: 'Регистрация запроса кандидатур АУ из суда'
					, controller: cc_request
					, buttons: [btnOk, "Отмена"]
					, width: self.width
					, minHeight: self.minHeight
					, onclose: function (btn, dlg_div) {
						if (btnOk == btn) {
							h_validation_msg.IfOkWithValidateResult(cc_request.Validate(), function () {
								var edited_request = cc_request.GetFormContent();
								self.Update(id_MRequest, edited_request);
								$(dlg_div).dialog("close");
							});
							return false;
						}
					}
				});
			}

			helper.Update = function (id_MRequest, request) {
				var self = this;
				var ajaxurl = this.base_crud_url + '&cmd=update&id=' + id_MRequest;
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка изменений запроса кандидатур на сервер", ajaxurl);
				v_ajax.ajax
					({
						dataType: "json"
						, type: 'POST'
						, data: request
						, success: function (data, textStatus) {
							if (null == data) {
								v_ajax.ShowAjaxError(data, textStatus);
							}
							else {
								if (self.on_change)
									self.on_change();
							}
						}
					});
			}

			helper.OnRemove = function () {
				var grid = $(this.selector + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');
				var rowdata = grid.jqGrid('getRowData', selrow);
				var self = this;
				var btnOk = 'Да, удалить';
				h_msgbox.ShowModal
					({
						title: 'Подтверждение удаления' // запросов кандидатур
						, html: v_dm_request_delete_confirm(rowdata)
						, width: 600//, height: 250
						, id_div: "cpw-form-ama-datamart-sro-request-delete-confirm"
						, buttons: [btnOk, 'Отмена']
						, onclose: function (btn) {
							if (btn == btnOk) {
								self.Remove(rowdata.id_MRequest);
							}
						}
					});
			}

			helper.Remove = function (id_MRequest) {
				var self = this;
				var ajaxurl = this.base_crud_url + '&cmd=delete&id=' + id_MRequest;
				var v_ajax = h_msgbox.ShowAjaxRequest("Удаления данных о запросе кандидатур с сервера", ajaxurl);
				v_ajax.ajax
					({
						dataType: "json"
						, type: 'GET'
						, cache: false
						, success: function (data, textStatus) {
							if (null == data) {
								v_ajax.ShowAjaxError(data, textStatus);
							}
							else {
								if (self.on_change)
									self.on_change();
							}
						}
					});
			}

			helper.DownloadResponce = function(cc_request, on_DownloadResponce) {
				var self= this;
				var request= cc_request.GetFormContent();
				var self = this;
				var ajaxurl = this.base_crud_url +
					(!request.id_MRequest && request.id_MRequest == undefined
						? '&cmd=add&'
						: '&cmd=update&id=' + request.id_MRequest);
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка заявления на сервер", ajaxurl);
				v_ajax.ajax
					({
						dataType: "json"
						, type: 'POST'
						, data: request
						, success: function (res, textStatus) {
							if (null == res) {
								v_ajax.ShowAjaxError(data, textStatus);
							}
							else {
								on_DownloadResponce({id_MRequest: request.id_MRequest || res.data.id});
								if(self.base_url == 'ama/datamart') {
									h_msgbox.ShowModal({
										title:'Документ-ответ успешно сформирован и скачан'
										,width:500, id_div:'cpw-msg-success-download-doc'
									});
								}else {
									window.open(self.base_dwnld_url + '&doctype=responce&id_MRequest=' + (request.id_MRequest || res.data.id));
								}
								if (self.on_change)
									self.on_change();
							}
						}
					});
			}

			helper.DownloadProtocol = function(cc_request,on_DownloadProtocol) {
				var self= this;
				var request= cc_request.GetFormContent();
				var self = this;
				var self = this;
				var ajaxurl = this.base_crud_url +
					(!request.id_MRequest
						? '&cmd=add&'
						: '&cmd=update&id=' + request.id_MRequest);
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка заявления на сервер", ajaxurl);
				v_ajax.ajax
					({
						dataType: "json"
						, type: 'POST'
						, data: request
						, success: function (res, textStatus) {
							if (null == res) {
								v_ajax.ShowAjaxError(res, textStatus);
							}
							else {
								if(self.base_url == 'ama/datamart') {
									h_msgbox.ShowModal({
										title:'Документ-протокол успешно сформирован и скачан'
										,width:500, id_div:'cpw-msg-success-download-doc'
									});
								}else {
									on_DownloadProtocol({id_MRequest: request.id_MRequest || res.data.id});
									window.open(self.base_dwnld_url + '&doctype=protocol' + '&id_MRequest=' + (request.id_MRequest || res.data.id));
								}
								if (self.on_change)
									self.on_change();
							}
						}
					});
			}

			helper.DownloadDischarge = function(cc_request, on_DownloadDischarge)
			{
				var self= this;
				var request= cc_request.GetFormContent();
				var self = this;
				var self = this;
				var ajaxurl = this.base_crud_url +
					(!request.id_MRequest ? '&cmd=add&' : '&cmd=update&id=' + request.id_MRequest);
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка заявления на сервер", ajaxurl);
				v_ajax.ajax
					({
						dataType: "json"
						, type: 'POST'
						, data: request
						, success: function (res, textStatus) {
							if (null == res)
							{
								v_ajax.ShowAjaxError(res, textStatus);
							}
							else
							{
								if (self.base_url == 'ama/datamart')
								{
									h_msgbox.ShowModal({title:'Выписка успешно сформирована и скачана',width:500, id_div:'cpw-msg-success-download-discharge'});
								}
								else
								{
									on_DownloadDischarge({id_MRequest: request.id_MRequest || res.data.id});
									var id_ProcedureStart = (!request.Согласия_АУ || !request.Согласия_АУ.В_отчет) ? null : request.Согласия_АУ.В_отчет;
									if (!id_ProcedureStart)
									{
										h_msgbox.ShowModal({
											title:'Невозможно получить выписку'
											,html:'Не выбран ни один согласный АУ для ответа.</br>Для получения выписки необходимо выбрать АУ для ответа.'
											,width:500, id_div:'cpw-msg-fail-download-discharge'
										});
									}
									else
									{
										window.open(self.base_dwnld_url + '&doctype=discharge&id_ProcedureStart=' + id_ProcedureStart)
									}
								}
								if (self.on_change)
									self.on_change();
							}
						}
					});
			}

			helper.DownloadConsent = function (cc_request) {
				var self = this;
				var request = cc_request.GetFormContent();
				var self = this;
				var self = this;
				var ajaxurl = this.base_crud_url +
					(!request.id_MRequest
						? '&cmd=add&'
						: '&cmd=update&id=' + request.id_MRequest);
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка заявления на сервер", ajaxurl);
				v_ajax.ajax
					({
						dataType: "json"
						, type: 'POST'
						, data: request
						, success: function (data, textStatus) {
							if (null == data) {
								v_ajax.ShowAjaxError(data, textStatus);
							}
							else {
								if (data.id)
									request.id_MRequest = data.id;
								if (self.base_url == 'ama/datamart') {
									h_msgbox.ShowModal({
										title: 'Документ-согласие успешно сформирован и скачан'
										, width: 500, id_div: 'cpw-msg-success-download-doc'
									});
								} else {
									window.open(self.base_dwnld_url + '&doctype=consent' + '&id_MRequest=' + (request.id_MRequest || data.id));
								}
								if (self.on_change)
									self.on_change();
							}
						}
					});
			}

			helper.debtorNameFormatter= function(cellvalue, options, rowObject)
			{
				var getShortName = function(parts) {
					var res= parts[0];
					if (1 < parts.length)
					{
						
						for (var i = 1; i < parts.length; i++)
						{
							if(parts[i]) {
								if(i==1) res+= ' ';
								res += parts[i].charAt(0) + '.';
							}
						}
					}
					return res
				}
				if ('n' != rowObject.DebtorCategory)
				{
					return h_debtorName.beautify_debtorName(rowObject.debtorName);
				}
				else
				{
					var res = getShortName(rowObject.debtorName.split(' '));
					if(rowObject.debtorName2)
						res += ', ' +getShortName(rowObject.debtorName2.split(' '));
					return res;
				}
			}

			return helper;
		}
	});