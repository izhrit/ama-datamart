define([
	'tpl!forms/ama/datamart/sro/request/v_dm_request_debtor.html'
], function(v_dm_request_debtor) {
	return function(options_arg) {
		var helper = {};
		helper.id_SRO = options_arg && options_arg.id_SRO;
		helper.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		helper.base_url_debtor_typeahead = helper.base_url + '?action=mrequest.typeahead';

		helper.citizenSearch = function(props) {
			if(!props) props = {};
			var self = this;
			return [
				{
					hint: true,
					highlight: true,
					minLength: 1
				},
				{
					name: 'debtors',
					display: function (debtor) {
						switch(debtor.category) {
							case 'l':
								return debtor.name;
							case 'n':
								var fioArray= debtor.name.split(' ');
								return fioArray[0];
						}
					},
					templates: {
						suggestion: self.request_ajax_debtor_suggestion
					},
					source: self.request_ajax_debtor('n', props.isMarriedCouple)
				}
			]
		}

		helper.organizationSearch = function () {
			var self = this;
			return [
				{
					hint: true,
					highlight: true,
					minLength: 1
				},
				{
					name: 'debtors',
					display: function (debtor) {
						return debtor.name
					},
					templates: {
						suggestion: self.request_ajax_debtor_suggestion
					},
					source: self.request_ajax_debtor('l')
				}
			]
		}

		helper.request_ajax_debtor_suggestion = function (data) {
			var div = document.createElement('div');
			div.innerHTML = v_dm_request_debtor(data);
			return div
		}

		helper.request_ajax_debtor = function (debtorCategory, isMarriedCouple) {
			var bloodhound = new Bloodhound({
				datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				remote: {
					url: this.base_url_debtor_typeahead,
					replace: function (url, query) {
						var newUrl = url
						if (debtorCategory)
							newUrl = newUrl + '&debtorCategory=' + debtorCategory
						if (isMarriedCouple)
							newUrl = newUrl + '&isMarriedCouple=' + 1
						if(this.id_SRO)
							newUrl = newUrl + '&id_SRO=' + this.id_SRO
						return newUrl + '&query=' + encodeURIComponent(query)
					},
					cache: false
				}
			});

			bloodhound.initialize();

			return bloodhound;
		}

		helper.fixInModal = function (sel) {
			var ta = $(sel + ' .typeahead.tt-input');
			ta.on('typeahead:open', function() {
				var $menu = ta.parent().find('.tt-menu');
				var me = $menu[0];
				me.offset = ta.offset();
				me.offset.top += ta.outerHeight();
				me.position = ta.position();
				$('body').append($menu);
				$menu.show();
				$menu.css('position', 'absolute');
				$menu.css('top', (me.offset.top) + 'px');
				$menu.css('left', (me.offset.left) + 'px');
				$menu.css('opacity', 1);
				$(this).data("myDropdownMenu", $menu);
			});
			ta.on('typeahead:close', function() {
				$(this).parent().append($(this).data("myDropdownMenu"));
				var $menu = ta.parent().find('.tt-menu');
				$menu.css('top', '100%');
				$menu.css('left', '0px');
			});
		}

		return helper;
	}
});