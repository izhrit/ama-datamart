﻿	define([
		'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/request/e_request.html'
	, 'forms/ama/datamart/sro/request/h_request_docs'
	, 'forms/ama/datamart/sro/request/h_request_search_consent'
	, 'forms/ama/datamart/common/request_consents/c_request_consents'
	, 'forms/ama/datamart/common/applicant/c_applicant'
	, 'forms/ama/datamart/common/applicant/h_applicant'
	, 'forms/ama/datamart/common/court_decision/c_dm_court_decision'
	, 'forms/ama/datamart/common/court_decision/h_dm_court_decision'
	, 'forms/ama/datamart/common/married_couple/c_married_couple'
	, 'forms/base/h_times'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_constraints'
	],
	function (c_fastened, tpl
		, h_request_docs, h_search_consent, c_request_consents, c_applicant, h_applicant, c_court_decision, h_court_decision, c_married_couple
		, h_times, h_msgbox, h_constraints
	) {
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_court_select2 = base_url + '?action=court.select2'

		var options = {
			base_url: base_url
			, ApplicantText: h_applicant.Text
			, field_spec:
				{
					'Должник.Супруги':
					{
						controller: c_married_couple
						, title: 'Информация о должнике'
					}
					, "Заявление_на_банкротство.В_суд": { 
						placeholder:'Арбитражный суд (обязательно выберите!)'
						, ajax: { url: base_url_court_select2, dataType: 'json' }
					}
					, 'Заявитель':
					{
						controller: c_applicant
						, title: 'Информация о заявителе'
						, id_div: 'cpw-form-ama-request-applicant'
						, btn_ok_title: 'Сохранить информацию о заявителе'
						, text: function(){}
					}
					, 'Запрос':
					{
							controller: c_court_decision
						, title: 'Судебный акт о запросе кандидатур АУ'
						, id_div: 'cpw-form-ama-request-court-decision'
						, btn_ok_title: 'Сохранить информацию о запросе кандидатур АУ судом'
						, text: function(){}
					}
					, 'Должник.Физ_лицо.Фамилия': []
					, 'Должник.Юр_лицо.Наименование': []
					, 'Согласия_АУ': { controller: function() { return c_request_consents({
						base_url: base_url
						, id_SRO: options_arg && options_arg.id_SRO
					})} }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			this.id_SRO = options_arg && options_arg.id_SRO;
			this.search_consent = h_search_consent({id_SRO: this.id_SRO, base_url: base_url});
			this.DispatchRequestUpstairs = options_arg && options_arg.DispatchRequestUpstairs;
			this.PrepareToBaseRender(sel);
			base_Render.call(this,sel);
			var self= this;

			$(sel + ' button.open').click(function () { self.OnOpen(); });
			$(sel + ' button.court-by-number').click(function () { self.OnSelectCourtByCaseNumber(); });
			$(sel + ' select.type').change(function () { self.OnSelectType(); });
			$(sel + ' .typeahead').on('typeahead:selected', function (e, datum) {
				self.DispatchRequestUpstairs(datum.Request);
				self.SelectConsentIfExist(datum);
				if(datum.Request && datum.Request && datum.Request.Согласия_АУ &&
					datum.Request.Согласия_АУ.Согласия && datum.Request.Согласия_АУ.Согласия.length !== 0) {
					$(sel + ' .request-contents-and-btn-wrapper>.row>button').focus();
				}
			});

			$(sel + ' [data-fc-selector="Запрос.Время.получения"]').change(function () {
				self.OnChangeDataInput('Запрос.Время.получен', $(this).val())
			})
			$(sel + ' [data-fc-selector="Запрос.Время.получен"]').change(function () {
				self.OnCheckCheckbox('Запрос.Время.получения', $(this).attr('checked'))
			})

			$(sel + ' [data-fc-selector="Запрос.Время.опроса"]').change(function () {
				self.OnChangeDataInput('Запрос.Время.опрошен_ау', $(this).val())
			})
			$(sel + ' [data-fc-selector="Запрос.Время.опрошен_ау"]').change(function () {
				self.OnCheckCheckbox('Запрос.Время.опроса', $(this).attr('checked'))
			})

			$(sel + ' [data-fc-selector="Запрос.Время.ответа"]').change(function () {
				self.OnChangeDataInput('Запрос.Время.отвечен', $(this).val())
			})
			$(sel + ' [data-fc-selector="Запрос.Время.отвечен"]').change(function () {
				self.OnCheckCheckbox('Запрос.Время.ответа', $(this).attr('checked'))
			})

			$(sel + ' .prepare-docs-btn').click(function() { h_request_docs.docsMenu.open(self); })

			self.PrepareForm();
		}

		var base_Destroy= controller.Destroy;
		controller.Destroy = function () {
			h_request_docs.docsMenu.destroy();
			base_Destroy.call(this);
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function () {
			var request = base_GetFormContent.call(this);
			var Должник = request.Должник;
			switch (Должник.Тип) {
				case 'Физ_лицо':
					if (Должник.Юр_лицо)
						delete Должник.Юр_лицо;
					if (Должник.Супруги)
						delete Должник.Супруги;
					break;
				case 'Юр_лицо':
					if (Должник.Физ_лицо)
						delete Должник.Физ_лицо;
					if (Должник.Супруги)
						delete Должник.Супруги;
					break;
				case 'Супруги':
					request.Должник = {
						Тип: Должник.Тип,
						Супруги: Должник.Супруги
					}
					break;
			}

			if(!request.Заявитель)
				request.Заявитель = h_applicant.GetDefaultObject();

			request.Запрос = h_court_decision.GetDefaultObject(request.Запрос);

			delete request.Запрос_дополнительная_информация;

			return request;
		}

		controller.OnSelectType = function () {
			var sel = this.fastening.selector;
			var type = $(sel + ' select.type').val();
			var root_item = $(sel + ' div.cpw-ama-datamart-request');
			switch (type) {
				case 'Юр_лицо': 
					root_item.removeClass('citizen marriage_couple').addClass('organization'); 
					$(sel + ' [data-fc-selector="Должник.Юр_лицо.Наименование"]').focus(); 
					break;
				case 'Физ_лицо': 
					root_item.removeClass('organization marriage_couple').addClass('citizen'); 
					$(sel + ' [data-fc-selector="Должник.Физ_лицо.Фамилия"]').focus(); 
					break;
				case 'Супруги':
					root_item.removeClass('citizen organization').addClass('marriage_couple');
					break;
			}
			this.DispatchRequestUpstairs({Должник: { Тип: type }});
		}

		controller.SelectConsentIfExist = function (datum) 
		{
			var sel = this.fastening.selector;
			if(datum.Request && datum.Request && datum.Request.Согласия_АУ &&
				datum.Request.Согласия_АУ.Согласия && datum.Request.Согласия_АУ.Согласия.length !== 0) {
				$(sel + ' [data-fc-selector="Согласия"] [data-array-index="0"] [data-fc-selector="В_ответе"]').click();
				$(sel + ' [data-fc-selector="Запрос.Время.опрошен_ау"]').click();
			}
				
		}

		controller.OnChangeDataInput = function (checkbox, data) {
			var sel = this.fastening.selector;
			if (data) {
				$(sel + ' [data-fc-selector="' + checkbox + '"]').attr('checked', 'checked');
			} else {
				$(sel + ' [data-fc-selector="' + checkbox + '"]').attr('checked', null);
			}
		}

		controller.OnCheckCheckbox = function (dataInput, isChecked) {
			var sel = this.fastening.selector;
			if (isChecked) {
				$(sel + ' [data-fc-selector="' + dataInput + '"]').val(h_times.stringifyDateUTC('r', h_times.safeDateTime()))
			} else {
				$(sel + ' [data-fc-selector="' + dataInput + '"]').val('')
			}
		}

		var trim = function (txt) {
			return txt.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
		}

		controller.OnOpen = function () {
			var sel = this.fastening.selector;
			var case_number = $(sel + ' input[data-fc-selector="Номер_судебного_дела"]').val();
			if ('' != trim(case_number)) {
				window.open('https://kad.arbitr.ru/Card?number=' + case_number);
			}
			else {
				h_msgbox.ShowModal({
					title: 'Отказ открыть страницу'
					, html: '<p>Чтобы открыть страницу дела на kad.arbitr.ru,</p><p style="text-align:right"> нужно указать номер дела..</p>'
					, width: 400, id_div: 'cpw-msg-can-not-open-kad-arbitr'
				});
			}
		}

		controller.nameFormatter= function(applicant) {
			switch(applicant.Тип) {
				case 'Юр_лицо':
					return applicant.Юр_лицо.Наименование
				case 'Физ_лицо':
					var name= applicant.Физ_лицо.Фамилия + ' '
					if(applicant.Физ_лицо.Имя) { name+= applicant.Физ_лицо.Имя[0] + '.' }
					if(applicant.Физ_лицо.Отчество) { name+= applicant.Физ_лицо.Отчество[0]+'.' }
					return name
			}
		}

		controller.Validate = function () {
			var request = this.GetFormContent();
			var vr = [];
			var block = function (msg) { vr.push({ check_constraint_result: false, description: msg }); }	
			switch (request.Должник.Тип) {
				case 'Юр_лицо':
					if ('' == trim(request.Должник.Юр_лицо.Наименование))
						block('Необходимо указать наименование должника!');
					break;
				case 'Физ_лицо':
					if ('' == trim(request.Должник.Физ_лицо.Фамилия)) {
						block('Необходимо указать фамилию должника!');
					} else if(h_constraints.recommend_Ciryllic().check(request.Должник.Физ_лицо.Фамилия)==='recommended') {
						block(h_constraints.recommend_Ciryllic().description(null, 'Фамилия'));
					}
					if(request.Должник.Физ_лицо.Имя && h_constraints.recommend_Ciryllic().check(request.Должник.Физ_лицо.Имя)==='recommended')
						block(h_constraints.recommend_Ciryllic().description(null, 'Имя'));
					if(request.Должник.Физ_лицо.Отчество && h_constraints.recommend_Ciryllic().check(request.Должник.Физ_лицо.Отчество)==='recommended')
						block(h_constraints.recommend_Ciryllic().description(null, 'Отчество'));
					if(request.Должник.Физ_лицо.СНИЛС && !h_constraints.Snils().check(request.Должник.Физ_лицо.СНИЛС))
						block(h_constraints.Snils().description(null, 'СНИЛС'));
					break;
				case 'Супруги':
					var cc_married_couple = this.fastening.get_fc_controller('Должник.Супруги');
					if(cc_married_couple.fastening) {
						var married_couple_result = cc_married_couple.Validate();
						vr = married_couple_result ? vr.concat(married_couple_result) : vr ;
					}
					break;
			}
			if(request.Должник.ИНН && !h_constraints.Inn().check(request.Должник.ИНН))
				block(h_constraints.Inn().description(null, 'ИНН'));
			if(request.Должник.ОГРН && !h_constraints.Ogrn().check(request.Должник.ОГРН))
				block(h_constraints.Ogrn().description(null, 'ОГРН'));
			if(!request.Запрос.Время.получения)
				block('Необходимо указать дату получения запроса!');
			if (!request.Заявление_на_банкротство.В_суд || (request.Заявление_на_банкротство.В_суд &&  !request.Заявление_на_банкротство.В_суд.id))
				block('Необходимо выбрать суд, в который было подано заявление о банкротстве!');
			var t = request? request.Заявление_на_банкротство.Дата_рассмотрения: '';
			if ('' != t && !(/^(0[1-9]|1\d|2\d|3[01])\.(0[1-9]|1[0-2])\.(19|20)\d{2} (20|21|22|23|[0-1]?\d{1}):([0-5]?\d{1})$/.test(t)))
				block('Время рассмотрения заявления должно быть записано в формате дд.мм.гггг чч:мм!');
			return 0 == vr.length ? null : vr;
		}

		controller.OnSelectCourtByCaseNumber = function ()
		{
			if (options_arg && options_arg.readonly)
			{
				h_msgbox.ShowModal({
					title:'Отказ искать суд по номеру',width:400, id_div:'cpw-msg-can-not-find-court-by-number-ro'
					,html:'Функция поиска суда по номеру дела недоступна в режиме "только для чтения"..'
				});
			}
			else
			{
				var sel= this.fastening.selector;
				var number= $(sel + ' [data-fc-selector="Номер_судебного_дела"]').val();
				if ('' == number)
				{
					h_msgbox.ShowModal({
						title:'Отказ искать суд по номеру',width:400, id_div:'cpw-msg-can-not-find-court-by-empty-number'
						,html:'Укажите непустой номер дела, чтобы найти по нему суд..'
					});
				}
				else
				{
					var ajaxurl= base_url + '?action=court.by-prefix&number=' + encodeURI(number);
					var v_ajax = h_msgbox.ShowAjaxRequest("Запрос суда по номеру дела", ajaxurl);
					v_ajax.ajax
					({
							dataType: "json", type: 'GET'
						, success: function (data, textStatus)
						{
							if (null != data)
							{
								$(sel + ' [data-fc-selector="Заявление_на_банкротство.В_суд"]').select2('data',data);
							}
							else
							{
								h_msgbox.ShowModal({
									title:'Отказ искать суд по номеру',width:600, id_div:'cpw-msg-can-not-find-court-by-number'
									,html:'Не удалось определить суд по номеру дела <b>' + number + '</b>'
								});
							}
						}
					});
				}
			}
		}

		controller.PrepareForm = function () {
			var sel = this.fastening.selector;
			
			if(!this.model) {
				$(sel + ' [data-fc-selector="Должник.Физ_лицо.Фамилия"]').focus();
				$(sel + ' [data-fc-selector="Запрос.Время.получен"]').attr('checked', true);
				this.OnCheckCheckbox('Запрос.Время.получения', true)
			} else if(this.model.Должник && (!this.model.Должник.Физ_лицо || !this.model.Должник.Юр_лицо)) {
				$(sel + ' [data-fc-selector="Запрос.Время.получен"]').attr('checked', true);
				this.OnCheckCheckbox('Запрос.Время.получения', true)
			}

			$(sel).tooltip();
			$(sel + ' input[data-fc-selector="Заявление_на_банкротство.Дата_рассмотрения"]').inputmask("99.99.9999 99:99");

			this.updateFields();
		}

		controller.updateFields = function () {
			var sel = this.fastening.selector;
			if(this.model) {
				if(this.model.Запрос) {
					if(this.model.Запрос.Время && !this.model.Запрос.Время.получения) {
						$(sel + ' [data-fc-selector="Запрос.Время.получен"]').attr('checked', true);
						this.OnCheckCheckbox('Запрос.Время.получения', true)
					}
					this.OnChangeDecision(this.model.Запрос);
				}
			}
		}

		controller.PrepareToBaseRender = function () {
			var self = this;
			var fspec= this.options.field_spec;

			fspec['Должник.Физ_лицо.Фамилия'] = this.search_consent.citizenSearch();
			fspec['Должник.Юр_лицо.Наименование'] = this.search_consent.organizationSearch();
			fspec['Должник.Супруги'].controller = function() { return c_married_couple({
				DispatchRequestUpstairs: self.DispatchRequestUpstairs
				, SelectConsentIfExist: self.SelectConsentIfExist.bind(self)
				, withTypeahead: true
				, base_url: base_url
			}); };

			fspec.Заявитель.controller = function() { return c_applicant({
				GetCurrentType: self.GetCurrentType.bind(self)
				, GetMarriedCouple: self.GetMarriedCouple.bind(self)
			}) }
			fspec.Заявитель.text = function (data)
			{
				if(data.Заявитель_должник) {
					return (self.GetCurrentType() === 'Супруги' ? h_applicant.Text.plural : h_applicant.Text.singular) + ' (кликните, чтобы поменять)'
				}
				return self.nameFormatter(data);
			}

			fspec.Запрос.text = function (data){
				self.OnChangeDecision(data)
				return '(кликните, чтобы поменять)';
			}

			this.OnDownloadDocument = h_request_docs.OnDownloadDocument.bind(this);
			this.DownloadResponce = options_arg && options_arg.DownloadResponce || h_request_docs.DownloadResponce.bind(this);
			this.DownloadProtocol = options_arg && options_arg.DownloadProtocol || h_request_docs.DownloadProtocol.bind(this);
			this.DownloadDischarge = options_arg && options_arg.DownloadDischarge || h_request_docs.DownloadDischarge.bind(this);
			this.DownloadConsent = options_arg && options_arg.DownloadConsent || h_request_docs.DownloadDischarge.bind(this);
		}

		controller.OnChangeDecision = function (decision) {
			var sel = this.fastening.selector;
			if(decision.Ссылка && decision.Ссылка !== '') {
				$(sel + ' .open-decision').attr("href", decision.Ссылка);
				$(sel + ' .open-decision').attr("target","_blank");
				$(sel + ' .open-decision').html('судебным актом от ' +decision.Время.акта);
				$(sel + ' .no-decision').hide();
				$(sel + ' .open-decision').show();
			} else {
				$(sel + ' .open-decision').attr("href", "#");
				$(sel + ' .open-decision').attr("target","_self");
				$(sel + ' .open-decision').html('');
				$(sel + ' .open-decision').hide();
				$(sel + ' .no-decision').show();
			}
			$(sel + ' .decision').html('(кликните, чтобы поменять)');
		}

		controller.GetCurrentType = function() {
			var sel = this.fastening.selector;
			return $(sel + ' [data-fc-selector="Должник.Тип"]').val();
		}

		controller.GetMarriedCouple = function() {
			var model = this.GetFormContent();
			return model.Должник
		}

		controller.SetSingularApplicant = function() {
			var sel = this.fastening.selector;
			if(!this.model || !this.model.Заявитель || this.model.Заявитель.Заявитель_должник) {
				$(sel + ' .applicant').html(h_applicant.Text.singular + ' (кликните, чтобы поменять)')
				$(sel + ' .applicant-is-debtor').html(h_applicant.Text.singular)
			}
		}

		controller.SetPluralApplicant = function() {
			var sel = this.fastening.selector;
			if(!this.model || !this.model.Заявитель || this.model.Заявитель.Заявитель_должник) {
				$(sel + ' .applicant').html(h_applicant.Text.plural + ' (кликните, чтобы поменять)')
				$(sel + ' .applicant-is-debtor').html(h_applicant.Text.plural)
			}
		}

		return controller;
	}
});