define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/request/e_request_wrapper.html'
	, 'forms/ama/datamart/sro/request/c_request'
], function(c_fastened, tpl
	, c_request
) {
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var options = {
			base_url: base_url
			, field_spec:
				{
					Форма_запроса: { controller: c_request }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render= controller.Render;

		controller.Render = function (sel)
		{
			this.PrepareToBaseRender();
			base_Render.call(this,sel);
			this.PrepareForm();
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function () {
			var request = base_GetFormContent.call(this);
			return request;
		}

		controller.DispatchTypeToRequest = function (type) {
			var fastening = this.fastening;
			var cc_request = fastening.get_fc_controller('Форма_запроса');
			if(cc_request.fastening) {
				this.SetRequestForm();
				cc_request.SelectType(type);
			}
		}

		controller.DispatchRequestUpstairs = function (request) {
			this.SetRequestForm(request);
		}

		controller.SetRequestForm = function (request) {
			var fastening = this.fastening;
			var cc_request = fastening.get_fc_controller('Форма_запроса');
			if(cc_request.fastening) {
				cc_request.SetFormContent(request);
				cc_request.Destroy();
				cc_request.Edit(cc_request.fastening.selector);
			}
		}

		controller.SetSearchForm = function (debtor) {
			var model = '';
			switch (debtor.Тип) {
				case 'Юр_лицо':
					model = {
						Тип: 'Юр_лицо',
						Юр_лицо: debtor.Юр_лицо.Наименование
					};
					break;
				case 'Физ_лицо':
					model = {
						Тип: 'Физ_лицо',
						Физ_лицо: debtor.Физ_лицо.Фамилия
					};
			}
		}

		controller.PrepareToBaseRender = function () {
			var self = this;
			var fspec= this.options.field_spec;

			fspec.Форма_запроса.controller = function() { return c_request({
				base_url: base_url
				, id_SRO: (options_arg && options_arg.id_SRO) && options_arg.id_SRO
				, DispatchRequestUpstairs: self.DispatchRequestUpstairs.bind(self)
				, DownloadResponce: self.DownloadResponce
				, DownloadProtocol: self.DownloadProtocol
				, DownloadDischarge: self.DownloadDischarge
				, DownloadConsent: self.DownloadConsent
			})}
		}

		controller.PrepareForm = function () {
			if(this.model) this.SetRequestForm(this.model)
		}

		controller.Validate = function () {
			var vr = [];

			var fastening = this.fastening;
			var cc_request = fastening.get_fc_controller('Форма_запроса');
			if(cc_request.fastening)
				vr = cc_request.Validate();

			return (!vr || 0 == vr.length) ? null : vr;
		}

		return controller;
	}
});