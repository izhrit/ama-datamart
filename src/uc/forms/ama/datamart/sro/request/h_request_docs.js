define([
	'tpl!forms/ama/datamart/sro/request/v_dm_request_docs_menu.html'
	, 'forms/base/h_validation_msg'
	, 'forms/base/h_msgbox'
], function(v_dm_request_docs_menu, h_validation_msg, h_msgbox) {
	return {
		docsMenu: {
			menuSel: 'div.cpw-ama-datamart-request-prepare-docs',
			el: null,
			open: function (cc_request) {
				var self = this;
				var sel = cc_request.fastening.selector;
				var btn = $(sel + ' .prepare-docs-btn');
				var btnPlaceTop = $(btn).offset().top + 39 + "px";
				var btnPlaceLeft = $(btn).offset().left + "px";
				if(!this.el) {
					self.el = document.createElement('div');
					self.el.innerHTML = v_dm_request_docs_menu();
					self.el.style.top = btnPlaceTop;
					self.el.style.left = btnPlaceLeft;
					self.el.style.position = 'absolute';
					self.el.style.zIndex = 103;
					$('body.cpw-ama').append(self.el)
					self.initDocsPrepareClicks(cc_request);
					$(sel + ' .prepare-docs-btn').mouseout(function(e) {
						var target = $(e.relatedTarget)
						if(!target.hasClass('prepare-doc') && !target.hasClass('cpw-ama-datamart-request-prepare-docs')) {
							self.el.style.display = 'none';
						}		
					})
					$(self.menuSel).mouseout(function(e) {
						var target = $(e.relatedTarget)
						if(!target.hasClass('prepare-doc') && !target.hasClass('cpw-ama-datamart-request-prepare-docs')) {
							self.el.style.display = 'none';
						}		
					})
					$(self.menuSel).click(function(e) {
						self.el.style.display = 'none';
					})
					$(self.menuSel).tooltip({
						position: {
						my: "right top",
						at: "left-5 top-5",
						collision: "none"
						}
					});
				}else {
					self.el.style.display = 'block';
					self.el.style.top = btnPlaceTop;
					self.el.style.left = btnPlaceLeft;
				}
			},
			initDocsPrepareClicks: function (cc_request) {
				$(this.menuSel + ' .prepare-responce-doc').click(function() {
					cc_request.OnDownloadDocument('responce');
				})
	
				$(this.menuSel + ' .prepare-protocol-doc').click(function() {
					cc_request.OnDownloadDocument('protocol');
				})

				$(this.menuSel + ' .prepare-consent-doc').click(function () {
					cc_request.OnDownloadDocument('consent');
				})

				$(this.menuSel + ' .prepare-discharge-doc').click(function() {
					cc_request.OnDownloadDocument('discharge');
				})
			},
			destroy: function() {
				if(this.el) {
					this.el.parentNode.removeChild(this.el);
					this.el = null;
				}
			}
		},

		OnDownloadDocument: function (docType) {
			var self = this;
			var model = this.GetFormContent();
			h_validation_msg.IfOkWithValidateResult(this.Validate(), function () {
				switch(docType) {
					case 'responce':
						self.DownloadResponce(function (data) {
							model.id_MRequest = data.id_MRequest
						});
						break;
					case 'protocol':
						self.DownloadProtocol(function (data) {
							model.id_MRequest = data.id_MRequest
						});
						break;
					case 'discharge':
						self.DownloadDischarge(function (data) {
							model.id_MRequest = data.id_MRequest
						});
						break;
					case 'consent':
						self.DownloadConsent(function (data) {
							model.id_MRequest = data.id_MRequest
						});
						break;
				}
				self.SetFormContent(model);
			})
		},

		DownloadResponce: function(on_DownloadResponce) {
			h_msgbox.ShowModal({
				title:'Документ подготовлен'
				,html:'Документ-ответ успешно сформирован и загружен'
				,width:500, id_div:'cpw-msg-success-download-responce'
			});
			on_DownloadResponce({id_MRequest: 1});
		},
		
		DownloadProtocol: function(on_DownloadProtocol) {
			h_msgbox.ShowModal({
				title:'Документ подготовлен'
				,html:'Документ-протокол успешно сформирован и скачан'
				,width:500, id_div:'cpw-msg-success-download-protocol'
			});
			on_DownloadProtocol({id_MRequest: 1});
		},
	
		DownloadDischarge: function(on_DownloadDischarge) {
			h_msgbox.ShowModal({
				title:'Документ подготовлен'
				,html:'Выписка успешно сформирована и скачана'
				,width:500, id_div:'cpw-msg-success-download-discharge'
			});
			on_DownloadDischarge({id_MRequest: 1});
		},

		DownloadConsent:  function (on_DownloadConsent) {
			h_msgbox.ShowModal({
				title: 'Документ подготовлен'
				, html: 'Документ-согласие успешно сформирован и скачан'
				, width: 500, id_div: 'cpw-msg-success-download-consent'
			});
			on_DownloadConsent({ id_MRequest: 1 });
		}
	}
});