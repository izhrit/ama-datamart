﻿define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/manager_document/c_sro_manager_document'
	, 'forms/ama/datamart/base/h_document_types'
],
	function (h_msgbox, c_sro_manager_document, h_document_types)
{
	return {
		ModalEditManagerDocuments: function (base_url, manager)
		{
			if (!manager.id_Manager || manager.id_Manager == '')
			{
				h_msgbox.ShowModal
					({ 
						title: 'Работа с документами недоступна!', buttons: ['Закрыть'], width: 410
						,html: 'Для того, чтобы начать работу с документами АУ, необходимо выслать ему пароль доступа!'
					});
			}
			else
			{
				var ajaxurl = base_url + '?action=manager.document.ru&cmd=get&id=' + manager.id_Manager;
				var v_ajax = h_msgbox.ShowAjaxRequest("Запрос информации об документах АУ с сервера", ajaxurl);
				var self = this;
				v_ajax.ajax({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							self.EditDocument(base_url, manager, data);
						}
					}
				});
			}
		}

		, EditDocument: function (base_url, manager, documents)
		{
			var c_manager_document = c_sro_manager_document({ base_url: base_url, id_Manager: manager.id_Manager });

			documents.forEach(function (item) { 
				var dt= item.DocumentType;
				item.DocumentType = h_document_types[dt];
				if (!item.DocumentType)
					item.DocumentType= 'Документ неизвестного типа "' + dt + '"';
			});

			c_manager_document.SetFormContent(documents);

			h_msgbox.ShowModal
			({
				title: 'Документы АУ ' + manager.lastName + ' ' + manager.firstName + ' ' + manager.middleName
				, controller: c_manager_document
				, buttons: ['Закрыть']
				, id_div: "cpw-form-ama-datamart-manager-documents"
			});
		}
	};
});