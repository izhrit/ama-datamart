﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/manager_document/e_sro_manager_document.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/manager_document/add/c_sro_manager_document_add'
	, 'forms/base/h_validation_msg'
	, 'forms/ama/datamart/base/h_document_types'
],
function (c_fastened, tpl, h_msgbox, c_sro_manager_document_add, h_validation_msg, h_document_types)
{
	return function (options_arg)
	{
		

		var controller = c_fastened(tpl);

		controller.size = { width: 680, height: 440 };
		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.id_Manager = ((options_arg && options_arg.id_Manager) ? options_arg.id_Manager : '');
		controller.base_crud_url = controller.base_url + '?action=manager.document.ru';
		controller.base_dwnld_url = controller.base_url + '?action=manager.document.dwnld';

		var base_Render = controller.Render;

		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			var self = this;
			$(sel + ' button.delete').click(function (e) { self.OnDelete(e) });
			$(sel + ' button.add').click(function () { self.OnAdd(); });
			$(sel + ' div.documents').click(function (e) { self.OnDocument(e); });
		}

		controller.OnAdd = function () {
			var self = this;
			var sel = this.fastening.selector;
			var fc_dom_item = $(this.fastening.selector);
			var model = this.fastening.get_fc_model_value(fc_dom_item);
			var с_document_add = c_sro_manager_document_add();
			var btnOk = 'Сохранить документ';
			h_msgbox.ShowModal
				({
					title: 'Добавление документа АУ'
					, controller: с_document_add
					, buttons: [btnOk, 'Отмена']
					, id_div: "cpw-ama-datamart-sro-manager-document-add"
					, onclose: function (btn, dlg_div) {
						if (btn == btnOk) {
							h_validation_msg.IfOkWithValidateResult(с_document_add.Validate(), function () {
								//готовим модель в которую будем добавлять новую строку с файлом
								var document = с_document_add.GetFormContent();
								// Переназначаем на тестовый документ в случае если тест
								if (window.setDocumentTestFile) {
									document.File = new Blob(['test document'], {
										type: 'text/plain'
									});
									document.FileName = 'test.txt';
								}
								var on_done = function (data) //функция добавления строки
								{
									document.id_ManagerDocument = data.id;
									document.DocumentType = h_document_types[document.DocumentType];
									//добавляем название файла и id файла в основную форму
									model.push(document);
									self.fastening.set_fc_model_value(fc_dom_item, model);
									//Обновляем события
									$(sel + ' button.delete').click(function (e) { self.OnDelete(e) });
									$(sel + ' div.document').click(function (e) { self.OnDocument(e); });
									с_document_add.Destroy();
									$(dlg_div).dialog("close");
								}
								if (!document.File) {
									on_done({ id_file: '' });
								}
								else {
									//берем файл из формы и готовим к отправке
									var File = new FormData();
									File.append('File', document.File);
									//отправляем файл через ajax и получаем id и тип файла
									var ajaxurl = self.base_crud_url + '&cmd=add&type=' + document.DocumentType + '&id_Manager=' + self.id_Manager;
									var v_ajax = h_msgbox.ShowAjaxRequest("Передача файла на сервер", ajaxurl);
									v_ajax.ajax({
										data: File,
										dataType: 'json',
										contentType: false,
										processData: false,
										type: 'POST',
										success: function (data) {
											if (data.id) {
												on_done(data);
											} else {
												h_msgbox.ShowModal({
													title: 'Произошла ошибка при загрузке документа'
													, width: 500, id_div: 'cpw-msg-can-not-upload'
												});
											}
										}
									});
								}
							});
							return false;
						}
					}
				});
		}

		controller.OnDelete = function (e) {
			e.stopPropagation();
			var i_item = parseInt($(e.target).parents('[data-fc-type="array-item"]').attr('data-array-index'));
			
			var self = this;
			var fc_dom_item = $(this.fastening.selector);
			var model = this.fastening.get_fc_model_value(fc_dom_item);
			var item = model[i_item];
			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal
				({
					title: 'Подтверждение удаления документа №' + (i_item + 1)
					, html: 'Вы действительно хотите удалить документ <br/>'
						+ '№' + (i_item + 1) + ' <span style="font-weight: 600;">' + item.DocumentType + '</span>?'
					, width: 450, height: 180
					, buttons: [btnOk, 'Нет, не удалять']
					, onclose: function (btn) {
						if (btn == btnOk) {
							self.RemoveDocument(item.id_ManagerDocument, i_item, fc_dom_item, model)
						}
					}
				});
		}

		controller.RemoveDocument = function (id, i_item, fc_dom_item, model) {
			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=delete&id=' + id;
			var v_ajax = h_msgbox.ShowAjaxRequest("Удаление договора с сервера", ajaxurl);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus) {
						if (null == data) {
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else {
							if (!data.ok) {
								h_msgbox.ShowModal({
									title: 'Отказ выполнять удаление договора'
									, html: '<p>' + data.message + '</p>'
									, width: 500, id_div: 'cpw-msg-can-not-delete-start'
								});
							} else {
								model.splice(i_item, 1);
								self.fastening.set_fc_model_value(fc_dom_item, model);
							}
						}
					}
				});
		}

		controller.OnDocument = function (e)
		{
			var i_item = $(e.target).attr("data-array-index")
			if (!$(e.target).attr("data-array-index")) {
				var i_item = $(e.target).parents('[data-fc-type="array-item"]').attr('data-array-index');
			}

			var self = this;
			var fc_dom_item = $(this.fastening.selector);
			var model = this.fastening.get_fc_model_value(fc_dom_item);
			var item = model[i_item];

			window.open(self.base_dwnld_url + '&id_ManagerDocument=' + item.id_ManagerDocument + '&FileName=' + encodeURI(item.FileName));
		}

		return controller;
	}
});