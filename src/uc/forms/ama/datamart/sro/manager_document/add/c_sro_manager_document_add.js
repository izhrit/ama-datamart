﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/sro/manager_document/add/e_sro_manager_document_add.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/base/h_document_types'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_msgbox, h_document_types, h_validation_msg)
{
	return function (options_arg)
	{
		
		var controller = c_fastened(tpl);

		controller.size = { width: 680, height: 265 };
		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;

		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			var self = this;
			$(sel + ' #document-file').change(function () { var files = this.files; self.isAdd(files); });
			
		}
		controller.isAdd = function (files) {

			var sel = this.fastening.selector;
			var self = this;
			//проверка размера файла
			var maxFileSize = 52428800000;
			var file = $(sel + ' #document-file');
			var flag = false;
			if (file.prop('files').length) {
				if (file.prop('files')[0].size <= maxFileSize) {
					flag = true;
				}
			}
			//создание модели и селектора
			this.model = this.GetFormContent();
			var sel = this.fastening.selector;
			//сохранение файла в модель
			if (flag) self.model.File = file.prop('files')[0];
			else self.model.File = '';
			//заполнение наименования названием файла
			$(sel + ' input[data-fc-selector="FileName"]').val(files[0].name);
			this.model.FileName = files[0].name;
			this.SetFormContent(this.model);
		}

		controller.Validate = function () {
			var res = null;
			var model = this.GetFormContent();
			if (!model.FileName || '' == model.FileName)
				res = h_validation_msg.error(res, "Наименование файла не может быть пустым");
			if (!model.DocumentType || '' == model.DocumentType)
				res = h_validation_msg.error(res, "Тип документа не может быть пустым");
			return res;
		}

		return controller;
	}
});