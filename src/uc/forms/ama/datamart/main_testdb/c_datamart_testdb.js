define([
	  'forms/ama/datamart/main/c_datamart'
	, 'forms/base/h_msgbox'
],
function (c_datamart, h_msgbox)
{
	return function()
	{
		var controller = c_datamart();

		controller.GetFormContent= function()
		{
			var result = null;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос на получение всей базы", 'ama/datamart_testdb?cmd=read');
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					result = data;
				}
			});
			var start = new Date().getTime();
			while (null == result)
			{
				var now = new Date().getTime();
				if (now - start > 10000)
				{
					break;
				}
			}
			return result;
		}

		controller.SetFormContent = function (data)
		{
		}

		return controller;
	}
});
