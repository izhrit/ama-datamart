﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/vote/main/e_committee_vote.html'
	, 'forms/ama/cabinetcc/documents/c_com_cab_documents'
	, 'forms/ama/cabinetcc/log/c_com_cab_log'
	, 'forms/ama/datamart/manager/committee/vote/questions/c_com_vote_questions'
],
function (c_fastened, tpl, c_com_cab_documents, c_com_cab_log, c_com_vote_questions)
{
	return function (options_arg)
	{
		var options = {
			field_spec:
				{
					Документы: { controller: function () { return c_com_cab_documents(options_arg); } }
					, Журнал: { controller: c_com_cab_log }
					, Вопросы: { controller: c_com_vote_questions }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.size = {width:900,height:500};

		return controller;
	}
});