﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/vote/questions/e_com_vote_questions.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);
		return controller;
	}
});