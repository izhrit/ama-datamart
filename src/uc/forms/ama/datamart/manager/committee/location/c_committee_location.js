﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/location/e_committee_location.html'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_validation_msg)
{
	return function (options)
	{
		var controller = c_fastened(tpl,options);

		controller.size = { width: 630, height: 645 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			var self = this;
			$(sel + ' input').change(function (e) { $(sel + ' span').focus(); });
			$(sel + ' input[data-fc-selector="Время_заседания"]').change(function () { self.FixTime("Время_заседания") });
			$(sel + ' input[data-fc-selector="Ознакомление.С_материалами.Время.Начало"]').change(function () { self.FixTime("Ознакомление.С_материалами.Время.Начало") });
			$(sel + ' input[data-fc-selector="Ознакомление.С_материалами.Время.Конец"]').change(function () { self.FixTime("Ознакомление.С_материалами.Время.Конец") });
			$(sel + ' input[data-fc-selector="Ознакомление.С_результатами.Время.Начало"]').change(function () { self.FixTime("Ознакомление.С_результатами.Время.Начало") });
			$(sel + ' input[data-fc-selector="Ознакомление.С_результатами.Время.Конец"]').change(function () { self.FixTime("Ознакомление.С_результатами.Время.Конец") });
		}

		controller.FixTime = function (selector) {
			var sel = this.fastening.selector;
			var regexp = /^(\d{1})\:(\d{2})$/;
			var val = $(sel + ' input[data-fc-selector="' + selector + '"]').val();
			if (regexp.test(val))
				$(sel + ' input[data-fc-selector="' + selector + '"]').val('0'+val);
		}

		var check_date_dd_mm_yyyyy= function(str)
		{
			var regexp = /^(\d{2})\.(\d{2})\.(\d{4})$/;
			return regexp.test(str);
		}

		var check_time_hh_mm = function (str)
		{
			var regexp = /^(\d{1,2})\:(\d{2})$/;
			return regexp.test(str);
		}

		controller.Validate = function ()
		{
			var res = null;
			var model = this.GetFormContent();

			if (!check_date_dd_mm_yyyyy(model.Дата_заседания))
				res = h_validation_msg.error(res, "Дата заседания (окончания приёма бюллетеней) должна быть указана в форме ДД.ММ.ГГГГ");
			if (!check_time_hh_mm(model.Время_заседания))
				res = h_validation_msg.error(res, "Время заседания (окончания приёма бюллетеней) должно быть указано в форме ЧЧ:ММ");

			if (!check_date_dd_mm_yyyyy(model.Ознакомление.С_материалами.С_даты))
				res = h_validation_msg.error(res, "Дата, с которой можно знакомиться с материалами должна быть указана в форме ДД.ММ.ГГГГ");
			if (!check_time_hh_mm(model.Ознакомление.С_материалами.Время.Начало))
				res = h_validation_msg.error(res, "Время, с которого можно знакомиться с материалами должно быть указано в форме ЧЧ:ММ");
			if (!check_time_hh_mm(model.Ознакомление.С_материалами.Время.Конец))
				res = h_validation_msg.error(res, "Время, до которого можно знакомиться с материалами должно быть указано в форме ЧЧ:ММ");
			if ('' == h_validation_msg.trim(model.Ознакомление.С_материалами.Адрес))
				res = h_validation_msg.error(res, "Адрес, по которому можно ознакомиться с материалами не может быть пустым");

			if (!check_date_dd_mm_yyyyy(model.Ознакомление.С_результатами.До_даты))
				res = h_validation_msg.error(res, "Дата, до которой можно знакомиться с результатами должна быть указана в форме ДД.ММ.ГГГГ");
			if (!check_time_hh_mm(model.Ознакомление.С_результатами.Время.Начало))
				res = h_validation_msg.error(res, "Время, с которого можно знакомиться с результатами должно быть указано в форме ЧЧ:ММ");
			if (!check_time_hh_mm(model.Ознакомление.С_результатами.Время.Конец))
				res = h_validation_msg.error(res, "Время, до которого можно знакомиться с результатами должно быть указано в форме ЧЧ:ММ");
			if ('' == h_validation_msg.trim(model.Ознакомление.С_материалами.Адрес))
				res = h_validation_msg.error(res, "Адрес, по которому можно ознакомиться с результатами не может быть пустым");

			if ('' == h_validation_msg.trim(model.Должник.Местонахождение))
				res = h_validation_msg.error(res, "Адрес должника не может быть пустым");
			if ('' == h_validation_msg.trim(model.Телефон_АУ))
				res = h_validation_msg.error(res, "Номер телефона АУ не может быть пустым");

			var sel = this.fastening.selector;
			var checked = $(sel + ' #cpw-electro-committee-offer').attr('checked');
			if ('checked' != checked)
				res = h_validation_msg.error(res, "Вы должны дать своё согласие с условиями публичной оферты");

			return res;
		}

		return controller;
	}
});