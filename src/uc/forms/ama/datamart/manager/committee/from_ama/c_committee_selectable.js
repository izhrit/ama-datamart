define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/from_ama/e_committee_selectable.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/manager/committee/main/c_committee_main'
	, 'forms/ama/datamart/base/h_procedure_types'
],
function (c_fastened, tpl, h_msgbox, c_committee_main, h_procedure_types)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_crud = base_url + '?action=meeting.crud'
		var support_html='<a href="mailto:support@russianit.ru">support@russianit.ru</a><br>+7(3412) 57 04 02<br>+7(499) 673 03 37<br>пн.-пт. 07.00-19.00(мск)<br>сб.-вс. 10.00-14.00(мск)';
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' div.selected-meeting').on('model_change', function () { self.OnMeetingChange(); });
			$(sel + ' button.delete-meeting').click(function () { self.OnDeleteMeeting(); });

			if (app && app.id_Meeting)
				self.LoadMeeting(app.id_Meeting);
			else h_msgbox.ShowModal({ title: 'Не удалось загрузить Комитет Кредиторов', width:550, buttons:[], html: 'Невозможно открыть Комитет Кредиторов, так как возникли проблемы при его создании. Попробуйте создать Комитет Кредиторов снова или обратитесь в техническую поддержку:<br>'+support_html });
		}

		controller.LoadMeeting= function(id_Meeting)
		{
			var sel = this.fastening.selector;
			var self = this;
			var ajaxurl = base_url_crud + '&cmd=get&id=' + id_Meeting;
			var v_ajax= h_msgbox.ShowAjaxRequest("Загрузка данных о заседании с сервера", ajaxurl);
			v_ajax.on_error=function()
			{
				var self = v_ajax;
				return function(data, textStatus)
				{
					if(data.status==404)
					{
						self.stop();
						h_msgbox.ShowModal({title:'Не удалось загрузить Комитет Кредиторов', width:550, buttons:[],html:'Не обнаружено выбранное собрание, возможно оно было удалено вами на Витрине данных ПАУ'});
					}
					else if(data.status==401)
					{
						self.stop();
						h_msgbox.ShowModal({title:'Не удалось авторизоваться на Витрине данных', width:550, buttons:[],html:'Не удалось авторизоваться, попробуйте перезапустить программу, в случае повторения ошибки обратитесь в техническую поддержку:<br>'+support_html});											
					}
					else
						self.ShowAjaxError(data, textStatus);
				}
			}
			v_ajax.ajax({
				  dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (meeting_info, textStatus)
				{
					self.ShowMeeting(id_Meeting, meeting_info);
				}
			});
		}

		controller.SafeUnshowMeeting= function()
		{
			var sel = this.fastening.selector;
			if (null != this.controller)
			{
				this.controller.Destroy();
				this.controller = null;
				this.controller_id_Meeting = null;
				$(sel + ' button.delete-meeting').hide();
				$(sel + ' div.selected-meeting').html('');
			}
		}

		controller.ShowMeeting = function (id_Meeting, meeting_info)
		{
			meeting_info.id_Meeting = id_Meeting;
			var sel = this.fastening.selector;
			$(sel + ' div.no-meetings').hide();
			this.SafeUnshowMeeting();
			this.controller_id_Meeting = id_Meeting;
			this.controller = c_committee_main({ base_url: base_url });
			this.controller.SetFormContent( meeting_info );
			this.controller.Edit(sel + ' div.selected-meeting');
			$(sel + ' button.delete-meeting').show();
			this.controller.ValidateToStart = function () {
				var self = this;
				var model = this.GetFormContent();
				var vres = null;
				if (!model.Вопросы || null == model.Вопросы || 0 == model.Вопросы.length)
					vres = self.AddInvalidDescription(vres, 'Не указано ни одного вопроса повестки дня!');
				if (!model.Участники || null == model.Участники || 0 == model.Участники.length)
					vres = self.AddInvalidDescription(vres, 'Не указано ни одного участника заседания!');
				if (!model.Должник || !model.Должник.Местонахождение || null == model.Должник.Местонахождение || 0 == model.Должник.Местонахождение.length)
					vres = self.AddInvalidDescription(vres, 'Не указан адрес должника!');
				return vres;
			}
		}

		controller.OnDeleteMeeting = function ()
		{
			if (!this.controller || null == this.controller)
			{
				h_msgbox.ShowModal({
					title: 'Неуместное удаление заседания комитета кредиторов'
					, html: 'Заседание комитета кредиторов для удаления не выбрано.'
				});
			}
			else
			{
				var meeting_info = this.controller.GetFormContent();
				if (meeting_info.Голосование && null != meeting_info.Голосование && 0 != meeting_info.Голосование.length)
				{
					h_msgbox.ShowModal({
						title: 'Неуместное удаление заседания комитета кредиторов', width: 600
						, html: 'Нельзя удалить запись о заседании после уведомления участников о голосовании!'
					});
				}
				else
				{
					var self = this;
					var b_ok= 'Да, удалить запись о заседании комитета кредиторов';
					h_msgbox.ShowModal({
						title: 'Подтверждение удаление заседания комитета кредиторов', width: 600
						, id_div: 'cpw-form-ama-datamart-committee-meeting-delete-confirm-form'
						, html: 'Вы действительно хотите удалить запись о засадании комитета кредиторов?'
						, buttons: [b_ok, 'Отмена']
						, onclose: function(b)
						{
							if (b==b_ok)
								self.DeleteMeeting();
						}
					});
				}
			}
		}

		controller.DeleteMeeting = function ()
		{
			var self = this;
			var ajaxurl = base_url_crud + '&cmd=delete&id=' + this.controller_id_Meeting;
			var v_ajax = h_msgbox.ShowAjaxRequest("Удаление записи о заседании на сервере", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok)
					{
						h_msgbox.ShowModal
						({
							title: 'Успешное удаление записи о заседании комитета кредиторов', width: 600
							, id_div: 'cpw-form-ama-datamart-committee-meeting-delete-ok-form'
							, html: 'Запись о заседании комитета кредиторов удалена!'
							, onclose: function ()
							{
								self.SafeUnshowMeeting();
								self.LoadNearest();
							}
						});
					}
					else
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка удаления заседания комитета кредиторов'
							, width: 450
							, html: '<span>Не удалось удалить запись о заседании комитета кредиторов по причине:</span><br/> <b>\"'
								+ responce.reason + '\"</b>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
				}
			});
		}

		controller.OnMeetingChange= function()
		{
			var meeting_info = this.controller.GetFormContent();
			var ajaxurl = base_url_crud + '&cmd=update&id=' + this.controller_id_Meeting;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача данных о заседании на сервер", ajaxurl);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: meeting_info
				, cache: false
				, success: function (m, textStatus)
				{
				}
			});
		}

		return controller;
	}
});
