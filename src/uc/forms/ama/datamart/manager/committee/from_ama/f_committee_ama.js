﻿define(['forms/ama/datamart/manager/committee/from_ama/c_committee_from_ama'],
	function (CreateController) {
		var form_spec =
		{
			CreateController: CreateController
			, key: 'committee_ama'
			, Title: 'Комитет кредиторов'
		};
		return form_spec;
	});
