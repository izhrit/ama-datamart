include ..\..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\member\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "пуст"

shot_check_png ..\..\shots\00new.png

wait_click_text "Добавить члена комитета кредиторов"
wait_text "Телефон"
play_stored_lines ama_committee_member_fields_1
shot_check_png ..\..\shots\00new_add.png
wait_click_text "Сохранить данные участника"

wait_click_text "Добавить члена комитета кредиторов"
wait_text "Телефон"
play_stored_lines ama_committee_member_fields_1
  js wbt.SetModelFieldValue("Фамилия",	"Иванов1");
wait_click_text "Сохранить данные участника"

shot_check_png ..\..\shots\01sav_added2.png

wait_click_text "Добавить члена комитета кредиторов"
wait_text "Телефон"
play_stored_lines ama_committee_member_fields_1
  js wbt.SetModelFieldValue("Фамилия",	"Иванов2");
wait_click_text "Сохранить данные участника"

shot_check_png ..\..\shots\01sav_added3.png

js $('[data-array-index="2"] .delete').click();
wait_text "Да, удалить"
shot_check_png ..\..\shots\01sav_delete.png
wait_click_text "Да, удалить"

shot_check_png ..\..\shots\01sav_added2.png

js $('[data-array-index="1"] .edit').click();
wait_text "Телефон"
shot_check_png ..\..\shots\01sav_edit.png
play_stored_lines ama_committee_member_fields_2
wait_click_text "Сохранить данные участника"

shot_check_png ..\..\shots\01sav.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01sav.json.result.txt
exit