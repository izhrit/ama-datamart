﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/members/e_committee_members.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/manager/committee/member/c_committee_member'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, h_msgbox, c_committee_member, h_validation_msg)
{
	return function (options)
	{
		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.add').click(function () { self.OnAdd(); });
			$(sel + ' button.add').button({ icons: { primary: 'ui-icon-plus' } });

			var on_delete = function (e) { self.OnDelete(e); }
			var on_edit_button = function (e) { self.OnEdit(e); }
			var on_edit = function (e) { 
				if (!$(e.target).hasClass('delete') && !$(e.target).parent().hasClass('delete') && 
					!$(e.target).hasClass('edit') && !$(e.target).parent().hasClass('edit'))
				{
					 self.OnEdit(e);
				}
			}
			this.fastening.on_after_render= function()
			{
				$(sel + ' button.delete').unbind('click', on_delete).click(on_delete);
				$(sel + ' button.edit').unbind('click', on_edit_button).click(on_edit_button);
				$(sel + ' [data-fc-type="array-item"]').unbind('click', on_edit).click(on_edit);
			}
			this.fastening.on_after_render();
		}

		controller.SetReadOnly= function(readonly)
		{
			if (!this.options)
				this.options = {};
			this.options.readonly = readonly;
			var sel = this.fastening.selector;
			$(sel + ' button.ui-button').button(readonly ? 'disable' : 'enable');
			$(sel + ' button.edit').attr('disabled', readonly ? 'disabled' : null);
			$(sel + ' button.delete').attr('disabled', readonly ? 'disabled' : null);
		}

		controller.OnDelete= function(e)
		{
			e.stopPropagation();
			var i_item = parseInt($(e.target).parents('[data-fc-type="array-item"]').attr('data-array-index'));

			var self = this;
			var fc_dom_item = $(this.fastening.selector);
			var model = this.fastening.get_fc_model_value(fc_dom_item);
			var item = model[i_item];
			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal
			({
				title: 'Подтверждение удаления участника №' + (i_item + 1)
				, html: 'Вы действительно хотите удалить участника №' + (i_item + 1)
					+ '<br/><span style="font-weight: 500;">' + item.Фамилия + ' ' + item.Имя + ' ' + item.Отчество + '</span>?'
				, width: 450, height: 180
				, buttons: [btnOk, 'Нет, не удалять']
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						model.splice(i_item, 1);
						self.fastening.set_fc_model_value(fc_dom_item, model);
						$(self.fastening.selector).trigger('model_change');
					}
				}
			});
		}

		controller.OnEdit = function (e)
		{
			var t = $(e.target);
			if ('array-item' != t.attr('data-fc-type'))
				t = t.parents('[data-fc-type="array-item"]');
			var i_item = parseInt(t.attr('data-array-index'));

			var self = this;
			var fc_dom_item = $(this.fastening.selector);
			var model = this.fastening.get_fc_model_value(fc_dom_item);
			var item = model[i_item];

			var readonly = self.options && self.options.readonly;
			var c_member = c_committee_member({ readonly: readonly });
			if (this.debtorName)
				c_member.debtorName = this.debtorName;
			c_member.SetFormContent(item);
			if (!readonly)
			{
				this.DoEdit(i_item, c_member);
			}
			else
			{
				h_msgbox.ShowModal
				({
					title: 'Данные участника', controller: c_member, buttons: ['Закрыть']
					, id_div: "cpw-form-ama-datamart-committee-member-readonly-form"
				});
			}
		}

		controller.DoEdit = function (i_item, c_member)
		{
			var self = this;
			var btnOk = 'Сохранить данные участника';
			h_msgbox.ShowModal
			({
				title: 'Редактирование данных участника комитета кредиторов №' + (i_item + 1)
				, controller: c_member
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-committee-member-form"
				, onclose: function (btn, dlg_div)
				{
					if (btn == btnOk)
					{
						h_validation_msg.IfOkWithValidateResult(c_member.Validate(), function ()
						{
							var fc_dom_item = $(self.fastening.selector);
							var model = self.fastening.get_fc_model_value(fc_dom_item);
							model[i_item] = c_member.GetFormContent();
							self.fastening.set_fc_model_value(fc_dom_item, model);
							$(self.fastening.selector).trigger('model_change');
							c_member.Destroy();
							$(dlg_div).dialog("close");
						});
						return false;
					}
				}
			});
		}

		controller.OnAdd= function()
		{
			var c_member = c_committee_member();
			if (this.debtorName)
				c_member.debtorName = this.debtorName;
			var self = this;
			var btnOk = 'Сохранить данные участника';
			var id_div = "cpw-form-ama-datamart-committee-member-form";
			h_msgbox.ShowModal
			({
				title: 'Добавление участника комитета кредиторов'
				, controller: c_member, id_div: id_div
				, buttons: [btnOk, 'Отмена']
				, onclose: function (btn, dlg_div)
				{
					if (btn == btnOk)
					{
						h_validation_msg.IfOkWithValidateResult(c_member.Validate(), function ()
						{
							var fc_dom_item = $(self.fastening.selector);
							var model = self.fastening.get_fc_model_value(fc_dom_item);
							if (!model)
								model = [];
							model.push(c_member.GetFormContent());
							self.fastening.set_fc_model_value(fc_dom_item, model);
							self.fastening.on_after_render();
							$(self.fastening.selector).trigger('model_change');
							c_member.Destroy();
							$(dlg_div).dialog("close");
						});
						return false;
					}
				}
			});
		}

		return controller;
	}
});