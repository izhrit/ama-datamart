define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/voting/e_committee_voting.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/manager/committee/vote/main/c_committee_vote'
],
function (c_fastened, tpl, h_msgbox, c_committee_vote)
{
	return function (options_arg)
	{
		var BuildClassesForItem = function (value, index)
		{
			return (!value[index].Нет_в_списках || 'false' == value[index].Нет_в_списках) ? '' : 'excluded';
		}

		var controller = c_fastened(tpl, { BuildClassesForItem: BuildClassesForItem });

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' tr[data-fc-type="array-item"]').click(function (e) { self.OnVote(e); });
		}

		controller.OnVote = function (e)
		{
			var t = $(e.target);
			if ('array-item' != t.attr('data-fc-type'))
				t = t.parents('[data-fc-type="array-item"]');
			var i_item = parseInt(t.attr('data-array-index'));

			var self = this;
			var fc_dom_item = $(this.fastening.selector);
			var model = this.fastening.get_fc_model_value(fc_dom_item);
			var item = model[i_item];

			var ajaxurl = this.base_url + '?action=meeting.vote&id_Vote=' + item.id_Vote;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных голосующего с сервера", ajaxurl);

			v_ajax.ajax
			({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						var c_vote = c_committee_vote(options_arg);
						c_vote.SetFormContent(data);
						h_msgbox.ShowModal
						({
							title: 'Голосование участника №' + (i_item + 1)
							, controller: c_vote
							, buttons: ['Отмена']
							, id_div: "cpw-form-ama-datamart-committee-vote-form"
						});
					}
				}
			});
		}

		return controller;
	}
});
