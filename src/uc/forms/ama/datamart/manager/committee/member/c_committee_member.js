﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/member/e_committee_member.html'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, copy_copy, h_validation_msg)
{
	return function (options)
	{
		var controller = c_fastened(tpl, options);

		controller.size = { width: 450, height: 445 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' input[data-fc-selector="Фамилия"]').focus();
			$(sel + ' button.to-print').click(function () { self.OnPrintAgrement(); });
			$(sel).tooltip();
			$(sel + ' input[data-fc-selector="телефон"]').inputmask("+7(999)-999-99-99");
		}

		controller.OnPrintAgrement = function ()
		{
			var model = this.GetFormContent();
			var url = 'ui-backend.php?action=meeting.agreement';
			url += '&Фамилия=' + model.Фамилия;
			url += '&Имя=' + model.Имя;
			url += '&Отчество=' + model.Отчество;
			url += '&телефон=' + model.телефон;
			url += '&email=' + model.email;
			url += '&Должник=' + (!this.debtorName ? '_______________________________________________________________' : this.debtorName);
			app.window_open(url,'ama-committee-agreement', 'width=1000,height=700,resizable=yes,scrollbars=yes,menubar=yes,toolbar=yes');
		}

		var fix_phone_number= function(txt)
		{
			return !txt || null == txt ? '' : txt.replace(/[^0-9.]/g, "");
		}

		var check_email = function (str) {
			var regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
			return regexp.test(str);
		}

		controller.Validate= function()
		{
			var res = null;
			var model = this.GetFormContent();
			var fio = h_validation_msg.trim(model.Фамилия + model.Имя + model.Отчество);
			if ('' == h_validation_msg.trim(fio))
				res = h_validation_msg.error(res, "ФИО НЕ может быть пустым");
			if (h_validation_msg.trim(model.email).length < 5)
				res = h_validation_msg.error(res, "Адрес электронной почты НЕ может быть короче 5 символов");
			else if (-1 == model.email.indexOf('@'))
				res = h_validation_msg.error(res, "Адрес электронной почты должен содержать символ '@'");
			else if (!check_email(model.email))
				res = h_validation_msg.error(res, "Адрес электронной почты некорректный");
			var fixed_phone = fix_phone_number(model.телефон);
			if (11 != fixed_phone.length)
				res = h_validation_msg.error(res, "В номере телефона должно быть 11 цифр");
			return res;
		}

		return controller;
	}
});