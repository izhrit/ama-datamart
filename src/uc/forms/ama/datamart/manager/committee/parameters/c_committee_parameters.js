﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/parameters/e_committee_parameters.html'
	, 'forms/ama/datamart/manager/committee/members/c_committee_members'
	, 'forms/ama/datamart/manager/committee/questions/c_committee_questions'
	, 'forms/ama/datamart/manager/committee/location/c_committee_location'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
	, 'forms/base/codec/codec.copy'
],
function (c_fastened, tpl, c_committee_members, c_committee_questions, c_committee_location, h_msgbox, h_validation_msg, codec_copy)
{
	return function ()
	{
		var options = {
			field_spec:
				{
					Участники: { controller: c_committee_members }
					,Вопросы: { controller: c_committee_questions }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.size = { width: 900, height: 600 };

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			if (this.ReadOnly())
			{
				var fspec= this.options.field_spec;
				fspec.Участники.controller = function () { return c_committee_members({ readonly: true }); }
				fspec.Вопросы.controller = function () { return c_committee_questions({ readonly: true }); }
			}
			base_Render.call(this, sel);

			this.fastening.get_fc_controller('Участники').debtorName = 
				(!this.model || null == this.model || null == this.model.Должник || !this.model.Должник)
					? '' : this.model.Должник.Наименование;

			var self = this;
			$(sel + ' div.location').click(function () { self.OnLocation(); });
		}

		controller.ReadOnly= function()
		{
			return this.model && this.model.Состояние && 'Приостановлено' != this.model.Состояние && 'Планирование' != this.model.Состояние;
		}

		controller.SetReadOnly= function(readonly)
		{
			var fastening = this.fastening;
			fastening.get_fc_controller('Участники').SetReadOnly(readonly);
			fastening.get_fc_controller('Вопросы').SetReadOnly(readonly);
		}

		controller.OnLocation= function()
		{
			var self = this;
			var readonly = self.ReadOnly();
			var c_location = c_committee_location({ readonly: readonly, Offer_accepted: 'true' });
			if (self.model)
				c_location.SetFormContent(codec_copy().Copy(self.model));
			if (!readonly)
			{
				this.EditLocation(c_location);
			}
			else
			{
				h_msgbox.ShowModal
				({
					title: 'Параметры заседания', controller: c_location, buttons: ['Закрыть']
					, id_div: "cpw-form-ama-datamart-committee-location-readonly-form"
				});
			}
		}

		controller.EditLocation = function (c_location)
		{
			var self = this;
			var btnOk = 'Сохранить параметры заседания';
			h_msgbox.ShowModal
			({
				title: 'Редактирование параметров заседания'
				, controller: c_location
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-committee-location-form"
				, onclose: function (btn, dlg_div)
				{
					if (btn == btnOk)
					{
						h_validation_msg.IfOkWithValidateResult(c_location.Validate(), function ()
						{
							if (!self.fastening.model)
								self.fastening.model = self.model = {};
							var location = c_location.GetFormContent();

							self.model.Дата_заседания = location.Дата_заседания;
							self.model.Время_заседания = location.Время_заседания;
							self.model.Ознакомление = location.Ознакомление;
							self.model.Должник = location.Должник;
							self.model.Телефон_АУ = location.Телефон_АУ;

							var sel = self.fastening.selector;
							$(sel + ' span.location-date').text(location.Дата_заседания + ' ' + location.Время_заседания);
							$(sel + ' div.debtor span.value.address').text(location.Должник.Местонахождение);
							$(sel).trigger('model_change');
							c_location.Destroy();
							$(dlg_div).dialog("close");
						});
						return false;
					}
				}
			});
		}

		return controller;
	}
});