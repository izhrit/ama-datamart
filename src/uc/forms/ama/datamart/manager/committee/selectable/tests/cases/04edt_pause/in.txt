wait_text "Голосование"
shot_check_png ..\..\shots\01edt.png

js $('button.add').hasClass('ui-state-disabled');
js $('button.edit').attr('disabled');
js $('button.delete').attr('disabled');

wait_click_text "Голосование"
wait_click_text "Приостановить"
wait_click_text "Основные параметры"

js $('button.add').hasClass('ui-state-disabled');
js $('button.edit').attr('disabled');
js $('button.delete').attr('disabled');

shot_check_png ..\..\shots\01edt_paused.png

wait_click_full_text "Голосование"
wait_click_text "Возобновить"
wait_click_text "Основные параметры"

js $('button.add').hasClass('ui-state-disabled');
js $('button.edit').attr('disabled');
js $('button.delete').attr('disabled');

wait_click_text "Голосование"
wait_click_text "Приостановить"

js wbt.SetModelFieldValue("Выбранное_заседание", 'Алексашенко');
js wbt.SetModelFieldValue("Выбранное_заседание", 'ООО "Новострой-Технология"');

js $('button.add').hasClass('ui-state-disabled');
js $('button.edit').attr('disabled');
js $('button.delete').attr('disabled');

wait_click_full_text "Голосование"
wait_click_text "Возобновить"
wait_click_text "Основные параметры"

js $('button.add').hasClass('ui-state-disabled');
js $('button.edit').attr('disabled');
js $('button.delete').attr('disabled');

exit