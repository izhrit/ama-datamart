include ..\..\..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Заседание"
shot_check_png ..\..\shots\01edt.png

js wbt.SetModelFieldValue("Выбранное_заседание", 'Алексашенко');
je $('[data-fc-selector="Выбранное_заседание"]').select2('data', { id: 1, text: '08.08.2018 (ООО "Новострой-Технология", Н)' }).trigger('select2-selected');

wait_click_text "Голосование"
je $("button.pause").click();
wait_click_text "Основные параметры"

wait_click_text "Выбор компании"
wait_click_text "Варианты решения"
je $('input.question-variant-3').val('Вневедомственная').change();
wait_click_text "Сохранить варианты ответов"
shot_check_png ..\..\shots\01edt_variants_changed.png

wait_click_text "Сохранить параметры вопроса"

je $('[data-fc-selector="Выбранное_заседание"]').select2('data', { id: 2, text: '09.10.2019 12:00 (Алексашенко А А, РИ)' }).trigger('select2-selected');
wait_text "Мухаметзянов"
shot_check_png ..\..\shots\01edt2.png
je $('[data-fc-selector="Выбранное_заседание"]').select2('data', { id: 1, text: '08.08.2018 (ООО "Новострой-Технология", Н)' }).trigger('select2-selected');
wait_text "Сидорова"

wait_click_text "Выбор компании"
shot_check_png ..\..\shots\01edt_variants_changed.png

exit