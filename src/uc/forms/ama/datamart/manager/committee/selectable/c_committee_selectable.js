define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/selectable/e_committee_selectable.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/manager/committee/main/c_committee_main'
	, 'forms/ama/datamart/base/h_procedure_types'
],
function (c_fastened, tpl, h_msgbox, c_committee_main, h_procedure_types)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_select2 = base_url + '?action=meeting.select2'
		var base_url_crud = base_url + '?action=meeting.crud'
		var base_url_nearest = base_url + '?action=meeting.nearest'

		var options = {
			field_spec: 
				{
					Выбранное_заседание: { ajax: { url: base_url_select2, dataType: 'json' } }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			if (this.model)
			{
				if (this.model.id_Manager)
					options.field_spec.Выбранное_заседание.ajax.url += '&id_Manager=' + this.model.id_Manager;
			}

			base_Render.call(this, sel);

			var self = this;
			$(sel + ' [data-fc-selector="Выбранное_заседание"]').on('select2-selected', function (e) { self.OnSelectMeeting(); });
			$(sel + ' div.selected-meeting').on('model_change', function () { self.OnMeetingChange(); });
			$(sel + ' button.delete-meeting').click(function () { self.OnDeleteMeeting(); });

			this.LoadNearest();
		}

		controller.SelectMeeting = function (id_Meeting, meeting_text)
		{
			var sel = this.fastening.selector;
			var select2 = $(sel + ' [data-fc-selector="Выбранное_заседание"]');
			select2.select2('data', { id: id_Meeting, text: meeting_text });
			this.LoadMeeting(id_Meeting);
		}

		controller.OnSelectMeeting= function()
		{
			var sel = this.fastening.selector;
			var selected_data = $(sel + ' [data-fc-selector="Выбранное_заседание"]').select2('data');
			this.LoadMeeting(selected_data.id);
		}

		controller.LoadNearest= function()
		{
			var sel = this.fastening.selector;
			var self = this;
			var ajaxurl = base_url_nearest + '&id_Manager=' + this.model.id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Загрузка данных о ближайшем заседании с сервера", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, processData: false
				, cache: false
				, success: function (data, textStatus)
				{
					var select2 = $(sel + ' [data-fc-selector="Выбранное_заседание"]');
					if (null == data)
					{
						select2.select2('data', null);
						$(sel + ' div.no-meetings').show();
					}
					else
					{
						select2.select2('data', data.selected_data);
						self.ShowMeeting(data.selected_data.id, data.meeting_info);
					}
				}
			});
		}

		controller.LoadMeeting= function(id_Meeting)
		{
			var sel = this.fastening.selector;
			var self = this;
			var ajaxurl = base_url_crud + '&cmd=get&id=' + id_Meeting;
			var v_ajax= h_msgbox.ShowAjaxRequest("Загрузка данных о заседании с сервера", ajaxurl);
			v_ajax.ajax({
				  dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (meeting_info, textStatus)
				{
					self.ShowMeeting(id_Meeting, meeting_info);
				}
			});
		}

		controller.SafeUnshowMeeting= function()
		{
			var sel = this.fastening.selector;
			if (null != this.controller)
			{
				this.controller.Destroy();
				this.controller = null;
				this.controller_id_Meeting = null;
				$(sel + ' button.delete-meeting').hide();
				$(sel + ' div.selected-meeting').html('');
			}
		}

		controller.ShowMeeting = function (id_Meeting, meeting_info)
		{
			meeting_info.id_Meeting = id_Meeting;
			var sel = this.fastening.selector;
			$(sel + ' div.no-meetings').hide();
			this.SafeUnshowMeeting();
			this.controller_id_Meeting = id_Meeting;
			this.controller = c_committee_main({ base_url: base_url });
			this.controller.SetFormContent( meeting_info );
			this.controller.Edit(sel + ' div.selected-meeting');
			$(sel + ' button.delete-meeting').show();
		}

		controller.OnDeleteMeeting = function ()
		{
			if (!this.controller || null == this.controller)
			{
				h_msgbox.ShowModal({
					title: 'Неуместное удаление заседания комитета кредиторов'
					, html: 'Заседание комитета кредиторов для удаления не выбрано.'
				});
			}
			else
			{
				var meeting_info = this.controller.GetFormContent();
				if (meeting_info.Голосование && null != meeting_info.Голосование && 0 != meeting_info.Голосование.length)
				{
					h_msgbox.ShowModal({
						title: 'Неуместное удаление заседания комитета кредиторов', width: 600
						, html: 'Нельзя удалить запись о заседании после уведомления участников о голосовании!'
					});
				}
				else
				{
					var self = this;
					var b_ok= 'Да, удалить запись о заседании комитета кредиторов';
					h_msgbox.ShowModal({
						title: 'Подтверждение удаление заседания комитета кредиторов', width: 600
						, id_div: 'cpw-form-ama-datamart-committee-meeting-delete-confirm-form'
						, html: 'Вы действительно хотите удалить запись о засадании комитета кредиторов?'
						, buttons: [b_ok, 'Отмена']
						, onclose: function(b)
						{
							if (b==b_ok)
								self.DeleteMeeting();
						}
					});
				}
			}
		}

		controller.DeleteMeeting = function ()
		{
			var self = this;
			var ajaxurl = base_url_crud + '&cmd=delete&id=' + this.controller_id_Meeting;
			var v_ajax = h_msgbox.ShowAjaxRequest("Удаление записи о заседании на сервере", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok)
					{
						h_msgbox.ShowModal
						({
							title: 'Успешное удаление записи о заседании комитета кредиторов', width: 600
							, id_div: 'cpw-form-ama-datamart-committee-meeting-delete-ok-form'
							, html: 'Запись о заседании комитета кредиторов удалена!'
							, onclose: function ()
							{
								self.SafeUnshowMeeting();
								self.LoadNearest();
							}
						});
					}
					else
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка удаления заседания комитета кредиторов'
							, width: 450
							, html: '<span>Не удалось удалить запись о заседании комитета кредиторов по причине:</span><br/> <b>\"'
								+ responce.reason + '\"</b>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
				}
			});
		}

		controller.OnMeetingChange= function()
		{
			var meeting_info = this.controller.GetFormContent();
			var ajaxurl = base_url_crud + '&cmd=update&id=' + this.controller_id_Meeting;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача данных о заседании на сервер", ajaxurl);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: meeting_info
				, cache: false
				, success: function (m, textStatus)
				{
					var sel = self.fastening.selector;
					var select2 = $(sel + ' [data-fc-selector="Выбранное_заседание"]');
					var selected_data = select2.select2('data');
					var iproc_part_start = selected_data.text.indexOf('(');
					if (-1 != iproc_part_start)
					{
						var proc_part = selected_data.text.substring(iproc_part_start);
						selected_data.text = meeting_info.Дата_заседания + " " + meeting_info.Время_заседания + " " + proc_part;
						select2.select2('data', selected_data);
					}
					
				}
			});
		}

		return controller;
	}
});
