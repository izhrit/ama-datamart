﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/rdetails/e_committee_result_details.html'
	, 'forms/ama/datamart/base/h_questions'
	, 'forms/base/h_numbering'
],
function (c_fastened, tpl, h_questions, h_numbering)
{
	return function ()
	{
		var controller = c_fastened(tpl, { foreign_data: h_numbering });

		controller.size = { width: 900/*, height: 720*/ }

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			if (this.model && "f1" == this.model.Вопрос.На_голосование.Форма_бюллетеня)
				this.model.Вопрос.На_голосование.Варианты = h_questions.Варианты_ответов_формы1;
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' tr.variant td.text').mouseover(function (e) { self.OnVariant(e); });
			$(sel + ' tr.variant td.total').mouseover(function (e) { self.OnVariant(e); });
			$(sel + ' tr.member td.fio').mouseover(function (e) { self.OnMember(e); });
		}

		controller.OnVariant= function(e)
		{
			var t = $(e.target);
			var tr = t.parents('tr');
			var ivariant = parseInt(tr.attr('data-ivariant'));
			var table = tr.parents('table');

			var sel = this.fastening.selector;
			$(sel + ' table td').removeClass('hi');

			$(sel + ' table tr.variant[data-ivariant="' + ivariant + '"] td.text').addClass('hi');
			$(sel + ' table tr.variant[data-ivariant="' + ivariant + '"] td.total').addClass('hi');
			$(sel + ' table  tr.member[data-ivariant="' + ivariant + '"] td.fio').addClass('hi');

			for (var i = ivariant; i < 20; i++)
			{
				$(sel + ' table tr.member[data-ivariant="' + ivariant + '"] td.pointer[data-ivariant="' + (i + 1) + '"]').addClass('hi');
			}

			var last_imember = null;
			for (var i= 20; i>=0; i--)
			{
				var trm = $(sel + ' table tr.member[data-imember="' + i + '"]');
				var trm_ivariant = trm.attr('data-ivariant');
				if (ivariant == trm_ivariant)
				{
					last_imember = i;
					break;
				}
			}
			if (null != last_imember)
			{
				$(sel + ' table tr.variant[data-ivariant="' + (ivariant + 1) + '"] td.pointer').addClass('hi');
				for (var i = 0; i <= last_imember; i++)
					$(sel + ' table tr.member[data-imember="' + i + '"] td.pointer[data-ivariant="' + ivariant + '"]').addClass('hi');
			}
		}

		controller.OnMember= function(e)
		{
			var t = $(e.target);
			var tr = t.parents('tr');
			var ivariant = parseInt(tr.attr('data-ivariant'));
			var imember = parseInt(tr.attr('data-imember'));
			var table = tr.parents('table');

			var sel = this.fastening.selector;
			$(sel + ' table td').removeClass('hi');

			$(sel + ' table tr.member[data-imember="' + imember + '"] td.fio').addClass('hi');
			$(sel + ' table tr.variant[data-ivariant="' + ivariant + '"] td.text').addClass('hi');
			$(sel + ' table tr.variant[data-ivariant="' + ivariant + '"] td.total').addClass('hi');
			$(sel + ' table tr.variant[data-ivariant="' + (ivariant + 1) + '"] td.pointer').addClass('hi');
			for (var i = 0; i <= imember; i++)
				$(sel + ' table tr.member[data-imember="' + i + '"] td.pointer[data-ivariant="' + ivariant + '"]').addClass('hi');
			for (var i = ivariant; i < 20; i++)
				$(sel + ' table tr.member[data-imember="' + imember + '"] td.pointer[data-ivariant="' + (i + 1) + '"]').addClass('hi');
		}

		return controller;
	}
});