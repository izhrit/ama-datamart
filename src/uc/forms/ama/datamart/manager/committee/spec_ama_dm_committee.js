define(function ()
{

	return {

		controller: {
		  "dm_committee_parameters":     { path: 'forms/ama/datamart/manager/committee/parameters/c_committee_parameters' }
		, "dm_committee_member":         { path: 'forms/ama/datamart/manager/committee/member/c_committee_member' }
		, "dm_committee_members":        { path: 'forms/ama/datamart/manager/committee/members/c_committee_members' }
		, "dm_committee_questions":      { path: 'forms/ama/datamart/manager/committee/questions/c_committee_questions' }
		, "dm_committee_question":       { path: 'forms/ama/datamart/manager/committee/question/c_committee_question' }
		, "dm_question_variants":        { path: 'forms/ama/datamart/manager/committee/question/variants/c_question_variants' }
		, "dm_committee_location":       { path: 'forms/ama/datamart/manager/committee/location/c_committee_location' }
		, "dm_committee_selectable":     { path: 'forms/ama/datamart/manager/committee/selectable/c_committee_selectable' }
		, "dm_committee_main":           { path: 'forms/ama/datamart/manager/committee/main/c_committee_main' }
		, "dm_committee_voting":         { path: 'forms/ama/datamart/manager/committee/voting/c_committee_voting' }
		, "dm_committee_vote":           { path: 'forms/ama/datamart/manager/committee/vote/main/c_committee_vote' }
		, "dm_committee_vote_questions": { path: 'forms/ama/datamart/manager/committee/vote/questions/c_com_vote_questions' }
		, "dm_committee_results":        { path: 'forms/ama/datamart/manager/committee/results/c_committee_results' }
		, "dm_committee_result_details": { path: 'forms/ama/datamart/manager/committee/rdetails/c_committee_result_details' }
		, "dm_committee_from_ama":       { path: 'forms/ama/datamart/manager/committee/from_ama/c_committee_from_ama' }
		}

		, content: {
		  "committee-members11":                   { path: 'txt!forms/ama/datamart/manager/committee/members/tests/contents/example11.json.txt' }
		, "committee-questions":                   { path: 'txt!forms/ama/datamart/manager/committee/questions/tests/contents/example.json.txt' }
		, "committee-member-01sav":                { path: 'txt!forms/ama/datamart/manager/committee/member/tests/contents/01sav.json.etalon.txt' }
		, "committee-member-kravtsov":             { path: 'txt!forms/ama/datamart/manager/committee/member/tests/contents/example_kravtsov.etalon.txt' }
		, "committee-question-01sav":              { path: 'txt!forms/ama/datamart/manager/committee/question/tests/contents/01sav.json.etalon.txt' }
		, "committee-members-01sav":               { path: 'txt!forms/ama/datamart/manager/committee/members/tests/contents/01sav.json.etalon.txt' }
		, "committee-questions-01sav":             { path: 'txt!forms/ama/datamart/manager/committee/questions/tests/contents/01sav.json.etalon.txt' }
		, "committee-parameters-01sav":            { path: 'txt!forms/ama/datamart/manager/committee/parameters/tests/contents/01sav.json.etalon.txt' }
		, "committee-voting-example":              { path: 'txt!forms/ama/datamart/manager/committee/voting/tests/contents/example.json.txt' }

		, "committee-vote-questions-sample1":      { path: 'txt!forms/ama/datamart/manager/committee/vote/questions/tests/contents/sample1.json.txt' }
		, "committee-vote-sample1":                { path: 'txt!forms/ama/datamart/manager/committee/vote/main/tests/contents/sample1.json.txt' }

		, "committee-example1":                    { path: 'txt!forms/ama/datamart/manager/committee/main/tests/contents/example1.json.txt' }
		, "committee-example2-voting":             { path: 'txt!forms/ama/datamart/manager/committee/main/tests/contents/example2_voting.json.txt' }
		, "committee-example3-pause":              { path: 'txt!forms/ama/datamart/manager/committee/main/tests/contents/example3_pause.json.txt' }
		, "committee-example4-finish":             { path: 'txt!forms/ama/datamart/manager/committee/main/tests/contents/example4_finish.json.txt' }
		, "committee-example5-took_place":         { path: 'txt!forms/ama/datamart/manager/committee/main/tests/contents/example5_took_place.json.txt' }
		, "committee-example6-did_not_take_place": { path: 'txt!forms/ama/datamart/manager/committee/main/tests/contents/example6_did_not_take_place.json.txt' }
		, "committee-example7-cancel":             { path: 'txt!forms/ama/datamart/manager/committee/main/tests/contents/example7_cancel.json.txt' }

		, "committee-rdetails-example-max":        { path: 'txt!forms/ama/datamart/manager/committee/rdetails/tests/contents/example-max.json.txt' }
		, "committee-rdetails-example-min":        { path: 'txt!forms/ama/datamart/manager/committee/rdetails/tests/contents/example-min.json.txt' }

		, "cabinetcc-location-01sav":              { path: 'txt!forms/ama/datamart/manager/committee/location/tests/contents/01sav.json.etalon.txt' }
		, "committee-question-variants-01sav":     { path: 'txt!forms/ama/datamart/manager/committee/question/variants/tests/contents/01sav.json.etalon.txt' }
		}

	};

});