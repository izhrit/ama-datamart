define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/results/e_committee_results.html'
	, 'forms/base/codec/codec.copy'
	, 'forms/ama/datamart/base/h_questions'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/manager/committee/rdetails/c_committee_result_details'
	, 'tpl!forms/ama/datamart/manager/committee/results/v_committee_results_confirm.html'
	, 'tpl!forms/ama/datamart/manager/committee/results/v_committee_not_confirm.html'
	, 'forms/ama/datamart/base/h_Meeting_state'
	, 'forms/ama/datamart/manager/committee/results/h_committee_results'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, codec_copy, h_questions, h_msgbox, c_committee_result_details, v_committee_results_confirm, v_committee_not_confirm, h_Meeting_state, h_committee_results, h_validation_msg)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl, { h_Meeting_state: h_Meeting_state, h_committee_results: h_committee_results });

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' table.questions input[type="radio"]').change(function (e) { self.OnRadio(e); });
			$(sel + ' table.questions a.details').click(function (e) { self.OnDetails(e); });
			$(sel + ' button.fix-results').click(function () { self.OnFixResults(); });
			$(sel + ' button.send-did-not-took-place').click(function () { self.OnSendDidNotTakePlace(); });
			$(sel + ' input[name="meeting-took-place"]').change(function (e) { self.OnRadioTookPlace(e); });

			$(sel + ' button.bulletins').click(function () { self.OnPrintBulletins(); });
			$(sel + ' button.protocol').click(function () { self.OnPrintProtocol(); });
		}

		controller.OnPrintBulletins= function()
		{
			var self = this;
			var validation_result = this.ValidateToPrint();
			h_validation_msg.IfOkValidateResult({
				validation_result: validation_result
				, title_reject: 'Отказ в печати бюллетеней'
				, on_validate_ok_func: function ()
				{
					app.window_open('ui-backend.php?action=meeting.bulletins&id_Meeting=' + self.model.id_Meeting,
						'ama-committee-bulletins', 'width=1000,height=700,resizable=yes,scrollbars=yes,menubar=yes,toolbar=yes');
				}
			});
		}

		controller.OnPrintProtocol = function ()
		{
			var self = this;
			var validation_result = this.ValidateToPrint();
			h_validation_msg.IfOkValidateResult({
				validation_result: validation_result
				, title_reject: 'Отказ в печати бюллетеней'
				, on_validate_ok_func: function ()
				{
					app.window_open('ui-backend.php?action=meeting.protocol&id_Meeting=' + self.model.id_Meeting,
						'ama-committee-protocol', 'width=1000,height=700,resizable=yes,scrollbars=yes,menubar=yes,toolbar=yes');
				}
			});
		}

		var AddInvalidDescription = function (validation_res, description)
		{
			if (!validation_res)
				validation_res = [];
			var ires = { check_constraint_result: false, description: description };
			validation_res.push(ires);
			return validation_res;
		}

		controller.ValidateToPrint = function ()
		{
			var model = this.GetFormContent();
			var vres = null;
			if (!model.Вопросы || null == model.Вопросы || 0 == model.Вопросы.length)
				vres = AddInvalidDescription(vres, 'Не указано ни одного вопроса повестки дня!');
			if (!model.Участники || null == model.Участники || 0 == model.Участники.length)
				vres = AddInvalidDescription(vres, 'Не указано ни одного участника заседания!');
			return vres;
		}

		controller.OnRadioTookPlace = function (e)
		{
			var sel= this.fastening.selector;
			var d = $(sel + ' div.cpw-ama-committee-results-form');
			d.removeClass('took-place');
			d.removeClass('did-not-took-place');
			var cls = d.find('input[name="meeting-took-place"]:checked').attr('value');
			d.addClass(cls);
			if ('did-not-took-place' == cls)
				$(sel + ' div.comments textarea').focus();
		}

		controller.OnRadio= function(e)
		{
			var r = $(e.target);
			var tr = r.parents('tr');
			tr.removeClass('decision-made');
			tr.removeClass('decision-is-not-made');

			tr.addClass(tr.find('input:checked').attr('value'));
		}

		var найти_вопрос= function(Вопросы, номер)
		{
			for (var i= 0; i<Вопросы.length; i++)
			{
				var вопрос= Вопросы[i];
				if (номер==вопрос.Номер || !вопрос.Номер && (i+1)==номер)
					return вопрос;
			}
			return null;
		}

		var СобратьДеталиПоВопросу= function(номер, вопрос, Голосование)
		{
			var res = {
				Вопрос: {
					Номер: номер
					, На_голосование: {
						Формулировка: вопрос.На_голосование.Формулировка
						, Форма_бюллетеня: вопрос.На_голосование.Форма_бюллетеня
					}
				}
				,Голоса:[]
			};
			if (вопрос.На_голосование.Варианты)
				res.Вопрос.На_голосование.Варианты = вопрос.На_голосование.Варианты;
			if (Голосование && null != Голосование)
			{
				for (var i = 0; i < Голосование.length; i++)
				{
					var голоса_участника = Голосование[i];
					var текст_ответа = null;
					if (голоса_участника.Ответы && null != голоса_участника.Ответы)
					{
						for (var j = 0; j < голоса_участника.Ответы.length; j++)
						{
							var ответ = голоса_участника.Ответы[j];
							if (номер == ответ.На_вопрос.Номер)
							{
								текст_ответа = ответ.Ответ;
								break;
							}
						}
					}
					if (текст_ответа && null != текст_ответа)
						res.Голоса.push({ Участник: голоса_участника.ФИО, Ответ: текст_ответа });
				}
				return res;
			}
			
		}

		controller.OnDetails= function(e)
		{
			var a = $(e.target);
			var tr = a.parents('tr');
			var номер = tr.attr('number');

			var model = this.model;

			var вопрос= найти_вопрос(model.Вопросы, номер);
			var details = СобратьДеталиПоВопросу(номер, вопрос, model.Голосование);

			var c_details = c_committee_result_details();
			c_details.SetFormContent(details);
			h_msgbox.ShowModal
			({
				title: 'Детали голосования по вопросу №' + номер
				, controller: c_details
				, id_div: "cpw-form-ama-committee-results-details-msg"
				, buttons: ['Закрыть детали голосования']
			});
		}

		controller.Результаты = function ()
		{
			var sel = this.fastening.selector;

			var результаты = [];

			$(sel + ' tr.question').each(function (index)
			{
				var tr = $(this);
				var результат = { По_вопросу: { Номер: tr.attr('number'), Формулировка: tr.find('span.question').text() } };
				if (tr.hasClass('decision-is-not-made'))
				{
					результат.Решение_НЕ_принято = true;
				}
				else
				{
					результат.Принято_решение = tr.find('textarea').text();
				}
				результаты.push(результат);
			});

			return результаты;
		}

		controller.OnSendDidNotTakePlace= function()
		{
			var self = this;
			var пояснение = $(this.fastening.selector + ' div.comments textarea').val();
			var btn_ok = "Да, зафиксировать, и уведомить"
			h_msgbox.ShowModal({
				title: 'Регистрация несостоявшегося заседания'
				, width: 600
				, html: v_committee_not_confirm(пояснение)
				, id_div: "cpw-form-ama-committee-no-confirm-msg"
				, buttons: [btn_ok, 'Отмена']
				, onclose: function (btn)
				{
					if (btn_ok == btn)
					{
						self.DidNotTookPlace(пояснение);
					}
				}
			});
		}

		controller.OnFixResults= function()
		{
			var результаты = this.Результаты();
			var self = this;
			var btn_ok = "Да, сохранить результаты и уведомить о них участников"
			h_msgbox.ShowModal({
				title: 'Подтверждение результатов заседания'
				, width: 800
				, html: v_committee_results_confirm(результаты)
				, id_div: "cpw-form-ama-committee-results-confirm-msg"
				, buttons: [btn_ok, 'Отмена']
				, onclose: function(btn)
				{
					if (btn_ok == btn)
					{
						self.FixResults(результаты);
					}
				}
			});
		}

		controller.FixResults= function(результаты)
		{
			var self = this;
			var url = this.base_url + '?action=meeting.state&state=e&id_Meeting=' + this.model.id_Meeting;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача паодведённых итогов заседания на сервер", url);

			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: { результаты: результаты }
				, cache: false
				, success: function (responce, textStatus)
				{
					self.OnFixedResults(результаты, responce);
				}
			});
		}

		controller.OnFixedResults= function(результаты, responce)
		{
			this.ChangeState('Состоялось');
			h_msgbox.ShowModal({
				title: 'Сообщение о сохранении итогов'
				, id_div: "cpw-form-ama-committee-results-fixed-msg"
				, html: 'Итоги заседания подведены'
			});
		}

		controller.ChangeState= function(state_descr)
		{
			var sel = this.fastening.selector;
			var d = h_Meeting_state.Meeting_State_description_byDescr[state_descr];
			$(sel + ' > div.cpw-ama-committee-results-form').attr('data-state', d.code);

			this.model.Состояние = state_descr;
			this.Render(sel);
		}

		controller.DidNotTookPlace = function (пояснения)
		{
			var self = this;
			var url = this.base_url + '?action=meeting.state&state=f&id_Meeting=' + this.model.id_Meeting;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача информации о том, что заседание не состоялось на сервер", url);

			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: { Пояснения: пояснения }
				, cache: false
				, success: function (responce, textStatus)
				{
					if (!responce.ok)
					{
						h_msgbox.ShowModal({
							title: 'Неудачная попытка объявить заседание несостоявшимся'
							, html: 'Попытка объявить заседание несостоявшимся завершилась неудачей'
						});
					}
					else
					{
						self.model.Заседание_не_состоялось = { Пояснения:пояснения };
						self.ChangeState('Не состоялось');
						h_msgbox.ShowModal({
							title: 'Сообщение о сохранении'
							, id_div: "cpw-form-ama-committee-not-ok-msg"
							, html: 'Заседание объявлено несостоявшимся'
						});
					}
				}
			});
		}

		return controller;
	}
});