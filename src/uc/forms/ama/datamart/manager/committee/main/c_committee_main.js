﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/main/e_committee_main.html'
	, 'forms/ama/datamart/manager/committee/parameters/c_committee_parameters'
	, 'forms/ama/datamart/manager/committee/voting/c_committee_voting'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/manager/committee/results/c_committee_results'
	, 'forms/base/h_validation_msg'
	, 'forms/ama/datamart/base/h_Meeting_state'
],
function (c_fastened, tpl, c_committee_parameters, c_committee_voting, h_msgbox, c_committee_results, h_validation_msg, h_Meeting_state)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var options = {
			field_spec:
				{
					Параметры: { controller: function () { return c_committee_parameters({ base_url: base_url }); } }
					, Голосование: { controller: function () { return c_committee_voting({ base_url: base_url }); } }
					, Результаты: { controller: function () { return c_committee_results({ base_url: base_url }); } }
				}
			, h_Meeting_state: h_Meeting_state
		};

		var controller = c_fastened(tpl, options);

		controller.base_url = base_url;

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.start').click(function () { self.OnStart(); });
			$(sel + ' button.pause').click(function () { self.OnPause(); });
			$(sel + ' button.stop').click(function () { self.OnStop(); });

			var c_Результаты = self.fastening.get_fc_controller('Результаты');
			var old_ChangeState = c_Результаты.ChangeState;
			c_Результаты.ChangeState = function (state_descr)
			{
				old_ChangeState.call(this, state_descr);
				self.ChangeStateWithoutResults(state_descr);
			};
		}

		controller.ChangeState = function (state_descr)
		{
			this.ChangeStateWithoutResults(state_descr);
			var c_Результаты = this.fastening.get_fc_controller('Результаты');
			c_Результаты.Render(c_Результаты.fastening.selector);
		}

		controller.ChangeStateWithoutResults = function (state_descr)
		{
			var d = h_Meeting_state.Meeting_State_description_byDescr[state_descr];
			var state_code = d.code;
			var fastening = this.fastening;
			var sel = fastening.selector;
			this.model.Состояние = state_descr;
			var r = $(sel + ' div.cpw-ama-committee-main-form');
			r.attr('data-state', state_code);
			fastening.get_fc_controller('Параметры').SetReadOnly(d.Readonly);

			$(sel + ' button.pause').attr('disabled', !d.can.pause);
			$(sel + ' button.start').attr('disabled', !d.can.start);
			$(sel + ' button.stop').attr('disabled', !d.can.stop);

			if ('Приостановлено' == state_descr)
			{
				r.removeClass('voting-first');
				r.addClass('voting-other');
			}
		}

		controller.OnPause = function ()
		{
			var self = this;
			var ajaxurl = this.base_url + '?action=meeting.state&state=c&id_Meeting=' + this.model.id_Meeting;
			var v_ajax = h_msgbox.ShowAjaxRequest("Рассылка уведомлений о приостановке заседания", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (responce, textStatus)
				{
					if (responce.ok)
					{
						self.ChangeState('Приостановлено');
					}
					else
					{
						h_msgbox.ShowModal({
							title: 'Неудачная приостановка заседания'
							, html: 'Попытка приостановить заседания завершилась неудачей'
						});
					}
				}
			});
		}

		controller.OnStop = function ()
		{
			var self = this;
			var ajaxurl = this.base_url + '?action=meeting.state&state=g&id_Meeting=' + this.model.id_Meeting;
			var v_ajax = h_msgbox.ShowAjaxRequest("Рассылка уведомлений об отмене заседания", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (responce, textStatus)
				{
					if (responce.ok)
					{
						self.ChangeState('Отменено');
					}
					else
					{
						h_msgbox.ShowModal({
							title: 'Неудачная отмена заседания'
							, html: 'Попытка отмены заседания завершилось неудачей'
						});
					}
				}
			});
		}

		controller.OnStart = function ()
		{
			var self = this;
			var validation_result = this.ValidateToStart();
			h_validation_msg.IfOkValidateResult({
				validation_result: validation_result
				, title_reject: 'Отказ разослать уведомления и начать голосование'
				, on_validate_ok_func: function ()
				{
					self.DoStart();
				}
			});
		}

		controller.AddInvalidDescription = function (validation_res, description)
		{
			if (!validation_res)
				validation_res = [];
			var ires = { check_constraint_result: false, description: description };
			validation_res.push(ires);
			return validation_res;
		}

		controller.ValidateToStart = function ()
		{
			var self = this;
			var model = this.GetFormContent();
			var vres = null;
			if (!model.Вопросы || null == model.Вопросы || 0 == model.Вопросы.length)
				vres = self.AddInvalidDescription(vres, 'Не указано ни одного вопроса повестки дня!');
			if (!model.Участники || null == model.Участники || 0 == model.Участники.length)
				vres = self.AddInvalidDescription(vres, 'Не указано ни одного участника заседания!');
			return vres;
		}

		controller.DoStart = function ()
		{
			var self = this;
			var ajaxurl = this.base_url + '?action=meeting.state&state=b&id_Meeting=' + this.model.id_Meeting;
			var v_ajax = h_msgbox.ShowAjaxRequest("Рассылка уведомлений о заседании и начало голосования на сервере", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (Голосование, textStatus)
				{
					if (null == Голосование)
					{
						v_ajax.ShowAjaxError(Голосование, textStatus);
					}
					else
					{
						var с_Голосование = self.fastening.get_fc_controller('Голосование');
						с_Голосование.Destroy();
						с_Голосование.SetFormContent(Голосование);
						с_Голосование.Edit(self.fastening.selector + ' [data-fc-selector="Голосование"]');
						self.ChangeState('Голосование');
					}
				}
			});
		}

		return controller;
	}
});