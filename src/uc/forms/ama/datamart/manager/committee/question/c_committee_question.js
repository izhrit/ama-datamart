﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/question/e_committee_question.html'
	, 'forms/ama/datamart/manager/committee/question/variants/c_question_variants'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
],
function (c_fastened, tpl, c_question_variants, h_msgbox, h_validation_msg)
{
	return function (options)
	{
		var controller = c_fastened(tpl, options);

		controller.size = { width: 600, height: 500 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			this.OnUpdateFormSelect();

			var self = this;
			$(sel + ' textarea[data-fc-selector="В_повестке"]').focus();
			$(sel + ' select').change(function () { self.OnUpdateFormSelect(); });
			$(sel + ' div.f2').click(function () { self.OnChooseVariants(); });
		}

		controller.OnUpdateFormSelect= function()
		{
			var sel = this.fastening.selector;
			var f = $(sel + ' select').val();
			switch (f)
			{
				case 'f1':
					$(sel + ' div.f1').show();
					$(sel + ' div.f2').hide();
					break;
				case 'f2':
					$(sel + ' div.f1').hide();
					$(sel + ' div.f2').show();
					break;
			}
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent= function()
		{
			var res_model = base_GetFormContent.call(this);
			if ('f1' == res_model.На_голосование.Форма_бюллетеня && res_model.На_голосование.Варианты)
				delete res_model.На_голосование.Варианты;
			return res_model;
		}

		controller.OnChooseVariants= function()
		{
			var fastening = this.fastening;
			var sel = fastening.selector;
			var fc_dom_item = $(sel + ' [data-fc-selector="На_голосование.Варианты"]');

			var self = this;
			var readonly = self.options && self.options.readonly;
			var сс_question_variants = c_question_variants({ readonly: readonly });
			сс_question_variants.SetFormContent(fastening.get_fc_model_value(fc_dom_item));

			var title = 'Варианты решений для типовой формы бюллетеня №2';
			if (readonly)
			{
				h_msgbox.ShowModal
				({
					title: title, controller: сс_question_variants, buttons: ['Закрыть']
					, id_div: "cpw-form-ama-committee-question-variants-readonly-form"
				});
			}
			else
			{
				var btnOk = 'Сохранить варианты ответов';
				h_msgbox.ShowModal
				({
					title: title
					, controller: сс_question_variants
					, buttons: [btnOk, 'Отмена']
					, id_div: "cpw-form-ama-committee-question-variants-form"
					, onclose: function (btn)
					{
						if (btn == btnOk)
						{
							var variants = сс_question_variants.GetFormContent();
							fastening.set_fc_model_value(fc_dom_item, variants);
							$(sel).trigger('model_change');
						}
					}
				});
			}
		}

		controller.Validate = function ()
		{
			var res = null;
			var model = this.GetFormContent();
			if (''==h_validation_msg.trim(model.В_повестке))
				res = h_validation_msg.error(res, "Формулировка в повестке дня НЕ может быть пустой");
			if ('' == h_validation_msg.trim(model.На_голосование.Формулировка))
				res = h_validation_msg.error(res, "Формулировка решения на голосование НЕ может быть пустой");
			if ('f2' == model.На_голосование.Форма_бюллетеня)
			{
				if (0 == model.На_голосование.Варианты.length)
					res = h_validation_msg.warning(res, "Не указаны варианты решения, хотя обычно несколько вариантов заполняют заранее", true);
				else if (1 == model.На_голосование.Варианты.length)
					res = h_validation_msg.warning(res, "Указан лишь один вариант решения, хотя обычно заранее заполняют несколько", true);
			}
			return res;
		}

		return controller;
	}
});