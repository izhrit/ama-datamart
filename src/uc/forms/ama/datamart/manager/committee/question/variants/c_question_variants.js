﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/committee/question/variants/e_question_variants.html'
],
function (c_fastened, tpl)
{
	return function (options)
	{
		var controller = c_fastened(tpl, options);

		controller.size = { width: 600, height: 525 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			if (!this.variants)
				this.variants = [];
			this.FillData(this.variants);

			var self = this;
			$(sel + ' input').change(function (e) { self.OnChange(e); });
		}

		controller.SetFormContent= function(variants)
		{
			this.variants = ('string'!=typeof variants) ? variants : JSON.parse(variants);
		}

		controller.GetFormContent= function()
		{
			var sel = this.fastening.selector;
			var variants = [];
			for (var i = 0; i < 10; i++)
			{
				var txt = $(sel + ' input.question-variant-' + i).val();
				txt = txt.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
				if ('' != txt)
				{
					variants.push(txt);
				}
			}
			return variants;
		}

		controller.OnChange = function (e)
		{
			this.FillData(this.GetFormContent());
		}

		controller.FillData = function (variants)
		{
			var sel = this.fastening.selector;
			var cursor_rendered = false;
			for (var i = 0; i < 10; i++)
			{
				var input_variant = $(sel + ' input.question-variant-' + i);
				var div_variant = input_variant.parents('div.variant');
				div_variant.removeClass('filled cursor empty');
				if (i < variants.length)
				{
					div_variant.addClass('filled');
					var variant = variants[i];
					input_variant.val(variant);
				}
				else
				{
					input_variant.val('');
					if (cursor_rendered)
					{
						div_variant.addClass('empty');
					}
					else
					{
						div_variant.addClass('cursor');
						cursor_rendered = true;
						input_variant.focus();
					}
				}
			}
		}

		return controller;
	}
});