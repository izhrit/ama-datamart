﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/manager/procedures/e_dm_man_procedures.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/manager/proc_viewers/c_dm_man_proc_viewers'
	, 'forms/ama/datamart/base/show_msg_under_construction'
	, 'forms/ama/datamart/base/h_procedure_types'
	, 'forms/ama/datamart/manager/committee/parameters/c_committee_parameters'
	, 'forms/ama/datamart/manager/committee/location/c_committee_location'
	, 'forms/ama/datamart/base/h_debtorName'
	, 'forms/ama/datamart/manager/procedure/c_dm_man_procedure'
	, 'forms/ama/datamart/base/h_questions'
	, 'forms/base/h_validation_msg'
	, 'tpl!forms/ama/datamart/manager/procedures/v_man_can_not_with_no.html'
],
function (c_fastened, tpl, h_msgbox, c_dm_man_proc_viewers, show_msg_under_construction, h_procedure_types
	, c_committee_parameters, c_committee_location, h_debtorName, c_dm_man_procedure, h_questions, h_validation_msg, v_man_can_not_with_no)
{
	return function (options_arg)
	{
		var text_show_Archive= 'Все процедуры, включая архивированные:';
		var text_show_UnArchive= 'Все процедуры, кроме архивированных:';
		var options = {
			text_show_UnArchive: text_show_UnArchive
			,field_spec:
				{
					'Показывать_архивные_процедуры':
					{
						text: function (f) { return f ? text_show_Archive : text_show_UnArchive }
					}
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();

			var self= this;
			$(sel + ' a[data-fc-selector="Показывать_архивные_процедуры"]').click(function () { self.OnChange_Показывать_архивные(); });
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.url_grid = controller.base_url + '?action=procedure.jqgrid&version=1';
		controller.url_proc_viewers_crud = controller.base_url + '?action=proc-viewers.ru';
		controller.url_procedure_crud = controller.base_url + '?action=procedure.ru';

		controller.meeting_crud_url = controller.base_url + '?action=meeting.crud';

		controller.PrepareUrl = function ()
		{
			var url = this.url_grid;
			if (this.model)
				url += '&id_Manager=' + this.model.id_Manager;

			var sel= this.fastening.selector;
			var txt= $(sel + ' a[data-fc-selector="Показывать_архивные_процедуры"]').text();
			var show_archive= txt==text_show_Archive;
			if (show_archive)
				url += '&show_archive=true';

			return url;
		}

		var procedureFormatter= function(cellvalue, options, rowObject)
		{
			return !rowObject.procedure_type || null == rowObject.procedure_type
				? '' : h_procedure_types.SafeShortForDBValue(rowObject.procedure_type);
		}

		var debtorNameFormatter = function (cellvalue, options, rowObject)
		{
			return h_debtorName.beautify_debtorName(rowObject.debtorName);
		}

		var viewersFormatter = function (cellvalue, options, rowObject)
		{
			var viewers = rowObject.Viewers;
			if (!viewers || null == viewers)
				viewers = '';
			else if ('string' != (typeof viewers) && viewers.length)
				viewers= viewers.join(', ');
			return viewers;
		}

		var dateFormat = function(cellvalue, options, rowObject)
		{
			var res= '';
			if (rowObject.publicDate && rowObject.publicDate.length > 0)
			{
				var dateAndTimeSplit = rowObject.publicDate.split(' ');
				if (2!=dateAndTimeSplit.length)
					dateAndTimeSplit = rowObject.publicDate.split('T');
				var dateSplit = dateAndTimeSplit[0].split('-');
				var timeSplit = dateAndTimeSplit[1].split(':')

				var date = dateSplit[2] + '.' + dateSplit[1] + '.' + dateSplit[0] + ' ' 
				+ timeSplit[0] + ':' + timeSplit[1];

				res= date;
			}
			if (0!=rowObject.Archived)
				res= '<span class="Archived">' + res + '</span>';
			return res;
		}

		controller.colModel =
		[
			  { name: 'id_MProcedure', hidden: true }
			, { name: 'id_Debtor_Manager', hidden: true }
			, { label: 'Тип пр.', name: 'procedure_type', width: 30, align: 'left', formatter: procedureFormatter }
			, { label: 'Должник', name: 'debtorName', align: 'left', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] }, formatter: debtorNameFormatter }
			, { label: 'На дату', name: 'publicDate', align: 'left', width: 50, search: false, formatter: dateFormat }
			, { label: 'За процедурой наблюдают', align: 'left', name: 'Viewers', search: false, formatter: viewersFormatter }
			, { label: ' ', name: 'myac', width: 20, align: 'right', search: false}
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url= self.PrepareUrl();
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка страницы списка процедур...'
				, recordtext: 'Показано процедур {1} из {2}'
				, emptyText: 'Нет процедур для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, pager: '#cpw-ama-datamart-procedures-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true 
				, onSelectRow: function (id, s, e) 
				{ 
					self.onSelectRow_if_no_menu(id, s, e, function () { self.OnProcedure(); });
				}
				, ondblClickRow: function () { self.OnProcedure(); }
				, loadComplete: function ()
				{
					self.RenderRowActions(grid);
					var dlcc_name = 'data-load-complete-count';
					var dlcc = grid.attr(dlcc_name);
					grid.attr(dlcc_name, !dlcc || null == dlcc ? 1 : parseInt(dlcc)+1);
				}
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка процедур", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnChange_Показывать_архивные= function()
		{
			var sel= this.fastening.selector;
			var txt= $(sel + ' a[data-fc-selector="Показывать_архивные_процедуры"]').text();
			var show_archive= txt==text_show_Archive;
			var grid = $(sel + ' table.grid');
			grid.jqGrid('setGridParam',{ url: this.PrepareUrl() });
			this.ReloadGrid();
		}

		controller.grid_row_buttons = function ()
		{
			var self = this;
			return [
				{ "class": "create-meeting", text: "Назначить заседание комитета кредиторов", click: function () { self.OnCommitteeMeeting(); } }
				, { "class": "open-properties", text: "Параметры процедуры", click: function () { self.OnProcedureProperties(); } }
				, { "class": "show-viewers", text: "Лица, допущенные к информации процедуры", click: function () { self.OnViewers(); } }
				, { "class": "open-datamart", text: "Открыть витрину процедуры", click: function () { self.OnProcedure(); } }
			];
		};

		controller.OnProcedure = function ()
		{
			if (!this.ShowProcedure)
			{
				alert('Просмотр информации, размещённой по процедуре не реализован');
			}
			else
			{
				var sel = this.fastening.selector;
				var grid = $(sel + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');
				var rowdata = grid.jqGrid('getRowData', selrow);
				var procedure_select2_data= h_procedure_types.PrepareSelect2(rowdata);
				this.ShowProcedure(procedure_select2_data);
			}
		}

		controller.Недоступно_без_id_MProcedure = function (obj_Функционал_Должник)
		{
			h_msgbox.ShowModal({
				title: 'Функционал не доступен', width: 600,
				html: v_man_can_not_with_no(obj_Функционал_Должник)
			});
		}

		controller.OnCommitteeMeeting= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			var self = this;

			$(sel + " ul.jquery-menu").hide();

			if (!rowdata.id_MProcedure)
			{
				this.Недоступно_без_id_MProcedure({ Функционал: 'электронный комитет кредиторов', Должник: rowdata.debtorName });
			}
			else
			{
				var ajaxurl = controller.base_url + '?action=meeting.fill-new&id_MProcedure=' + rowdata.id_MProcedure;
				var v_ajax = h_msgbox.ShowAjaxRequest("Запрос данных для нового заседания с сервера", ajaxurl);
				v_ajax.ajax({
					dataType: "json"
					, type: 'POST'
					, cache: false
					, success: function (data, textStatus)
					{
						if (!data || null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							self.OnFilledCommitteeMeeting(rowdata, data);
						}
					}
				});
			}
		}

		controller.OnFilledCommitteeMeeting = function (rowdata, start_data)
		{
			var self = this;
			var c_location = c_committee_location({ Offer_accepted: start_data.Offer_accepted });
			c_location.SetFormContent({
				Телефон_АУ: start_data.Телефон_АУ
				, Должник: start_data.Должник
				, Ознакомление: start_data.Ознакомление
				, Дата_заседания: start_data.Дата_заседания
				, Время_заседания: start_data.Время_заседания
				, Участники: start_data.Участники
				, Вопросы: h_questions.Начальная_повестка_комитета_кредиторов(rowdata.procedure_type, rowdata.debtorName)
			});
			var btnOk = 'Перейти к участникам и повестке';
			h_msgbox.ShowModal
			({
				title: 'Назначение заседания комитета кредиторов'
				, controller: c_location
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-committee-location-form"
				, onclose: function (btn, dlg_div)
				{
					if (btn == btnOk)
					{
						h_validation_msg.IfOkWithValidateResult(c_location.Validate(), function ()
						{
							var meeting = c_location.GetFormContent();
							meeting.id_MProcedure = rowdata.id_MProcedure;
							var meeting_text = meeting.Дата_заседания + ' ' + meeting.Время_заседания + ' (' + h_procedure_types.PrepareText(rowdata) + ')';
							c_location.Destroy();
							$(dlg_div).dialog("close");
							self.OnCommitteeMeeting_for_location(meeting, meeting_text);
						});
						return false;
					}
				}
			});
		}

		controller.OnCommitteeMeeting_for_location = function (meeting, meeting_text)
		{
			var self = this;
			var btnOk = 'Сохранить заседание комитета кредиторов';
			var c_meeting = c_committee_parameters();
			c_meeting.SetFormContent(meeting);
			h_msgbox.ShowModal
			({
				title: 'Назначение заседания комитета кредиторов'
				, controller: c_meeting
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-committee-form"
				, onclose: function (btn, dlg_div)
				{
					if (btn == btnOk)
					{
						var meeting = c_meeting.GetFormContent();
						$(dlg_div).dialog("close");
						self.AddCommitteeMeeting(meeting, meeting_text);
						return false;
					}
				}
			});
		}

		controller.AddCommitteeMeeting = function (meeting, meeting_text)
		{
			var self = this;
			var ajaxurl = this.meeting_crud_url + '&cmd=add';
			var v_ajax = h_msgbox.ShowAjaxRequest("Добавление данных о заседании на сервер", ajaxurl);
			v_ajax.ajax({
				  dataType: "json"
				, type: 'POST'
				, data: meeting
				, cache: false
				, success: function (data, textStatus)
				{
					if (!data || null == data || !data.id || null == data.id)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.ShowMeeting(data.id, meeting_text);
					}
				}
			});
		}

		controller.OnViewers = function ()
		{
			if (app.disable_viewers)
			{
				show_msg_under_construction();
				return;
			}
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;

			$(sel + " ul.jquery-menu").hide();

			if (!rowdata.id_MProcedure)
			{
				this.Недоступно_без_id_MProcedure({ Функционал: 'допуск к информации процедуры', Должник: rowdata.debtorName });
			}
			else
			{
				var ajaxurl = this.url_proc_viewers_crud + '&cmd=get&id=' + rowdata.id_MProcedure;
				var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных о наблюдателях с сервера", ajaxurl);
				v_ajax.ajax
				({
					  dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							self.EditViewers(rowdata.id_MProcedure, data);
							self.ReloadGrid();
							$(sel).trigger('model_change');
						}
					}
				});
			}
		}

		controller.EditViewers = function (id, proc_viewers)
		{
			var db_value = proc_viewers.ТипПроцедуры;
			proc_viewers.ТипПроцедуры = h_procedure_types.SafeTextForDBValue(db_value);
			var c_proc_viewqers = c_dm_man_proc_viewers({ base_url: this.base_url, id_Manager: !this.model ? null : this.model.id_Manager });
			var self = this;
			c_proc_viewqers.SetFormContent(proc_viewers);
			var btnOk = 'Сохранить список наблюдателей за процедурой';
			h_msgbox.ShowModal
			({
				title: 'Настройка списка наблюдателей за процедурой'
				, controller: c_proc_viewqers
				, width: 580, height: 450
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-proc-viewers"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						self.UpdateViewers(id, c_proc_viewqers.GetFormContent());
					}
				}
			});
		}

		controller.UpdateViewers = function (id, proc_viewers)
		{
			if (this.model)
				proc_viewers.id_Manager = this.model.id_Manager;
			var sel = this.fastening.selector;
			var self = this;
			var ajaxurl = this.url_proc_viewers_crud + '&cmd=update&id=' + id;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача данных на сервер", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'POST'
				, data: proc_viewers
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						h_msgbox.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.ReloadGrid();
						$(sel).trigger('model_change');
					}
				}
			});
		}

		controller.OnProcedureProperties= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;

			$(sel + " ul.jquery-menu").hide();
			if (!rowdata.id_MProcedure)
			{
				this.Недоступно_без_id_MProcedure({ Функционал: 'редактирование информации о процедуре', Должник: rowdata.debtorName });
			}
			else
			{
				var ajaxurl = this.url_procedure_crud + '&cmd=get&id=' + rowdata.id_MProcedure;
				var v_ajax = h_msgbox.ShowAjaxRequest("Получение параметров процедуры с сервера", ajaxurl);
				v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (procedure, textStatus)
					{
						if (null == procedure)
						{
							v_ajax.ShowAjaxError(procedure, textStatus);
						}
						else
						{
							self.EditProcedure(rowdata.id_MProcedure, procedure);
							self.ReloadGrid();
							$(sel).trigger('model_change');
						}
					}
				});
			}
		}

		controller.EditProcedure= function (id, procedure)
		{
			var db_value = procedure.ТипПроцедуры;
			procedure.ТипПроцедуры = h_procedure_types.SafeTextForDBValue(db_value);
			var c_procedure = c_dm_man_procedure({ base_url: this.base_url, id_Manager: !this.model ? null : this.model.id_Manager });
			var self = this;
			c_procedure.SetFormContent(procedure);
			var btnOk = 'Сохранить параметры процедуры';
			h_msgbox.ShowModal
			({
				title: 'Параметры процедуры'
				, controller: c_procedure
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-procedure"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						self.UpdateProcedure(id, c_procedure.GetFormContent());
					}
				}
			});
		}

		controller.UpdateProcedure = function (id, procedure)
		{
			if (this.model)
				procedure.id_Manager = this.model.id_Manager;
			var sel = this.fastening.selector;
			var self = this;
			var ajaxurl = this.url_procedure_crud + '&cmd=update&id=' + id;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача параметров процедуры на сервер", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'POST'
				, data: procedure
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						h_msgbox.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.ReloadGrid();
						$(sel).trigger('model_change');
					}
				}
			});
		}

		return controller;
	}
});