include ..\..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Искать"

start_store_lines_as new_ajax

$.ajaxTransport( '+*', function( options, originalOptions, jqXHR ) {
  var ih= options.url.indexOf("ama/datamart?action=procedure.jqgrid");
  if(-1 != ih) {

    return {
      send: function( headers, completeCallback ) {

		var res=
{
	"page": 1,
	"total": 1,
	"records": 3,
	"rows": [
		{
			"id_MProcedure": 1,
			"id_Manager": 1,
			"caseNumber": "А81-2868/2018",
			"procedure_type": "n",
			"debtorInn": "322223234",
			"debtorOgrn": "234234",
			"debtorName": "Общество с ограниченной ответственностью Которое было длинное",
			"Manager": "Иванов И. И.",
			"Viewers": "Сергеева Светлана Михайловна (Банк Возрождение), Алексеев О А (ФНС)"
		},
		{
			"id_MProcedure": 2,
			"id_Manager": 1,
			"caseNumber": "А16-601/2017",
			"procedure_type": "i",
			"debtorInn": "230678",
			"debtorSnils": "6827394",
			"debtorName": "индивидуальный предприниматель Алексашенко А А",
			"Manager": "Иванов И. И.",
			"Viewers": "Сергеева Светлана Михайловна (Банк Возрождение), Селянин И О"
		},
		{
			"id_MProcedure": 3,
			"id_Manager": 1,
			"caseNumber": "А16-936/2014",
			"procedure_type": "k",
			"debtorInn": "234788",
			"debtorOgrn": "234234345634",
			"debtorName": "Открытое акционерное общество \"ОАО КАРИНА\"",
			"Manager": "Иванов И. И.",
			"Viewers": "Сергеева Светлана Михайловна (Банк Возрождение), Селянин И О, Алексеев О А (ФНС)"
		}
	]
}
        completeCallback(200, 'success', { text: JSON.stringify(res) });
      },
      abort: function() {
        // Abort code
      }
    };
  }
});

stop_store_lines

execute_javascript_stored_lines new_ajax
je $('#gs_debtorName').val('О').change().keyup().keydown();
je $('#gs_debtorName').val('').change().keyup().keydown();
wait_text "ОАО "
shot_check_png ..\..\shots\01beauty_names.png

exit