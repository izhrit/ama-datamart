﻿define([
	'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/manager/requests/e_dm_man_requests.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/base/h_Request_state'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/ama/datamart/manager/request/c_dm_man_request'
],
function (c_fastened, tpl, h_msgbox, h_Request_state, h_codec_datetime, c_dm_man_request)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_crud_url = controller.base_url + '?action=request.crud';
		controller.base_grid_url = controller.base_url + '?action=request.jqgrid';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_Manager=' + this.model.id_Manager;
			return url;
		}

		var statusFormatter = function (cellvalue, options, rowObject)
		{
			return !rowObject.state || null == rowObject.state
				? '' : h_Request_state.byCode[rowObject.state].Состояние.доступа;
		}

		var timeFormatter = function (cellvalue, options, rowObject)
		{
			var timeLastChange = rowObject.TimeLastChange.substr(0, 10) + 'T' + rowObject.TimeLastChange.slice(11)
			return h_codec_datetime.mysql_txt2txt_ru().Encode(timeLastChange)
		}

		controller.colModel =
		[
			  { name: 'id_Request', hidden: true }
			, { label: 'Имя', name: 'Имя', width:210 }
			, { label: 'email', name: 'email', width:120 }
			, { label: 'Представляет', name: 'Представляет', width: 210, search: false }
			, { label: 'В процедуре', name: 'В_процедуре', width: 210 }
			, { label: 'Статус', name: 'Статус', formatter: statusFormatter, width: 80, search: false }
			, { label: 'Изменено', name: 'Изменено', formatter: timeFormatter, width: 120, search: false }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = self.PrepareUrl();
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка списка запросов...'
				, recordtext: 'Запросы {0} - {1} из {2}'
				, emptyText: 'Нет запросов для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, pager: '#cpw-ama-datamart-requests-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true 
				, onSelectRow: function () { self.OnEdit(); }
				, ondblClickRow: function () { self.OnEdit(); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка запросов", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnEdit= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=get&id_MUser=' + this.model.id_MUser + '&id=' + rowdata.id_Request;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение параметров запроса с сервера", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.EditRequest(rowdata.id_Request, data);
					}
				}
			});
		}

		controller.EditRequest = function (id_Request, request)
		{
			var self = this;
			var id_div = "cpw-form-ama-datamart-man-request";
			var c_request = c_dm_man_request();
			c_request.SetFormContent({
				Должник: request.Должник.Наименование
				, Заявитель: {
					Имя: request.Заявитель.Имя
					, EMail: request.Заявитель.email
					, Представляет: request.Процедура.КогоПредставляетЗаявитель
					, Поясняет: request.Пояснения
				}
				, TimeLastChange: request.TimeLastChange
				, State: request.State
			});
			c_request.OnReject = function () { $('#' + id_div).dialog('close'); self.OnDecision(id_Request, 'reject'); }
			c_request.OnApprove = function () { $('#' + id_div).dialog('close'); self.OnDecision(id_Request, 'approve'); }
			h_msgbox.ShowModal({
				title: 'Заявка на доступ к данным процедуры на Витрине ПАУ'
				, controller: c_request, buttons: ['Закрыть'], id_div: id_div
			});
		}

		controller.OnDecision = function (id_Request, decision)
		{
			var self = this;
			var sel = this.fastening.selector;
			var ajaxurl = this.base_url + '?action=request.resolve';
			ajaxurl += '&id_Manager=' + this.model.id_Manager;
			ajaxurl += '&id_Request=' + id_Request;
			ajaxurl += '&decision=' + decision;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача резолюции на сервер", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.ReloadGrid();
						$(sel).trigger('model_change');
					}
				}
			});
		}

		return controller;
	}
});