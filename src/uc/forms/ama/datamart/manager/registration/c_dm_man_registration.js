﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/registration/e_dm_man_registration.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 660, height: 470 };

		var base_Render = controller.Render;

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.selProfile = !options_arg ? '' : options_arg.sel;
		controller.selCustomerGrid = !options_arg ? '' : options_arg.selCustomerGrid;
		controller.CustomerContract = !options_arg ? '' : options_arg.CustomerContract;

		controller.Render= function(sel)
		{

			base_Render.call(this, sel);
			var self = this;
			if (this.selCustomerGrid) {
				$(sel + ' div.registration-block').css('display', 'none');
				$(sel + ' input[data-fc-selector="manager_new_contract"]').val(self.CustomerContract);
				$(sel + ' input[data-fc-selector="manager_new_contract"]').attr('disabled', 'disabled');
				$(sel + ' input[data-fc-selector="manager_contract_password"]').parent().remove();
				$(sel + ' button.unregister-enabled, ' + sel + ' button.unregister-disabled').html('Выписать из договора');
				$(sel + ' button.register').html('Прописать в договор');

				if (this.model.ContractNumberToChange) {
					$(sel + ' div.registration-block').css('display', 'block');
					$(sel + ' div.item-decline').css('display', 'block');
				}
			}
			$(sel + ' button.unregister-enabled').click(function () { self.OnUnregister() });
			$(sel + ' button.register').click(function () { self.OnRegister() });
			$(sel + ' button.decline').click(function () { self.OnDecline() });
		}

		

		controller.SetStatus = function (status, number) {
			var sel = this.fastening.selector;
			var self = this;
			$(sel + ' div.cpw-ama-datamart-manager-registration')
				.attr('contract_status', status);
			$(self.selProfile + ' div.cpw-ama-datamart-customer-manager')
				.attr('contract_status', status);
			if (number && status == 'true') {
				$(sel + ' div.cpw-ama-datamart-manager-registration .contract-enabled span')
					.html(number);
				$(self.selProfile + ' div.cpw-ama-datamart-customer-manager .contract-enabled span')
					.html(number);
			}
			if (number && status == 'wait') {
				$(sel + ' div.cpw-ama-datamart-manager-registration .contract-wait span')
					.html(number);
				$(self.selProfile + ' div.cpw-ama-datamart-customer-manager .contract-wait span')
					.html(number);
			}
		}

		controller.OnRegister = function () {
			var self = this;
			var Manager = this.GetFormContent();

			var btnOk = "Да, прописать";
			var confirmText = !this.CustomerContract ? "Вы уверены что хотите прописаться в договор №" + Manager.manager_new_contract + '?' :
				"Вы уверены что хотите прописать в договор №" + Manager.manager_new_contract + ' арбитражного управляющего</br>' + Manager.lastName + ' ' + Manager.firstName + ' ' + Manager.middleName + '?'

			if (self.OkToRegister(Manager)) {
				h_msgbox.ShowModal({
					width: 620
					, title: "Подтверждение операции с пропиской в договор №" + this.model.manager_new_contract
					, html: confirmText
					, buttons: [btnOk, 'Нет, отменить']
					, id_div: "cpw-form-ama-datamart-manager-form-confirm"
					, onclose: function (btn) {
						if (btn == btnOk) {
							self.Register(Manager);
						}
					}
				});
			}
			
		}

		controller.Register = function (Manager) {
			var self = this
			
			var ajaxurl = this.base_url + '?action=manager.contract&cmd=register&id_Manager=' + Manager.id_Manager;
			if (self.CustomerContract) {
				ajaxurl += "&category=contract"
			}
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос прописки АУ в договор", ajaxurl);
			var sel = this.fastening.selector;
			
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: Manager
				, success: function (responce, textStatus) {
					if (null == responce) {
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok) {
						if ((Manager.manager_contract_password && null != Manager.manager_contract_password) ||self.CustomerContract) {
							self.SetStatus('true', Manager.manager_new_contract);
							Manager.ContractNumber = Manager.manager_new_contract;
							Manager.ContractNumberToChange = null;

							if (self.selCustomerGrid) {
								var grid = $(self.selCustomerGrid + ' table.grid');
								var selrow = grid.jqGrid('getGridParam', 'selrow');
								var rowdata = grid.jqGrid('getRowData', selrow);
								rowdata.registration = "Прописан";
								grid.jqGrid('setRowData', selrow, rowdata);
								$(sel + ' div.registration-block').css('display', 'none');
							}
						} else {
							self.SetStatus('wait', Manager.manager_new_contract);
							Manager.ContractNumber = null
							Manager.ContractNumberToChange = Manager.manager_new_contract

							h_msgbox.ShowModal({ title: 'Предупреждение', width: 520, html: "Ваш запрос на прописку в договоре №" + Manager.ContractNumberToChange + " находится на рассмотрении. </br> Пока абонент не подтвердит прописку, вы не имеете доступ к договору." });
						}
						
					}
					else {
						h_msgbox.ShowModal
							({
								title: 'Ошибка выписки из договора	'
								, width: 400
								, html: '<span>Не удалось сохранить свойства АУ по причине:</span><br/> <center><b>\"'
									+ responce.reason + '\"</b></center>'
									+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
							});
					}
				}
			});
		}

		controller.OkToRegister = function (Manager) {
			var self = this;
			var title = "Проверка перед пропиской договора";

			if (Manager.manager_new_contract.length <= 0) {
				h_msgbox.ShowModal({ title: title, width: 410, html: "Поле «Номер договора» обязательное!" });
				return false;
			}
			if (!Manager.manager_new_contract.match(/^\d+/)) {
				h_msgbox.ShowModal({ title: title, width: 410, html: "Поле «Номер договора» должно состоять только из цифр!" });
				return false;
			}
			if (Manager.manager_new_contract == Manager.ContractNumber) {
				h_msgbox.ShowModal({ title: title, width: 410, html: "Арбитражный управляющий уже прописан в данный договор!" });
				return false;
			}
			if (Manager.manager_new_contract == Manager.ContractNumberToChange && !self.CustomerContract && !Manager.manager_contract_password) {
				h_msgbox.ShowModal({ title: title, width: 410, html: "Арбитражный управляющий уже ожидает прописку в данный договор!" });
				return false;
			}
			return true
		}

		controller.OnUnregister = function () {
			var self = this;
			this.model = this.GetFormContent();
			var Manager = this.model;
			var btnOk = "Да, выписать";

			var confirmText = !this.CustomerContract ? "Вы уверены что хотите выписаться из договора №" + Manager.ContractNumber + '?' :
				"Вы уверены что хотите выписать из договора №" + Manager.ContractNumber + ' арбитражного управляющего</br>' + Manager.lastName + ' ' + Manager.firstName + ' ' + Manager.middleName + '?'

			h_msgbox.ShowModal({
				width: 620
				, title: "Подтверждение операции с выпиской из договора №" + this.model.ContractNumber
				, html: confirmText
				, buttons: [btnOk, 'Нет, отменить']
				, id_div: "cpw-form-ama-datamart-manager-form-confirm"
				, onclose: function (btn) {
					if (btn == btnOk) {
						self.Unregister();
					}
				}
			});
		}

		controller.Unregister = function () {
			var self = this;
			var sel = this.fastening.selector;
			var Manager = this.model;
			var ajaxurl = this.base_url + '?action=manager.contract&cmd=unregister&id_Manager=' + Manager.id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача информации об АУ на сервер", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (responce, textStatus) {
					if (null == responce) {
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok) {
						self.SetStatus('false');
						Manager.ContractNumber = null;

						if (self.selCustomerGrid) {
							var grid = $(self.selCustomerGrid + ' table.grid');
							var selrow = grid.jqGrid('getGridParam', 'selrow');
							var rowdata = grid.jqGrid('getRowData', selrow);
							rowdata.registration = "Не прописан";
							grid.jqGrid('setRowData', selrow, rowdata);
							$(sel + ' div.registration-block').css('display', 'block');
						}
					}
					else {
						h_msgbox.ShowModal
							({
								title: 'Ошибка выписки из договора	'
								, width: 400
								, html: '<span>Не удалось выписать из договора АУ по причине:</span><br/> <center><b>\"'
									+ responce.reason + '\"</b></center>'
									+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
							});
					}
				}
			});
		}

		controller.OnDecline = function () {
			var self = this;
			this.model = this.GetFormContent();
			var Manager = this.model;
			var btnOk = "Да, отклонить";

			h_msgbox.ShowModal({
				width: 620
				, title: "Подтверждение операции с выпиской из договора №" + Manager.ContractNumberToChange
				, html: "Вы уверены что хотите отклонить прописку в договор №" + Manager.ContractNumberToChange + " арбитражного управляющего</br>" + Manager.lastName + ' ' + Manager.firstName + ' ' + Manager.middleName + '?'
				, buttons: [btnOk, 'Нет, отменить']
				, id_div: "cpw-form-ama-datamart-manager-form-confirm"
				, onclose: function (btn, dlg_div) {
					if (btn == btnOk) {
						self.Decline();
					}
				}
			});
		}

		controller.Decline = function () {
			var self = this;
			var sel = this.fastening.selector;
			var Manager = this.model;
			var ajaxurl = this.base_url + '?action=manager.contract&cmd=decline&id_Manager=' + Manager.id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача информации об АУ на сервер", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (responce, textStatus) {
					if (null == responce) {
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok) {
						self.SetStatus('false');
						Manager.ContractNumber = null;
						Manager.ContractNumberToChange = null;

						if (self.selCustomerGrid) {
							var grid = $(self.selCustomerGrid + ' table.grid');
							var selrow = grid.jqGrid('getGridParam', 'selrow');
							var rowdata = grid.jqGrid('getRowData', selrow);
							grid.jqGrid('delRowData', selrow, rowdata);
							$(sel).dialog('close')
							$(self.selProfile).dialog('close')
						}
					}
					else {
						h_msgbox.ShowModal
							({
								title: 'Ошибка отклонения прописки'
								, width: 400
								, html: '<span>Не удалось отклонить прописку АУ по причине:</span><br/> <center><b>\"'
									+ responce.reason + '\"</b></center>'
									+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
							});
					}
				}
			});
		}
		return controller;
	}
});