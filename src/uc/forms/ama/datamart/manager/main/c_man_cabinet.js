﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/main/e_man_cabinet.html'
	, 'forms/ama/datamart/manager/procedures/c_dm_man_procedures'
	, 'forms/ama/datamart/procedure/selectable/c_dm_sel_procedure'
	, 'forms/ama/datamart/customer/manager/h_cust_manager'
	, 'forms/ama/datamart/manager/committee/selectable/c_committee_selectable'
	, 'forms/ama/datamart/manager/contacts/c_man_contacts'
	, 'forms/ama/datamart/common/news/c_news'
	, 'forms/ama/datamart/handbook/main/c_dm_handbook'
	, 'forms/ama/datamart/common/schedule/c_dm_schedule'
	, 'forms/ama/datamart/common/push/profile/h_dm_push_profile'
	, 'forms/ama/datamart/manager/assignments/c_dm_man_assignments'
	, 'forms/ama/datamart/manager/proc_control/selectable/c_man_proc_control_selectable'
	, 'forms/ama/datamart/common/google_calendar/login/h_dm_google_calendar_login'
],
function (c_fastened, tpl
	, c_dm_man_procedures
	, c_dm_sel_procedure
	, h_cust_manager
	, c_committee_selectable
	, c_dm_man_contacts
	, c_news
	, c_dm_handbook
	, c_dm_viewer_schedule
	, h_dm_push_profile
	, c_dm_man_assignments
	, c_man_proc_control_selectable
	, h_dm_google_calendar_login
	)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var options = {
			field_spec:
				{
					Процедуры: { controller: function () { return c_dm_man_procedures({ base_url: base_url }); }, render_on_activate:true }
					, Витрина: { controller: function () { return c_dm_sel_procedure({ base_url: base_url }); }, render_on_activate:true }
					, ЗаседанияКК: { controller: function () { return c_committee_selectable({ base_url: base_url }); } }
					, Новости: { controller: function () { return c_news({ base_url: base_url }); }, render_on_activate:true }
					, Контакты: { controller: function () { return c_dm_man_contacts({ base_url: base_url }); }, render_on_activate:true }
					, Справочник: { controller: function () { return c_dm_handbook({ base_url: base_url }); }, render_on_activate:true }
					, Календарь: { controller: function () { return c_dm_viewer_schedule({ base_url: base_url }); }, render_on_activate:true  }
					, Назначения: { controller: function () { return c_dm_man_assignments({ base_url: base_url }); }, render_on_activate:true  }
					, ВРаботе: { controller: function () { return c_man_proc_control_selectable({ base_url: base_url }); }, render_on_activate: true }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.base_url = base_url;
		controller.manager_crud_url = controller.base_url + '?action=manager.ru';

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;

			var c_procedures = this.fastening.get_fc_controller('Процедуры');
			c_procedures.ShowProcedure = function (id_text) { self.ShowProcedure(id_text); }
			if (null != this.model.id_Contract)
			{
				c_procedures.ShowMeeting = function (id_Meeting, meeting_text) { self.ShowMeeting(id_Meeting, meeting_text); }
			}
			

			var id_ManagerCertAuth = this.model.id_ManagerCertAuth;

			$(sel + " .jquery-menu").menu();

			$(sel + " .profile-dropdown").click(function(){
				$(this).find("ul.jquery-menu").toggle();
			})
			
			//close menu
			$(document).on("click", function (e) {
				/*menu buttons*/
				if($(e.target).hasClass("open-profile")) self.OnProfile();
				if($(e.target).hasClass("logout")) { if (self.OnLogout) { self.OnLogout(id_ManagerCertAuth); return false; } }
				if($(e.target).hasClass("notifications")) self.OnNotifications();
				if($(e.target).hasClass("google-calendar-dwnld")) self.OnGoogleCalendar();

				if(!$(e.target).hasClass("profile-dropdown") && !$(e.target).parent().hasClass("profile-dropdown")) {
					$(sel + " .profile-dropdown ul.jquery-menu").hide();
				}
			});

			$(sel + ' ul.ui-tabs-nav > li > a').click(function () { $(window).resize(); });

			this.ProcessSection();
		}

		controller.ProcessSection = function ()
		{
			if (app.open_section && "false"!=app.open_section)
			{
				switch (app.open_section)
				{
					case "sl":
						this.GoToTab('cpw-ama-datamart-manager-main-hndb');
						var sel= this.fastening.selector;
						$(sel + ' .search-button-text').click();
						app.open_section = "false";
						break;
					case 'appointments': 
						this.GoToTab('cpw-ama-datamart-manager-main-assgnmnt');
						var c_assignments = this.fastening.get_fc_controller('Назначения');
						c_assignments.GoToTab('cpw-form-ama-dm-manager-assignments-cnsnts');
						app.open_section = "false";
						break;
					case 'reg-appointment': 
						this.GoToTab('cpw-ama-datamart-manager-main-assgnmnt');
						var c_assignments = this.fastening.get_fc_controller('Назначения');
						c_assignments.GoToTab('cpw-form-ama-dm-manager-assignments-cnsnts');
						break;
					default:
						app.open_section = "false";
				}
			}
		}

		controller.GoToTab = function (id_tab)
		{
			var sel= this.fastening.selector;
			$(sel + ' #cpw-ama-datamart-manager-main-tabs > ul > li > a[href="#' + id_tab + '"]').click();
		}

		controller.ShowProcedure = function (id_text)
		{
			var sel = this.fastening.selector;
			$(sel + ' #cpw-ama-datamart-manager-main-tabs > ul > li > a[href="#cpw-ama-datamart-manager-main-dmrt"]').click();
			var c_datamart = this.fastening.get_fc_controller('Витрина');
			c_datamart.SelectProcedure(id_text);
		}

		controller.ShowMeeting = function (id_Meeting, meeting_text)
		{
			var sel = this.fastening.selector;
			$(sel + ' #cpw-ama-datamart-manager-main-tabs > ul > li > a[href="#cpw-ama-datamart-manager-main-meet"]').click();
			var c_meeting = this.fastening.get_fc_controller('ЗаседанияКК');
			c_meeting.SelectMeeting(id_Meeting, meeting_text);
		}

		controller.OnProfile= function ()
		{
			h_cust_manager.ModalEditManagerProfile(null, null, this.base_url, this.model.id_Manager, null);
		}
		controller.OnNotifications = function () {
			h_dm_push_profile.ModalEditPushProfile(base_url, this.model.id_Manager, 'm');
		}
		controller.OnGoogleCalendar = function () {
			h_dm_google_calendar_login.ModalCalendarLogin(base_url, this.model.id_Manager);
		}
		return controller;
	}
});