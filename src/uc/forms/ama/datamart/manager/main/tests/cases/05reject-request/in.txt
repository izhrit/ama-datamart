include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\committee\location\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Процедуры"

wait_click_text "Контакты"
wait_click_text "Запросившие доступ"

wait_text "Алексеев"

shot_check_png ..\..\shots\requests.png

wait_click_text "Алексеев"
shot_check_png ..\..\shots\request.png

wait_click_text "Отклонить"
wait_text_disappeared "Заявка на доступ"

shot_check_png ..\..\shots\requests_rejected.png

exit