﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/procedure/e_dm_man_procedure.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 650, height: 360 };

		return controller;
	}
});