﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/viewer/e_dm_man_viewer.html'
],
function (c_fastened, tpl)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_select2 = base_url + '?action=procedure.select2&version=1'
		if (options_arg && options_arg.id_Manager)
			base_url_select2 += '&id_Manager=' + options_arg.id_Manager;

		var options = {
			field_spec:
				{
					Доступ_к_процедурам: { multiple: true, ajax: { url: base_url_select2, dataType: 'json' } }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.size = { width: 590, height: 450 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			$(sel + ' input[data-fc-selector="Email"]').focus();
		}

		return controller;
	}
});