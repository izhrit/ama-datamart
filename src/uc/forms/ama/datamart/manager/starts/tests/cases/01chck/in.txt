include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\..\common\start\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

je app.cpw_Now = function () { var res= new Date(2015, 6, 26, 17, 36, 0, 0); if (!app.cpw_Now_seconds && 0!==app.cpw_Now_seconds) app.cpw_Now_seconds= 0; app.cpw_Now_seconds+= 60; res.setSeconds(res.getSeconds() + app.cpw_Now_seconds); return res;};

wait_text "Искать"

wait_text "Показано согласий 2 из 2"
shot_check_png ..\..\shots\initial.png

wait_click_text "СВЕТЛЫЙ ГОРОД"
wait_click_text "Только что"
wait_click_text "Отмена"
wait_click_text "СВЕТЛЫЙ ГОРОД"
wait_value "26.07.2015 17:37"
wait_click_text "Только что"
wait_click_full_text "Сохранить"
wait_click_text "СВЕТЛЫЙ ГОРОД"
wait_value "26.07.2015 17:38"
wait_click_text "Отмена"

wait_click_text "Зарегистрировать согласие"
wait_text "Регистрация согласия"


# Начало заполнения формы создания согласия

play_stored_lines ama_datamart_start_fields_1

# Конец заполнения формы создания согласия

wait_click_text "Только что"
wait_click_text "Отмена"


wait_text "Показано согласий 3 из 3"
wait_click_text "Голохвастов"
wait_value "26.07.2015 17:40"
wait_click_text "Отмена"

wait_click_text "Зарегистрировать согласие"
wait_text "Согласен"
js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", "Абайдуллин");
js wbt.SetModelFieldValue("Должник.Физ_лицо.Имя", "Валерий");
js wbt.SetModelFieldValue("Должник.Физ_лицо.Отчество", "Кимович");
js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Удмуртской');
wait_click_text "Свериться"
wait_text "новая информация"
shot_check_png ..\..\shots\new_info.png
wait_click_text "OK"
shot_check_png ..\..\shots\with_new_info.png
wait_click_text "Отмена"
wait_text Ё34/2021

wait_click_text "Зарегистрировать согласие"
wait_text "Регистрация согласия"


# Начало заполнения формы создания согласия

play_stored_lines ama_datamart_start_fields_2

js wbt.SetModelFieldValue("Показывать_СРО", null);
js wbt.CheckModelFieldValue("Показывать_СРО", null);

# Конец заполнения формы создания согласия

wait_click_text "Свериться"
wait_text "Нет новой"
wait_click_text "OK"
js $('input[data-fc-selector="Время_сверки"]').val()
wait_click_text "Отмена"


wait_text "Показано согласий 5 из 5"

je app.cpw_Now = function () { return new Date(2015, 11, 26, 17, 36, 0, 0); };

wait_click_text "Абайдуллин"
js wbt.SetModelFieldValue("Номер_судебного_дела", '');
js wbt.SetModelFieldValue("Заявление_на_банкротство.Дата_рассмотрения", '');
wait_click_text "Свериться"
wait_text "новая информация"
shot_check_png ..\..\shots\new_info2.png
wait_click_text "OK"
js $('input[data-fc-selector="Время_сверки"]').val()
wait_click_text "Отмена"
wait_text "Показано согласий 5 из 5"

exit