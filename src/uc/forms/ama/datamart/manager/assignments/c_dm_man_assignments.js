define([
	'forms/base/fastened/c_fastened'
, 'tpl!forms/ama/datamart/manager/assignments/e_dm_man_assignments.html'
, 'forms/ama/datamart/manager/starts/c_dm_man_starts'
, 'forms/ama/datamart/manager/mrequests/c_dm_man_mrequests'
, 'forms/ama/datamart/manager/debtors/c_dm_man_debtors'
, 'forms/ama/datamart/common/debtor_full/c_dm_debtor_full'
],
function (c_fastened, tpl, c_dm_man_starts, c_dm_man_mrequests, c_dm_man_debtors, c_dm_debtor_full)
{
return function (options_arg)
{
	var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

	var options = {
		field_spec:
			{
				Согласия: { controller: function () { return c_dm_man_starts({ base_url: base_url }); }, render_on_activate:true }
				, Запросы: { controller: function () { return c_dm_man_mrequests({ base_url: base_url }); }, render_on_activate:true }
				, Должники: { controller: c_dm_man_debtors, render_on_activate:true }
				, Должник: { controller: function () { return c_dm_debtor_full({ base_url: base_url }); }, render_on_activate:true }
			}
	};

	var controller = c_fastened(tpl, options);
	var base_Render = controller.Render;
	controller.Render = function (sel)
	{
		this.PrepareToBaseRender(sel);
		base_Render.call(this, sel);

		var self = this;
		$(sel + ' div#cpw-form-ama-dm-manager-assignments-tabs').on('tabsactivate', function (event, ui) { self.OnActivateTab(event, ui); });
	}

	controller.OnActivateTab = function (event, ui)
	{
		var tab_id= ui.newPanel.attr('id');
		switch (tab_id)
		{
			case 'cpw-form-ama-dm-manager-assignments-cnsnts':
				this.ReloadConsents();
				break;
			case 'cpw-form-ama-dm-manager-assignments-rqsts':
				this.ReloadRequests();
				break;
			case 'cpw-form-ama-dm-manager-assignments-dbtrs':
				this.ReloadDebtors();
				break;
		}
	}

	controller.ReloadConsents = function(){
		var fastening= this.fastening;
		var c_consents= fastening.get_fc_controller('Согласия');
		if(c_consents.fastening)
		c_consents.ReloadGrid();
	}

	controller.ReloadRequests = function(){
		var fastening= this.fastening;
		var c_requests= fastening.get_fc_controller('Запросы');
		if(c_requests.fastening)
			c_requests.ReloadGrid();
	}

	controller.ReloadDebtors = function(){
		var fastening= this.fastening;
		var c_debtors= fastening.get_fc_controller('Должники');
		if(c_debtors.fastening)
			c_debtors.ReloadGrid();
	}

	controller.SelectDebtorInDebtorTab = function (debtorsTabSelector)
	{
		var self = this;
		var grid = $(debtorsTabSelector + ' table.grid');
		var selrow = grid.jqGrid('getGridParam', 'selrow');
		var rowdata = grid.jqGrid('getRowData', selrow);
		$(debtorsTabSelector + " ul.jquery-menu").hide();

		self.GoToTab('cpw-form-ama-dm-manager-assignments-dbtr');

		var c_debtor_full = this.fastening.get_fc_controller('Должник');
		if(c_debtor_full.fastening)
			c_debtor_full.SelectDebtor(rowdata.id_PreDebtor);
	}

	controller.GoToTab = function (id_tab)
	{
		var sel= this.fastening.selector;
		$(sel + ' #cpw-form-ama-dm-manager-assignments-tabs > ul > li > a[href="#' + id_tab + '"]').click();
	}

	controller.PrepareToBaseRender = function (sel)
	{
		var self = this;
		var fspec= this.options.field_spec;

		fspec['Должники'].controller = function () {
			return c_dm_man_debtors({
				base_url: base_url
				, SelectDebtorInDebtorTab: self.SelectDebtorInDebtorTab.bind(self)
			}); 
		}
	}

	return controller;
}
});