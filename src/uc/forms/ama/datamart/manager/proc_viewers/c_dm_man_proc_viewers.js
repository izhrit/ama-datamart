﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/proc_viewers/e_dm_man_proc_viewers.html'
],
function (c_fastened, tpl)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_select2 = base_url + '?action=viewer.select2'
		if (options_arg && options_arg.id_Manager)
			base_url_select2 += '&id_Manager=' + options_arg.id_Manager;

		var options = {
			field_spec:
				{
					Доступ_у_наблюдателей: { multiple: true, ajax: { url: base_url_select2, dataType: 'json' } }
				}
		};

		var controller = c_fastened(tpl, options);
		return controller;
	}
});