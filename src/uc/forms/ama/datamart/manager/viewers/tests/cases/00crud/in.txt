include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\viewer\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Искать"
wait_click_text "Сергеева"
shot_check_png ..\..\shots\00new.png

je $($("div.actions-button")[0]).click();
wait_click_text "Добавить"
shot_check_png ..\..\shots\00add.png
play_stored_lines ama_datamart_viewer_fields_1
shot_check_png ..\..\shots\00add_filled.png
wait_click_text "Отмена"
shot_check_png ..\..\shots\00new.png

je $($("div.actions-button")[0]).click();
wait_click_text "Добавить"
shot_check_png ..\..\shots\00add.png
play_stored_lines ama_datamart_viewer_fields_1
shot_check_png ..\..\shots\00add_filled.png
js wbt.SetModelFieldValue("Email", "x1y.com");
wait_click_text "Сохранить настройки наблюдателя"
wait_text "Проверка"
shot_check_png ..\..\shots\00add_filled_bad_email.png
click_text "OK"
js wbt.SetModelFieldValue("Email", "x@y.");
wait_click_text "Сохранить настройки наблюдателя"
wait_text "Проверка"
shot_check_png ..\..\shots\00add_filled_short_email.png
click_text "OK"
js wbt.SetModelFieldValue("Email", "x3@y.com");
js wbt.SetModelFieldValue("Имя", " ");
wait_click_text "Сохранить настройки наблюдателя"
wait_text "Проверка"
shot_check_png ..\..\shots\00add_filled_empty_name.png
click_text "OK"
wait_click_text "Отмена"
shot_check_png ..\..\shots\00new.png

je $($("div.actions-button")[0]).click();
wait_click_text "Добавить"
shot_check_png ..\..\shots\00add.png
play_stored_lines ama_datamart_viewer_fields_1
shot_check_png ..\..\shots\00add_filled.png
wait_click_text "Сохранить настройки наблюдателя"
shot_check_png ..\..\shots\00added.png

je $("td[title='Петрова П П'] ~ td > div.actions-button").click();
wait_click_text "Редактировать"
check_stored_lines ama_datamart_viewer_fields_1
play_stored_lines ama_datamart_viewer_fields_2
shot_check_png ..\..\shots\00edit_filled.png
wait_click_text "Отмена"
je $('tr.ui-state-highlight').removeClass('ui-state-highlight');
shot_check_png ..\..\shots\00added.png

je $("td[title='Селянин И О']").click();
je $("td[title='Петрова П П'] ~ td > div.actions-button").click();
wait_click_text "Редактировать"
check_stored_lines ama_datamart_viewer_fields_1
play_stored_lines ama_datamart_viewer_fields_2
shot_check_png ..\..\shots\00edit_filled.png
js wbt.SetModelFieldValue("Email", "x1y.com");
wait_click_text "Сохранить настройки наблюдателя"
wait_text "Проверка"
shot_check_png ..\..\shots\00edit_filled_bad_email.png
click_text "OK"
js wbt.SetModelFieldValue("Email", "x@y.");
wait_click_text "Сохранить настройки наблюдателя"
wait_text "Проверка"
shot_check_png ..\..\shots\00edit_filled_short_email.png
click_text "OK"
js wbt.SetModelFieldValue("Email", "x3@y.com");
js wbt.SetModelFieldValue("Имя", " ");
wait_click_text "Сохранить настройки наблюдателя"
wait_text "Проверка"
shot_check_png ..\..\shots\00edit_filled_empty_name.png
click_text "OK"
wait_click_text "Отмена"
je $('tr.ui-state-highlight').removeClass('ui-state-highlight');
shot_check_png ..\..\shots\00added.png

je $("td[title='Селянин И О']").click();
je $("td[title='Петрова П П'] ~ td > div.actions-button").click();
wait_click_text "Редактировать"
check_stored_lines ama_datamart_viewer_fields_1
play_stored_lines ama_datamart_viewer_fields_2
shot_check_png ..\..\shots\00edit_filled.png
wait_click_text "Сохранить настройки наблюдателя"
shot_check_png ..\..\shots\00edited.png

je $("td[title='Еленина Е Е'] ~ td > div.actions-button").click();
wait_click_text "Редактировать"
check_stored_lines ama_datamart_viewer_fields_2
wait_click_text "Отмена"
je $('tr.ui-state-highlight').removeClass('ui-state-highlight');
shot_check_png ..\..\shots\00edited.png

je $("td[title='Селянин И О']").click();
je $("td[title='Еленина Е Е'] ~ td > div.actions-button").click();
shot_check_png ..\..\shots\00edited_selected.png
wait_click_text "Удалить"
shot_check_png ..\..\shots\00delete_confirm.png
wait_click_text "Отмена"
je $("td[title='Еленина Е Е'] ~ td > div.actions-button").click();
shot_check_png ..\..\shots\00edited_selected.png
wait_click_text "Удалить"
shot_check_png ..\..\shots\00delete_confirm.png
wait_click_text "Да, удалить"
wait_click_text "Сергеева"
shot_check_png ..\..\shots\00new.png

je $("td[title='Сергеева Светлана Михайловна (Банк Возрождение)'] ~ td > div.actions-button").click();
wait_click_text "Удалить"
wait_click_text "Да, удалить"

je $("td[title='Алексеев О А (ФНС)'] ~ td > div.actions-button").click();
wait_click_text "Удалить"
wait_click_text "Да, удалить"

shot_check_png ..\..\shots\00deleted.png

exit