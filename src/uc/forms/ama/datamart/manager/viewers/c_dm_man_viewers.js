﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/manager/viewers/e_dm_man_viewers.html'
	, 'forms/ama/datamart/manager/viewer/c_dm_man_viewer'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/ama/datamart/manager/viewers/v_dm_man_viewers_delete_confirm.html'
	, 'forms/base/ql'
	, 'forms/ama/datamart/base/show_msg_under_construction'
],
function (c_fastened, tpl, c_dm_man_viewer, h_msgbox, v_datamart_viewers_delete_confirm, ql, show_msg_under_construction)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();

			var self = this;
			this.AddButtonToPagerLeft('cpw-ama-datamart-viewers-pager', 'Добавить наблюдателя', function () { self.OnAdd(); });
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		controller.base_grid_url = controller.base_url + '?action=viewer.jqgrid';
		controller.base_crud_url = controller.base_url + '?action=viewer.crud';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_Manager=' + this.model.id_Manager;
			return url;
		}

		controller.colModel =
		[
			  { name: 'id_ManagerUser', hidden: true }
			, { label: 'Наблюдатель', name: 'UserName', width: 100 }
			, { label: 'Видимые процедуры', name: 'Procedures', width: 150 }
			, { label: ' ', name: 'myac', width: 20, align: 'right', search: false}
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = self.PrepareUrl();
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка списка наблюдателей...'
				, recordtext: 'Показано наблюдателей {1} из {2}'
				, emptyText: 'Нет наблюдателей для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, pager: '#cpw-ama-datamart-viewers-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, onSelectRow: function () { /*nothing*/ }
				, ondblClickRow: function () { self.OnEdit(); }
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка наблюдателей", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.grid_row_buttons = function ()
		{
			var self = this;
			return [
				{ "class": "edit-viewer", text: "Редактировать", click: function () { self.OnEdit(); } }
				, { "class": "delete-viewer", text: "Удалить", click: function () { self.OnRemove(); }  }
			];
		};

		controller.OkToSave= function(viewer)
		{
			var email = viewer.Email;
			var title = "Проверка перед сохранением";
			if (5>email.length)
			{
				h_msgbox.ShowModal({ title: title, width:410, html: "Длина E-mail должна быть не менее 5 символов!" });
				return false;
			}
			if (-1==email.indexOf('@'))
			{
				h_msgbox.ShowModal({ title: title, width: 410, html: "E-mail должен содержать символ '@'!" });
				return false;
			}
			var name = viewer.Имя.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
			if ('' == name)
			{
				h_msgbox.ShowModal({ title: title, width: 410, html: "Имя не может быть пустым!" });
				return false;
			}
			return true;
		}

		controller.OnAdd= function()
		{
			var sel = this.fastening.selector;
			var self = this;
			var c_viewer = c_dm_man_viewer({ base_url: this.base_url, id_Manager: !this.model ? null : this.model.id_Manager });
			c_viewer.SetFormContent({});
			var btnOk = 'Сохранить настройки наблюдателя';
			h_msgbox.ShowModal
			({
				title: 'Добавление наблюдателя'
				, controller: c_viewer
				, buttons: [btnOk, 'Отмена']
				, id_div:"cpw-form-ama-datamart-viewer"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var viewer = c_viewer.GetFormContent();
						if (!self.OkToSave(viewer))
							return false;
						self.Add(viewer);
					}
				}
			});
		}

		controller.Add= function(viewer)
		{
			var self = this;

			var ajaxurl = this.base_crud_url + '&cmd=add';
			if (this.model)
				viewer.id_Manager = this.model.id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Добавление данных наблюдателя на сервер", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'POST'
				, data: viewer
				, cache: false
				, success: function (responce, textStatus)
				{
					self.ProcessStoredViewer(responce, textStatus, v_ajax);
				}
			});
		}

		controller.ProcessStoredViewer = function (responce, textStatus, v_ajax)
		{
			if (null == responce)
			{
				v_ajax.ShowAjaxError(responce, textStatus);
			}
			else if (true == responce.ok)
			{
				this.ReloadGrid();
				$(this.fastening.selector).trigger('model_change');
			}
			else
			{
				h_msgbox.ShowModal
				({
					title: 'Ошибка сохранения свойств наблюдателя'
					, width: 450
					, html: '<span>Не удалось сохранить свойства наблюдателя по причине:</span><br/> <center><b>\"'
						+ responce.reason + '\"</b></center>'
						+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
				});
			}
		}

		controller.OnEdit = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			$(sel + " ul.jquery-menu").hide();

			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=get&id_Manager=' + this.model.id_Manager + '&id=' + rowdata.id_ManagerUser;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных наблюдателя с сервера", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.EditViewer(rowdata.id_ManagerUser, data);
					}
				}
			});
		}

		controller.EditViewer = function (id, viewer)
		{
			var c_viewer = c_dm_man_viewer({ base_url: this.base_url, id_Manager: !this.model ? null : this.model.id_Manager });
			var self = this;
			c_viewer.SetFormContent(viewer);
			var btnOk = 'Сохранить настройки наблюдателя';
			h_msgbox.ShowModal
			({
				title: 'Редактирование наблюдателя'
				, controller: c_viewer
				, width: 580, height: 450
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-viewer"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var viewer = c_viewer.GetFormContent();
						if (!self.OkToSave(viewer))
							return false;
						self.Update(id, viewer);
					}
				}
			});
		}

		controller.Update = function (id,viewer)
		{
			var sel = this.fastening.selector;
			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=update&id=' + id;
			if (this.model)
				viewer.id_Manager = this.model.id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Сохранение записи о наблюдателе на сервер", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'POST'
				, data: viewer
				, cache: false
				, success: function (responce, textStatus)
				{
					self.ProcessStoredViewer(responce, textStatus, v_ajax);
				}
			});
		}

		controller.OnRemove = function ()
		{
			var sel = this.fastening.selector;

			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;
			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal
			({
				title: 'Подтверждение удаления' //наблюдателей
				, html: v_datamart_viewers_delete_confirm(rowdata)
				, width: 500//, height: 250
				, id_div: "cpw-form-ama-datamart-viewer-delete-confirm"
				, buttons: [btnOk, 'Отмена']
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						self.Remove(rowdata.id_ManagerUser);
					}
				}
			});
		}

		controller.Remove = function (row)
		{
			var sel = this.fastening.selector;
			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=delete&id=' + row;
			if (this.model)
				ajaxurl += '&id_Manager=' + this.model.id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Удаление данных наблюдателя с сервера", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.ReloadGrid();
						$(sel).trigger('model_change');
					}
				}
			});
		}

		return controller;
	}
});