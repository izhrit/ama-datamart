﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/contacts/e_man_contacts.html'
	, 'forms/ama/datamart/manager/viewers/c_dm_man_viewers'
	, 'forms/ama/datamart/manager/requests/c_dm_man_requests'
	, 'forms/ama/datamart/manager/income/c_dm_man_income'
],
function (c_fastened, tpl
	, c_dm_man_viewers
	, c_dm_man_requests
	, c_dm_man_income
	)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var options = {
			field_spec:
				{
					  Наблюдатели: { controller: function () { return c_dm_man_viewers({ base_url: base_url }); }, render_on_activate:true }
					, Запросы: { controller: function () { return c_dm_man_requests({ base_url: base_url }); }, render_on_activate:true }
					, Входящее: { controller: function () { return c_dm_man_income({ base_url: base_url }); }, render_on_activate:true }
				}
		};

		var controller = c_fastened(tpl, options);

		return controller;
	}
});