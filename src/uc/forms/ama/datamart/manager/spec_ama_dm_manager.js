define([
	'forms/base/h_spec'
	, 'forms/ama/datamart/manager/committee/spec_ama_dm_committee'
],
function (h_spec, spec_ama_dm_committee)
{

	var spec_manager= {

		controller: {
			"dm_man_assignments": {
				path: 'forms/ama/datamart/manager/assignments/c_dm_man_assignments'
				, title: 'назначения в кабинете АУ с заявлениями о банкротстве и запросами на кандидатуру АУ'
			}
			,"dm_man_viewer": {
				path: 'forms/ama/datamart/manager/viewer/c_dm_man_viewer'
				, title: 'права наблюдателя на информацию по процедурам'
			}
			, "dm_man_proc_viewers": {
				path: 'forms/ama/datamart/manager/proc_viewers/c_dm_man_proc_viewers'
				, title: 'наблюдатели для процедуры'
			}
			, "dm_man_viewers": {
				path: 'forms/ama/datamart/manager/viewers/c_dm_man_viewers'
				, title: 'наблюдатели АУ'
			}
			, "dm_man_procedures": {
				path: 'forms/ama/datamart/manager/procedures/c_dm_man_procedures'
				, title: 'процедуры АУ'
			}
			, "dm_man_main": {
				path: 'forms/ama/datamart/manager/main/c_man_cabinet'
				, title: 'главная форма АУ'
			}
			, "dm_man_mrequests": {
				path: 'forms/ama/datamart/manager/mrequests/c_dm_man_mrequests'
				, title: 'запросы к АУ на кандидатуру'
			}
			, "dm_man_contacts": {
				path: 'forms/ama/datamart/manager/contacts/c_man_contacts'
				, title: 'контакты АУ'
			}
			, "dm_man_procedure": {
				path: 'forms/ama/datamart/manager/procedure/c_dm_man_procedure'
				, title: 'свойства процедуры'
			}
			, "dm_man_request": {
				path: 'forms/ama/datamart/manager/request/c_dm_man_request'
				, title: 'запрос к АУ на доступ к процедуре'
			}
			, "dm_man_requests": {
				path: 'forms/ama/datamart/manager/requests/c_dm_man_requests'
				, title: 'запросы к АУ на доступ к процедуре'
			}
			, "dm_man_income": {
				path: 'forms/ama/datamart/manager/income/c_dm_man_income'
				, title: 'входящие для АУ'
			}
			, "dm_man_starts": {
				path: 'forms/ama/datamart/manager/starts/c_dm_man_starts'
				, title: 'зпаявления о банкротстве в кабинете АУ'
			}
			, "dm_man_registration": {
				path: 'forms/ama/datamart/manager/registration/c_dm_man_registration'
				, title: 'Прописка АУ'
			}
			, "dm_man_debtors": {
				path: 'forms/ama/datamart/manager/debtors/c_dm_man_debtors'
				, title: 'список должников'
			}
			, "dm_man_proc_control_procedure": {
				path: 'forms/ama/datamart/manager/proc_control/procedure/c_man_proc_control_procedure'
				, title: 'Форма редактирования информации о процедуре'
			}
		}

		, content: {
		  "man_proc_viewers-01sav":                { path: 'txt!forms/ama/datamart/manager/proc_viewers/tests/contents/01sav.json.etalon.txt' }
		, "man_proc_viewers-sample1":              { path: 'txt!forms/ama/datamart/manager/proc_viewers/tests/contents/sample1.json.txt' }
		, "man_viewer-01sav":                      { path: 'txt!forms/ama/datamart/manager/viewer/tests/contents/01sav.json.etalon.txt' }

		, "man_procedure-01sav":                   { path: 'txt!forms/ama/datamart/manager/procedure/tests/contents/01sav.json.etalon.txt' }
		, "man_procedure-sample1":                 { path: 'txt!forms/ama/datamart/manager/procedure/tests/contents/sample1.json.txt' }
		}

	};

	return h_spec.combine(spec_manager, spec_ama_dm_committee);
});