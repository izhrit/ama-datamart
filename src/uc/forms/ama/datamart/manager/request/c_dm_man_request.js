﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/manager/request/e_dm_man_request.html'
	, 'forms/ama/datamart/base/h_Request_state'
],
function (c_fastened, tpl, h_Request_state)
{
	return function ()
	{
		var controller = c_fastened(tpl, { h_Request_state: h_Request_state });

		controller.size = { width: 720, height: 500 };

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.reject').click(function (e)
			{
				e.preventDefault();
				if (self.OnReject)
					self.OnReject();
			});
			$(sel + ' button.approve').click(function (e)
			{
				e.preventDefault();
				if (self.OnApprove)
					self.OnApprove();
			});
		}

		return controller;
	}
});
