﻿define([
	'forms/base/fastened/c_fastened'
	, 'forms/ama/datamart/base/h_show_hide_search'
],
function (c_fastened, h_show_hide_search)
{
	return function (tpl, options)
	{
		var controller = c_fastened(tpl, options);

		controller.BindResizeWindow = function ()
		{
			if (!this.on_resize)
			{
				var sel = this.fastening.selector;
				this.on_resize = function ()
				{
					if ($(document).width() < 700)
					{
						return false;
					}
					else
					{
						var $grid = $(sel + ' table.grid'),
							newWidth = $grid.closest(".ui-jqgrid").parent().width();

						if (newWidth)
							$grid.jqGrid("setGridWidth", newWidth, true);
					}
				};
				this.on_resize();
			}
			$(window).on("resize", this.on_resize);
		}

		var base_Destroy = controller.Destroy;
		controller.Destroy = function ()
		{
			base_Destroy.call(this);
			this.SafeUnbindResize();
			this.SafeUnbindHideMenu();
		}

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.BindResizeWindow();

			h_show_hide_search.BindShowHideSearchButton(sel);
		}

		controller.AddButtonToPagerLeft = function (id_pager, text, handler)
		{
			$("<button>", {
				"class": "button",
				text: text,
				click: handler
			})
				.prependTo($('#' + id_pager + ' td#' + id_pager + '_left'));
		}

		controller.SafeUnbindResize = function ()
		{
			if (this.on_resize)
			{
				$(window).off("resize", this.on_resize);
				controller.on_resize = null;
				delete controller.on_resize;
			}
		}

		controller.SafeUnbindHideMenu = function ()
		{
			if (controller.hide_menu)
			{
				$(document).off("click mouseleave", controller.hide_menu);
				controller.hide_menu = null;
				delete controller.hide_menu;
			}
		}

		controller.RenderRowActions = function (grid)
		{
			if (this.grid_row_buttons)
			{
				var buttons = this.grid_row_buttons();
				var sel = this.fastening.selector;
				var grid = $(sel + ' table.grid');

				if(!$(sel + " .jquery-menu").length){
					var build_menu = function ()
					{
						var ul = $("<ul>", { "class": "jquery-menu" });
						ul.append($("<li>", { "class": "arrow" }));
						for (var i = 0; i < buttons.length; i++)
							ul.append($("<li>", buttons[i]));

						ul.append($("<li>", { "class": "arrow-down" }));
						return ul;
					}

					$(sel).append(build_menu());
					$(sel + " .jquery-menu").menu();
				}

				var self = this;
				$(grid).find("tbody tr.jqgrow td:last-child")
					.each(function ()
					{
						$("<div>", {
							"class": "actions-button",
							click: function (ev)
							{
								$(sel + " ul.jquery-menu").hide();

								var $this = $(this);

								var $parent = $(sel + " ul.jquery-menu").closest('.ui-jqgrid-btable');
								if (0 == $parent.length)
								{
									var $thisPosition = $this.offset();
									var $thisHeight = $this.height();
									var $thisWidth = $this.width();

									var menuWidth = $(sel + " ul.jquery-menu").width();
									var menuMarginLeft = 18;

									var top = $thisPosition.top + $thisHeight;
									var left = $thisPosition.left + ($thisWidth/2) - menuWidth + menuMarginLeft;

									$(sel + " ul.jquery-menu").css({ "top": top, "left": left, "position": "fixed" })
								}
								else
								{
									var parentPosition = $parent.offset();

									var thisPosition = $this.offset();
									var thisHeight = $this.height();
									var thisWidth = $this.width();

									var $menu = $parent.find('.jquery-menu');
									var menuWidth = $menu.width();
									var menuHeight = $menu.outerHeight();
									var menuMarginLeft = 18;

									var windowHeight = $(window).height();
									var bottomSize = windowHeight - thisPosition.top;

									if (bottomSize < menuHeight)
									{
										$('li.arrow').hide();
										$('li.arrow-down').show();

										var top = thisPosition.top + thisHeight - menuHeight - parentPosition.top;
										var left = thisPosition.left + (thisWidth / 2) - menuWidth + menuMarginLeft - parentPosition.left;
									} else
									{
										$('li.arrow').show();
										$('li.arrow-down').hide();

										var top = thisPosition.top + thisHeight + (menuHeight / 2) - parentPosition.top;
										var left = thisPosition.left + (thisWidth / 2) - menuWidth + menuMarginLeft - parentPosition.left;
									}

									$(sel + " ul.jquery-menu").css({ "top": top, "left": left, "position": "absolute" })
								}
								$(sel + " ul.jquery-menu").show();

								self.SafeUnbindHideMenu();
								controller.hide_menu = function (e)
								{
									if (!$(e.target).hasClass("actions-button"))
									{
										$(sel + " ul.jquery-menu").hide();
										self.SafeUnbindHideMenu();
									}
								}
								$(document).on("click mouseleave", controller.hide_menu);
							}
						})
						.prependTo($(this))
					});
			}
		}

		controller.onSelectRow_if_no_menu = function(id,s,e,f)
		{
			if (!$(e.target).hasClass('actions-button'))
				f(id,s,e);
		}

		controller.ReloadGrid = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.trigger('reloadGrid');
		}

		return controller;
	}
});