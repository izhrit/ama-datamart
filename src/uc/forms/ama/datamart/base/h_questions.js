﻿define(['forms/ama/datamart/base/h_procedure_types']
,function (h_procedure_types)
{
	var helper = {};

	helper.Варианты_ответов_формы1 = ['За', 'Против', 'Воздержался'];

	helper.Начальная_повестка_комитета_кредиторов= function(procedure_type_short, debtorName)
	{
		var proc_info = h_procedure_types.procedure_types_by_short[procedure_type_short];
		var Такого_то_управляющего_о_том_то = proc_info.Управляющий.Какого.toLowerCase() + " управляющего";
		if ('КП' == proc_info.short)
			Такого_то_управляющего_о_том_то += ' ' + debtorName + ' о своей деятельности';
		return [{
			"В_повестке": "Отчёт " + Такого_то_управляющего_о_том_то,
			"На_голосование": {
				"Формулировка": "Принять к сведению отчет " + Такого_то_управляющего_о_том_то,
				"Форма_бюллетеня": "f1"
			}
		}];
	}

	return helper;
})