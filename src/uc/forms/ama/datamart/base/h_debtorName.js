﻿define(function ()
{
	var helper = {};

	helper.changes = [
		{ prefix: "общество с ограниченной ответственностью", change_to: "ООО" }
		, { prefix: "открытое акционерное общество", change_to: "ОАО" }
		, { prefix: "индивидуальный предприниматель", change_to: "ИП" }
		, { prefix: "публичное акционерное общество", change_to: "ПАО" }
		, { prefix: "закрытое акционерное общество", change_to: "ЗАО" }
		, { prefix: "акционерное общество", change_to: "АО" }
		, { prefix: "управление федеральной налоговой службы", change_to: "УФНС" }
		, { prefix: "инспекция федеральной налоговой службы россии", change_to: "ИФНС" }
		, { prefix: "инспекция федеральной налоговой службы", change_to: "ИФНС" }
		, { prefix: "государственное казенное учреждение", change_to: "ГКУ" }
		, { prefix: "управляющая компания", change_to: "УК" }
		
	];

	helper.beautify_debtorName= function(debtorName)
	{
		if (debtorName && null != debtorName && debtorName.length)
		{
			for (var i = 0; i < this.changes.length; i++)
			{
				var change = this.changes[i];
				if (change.prefix.length < debtorName.length)
				{
					var debtorPrefix = debtorName.substring(0, change.prefix.length).toLowerCase();
					if (debtorPrefix == change.prefix)
					{
						return change.change_to + debtorName.substring(change.prefix.length);
					}
				}
			}
		}
		return debtorName;
	}

	var excludePrefix = function (txt, prefix)
	{
		if (prefix.length < txt.length)
		{
			var txtPrefix = txt.substring(0, prefix.length).toLowerCase();
			if (txtPrefix == prefix)
				return txt.substring(prefix.length);
		}
		return txt;
	}

	var excludePrefix_and_trim = function (txt, prefix)
	{
		var res= excludePrefix(txt, prefix);
		if (res.length!=txt.length)
			res= $.trim(res);
		return res;
	}

	helper.beautify_debtorName_extra_short = function (debtorName)
	{
		var res= $.trim(debtorName);
		if (res && null != res && res.length)
		{
			for (var i = 0; i < this.changes.length; i++)
			{
				var change = this.changes[i];
				res= excludePrefix_and_trim(res,change.prefix);
				res= excludePrefix_and_trim(res,change.change_to.toLowerCase());
			}
		}
		res= $.trim(res);
		if (res.length > 2)
		{
			if ('"'==res.charAt(0) && '"'==res.charAt(res.length-1))
				res= $.trim(res.substring(1,res.length-1));
		}
		if (res.length > 2)
		{
			if ('«'==res.charAt(0) && '»'==res.charAt(res.length-1))
				res= $.trim(res.substring(1,res.length-1));
		}
		return res;
	}

	return helper;
})