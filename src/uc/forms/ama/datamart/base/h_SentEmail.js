﻿define(['forms/base/h_md5']
, function (h_md5)
{
	var helper = {};

	helper.RecipientType_description = [
		  { code: 'm', table: 'Manager', descr: 'АУ' }
		, { code: 'v', table: 'MUser', descr: 'Наблюдатель' }
		, { code: 'c', table: 'Vote', descr: 'Голос КК' }

		, { code: 'a', table: 'AwardVote', descr: 'Голос за АУ года' }
		, { code: 'p', table: 'Pushed_event', descr: 'Подп. соб. ЕФРСБ' }
		, { code: 's', table: 'Pushed_message', descr: 'Подп. об. ЕФРСБ' }
		, { code: 'e', table: 'efrsb_manager', descr: 'АУ без дог.' }
		, { code: 'g', table: 'GCPushedEvent' }
		, { code: 'd', table: 'PreDebtor' }
	];

	helper.RecipientType_description_byCode = {};
	helper.RecipientType_description_byTable = {};
	for (var i = 0; i<helper.RecipientType_description.length; i++)
	{
		var d = helper.RecipientType_description[i];
		helper.RecipientType_description_byTable[d.table] = d;
		helper.RecipientType_description_byCode[d.code] = d;
	}

	helper.EmailType_descriptions = [
		  { table: 'Manager', code: 'p', descr: 'смена пароля АУ' }
		, { table: 'Manager', code: 'b', descr: 'блокирование АУ' }
		, { table: 'Manager', code: 'd', descr: 'смена пароля АУ через сро' }
		, { table: 'Manager', code: 'y', descr: 'уведомление о запросах в сро' }

		, { table: 'efrsb_manager', code: 'q', descr: 'создание АУ через сро' }

		, { table: 'MUser', code: 'c', descr: 'создание наблюдателя' }
		, { table: 'MUser', code: 'm', descr: 'смена email наблюдателя' }
		, { table: 'MUser', code: 'h', descr: 'информация об экспозиции' }

		, { table: 'Vote', code: 'n', descr: 'о заседании КК', Наименование_документа: 'Уведомление о заседании', FileName_prefix: 'N' }
		, { table: 'Vote', code: 'a', descr: 'о начале подписания КК', Наименование_документа: 'Уведомление о голосовании', FileName_prefix: 'I' }
		, { table: 'Vote', code: 's', descr: 'о подписании КК', Наименование_документа: 'Уведомление о подписи', FileName_prefix: 'G' }
		, { table: 'Vote', code: 'r', descr: 'об итогах КК', Наименование_документа: 'Уведомление об итогах', FileName_prefix: 'R' }
		, { table: 'Vote', code: 'f', descr: 'КК не состоялось', Наименование_документа: 'Уведомление о несостоявшемся', FileName_prefix: 'F' }
		, { table: 'Vote', code: 'o', descr: 'КК приостановлено', Наименование_документа: 'Уведомление о приостановке', FileName_prefix: 'P' }
		, { table: 'Vote', code: 't', descr: 'КК продолжено', Наименование_документа: 'Уведомление о продолжении', FileName_prefix: 'T' }
		, { table: 'Vote', code: 'z', descr: 'КК продолжено без вас', Наименование_документа: 'Уведомление о блокировании', FileName_prefix: 'Z' }
		, { table: 'Vote', code: 'l', descr: 'КК отменено', Наименование_документа: 'Уведомление об отмене', FileName_prefix: 'L' }

		, { table: 'AwardVote', code: 'w', descr: 'голос за премию АУ' }
		, { table: 'Pushed_event', code: 'e', descr: 'уведомление о событии' }
		, { table: 'Pushed_message', code: 'g', descr: 'уведомление о публикации на ЕФРСБ' }
		, { table: 'GCPushedEvent', code: 'x', descr: 'уведомление о событии из google календаря' }
		, { table: 'PreDebtor', code: 'k', descr: 'предоставление доступа к кабинету должника' }
	];

	helper.EmailType_description_byCode = {};
	helper.EmailType_description_byDescr = {};
	for (var i = 0; i<helper.EmailType_descriptions.length; i++)
	{
		var d = helper.EmailType_descriptions[i];
		helper.EmailType_description_byCode[d.code] = d;
		helper.EmailType_description_byDescr[d.descr] = d;
	}

	helper.PrepareRow= function(descr, row)
	{
		if (!this.EmailType_description_byDescr[descr])
			throw 'неизвестный тип письма "' + descr + '"';
		var ed = this.EmailType_description_byDescr[descr];

		row.RecipientType= this.RecipientType_description_byTable[ed.table].code;
		row.EmailType = ed.code;

		if (row.Body)
		{
			if (!row.ExtraParams)
				row.ExtraParams = {};
			row.ExtraParams.Body = row.Body;
			delete row.Body;
		}

		return row;
	}

	var time_to_fname = function (t)
	{
		var res = '';
		for (var i = 0; i < t.length; i++)
		{
			var c = t.charAt(i);
			if (-1 !== '0123456789'.indexOf(c))
				res += c;
		}
		return res;
	}

	helper.Запись_о_документе= function(row_SentEmail)
	{
		var ed = this.EmailType_description_byCode[row_SentEmail.EmailType];
		var запись= {
			Время: row_SentEmail.TimeDispatch
			, Наименование: ed.Наименование_документа
			, Файл: !row_SentEmail.TimeSent ? '' :
				row_SentEmail.FileName ? row_SentEmail.FileName :
				ed.FileName_prefix + "."
				+ time_to_fname(row_SentEmail.TimeSent)
				+ ".eml"
			, id_SentEmail: row_SentEmail.id_SentEmail
		}

		if (row_SentEmail.Message)
		{
			запись.Размер= row_SentEmail.Message.length;
			запись.doc_md5 = h_md5(row_SentEmail.Message);
		}
		if (row_SentEmail.Размер)
			запись.Размер = row_SentEmail.Размер;
		if (row_SentEmail.doc_md5)
			запись.doc_md5 = row_SentEmail.doc_md5;
		return запись;
	}

	helper.IsFor= function(row_SentEmail,table,id)
	{
		var d = this.RecipientType_description_byTable[table];
		return d.code == row_SentEmail.RecipientType
			&& id == row_SentEmail.RecipientId;
	}

	return helper;
})