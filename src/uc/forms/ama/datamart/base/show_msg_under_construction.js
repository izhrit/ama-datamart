﻿define([
	'forms/base/h_msgbox'
]
, function (h_msgbox)
{
	return function()
	{
		h_msgbox.Show({
			title: "Сообщение о стадии разработки"
			, html: "<center>Данный раздел находится в состоянии разработки и в настоящий момент не готов к использованию.</center>"
			, width: 450, height: 200
		});
	}
});