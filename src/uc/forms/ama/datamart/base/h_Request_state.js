﻿define(function ()
{
	var helper = {};

	helper.variant = [
		  { code: 'a', Состояние: { доступа: 'запрошено', запроса: 'на рассмотрении' } }
		, { code: 'r', Состояние: { доступа: 'отказано', запроса: 'в доступе отказано' } }
		, { code: 'p', Состояние: { доступа: 'открыто', запроса: 'доступ предоставлен' } }
	];

	helper.byCode = {};
	for (var i = 0; i < helper.variant.length; i++)
	{
		var d = helper.variant[i];
		helper.byCode[d.code] = d;
	}

	return helper;
})