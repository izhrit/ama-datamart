define(function ()
{
	var helper = {}

	helper.dn_names_to_fix = {
		'OID.1.2.643.100.3':'���'
		,'OID.1.2.840.113549.1.9.2':'UN'
	}

	helper.GetDNFields = function (text)
	{
		var res = {};
		var parts= text.split(',');

		for (var i = 0; i < parts.length; i++)
		{
			var part= parts[i];
			var name_value= part.split('=');

			var name= name_value[0];
			var b= 0;
			while (b<(name.length-1) && ' '==name.charAt(b))
				b++;
			var e= name.length-1;
			while (0<=e && ' '==name.charAt(e))
				e--;
			name= name.substring(b,e+1);
			var value= name_value[1];

			var fixed_name= this.dn_names_to_fix[name];
			if (fixed_name)
				name= fixed_name;

			res[name]= value;
		}

		return res;
	}

	return helper;
});