﻿define(['forms/base/h_md5']
, function (h_md5)
{
	var helper = {};

	helper.Vote_document_type_description = [
		  { code: 'b', descr: 'Бюллетень', Наименование_документа: 'Бюллетень голосования', FileName_prefix: 'B', FileName_extension: 'txt' }
		, { code: 's', descr: 'Подпись', Наименование_документа: 'Подпись участника под бюллетенем', FileName_prefix: 'S', FileName_extension: 'txt' }
		, { code: 'r', descr: 'ПодписьОператора', Наименование_документа: 'Подпись ИС «Витрина данных ПАУ»', FileName_prefix: 'O', FileName_extension: 'sig' }
	];

	helper.Vote_document_type_description_byCode = {};
	helper.Vote_document_type_description_byDescr = {};
	for (var i = 0; i < helper.Vote_document_type_description.length; i++)
	{
		var d = helper.Vote_document_type_description[i];
		helper.Vote_document_type_description_byCode[d.code] = d;
		helper.Vote_document_type_description_byDescr[d.descr] = d;
	}

	helper.Запись_о_документе= function(row_Vote_document)
	{
		var запись = {
			Время: row_Vote_document.Vote_document_time
			, Файл: row_Vote_document.FileName
			, id_Vote_document: row_Vote_document.id_Vote_document
		}
		if (row_Vote_document.Body)
		{
			запись.doc_md5 = h_md5(row_Vote_document.Body);
			запись.Размер= row_Vote_document.Body.length;
		}
		if (row_Vote_document.doc_md5)
			запись.doc_md5 = row_Vote_document.doc_md5;
		if (row_Vote_document.Размер)
			запись.Размер = row_Vote_document.Размер;
		if (!this.Vote_document_type_description_byCode[row_Vote_document.Vote_document_type])
		{
			запись.Наименование = 'Документ неизвестного типа "' + row_Vote_document.Vote_document_type + '"';
		}
		else
		{
			var d = this.Vote_document_type_description_byCode[row_Vote_document.Vote_document_type];
			запись.Наименование = d.Наименование_документа;
		}
		return запись;
	}

	var time_to_fname = function (t)
	{
		var res = '';
		for (var i = 0; i < t.length; i++)
		{
			var c = t.charAt(i);
			if (-1 !== '0123456789'.indexOf(c))
				res += c;
		}
		return res;
	}

	helper.PrepareRow = function (descr, row_Vote_document)
	{
		if (!this.Vote_document_type_description_byDescr[descr])
			throw 'неизвестный тип документа "' + descr + '"';
		var d = this.Vote_document_type_description_byDescr[descr];

		row_Vote_document.FileName = d.FileName_prefix
			+ "." + time_to_fname(row_Vote_document.Vote_document_time)
			+ "." + d.FileName_extension;
		row_Vote_document.Vote_document_type = d.code;

		return row_Vote_document;
	}

	return helper;
})