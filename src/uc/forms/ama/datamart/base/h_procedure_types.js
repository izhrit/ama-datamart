﻿define(function ()
{
	var helper = {};

	helper.procedure_types_array = [
		{
			db: 'n', short: 'Н', text: 'Наблюдение'
			, Управляющий: { Сокращение: "ВУ", Какой: "Временный" , Какого: "Временного" }
		}
		, {
			db: 'k', short: 'КП', text: 'Конкурсное производство'
			, Управляющий: { Сокращение: "КУ", Какой: "Конкурсный", Какого: "Конкурсного" }
		}
		, {
			db: 'i', short: 'РИ', text: 'Реализация имущества'
			, Управляющий: { Сокращение: "ФУ", Какой: "Финансовый", Какого: "Финансового" }
		}
		, {
			db: 'd', short: 'РД', text: 'Реструктуризация долгов'
			, Управляющий: { Сокращение: "ФУ", Какой: "Финансовый", Какого: "Финансового" }
		}
		, {
			db: 'v', short: 'ВУ', text: 'Внешнее управление'
			, Управляющий: { Сокращение: "ВУ", Какой: "Внешний", Какого: "Внешнего" }
		}
		, {
			db: 'o', short: 'ОД', text: 'Конкурсное производство для отсутствующего должника'
			, Управляющий: { Сокращение: "КУ", Какой: "Конкурсный", Какого: "Конкурсного" }
		}
	];

	helper.procedure_types_by_db_value = {};
	helper.procedure_types_by_short= {};
	for (var i = 0; i < helper.procedure_types_array.length; i++)
	{
		var rec = helper.procedure_types_array[i];
		helper.procedure_types_by_db_value[rec.db] = rec;
		helper.procedure_types_by_short[rec.short] = rec;
	}

	helper.SafeShortForDBValue = function (db_value)
	{
		return !this.procedure_types_by_db_value[db_value]
			? db_value
			: this.procedure_types_by_db_value[db_value].short;
	}

	helper.SafeTextForDBValue = function (db_value)
	{
		return !this.procedure_types_by_db_value[db_value]
			? db_value
			: this.procedure_types_by_db_value[db_value].text;
	}

	helper.PrepareText= function(row)
	{
		var res = (row.debtorName) ? row.debtorName : row.d.Name;
		if (row.procedure_type)
		{
			res += ', ' + this.SafeShortForDBValue(row.procedure_type);
		}
		else if (row.mp)
		{
			res += ', ' + this.SafeShortForDBValue(row.mp.procedure_type);
		}
		return res;
	}

	helper.PrepareSelect2= function(row)
	{
		var id;
		if (row.id_MProcedure) {
			id = row.id_MProcedure
		} else if (row.mp) {
			id = row.mp.id_MProcedure
		}
		if (row.id_Debtor_Manager) {
			id = 'e' + row.id_Debtor_Manager
		} else if (row.edm) {
			id = 'e' + row.edm.id_Debtor_Manager
		}
		if (row.id_Request) {
			id = 'r' + row.id_Request
		} else if (row.rq) {
			id = 'r' + row.rq.id_Request
		}
		return { id: id, text: this.PrepareText(row) };
	}

	return helper;
})