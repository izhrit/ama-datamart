﻿define(function ()
{
	var helper = {};

	helper.Meeting_State_descriptions = [
		  { code: 'a', descr: 'Планирование',   Readonly: false, can: { start: true,  pause: false, stop: false } }
		, { code: 'b', descr: 'Голосование',    Readonly: true,  can: { start: false, pause: true,  stop: true } }
		, { code: 'c', descr: 'Приостановлено', Readonly: false, can: { start: true,  pause: false, stop: true } }
		, { code: 'd', descr: 'Завершено',      Readonly: true,  can: { start: false, pause: false, stop: false } }
		, { code: 'e', descr: 'Состоялось',     Readonly: true,  can: { start: false, pause: false, stop: false } }
		, { code: 'f', descr: 'Не состоялось',  Readonly: true,  can: { start: false, pause: false, stop: false } }
		, { code: 'g', descr: 'Отменено',       Readonly: true,  can: { start: false, pause: false, stop: false } }
	];

	helper.Meeting_State_description_byCode = {};
	helper.Meeting_State_description_byDescr = {};
	for (var i = 0; i < helper.Meeting_State_descriptions.length; i++)
	{
		var d = helper.Meeting_State_descriptions[i];
		helper.Meeting_State_description_byCode[d.code] = d;
		helper.Meeting_State_description_byDescr[d.descr] = d;
	}

	return helper;
})