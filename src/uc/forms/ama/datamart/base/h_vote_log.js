﻿define(function ()
{
	var helper = {};

	helper.vote_log_type_array = [
		  { code: 'n', descr: 'Начало голосования', text: 'Уведомление о заседании на адрес' } /* ау */
		, { code: 's', descr: 'Подписан бюллетень', text: 'Подпись бюллетеня на адрес' }
		, { code: 'a', descr: 'Выбран ответ',       text: 'Выбран ответ для вопроса' }
		, { code: 'c', descr: 'Отправлен код',      text: 'Отправлен код подтверждения на номер' }
		, { code: 'b', descr: 'Письмо о подписи',   text: 'Отправление бюллетеня на адрес' }
		, { code: 'l', descr: 'Вход в кабинет',     text: 'Осуществлён вход в кабинет с IP ' }
		, { code: 'r', descr: 'Итоги заседания',    text: 'Уведомление об итогах заседания на адрес' } /* ау */
		, { code: 'f', descr: 'Не состоялось',      text: 'Уведомление, заседание НЕ состоялось, на адрес' } /* ау */
		, { code: 'е', descr: 'Приостановлено',     text: 'Уведомление о приостановке голосования на адрес' } /* ау */
		, { code: 't', descr: 'Продолжено',         text: 'Уведомление о продолжении голосования на адрес' } /* ау */
		, { code: 'z', descr: 'Продолжено без вас', text: 'Уведомление о блокировании кабинета на адрес' } /* ау */
		, { code: 'o', descr: 'Отменено',           text: 'Уведомление об отмене на адрес' } /* ау */
		, { code: 'w', descr: 'Неверный код',       text: 'Введён неверный код подтверждения' }
		, { code: 'd', descr: 'Код прислан поздно', text: 'Слишком поздно введён код подтверждения' }
	];

	helper.byCode = {};
	for (var i = 0; i < helper.vote_log_type_array.length; i++)
	{
		var t = helper.vote_log_type_array[i];
		helper.byCode[t.code] = t;
	}

	helper.text_by_code_body= function(code, body)
	{
		var res = (this.byCode[code]) ? this.byCode[code].text : 'случилось событие закодированное как "' + code + '"'
		if (body)
			res += ' ' + body;
		return res;
	}

	helper.Запись_о_событии= function(row)
	{
		return {
			Время: row.Vote_log_time
			, Событие: this.text_by_code_body(row.Vote_log_type, row.body)
		};
	}

	return helper;
})