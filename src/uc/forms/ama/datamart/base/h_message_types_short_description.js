﻿define([
	'forms/ama/datamart/base/h_MessageType.etalon'
],
function (h_MessageType)
{
	var MessageInfo_MessageType_desciptions_readable_short_by_db_value = {};
	for (var i= 0; i < h_MessageType.length; i++)
	{
		var darr= h_MessageType[i];
		var db_value= darr[0];
		var Readable_short_about= darr[2];
		MessageInfo_MessageType_desciptions_readable_short_by_db_value[db_value]= Readable_short_about;
	}

	return MessageInfo_MessageType_desciptions_readable_short_by_db_value;
});