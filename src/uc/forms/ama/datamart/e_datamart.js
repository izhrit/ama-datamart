require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl: 'js/libs/tpl',
			css: 'js/libs/css',
			img: 'js/libs/image',
			txt: 'js/libs/txt'
		}
	}
}),

require
(
	[
		  'forms/ama/datamart/main/f_datamart'
		, 'forms/ama/cabinetcc/main/f_cabinetcc'
		, 'forms/ama/award/main/f_award'
		, 'forms/ama/award/admin/main/f_award_admin'
		, 'forms/ama/datamart/common/unsubscribe/f_unsubscribe'
		, 'forms/ama/application/main/f_application'
		, 'forms/ama/application/preview/f_application_preview'
		, 'forms/ama/datamart/manager/committee/from_ama/f_committee_ama'
	],
	function ()
	{
		var extension =
		{
			Title: '���'
			, key: 'ama'
			, forms: {}
		};
		var forms = Array.prototype.slice.call(arguments, 0);
		for (var i = 0; i < forms.length; i++)
		{
			var form = arguments[i];
			extension.forms[form.key] = form;
		}
		if (RegisterCpwFormsExtension)
		{
			RegisterCpwFormsExtension(extension);
		}
		return extension;
	}
);