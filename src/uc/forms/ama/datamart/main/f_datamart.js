define(['forms/ama/datamart/main/c_datamart'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'datamart'
		, Title: 'Витрина данных ПАУ'
	};
	return form_spec;
});
