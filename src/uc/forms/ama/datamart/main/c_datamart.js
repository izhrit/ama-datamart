define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/main/e_datamart.html'
	, 'forms/ama/datamart/login/main/c_login'
	, 'forms/ama/datamart/manager/main/c_man_cabinet'
	, 'forms/ama/datamart/viewer/main/c_dm_viewer_main'
	, 'forms/ama/datamart/customer/main/c_customer_cabinet'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/main/c_sro_cabinet'
],
function (c_fastened, tpl, c_login, c_man_cabinet, c_viewer_cabinet, c_customer_cabinet, h_msgbox, c_sro_cabinet)
{
	return function(options_arg)
	{
		var options = {
			base_url: options_arg && options_arg.base_url ? options_arg.base_url : 'ama/datamart'
			,field_spec:
				{
					ФормаВхода: { controller: function () { return c_login(options_arg); } }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			var login_controller = this.fastening.fc_data[0].controller;
			login_controller.OnLogin = function (login_data) { self.OnLogin(login_data); };
		}

		controller.AutoLogin = function (category, id)
		{
			var login_controller = this.fastening.fc_data[0].controller;
			login_controller.AutoLogin(category, id);
		}

		controller.OnLogin= function(login_data)
		{
			this.OnLogged(login_data);
		}

		controller.OnLogged= function(logged_data)
		{
			var self = this;
			var sel = this.fastening.selector;
			var login_sel = sel + ' > div > div.login';
			$(login_sel).hide();
			$(login_sel + ' input').val('');

			var logged_sel = sel + ' > div > div.logged';
			$(logged_sel).show();
			if (logged_data.КабинетАУ)
			{
				this.logged_controller = c_man_cabinet({ base_url: this.fastening.options.base_url });
				this.logged_controller.SetFormContent(logged_data.КабинетАУ);
			}
			else if (logged_data.Витрина)
			{
				this.logged_controller = c_viewer_cabinet({ base_url: this.fastening.options.base_url });
				this.logged_controller.SetFormContent(logged_data.Витрина);
			}
			else if (logged_data.КабинетАбонента)
			{
				this.logged_controller = c_customer_cabinet({ base_url: this.fastening.options.base_url });
				this.logged_controller.SetFormContent(logged_data.КабинетАбонента);
			}
			else if (logged_data.КабинетСРО)
			{
				this.logged_controller = c_sro_cabinet({ base_url: this.fastening.options.base_url });
				this.logged_controller.SetFormContent(logged_data.КабинетСРО);
			}
			this.logged_controller.OnLogout = function (id_ManagerCertAuth) { self.OnLogout(id_ManagerCertAuth); }
			this.logged_controller.Edit(logged_sel);
		}

		controller.OnLogout = function (id_ManagerCertAuth)
		{
			var sel = this.fastening.selector;

			var logged_sel = sel + ' > div > div.logged';
			/* disable for popover */
			$(document).off('click');
			
			this.logged_controller.Destroy();
			var logged = $(logged_sel);
			logged.html('Сессия закрывается..');
			this.logged_controller = null;

			var on_success = function ()
			{
				logged.hide();
				$(sel + ' > div > div.login').show();
			}
			if (!id_ManagerCertAuth)
			{
				var v_ajax = h_msgbox.ShowAjaxRequest("Завершение сессии на сервере", options.base_url + '?action=logout');
				v_ajax.ajax({ dataType: "json", type: 'GET', cache: false, success: on_success});
			}
			else
			{
				var v_ajax = h_msgbox.ShowAjaxRequest("Завершение сессии на сервере", options.base_url + '?action=manager.cert.login&cmd=logout&id=' + id_ManagerCertAuth);
				v_ajax.ajax({ dataType: "json", type: 'GET', cache: false, success: on_success});
			}
		}

		return controller;
	}
});
