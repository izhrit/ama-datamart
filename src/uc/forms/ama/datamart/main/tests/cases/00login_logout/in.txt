include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

# -----------------------------------------------
wait_click_text "Кабинет АУ"

shot_check_png ..\..\shots\00new_manager.png

wait_click_full_text "Войти"
wait_text "Неудачная аутентификация"
shot_check_png ..\..\shots\00new_bad_login_manager.png
wait_click_text "OK"

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетАУ"] ';
js wbt.SetModelFieldValue("Login", "x@y.com");
js wbt.SetModelFieldValue("Password", "1");
wait_click_full_text "Войти"
js wbt.Model_selector_prefix= '';

je $('.fc-toolbar-title').css('color','white');
je $('.fc-daygrid .fc-daygrid-day-number').css('color','white');
je $('.fc-day-today').removeClass('fc-day-today ');
shot_check_png ..\..\shots\00new_logged_manager.png

je $(".profile-dropdown").click();

wait_click_text "Выйти"

# -----------------------------------------------
wait_click_text "Витрина"

shot_check_png ..\..\shots\00new_viewer.png

wait_click_full_text "Войти"
wait_text "Неудачная аутентификация"
shot_check_png ..\..\shots\00new_bad_login_viewer.png
wait_click_text "OK"
wait_text_disappeared "Неудачная аутентификация"

js wbt.Model_selector_prefix= '[data-fc-selector="Витрина"] ';
js wbt.SetModelFieldValue("Login", "a@o.u");
js wbt.SetModelFieldValue("Password", "1");
wait_click_full_text "Войти"
js wbt.Model_selector_prefix= '';

je $('.fc-toolbar-title').css('color','white');
je $('.fc-daygrid .fc-daygrid-day-number').css('color','white');
je $('.fc-day-today').removeClass('fc-day-today ');
shot_check_png ..\..\shots\00new_logged_viewer.png

je $(".profile-dropdown").click();

wait_click_text "Выйти"

# -----------------------------------------------
wait_click_text "Кабинет абонента ИС ПАУ"

shot_check_png ..\..\shots\00new_customer.png

wait_click_full_text "Войти"

shot_check_png ..\..\shots\00new_bad_login_customer.png

wait_click_text "OK"

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетАбонента"] ';
js wbt.SetModelFieldValue("Login", "1");
js wbt.SetModelFieldValue("Password", "1");

wait_click_full_text "Войти"

je $('.fc-toolbar-title').css('color','white');
je $('.fc-daygrid .fc-daygrid-day-number').css('color','white');
je $('.fc-day-today').removeClass('fc-day-today ');
shot_check_png ..\..\shots\00new_logged_customer.png

je $(".profile-dropdown").click();

wait_click_text "Выйти"

# -----------------------------------------------
wait_click_text "Кабинет СРО АУ"

shot_check_png ..\..\shots\00new_sro.png

wait_click_full_text "Войти"

shot_check_png ..\..\shots\00new_bad_login_sro.png

wait_click_text "OK"

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетСРО"] ';
js wbt.SetModelFieldValue("Login", "s@s.ru");
js wbt.SetModelFieldValue("Password", "1");

wait_click_full_text "Войти"

shot_check_png ..\..\shots\00new_logged_sro.png

je $(".profile-dropdown").click();

wait_click_text "Выйти"

exit