define([
	  'forms/base/h_spec'
	, 'forms/ama/datamart/customer/spec_ama_dm_customer'
	, 'forms/ama/datamart/tests/spec_ama_dm_test'
	, 'forms/ama/datamart/manager/spec_ama_dm_manager'
	, 'forms/ama/datamart/procedure/spec_dm_procedure'
	, 'forms/ama/datamart/viewer/spec_dm_viewer'
	, 'forms/ama/datamart/handbook/spec_dm_handbook'
	, 'forms/ama/datamart/admin/spec_dm_admin'
	, 'forms/ama/datamart/common/spec_dm_common'
	, 'forms/ama/datamart/sro/spec_ama_dm_sro'
	, 'forms/ama/datamart/debtor/spec_dm_debtor'
]
, function (h_spec, customer_spec, test_spec, manager_spec, procedure_spec, viewer_spec, spec_dm_handbook, spec_dm_admin, spec_dm_common, spec_ama_dm_sro, spec_dm_debtor)
{
	return h_spec.combine(
		customer_spec
		, test_spec
		, manager_spec
		, procedure_spec
		, viewer_spec
		, spec_dm_handbook
		, spec_dm_admin
		, spec_dm_common
		, spec_ama_dm_sro
		, spec_dm_debtor
	);
});