define(['forms/ama/datamart/common/choose_efrsb_msg_type/c_choose_efrsb_msg_type'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'choose_efrsb_msg_type'
		, Title: 'Выбор типа сообщения ЕФРСБ'
	};
	return form_spec;
});
