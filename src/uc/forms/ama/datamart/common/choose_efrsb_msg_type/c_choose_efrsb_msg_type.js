define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/choose_efrsb_msg_type/e_choose_efrsb_msg_type.html'
	, 'forms/ama/datamart/base/h_MessageType.etalon'
	, 'forms/ama/datamart/base/h_MessageType_priority'
	, 'forms/ama/datamart/base/h_MessageType_group'
	, 'txt!forms/ama/datamart/common/choose_efrsb_msg_type/s_choose_efrsb_msg_type.css'
],
function (c_fastened, tpl, h_MessageType, h_MessageType_priority, h_MessageType_group, css)
{
	return function ()
	{
		var message_types= [];
		var MessageType_by_db_value = {};
		for (var i = 0; i < h_MessageType.length; i++)
		{
			var darr = h_MessageType[i];
			var db_value = darr[0];
			var Readable= darr[1];
			var Readable_short_about = darr[2];
			var msg_type= {
				db_value:db_value
				,short_name:Readable_short_about
				,full_name:Readable
			};
			message_types.push(msg_type);
			MessageType_by_db_value[db_value] = {
				db_value:db_value
				,Readable_short_about:Readable_short_about
				,Readable:Readable
			};
		}

		message_types.sort(function (m1, m2) { 
			var p1= h_MessageType_priority[m1.db_value];
			if (!p1)
				p1= 0;
			var p2= h_MessageType_priority[m2.db_value];
			if (!p2)
				p2= 0;
			return p1==p2 ? 0 : p1>p2 ? -1 : 1;
		});

		var contains_t= function(arr,t)
		{
			if (arr && arr.length)
			{
				for (var i= 0; i<arr.length; i++)
				{
					var ti= arr[i];
					if (ti==t.db_value || ti==t.short_name || ti==t.full_name)
						return true;
				}
			}
			return false;
		}

		var implemented= function(t,model)
		{
			return !model ? false : contains_t(model.implemented,t);
		}

		var actual= function(t,model)
		{
			return !model ? false : contains_t(model.actual,t);
		}

		var options = {
			css: css
			,implemented:implemented
			,actual:actual
			,message_types: message_types
			,h_MessageType_group: h_MessageType_group
		};

		var controller = c_fastened(tpl, options);

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$input_search= $(sel + ' input.search');
			$input_search.keyup(function () { self.OnSearch(); });
			$input_search.keydown(function () { self.OnSearch(); });
			$input_search.change(function () { self.OnSearch(); });

			$input_search.focus();

			$(sel + ' input.show-tree').change(function () { self.OnChangeListTreeMode(); });
			$(sel + ' input.show-only-actual').change(function () { self.OnChangeShowOnlyActual(); });
			$(sel + ' div.list li.item').click(function (e) { self.OnListItemClick(e); });
			$(sel + ' div.tree div.db-value').click(function (e) { self.OnTreeDBValue(e); });
			$(sel + ' div.tree li').click(function (e) { self.OnListItemClick(e); });
		}

		controller.GetSettings= function()
		{
			var sel = this.fastening.selector;
			var TreeMode= 'checked'==$(sel + ' input.show-tree').attr('checked');
			var ShowOnlyActual= 'checked'==$(sel + ' input.show-only-actual').attr('checked');
			return {TreeMode:TreeMode, ShowOnlyActual:ShowOnlyActual};
		}

		controller.OnListItemClick= function(e)
		{
			var $li= $(e.target).closest('li');
			var db_value= $li.attr('data-db_value');
			if (this.OnChoose)
				this.OnChoose(MessageType_by_db_value[db_value],this.GetSettings());
		}

		controller.OnTreeDBValue= function(e)
		{
			var $div= $(e.target).closest('div.db-value');
			var db_value= $div.attr('data-db_value');
			if (this.OnChoose)
				this.OnChoose(MessageType_by_db_value[db_value],this.GetSettings());
		}

		controller.OnChangeListTreeMode= function()
		{
			var sel = this.fastening.selector;
			var checked= 'checked'==$(sel + ' input.show-tree').attr('checked');
			if (checked)
			{
				$(sel + ' div.list').hide();
				$(sel + ' div.tree').show();
			}
			else
			{
				$(sel + ' div.list').show();
				$(sel + ' div.tree').hide();
			}
		}

		controller.DoSearch= function(q,show_actual)
		{
			var sel = this.fastening.selector;
			$(sel + ' div.list .item').each(function () {
				var $item = $(this);
				var short_name = $item.find(' .short-name').text().toUpperCase();
				var full_name = $item.find(' .full-name').text().toUpperCase();
				if ((-1 != short_name.indexOf(q) || -1 != full_name.indexOf(q))
					&& (!show_actual || 'true'==$item.attr('data-actual') ))
				{
					$item.show();
				}
				else
				{
					$item.hide();
				}
			});
		}

		controller.OnSearch= function()
		{
			var sel = this.fastening.selector;
			if (!this.search)
			{
				this.search= true;
				var q = $(sel + ' input.search').val().toUpperCase();
				var a = 'checked'==$(sel + ' input.show-only-actual').attr('checked');
				while (q != this.searched_query || a!=this.searched_actual)
				{
					this.DoSearch(q,a);
					this.searched_query= q;
					this.searched_actual= a;
					q = $(sel + ' input.search').val().toUpperCase();
					a = 'checked'==$(sel + ' input.show-only-actual').attr('checked');
				}
				this.search= false;
			}
		}

		controller.OnChangeShowOnlyActual= function()
		{
			this.OnSearch();
		}

		return controller;
	}
});