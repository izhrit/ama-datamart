define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/mrequest/e_mrequest.html'
	, 'forms/ama/datamart/common/request_consents/c_request_consents'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
	, 'forms/ama/datamart/common/applicant/c_applicant'
	, 'forms/ama/datamart/common/court_decision/c_dm_court_decision'
	, 'forms/ama/datamart/common/married_couple/c_married_couple'
	, 'tpl!forms/ama/datamart/common/mrequest/v_dm_request_give_consent_confirm.html'
],
function (c_fastened, tpl, c_request_consents, h_msgbox, h_validation_msg, c_applicant, c_court_decision, c_married_couple, v_dm_request_give_consent_confirm)
{
	return function (options_arg) 
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_court_select2 = base_url + '?action=court.select2'
		var base_url_manager_select2 = base_url + '?action=mrequest.manager.select2'
		if (options_arg && options_arg.id_Contract)
			base_url_manager_select2 += '&id_Contract=' + options_arg.id_Contract;

		var options = {
			id_Manager: ((options_arg && options_arg.id_Manager) ? options_arg.id_Manager : null)
			, field_spec:
			{
				'Должник.Супруги':
					{
						controller: function() { return c_married_couple({ readonly: true }) }
						, title: 'Информация о должнике'
					}
				, "Согласия_АУ": { controller: function () { return c_request_consents({ base_url: base_url, readonly: true }) } }
				, "АУ": { ajax: { url: base_url_manager_select2, dataType: 'json' } }
				, "Заявление_на_банкротство.В_суд": 
				{
					placeholder:'Арбитражный суд (обязательно выберите из списка!)'
					, ajax: { url: base_url_court_select2, dataType: 'json' }
				}
				, 'Заявитель':
				{
					controller: function () { return c_applicant({ readonly: true }) }
					, title: 'Информация о заявителе'
					, btn_close_title: 'Закрыть информацию о заявителе'
					, id_div: 'cpw-form-ama-mrequest-applicant'
					, text: function(){}
				}
				, 'Запрос_дополнительная_информация':
				{
					controller: function() { return c_court_decision({ onlyAddInfo: true, readonly: true }) }
					, title: 'Дополнительная информация'
					, readonly: true
					, id_div: 'cpw-form-ama-consent-court-decision-mrequest-au-add-info'	
					, text: function(){ return 'дополнительная информация'; }
				}
			}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			this.PrepareToBaseRender();
			base_Render.call(this, sel);
			var self = this;

			$(sel + ' button.open').click(function () { self.OnOpen(); });
			$(sel + ' .send-consent-to-sro').click(function () { self.OnSendConsentToSRO(); })

			this.PrepareForm();
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var request = base_GetFormContent.call(this);
			var Должник = request.Должник;
			switch (Должник.Тип) {
				case 'Физ_лицо':
					if (Должник.Юр_лицо)
						delete Должник.Юр_лицо;
					if (Должник.Супруги)
						delete Должник.Супруги;
					break;
				case 'Юр_лицо':
					if (Должник.Физ_лицо)
						delete Должник.Физ_лицо;
					if (Должник.Супруги)
						delete Должник.Супруги;
					break;
				case 'Супруги':
					request.Должник = {
						Тип: Должник.Тип,
						Супруги: Должник.Супруги
					}
					break;
			}

			delete request.Запрос_дополнительная_информация;
	
			return request;
		}

		var trim = function (txt) {
			return txt.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
		}

		controller.OnOpen = function ()
		{
			var sel = this.fastening.selector;
			var case_number = $(sel + ' input[data-fc-selector="Номер_судебного_дела"]').val();
			if ('' != trim(case_number))
			{
				window.open('https://kad.arbitr.ru/Card?number=' + case_number);
			}
			else {
				h_msgbox.ShowModal({
					title: 'Отказ открыть страницу'
					, html: '<p>Чтобы открыть страницу дела на kad.arbitr.ru,</p><p style="text-align:right"> нужно указать номер дела..</p>'
					, width: 400, id_div: 'cpw-msg-can-not-open-kad-arbitr'
					, buttons: ["ОК"]
				});
			}
		}

		controller.OnSendConsentToSRO = function () 
		{
			var sel = this.fastening.selector;
			var self = this;
			h_validation_msg.IfOkWithValidateResult(self.Validate(), function ()
			{
				var mrequest = self.GetFormContent();
				var debtorName = ''
				if (mrequest && mrequest.Должник)
				{
					debtorName = self.nameFormatter(mrequest.Должник);
				}
				var btnOk = 'Да, уверен'
				h_msgbox.ShowModal({
					title: 'Подтверждение согласия'
					, buttons: [btnOk, "Отмена"]
					, width: 400, id_div: 'cpw-msg-new-info-from-kad-arbitr'
					, html: v_dm_request_give_consent_confirm({
						managerName: (mrequest && mrequest.АУ) ? mrequest.АУ.text : '',
						debtorName: debtorName || ''
					})
					, onclose: function (btn, dlg_div)
					{
						var id_Manager = null
						if (mrequest.АУ) id_Manager = mrequest.АУ.id
						if (btnOk == btn) {
							self.AddProcedureStart(mrequest.id_MRequest, id_Manager)
							$(sel + ' .select2.au span').html('');
						}
					}
				});

			});
		}

		controller.nameFormatter = function (person) 
		{
			switch (person.Тип) {
				case 'Юр_лицо':
					return person.Юр_лицо.Наименование
				case 'Физ_лицо':
					var name = person.Физ_лицо.Фамилия + ' '
					if (person.Физ_лицо.Имя) { name += person.Физ_лицо.Имя[0] + '.' }
					if (person.Физ_лицо.Отчество) { name += person.Физ_лицо.Отчество[0] + '.' }
					return name
			}
		}

		controller.Validate = function ()
		{
			if (!options_arg || !options_arg.id_Manager)
			{
				var request = this.GetFormContent();
				var vr = [];
				var block = function (msg) { vr.push({ check_constraint_result: false, description: msg }); }
				if (!request.АУ)
					block('Необходимо выбрать арбитражного управляющего для отправки согласия!');

				return 0 == vr.length ? null : vr;
			}
		}

		controller.AddProcedureStart = function (id_MRequest, id_Manager)
		{
			var self = this;
			var ajaxurl = base_url + '?action=start.agree&id=' + id_MRequest;
			if (options_arg && options_arg.id_Contract) ajaxurl += '&id_Contract=' + options_arg.id_Contract;
			if (options_arg && options_arg.id_Manager) ajaxurl += '&id_Manager=' + options_arg.id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка согласия на сервер", ajaxurl);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'POST'
					, data: { id_Manager: id_Manager }
					, success: function (data, textStatus) {
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							if (data.ok)
							{
								self.Reload();
								if (options_arg && options_arg.on_change)
									options_arg.on_change();
							} 
							else 
							{
								h_msgbox.ShowModal({
									title: 'Отказ выполнять отправку согласия'
									, html: data.message
									, width: 450
									, buttons: ["OK"]
									, id_div: 'cpw-form-ama-fail-request-agree'
								});
							}
						}
					}
				});
		}

		controller.Reload = function ()
		{
			var self = this;
			var model = self.GetFormContent();
			var ajaxurl = base_url + '?action=mrequest.crud&cmd=get&id=' + model.id_MRequest;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных запроса кандидатур с сервера", ajaxurl);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus) {
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							var request_consents = self.fastening.get_fc_controller('Согласия_АУ')
							request_consents.SetFormContent(data.Согласия_АУ)
							request_consents.Destroy()
							request_consents.Edit(request_consents.fastening.selector)
						}
					}
				});
		}

		controller.PrepareForm = function()
		{
			var sel = this.fastening.selector;

			if (this.model && this.model.Заявитель)
			{
				$(sel + ' .applicant').html(this.nameFormatter(this.model.Заявитель));
			} 
			else
			{
				$(sel + ' .applicant').hide()
				$(sel + ' .applicant-is-debtor').show()
			}

			if(this.model && this.model.Запрос ) 
			{
				if(this.model.Запрос.Ссылка) {
					$(sel + ' .open-decision').attr("href", this.model.Запрос.Ссылка);
					$(sel + ' .open-decision').html("судебным актом от "+this.model.Запрос.Время.акта);
					$(sel + ' .open-decision').attr("target","_blank");
					$(sel + ' .open-decision').show();
					$(sel + ' .no-decision').hide();
				}
				if(this.model.Запрос.Дополнительная_информация && this.model.Запрос.Дополнительная_информация !== '')
					$(sel + ' .decision-add-info').show();
			}

			$(sel + ' [data-fc-selector="Заявление_на_банкротство.В_суд"]').select2("enable", false);
		}

		controller.PrepareToBaseRender = function (sel)
		{
			var self = this;
			var fspec= this.options.field_spec;
			if(this.model)
				fspec.АУ.ajax.url = fspec.АУ.ajax.url + '&id_SRO=' + this.model.id_SRO;

			fspec.Заявитель.text = function (data) {
				return self.nameFormatter(data);
			}
		}

		return controller;
	}
});