﻿define([
	'forms/base/h_msgbox'
	, 'forms/ama/datamart/common/mrequest/c_mrequest'
	, 'forms/ama/datamart/base/h_debtorName'
],
	function (h_msgbox, c_mrequest, h_debtorName) {
		return function (options_arg) {
			var helper = {};

			if (options_arg) {
				if (options_arg.on_change)
					helper.on_change = options_arg.on_change;
				if (options_arg.selector)
					helper.selector = options_arg.selector;
				if (options_arg.id_Contract)
					helper.id_Contract = options_arg.id_Contract;
				if (options_arg.id_Manager)
					helper.id_Manager = options_arg.id_Manager;
			}
			helper.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
			helper.base_crud_url = helper.base_url + '?action=mrequest.crud';

			helper.width = 920;
			helper.minHeight = 420;

			helper.prepare_controller_mrequest = function (id_SRO) {
				var сc_mrequest = c_mrequest({ 
				  base_url: this.base_url, 
				  id_Contract:this.id_Contract, 
				  id_Manager: this.id_Manager,
				  id_SRO: id_SRO,
				  on_change: this.on_change.bind(this)
				});
				if (this.id_Manager)
					сc_mrequest.id_Manager= this.id_Manager;
				return сc_mrequest;
			}

			helper.OnOpen = function () {
				var grid = $(this.selector + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');
				var rowdata = grid.jqGrid('getRowData', selrow);
				$(this.selector + " ul.jquery-menu").hide();

				var self = this;
				var ajaxurl = this.base_crud_url + '&cmd=get&id=' + rowdata.id_MRequest;
				var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных запроса кандидатур с сервера", ajaxurl);
				v_ajax.ajax
					({
						dataType: "json"
						, type: 'GET'
						, cache: false
						, success: function (data, textStatus) {
							if (null == data) {
								v_ajax.ShowAjaxError(data, textStatus);
							}
							else {
								self.DoOpen(data, rowdata.id_SRO);
							}
						}
					});
			}

			helper.DoOpen = function (request, id_SRO) {
				var self = this;
				var cc_mrequest = this.prepare_controller_mrequest(id_SRO);
				cc_mrequest.SetFormContent(request);
				h_msgbox.ShowModal({
					title: 'Запрос согласия на утверждение АУ в процедуру'
					, controller: cc_mrequest
					, id_div: "cpw-form-ama-datamart-mrequest-read-form"
					, width: self.width
					, minHeight: self.minHeight
					, buttons: ["Закрыть"]
				});
			}

			helper.debtorNameFormatter = function (cellvalue, options, rowObject) {
				var getShortName = function(parts) {
					var res= parts[0];
					if (1 < parts.length)
					{
						for (var i = 1; i < parts.length; i++)
						{
							if(parts[i]) {
								if(i==1) res+= ' ';
								res += parts[i].charAt(0) + '.';
							}
						}
					}
					return res
				}
				if ('n' != rowObject.DebtorCategory)
				{
					return h_debtorName.beautify_debtorName(rowObject.debtorName);
				}
				else
				{
					var res = getShortName(rowObject.debtorName.split(' '));
					if(rowObject.debtorName2)
						res += ', ' +getShortName(rowObject.debtorName2.split(' '));
					return res;
				}
			  };

			helper.consentTextFormatter = function (cellvalue, options, rowObject)
			{
				var amount= rowObject.consents.amount;
				if (0 === amount)
				{
					return "";
				}
				else if (rowObject.consents.ourPosition === null)
				{
					return amount + ((rowObject.consents.amount == 1) ? " уже согласился" : " уже согласились");
				}
				else if (rowObject.consents.ourPosition === 1)
				{
					var res= "1й " + rowObject.consents.managerName;
					if (amount>1)
						res+= " +" + (amount - 1) + " за ним";
					return res;
				}
				else
				{
					var ending =  "";
					var amount = amount.toString();
					if (amount[amount.length-1]>=2 && amount[amount.length-1]<=4)
						ending =  "-х";
					if ((amount[amount.length-1]>=5 && amount[amount.length-1]<=9) || (amount[amount.length-1] == 0 && amount.length<3))
						ending =  "-и";
					return (
						"Ваше " + rowObject.consents.ourPosition + "-е из " + amount + ending
					);
				}
			}

			return helper;
		}
	});