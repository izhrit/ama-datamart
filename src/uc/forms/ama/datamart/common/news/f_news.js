define(['forms/ama/datamart/common/news/c_news'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'news_orpau'
		, Title: 'Новости витрины данных ПАУ для кабинета ОРПАУ'
	};
	return form_spec;
});
