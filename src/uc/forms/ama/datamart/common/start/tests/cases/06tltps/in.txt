include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet
wait_text "Должник:"
je app.cpw_Now= function(){return new Date(2017,1,1,1,1,1); }

je $('.check-kad-arbitr').mouseover();
wait_text "При нажатии произойдет проверка наличия судебного акта о введении процедуры на kad.arbitr.ru"
je $('.check-kad-arbitr').mouseleave();

je $('[data-fc-selector="Время_сверки"]').mouseover();
wait_text "Дата и время последней сверки данных с базой kad.arbitr.ru"
je $('[data-fc-selector="Время_сверки"]').mouseleave();

je $('[data-fc-selector="Заявление_на_банкротство.Дата_подачи"]').mouseover();
wait_text "Дата подачи должником заявления о банкротстве"
je $('[data-fc-selector="Заявление_на_банкротство.Дата_подачи"]').mouseleave();

je $('[data-fc-selector="Заявление_на_банкротство.Дата_рассмотрения"]').mouseover();
wait_text "Дата и время заседания АС по делу о банкротстве"
je $('[data-fc-selector="Заявление_на_банкротство.Дата_рассмотрения"]').mouseleave();

je $('.docs-btn-wrapper').mouseover();
wait_text "Просмотр документов доступен, если Ваша кандидатура представлена в ответе на запрос из суда"
je $('.docs-btn-wrapper').mouseleave();

exit