wait_text "kad.arbitr"
je app.cpw_Now= function(){return new Date(2017,1,1,1,1,1); }
shot_check_png ..\..\shots\00new.png

wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\00new_validation.png
wait_click_text "OK"

js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", "Голохвастов");

wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_no_court.png
wait_click_text "OK"

js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", "");
js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Москвы');

wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_no_lastName.png
wait_click_text "OK"

js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", "Голохвастов");

js wbt.SetModelFieldValue("Должник.ИНН", "123");
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_wrong_inn.png
wait_click_text "OK"
js wbt.SetModelFieldValue("Должник.ИНН", "");

js wbt.SetModelFieldValue("Должник.ОГРН", "123");
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_wrong_ogrn.png
wait_click_text "OK"
js wbt.SetModelFieldValue("Должник.ОГРН", "");

js wbt.SetModelFieldValue("Должник.Физ_лицо.СНИЛС", "123");
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_wrong_snils.png
wait_click_text "OK"
js wbt.SetModelFieldValue("Должник.Физ_лицо.СНИЛС", "");

js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", "123");
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_wrong_family.png
wait_click_text "OK"

js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", "abc");
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_wrong_family2.png
wait_click_text "OK"
js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", "Голохвастов");

js wbt.SetModelFieldValue("Должник.Физ_лицо.Имя", "123");
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_wrong_name.png
wait_click_text "OK"

js wbt.SetModelFieldValue("Должник.Физ_лицо.Имя", "abc");
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_wrong_name2.png
wait_click_text "OK"
js wbt.SetModelFieldValue("Должник.Физ_лицо.Имя", "");

js wbt.SetModelFieldValue("Должник.Физ_лицо.Отчество", "123");
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_wrong_partonimic.png
wait_click_text "OK"

js wbt.SetModelFieldValue("Должник.Физ_лицо.Отчество", "abc");
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_wrong_partonimic2.png
wait_click_text "OK"
js wbt.SetModelFieldValue("Должник.Физ_лицо.Отчество", "");

js wbt.SetModelFieldValue("Должник.Тип", "Юр_лицо");
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\validation_no_Name.png
wait_click_text "OK"

exit