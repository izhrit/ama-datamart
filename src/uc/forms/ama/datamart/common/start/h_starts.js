﻿define([
	'forms/ama/datamart/customer/assignments/h_dm_cust_assignments'
],
function (h_dm_cust_assignments)
{
	return function (options_arg)
	{
		assignment = h_dm_cust_assignments(options_arg);
		var helper = {};
		helper = assignment.start();

		return helper;
	}
});