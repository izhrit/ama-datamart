define([
	'tpl!forms/ama/datamart/common/start/v_start_docs_menu.html'
	, 'forms/base/h_msgbox'
], function(v_start_docs_menu, h_msgbox) {
	return {
		docsMenu: {
			menuSel: 'div.cpw-ama-datamart-start-docs',
			el: null,
			open: function (cc_start) {
				var self = this;
				var sel = cc_start.fastening.selector;
				var btnClass = "docs-btn";
				var btn = $(sel + ' .'+btnClass);
				var btnPlaceTop = $(btn).offset().top + 36 + "px";
				var btnPlaceLeft = $(btn).offset().left + "px";
				if(!this.el) {
					var menuClass = "cpw-ama-datamart-start-docs";
					self.el = document.createElement('div');
					self.el.innerHTML = v_start_docs_menu();
					self.el.style.top = btnPlaceTop;
					self.el.style.left = btnPlaceLeft;
					self.el.style.position = 'absolute';
					self.el.style.zIndex = 103;
					$('body.cpw-ama').append(self.el)
					self.initDocsGetClicks(cc_start);
					btn.mouseout(function(e) {
						var target = $(e.relatedTarget)
						if(!target.hasClass(btnClass) && !target.hasClass(menuClass) && target.closest("."+menuClass).length===0) {
							self.el.style.display = 'none';
						}		
					})
					$(self.menuSel).mouseout(function(e) {
						var target = $(e.relatedTarget)
						if(!target.hasClass(btnClass) && !target.hasClass(menuClass) && target.closest("."+menuClass).length===0) {
							self.el.style.display = 'none';
						}		
					})
					$(self.menuSel).click(function(e) {
						self.el.style.display = 'none';
					})
				}else {
					self.el.style.display = 'block';
					self.el.style.top = btnPlaceTop;
					self.el.style.left = btnPlaceLeft;
				}
			},
			initDocsGetClicks: function (cc_start) {
				$(this.menuSel + ' .responce-doc').click(function() {
					cc_start.OnDownloadDocument('responce');
				})
	
				$(this.menuSel + ' .protocol-doc').click(function() {
					cc_start.OnDownloadDocument('protocol');
				})

				$(this.menuSel + ' .consent-doc').click(function() {
					cc_start.OnDownloadDocument('consent');
				})

				$(this.menuSel + ' .discharge-doc').click(function() {
					cc_start.OnDownloadDocument('discharge');
				})
			},
			destroy: function() {
				if(this.el) {
					this.el.parentNode.removeChild(this.el);
					this.el = null;
				}
			}
		},

		OnDownloadDocument: function (docType) {
			switch(docType) {
				case 'responce':
					this.DownloadResponce(); break;
				case 'protocol':
					this.DownloadProtocol(); break;
				case 'discharge':
					this.DownloadDischarge(); break;
				case 'consent':
					this.DownloadConsent(); break;
			}
		},

		DownloadResponce: function() {
			h_msgbox.ShowModal({
				title:'Документ подготовлен'
				,html:'Документ-ответ успешно загружен'
				,width:500, id_div:'cpw-msg-success-download-responce'
			});
		},
		
		DownloadProtocol: function() {
			h_msgbox.ShowModal({
				title:'Документ подготовлен'
				,html:'Документ-протокол успешно загружен'
				,width:500, id_div:'cpw-msg-success-download-protocol'
			});
		},
	
		DownloadDischarge: function() {
			h_msgbox.ShowModal({
				title:'Документ подготовлен'
				,html:'Выписка успешно загружена'
				,width:500, id_div:'cpw-msg-success-download-discharge'
			});
		},

		DownloadConsent: function () {
			h_msgbox.ShowModal({
				title: 'Документ подготовлен'
				, html: 'Документ-согласие успешно загружен'
				, width: 500, id_div: 'cpw-msg-success-download-consent'
			});
		}
	}
});