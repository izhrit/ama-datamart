define([
], function() {
	return {
		GetDefaultObject: function(courtDecision) {
			if(!courtDecision) courtDecision = {};
			var defaulCourtDecision = {
				Ссылка: courtDecision.Ссылка || '',
				Дополнительная_информация: courtDecision.Дополнительная_информация || ''
			}
			if(typeof courtDecision.Показывать_СРО !== 'undefined')
				defaulCourtDecision.Показывать_СРО = courtDecision.Показывать_СРО;
			defaulCourtDecision.Время = courtDecision.Время || {};
			defaulCourtDecision.Время.акта = (courtDecision.Время && courtDecision.Время.акта) || '';
			return defaulCourtDecision;
		}
	}
});