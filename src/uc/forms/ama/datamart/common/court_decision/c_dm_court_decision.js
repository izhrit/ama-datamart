﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/court_decision/e_dm_court_decision.html'
	, 'forms/base/h_msgbox'
],
	function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var options = {
			showCheckbox: options_arg && options_arg.showCheckbox
			,onlyAddInfo: options_arg && options_arg.onlyAddInfo
			,readonly: options_arg && options_arg.readonly
		};

		var controller = c_fastened(tpl, options);

		if(options_arg && options_arg.onlyAddInfo) {
			controller.size = { width: 600, height: 190 };
		} else {
			controller.size = { width: 935, height: 360 };
		}

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			var self = this;
			base_Render.call(this,sel);
			this.GetShowToSRO = options_arg && options_arg.GetShowToSRO;

			$(sel + ' .open-act').click(function() { self.OnOpenAct(); });
			if(options.showCheckbox && this.GetShowToSRO)
				$(sel + ' [data-fc-selector="Показывать_СРО"]').prop('checked', this.GetShowToSRO());
		}

		var trim = function (txt) {
			return txt.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
		}

		controller.OnOpenAct = function () {
			var court_decision = this.GetFormContent();
			if(!court_decision.Ссылка || trim(court_decision.Ссылка) === '') {
				h_msgbox.ShowModal({
					title:'Укажите ссылку на судебный акт'
					,width:500, id_div:'cpw-msg-error-open-act'
				});
				return;
			}
			window.open(court_decision.Ссылка, '_blank');
		}

		controller.Validate = function() {
			if(options.onlyAddInfo)
				return null;

			var court_decision = this.GetFormContent();
			var vr= [];
			var block = function (msg) { vr.push({ check_constraint_result: false, description: msg }); }
			if(court_decision.Ссылка && trim(court_decision.Ссылка) !== '' && trim(court_decision.Время.акта) == '') {
				block('Необходимо указать дату судебного акта!');
			}
			if(trim(court_decision.Ссылка) == '' && trim(court_decision.Время.акта) !== '') {
				block('Необходимо указать ссылку!');
			}
			return 0==vr.length ? null : vr;
		}
		return controller;
	}
});