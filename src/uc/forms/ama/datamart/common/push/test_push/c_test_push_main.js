define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/push/test_push/e_test_push_main.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox) {
	return function (options_arg) {

		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			if (!this.model)
				this.model = { Тип_пользователя: { id: 'c', text: 'Кабинет абонента ИС ПАУ' } };
			if (!this.model.Тип_пользователя)
				this.model.Тип_пользователя= { id: 'c', text: 'Кабинет абонента ИС ПАУ' };
			if (!this.model.Категория)
				this.model.Категория= { id: 'efrsb', text: 'ЕФРСБ' };
			if (!this.model.Заголовок)
				this.model.Заголовок= 'тестовое уведомление';
			if (!this.model.MessageGUID)
				this.model.MessageGUID = 'DA7621EED545925BA2F4D2DD4AE2CC3E';
			if (!this.model.id_Новости)
				this.model.id_Новости= 1;
			if (!this.model.Сообщение)
				this.model.Сообщение= 'тестовое сообщение';
			if (!this.model.app_id)
				this.model.app_id= $.cookie("test-push-app-id");

			base_Render.call(this, sel);
			var self = this
			$(sel + ' #notification-user-type').click(function () { self.GetUsersOptions($(this).val()) });
			$(sel + ' .cpw-test-push button.send').click(function () { self.SendNotificationToUser()});

			this.GetUsersOptions($(sel + ' #notification-user-type').val());
		}
		
		controller.SendNotificationToUser = function () {
			var content_form = this.GetFormContent();

			if (content_form.app_id)
				$.cookie("test-push-app-id",content_form.app_id);

			content_form = JSON.stringify(content_form);
			
			var url = this.base_url + '?action=push.test';
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос отправки уведомления", url);
			v_ajax.ajax({
				dataType: "json"
				, data: {
					content : content_form
				}
				, type: 'POST'
				, cache: false
				, success: function (response, textStatus) {
					$('.cpw-test-push .status').html('');
					if (null == response) {
						v_ajax.ShowAjaxError(response, textStatus);
					} else {
						$('.cpw-test-push .status').html('<b>Статус: </b>' + response.status);
					}
				}
			});
		}

		controller.SetUsersOptions = function () {
			var self = this;
			var sel = this.fastening.selector;

			var current_user_option = $(sel + ' #notification-user-type').val();
			this.GetUsersOptions(current_user_option);
		}

		controller.GetUsersOptions = function (user) {
			var self = this;
			var sel = this.fastening.selector;
			var url = this.base_url + '?action=push.test&userType=' + user;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос на получение пользователей", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus) {
					if (null == data) {
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else {
						self.AppendHtmlOptions(data, user);
						self.GetUserDestination($(sel + ' #notification-users').val(), data)
						$(sel + ' #notification-users').click(function () { self.GetUserDestination($(sel + ' #notification-users').val(), data) });
					}
				}
			});
	}
		controller.GetUserDestination = function (user, data) {
			var self = this;
			var sel = this.fastening.selector;
			var notification_users = $(sel + ' #notification-users')
			var app_id = $(sel + ' #text-push-app-id-textarea')
			switch ($(sel + ' #notification-user-type').val()) {
				case 'c':
					for (var i = 0; i < data.length; i++) {
						if (data[i].id_Contract == notification_users.val()) {
							app_id.val(data[i].Destination)
						}
					}
					break;
				case 'm':
					for (var i = 0; i < data.length; i++) {
						if (data[i].id_Manager == notification_users.val()) {
							app_id.val(data[i].Destination)
						}
					}
					break;
				case 'v':
					for (var i = 0; i < data.length; i++) {
						if (data[i].id_MUser == notification_users.val()) {
							app_id.val(data[i].Destination)
						}
					}
					break;
			}
		}


		controller.AppendHtmlOptions = function (data, user) {
			var sel = this.fastening.selector;
			var notification_users = $(sel + ' #notification-users')
			
			notification_users.html('');
			notification_users.val('');
			

			$(sel + ' #s2id_notification-users span').html('');
			switch (user) {
				case 'c':
					for (var i = 0; i < data.length; i++) {
						notification_users.append("<option value='" + data[i].id_Contract + "'> Номер договора: " + data[i].ContractNumber + "</option>")
					}
					break;
				case 'm':
					for (var i = 0; i < data.length; i++) {
						notification_users.append("<option value='" + data[i].id_Manager + "'>" + data[i].lastName + " " + data[i].firstName + " " + data[i].middleName + "</option>")
					}
					break;
				case 'v':
					for (var i = 0; i < data.length; i++) {
						notification_users.append("<option value='" + data[i].id_MUser + "'>" + data[i].UserName + "</option>")
					}
					break;
			}
			
			
		}

		return controller;
	}
});
