﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/push/event_type_settings/e_dm_push_event_type_settings.html'
],
	function (c_fastened, tpl) {
		return function () {
			var controller = c_fastened(tpl);
			return controller;
		}
	});