define(function () {

	return {

		controller: {
			"dm_push_profile": {
				path: 'forms/ama/datamart/common/push/profile/c_dm_push_profile'
				, title: 'Уведомления'
			}
			,"dm_push_settings": {
				path: 'forms/ama/datamart/common/push/settings/c_dm_push_settings'
				, title: 'Типовые настройки'
			}
			, "dm_push_event_type_settings": {
				path: 'forms/ama/datamart/common/push/event_type_settings/c_dm_push_event_type_settings'
				, title: 'Настройки типового уведомления'
			}
			, "dm_push_time_settings": {
				path: 'forms/ama/datamart/common/push/time_settings/c_dm_push_time_settings'
				, title: 'Настройки времени уведомления'
			}
			, "push": {
				path: 'forms/ama/datamart/common/push/profile/c_dm_push_profile'
				, title: 'Уведомления витрины данных'
			}
			, "latest": {
				path: 'forms/ama/datamart/common/push/latest/c_dm_push_latest'
				, title: 'Журнал уведомлений'
			}
		}

		, content: {
			"push_profile-01sav": {
				path: 'txt!forms/ama/datamart/common/push/tests/contents/01sav.json.etalon.txt'
			}
			,"push_profile-example-to-mobile": {
				path: 'txt!forms/ama/datamart/common/push/tests/contents/example-to-mobile.json.txt'
			}
		}

	};

});