﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/push/time_settings/e_dm_push_time_settings.html'
	, 'forms/ama/datamart/common/push/time_settings/h_time_zones'
],
	function (c_fastened, tpl, h_time_zones) {
		return function (options_arg) {

			var варианты_времени = [
				'08:00'
				,'09:00'
				,'10:00'
				,'11:00'
				,'12:00'
				,'13:00'
				,'14:00'
				,'15:00'
				,'16:00'
				,'17:00'
				,'18:00'
				,'19:00'
				,'20:00'
				,'21:00'
				,'22:00'
				,'23:00'
				,'00:00'
				,'01:00'
				,'02:00'
				,'03:00'
				,'04:00'
				,'05:00'
				,'06:00'
				,'07:00'
			];

			var controller = c_fastened(tpl, {варианты_времени: варианты_времени, часовые_пояса: h_time_zones });

			controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

			var base_Render = controller.Render;
			controller.Render = function (sel) {
				base_Render.call(this, sel);
			}

			return controller;
		}
	});