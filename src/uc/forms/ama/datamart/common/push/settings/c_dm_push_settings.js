﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/push/settings/e_dm_push_settings.html'
	, 'forms/ama/datamart/common/push/time_settings/c_dm_push_time_settings'
	, 'forms/ama/datamart/common/push/time_settings/h_time_zones'
	, 'forms/ama/datamart/common/push/efrsb/c_dm_push_efrsb'
],
function (c_fastened, tpl, c_dm_push_time_settings, h_time_zones, c_dm_push_efrsb)
{
	return function ()
	{
		var options = {
			field_spec:
			{
				 О_событиях: {controller: c_dm_push_efrsb, id_div: 'cpw-dm-push-efrsb-event-form'}
				,Присылать_уведомления:
				{
					controller: c_dm_push_time_settings
					, title: 'Настройки времени уведомления'
					, id_div: 'cpw-dm-push-time-form'
					, width: 400, height: 210
					, text: function (настройки)
					{
						var res= "c " + настройки.с + ' до ' + настройки.до;
						if (настройки.Часовой_пояс)
							res+= ', время: ' + h_time_zones[настройки.Часовой_пояс];
						return res;
					}
				}
				, Учитывать_выходные:{text: function (f) { return f ? 'только рабочие дни' : 'рабочие и выходные дни';}}
			}
		};

		var controller = c_fastened(tpl, options);

		var prep_default_model = function ()
		{
			return {
				О_событиях: {
					  О_проведении_торгов: { Уведомлять: "за_1_день" }
					, О_собраниях: { Уведомлять: "за_5_дней" }
					, О_судебном_заседании: { Уведомлять: "за_1_день" }
					, Уведомлять_об_объявлениях_на_ефрсб: true
				}
				, Учитывать_выходные: true
				, Присылать_уведомления: { 
					с: "10:00"
					, до: "17:00"
					, Часовой_пояс: "мск+0"
				}
				, Направлять: "никуда"
				, ПунктНазначения: ""
			};
		}

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			if (!this.model || null==this.model)
				this.model= prep_default_model();
			if ("в_мобильное_приложение_на_телефон"==this.model.Направлять)
				this.to_mobile_app= true;
			base_Render.call(this, sel);

			this.On_Куда_направлять(this.model.Направлять, /*do_not_clear=*/true);

			var self = this;
			$(sel + ' [data-fc-selector="Направлять"]').on('change', function (e) { self.On_Куда_направлять(this.value) })
		}

		var base_GetFormContent= controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var res= base_GetFormContent.call(this);
			if ("никуда"==res.Направлять)
				res.ПунктНазначения = "";
			return res;
		}

		controller.On_Куда_направлять = function (куда_направлять, do_not_clear)
		{
			var sel = this.fastening.selector;
			switch (куда_направлять)
			{
				case 'никуда': 
					$(sel + ' div.destination').hide();
					$(sel + ' div.destination.mobile-id-no').hide();
					$(sel + ' div.destination.mobile-id').hide();
					break;
				case 'на_адрес_электронной_почты':
					$(sel + ' div.destination').show();
					$(sel + ' div.destination.mobile-id-no').hide();
					$(sel + ' div.destination.mobile-id').hide();
					$(sel + ' div.destination label').text('email:');
					if (!do_not_clear)
						$(sel + ' [data-fc-selector="ПунктНазначения"]').val('').select();
					break;
				case 'смс_сообщениями_на_телефон':
					$(sel + ' div.destination').show();
					$(sel + ' div.destination.mobile-id-no').hide();
					$(sel + ' div.destination.mobile-id').hide();
					$(sel + ' div.destination label').text('телефон:');
					if (!do_not_clear)
						$(sel + ' [data-fc-selector="ПунктНазначения"]').val('').select();
					break;
				case 'в_мобильное_приложение_на_телефон':
					$(sel + ' div.destination').hide();
					if (!this.to_mobile_app)
					{
						$(sel + ' div.destination.mobile-id').hide();
						$(sel + ' div.destination.mobile-id-no').show();
					}
					else
					{
						$(sel + ' div.destination label').text('app-id:');
						$(sel + ' div.destination.mobile-id-no').hide();
						$(sel + ' div.destination.mobile-id').show();
						$(sel + ' div.destination.mobile-id div.app-id').text(this.model.ПунктНазначения);
					}
					break;
			}
		}

		return controller;
	}
});