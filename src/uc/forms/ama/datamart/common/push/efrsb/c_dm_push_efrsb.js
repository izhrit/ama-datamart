﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/push/efrsb/e_dm_push_efrsb.html'
	, 'forms/ama/datamart/common/push/event_type_settings/c_dm_push_event_type_settings'
],
function (c_fastened, tpl, c_dm_push_event_type_settings)
{
	return function ()
	{
		var settings_for_event = function (title)
		{
			return {
				controller: c_dm_push_event_type_settings
				, title: title
				, id_div: 'cpw-dm-push-timespan-form'
				, width: 300, height: 240
				, text: function (настройки) {
					if (!настройки || null == настройки)
						return 'НЕ уведомлять';
					switch (настройки.Уведомлять) {
						case 'за_1_день': return 'Уведомлять за 1 день';
						case 'за_3_дня': return 'Уведомлять за 3 дня';
						case 'за_5_дней': return 'Уведомлять за 5 дней';
						case 'за_15_дней': return 'Уведомлять за 15 дней';
						case 'никогда':
						default:
							return 'НЕ уведомлять';
					}
				}
			}
		}

		var options = {
			field_spec:
			{
				Уведомлять_об_объявлениях_на_ефрсб: { text: function (f) { return f ? 'Уведомлять' : 'НЕ уведомлять';}}
				,О_проведении_торгов: settings_for_event('О проведении торгов')
				,О_судебном_заседании: settings_for_event('О судебном заседании')
				,О_собраниях: settings_for_event('О собраниях')
			}
		};

		var controller = c_fastened(tpl, options);
		return controller;
	}
});
