define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/push/latest/e_dm_push_latest.html'
	, 'forms/base/h_msgbox'
],
	function (c_fastened, tpl, h_msgbox) {
		return function (options_arg) {
			var controller = c_fastened(tpl);

			controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

			controller.base_grid_url = controller.base_url + '?action=push.jqgrid';

			var base_Render = controller.Render;
			controller.Render = function (sel) {
				base_Render.call(this, sel);
				this.RenderGrid();
			}

			controller.colModel =
				[
					  { name: 'MessageGUID', hidden: true }
					, { label: 'Время события', name: 'EventTime', sortable: false, width: 10}
					, { label: 'Событие', name: 'message', sortable: false, width: 35}
					, { label: 'На отправку', name: 'Time_dispatched', sortable: false, width: 10}
					, { label: 'Отправлено', name: 'Time_pushed', sortable: false, width: 10}
				];

			controller.RenderGrid = function () {
				var sel = this.fastening.selector;
				var self = this;

				var grid = $(sel + ' table.grid');

				var url = this.base_grid_url + '&id_Push_receiver=' + this.model.id_Push_receiver;

				grid.jqGrid
					({
						datatype: 'json'
						, url: url
						, colModel: self.colModel
						, gridview: true
						, loadtext: 'Загрузка...'
						, recordtext: 'Показано уведомлений {1} из {2}'
						, emptyText: 'Нет уведомлений для отображения'
						, pgtext: "Страница {0} из {1}"
						, rownumbers: false
						, rowNum: 10
						, pager: '#cpw-cpw-ama-dm-latest-pushes-pager'
						, viewrecords: true
						, autowidth: true
						, height: 'auto'
						, multiselect: false
						, multiboxonly: true
						, shrinkToFit: true
						, ignoreCase: true
						, onSelectRow: function () { self.OnClickRow(); }
						, loadError: function (jqXHR, textStatus, errorThrown) {
							h_msgbox.ShowAjaxError("Загрузка списка уведомлений", url, jqXHR.responseText, textStatus, errorThrown)
						}
					});
				grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
			}

			controller.OnClickRow = function () {
				var sel = this.fastening.selector;
				var grid = $(sel + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');
				var rowdata = grid.jqGrid('getRowData', selrow);

				var url = "https://old.bankrot.fedresurs.ru/MessageWindow.aspx?ID=" + rowdata.MessageGUID;
				window.open(url, "Сообщение", "toolbar=no,location=no,status=no,menubar=no,resizable=yes,directories=no,scrollbars=yes,width=1000,height=600");
			}

			return controller;
		}
	});
