include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet

wait_text "Уведомления"
shot_check_png ..\..\shots\00new.png

play_stored_lines ama_datamart_push_receiver_settings_1

shot_check_png ..\..\shots\01sav.png
wait_text_disappeared "О собрании"
wait_click_text "Сохранить отредактированную модель"

dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01sav.json.result.txt
wait_click_text "Редактировать"

wait_text "Уведомления"
shot_check_png ..\..\shots\01sav.png

js wbt.SetModelFieldValue("Направлять", "приложение");
shot_check_png ..\..\shots\bad-destination-mobile.png

exit