include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet

wait_text "Уведомления"
shot_check_png ..\..\shots\01sav.png

play_stored_lines ama_datamart_push_receiver_settings_default

shot_check_png ..\..\shots\00new.png

wait_click_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\00new.json.result.txt

exit