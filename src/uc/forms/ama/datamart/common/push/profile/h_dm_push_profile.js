﻿define([
	'forms/base/h_msgbox'
	, 'forms/ama/datamart/common/push/profile/c_dm_push_profile'
],
function (h_msgbox, c_push_profile) {
	return {

		ModalEditPushProfile: function (base_url, id_Manager_Contract_MUser, Category) {
			var ajaxurl = base_url + '?action=push.crud&cmd=get&Category=' + Category + '&id=' + id_Manager_Contract_MUser + '&version=1';
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос информации о настройках уведомлений с сервера", ajaxurl);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (push_settings, textStatus) {
					self.EditProfile(base_url, id_Manager_Contract_MUser, push_settings, Category);
				}
			});
		}

		, OkToSave: function (profile) {
			var title = "Проверка перед сохранением";
			if (profile.Направлять == 'в_мобильное_приложение_на_телефон' && profile.ПунктНазначения.length <= 0) {

				h_msgbox.ShowModal({
					title: title, width: 410, html: "Настроить отправку уведомлений в мобильное приложение можно только из самого мобильного приложения.</br></br></br> Приложения доступны <b>бесплатно</b> для смартфонов под управлением <a target='_blank' href='https://russianit.ru/mobile-ama/'>iOS</a> и <a target='_blank' href='https://russianit.ru/mobile-ama/'>Android</a>"
				});

				return false;
			}
			switch (profile.Направлять) {
				case 'смс_сообщениями_на_телефон':
					var phone = profile.ПунктНазначения;
					if (10 > phone.length) {
						h_msgbox.ShowModal({ title: title, width: 410, html: "Длина телефона должна быть не менее 10 символов!" });
						return false;
					}
					if (!phone.match(/^\d+/)) {
						h_msgbox.ShowModal({ title: title, width: 410, html: "Поле должно состоять только из цифр!" });
						return false;
					}
					return true
					break;
				case 'на_адрес_электронной_почты':
					var email = profile.ПунктНазначения;
					if (5 > email.length) {
						h_msgbox.ShowModal({ title: title, width: 410, html: "Длина E-mail должна быть не менее 5 символов!" });
						return false;
					}
					if (-1 == email.indexOf('@')) {
						h_msgbox.ShowModal({ title: title, width: 410, html: "E-mail должен содержать символ '@'!" });
						return false;
					}
					return true
					break;
				default:
					return true
			}
		}

		, EditProfile: function (base_url, id_Manager_Contract_MUser, push_settings, Category) {
			var c_notify_profile = c_push_profile({ base_url: base_url });
			
			var self = this;
			c_notify_profile.SetFormContent(push_settings);
			var btnOk = 'Сохранить настройки уведомлений';
			var height= (window.app.disable_viewers2) ? 740 : 670;
			h_msgbox.ShowModal
				({
					title: 'Уведомления для пользователя витрины данных'
					, controller: c_notify_profile
					, buttons: [btnOk, 'Отмена']
					, id_div: "cpw-form-ama-datamart-push-profile"
					, width: 685, height: height
					, onclose: function (btn) {
						if (btn == btnOk) {
							var new_push_settings = c_notify_profile.GetFormContent();
							if (!self.OkToSave(new_push_settings))
								return false;
							if (null != push_settings)
								new_push_settings.id_Push_receiver = push_settings.id_Push_receiver;

							self.UpdateNotifications(base_url, id_Manager_Contract_MUser, new_push_settings, Category);
						}
					}
				});
		}

		, UpdateNotifications: function (base_url, id_Manager_Contract_MUser, profile, Category) {
			var ajaxurl = base_url + '?action=push.crud&cmd=update&Category=' + Category + '&id=' + id_Manager_Contract_MUser + '&version=1';
			var v_ajax = h_msgbox.ShowAjaxRequest("Сохранение настроек уведомлений на сервере", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: profile
				, cache: false
				, success: function (responce, textStatus) {
					if (null == responce) {
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true != responce.ok) {
						h_msgbox.ShowModal
							({
								title: 'Ошибка сохранения уведомлений'
								, width: 400
								, html: '<span>Не удалось сохранить настроек уведомления по причине:</span><br/> <center><b>\"'
									+ responce.reason + '\"</b></center>'
									+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
							});
					}
				}
			});
		}
	};
});