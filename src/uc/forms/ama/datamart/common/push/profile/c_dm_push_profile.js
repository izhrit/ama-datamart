﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/push/profile/e_dm_push_profile.html'
	, 'forms/ama/datamart/common/push/settings/c_dm_push_settings'
	, 'forms/ama/datamart/common/push/latest/c_dm_push_latest'
],
	function (c_fastened, tpl, c_dm_push_settings, c_dm_push_latest) {
    return function (options_arg) {
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var options = {
			field_spec:
			{
				ТиповыеНастройки: { controller: c_dm_push_settings }
			  , ЖурналУведомлений: { controller: function () { return c_dm_push_latest({ base_url: base_url}); }, render_on_activate: true }
			}
		};

		var controller = c_fastened(tpl, options);

		return controller;
	}
	});
