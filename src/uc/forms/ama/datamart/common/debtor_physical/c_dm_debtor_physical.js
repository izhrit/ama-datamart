define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/debtor_physical/e_dm_debtor_physical.html'
	, 'forms/ama/datamart/sro/request/h_request_search_consent'
	, 'forms/base/h_constraints'
], function(c_fastened, tpl
	, h_search_consent, h_constraints
) {
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var options = {
			withTypeahead: options_arg && options_arg.withTypeahead
			, field_spec:
			{
				'Физ_лицо.Фамилия': []
			}
		};
	
		var controller = c_fastened(tpl, options);
		controller.size = { width: 868, height: 300 };

		var base_Render = controller.Render;
		controller.Render = function (sel) {
			var self = this;
			this.readonly = options_arg && options_arg.readonly;
			this.SelectConsentIfExist = options_arg && options_arg.SelectConsentIfExist;
			this.search_consent = h_search_consent({id_SRO: this.id_SRO, base_url: base_url});
			this.PrepareToBaseRender();
			base_Render.call(this, sel);

			if(this.readonly) this.SetReadOnly();
			if($(sel).closest('.ui-dialog').length) this.search_consent.fixInModal(sel);
			$(sel + ' .typeahead').on('typeahead:selected', function (e, datum) {
				self.DispatchRequestUpstairs(datum.Request);
				self.SelectConsentIfExist && self.SelectConsentIfExist(datum);
			});
		}

		controller.SetReadOnly = function ()
		{
			var sel= this.fastening.selector;
			$(sel + ' input[type="text"]').prop('disabled', true);
		}

		var base_DispatchRequestUpstairs = options_arg && options_arg.DispatchRequestUpstairs;
		controller.DispatchRequestUpstairs = function (request) {
			var sel = this.fastening.selector;
			$(sel + ' [data-fc-selector="Физ_лицо.Имя"]').val(request.Должник.Супруги.Должник1.Физ_лицо.Имя);
			$(sel + ' [data-fc-selector="Физ_лицо.Отчество"]').val(request.Должник.Супруги.Должник1.Физ_лицо.Отчество);
			$(sel + ' [data-fc-selector="Физ_лицо.Отчество"]').val(request.Должник.Супруги.Должник1.Физ_лицо.Отчество);
			$(sel + ' [data-fc-selector="Физ_лицо.СНИЛС"]').val(request.Должник.Супруги.Должник1.Физ_лицо.СНИЛС);
			$(sel + ' [data-fc-selector="ОГРН"]').val(request.Должник.Супруги.Должник1.ОГРН);
			$(sel + ' [data-fc-selector="ИНН"]').val(request.Должник.Супруги.Должник1.ИНН);
			$(sel + ' [data-fc-selector="Адрес"]').val(request.Должник.Супруги.Должник1.Адрес);
			base_DispatchRequestUpstairs && base_DispatchRequestUpstairs(request)
		}

		var trim = function (txt)
		{
			return txt.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
		}

		controller.Validate = function ()
		{
			var debtor = this.GetFormContent();
			var vr= [];
			var block = function (msg) { vr.push({check_constraint_result:false,description:msg}); }
			if ('' == trim(debtor.Физ_лицо.Фамилия))
			{
				block('Необходимо указать фамилию должника!');
			}
			else if(h_constraints.recommend_Ciryllic().check(debtor.Физ_лицо.Фамилия)==='recommended')
			{
				block(h_constraints.recommend_Ciryllic().description(null, 'Фамилия'));
			}
			if(debtor.Физ_лицо.Имя && h_constraints.recommend_Ciryllic().check(debtor.Физ_лицо.Имя)==='recommended')
				block(h_constraints.recommend_Ciryllic().description(null, 'Имя'));
			if(debtor.Физ_лицо.Отчество && h_constraints.recommend_Ciryllic().check(debtor.Физ_лицо.Отчество)==='recommended')
				block(h_constraints.recommend_Ciryllic().description(null, 'Отчество'));
			if(debtor.Физ_лицо.СНИЛС && !h_constraints.Snils().check(debtor.Физ_лицо.СНИЛС))
				block(h_constraints.Snils().description(null, 'СНИЛС'));
			
			if(debtor.ИНН && !h_constraints.Inn().check(debtor.ИНН))
				block(h_constraints.Inn().description(null, 'ИНН'));
			if(debtor.ОГРН && !h_constraints.Ogrn().check(debtor.ОГРН))
				block(h_constraints.Ogrn().description(null, 'ОГРН'));

			return 0==vr.length ? null : vr;
		}

		controller.PrepareToBaseRender = function () {
			var fspec= this.options.field_spec;

			fspec['Физ_лицо.Фамилия'] = this.search_consent.citizenSearch({ isMarriedCouple: true });
		}

		return controller;
	}
});