define([
], function() {
	return {
		GetDefaultObject: function(efrsb) {
			if(!efrsb) efrsb = {};
			return {
				Номер: efrsb.Номер || '',
				Дата_публикации: efrsb.Дата_публикации || '',
				ID: efrsb.ID || '',
				Дополнительная_информация: efrsb.Дополнительная_информация || ''
			}
		}
	}
});