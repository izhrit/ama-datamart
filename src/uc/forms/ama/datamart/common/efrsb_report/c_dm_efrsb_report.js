define([
	'forms/base/fastened/c_fastened'
, 'tpl!forms/ama/datamart/common/efrsb_report/e_dm_efrsb_report.html'
, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
return function (options_arg)
{
	var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

	var options = {
		onlyAddInfo: options_arg && options_arg.onlyAddInfo
		, readonly: options_arg && options_arg.readonly
	};

	var controller = c_fastened(tpl, options);

	if(options_arg && options_arg.onlyAddInfo) {
		controller.size = { width: 600, height: 190 };
	} else {
		controller.size = { width: 650, height: 300 };
	}


	var base_Render= controller.Render;
	controller.Render = function (sel)
	{
		var self = this;
		base_Render.call(this,sel);

		$(sel + ' .open-report-btn').click(function() { self.OpOpenReport(); });
	}

	var trim = function (txt) {
		return txt.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
	}

	controller.OpOpenReport = function () {
		var efrsb_report = this.GetFormContent();
		if(!efrsb_report.ID || trim(efrsb_report.ID) === '') {
			h_msgbox.ShowModal({
				title:'Отказ открыть страницу'
				,html:'<p>Чтобы открыть страницу сообщения на bankrot.fedresurs.ru, нужно указать ID сообщения..</p>'
				,width:400, id_div:'cpw-msg-can-not-open-efrsb-msg'
			});
			return;
		}
		window.open("https://old.bankrot.fedresurs.ru/MessageWindow.aspx?ID=" + efrsb_report.ID, '_blank');
	}

	controller.Validate = function() {
		if(options.onlyAddInfo)
			return null;
		var efrsb_report = this.GetFormContent();
		var vr= [];
		var block = function (msg) { vr.push({ check_constraint_result: false, description: msg }); }
		if(trim(efrsb_report.ID) === '' && trim(efrsb_report.Номер) !== '' && trim(efrsb_report.Дата_публикации) !== '')
			block('Необходимо указать ID сообщения!');
		if(trim(efrsb_report.Номер) === '' && trim(efrsb_report.ID) !== '' && trim(efrsb_report.Дата_публикации) !== '')
			block('Необходимо указать номер сообщения!');
		if(trim(efrsb_report.Дата_публикации) === '' && trim(efrsb_report.ID) !== '' && trim(efrsb_report.Номер) !== '' )
			block('Необходимо указать дату публикации!');
		return 0==vr.length ? null : vr;
	}
	return controller;
}
});