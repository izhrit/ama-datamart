﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/request_consents/e_request_consents.html'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/fastened/fastening/h_fastening_clip'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/sro/consent/c_consent'
], function (c_fastened, tpl, copy_codec, h_fastening_clip, h_msgbox, c_consent) 
{
	return function (options_arg) 
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_aus_select = base_url + '?action=mrequest.select2'+
			(options_arg && options_arg.id_SRO ? '&id_SRO='+options_arg.id_SRO : '');
		var base_consent_crud_url = base_url + '?action=consent.crud';

		var safe_collect_manager_for_select2= function(items,results)
		{
			if (items && null != items)
			{
				for (var i = 0; i < items.length; i++)
				{
					var ps= items[i];
					var text= ps.АУ.text + ' согл.упр.должн-ом ' + ps.Заявление_на_банкротство.Должник;
					if (ps.Номер_судебного_дела)
						text+ ' (дело № ' + ps.Номер_судебного_дела + ')';
					results.push
					({
						id: ps.id_ProcedureStart
						,text: text
						,item: {
							АУ: { id: ps.АУ.id, text: ps.АУ.text }
							,id_ProcedureStart: ps.id_ProcedureStart
							,Заявление_на_банкротство: { Дата_подачи: ps.Заявление_на_банкротство.Дата_подачи }
						}
					})
				}
			}
		}

		var options =
		{
			ccopy: copy_codec(),
			field_spec: {
				АУ: {
					placeholder: 'Выберите АУ',
					allowClear: true,
					ajax: {
						url: base_url_aus_select,
						dataType: 'json',
						data: function (params) { return { query: params, type: 'public' }; },
						results: function (data)
						{
							var results = []
							var model = base_GetFormContent.call(controller);
							if (!model) 
								model = {};
							model.АУ_с_сервера = data
							controller.SetFormContent(model);
							safe_collect_manager_for_select2(data,results);
							safe_collect_manager_for_select2(model.АУ_удаленные,results);
							return { "results": results };
						}
					}
				}
			},
			readonly: (options_arg && options_arg.readonly)
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel) {
			base_Render.call(this, sel);
			var self = this;

			if (this.model && this.model.В_отчет)
			$(sel + ' [data-fc-selector="Согласия"] .answer input[value="' + this.model.В_отчет + '"]').click()

			$(sel + ' [data-fc-selector="АУ"]').on('change', function (e) { self.OnSelectСогласие(e); });

			var clickDltBtns = function (mode) {
				var dltBtns = $(sel + ' .do_delete');
				if(mode == 'add') { dltBtns.on('click', $.proxy(self.on_delete, self))}
				else { dltBtns.off('click', $.proxy(self.on_delete, self)) }
			}

			clickDltBtns('add');
			$(sel + ' .do_edit').click(function (e) { self.on_edit(e); });
			$(sel + ' .answer input').click(function (e) { self.on_answer(e); })

			this.fastening.on_after_update_dom = function () {
				clickDltBtns('remove');
				clickDltBtns('add');

				var do_edit_items = $(sel + ' .do_edit');
				do_edit_items.off('click');
				do_edit_items.click(self.on_edit.bind(self));

				var do_answer_items = $(sel + ' .answer input');
				do_answer_items.off('click');
				do_answer_items.click(self.on_answer.bind(self));

				self.addRadioAttrs()
				var model = self.GetFormContent()
				if (model && model.В_отчет)
					$(sel + ' [data-fc-selector="Согласия"] .answer input[value="' + model.В_отчет + '"]').click()
			};

		}

		controller.OnSelectСогласие = function (e)
		{
			if (e.added)
			{
				this.ДобавитьВыбранноеСогласие(e.added)
			}
			else
			{
				var sel= this.fastening.selector;
				$(sel + ' .select .select2-choice').addClass('select2-default')
				$(sel + ' .select .select2-choice span').html('Выберите АУ')
			}
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var request_consents = base_GetFormContent.call(this);
			var result = {}
			if (request_consents.Согласия && 0!==request_consents.Согласия.length)
			{
				result.Согласия = request_consents.Согласия;
				result.В_отчет = request_consents.В_отчет;
			}
			return result;
		}

		controller.Найти_согласие_среди_выбранных = function (id_ProcedureStart)
		{
			var fastening= this.fastening
			var sel = fastening.selector
			var fc_dom_item = $(sel + ' [data-fc-selector="Согласия"]');
			var model = fastening.get_fc_model_value(fc_dom_item);
			if (model)
			{
				for (var i = 0; i < model.length; i++)
				{
					if (model[i].id_ProcedureStart == id_ProcedureStart)
						return model[i];
				}
			}
			return null;
		}

		controller.ДобавитьВыбранноеСогласие= function (selected_ps)
		{
			var obj = this.fastening
			var sel = obj.selector
			var fc_dom_item = $(sel + ' [data-fc-selector="Согласия"]');
			var model = obj.get_fc_model_value(fc_dom_item);
			var выбранное_ранее_согласие= this.Найти_согласие_среди_выбранных(selected_ps.item.id_ProcedureStart);
			if (null==выбранное_ранее_согласие)
			{
				if (null == model || !model)
					model= [];
				model.push(selected_ps.item);
				obj.set_fc_model_value(fc_dom_item, model);
				$(sel).trigger('model_change');
			}
			$(sel + ' [data-fc-selector="АУ"]').val('').trigger('change');
			var model = base_GetFormContent.call(this);
			if (model && model.В_отчет)
				$(sel + ' [data-fc-selector="Согласия"] .answer input[value="' + model.В_отчет + '"]').click()
		}

		controller.on_answer = function (e) {
			var obj = this.fastening
			var sel = obj.selector
			var item = h_fastening_clip.get_parent_fc_of_type($(e.currentTarget), 'array-item')
			$(sel + ' [data-fc-selector="Согласия"] .row').removeClass('selected')
			item.addClass('selected')

			var model = base_GetFormContent.call(this);
			var selectedItemId = item.attr('data-array-index')
			if(model.Согласия) {
				for(var i = 0; i < model.Согласия.length; i++) {
					if(i!==selectedItemId) 
						delete model.Согласия[i].В_ответе
				}
			}
			model.В_отчет = model.Согласия[selectedItemId].id_ProcedureStart;
			this.SetFormContent(model)
			$(sel).trigger('model_change');
		}

		controller.on_edit = function (e) {
			var obj = this.fastening
			var fc_dom_item = $(obj.selector + ' [data-fc-selector="Согласия"]');
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.currentTarget), 'array-item').attr('data-array-index');
			var model = obj.get_fc_model_value(fc_dom_item);
			var item = model[i_item];

			var ajaxurl = base_consent_crud_url + '&cmd=get&id=' + item.id_ProcedureStart + '&id_Contract=' + item.id_Contract;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных запроса кандидатур с сервера", ajaxurl);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus) {
						if (null == data) {
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else {
							var consent = c_consent({ readonly: true, id_Contract: item.id_Contract, hideMakeRequestBtn: true });
							consent.SetFormContent(data);
							h_msgbox.ShowModal
								({
									title: 'Согласие АУ', controller: consent, buttons: ['Закрыть']
									,width: 920, minHeight: 540
									, id_div: "cpw-form-ama-datamart-consent-readonly-form"
								});
						}
					}
				});
		}

		controller.on_delete = function (e) {
			var obj = controller.fastening
			var sel = obj.selector
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.currentTarget), 'array-item').attr('data-array-index');
			var model = base_GetFormContent.call(this);
			var fc_dom_item = $(sel + ' [data-fc-selector="Согласия"]');
			var Согласия = obj.get_fc_model_value(fc_dom_item);
			if(Согласия) {
				if(Согласия[i_item] && Согласия[i_item].В_ответе) {
					if(Согласия[i_item].В_ответе == model.В_отчет)
						delete model.В_отчет
					delete Согласия[i_item].В_ответе;
					$(sel + ' [data-fc-selector="Согласия"] .row').removeClass('selected')
					$(sel + ' [data-fc-selector="Согласия"] [data-fc-selector="В_ответе"]').prop('checked', false);
				}
				var deleted_au = copy_codec().Copy(Согласия[i_item]);
				Согласия.splice(i_item, 1);
				obj.set_fc_model_value(fc_dom_item, Согласия);
				$(sel).trigger('model_change');
				var model = controller.storeDeletedLocalAUs(model, deleted_au);
				controller.SetFormContent(model);
			}
				
			$(sel).trigger('model_change');
			return false;
		}

		controller.storeDeletedLocalAUs = function(model, au) {
			var isInDeletedAus = false
			var isInFetchedAus = false

			if(model.АУ_удаленные) {
				for(var i=0; i<model.АУ_удаленные.length; i++) {
					if(model.АУ_удаленные[i].id_ProcedureStart === au.id_ProcedureStart)
						isInDeletedAus = true
				}
			}
			if(model.АУ_с_сервера) {
				for(var i=0; i<model.АУ_с_сервера.length; i++) {
					if(model.АУ_с_сервера[i].id_ProcedureStart === au.id_ProcedureStart)
						isInFetchedAus = true
				}
			}

			if(!isInDeletedAus && !isInFetchedAus) { 
				if(!model.АУ_удаленные) model.АУ_удаленные = [];
				model.АУ_удаленные.push(au)
			}
			return model
		}

		controller.addRadioAttrs = function ()
		{
			var self = this;
			var model = self.GetFormContent()
			var sel = self.fastening.selector
			if (model && model.Согласия) {
				for (var i = 0; i < model.Согласия.length; i++) {
					$(sel + ' [data-fc-selector="Согласия"] [data-fc-type="array-item"][data-array-index="' + i + '"] [data-fc-selector="В_ответе"]')
						.val(model.Согласия[i].id_ProcedureStart)
				}
			}
		}

		return controller;
	}
})