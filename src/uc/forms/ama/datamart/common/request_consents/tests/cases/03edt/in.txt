include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\..\sro\consent\tests\cases\in.lib.txt quiet
include ..\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "18.01.2021"
shot_check_png ..\..\shots\01sav.png
wait_click_text "Сидоров"
wait_text "Согласие АУ"
shot_check_png ..\..\shots\03edt.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\03edt.json.result.txt
exit