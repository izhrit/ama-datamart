define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/cust_query/list/e_cust_query_list.html'
],
function (c_fastened, tpl)
{
	return function()
	{
		var controller = c_fastened(tpl);

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' a.query').click(function (e){e.preventDefault(); self.OnQueryClick(e); });
			$(sel + ' button.reload').click(function (e){e.preventDefault(); if (self.OnReloadList) self.OnReloadList(); });
			$(sel + ' a.folder-title').click(function (e){e.preventDefault(); self.OnTogleFolder(e); });
		}

		controller.OnQueryClick = function (e)
		{
			if (this.OnGotoQuery)
			{
				var iquery= $(e.target).data('i-qlist');
				this.OnGotoQuery(iquery);
			}
		}

		controller.OnTogleFolder = function (e)
		{
			if (this.OnGotoFolder)
			{
				var $a= $(e.target).parents('a');
				var ifolder= $a.data('i-flist');
				this.OnGotoFolder(ifolder);
			}
			else
			{
				var a_item = $(e.target).parent('a');
				var li_item = a_item.parent('li.folder');
				if (!li_item.hasClass('closed'))
				{
					li_item.addClass('closed')
				}
				else
				{
					li_item.removeClass('closed')
				}
			}
		}

		return controller;
	}
});
