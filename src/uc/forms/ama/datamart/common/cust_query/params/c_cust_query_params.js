define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/cust_query/params/e_cust_query_params.html'
],
function (c_fastened, tpl)
{
	return function()
	{
		// параметр в тексте запроса начинается с "?:" и заканчивается "."
		var split_sql_text = function (sql_text)
		{
			var res = {text:[],param:[]};
			var parts= sql_text.split('?:');
			res.text.push(parts[0]);
			for (var i = 1; i < parts.length; i++)
			{
				var part= parts[i];
				var pos= part.indexOf('.');
				res.param.push(part.substring(0,pos));
				res.text.push(part.substring(pos+1));
			}
			return res;
		}

		// параметр в тексте запроса начинается с "?:" и заканчивается "."
		var build_params_from_sql_text = function (sql_text)
		{
			var tp= split_sql_text(sql_text);

			var param_by_name = {};
			for (var i = 0; i < tp.param.length; i++)
			{
				var par_name= tp.param[i];
				if (!param_by_name[par_name])
				{
					param_by_name[par_name]= [i];
				}
				else
				{
					param_by_name[par_name].push(i);
				}
			}

			var params= [];
			for (var name in param_by_name)
			{
				var par_type= name.charAt(0);
				var par_name= name.substring(1);
				params.push({name:par_name,type:par_type});
			}

			return params;
		}

		var controller = c_fastened(tpl, {build_params_from_sql_text:build_params_from_sql_text});

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' a.back').click(function (e){e.preventDefault(); if (self.OnGotoList) self.OnGotoList(); });
			$(sel + ' button.execute').click(function (e){e.preventDefault(); var params= self.GetParams(); if (self.OnExecute) self.OnExecute(params); });
		}

		controller.GetParams = function ()
		{
			var res = {};
			var sel= this.fastening.selector;
			$(sel + ' .param').each(function () {
				$this= $(this);
				var param_name= $this.attr('data-param-name');
				var $input= $this.find('input');
				var $input_type= $input.attr('type');
				var param_value= null;
				switch ($input_type)
				{
					case 'checkbox':
						param_value= null!=$input.attr('checked');
						break;
					default:
						param_value= $input.val();
				}
				res[param_name]= param_value;
			});
			return res;
		}

		return controller;
	}
});
