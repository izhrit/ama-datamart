define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/cust_query/main/e_cust_query_main.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/common/cust_query/list/c_cust_query_list'
	, 'forms/ama/datamart/common/cust_query/params/c_cust_query_params'
	, 'forms/ama/datamart/common/cust_query/result/c_cust_query_result'
	, 'forms/base/codec/url/codec.url'
	, 'tpl!forms/ama/datamart/common/cust_query/main/v_cust_query_breadcrumbs.html'
],
function (c_fastened, tpl, h_msgbox, c_cust_query_list, c_cust_query_params, c_cust_query_result, codec_url, v_cust_query_breadcrumbs)
{
	return function(options_arg)
	{
		var options= { v_cust_query_breadcrumbs: v_cust_query_breadcrumbs };
		var controller = c_fastened(tpl, options);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		controller.current_path= [];

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			this.model = {};
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' div.header span.path').on('back-path', function (e,i) { self.OnBackPath(i); })

			this.LoadAndRenderList();
		}

		controller.OnBackPath = function (i)
		{
			var i= parseInt(i);
			while (i!=(this.current_path.length-1))
				this.current_path.pop();
			var sel= this.fastening.selector;
			$(sel + ' div.header span.path').html(v_cust_query_breadcrumbs(this.current_path));
			this.OnReloadList();
		}

		controller.PreparePathForUrl = function ()
		{
			var path= '/';
			var ccurl= codec_url();
			for (var i = 0; i < this.current_path.length; i++)
			{
				var f= this.current_path[i];
				path+= ccurl.Encode(f.foldername) + '/';
			}
			return path;
		}

		controller.LoadAndRenderList = function ()
		{
			var self = this;
			var url = this.base_url + '?action=custom_query.list&path=' + this.PreparePathForUrl();
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение списка пользовательских запросов к базе данных", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (list)
				{
					self.RenderList(list);
				}
			});
		}

		controller.OnReloadList = function ()
		{
			this.list= null;
			delete this.list;
			this.SafeClear('list');
			this.LoadAndRenderList();
		}

		controller.RenderList = function (list)
		{
			this.list= list;
			var self= this;
			var sel= this.fastening.selector;
			$(sel + ' div.cpw-custom-query').attr('data-state','list');
			if (!this.controller)
				this.controller = {};
			var c= this.controller.list= c_cust_query_list(options_arg);
			c.OnReloadList = function () { self.OnReloadList(); }
			c.OnGotoQuery = function (iquery){ self.OnGotoQuery(iquery); };
			c.OnGotoFolder = function (ifolder){ self.OnGotoFolder(ifolder); };
			c.SetFormContent(list);
			c.Edit(sel + ' div.cpw-custom-query div.list');
		}

		controller.SafeClear = function (n)
		{
			var sel= this.fastening.selector;
			if (this.controller[n])
			{
				this.controller[n].Destroy();
				this.controller[n]= null;
				delete this.controller[n];
				$(sel + ' div.cpw-custom-query div.' + n).html('');
			}
		}

		controller.OnGotoList = function ()
		{
			this.SafeClear('params');
			this.SafeClear('result');
			var sel= this.fastening.selector;
			$(sel + ' div.cpw-custom-query').attr('data-state','list');
		}

		controller.OnGotoParams= function ()
		{
			this.SafeClear('result');
			var sel= this.fastening.selector;
			$(sel + ' div.cpw-custom-query').attr('data-state','params');
		}

		controller.OnGotoQuery = function (iquery)
		{
			var self= this;
			var sel= this.fastening.selector;
			$(sel + ' div.cpw-custom-query').attr('data-state','params');
			var c= this.controller.params= c_cust_query_params(options_arg);
			c.OnGotoList = function () { self.OnGotoList(); }
			c.OnExecute = function (params) { self.OnExecute(iquery,params); }
			c.SetFormContent(this.list.queries[iquery]);
			c.Edit(sel + ' div.cpw-custom-query div.params');
		}

		controller.OnGotoFolder = function (ifolder)
		{
			var folder= this.list.folders[ifolder];
			this.current_path.push(folder);
			var sel= this.fastening.selector;
			$(sel + ' div.header span.path').html(v_cust_query_breadcrumbs(this.current_path));
			this.OnReloadList();
		}

		controller.OnExecute = function (iquery,params)
		{
			var self= this;
			var encoded_filename= codec_url().Encode(this.list.queries[iquery].filename);
			var url = this.base_url + '?action=custom_query.execute&filename=' + encoded_filename + '&path=' +  this.PreparePathForUrl();
			var v_ajax = h_msgbox.ShowAjaxRequest("Выполнение пользовательского запроса к базе данных", url);
			v_ajax.ajax({
				dataType: "json", type: 'POST', data: params, cache: false
				, success: function (result)
				{
					self.OnShowResult(result);
				}
			});
		}

		controller.OnShowResult = function (result)
		{
			var self= this;
			var sel= this.fastening.selector;
			$(sel + ' div.cpw-custom-query').attr('data-state','result');
			var c= this.controller.result= c_cust_query_result(options_arg);
			c.OnGotoList = function () { self.OnGotoList(); }
			c.OnGotoParams = function () { self.OnGotoParams(); }
			c.SetFormContent(result);
			c.Edit(sel + ' div.cpw-custom-query div.result');
		}

		return controller;
	}
});
