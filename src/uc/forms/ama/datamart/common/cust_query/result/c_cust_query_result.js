define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/cust_query/result/e_cust_query_result.html'
],
function (c_fastened, tpl)
{
	return function()
	{
		var controller = c_fastened(tpl);

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' a.back').click(function (e){e.preventDefault(); if (self.OnGotoList) self.OnGotoList(); });
			$(sel + ' a.params').click(function (e){e.preventDefault(); if (self.OnGotoParams) self.OnGotoParams(); });
		}

		return controller;
	}
});
