define(function ()
{
	return {

		controller: {
			"cust_query_list": {
				path: 'forms/ama/datamart/common/cust_query/list/c_cust_query_list'
				, title: 'список пользовательских запросов к базе данных'
			}
			, "cust_query_params": {
				path: 'forms/ama/datamart/common/cust_query/params/c_cust_query_params'
				, title: 'параметры пользовательского запроса к базе данных'
			}
			, "cust_query_result": {
				path: 'forms/ama/datamart/common/cust_query/result/c_cust_query_result'
				, title: 'результат выполнения пользовательского запроса к базе данных'
			}
			, "cust_query_main": {
				path: 'forms/ama/datamart/common/cust_query/main/c_cust_query_main'
				, title: 'пользовательские запросы к базе данных'
			}
		}

		, content: {
			"cust_query_list_example1": {
				path: 'txt!forms/ama/datamart/common/cust_query/list/tests/contents/example1.json.txt'
			}
			,"cust_query_list_example2": {
				path: 'txt!forms/ama/datamart/common/cust_query/list/tests/contents/example2.json.txt'
			}
			,"cust_query_params_example1": {
				path: 'txt!forms/ama/datamart/common/cust_query/params/tests/contents/example1.json.txt'
			}
			,"cust_query_params_example2": {
				path: 'txt!forms/ama/datamart/common/cust_query/params/tests/contents/example2.json.txt'
			}
			,"cust_query_result_example1": {
				path: 'txt!forms/ama/datamart/common/cust_query/result/tests/contents/example1.json.txt'
			}
			,"cust_query_params_example3": {
				path: 'txt!forms/ama/datamart/common/cust_query/params/tests/contents/example3.json.txt'
			}
		}
	};

});