define(['forms/ama/datamart/common/unsubscribe/c_unsubscribe'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'unsubscribe'
		, Title: 'Отписка от рассылки'
	};
	return form_spec;
});
