define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/unsubscribe/e_unsubscribe.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox) {
	return function (options_arg) {
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var options = {
		};
		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel) {

			base_Render.call(this, sel);
			var self = this;
			$(sel + ' .cpw-unsubscribe button.unsub').click(function () { self.UnsubscribeEmail() });
		}

		controller.UnsubscribeEmail = function () {
			var content_form = this.GetFormContent();
			var sel = this.fastening.selector;
			var email = $_GET['email'],
				guid = $_GET['guid']
			var data_result = {
				 form: content_form
				,params: {
					  email: email
					, guid: guid
				}
			}
			data = JSON.stringify(data_result);
			
			var url = base_url + '?action=push.unsubscribe';
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос отписки от рассылки", url);
			v_ajax.ajax({
				dataType: "json"
				, data: data_result
				, type: 'POST'
				, cache: false
				, success: function (response, textStatus) {
					$('.cpw-test-push .status').html('');
					if (null == response) {
						v_ajax.ShowAjaxError(response, textStatus);
					} else if (response.success) {
						$(sel + ' div.cpw-unsubscribe div.content').html('<p>Ваш электронный адрес <b>' 
							+ email + '</b> <span>успешно отписан от рассылки.</span></p>')
					} else {
						$(sel + ' div.cpw-unsubscribe div.content').html('<p>Пользователь не найден!</p>')
					}
				}
			});
		}
		return controller;
	}
});