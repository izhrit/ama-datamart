﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/schedule/e_dm_schedule.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/base/h_debtorName'
],
function (c_fastened, tpl, h_msgbox, h_debtorName)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_events_url = controller.base_url + '?action=schedule.events'

		var base_Render = controller.Render;
		controller.Render= function (sel)
		{
			base_Render.call(this, sel);
			this.renderCalendar();
		}

		controller.prepare_events_url = function ()
		{
			var base_events_url= this.base_events_url;
			if (this.model)
			{
				if (this.model.id_MUser)
				{
					base_events_url += '&id_MUser=' + this.model.id_MUser;
				}
				else if (this.model.id_Manager)
				{
					base_events_url += '&id_Manager=' + this.model.id_Manager;
				}
				else if (this.model.id_Contract)
				{
					base_events_url += '&id_Contract=' + this.model.id_Contract;
				}
			}
			return base_events_url;
		}

		controller.Destroy = function ()
		{
			if (this.calendar && null != this.calendar)
			{
				this.calendar.destroy();
				this.calendar= null;
				delete this.calendar;
			}
		}

		var eventDidMount= function (info)
		{
			new Tooltip(info.el, {
				title: info.event.extendedProps.description,
				placement: 'top',
				trigger: 'hover',
				container: 'body'
			});
		}

		var headerToolbar= {
			left: 'prev,next today',
			center: 'title',
			right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
		}

		var SafeOpenEfsrb = function (url)
		{
			if (url)
			{
				window.open(url, "Сообщение", 
				"toolbar=no,location=no,status=no,menubar=no,resizable=yes,directories=no,scrollbars=yes,width=1000,height=600");
			}
		}

		controller.renderCalendar = function ()
		{
			var self = this;
			var sel = this.fastening.selector;
			var calendarEl = $(sel + ' div.cal-placer')[0];

			var base_events_url= this.prepare_events_url();
			this.calendar = new FullCalendar.Calendar(calendarEl, {
				headerToolbar: headerToolbar,
				locale: 'ru',
				initialView: 'dayGridMonth',
				eventDidMount: eventDidMount,
				contentHeight: 500,
				scrollTime: '09:00:00',
				dayMaxEventRows: true,
				events: function (fetchInfo, successCallback, failureCallback)
				{
					var regxp = /^(\d{4})\-(\d{2})\-(\d{2})\T(\d{2})\:(\d{2})\:(\d{2})/g;
					var view_start = fetchInfo["startStr"].match(regxp)[0];
					var view_end = fetchInfo["endStr"].match(regxp)[0];
					var url = base_events_url + '&start=' + view_start + "&end=" + view_end;
					var v_ajax = h_msgbox.ShowAjaxRequest("Загрузка календарных событий с сервера", url, "id-div-ShowAjaxRequest_events");
					v_ajax.ajax({
						type: 'GET'
						, dataType: 'json'
						, cache: false
						, success: function (responce, textStatus) {
							if (null != responce)
							{
								var formated_response = self.format_response(responce);
								successCallback(formated_response);
							}
							else
							{
								v_ajax.ShowAjaxError(responce, textStatus);
								failureCallback(responce);
							}
						}
					});
				},
				eventClick: function (info) { info.jsEvent.preventDefault(); SafeOpenEfsrb(info.event.url); },
				eventTimeFormat: { hour: '2-digit', minute: '2-digit' }
			});

			$(sel + ' div.refresh-btn__wrapper').on('click', function () { self.Reload(); });
			this.calendar.render();
		}

		controller.Reload = function ()
		{
			if (this.calendar && null!=this.calendar)
				this.calendar.refetchEvents();
		}

		var prepare_event_start= function(s)
		{
			return (s.indexOf("00:00:00") === -1) ? s : s.substring(0, s.indexOf("00:00:00") - 1);
		}

		controller.format_response = function (response)
		{
			var res = [];
			for (var i= 0; i < response.length; i++)
			{
				var item= response[i];
				res.push({
					title: item.title
					,start: prepare_event_start(item.start)
					,description: item.description
					,url: item.url
				});
			}
			return res;
		}

		window.cpw_ama_datamart_test_scheduleGotoDate = function (date)
		{
			controller.calendar.gotoDate(date);
		}

		return controller;
	}
});