define(['forms/ama/datamart/common/schedule/c_dm_schedule'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'schedule_orpau'
		, Title: 'Календарь витрины данных ПАУ для кабинета ОРПАУ'
	};
	return form_spec;
});
