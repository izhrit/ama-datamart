define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/married_couple/e_married_couple.html'
	, 'forms/ama/datamart/common/debtor_physical/c_dm_debtor_physical'
], function(c_fastened, tpl
	, c_dm_debtor_physical
) {
	return function (options_arg)
	{
		var options = {
			readonly: options_arg && options_arg.readonly
			, field_spec: {
				Должник1: { controller: c_dm_debtor_physical },
				Должник2: { controller: c_dm_debtor_physical }
			}
		}

		var controller = c_fastened(tpl, options);

		var base_Render= controller.Render;

		controller.Render = function (sel)
		{
			this.PrepareToBaseRender(sel);
			base_Render.call(this,sel);
		}

		controller.PrepareToBaseRender = function (sel)
		{
			var fspec= this.options.field_spec;

			fspec.Должник1 = this.debtor_modal_object;
			fspec.Должник2 = this.debtor_modal_object;
		}

		var trim = function (txt)
		{
			return txt.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
		}

		controller.debtor_modal_object =
		{
			controller: function () { return c_dm_debtor_physical({
				readonly: options.readonly
				, DispatchRequestUpstairs: options_arg && options_arg.DispatchRequestUpstairs
				, withTypeahead: options_arg && options_arg.withTypeahead
				, SelectConsentIfExist: options_arg && options_arg.SelectConsentIfExist
				, base_url: options_arg && options_arg.base_url
			}) }
			, title: 'Информация о должнике'
			, buttons: !options.readonly ? null : ["Закрыть"]
			, id_div: 'cpw-form-ama-married_couple-debtor_physical'
			, text: function(data){
				var second_name = data.Физ_лицо.Фамилия;
				if(!second_name || trim(second_name)==='')
					return options.readonly ? 'Должник не указан' : 'Неизвестное лицо (кликните, чтобы указать)';
				return  second_name +
					(!data.Физ_лицо.Имя || trim(data.Физ_лицо.Имя)==='' ? '' : ' ' + data.Физ_лицо.Имя) +
					(!data.Физ_лицо.Отчество || trim(data.Физ_лицо.Отчество)==='' ? '' : ' ' + data.Физ_лицо.Отчество) + 
					' (ИНН: ' + (!data.ИНН || trim(data.ИНН)==='' ? 'не указан' : data.ИНН) +
					', СНИЛС: ' +  (!data.Физ_лицо.СНИЛС || trim(data.Физ_лицо.СНИЛС)==='' ? 'не указан' : data.Физ_лицо.СНИЛС)+')';
			}
		}

		controller.Validate = function ()
		{
			var debtorsCouple = this.GetFormContent();
			var vr= [];
			var block = function (msg) { vr.push({check_constraint_result:false,description:msg}); }

			if(!debtorsCouple.Должник1 || !debtorsCouple.Должник2)
				block('Необходимо указать фамилии всех должников!');

			return 0==vr.length ? null : vr;
		}

		return controller;
	}
});