define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/common/applicant/e_applicant.html'
	, 'forms/ama/datamart/common/married_couple/c_married_couple'
	, 'forms/base/h_constraints'
], function (c_fastened, tpl, c_married_couple, h_constraints) {
	return function (options_arg) {
		var options = {
			field_spec: {
				'Супруги':
					{
						controller: function() { return c_married_couple({ readonly: true }) }
						, title: 'Информация о должнике'
					}
			}
		};
		var controller = c_fastened(tpl, options);
		controller.size = { width: 920, height: 300 };

		var base_Render = controller.Render;
		controller.Render = function (sel) {
			this.readonly = options_arg && options_arg.readonly;
			this.GetCurrentType = options_arg && options_arg.GetCurrentType;
			this.GetMarriedCouple = options_arg && options_arg.GetMarriedCouple;
			this.PrepareToBaseRender();
			base_Render.call(this, sel);
			var self = this;
	
			$(sel + ' select.type').change(function () { self.OnSelectType(); });
			$(sel + ' [data-fc-selector="Заявитель_должник"]').change(function () { self.OnChangeIsDebtorApplicant(); });

			this.PrepareForm();
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function () {
			var applicant = base_GetFormContent.call(this);
			if (applicant) {
				switch (applicant.Тип) {
					case 'Физ_лицо':
						if (applicant.Юр_лицо)
							delete applicant.Юр_лицо;
						break;
					case 'Юр_лицо':
						if (applicant.Физ_лицо)
							delete applicant.Физ_лицо;
						break;
				}
				if(applicant.Заявитель_должник) {
					applicant.Тип = '';
				}
			}

			delete applicant.Супруги;

			return applicant;
		}

		controller.SetReadOnly = function () {
			var sel = this.fastening.selector;
			$(sel + ' div.select2').select2('enable', false);
			$(sel + ' input[type="text"]').prop('disabled', true);
			$(sel + ' input[type="checkbox"]').prop('disabled', true);
			$(sel + ' select').prop('disabled', true);
		}

		controller.OnSelectType = function () {
			var sel = this.fastening.selector;
			var type = $(sel + ' select.type').val();
			var root_item = $(sel + ' div.cpw-ama-datamart-applicant');
			switch (type) {
				case 'Юр_лицо': root_item.removeClass('citizen').addClass('organization'); break;
				case 'Физ_лицо': root_item.removeClass('organization').addClass('citizen'); break;
			}
		}

		var trim = function (txt) {
			return txt.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
		}

		controller.OnChangeIsDebtorApplicant = function () {
			var sel= this.fastening.selector;
			var checked= $(sel + ' [data-fc-selector="Заявитель_должник"]').attr('checked');

			if ('checked' != checked)
			{
				if(this.options.isMarriageCouple)
					this.UnsetMarriageApplicant();	
				$(sel + ' div.select2').select2('enable', true);
				$(sel + ' input[type="text"]').prop('disabled', false);
				$(sel + ' select').prop('disabled', false);
			}
			else
			{
				if(this.options.isMarriageCouple) {
					if(!$(sel + ' select.type option[value="Супруги"]').length)
						$(sel + ' select.type').append(new Option("Супруги", "Супруги"));
					this.SetMarriageApplicant();
				}
				this.ResetFields();
				$(sel + ' div.select2').select2('enable', false);
				$(sel + ' input[type="text"]').prop('disabled', true);
				$(sel + ' select').prop('disabled', true);
			}
		}

		controller.ResetFields = function () {
			var sel = this.fastening.selector;
			$(sel + ' [data-fc-selector="Юр_лицо.Наименование"]').val('');
			$(sel + ' [data-fc-selector="Физ_лицо.Фамилия"]').val('');
			$(sel + ' [data-fc-selector="Физ_лицо.Имя"]').val('');
			$(sel + ' [data-fc-selector="Физ_лицо.Отчество"]').val('');
			$(sel + ' [data-fc-selector="ИНН"]').val('');
			$(sel + ' [data-fc-selector="ОГРН"]').val('');
			$(sel + ' [data-fc-selector="Физ_лицо.СНИЛС"]').val('');
			$(sel + ' [data-fc-selector="Адрес"]').val('');
		}

		controller.Validate = function () {
			var applicant = this.GetFormContent();
			var vr = [];
			if(applicant.Заявитель_должник) return null;
			var block = function (msg) { vr.push({ check_constraint_result: false, description: msg }); }
			switch (applicant.Тип) {
				case 'Юр_лицо':
					if ('' == trim(applicant.Юр_лицо.Наименование))
						block('Необходимо указать наименование заявителя!');
					break;
				case 'Физ_лицо':
					if ('' == trim(applicant.Физ_лицо.Фамилия)) {
						block('Необходимо указать фамилию заявителя!');
					} else if(h_constraints.recommend_Ciryllic().check(applicant.Физ_лицо.Фамилия)==='recommended') {
						block(h_constraints.recommend_Ciryllic().description(null, 'Фамилия'));
					}
					if (applicant.Физ_лицо.Имя && h_constraints.recommend_Ciryllic().check(applicant.Физ_лицо.Имя) === 'recommended')
						block(h_constraints.recommend_Ciryllic().description(null, 'Имя'));
					if (applicant.Физ_лицо.Отчество && h_constraints.recommend_Ciryllic().check(applicant.Физ_лицо.Отчество) === 'recommended')
						block(h_constraints.recommend_Ciryllic().description(null, 'Отчество'));
					if (applicant.Физ_лицо.СНИЛС && !h_constraints.Snils().check(applicant.Физ_лицо.СНИЛС))
						block(h_constraints.Snils().description(null, 'СНИЛС'));
					break;
			}
			if (applicant.ИНН && !h_constraints.Inn().check(applicant.ИНН))
				block(h_constraints.Inn().description(null, 'ИНН'));
			if (applicant.ОГРН && !h_constraints.Ogrn().check(applicant.ОГРН))
				block(h_constraints.Ogrn().description(null, 'ОГРН'));

			return 0 == vr.length ? null : vr;
		}

		controller.PrepareForm = function () {
			var sel = this.fastening.selector;
			if(this.readonly) { this.SetReadOnly(); }
			if(this.model) {
				if(this.model.Заявитель_должник)
					this.OnChangeIsDebtorApplicant();
			}
			if(this.options.isMarriageCouple && (this.model && this.model.Заявитель_должник))
				this.SetMarriageApplicant();
		}

		controller.UnsetMarriageApplicant = function () {
			var sel = this.fastening.selector;
			$(sel + ' .form').show()
			$(sel + ' .marriage_couple').hide();
			$(sel + ' select.type').val('Физ_лицо');
			$(sel + ' select.type' + ' option[value=Супруги]').remove();
		}

		controller.SetMarriageApplicant = function () {
			var sel = this.fastening.selector;
			$(sel + ' .form').hide()
			$(sel + ' .marriage_couple').show();
			$(sel + ' select.type').val('Супруги');
			$(sel + ' .applicant-is-debtor-label').html('Заявителями являются сами должники');
			$(sel + ' [data-fc-selector="Заявитель_должник"]').prop('checked', true);
		}

		controller.PrepareToBaseRender = function () {
			if(!this.model) {
				this.model = {}
				this.model.Заявитель_должник = true;
			}
			if(this.GetCurrentType && this.GetCurrentType() === 'Супруги') {
				options.isMarriageCouple = true;
				if(this.GetMarriedCouple) {
					var married_couple = this.GetMarriedCouple();
					if(married_couple.Супруги)
						this.model.Супруги = married_couple.Супруги;
				}
			}
		}

		return controller;
	}
});