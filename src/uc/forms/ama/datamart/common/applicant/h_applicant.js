define([], function() {
	return {
		GetDefaultObject: function() {
			return {
				Заявитель_должник: true
			}
		},
		Text: {
			singular: 'Заявителем является должник'
			, plural: 'Заявителями являются должники'
		}
	}
});