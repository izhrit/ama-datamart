define([
	'forms/base/h_spec'
	, 'forms/ama/datamart/common/cust_query/spec_custom_query'
	, 'forms/ama/datamart/common/push/spec_ama_dm_push'
	, 'forms/ama/datamart/common/google_calendar/spec_ama_dm_google_calendar'
],function (h_spec,spec_custom_query,spec_ama_dm_push, spec_ama_dm_google_calendar)
{
	var spec_local= {

		controller: {
			"applicant": {
				path: 'forms/ama/datamart/common/applicant/c_applicant'
				, title: 'Заявитель'
			}
			,"court_decision": {
				path: 'forms/ama/datamart/common/court_decision/c_dm_court_decision'
				, title: 'Добавление ссылки на Судебный акт о запросе кандидатур АУ'
			}
			,"debtor_physical": {
				path: 'forms/ama/datamart/common/debtor_physical/c_dm_debtor_physical'
				, title: 'Информация о должнике - физическом лице'
			}
			,"efrsb_report": {
				path: 'forms/ama/datamart/common/efrsb_report/c_dm_efrsb_report'
				, title: 'Публикация на ЕФРСБ о введении процедуры'
			}
			,"married_couple": {
				path: 'forms/ama/datamart/common/married_couple/c_married_couple'
				, title: 'Информация о супругах'
			}
			,"mrequest": {
				path: 'forms/ama/datamart/common/mrequest/c_mrequest'
				, title: 'Запрос от СРО'
			}
			,"news": {
				path: 'forms/ama/datamart/common/news/c_news'
				, title: 'Новости витрины данных'
			}
			, "dm_request_consents": {
				path: 'forms/ama/datamart/common/request_consents/c_request_consents'
				, title: 'согласия АУ'
			}
			, "schedule": {
				path: 'forms/ama/datamart/common/schedule/c_dm_schedule'
				, title: 'календарь событий наблюдателя'
			}
			, "start": {
				path: 'forms/ama/datamart/common/start/c_start'
				, title: 'Заявление о банкротстве'
			}
			,"dm_debtor": {
				path: 'forms/ama/datamart/common/debtor/c_dm_debtor'
				, title: 'форма должника (сопровождение)'
			}
			,"dm_debtor_add_info": {
				path: 'forms/ama/datamart/common/debtor/add_info/c_dm_debtor_add_info'
				, title: 'форма должника (сопровождение)'
			}
			,"dm_debtor_address_for_application": {
				path: 'forms/ama/datamart/common/debtor/address_for_application/c_dm_debtor_address_for_application'
				, title: 'форма должника (сопровождение)'
			}
			,"dm_debtor_info": {
				path: 'forms/ama/datamart/common/debtor/info/c_dm_debtor_info'
				, title: 'форма должника (сопровождение)'
			}
			, "unsubscribe": {
				path: 'forms/ama/datamart/common/unsubscribe/c_unsubscribe'
				, title: 'Отписка от рассылки'
			}
			,"dm_debtor_full_doc": {
				path: 'forms/ama/datamart/common/debtor_full/doc/main/c_dm_debtor_doc'
				, title: 'создание/редактирование прилагаемого документа'
			}
			,"dm_debtor_full_doc_detail": {
				path: 'forms/ama/datamart/common/debtor_full/doc_detail/c_dm_debtor_doc_detail_wrapper'
				, title: 'прилагаемый документ должника детально'
			}
			,"dm_debtor_full_doc_status": {
				path: 'forms/ama/datamart/common/debtor_full/doc_status/c_dm_debtor_doc_status'
				, title: 'состояние прилагаемого документа'
			}
			,"dm_debtor_full_docs_list": {
				path: 'forms/ama/datamart/common/debtor_full/docs_list/c_dm_debtor_docs_list'
				, title: 'список прилагаемых документов должника'
			}
			,"dm_debtor_full_params": {
				path: 'forms/ama/datamart/common/debtor_full/params/c_dm_debtor_params'
				, title: 'параметры должника'
			}
			,"dm_debtor_full": {
				path: 'forms/ama/datamart/common/debtor_full/c_dm_debtor_full'
				, title: 'полная иформация о должнике'
			}
			,"dm_debtor_full_doc_places_to_find": {
				path: 'forms/ama/datamart/common/debtor_full/doc/places_to_find/c_dm_debtor_doc_places_to_find'
			}
			,"choose_efrsb_msg_type": {
				path: 'forms/ama/datamart/common/choose_efrsb_msg_type/c_choose_efrsb_msg_type'
			}
		}

		, content: {
			"applicant-01sav": {
				path: 'txt!forms/ama/datamart/common/applicant/tests/contents/01sav.json.etalon.txt'
				, title: 'Данные о заявителе'
			}
			, "dm_court_decision_01sav": {
				path: 'txt!forms/ama/datamart/common/court_decision/tests/contents/01sav.json.etalon.txt'
				, title: 'Ссылка на определение'
			}
			, "dm_debtor_physical_01sav": {
				path: 'txt!forms/ama/datamart/common/debtor_physical/tests/contents/01sav.json.etalon.txt'
				, title: 'Информация о должнике - физическом лице'
			}
			, "dm_efrsb_report_01sav": {
				path: 'txt!forms/ama/datamart/common/efrsb_report/tests/contents/01sav.json.etalon.txt'
				, title: 'Публикация на ЕФРСБ о введении процедуры'
			}
			, "dm_married_couple_01sav": {
				path: 'txt!forms/ama/datamart/common/married_couple/tests/contents/01sav.json.etalon.txt'
				, title: 'Данные о супругах'
			}
			,"mrequest-01sav": {
				path: 'txt!forms/ama/datamart/common/mrequest/tests/contents/01sav.json.etalon.txt'
				, title: 'Данные о запросе'
			}
			,"dm_request_consents_01sav": {
				path: 'txt!forms/ama/datamart/common/request_consents/tests/contents/01sav.json.etalon.txt'
				, title: 'Согласия АУ'
			}
			,"start-01sav": {
				path: 'txt!forms/ama/datamart/common/start/tests/contents/01sav.json.etalon.txt'
				, title: 'Заявление о банкротстве 01sav'
			}
			,"start-09sav-married": {
				path: 'txt!forms/ama/datamart/common/start/tests/contents/09sav-married.json.etalon.txt'
				, title: 'Заявление о банкротстве (супруги)'
			}
			,"dm_debtor-01sav": {
				path: 'txt!forms/ama/datamart/common/debtor/tests/contents/01sav.json.etalon.txt'
				, title: 'форма сопровождаемого'
			}
			,"dm_debtor_add_info-01sav": {
				path: 'txt!forms/ama/datamart/common/debtor/add_info/tests/contents/01sav.json.etalon.txt'
				, title: 'форма сопровождаемого (дополнительная информация о должнике)'
			}
			,"dm_debtor_address_for_application-01sav": {
				path: 'txt!forms/ama/datamart/common/debtor/address_for_application/tests/contents/01sav.json.etalon.txt'
				, title: 'форма сопровождаемого (адрес для заявления)'
			}
			,"dm_debtor_info-01sav": {
				path: 'txt!forms/ama/datamart/common/debtor/info/tests/contents/01sav.json.etalon.txt'
				, title: 'форма сопровождаемого (информация о должнике)'
			}
			, "dm_debtor_full_doc-01sav": {
				path: 'txt!forms/ama/datamart/common/debtor_full/doc/main/tests/contents/01sav.json.etalon.txt'
			}
			, "dm_debtor_full_doc_status-01sav": {
				path: 'txt!forms/ama/datamart/common/debtor_full/doc_status/tests/contents/01sav.json.etalon.txt'
			}
			,"dm_debtor_full_doc_places_to_find-01fill": {
				path: 'txt!forms/ama/datamart/common/debtor_full/doc/places_to_find/tests/contents/01fill.json.etalon.txt'
			}
			,"choose_efrsb_msg_type-implemented_t": {
				path: 'txt!forms/ama/datamart/common/choose_efrsb_msg_type/tests/contents/implemented_t.json.txt'
			}
			,"choose_efrsb_msg_type-actual_t": {
				path: 'txt!forms/ama/datamart/common/choose_efrsb_msg_type/tests/contents/actual_t.json.txt'
			}
			,"choose_efrsb_msg_type-tree_mode": {
				path: 'txt!forms/ama/datamart/common/choose_efrsb_msg_type/tests/contents/tree_mode.json.txt'
			}
		}

	};
	return h_spec.combine(spec_custom_query,spec_local,spec_ama_dm_push, spec_ama_dm_google_calendar);
});