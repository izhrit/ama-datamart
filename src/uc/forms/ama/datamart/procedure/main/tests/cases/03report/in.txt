wait_loaded
wait_text_disappeared "Редактировать модель в элементе управления"
click_text "Отчёт"
shot_check_png ..\..\shots\02report.png
je $(".dm-selector > button").click();
wait_text "encoding"
shot_check_png ..\..\shots\02report_raw.png
wait_click_text "Закрыть"
je $($("a.head-tab")[1]).click();
shot_check_png ..\..\shots\02report.png
exit