define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/main/e_dm_procedure.html'
	, 'forms/ama/datamart/procedure/raw/h_dm_procedure_raw'
	, 'forms/ama/datamart/procedure/section/base/h_sections'
	, 'forms/ama/datamart/procedure/section/base/x_sections'
	, 'forms/ama/datamart/procedure/section/efrsb/c_procedure_efrsb'
],
function (c_fastened, tpl, h_dm_procedure_raw, h_section, x_sections, c_procedure_efrsb)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_dbinfo = base_url + '?action=procedure.dbinfo'

		var controller = c_fastened(tpl, { h_section: h_section } );

		controller.ClearSections= function()
		{
			this.section= [];
			for (var i = 0; i < h_section.length; i++)
				this.section.push( { raw: '', parsed:null } );
		}
		controller.ClearSections();

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' div.dm-selector button#raw-view').click(function (e) { h_dm_procedure_raw.OnRawView(base_url_dbinfo, self.model); });
			$(sel + ' div.dm-selector a.head-tab').click(function (e) { e.preventDefault(); self.OnChangeSection(e); })

			this.ShowAnyProcedure();
		}

		controller.UseCodec(x_sections);

		controller.ShowAnyProcedure= function()
		{
			var sel = this.fastening.selector;

			$(sel + ' div.dm-selector a.head-tab').removeClass('active');
			$(sel + ' div.dm-selector a.head-tab').removeClass("nonempty")

			$(sel + ' div.dm-selector a.head-tab[isection="efrsb"]').addClass("nonempty")
			if (null == this.controller) this.ShowSection('efrsb');

			if (null != this.model && null != null != this.model.section)
			{
				for (var i = 0; i < this.model.section.length; i++)
				{
					var section = this.model.section[i];
					if (null != section.decoded)
					{
						$(sel + ' div.dm-selector a.head-tab[isection="' + i + '"]').addClass('nonempty');
					}
				}
			}
		}

		controller.ShowSection= function(isection)
		{
			if ('efrsb' == isection) {
				this.ShowEfrsb();
			}
			else if (null != this.model.section[isection].decoded)
			{
				var sel = this.fastening.selector;
				$(sel + ' div.dm-selector a.head-tab').removeClass("active");
				$(sel + ' div.dm-selector a.head-tab[isection="' + isection + '"]').addClass('active');

				if (null != this.controller)
				{
					this.controller.Destroy();
					this.controller = null;
				}
				var controller = this.controller = h_section[isection].controller();
				controller.SetFormContent(this.model.section[isection].decoded);
				controller.Edit(sel + ' div.body');
			}
		}

		controller.ShowEfrsb = function () {
			var sel = this.fastening.selector;
			$(sel + ' div.dm-selector a.head-tab').removeClass("active");
			$(sel + ' div.dm-selector a.head-tab[isection="efrsb"]').addClass('active');

			if (null != this.controller) {
				this.controller.Destroy();
				this.controller = null;
			}
			var controller = this.controller = c_procedure_efrsb(options_arg);
			var efrsb_model = {};
			if (this.model && this.model.id_Procedure)
				efrsb_model.id_Procedure = this.model.id_Procedure;
			controller.SetFormContent(efrsb_model);
			controller.Edit(sel + ' div.body');
		}

		controller.OnChangeSection = function (e)
		{
			var target = $(e.currentTarget);
			this.ShowSection(target.attr('isection'));
		}

		return controller;
	}
});
