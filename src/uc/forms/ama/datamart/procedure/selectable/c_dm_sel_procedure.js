define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/selectable/e_dm_sel_procedure.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/procedure/main/c_dm_procedure'
	, 'forms/ama/datamart/base/h_show_hide_search'
],
function (c_fastened, tpl, h_msgbox, c_dm_procedure, h_show_hide_search)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_select2 = base_url + '?action=procedure.select2&version=1'
		var base_url_info= base_url + '?action=procedure.info&section=*'

		var options = {
			field_spec: 
				{
					Выбранная_процедура: { ajax: { url: base_url_select2, dataType: 'json' } }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			if (this.model)
			{
				if (this.model.id_Manager)
					options.field_spec.Выбранная_процедура.ajax.url += '&id_Manager=' + this.model.id_Manager;
				if (this.model.id_Contract)
					options.field_spec.Выбранная_процедура.ajax.url += '&id_Contract=' + this.model.id_Contract;
				if (this.model.id_MUser)
					options.field_spec.Выбранная_процедура.ajax.url += '&id_MUser=' + this.model.id_MUser;
			}

			base_Render.call(this, sel);

			h_show_hide_search.BindShowHideSearchButton(sel);

			var self = this;
			$(sel + ' [data-fc-selector="Выбранная_процедура"]').on('select2-selected', function (e) { self.OnSelectProcedure(); });
		}

		controller.SelectProcedure = function (selected_data, on_done)
		{
			var sel = this.fastening.selector;
			var select2 = $(sel + ' [data-fc-selector="Выбранная_процедура"]');
			select2.select2('data', selected_data);
			this.LoadProcedure(selected_data.id, on_done);
		}

		controller.OnSelectProcedure= function()
		{
			var sel = this.fastening.selector;
			var selected_data = $(sel + ' [data-fc-selector="Выбранная_процедура"]').select2('data');
			this.LoadProcedure(selected_data.id);
		}

		controller.LoadProcedure= function(id_Procedure, on_done)
		{
			id_Procedure = '' + id_Procedure;
			if ('r' == id_Procedure.charAt(0))
			{
				this.ShowRequest(id_Procedure);
			}
			else if ('e' == id_Procedure.charAt(0))
			{
				this.ShowEfrsbProcedure(id_Procedure);
			}
			else
			{
				var sel = this.fastening.selector;
				var self = this;
				var ajaxurl = base_url_info + '&id_MProcedure=' + id_Procedure;
				var v_ajax = h_msgbox.ShowAjaxRequest("Загрузка данных о процедуре с сервера", ajaxurl);
				v_ajax.ajax({
					dataType: "text"
					, type: 'GET'
					, processData: false
					, cache: false
					, success: function (procedure_info, textStatus)
					{
						self.ShowProcedure(id_Procedure, procedure_info, on_done);
					}
				});
			}
		}

		controller.ShowRequest = function (id_Request)
		{
			var sel = this.fastening.selector;
			if (null != this.controller)
			{
				this.controller.Destroy();
				this.controller = null;
			}
			$(sel + ' div.procedure-selector div.search-bar').show();
			this.controller = c_dm_procedure({ base_url: base_url });
			this.controller.SetFormContent({ id_Procedure: id_Request });
			this.controller.Edit(sel + ' div.selected-procedure');
		}

		controller.ShowEfrsbProcedure = function (id_Debtor_Manager) {
			var sel = this.fastening.selector;
			if (null != this.controller) {
				this.controller.Destroy();
				this.controller = null;
			}
			$(sel + ' div.procedure-selector div.search-bar').show();
			this.controller = c_dm_procedure({ base_url: base_url });
			this.controller.SetFormContent({ id_Procedure: id_Debtor_Manager });
			this.controller.Edit(sel + ' div.selected-procedure');
		}

		controller.ShowProcedure= function(id_Procedure, procedure_info, on_done)
		{
			var sel = this.fastening.selector;
			if (null!=this.controller)
			{
				this.controller.Destroy();
				this.controller = null;
			}
			$(sel + ' div.procedure-selector div.search-bar').show();
			this.controller = c_dm_procedure({ base_url: base_url });
			this.controller.SetFormContent({ id_Procedure: id_Procedure, procedure_info: procedure_info });
			this.controller.Edit(sel + ' div.selected-procedure');
			if (on_done) on_done();
		}

		return controller;
	}
});
