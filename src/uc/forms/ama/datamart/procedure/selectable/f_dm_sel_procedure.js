define(['forms/ama/datamart/procedure/selectable/c_dm_sel_procedure'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'datamart_orpau'
		, Title: 'Информация витрины данных ПАУ для кабинета ОРПАУ'
	};
	return form_spec;
});
