define([
	'forms/base/h_spec'
	, 'forms/ama/datamart/procedure/section/spec_dm_proc_section'
],
function (h_spec, sections_spec)
{
	var dm_procedure_specs = {

		controller: {
		  "dm_procedure":        { path: 'forms/ama/datamart/procedure/main/c_dm_procedure' }
		, "dm_procedure_raw":    { path: 'forms/ama/datamart/procedure/raw/c_dm_procedure_raw' }
		, "dm_sel_procedure":    { path: 'forms/ama/datamart/procedure/selectable/c_dm_sel_procedure' }
		}

		, content: {
		  "raw-full-organization": { path: 'txt!forms/ama/datamart/procedure/raw/tests/contents/full_organization.json.txt' }
		, "raw-full-person":       { path: 'txt!forms/ama/datamart/procedure/raw/tests/contents/full_person.json.txt' }
		, "raw-registry":          { path: 'txt!forms/ama/datamart/procedure/raw/tests/contents/registry.json.txt' }
		, "raw-report":            { path: 'txt!forms/ama/datamart/procedure/raw/tests/contents/report.json.txt' }
		, "raw-nofiles":           { path: 'txt!forms/ama/datamart/procedure/raw/tests/contents/nofiles.json.txt' }

		, "proc-empty":            { path: 'txt!forms/ama/datamart/procedure/main/tests/contents/empty.txt' }
		, "proc-bad":              { path: 'txt!forms/ama/datamart/procedure/main/tests/contents/bad.txt' }
		, "proc-registry":         { path: 'txt!forms/ama/datamart/procedure/main/tests/contents/registry.txt' }
		, "proc-full":             { path: 'txt!forms/ama/datamart/procedure/main/tests/contents/full.txt' }
		, "proc-report":           { path: 'txt!forms/ama/datamart/procedure/main/tests/contents/report.txt' }
		}

	};

	return h_spec.combine(dm_procedure_specs, sections_spec);
});