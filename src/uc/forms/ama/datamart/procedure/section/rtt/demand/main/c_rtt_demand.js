﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/section/rtt/demand/main/e_rtt_demand.html'
	, 'forms/ama/datamart/procedure/section/rtt/demand/common/c_dm_rtt_demand_common'
	, 'forms/ama/datamart/procedure/section/rtt/demand/creditor/c_rtt_creditor'
],
function (c_fastened, tpl
	, c_rtt_demand_common
	, c_rtt_creditor
	)
{
	return function ()
	{
		var options = {
			field_spec:
				{
					Общее: { controller: c_rtt_demand_common }
					, Кредитор: { controller: c_rtt_creditor }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.size = { width: 650, height: 500 };

		return controller;
	}
});