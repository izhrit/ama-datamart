define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/procedure/section/rtt/paged/e_rtt_paged.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/procedure/section/rtt/paged/a_dm_rtt_jqgrid'
	, 'forms/ama/datamart/procedure/section/rtt/base/x_rtt'
	, 'forms/base/h_number_format'
	, 'forms/ama/datamart/base/h_debtorName'
	, 'forms/ama/datamart/procedure/section/rtt/paged/a_dm_rtt_demand'
	, 'forms/ama/datamart/procedure/section/rtt/demand/main/c_rtt_demand'
],
function (c_fastened, tpl, h_msgbox, a_dm_rtt_jqgrid, x_rtt, h_number_format, h_debtorName, a_dm_rtt_demand, c_rtt_demand)
{
	var ajax_transport_included = false;
	return function()
	{
		var sumFormatter = function (cellvalue, options, rowObject)
		{
			return h_number_format(rowObject.Сумма, 2, ' ,', ' ');
		}

		var creditorNameFormatter = function (cellvalue, options, rowObject)
		{
			return h_debtorName.beautify_debtorName(rowObject.Кредитор);
		}

		var colModel =
		[
			  { label: '№', name: 'Номер', width: 30, sortable: true, searchoptions: { sopt: ['cn'] }, align: 'center' }
			, { label: 'Оч.', name: 'Очередь', width: 30, sortable: true, align: 'center', searchoptions: { sopt: ['cn'] } }
			, { label: 'Назначение', name: 'Назначение', width: 55, sortable: true, searchoptions: { sopt: ['cn'] } }
			, { label: 'Кредитор', name: 'Кредитор', sortable: true, searchoptions: { sopt: ['cn'] }, formatter: creditorNameFormatter }
			, { label: 'Размер (руб.)', name: 'Сумма', width: 80, sortable: true, searchoptions: { sopt: ['cn'] }, align: 'right', formatter: sumFormatter }
			, { label: 'Дата возн.', name: 'Дата_возникновения', width: 50, sortable: true, search: false, align: 'center' }
		];

		var controller = c_fastened(tpl);

		controller.colModel = colModel;

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();

			$('#gridPager > div > table > tbody > tr > td#gridPager_right').remove();
			$('#gridPager > div > table > tbody > tr > td#gridPager_center').attr('colspan', '2');

			$('.dm-selector button').hide();
			$("<button>", {
				text: 'Необработанное представление',
				click: function() {
					$('.dm-selector button#raw-view').trigger('click');
				}
			})
			.addClass("button")
			.prependTo(
				$('#gridPager table tr td#gridPager_left').append()
			);
		}

		controller.selection = '1-я очередь';
		controller.base_grid_url = 'ama/rtt/items';
		controller.PrepareUrl= function()
		{
			return (''==this.selection)
				? (this.base_grid_url)
				: (this.base_grid_url + '/' + this.selection);
		}

		controller.RenderGrid = function ()
		{
			var self = this;
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				  datatype: "json"
				, url: self.PrepareUrl()
				, colModel: this.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: ''
				, emptyrecords: ''
				, emptyText: 'Нет требований удовлетворяющих условиям фильтрации для просмотра'
				, rownumbers: false
				, rowNum: 10
				, rowList: [10, 20, 50, 100]
				, pager: '#gridPager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, ignoreCase: true
				, onSelectRow: function () { self.OnDemand(); }
				, loadError: function (xhr, status, error) { self.OnLoadError(xhr, status, error); }
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnLoadError = function (xhr, status, error)
		{
			var self = this;
			h_msgbox.ShowAjaxError("Ошибка получения текущих требований: " + error, self.PrepareUrl(), xhr.responseText, status);
		}

		controller.OnDemand= function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;
			var ajaxurl = 'ama/rtt/demand?demand_num=' + rowdata.Номер;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных о текущем требовании с сервера", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						var c_demand = c_rtt_demand();
						c_demand.SetFormContent(data);
						h_msgbox.ShowModal
						({
							title: 'Информация о текущем требовании'
							, controller: c_demand
							, buttons: ['Закрыть']
							, id_div: "cpw-form-ama-datamart-demand"
						});
					}
				}
			});
		}

		controller.ChangeSelection= function(selection)
		{
			this.selection= selection;
			if (!selection || ''==selection)
			{
				var title1 = 'Все требования';
				var title2 = '';
			}
			else
			{
				var parts = selection.split('$');
				if (1==parts.length)
				{
					var title1 = parts[0];
					var title2 = '';
				}
				else
				{
					var title1 = parts[0];
					var title2 = ' - ' + parts[1];
				}
			}
			var self = this;
			var sel = this.fastening.selector;
			$(sel + ' div.ama-rtt-paged span.title1').text(title1);
			$(sel + ' div.ama-rtt-paged span.title2').text(title2);

			var grid = $(sel + ' table.grid');
			grid.setGridParam({ page: 1, url: self.PrepareUrl() }).trigger("reloadGrid");

			$(window).resize();
		}

		var base_SetFormContent = controller.SetFormContent;
		controller.SetFormContent = function (model)
		{
			a_dm_rtt_demand.options = { url_prefix: 'ama/rtt/demand', model: model };
			a_dm_rtt_jqgrid.options.url_prefix= 'ama/rtt/items';
			a_dm_rtt_jqgrid.options.model= model;
			if (!ajax_transport_included)
			{
				ajax_transport_included = true;
				$.ajaxTransport('+*', a_dm_rtt_jqgrid.prepare_try_to_prepare_send_abort());
				$.ajaxTransport('+*', a_dm_rtt_demand.prepare_try_to_prepare_send_abort());
			}
			return base_SetFormContent.call(this, model);
		}

		controller.UseCodec(x_rtt());

		return controller;
	}
});
