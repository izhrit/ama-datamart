﻿define(function ()
{
	var helper = {};

	helper.ДляВсехТребований = function (rtt, on_demand)
	{
		var tnum = 1;
		if (rtt.Кредиторы)
		{
			var Кредиторы = rtt.Кредиторы;
			for (var i = 0; i < Кредиторы.length; i++)
			{
				var Кредитор = Кредиторы[i];
				var Требования = Кредитор.Требования;
				for (var j = 0; j < Требования.length; j++)
				{
					var Требование = Требования[j];
					var tnum_Требование = tnum;
					if ('другие требования не нужны' == on_demand(tnum_Требование, Кредитор, Требование))
						return;
					tnum++;
				}
			}
		}
	}

	var empty_queue = { "Кредиторов": 0, "Требований": 0, "На_сумму": 0 };
	helper.default_Summary = {
		"Кредиторов": 0
		, "1-я очередь": empty_queue
		, "2-я очередь": empty_queue
		, "3-я очередь": empty_queue
		, "4-я очередь": empty_queue
		, "5-я очередь": empty_queue
	};

	helper.Summary_path = function (Требование)
	{
		return [Требование.Очередь + "-я очередь"];
	}

	return helper;
});