define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/section/rtt/main/e_rtt.html'
	, 'forms/ama/datamart/procedure/section/rtt/summary/c_rtt_summary'
	, 'forms/ama/datamart/procedure/section/rtt/paged/c_rtt_paged'
],
	function (c_fastened, tpl, c_summary, c_paged)
{
	return function()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.paged = c_paged();
			this.summary = c_summary();

			if (!this.model)
			{
				this.paged.CreateNew(sel + ' div.ama-datamart-rtt-main div.grid');
				this.summary.CreateNew(sel + ' div.ama-datamart-rtt-main div.selector');
			}
			else
			{
				this.paged.SetFormContent(this.model);
				this.summary.SetFormContent(this.model);

				this.paged.Edit(sel + ' div.ama-datamart-rtt-main div.grid');
				this.summary.Edit(sel + ' div.ama-datamart-rtt-main div.selector');
			}

			var self = this;
			this.summary.OnChangeSelection = function (selection) { self.paged.ChangeSelection(selection); }
		}

		controller.SetFormContent = function (model)
		{
			this.model = model;
		}

		return controller;
	}
});
