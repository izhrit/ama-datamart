﻿define([
	  'forms/base/codec/xml/codec.xml'
	, 'forms/base/codec/codec.copy'
],
function (BaseCodec, codec_copy)
{
	return function ()
	{
		var res = BaseCodec();

		res.schema =
		{
			tagName: 'Оперативно_рабочая_информация_о_текущих_требованиях'
			, fields:
			{
				'Кредиторы':
				{
					type: 'array'
					, item:
					{
						tagName: 'Кредитор'
						, fields:
						{
							'Требования':
							{
								type: 'array'
								, item:
								{
									tagName: 'Требование'
								}
							}
						}
					}
				}
			}
		};

		var base_Decode = res.Decode;
		res.Decode= function(d)
		{
			return ('string' !== typeof d) ? d : base_Decode.call(this,d);
		}

		return res;
	}

});
