define([
	  'forms/base/codec/codec'
	, 'forms/ama/datamart/procedure/section/rtt/base/h_rtt'
	, 'forms/base/codec/codec.copy'
],
function (base_codec, h_rtt, codec_copy)
{
	return function ()
	{
		var codec = base_codec();

		codec.Encode = function (rtt)
		{
			var summary = codec_copy().Copy(h_rtt.default_Summary)
			if (rtt)
			{
				var текущий_Кредитор = null;
				var текущий_Кредитор_nodes = {};
				h_rtt.ДляВсехТребований(rtt,
					function (tnum, Кредитор, Требование)
					{
						var path = h_rtt.Summary_path(Требование);
						if (текущий_Кредитор != Кредитор)
							текущий_Кредитор_nodes = {};
						var txt_path = 'root';
						var node = summary;
						if (!текущий_Кредитор_nodes[txt_path])
						{
							текущий_Кредитор_nodes[txt_path] = true;
							node.Кредиторов++;
						}
						for (var i = 0; i < path.length; i++)
						{
							txt_path += '.' + path[i];
							node = node[path[i]];
							if (!текущий_Кредитор_nodes[txt_path])
							{
								текущий_Кредитор_nodes[txt_path] = true;
								node.Кредиторов++;
							}
						}
						var сумма = checkAndCorrectNumber(Требование.Размер ? Требование.Размер : Требование.Сумма);
						node.На_сумму += сумма;
						node.Требований++;
						текущий_Кредитор = Кредитор;
					}
				);
			}
			return summary;
		}

		function checkAndCorrectNumber(str){
			if(typeof str === 'string')
				return Number(str.replace(/,/, "."));

			return str;
		}

		return codec;
	}
});