define(function ()
{
	return {

		controller:
		{
			"dm_rtt": { path: 'forms/ama/datamart/procedure/section/rtt/main/c_rtt' }
			, "dm_rtt_summary": { path: 'forms/ama/datamart/procedure/section/rtt/summary/c_rtt_summary' }
			, "dm_rtt_paged": { path: 'forms/ama/datamart/procedure/section/rtt/paged/c_rtt_paged' }

			, "dm_rtt_demand_common": { path: 'forms/ama/datamart/procedure/section/rtt/demand/common/c_dm_rtt_demand_common' }
			, "dm_rtt_demand_creditor": { path: 'forms/ama/datamart/procedure/section/rtt/demand/creditor/c_rtt_creditor' }
		}

		, content:
		{
			"rtt-example1": { path: 'txt!forms/ama/datamart/procedure/section/rtt/tests/rtt-example1.xml' }
		}

	};
});