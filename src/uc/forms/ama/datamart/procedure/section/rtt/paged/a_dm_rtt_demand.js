﻿define([
	  'forms/base/codec/url/codec.url'
	, 'forms/base/codec/url/codec.url.args'
	, 'forms/ama/datamart/procedure/section/rtt/base/h_rtt'
],
function (codec_url, codec_url_args, h_rtt)
{
	var url_prefix = 'ama/datamart?action=rtt.demand.info';

	var transport = { options: { url_prefix: url_prefix } };

	transport.prepare_try_to_prepare_send_abort= function()
	{
		var self= this;
		return function (options, originalOptions, jqXHR)
		{
			if (self.options && 0 == options.url.indexOf(self.options.url_prefix))
			{
				var send_abort =
				{
					send: function (headers, completeCallback)
					{
						var decoded_url = codec_url().Decode(options.url);
						var args = codec_url_args().Decode(decoded_url);

						var demand_num = parseInt(args.demand_num);

						var tt = null;
						h_rtt.ДляВсехТребований(self.options.model,
							function (tnum, Кредитор, Требование)
							{
								if (demand_num == tnum)
								{
									tt = {
										Кредитор: Кредитор
										, Требование: Требование
									};
									return 'другие требования не нужны';
								}
							});
						completeCallback(200, 'success', { text: JSON.stringify(tt) });
					}
					, abort: function ()
					{
					}
				}
				return send_abort;
			}
		}
	}

	return transport;
});