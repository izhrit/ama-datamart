﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/section/rtt/demand/common/e_dm_rtt_demand_common.html'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, h_number_format)
{
	return function ()
	{
		var controller = c_fastened(tpl, { h_number_format: h_number_format });

		return controller;
	}
});