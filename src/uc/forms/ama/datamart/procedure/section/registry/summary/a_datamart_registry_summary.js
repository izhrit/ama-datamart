﻿define(['forms/ama/datamart/procedure/section/registry/summary/x_summary'],
function (x_summary)
{
	var url_prefix = 'ama/registry/summary';

	var transport = { options: { url_prefix: 'ama/registry/summary' } };

	var s_x_summary = x_summary();

	transport.prepare_try_to_prepare_send_abort= function()
	{
		var self= this;
		return function (options, originalOptions, jqXHR)
		{
			if (self.options && 0 == options.url.indexOf(self.options.url_prefix))
			{
				var send_abort =
				{
					send: function (headers, completeCallback)
					{
						var res = s_x_summary.Encode(self.options.model);
						completeCallback(200, 'success', { text: JSON.stringify(res) });
					}
					, abort: function ()
					{
					}
				}
				return send_abort;
			}
		}
	}

	return transport;
});