define([
	  'forms/base/codec/codec'
	, 'forms/ama/datamart/procedure/section/registry/base/h_registry'
	, 'forms/base/codec/codec.copy'
],
function (base_codec, h_registry, codec_copy)
{
	return function ()
	{
		var codec = base_codec();

		codec.Encode = function (registry)
		{
			var summary = codec_copy().Copy(h_registry.default_Summary)
			if (registry)
			{
				var текущий_Кредитор = null;
				var текущий_Кредитор_nodes = {};
				h_registry.ДляВсехТребований(registry,
					function (tnum, Кредитор, Требование, родительское_Требование)
					{
						var path = h_registry.Summary_path(Требование, родительское_Требование);
						if (текущий_Кредитор != Кредитор)
							текущий_Кредитор_nodes = {};
						var txt_path = 'root';
						var node = summary;
						if (!текущий_Кредитор_nodes[txt_path])
						{
							текущий_Кредитор_nodes[txt_path] = true;
							node.Кредиторов++;
						}
						for (var i = 0; i < path.length; i++)
						{
							txt_path += '.' + path[i];
							node = node[path[i]];
							if (!текущий_Кредитор_nodes[txt_path])
							{
								текущий_Кредитор_nodes[txt_path] = true;
								node.Кредиторов++;
							}
						}
						var сумма = Number(Требование.Размер ? Требование.Размер : Требование.Сумма);
						if (!Требование.Реестр.Исключено) {
							node.На_сумму += сумма;
						}
						node.Требований++;
						текущий_Кредитор = Кредитор;
					}
				);
			}
			return summary;
		}

		return codec;
	}
});