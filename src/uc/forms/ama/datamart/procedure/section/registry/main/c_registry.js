define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/section/registry/main/e_registry.html'
	, 'forms/ama/datamart/procedure/section/registry/paged/c_registry_paged'
	, 'forms/ama/datamart/procedure/section/registry/summary/c_summary'
	, 'forms/ama/datamart/base/show_msg_under_construction'
],
function (c_fastened, tpl, c_paged, c_summary, show_msg_under_construction)
{
	return function()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.paged = c_paged();
			this.summary = c_summary();

			if (!this.model)
			{
				this.paged.CreateNew(sel + ' div.ama-registry-main div.grid');
				this.summary.CreateNew(sel + ' div.ama-registry-main div.selector');
			}
			else
			{
				this.paged.SetFormContent(this.model);
				this.summary.SetFormContent(this.model);

				this.paged.Edit(sel + ' div.ama-registry-main div.grid');
				this.summary.Edit(sel + ' div.ama-registry-main div.selector');
			}

			var self = this;
			this.summary.OnChangeSelection = function (selection) { self.paged.ChangeSelection(selection); }

			$(sel + ' div.commands button.print').button().click(function (e) { e.preventDefault(); self.OnPrint(); });
		}

		controller.OnPrint = function ()
		{
			show_msg_under_construction();
		}

		controller.SetFormContent = function (model)
		{
			this.model = model;
		}

		return controller;
	}
});
