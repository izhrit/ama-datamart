define(function ()
{

	return {

		controller: {
		  "dm_registry_summary": { path: 'forms/ama/datamart/procedure/section/registry/summary/c_summary' }
		, "dm_registry_paged":   { path: 'forms/ama/datamart/procedure/section/registry/paged/c_registry_paged' }
		, "dm_registry":         { path: 'forms/ama/datamart/procedure/section/registry/main/c_registry' }
		, "dm_demand":           { path: 'forms/ama/datamart/procedure/section/registry/demand/main/c_dm_demand' }
		, "dm_demand_common":    { path: 'forms/ama/datamart/procedure/section/registry/demand/common/c_dm_demand_common' }
		, "dm_demand_creditor":  { path: 'forms/ama/datamart/procedure/section/registry/demand/creditor/c_creditor' }
		}

		, content: {
		  "rtk-example1":          { path: 'txt!forms/ama/datamart/procedure/section/registry/tests/rtk-example1.xml' }
		, "rtk-example1-not-empty-sum":{ path: 'txt!forms/ama/datamart/procedure/section/registry/tests/rtk-example7.xml' }

		, "rtk-example-large":     { path: 'txt!forms/ama/datamart/procedure/section/registry/tests/rtk-example3.xml' }
		, "tk-example-1":          { path: 'txt!forms/ama/datamart/procedure/section/registry/demand/tests/tk-example-1.json.txt' }
		, "tk-example-2":          { path: 'txt!forms/ama/datamart/procedure/section/registry/demand/tests/tk-example-2.json.txt' }
		, "rtk-example-ama":       { path: 'txt!forms/ama/datamart/procedure/section/registry/tests/rtk-example4.xml' }
		, "rtk-example-ama1":      { path: 'txt!forms/ama/datamart/procedure/section/registry/tests/rtk-example5.xml' }
		, "rtk-example-ama-empty": { path: 'txt!forms/ama/datamart/procedure/section/registry/tests/rtk-example6.xml' }

		, "creditor-example-1":    { path: 'txt!forms/ama/datamart/procedure/section/registry/demand/creditor/tests/contents/sample1.json.txt' }
		}

	};

});