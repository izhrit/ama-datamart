﻿define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/codec/codec.copy'
	, 'forms/ama/datamart/procedure/section/registry/base/h_registry'
], function (ajax_jqGrid, codec_copy, h_registry)
{
	var transport = ajax_jqGrid();

	transport.options.CustomParseUrl = function (parsed_url)
	{
		var url_without_prefix = parsed_url.url.substring(parsed_url.prefix.length, parsed_url.url.length);
		var iq = url_without_prefix.indexOf('?');

		var path_part = url_without_prefix.substring(1, iq);
		parsed_url.Summary_path = path_part.split('$');
	}

	var base_RowIsOkForFilter = transport.options.RowIsOkForFilter;
	transport.options.RowIsOkForFilter = function (row, prepared_row, parsed_url)
	{
		if (parsed_url.Summary_path && parsed_url.Summary_path.length && 0 != parsed_url.Summary_path.length)
		{
			for (var i= 0; i<parsed_url.Summary_path.length; i++)
			{
				if (i >= prepared_row.summary_path.length ||
					('?'!=parsed_url.Summary_path[i] &&
					prepared_row.summary_path[i] != parsed_url.Summary_path[i]))
				{
					return false;
				}
			}
		}
		var res = base_RowIsOkForFilter.call(this, row, prepared_row, parsed_url);
		return res;
	}

	transport.rows_all = function ()
	{
		var rows = [];

		var registry = this.options.model;
		h_registry.ДляВсехТребований(registry,
			function (tnum, Кредитор, Требование, родительское_Требование)
			{
				var tk = {
					Очередь: родительское_Требование ? '3' : (Требование.Очередь + ' ').charAt(0)
					, Номер: tnum
					, Сумма: родительское_Требование ? Требование.Сумма : Требование.Размер
					, Вид: родительское_Требование ? 'Проценты на требование' : Требование.Вид_обязательства
				};

				tk.summary_path = h_registry.Summary_path(Требование, родительское_Требование);

				if (Кредитор.Организация)
					tk.Кредитор = Кредитор.Организация.Наименование;

				if (Кредитор.Физическое_лицо)
				{
					var np = Кредитор.Физическое_лицо;
					tk.Кредитор = np.Фамилия + ' ' + np.Имя + ' ' + np.Отчество;
				}

				if (Требование.Реестр && Требование.Реестр.Внесено)
					tk.Дата_включения = Требование.Реестр.Внесено.Дата;

				rows.push(tk);
			});

		return rows;
	}

	return transport;
});