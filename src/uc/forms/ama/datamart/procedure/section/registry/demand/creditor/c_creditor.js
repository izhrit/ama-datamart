define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/section/registry/demand/creditor/e_creditor.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function(sel)
		{
			base_Render.call(this, sel);

			var self = this;

			$(sel + ' input[type="text"]').attr("readonly", "readonly");
			$(sel + ' textarea').attr("readonly", "readonly");
			$(sel + ' select').attr("disabled", "disabled");
			$(sel + ' input[type="checkbox"]').attr("disabled", "disabled");

			var person_type_select = $(sel + ' select.person-type');
			person_type_select.val((!this.model || !this.model.Кредитор || this.model.Кредитор.Организация) ? 'legal' : 'natural');
			self.SetPersonType(person_type_select.val());

			var cbox_structured_bank_data = $(sel + ' input.structured-bank-data');
			var checked = !(this.model 
				&& this.model.Кредитор
				&& this.model.Кредитор.Банковские_реквизиты
				&& this.model.Кредитор.Банковские_реквизиты.Банк
				&& this.model.Кредитор.Банковские_реквизиты.Номер_счёта) ? null : 'checked';
			cbox_structured_bank_data.attr('checked', checked);
			self.SetStructuredBankData(cbox_structured_bank_data.attr('checked'));
		}

		controller.SetPersonType = function (person_type)
		{
			var sel = this.fastening.selector;
			if (person_type == 'natural')
			{
				$(sel + " .person-data.legal").hide();
				$(sel + " .person-data.natural").show();
			}
			else 
			{
				$(sel + " .person-data.natural").hide();
				$(sel + " .person-data.legal").show();
			}
		}

		controller.SetStructuredBankData = function (checked)
		{
			var sel = this.fastening.selector;
			var div = $(sel + ' div.ama-creditor');
			if ('checked' == checked)
			{
				$(sel + " .bank-data.structured").show();
				$(sel + " .bank-data.unstructured").hide();
			} else
			{
				$(sel + " .bank-data.structured").hide();
				$(sel + " .bank-data.unstructured").show();
			}
		}

		return controller;
	}
});
