﻿define([
	  'forms/base/codec/xml/codec.xml'
	, 'forms/base/log'
	, 'forms/base/codec/codec.copy'
],
function (BaseCodec, GetLogger, codec_copy)
{
	var log = GetLogger('x_registry');
	return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();

		res.schema =
		{
			tagName: 'Оперативно_рабочая_информация_о_требованиях_кредиторов'
			, fields:
			{
				'Кредиторы':
				{
					type: 'array'
					, item:
					{
						tagName: 'Кредитор'
						, fields:
						{
							'Требования':
							{
								type: 'array'
								, item:
								{
									tagName: 'Требование', fields:
									{
										'Документы': { type: 'array', item: { tagName: 'Документ' } }
										, 'Список_процентов_за_время_процедур': { type: 'array', item: { tagName: 'Проценты_за_время_процедур' } }
									}
								}
							}
						}
					}
				}
			}
		};

		var base_Decode = res.Decode;
		res.Decode= function(d)
		{
			return ('string' !== typeof d) ? d : base_Decode.call(this,d);
		}

		log.Debug('Create }');
		return res;
	}

});
