﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/section/registry/demand/main/e_dm_demand.html'
	, 'forms/ama/datamart/procedure/section/registry/demand/common/c_dm_demand_common'
	, 'forms/ama/datamart/procedure/section/registry/demand/creditor/c_creditor'
],
function (c_fastened, tpl
	, c_dm_demand_common
	, c_creditor
	)
{
	return function ()
	{
		var options = {
			field_spec:
				{
					Общее: { controller: c_dm_demand_common }
					, Кредитор: { controller: c_creditor }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.size = { width: 650, height: 500 };

		return controller;
	}
});