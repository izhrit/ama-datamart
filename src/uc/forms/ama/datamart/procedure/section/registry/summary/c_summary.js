define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/section/registry/summary/e_summary.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/procedure/section/registry/summary/a_datamart_registry_summary'
	, 'forms/ama/datamart/procedure/section/registry/base/x_registry'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, h_msgbox, a_datamart_registry_summary, x_registry, h_number_format)
{
	var ajax_transport_included = false;
	return function()
	{
		var controller = c_fastened(tpl, { foreign_data: function (n) { return h_number_format(n, 0, ',', ' '); } });

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			var empty_queue = { "Кредиторов": '', "Требований": '', "На_сумму": '' };
			this.model = {
				"1-я очередь": empty_queue
				, "2-я очередь": empty_queue
				, "3-я очередь": {
					"без залога": empty_queue
					, "с залогом": empty_queue
					, "штрафы": empty_queue
					, "проценты": empty_queue
				}
				, "за реестром": empty_queue
			};

			base_Render.call(this, sel);
			this.fastening.format_number = function (n) { return h_number_format(n, 0, ',', ' '); }

			var self = this;
			var sOnClick = function (e) { self.OnClick(e); };

			var ajaxurl = 'ama/registry/summary';
			$.ajax
			({
				url: ajaxurl
				, dataType: "json"
				, type: 'POST'
				, cache: false
				, error: function (data, textStatus)
				{
					h_msgbox.ShowAjaxError('Получение суммарных данных о реестре', ajaxurl, data, textStatus);
				}
				, success: function (data, textStatus)
				{
					$(sel).html('');
					$(sel).html(tpl({ model: data, foreign_data: self.fastening.foreign_data }));
					self.Rebind(sOnClick);
				}
			});

			this.Rebind(sOnClick);
		}

		controller.Rebind = function (sOnClick)
		{
			var self = this;
			var sel = this.fastening.selector;
			var tr = $(sel + ' table tr');
			tr.off('click', sOnClick);
			tr.click(sOnClick);
			this.SetSelection(this.selection);
		}

		controller.selection = '3-я очередь$без залога';

		controller.OnClick= function(e)
		{
			e.preventDefault();
			this.SetSelection($(e.target).parent('tr').attr('selection'));
		}

		controller.SetSelection= function(selection)
		{
			var sel = this.fastening.selector;
			$(sel + ' table tr').removeClass('selected');
			$(sel + ' table tr[selection="' + selection + '"]').addClass('selected');
			if (this.OnChangeSelection)
				this.OnChangeSelection(selection);
		}

		var base_SetFormContent = controller.SetFormContent;
		controller.SetFormContent= function(model)
		{
			if (!ajax_transport_included)
			{
				ajax_transport_included = true;
				$.ajaxTransport('+*',a_datamart_registry_summary.prepare_try_to_prepare_send_abort());
			}
			a_datamart_registry_summary.options=
				{ 
					url_prefix: 'ama/registry/summary'
					, model: model
				};
			return base_SetFormContent.call(this, model);
		}

		controller.UseCodec(x_registry());

		return controller;
	}
});
