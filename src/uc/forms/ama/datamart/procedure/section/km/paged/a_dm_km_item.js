﻿define([
	  'forms/base/codec/url/codec.url'
	, 'forms/base/codec/url/codec.url.args'
	, 'forms/ama/datamart/procedure/section/km/base/h_km'
	, 'forms/base/codec/codec.copy'
],
function (codec_url, codec_url_args, h_km, codec_copy)
{
	var url_prefix = 'ama/datamart?action=km.item.info';

	var transport = { options: { url_prefix: url_prefix } };

	transport.prepare_try_to_prepare_send_abort= function()
	{
		var self= this;
		return function (options, originalOptions, jqXHR)
		{
			if (self.options && 0 == options.url.indexOf(self.options.url_prefix))
			{
				var send_abort =
				{
					send: function (headers, completeCallback)
					{
						var decoded_url = codec_url().Decode(options.url);
						var args = codec_url_args().Decode(decoded_url);

						var pos = parseInt(args.pos);

						var km_item = null;
						h_km.ДляВсехОбъектов(self.options.model,
							function (tkm, Категория, ОбъектКМ)
							{
								if (pos == tkm)
								{
									km_item = codec_copy().Copy(ОбъектКМ);
									km_item.Группа = Категория.Группа;
									return 'другие объекты не нужны';
								}
							});
						completeCallback(200, 'success', { text: JSON.stringify(km_item) });
					}
					, abort: function ()
					{
					}
				}
				return send_abort;
			}
		}
	}

	return transport;
});