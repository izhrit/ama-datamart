define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/section/km/item/e_km_item.html'
],
function (c_fastened, tpl)
{
	return function()
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 800, height: 600 };

		return controller;
	}
});
