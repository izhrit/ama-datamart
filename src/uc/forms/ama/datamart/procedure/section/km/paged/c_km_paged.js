define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/procedure/section/km/paged/e_km_paged.html'
	, 'forms/ama/datamart/procedure/section/km/base/x_km'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/procedure/section/km/paged/a_dm_km_jqgrid'
	, 'forms/ama/datamart/procedure/section/km/paged/a_dm_km_item'
	, 'forms/ama/datamart/procedure/section/km/item/h_km_item'
],
function (c_fastened, tpl, x_km, h_msgbox, a_dm_km_jqgrid, a_dm_km_item, h_km_item)
{
	var ajax_transport_included = false;
	return function()
	{
		var colModel =
		[
			  { hidden:true, name: 'pos' }
			, { label: '№', name: 'Номер', width: 30, sortable: true, searchoptions: { sopt: ['cn'] }, align: 'center' }
			, { label: 'Группа', name: 'Группа', width: 100, sortable: true, searchoptions: { sopt: ['cn'] } }
			, { label: 'Наименование', name: 'Наименование', width: 300, sortable: true, searchoptions: { sopt: ['cn'] } }
		];

		var controller = c_fastened(tpl);

		controller.colModel = colModel;

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();

			$('#gridPager > div > table > tbody > tr > td#gridPager_right').remove();
			$('#gridPager > div > table > tbody > tr > td#gridPager_center').attr('colspan', '2');

			$("<button>", {
				text: 'Необработанное представление',
				click: function ()
				{
					$('.dm-selector button#raw-view').trigger('click');
				}
			})
			.addClass("button")
			.prependTo(
				$('#gridPager table tr td#gridPager_left').append()
			);
		}

		controller.selection = '';
		controller.base_grid_url = 'ama/km/items';
		controller.PrepareUrl= function()
		{
			return (''==this.selection)
				? (this.base_grid_url)
				: (this.base_grid_url + '/' + this.selection);
		}

		controller.RenderGrid = function ()
		{
			var self = this;
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				  datatype: "json"
				, url: self.PrepareUrl()
				, colModel: this.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: ''
				, emptyrecords: ''
				, emptyText: 'Нет элементов конкурсной массы удовлетворяющих условиям фильтрации для просмотра'
				, rownumbers: false
				, rowNum: 10
				, rowList: [10, 20, 50, 100]
				, pager: '#gridPager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, ignoreCase: true
				, onSelectRow: function () { self.OnItem(); }
				, loadError: function (xhr, status, error) { self.OnLoadError(xhr, status, error); }
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnLoadError = function (xhr, status, error)
		{
			var self = this;
			h_msgbox.ShowAjaxError("Ошибка получения конкурсной массы: " + error, self.PrepareUrl(), xhr.responseText, status);
		}

		controller.OnItem = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;
			var ajaxurl = 'ama/km/item?pos=' + rowdata.pos;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных об элементе конкурсной массы с сервера", ajaxurl);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							h_km_item.ShowItem(data);
						}
					}
				});
		}

		var base_SetFormContent = controller.SetFormContent;
		controller.SetFormContent = function (model)
		{
			a_dm_km_item.options = { url_prefix: 'ama/km/item', model: model };
			a_dm_km_jqgrid.options.url_prefix= 'ama/km/items';
			a_dm_km_jqgrid.options.model= model;
			if (!ajax_transport_included)
			{
				ajax_transport_included = true;
				$.ajaxTransport('+*', a_dm_km_item.prepare_try_to_prepare_send_abort());
				$.ajaxTransport('+*', a_dm_km_jqgrid.prepare_try_to_prepare_send_abort());
			}
			return base_SetFormContent.call(this, model);
		}

		controller.UseCodec(x_km());

		return controller;
	}
});
