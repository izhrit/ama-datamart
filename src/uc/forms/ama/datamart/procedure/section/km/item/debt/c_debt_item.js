﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/section/km/item/debt/e_debt_item.html'
	, 'forms/base/h_number_format'
],
	function (c_fastened, tpl, h_number_format) {
		return function () {
			var controller = c_fastened(tpl, { h_number_format: h_number_format });
			controller.size = { width: 800, height: 600 };
			return controller;
		}
	});
