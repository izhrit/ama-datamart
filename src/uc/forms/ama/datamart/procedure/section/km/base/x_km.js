﻿define([
	  'forms/base/codec/xml/codec.xml'
	, 'forms/base/codec/codec.copy'
],
function (BaseCodec, codec_copy)
{
	return function ()
	{
		var res = BaseCodec();

		res.schema =
			{
				tagName: 'Оперативно_рабочая_информация_о_конкурсной_массе'
				, fields:
				{
					'Конкурсная_масса':
					{
						type: 'array'
						, item:
						{
							tagName: 'Категория'
							, fields:
							{
								'Объекты':
								{
									type: 'array'
									, item:
									{
										tagName: 'Объект'
									}
								}
								,'Дебиторская_задолженность':
								{
									type: 'array'
									, item:
									{
										tagName: 'Задолженность'
									}
								}
							}
						}
					}
				}
			};

		var base_Decode = res.Decode;
		res.Decode= function(d)
		{
			return ('string' !== typeof d) ? d : base_Decode.call(this,d);
		}

		return res;
	}

});
