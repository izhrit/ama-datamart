﻿define([
	'forms/base/h_msgbox'
	, 'forms/ama/datamart/procedure/section/km/item/building/c_building_item'
	, 'forms/ama/datamart/procedure/section/km/item/car/c_car_item'
	, 'forms/ama/datamart/procedure/section/km/item/debt/c_debt_item'
	, 'forms/ama/datamart/procedure/section/km/item/money/c_money_item'
	, 'forms/ama/datamart/procedure/section/km/item/other/c_other_item'
	, 'forms/ama/datamart/procedure/section/km/item/securities/c_securities_item'

],
	function (h_msgbox, c_building_item, c_car_item, c_debt_item, c_money_item, c_other_item, c_securities_item) {
		return {
			ShowItem: function (data) {

				var c_item;
				var title;
				title = ' №' + data.Номер + ' в группе "' + data.Группа
				if (data.Подгруппа) 
					title += '/' + data.Подгруппа + '"';
				else title+= '"';
				if (data.Группа == "Здания" || data.Группа == "Сооружения") {
					c_item = c_building_item();
				}
				else if (data.Группа == "Транспортные средства") {
					c_item = c_car_item();
				}
				else if (data.Группа == "Дебиторская задолженность") {
					c_item = c_debt_item();
				}
				else if (data.Группа == "Денежные средства") {
					c_item = c_money_item();
				}
				else if (data.Группа == "Долгосрочные финансовые вложения" || data.Группа == "Краткосрочные финансовые вложения") {
					c_item = c_securities_item();
				}
				else {
					c_item = c_other_item();
				}
				c_item.SetFormContent(data);
				h_msgbox.ShowModal
					({
						title: 'Информация об объекте конкурсной массы' + title
						, controller: c_item
						, buttons: ['Закрыть']
						, id_div: "cpw-form-ama-datamart-km-item"
					});
			}
		};
	});