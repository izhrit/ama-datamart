define(function () {
	return {

		controller:
		{
			"dm_km_paged": {
				path: 'forms/ama/datamart/procedure/section/km/paged/c_km_paged'
				, title: "таблица с элементами конкурсной массы"
			}
			, "dm_km": { path: 'forms/ama/datamart/procedure/section/km/main/c_km' }
			, "dm_km_item": { path: 'forms/ama/datamart/procedure/section/km/item/c_km_item' }
		}

		, content:
		{
			"km-example1": {
				path: 'txt!forms/ama/datamart/procedure/section/km/tests/km-example1.xml'
				, title: 'пример конкурсной массы1'
			}
			, "km-example2": {
				path: 'txt!forms/ama/datamart/procedure/section/km/tests/km-example2.xml'
				, title: 'пример конкурсной массы по новой схеме'
			}
			, "km-example3": {
				path: 'txt!forms/ama/datamart/procedure/section/km/tests/km-example3.xml'
				, title: 'пример конкурсной массы по старой схеме'
			}
			, "km-new_scheme": {
				path: 'txt!forms/ama/datamart/procedure/section/km/tests/km-new_scheme.xml'
				, title: 'пример конкурсной массы по старой схеме'
			}
			, "km-old_scheme": {
				path: 'txt!forms/ama/datamart/procedure/section/km/tests/km-old_scheme.xml'
				, title: 'пример конкурсной массы по старой схеме'
			}
			, "km-house1": { path: 'txt!forms/ama/datamart/procedure/section/km/item/tests/contents/house1.xml' }
		}

	};
});