﻿define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/ama/datamart/procedure/section/km/base/h_km'
], function (ajax_jqGrid, h_km)
{
	var transport = ajax_jqGrid();
	transport.options.RowSort = function (args) {
		var sidx = args.sidx;
		var sord = args.sord;
		return function (a, b) {
			var aName = '';
			var bName = '';
			if (a[sidx])
				aName = a[sidx];
			if (b[sidx])
				bName = b[sidx];
			var res;
			if (sidx != "Номер")
			{
				res = ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
			}
			else
			{
				if (a["Группа"] != "Дебиторская задолженность" && b["Группа"] != "Дебиторская задолженность") {
					var first = parseInt(aName);
					var second = parseInt(bName);
					res = ((first < second) ? -1 : ((first > second) ? 1 : 0));
				}
				else if (a["Группа"] == "Дебиторская задолженность" && b["Группа"] == "Дебиторская задолженность") {
					var first = parseInt(aName.substr(1));
					var second = parseInt(bName.substr(1));
					res = ((first < second) ? -1 : ((first > second) ? 1 : 0));
				}
				else if (a["Группа"] == "Дебиторская задолженность") {
					res = 1;
				}
				else res = -1;
			}
			if ('desc' == sord)
				res = -res;
			return res;
		}
	}

	transport.rows_all = function ()
	{
		var rows = [];

		var km = this.options.model;
		h_km.ДляВсехОбъектов(km,
			function (kmnum, Категория, ОбъектКМ) {
				var name, number;
				if (Категория.Группа != 'Дебиторская задолженность') {
					name = ОбъектКМ.Наименование;
					number = ОбъектКМ.Номер;
				}
				else
				{
					name = 'Задолженность ' + ОбъектКМ.Сумма_задолженности + ' рублей от ' + ОбъектКМ.Дебитор.Наименование;
					number = 'Д'+ОбъектКМ.Номер
				}
				var kmitem = {
					pos: kmnum
					, Номер: number
					, Наименование: name
					, Группа: Категория.Группа
				};
				rows.push(kmitem);
			});

		return rows;
	}

	return transport;
});