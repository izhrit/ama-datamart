define([
	'forms/base/h_spec'
	, 'forms/ama/datamart/procedure/section/registry/spec_dm_registry'
	, 'forms/ama/datamart/procedure/section/report/spec_dm_report'
	, 'forms/ama/datamart/procedure/section/rtt/spec_dm_rtt'
	, 'forms/ama/datamart/procedure/section/km/spec_dm_km'
],
function (h_spec, registry_spec, report_spec, rtt_spec, km_spec)
{
	return h_spec.combine(registry_spec, report_spec, rtt_spec, km_spec);
});