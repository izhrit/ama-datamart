﻿require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl: 'js/libs/tpl',
		}
	}
}),

require
(
	[
		  'forms/ama/datamart/procedure/section/report/base/x_report_view_std'
	],
	function (x_report_view_std)
	{
		var extension =
		{
			Title: 'ПАУ'
			, key: 'ama'
			, forms: {}
			, templates: {}
		};
		extension.templates.Report_GetHeaders = function (xml_report, headers)
		{
			return x_report_view_std().Decode(xml_report, headers);
		}
		if (RegisterCpwFormsExtension)
		{
			RegisterCpwFormsExtension(extension);
		}
		return extension;
	}
);