define([
    'forms/base/fastened/c_fastened'
    , 'tpl!forms/ama/datamart/procedure/section/report/summary/e_summary.html'
],
function (c_fastened, tpl)
{
    var ajax_transport_included = false;
    return function ()
    {
    	return c_fastened(tpl);
    }
});