define([
	'forms/base/fastened/c_fastened'
	, 'forms/ama/datamart/procedure/section/report/base/x_report'
	, 'forms/base/h_number_format'
	, 'tpl!forms/ama/datamart/procedure/section/report/base/v_report_std.html'
	],
function (c_fastened, x_report, h_number_format, v_report)
{
	return function()
	{
		var controller = null;
		controller = c_fastened(function (obj)
		{
			return v_report({ report: obj.value(), h_number_format: h_number_format, headers: controller.headers });
		});
		controller.headers = [];
		controller.UseCodec(x_report());
		return controller;
	}
});