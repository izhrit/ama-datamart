﻿define([
	  'forms/ama/datamart/procedure/section/report/base/x_report'
	, 'tpl!forms/ama/datamart/procedure/section/report/base/v_report_std.html'
	, 'forms/base/h_number_format'
	, 'forms/base/log'
]
, function (x_report, v_report, h_number_format, GetLogger)
{
	var log = GetLogger('x_report_view_std');
	return function ()
	{
		var res = {};

		res.Decode= function(xml_report, headers)
		{
			log.Debug('Decode {');
			var report = x_report().Decode(xml_report);
			var report_html = v_report({ report: report, h_number_format: h_number_format, headers: headers });
			log.Debug('Decode }');
			return report_html;
		}

		return res;
	}

});
