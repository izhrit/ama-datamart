define(function ()
{
	return {

		controller:
		{
		  "dm_report_summary":   { path: 'forms/ama/datamart/procedure/section/report/summary/c_summary' }
		, "dm_report_view":      { path: 'forms/ama/datamart/procedure/section/report/view/c_view' }
		, "dm_report":           { path: 'forms/ama/datamart/procedure/section/report/main/c_report' }
		}

		, content:
		{
		  "report-example":        { path: 'txt!forms/ama/datamart/procedure/section/report/tests/report.xml' }
		, "empty-report-example":  { path: 'txt!forms/ama/datamart/procedure/section/report/tests/report_empty.xml' }
		, "report-n-example":      { path: 'txt!forms/ama/datamart/procedure/section/report/tests/report_n.xml' }
		, "report-rd-example":     { path: 'txt!forms/ama/datamart/procedure/section/report/tests/report_rd.xml' }
		, "report-ri-example":     { path: 'txt!forms/ama/datamart/procedure/section/report/tests/report_ri.xml' }

		, "report-map":            { path: 'txt!forms/ama/datamart/procedure/section/report/summary/tests/contents/map.json.txt' }
		}

	};
});