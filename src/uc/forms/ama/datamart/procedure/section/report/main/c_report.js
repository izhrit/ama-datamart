define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/section/report/main/e_report.html'
	, 'forms/ama/datamart/procedure/section/report/view/c_view'
	, 'forms/ama/datamart/procedure/section/report/summary/c_summary'
	, 'forms/ama/datamart/base/show_msg_under_construction'
],
function (c_fastened, tpl, c_view, c_summary, show_msg_under_construction)
{
	return function()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.view = c_view();
			this.summary = c_summary();

			if (!this.model)
			{
				this.view.CreateNew(sel + ' div.ama-report-main div.grid');
				this.summary.CreateNew(sel + ' div.ama-report-main div.selector');
			}
			else
			{
				this.view.SetFormContent(this.model);
				this.view.Edit(sel + ' div.ama-report-main .view');

				this.summary.SetFormContent(this.view.headers);
				this.summary.Edit(sel + ' div.ama-report-main .selector');
			}

			var self = this;
			$(sel + ' div.commands button.print').button().click(function (e) { e.preventDefault(); self.OnPrint(); });
		}

		controller.OnPrint = function ()
		{
			show_msg_under_construction();
		}

		var base_SetFormContent = controller.SetFormContent;
		controller.SetFormContent = function (model)
		{
			this.model = model;
		}

		return controller;
	}
});
