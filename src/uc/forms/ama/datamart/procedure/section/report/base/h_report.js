﻿define(function ()
{
	var helper = {};

	helper.default_Summary = {
		"Тип_процедуры": null
		, "Арбитражный_управляющий": null
		, "Должник": null
		, "Судебное_дело": null
		, "Привлеченные_специалисты": null
		, "Жалобы_На_Действия_АУ": null
		, "Меры": null
		, "Реестродержатель": null
	};

	return helper;
});