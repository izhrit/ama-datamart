﻿define([
	  'forms/base/codec/xml/codec.xml'
	, 'forms/base/log'
	, 'forms/base/codec/codec.copy'
],
function (BaseCodec, GetLogger, codec_copy)
{
	var log = GetLogger('x_report');
	return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();

		res.schema =
		{
			tagName: 'Оперативно_рабочая_информация_для_отчёта_АУ_по_процедуре'
			, fields:
			{
				КПиРИ: {
					fields: {
						Расходы: {
							type: 'array'
							, item:
								{
									tagName: 'Группа_расходов'
									, fields:
									{
										Расходы_группы: { type: 'array', item: { tagName: 'Расход' } }
									}
								}
						}
						, Дебиторская_задолженность: { type: 'array', item: { tagName: 'Задолженность' } }
						, Работники: {
							fields: {
								Работающие: { type: 'array', item: { tagName: 'Работник' } }
								,Уволенные: { type: 'array', item: { tagName: 'Работник' } }
							}
						}
						, Поступившие_денежные_средства: { type: 'array', item: { tagName: 'Поступление' } }
						, Субсидиарка: { type: 'array', item: { tagName: 'Субсидиарка' } }
					}
				}
				, Привлеченные_специалисты: { type: 'array', item: { tagName: 'Специалист' } }
				, Жалобы_На_Действия_АУ: {
					type: 'array', item: {
						tagName: 'Жалоба'
						, fields: {
							Рассмотрение: {
								fields: {
									Организация: { type: 'string' }
									, Дата: { type: 'string' }
									, Решение: { type: 'string' }
								}
							}
						}
					}
				}
				, Меры: {
					type: 'array', item: {
						tagName: 'Группа_мер'
						, fields:
							{
								Меры_в_группе: { type: 'array', item: { tagName: 'Мера' } }
							}
					}
				}
			}
		};

		var base_Decode = res.Decode;
		res.Decode= function(d)
		{
			return ('string' !== typeof d) ? d : base_Decode.call(this,d);
		}

		log.Debug('Create }');
		return res;
	}

});
