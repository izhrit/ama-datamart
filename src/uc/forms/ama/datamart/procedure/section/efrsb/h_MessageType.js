define(['forms/ama/datamart/common/news/h_MessageTypes.etalon']
,function (h_MessageTypes_etalon) {
	var helper = {};

	helper.DetermineMessageType = function (type)
	{
		return h_MessageTypes_etalon.Readable_short_about[type].toString();
	}

	helper.SelectableMessageType = function ()
	{
		var res= ':Все сообщения';
		var names= h_MessageTypes_etalon.Readable_short_about;
		for (var name in names)
		{
			res+= ';';
			res+= name + ':' + names[name];
		}
		return res;
	}
	return helper;
})