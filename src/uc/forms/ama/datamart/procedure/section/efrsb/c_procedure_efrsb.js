﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/procedure/section/efrsb/e_procedure_efrsb.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/procedure/section/efrsb/h_MessageType'
	, 'forms/ama/datamart/base/h_DecisionType'
	, 'forms/base/codec/datetime/s_codec.mysql_txt2txt_ru_legal'
],
	function (c_fastened, tpl, h_msgbox, h_MessageType, h_DecisionType, s_codec_mysql_txt2txt_ru_legal) {
		return function (options_arg) {
			var controller = c_fastened(tpl);

			controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
			controller.base_grid_url = controller.base_url + '?action=efrsb.jqgrid';

			var base_Render = controller.Render;
			controller.Render = function (sel) {
				base_Render.call(this, sel);
				this.RenderGrid();
			}

			controller.PrepareUrl = function () {
				var url = this.base_grid_url;
				if (this.model && this.model.id_Procedure)
					url += '&id_Procedure=' + this.model.id_Procedure;
				return url;
			}

			var timeFormatter = function (cellvalue, options, rowObject)
			{
				var res= rowObject.PublishDate;

				var parts= res.split('.');

				var len= parts.length;
				if (len > 1)
				{
					parts[len-1]= '<span class="extra">' + parts[len-1] + '</span>';
				}

				return parts.join('.');
			}

			var messageFormatter = function (cellvalue, options, rowObject)
			{
				var type= h_MessageType.DetermineMessageType(rowObject.MessageType);
				type= type.charAt(0).toUpperCase() + type.substring(1);
				var res= '<span class="about">' + type + '</span>';
				if (rowObject.extra)
				{
					var extra= rowObject.extra;
					res+= ' <span class="extra">(';

					if (extra.тип_акта)
						res+= ' ' + h_DecisionType.short_by_id(extra.тип_акта);

					if (extra.event)
					{
						if ('a' == rowObject.MessageType)
							res+= ', заседание на ';
						res+= s_codec_mysql_txt2txt_ru_legal.Encode(extra.event);
						if (extra.форма) 
							res+= ', форма: ' + extra.форма;
					}

					if (extra.на_сумму)
						res+= ' на ' + extra.на_сумму + ' руб.';
					if (extra.на_сумму)
						res+= ' от ' + extra.кредитор;

					res+= ')</span>';
				}
				return res;
			}

			controller.colModel =
			[
				  { name: 'MessageGUID', hidden: true }
				, { label: 'Время', name: 'PublishDate', align: 'left', width: 50, sortable: false, search: false, formatter: timeFormatter }
				, { label: 'Сообщение', name: 'MessageType', align: 'left', width: 300, sortable: false, stype: 'select'
					, formatter: messageFormatter
					, searchoptions:
					{
						sopt: ['eq']
						, value: h_MessageType.SelectableMessageType()
						, dataInit: function (el)
							{
								$("option:contains('Все сообщения')", el).attr("selected", "selected");
								$(el).trigger('change');
							}
					}
				}
				, { label: 'Номер', align: 'left' , width: 50, sortable: false, name: 'Number' }
			];

			controller.RenderGrid = function () {
				var sel = this.fastening.selector;
				var self = this;

				var url = this.PrepareUrl();
				var grid = $(sel + ' table.grid');
				grid.jqGrid
					({
						datatype: 'json'
						, url: url
						, colModel: self.colModel
						, gridview: true
						, loadtext: 'Загрузка сообщений ЕФРСБ по процедуре...'
						, recordtext: 'Сообщения {0} - {1} из {2}'
						, emptyText: 'Нет сообщений для отображения'
						, rownumbers: false
						, rowNum: 10
						, rowList: [5, 10, 15, 50, 100]
						, pager: '#cpw-ama-datamart-procedures-efrsb-pager'
						, viewrecords: true
						, autowidth: true
						, height: 'auto'
						, multiselect: false
						, multiboxonly: false
						, ignoreCase: true
						, onSelectRow: function () { self.OpenFull(); }
						, loadError: function (jqXHR, textStatus, errorThrown) {
							h_msgbox.ShowAjaxError("Загрузка сообщений", url, jqXHR.responseText, textStatus, errorThrown)
						}
					});
				grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OpenFull = function () {
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			var url = "https://old.bankrot.fedresurs.ru/MessageWindow.aspx?ID=" + rowdata.MessageGUID;
			window.open(url, "Сообщение", "toolbar=no,location=no,status=no,menubar=no,resizable=yes,directories=no,scrollbars=yes,width=1000,height=600");
		}

		return controller;
	}
});