﻿define(['forms/ama/datamart/procedure/section/base/h_sections']
, function (h_section)
{
	var codec = {};

	var splitter = '<!--/-->';

	codec.Decode= function(data)
	{
		var id_Procedure_sections = null;
		if ('string' != typeof data)
		{
			id_Procedure_sections = data;
		}
		else
		{
			var ipos = data.indexOf('|');
			id_Procedure_sections = {
				id_Procedure: data.substring(0,ipos)
				, procedure_info: data.substring(ipos+1)
			};
		}

		var sections = [];
		var xml_content = !id_Procedure_sections.procedure_info
			? '' : id_Procedure_sections.procedure_info.split(splitter);
		for (var i = 0; i < h_section.length; i++)
		{
			if (0>i || i >= xml_content.length)
			{
				sections.push({raw:'',decoded:null});
			}
			else
			{
				var decoded = null;
				try
				{
					decoded = h_section[i].codec.Decode(xml_content[i]);
				}
				catch (ex) { }
				sections.push({ raw: xml_content[i], decoded: decoded });
			}
		}
		return { id_Procedure: id_Procedure_sections.id_Procedure, section: sections };
	}

	codec.Encode= function(model)
	{
		var xml_content= [];
		for (var i = 0; i < h_section.length; i++)
			xml_content.push(i>=section.length ? '' : section[i].raw);
		return xml_content.join(splitter);
	}

	return codec;
});
