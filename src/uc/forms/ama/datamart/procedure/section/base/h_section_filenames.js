﻿define(function ()
{
	return [
		'registry.xml'
		, 'report.xml'
		, 'km.xml'
		, 'current_claims.xml'
	];
});
