﻿define([
	  'forms/ama/datamart/procedure/section/registry/main/c_registry'
	, 'forms/ama/datamart/procedure/section/report/main/c_report'
	, 'forms/ama/datamart/procedure/section/registry/base/x_registry'
	, 'forms/ama/datamart/procedure/section/report/base/x_report'
	, 'forms/ama/datamart/procedure/section/km/base/x_km'
	, 'forms/ama/datamart/procedure/section/km/main/c_km'
	, 'forms/ama/datamart/procedure/section/rtt/base/x_rtt'
	, 'forms/ama/datamart/procedure/section/rtt/main/c_rtt'
	, 'forms/ama/datamart/procedure/section/base/h_section_filenames'
],
function (c_registry, c_report, x_registry, x_report, x_km, c_km, x_rtt, c_rtt, h_section_filenames)
{
	var section_option = [
		  { title: 'Реестр', file_name: 'registry.xml', codec: x_registry(), controller: c_registry }
		, { title: 'Отчёт', file_name: 'report.xml', codec: x_report(), controller: c_report }
		, { title: 'Конкурсная масса', file_name: 'km.xml', codec: x_km(), controller: c_km }
		, { title: 'Текущие требования', file_name: 'current_claims.xml', codec: x_rtt(), controller: c_rtt }
	];
	if (section_option.length != h_section_filenames.length)
		throw 'количество описаний разделов витрины не соответствует количеству имён файлов!';
	var byFileNames = {};
	for (var i = 0; i < h_section_filenames.length; i++)
	{
		var filename = h_section_filenames[i];
		if (byFileNames[filename])
			throw 'имя файла раздела витрины ' + filename + ' указано более одного раза!';
		byFileNames[filename] = filename;
	}
	for (var i = 0; i < section_option.length; i++)
	{
		var filename = section_option[i].file_name;
		if (!byFileNames[filename])
			throw 'для раздела витрины ' + filename + ' не указано имя файла в списке имён файлов!';
	}
	return section_option;
});
