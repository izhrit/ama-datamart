﻿define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/procedure/raw/c_dm_procedure_raw'
	, 'forms/ama/datamart/procedure/section/base/h_sections'
],
function (h_msgbox, c_dm_procedure_raw, h_section)
{
	var helper= {}

	helper.OnRawView = function (base_url_dbinfo, procedure_model)
	{
		if (!procedure_model || !procedure_model.id_Procedure)
		{
			h_msgbox.ShowModal({ title: 'Необработанное представление', html: 'Процедура не задана!' });
		}
		else
		{
			var self = this;
			var ajaxurl = base_url_dbinfo + '&id_MProcedure=' + procedure_model.id_Procedure;
			var v_ajax = h_msgbox.ShowAjaxRequest("Загрузка установочных данных о процедуре с сервера", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, processData: false
				, cache: false
				, success: function (dbinfo, textStatus)
				{
					if (null != dbinfo)
					{
						self.ShowRawView(dbinfo, procedure_model.section);
					}
					else
					{
						v_ajax.ShowAjaxError(dbinfo, textStatus);
					}
				}
			});
		}
	};

	helper.ShowRawView = function (dbinfo, section)
	{
		var files = {};
		for (var i = 0; i < h_section.length; i++)
			files[h_section[i].file_name] = section[i].raw;
		var raw_model = { dbinfo: dbinfo, files: files };
		var c_raw = c_dm_procedure_raw();
		c_raw.SetFormContent(raw_model);
		h_msgbox.ShowModal
		({
			title: 'Необработанное представление данных по процедуре на витрине'
			, controller: c_raw
			, buttons: ['Закрыть']
			, id_div: "cpw-form-ama-datamart-procedure-raw-form"
		});
	};

	return helper;
});
