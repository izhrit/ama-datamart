define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/procedure/raw/e_dm_procedure_raw.html'
	, 'forms/ama/datamart/base/h_procedure_types'
	, 'forms/ama/datamart/procedure/section/base/h_sections'
],
	function (c_fastened, tpl, h_procedure_types, h_section)
{
	return function ()
	{
		var controller = c_fastened(tpl, { h_procedure_types: h_procedure_types, h_section: h_section});
		controller.size = { width: 940, height: 635 }
		return controller;
	}
});
