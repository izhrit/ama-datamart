﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/customer/procedures/e_dm_cust_procedures.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/base/h_procedure_types'
	, 'forms/ama/datamart/base/h_debtorName'
],
function (c_fastened, tpl, h_msgbox, h_procedure_types, h_debtorName)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_grid_url = controller.base_url + '?action=procedure.jqgrid';
		controller.base_crud_url = controller.base_url + '?action=proc-viewers.ru';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_Contract=' + this.model.id_Contract;
			return url;
		}

		var  procedureFormatter= function(cellvalue, options, rowObject)
		{
			return h_procedure_types.SafeShortForDBValue(rowObject.procedure_type);
		}

		var debtorNameFormatter= function(cellvalue, options, rowObject)
		{
			return h_debtorName.beautify_debtorName(rowObject.debtorName);
		}

		var dateFormat = function(cellvalue, options, rowObject)
		{
			if(rowObject.publicDate && rowObject.publicDate.length > 0)
			{
				var dateAndTimeSplit = rowObject.publicDate.split(' ');
				if (2!=dateAndTimeSplit.length)
					dateAndTimeSplit = rowObject.publicDate.split('T');
				var dateSplit = dateAndTimeSplit[0].split('-');
				var timeSplit = dateAndTimeSplit[1].split(':')

				var date = dateSplit[2] + '.' + dateSplit[1] + '.' + dateSplit[0] + ' ' 
					+ timeSplit[0] + ':' + timeSplit[1];

				return date
			}

			return '';
		}

		controller.colModel =
		[
			{ name: 'id_MProcedure', hidden: true }
			, { label: 'Арбитражный управляющий', name: 'Manager', align: 'left', width: 100 }
			, { label: 'Тип пр.', name: 'procedure_type', align: 'left', width: 30, formatter: procedureFormatter }
			, { label: 'Должник', name: 'debtorName', align: 'left', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] }, formatter: debtorNameFormatter, width: 150 }
			, { label: 'На дату', name: 'publicDate', align: 'left', width: 50, search: false, formatter: dateFormat }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = self.PrepareUrl();
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показано процедур {1} из {2}'
				, emptyText: 'Нет процедур для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, pager: '#cpw-cpw-ama-dm-customer-procedures-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true 
				, onSelectRow: function () { self.OnProcedure(); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка процедур", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnProcedure = function ()
		{
			if (!this.ShowProcedure)
			{
				alert('Просмотр информации, размещённой по процедуре не реализован');
			}
			else
			{
				var sel = this.fastening.selector;
				var grid = $(sel + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');
				var rowdata = grid.jqGrid('getRowData', selrow);
				this.ShowProcedure(h_procedure_types.PrepareSelect2(rowdata));
			}
		}

		return controller;
	}
});