define([
  "forms/ama/datamart/base/c_adapted_grid",
  "tpl!forms/ama/datamart/customer/mrequests/e_dm_cust_mrequests.html",
  "forms/base/h_msgbox",
  "forms/ama/datamart/common/mrequest/h_mrequest"
], function (c_fastened, tpl, h_msgbox, h_mrequest) {
  return function (options_arg) {
	var controller = c_fastened(tpl);

	controller.base_url =
	  options_arg && options_arg.base_url
		? options_arg.base_url
		: "ama/datamart";
	controller.base_grid_url =
	controller.base_url + "?action=mrequest.jqgrid";
	controller.base_crud_url = controller.base_url + "?action=mrequest.crud";

	var base_Render = controller.Render;
	controller.Render = function (sel) {
	  base_Render.call(this, sel);
	  var self = this;
	  this.requests_helper = h_mrequest({
		base_url: this.base_url,
		id_Contract: this.model.id_Contract,
		selector: sel,
		on_change: function () {
		  self.ReloadGrid();
		  $(sel).trigger("model_change");
		},
	  });

	  this.RenderGrid();
	  this.AddButtonToPagerLeft(
		"cpw-cpw-ama-dm-mrequests-pager",
		"Обновить",
		function () {
		  self.ReloadGrid();
		}
	  );
	};

	controller.PrepareUrl = function () {
	  var url = this.base_grid_url;
	  if (this.model)
		url +=
		  "&id_Contract=" +
		  this.model.id_Contract;
	  return url;
	};

	controller.colModel = function () {
	  return [
		{ name: "id_MRequest", hidden: true },
		{
		  label: "Должник",
		  name: "debtorName",
		  align: "left",
		  formatter: this.requests_helper.debtorNameFormatter,
		  width: 150,
		},
		{ label: "В суд", name: "Court", width: 120 },
		{ label: "Время запроса", name: "DateOfRequestAct", width: 120 },
		{ label: "id_SRO", name: "id_SRO", width: 1, hidden: true, search: false },
		{
		  label: "Согласия",
		  name: "consents",
		  formatter: this.requests_helper.consentTextFormatter,
		  width: 150,
		  search: false,
		  sortable:false
		},
	  ];
	};

	controller.RenderGrid = function () {
	  var sel = this.fastening.selector;
	  var self = this;

	  var grid = $(sel + " table.grid");
	  var url = self.PrepareUrl();
	  grid.jqGrid({
		datatype: "json",
		url: url,
		colModel: self.colModel(),
		gridview: true,
		loadtext: "Загрузка...",
		recordtext: "Показано запросов {1} из {2}",
		emptyText: "Нет запросов для отображения",
		pgtext: "Страница {0} из {1}",
		rownumbers: false,
		rowNum: 15,
		rowList: [15, 30, 60],
		pager: "#cpw-cpw-ama-dm-mrequests-pager",
		viewrecords: true,
		autowidth: true,
		height: "auto",
		multiselect: false,
		multiboxonly: false,
		ignoreCase: true,
		shrinkToFit: true,
		searchOnEnter: true,
		onSelectRow: function (id, s, e) {
		  self.onSelectRow_if_no_menu(id, s, e, function () {
			self.requests_helper.OnOpen();
		  });
		},
		loadComplete: function () {
		  self.RenderRowActions(grid);
		},
		loadError: function (jqXHR, textStatus, errorThrown) {
		  h_msgbox.ShowAjaxError(
			"Загрузка списка запросов",
			url,
			jqXHR.responseText,
			textStatus,
			errorThrown
		  );
		},
	  });
	  grid.jqGrid("filterToolbar", {
		stringResult: true,
		searchOnEnter: false,
	  });
	};

	return controller;
  };
});
