define(function ()
{

	return {

		controller: {
			"dm_cust_escort": {
				path: 'forms/ama/datamart/customer/escort/main/c_dm_cust_escort'
				, title: 'сопровождающий'
			}
			,"dm_cust_escort_account": {
				path: 'forms/ama/datamart/customer/escort/escort_account/c_dm_cust_escort_account'
				, title: 'аккаунт (сопровождающий)'
			}
			,"dm_cust_escort_faces": {
				path: 'forms/ama/datamart/customer/escort/escort_faces/c_dm_cust_escort_faces'
				, title: 'лица	 (сопровождающий)'
			}
			,"dm_cust_escort_for_contacts": {
				path: 'forms/ama/datamart/customer/escort/escort_for_contacts/c_dm_cust_escort_for_contacts'
				, title: 'для контактов (сопровождающий)'
			}
			,"dm_cust_escort_for_conract": {
				path: 'forms/ama/datamart/customer/escort/escort_for_contract/c_dm_cust_escort_for_contract'
				, title: 'для договора (сопровождающий)'
			}
			,"dm_cust_escorts": {
				path: 'forms/ama/datamart/customer/escorts/c_dm_cust_escorts'
				, title: 'список сопровождающих в кабинете абонента'
			}
			,"dm_cust_assignments": {
				path: 'forms/ama/datamart/customer/assignments/c_dm_cust_assignments'
				, title: 'назначения в кабинете абонента с заявлениями о банкротстве и запросами на кандидатуру АУ'
			}
			,"dm_customer_cabinet": {
				path: 'forms/ama/datamart/customer/main/c_customer_cabinet'
				, title: 'главная форма абонента'
			}
			, "dm_cust_procedures": {
				path: 'forms/ama/datamart/customer/procedures/c_dm_cust_procedures'
				, title: 'процедуры абонента'
			}
			, "dm_cust_managers": {
				path: 'forms/ama/datamart/customer/managers/c_dm_cust_managers'
				, title: 'АУ абонента'
			}
			, "dm_cust_mrequests": {
				path: 'forms/ama/datamart/customer/mrequests/c_dm_cust_mrequests'
				, title: 'АУ абонента'
			}
			, "dm_cust_manager": {
				path: 'forms/ama/datamart/customer/manager/c_cust_manager'
				, title: 'профиль АУ'
			}
			, "dm_cust_starts": {
				path: 'forms/ama/datamart/customer/starts/c_dm_cust_starts'
				, title: 'Таблица заявлений о банкротстве для абонента'
			}
			, "dm_cust_debtors": {
				path: 'forms/ama/datamart/customer/debtors/c_dm_cust_debtors'
				, title: 'сопровождаемые должники в кабинете абонента'
			}
		}

		, content: {
		  "dm_cust-manager-01sav":   { path: 'txt!forms/ama/datamart/customer/manager/tests/contents/01sav.json.etalon.txt' }
		, "dm_cust-manager-sample1": { path: 'txt!forms/ama/datamart/customer/manager/tests/contents/sample1.json.txt' }
		, "dm_cust-escort-01sav": { path: 'txt!forms/ama/datamart/customer/escort/main/tests/contents/01sav.json.etalon.txt' }
		, "dm_cust-escort_account-01sav": { path: 'txt!forms/ama/datamart/customer/escort/escort_account/tests/contents/01sav.json.etalon.txt' }
		, "dm_cust-escort_for_contacts-01sav": { path: 'txt!forms/ama/datamart/customer/escort/escort_for_contacts/tests/contents/01sav.json.etalon.txt' }
		, "dm_cust-escort_for_contract-01sav": { path: 'txt!forms/ama/datamart/customer/escort/escort_for_contract/tests/contents/01sav.json.etalon.txt' }
		}

	};

});