﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/customer/managers/e_dm_cust_managers.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/customer/manager/h_cust_manager'
],
function (c_fastened, tpl, h_msgbox, h_cust_manager)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_grid_url = controller.base_url + '?action=manager.jqgrid';
		controller.base_crud_url = controller.base_url + '?action=manager.ru';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_Contract=' + this.model.id_Contract;
			return url;
		}

		controller.colModel =
		[
			  { name: 'id_Manager', hidden: true }
			, { label: '№ на ЕФРСБ', name: 'efrsbNumber', width: 135 }
			, { label: 'Фамилия', name: 'lastName', width: 275 }
			, { label: 'Имя', name: 'firstName', width: 275 }
			, { label: 'Отчество', name: 'middleName', width: 275 }
			, { label: 'Прописка', name: 'registration', width: 175 }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url= self.PrepareUrl();
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показано АУ {1} из {2}'
				, emptyText: 'Нет АУ для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				// , rowList: [5, 10, 15]
				, pager: '#cpw-cpw-ama-dm-customer-managers-pager'
				, viewrecords: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, onSelectRow: function () { self.OnManager();}
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка АУ", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
			grid.jqGrid('setLabel','efrsbNumber','',{'text-align':'left'});
			grid.jqGrid('setLabel','lastName','',{'text-align':'left'});
			grid.jqGrid('setLabel','firstName','',{'text-align':'left'});
			grid.jqGrid('setLabel','middleName','',{'text-align':'left'});
		}

		controller.OnManager= function()
		{
			var self = this;
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			
			self = this;
			h_cust_manager.ModalEditManagerProfile(sel, self.model.ContractNumber, this.base_url, rowdata.id_Manager, function ()
			{
				self.ReloadGrid();
				$(sel).trigger('model_change');
			});
		}

		return controller;
	}
});