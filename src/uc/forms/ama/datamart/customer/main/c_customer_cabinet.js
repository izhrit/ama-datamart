﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/customer/main/e_customer_cabinet.html'

	, 'forms/ama/datamart/procedure/selectable/c_dm_sel_procedure'
	, 'forms/ama/datamart/customer/procedures/c_dm_cust_procedures'
	, 'forms/ama/datamart/customer/managers/c_dm_cust_managers'
	, 'forms/ama/datamart/common/news/c_news'
	, 'forms/ama/datamart/handbook/main/c_dm_handbook'
	, 'forms/ama/datamart/common/schedule/c_dm_schedule'
	, 'forms/ama/datamart/common/push/profile/h_dm_push_profile'
	, 'forms/ama/datamart/customer/assignments/c_dm_cust_assignments'
],
function (c_fastened, tpl
	, c_dm_sel_procedure
	, c_dm_cust_procedures
	, c_dm_cust_managers
	, c_news
	, c_dm_handbook
	, c_dm_viewer_schedule
	, h_dm_push_profile
	, c_dm_cust_assignments
	)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'ama/datamart' : options_arg.base_url;

		var options = {
			field_spec:
				{
					Витрина: { controller: function () { return c_dm_sel_procedure({ base_url: base_url }); }, render_on_activate:true }
					, Процедуры: { controller: function () { return c_dm_cust_procedures({ base_url: base_url }); }, render_on_activate:true }
					, АУ: { controller: function () { return c_dm_cust_managers({ base_url: base_url }); }, render_on_activate:true }
					, Новости: { controller: function () { return c_news({ base_url: base_url }); }, render_on_activate:true }
					, Справочники: { controller: function () { return c_dm_handbook({ base_url: base_url }); }, render_on_activate:true }
					, Календарь: { controller: function () { return c_dm_viewer_schedule({ base_url: base_url }); }, render_on_activate:true  }
					, Назначения: { controller: function () { return c_dm_cust_assignments({ base_url: base_url }); }, render_on_activate:true  }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' div > header button.logout').click(function () { if (self.OnLogout) self.OnLogout(); });

			var c_procedures = this.fastening.get_fc_controller('Процедуры');
			c_procedures.ShowProcedure = function (id_text) { self.ShowProcedure(id_text); }

			$(sel + " .jquery-menu").menu();

			$(sel + " .profile-dropdown").click(function (e)
			{
				e.preventDefault();
				$(this).find("ul.jquery-menu").toggle();
			})

			//close menu
			$(document).on("click", function (e)
			{
				var target = $(e.target);
				/*menu buttons*/
				if (target.hasClass("open-profile")) self.OnProfile();
				if (target.hasClass("logout")) { if (self.OnLogout) { self.OnLogout(); return false; } }
				if (target.hasClass("notifications")) self.OnNotifications();

				if (!target.hasClass("profile-dropdown") &&
					!target.parent().hasClass("profile-dropdown") &&
					!target.parent().parent().hasClass("profile-dropdown"))
				{
					$(sel + " .profile-dropdown ul.jquery-menu").hide();
				}
			});

			$(sel + ' ul.ui-tabs-nav > li > a').click(function () { $(window).resize(); });

			this.ProcessSection();
		}

		controller.ProcessSection = function ()
		{
			if (app.open_section && "false"!=app.open_section)
			{
				switch (app.open_section)
				{
					case "sl":
						this.GoToTab('cpw-ama-datamart-customer-main-hndb');
						var sel= this.fastening.selector;
						$(sel + ' .search-button-text').click();
						app.open_section = "false";
						break;
					case 'appointments': 
						this.GoToTab('cpw-ama-datamart-customer-main-assgnmnts');
						var c_assignments = this.fastening.get_fc_controller('Назначения');
						c_assignments.GoToTab('cpw-form-ama-dm-customer-assignments-cnsnts');
						app.open_section = "false";
						break;
					case 'reg-appointment': 
						this.GoToTab('cpw-ama-datamart-customer-main-assgnmnts');
						var c_assignments = this.fastening.get_fc_controller('Назначения');
						c_assignments.GoToTab('cpw-form-ama-dm-customer-assignments-cnsnts');
						break;
					default:
						app.open_section = "false";
				}
			}
		}

		controller.GoToTab = function (id_tab)
		{
			var sel= this.fastening.selector;
			$(sel + ' #cpw-ama-datamart-customer-main-tabs > ul > li > a[href="#' + id_tab + '"]').click();
		}

		controller.ShowProcedure= function(id_text)
		{
			var sel = this.fastening.selector;
			$(sel + ' #cpw-ama-datamart-customer-main-tabs > ul > li > a[href="#cpw-ama-datamart-customer-main-dmrt"]').click();
			var c_datamart = this.fastening.get_fc_controller('Витрина');
			c_datamart.SelectProcedure(id_text);
		}

		controller.OnNotifications = function ()
		{
			h_dm_push_profile.ModalEditPushProfile(base_url, this.model.id_Contract, 'c');
		}

		return controller;
	}
});