﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/customer/manager/e_cust_manager.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/manager/registration/c_dm_man_registration'
],
function (c_fastened, tpl, h_msgbox, c_dm_man_registration)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 660, height: 420 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			this.PasswordEnabled

			this.PasswordEnabled = (this.model && this.model.PasswordEnabled && true == this.model.PasswordEnabled);
			this.id_Manager = !this.model ? null : this.model.id_Manager;
			if (!this.PasswordEnabled)
				this.PasswordEnabled = false;
			var self = this;
			$(sel + ' button.password').click(function () { self.OnChangePassword(); });
			$(sel + ' button.block').click(function () { self.OnBlock(); });
			$(sel + ' a.contract-btn').click(function () { self.OnContract(); });
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_password_url = controller.base_url + '?action=manager.password';
		controller.selCustomerGrid = !options_arg ? '' : options_arg.sel;
		controller.CustomerContract = !options_arg ? '' : options_arg.CustomerContract;

		controller.OnContract = function () {
			var self = this;
			var base_url = this.base_url;
			var sel = this.fastening.selector;
			var manager = this.model;
			var c_registration = c_dm_man_registration({ base_url: base_url, sel: sel, selCustomerGrid: self.selCustomerGrid, CustomerContract: self.CustomerContract});
			c_registration.SetFormContent(manager);
			h_msgbox.ShowModal
				({
					title: 'Прописка арбитражного управляющего'
					, controller: c_registration
					, buttons: ['Закрыть']
					, id_div: "cpw-form-ama-datamart-manager-registration"
				});
		}

		controller.OnChangePassword= function()
		{
			this.model = this.GetFormContent();
			if (!this.model || !this.model.Email || null == this.model.Email || '' == this.model.Email)
			{
				h_msgbox.ShowModal({ width:400, html: 'Укажите E-mail (адрес электронной почты) АУ.' });
			}
			else
			{
				var self = this;
				var sel = this.fastening.selector;
				var btnOk= this.PasswordEnabled 
						? 'Да, хочу сменить пароль АУ'
						: 'Да, хочу предоставить пароль АУ';
				h_msgbox.ShowModal({
					width: 520
					, title: "Подтверждение операции с паролем АУ"
					, html: true == this.PasswordEnabled 
						? '<span>Вы уверены что хотите сменить пароль АУ</span><br/> и выслать его на адрес <strong>' + this.model.Email + '</strong>?'
						: '<span>Вы уверены что хотите предоставить пароль АУ</span><br/> и выслать его на адрес <strong>' + this.model.Email + '</strong>?'
					, buttons: [btnOk,'Нет, отменить']
					, id_div: "cpw-form-ama-datamart-manager-form-confirm"
					, onclose: function (btn)
					{
						if (btn == btnOk)
						{
							self.ChangePassword();
						}
					}
				});
			}
		}

		controller.ChangePassword= function()
		{
			var self = this;
			var sel = this.fastening.selector;

			var url = this.base_password_url + '&cmd=change&id_Manager=' + this.id_Manager + '&email=' + this.model.Email;
			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса на создание пароля", url);

			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok)
					{
						self.PasswordEnabled = true;
						$(sel + ' div.cpw-ama-datamart-customer-manager')
							.attr('password_enabled', self.PasswordEnabled);
						h_msgbox.ShowModal({width: 200, html: '<center>Пароль выслан</center>'});
					}
					else
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка сохранения пароля'
							, width: 450
							, html: '<span>Не удалось сменить пароль и email АУ по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>' 
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
				}
			});
		}

		controller.OnBlock = function ()
		{
			this.model = this.GetFormContent();
			if (true != this.PasswordEnabled)
			{
				h_msgbox.ShowModal( { html: 'Доступ у АУ отсутствует' });
			}
			else
			{
				var self = this;
				var btnOk = 'Да, хочу заблокировать доступ для данного АУ';
				h_msgbox.ShowModal({
					width: 520
					, title: 'Предупреждение'
					, html: 'Вы уверены что хотите заблокировать доступ для данного АУ?'
					, buttons: [btnOk, 'Нет, отменить']
					, id_div: "cpw-form-ama-datamart-manager-form-confirm"
					, onclose: function (btn)
					{
						if (btn == btnOk)
						{
							self.BlockAccess();
						}
					}
				});
			}
		}

		controller.BlockAccess = function ()
		{
			var self = this;
			var sel = this.fastening.selector;

			var url = this.base_password_url + '&cmd=block&id_Manager=' + this.id_Manager + '&email=' + this.model.Email;
			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса на блокирование доступа", url);

			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok)
					{
						self.PasswordEnabled = false;
						$(sel + ' div.cpw-ama-datamart-customer-manager')
							.attr('password_enabled', self.PasswordEnabled);
						h_msgbox.ShowModal({
							width: 200, html: '<center>Доступ заблокирован</center>'
						});
					}
					else
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка блокировки пароля'
							, width: 450
							, html: '<span>Не удалось заблокировать пароль на email АУ по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
				}
			});
		}

		return controller;
	}
});