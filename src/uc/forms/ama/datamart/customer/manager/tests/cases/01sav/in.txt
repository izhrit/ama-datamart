include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Доступ к витрине"
shot_check_png ..\..\shots\00new.png

play_stored_lines ama_datamart_manager_fields_1

shot_check_png ..\..\shots\01sav.png
wait_click_text "Атрибуты для документов"
shot_check_png ..\..\shots\01sav_attr.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01sav.json.result.txt
exit