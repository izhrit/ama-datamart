﻿define([
	  'forms/base/h_msgbox'
	, 'forms/ama/datamart/customer/manager/c_cust_manager'
],
function (h_msgbox, c_cust_manager)
{
	return {
		ModalEditManagerProfile: function (sel, ContractNumber, base_url, id_Manager, on_done)
		{
			var ajaxurl = base_url + '?action=manager.ru&cmd=get&id=' + id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос информации об АУ с сервера", ajaxurl);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.EditProfile(sel, ContractNumber, base_url, id_Manager, on_done, data);
					}
				}
			});
		}

		, OkToSave: function(manager)
		{
			if (!manager.Email || '' == manager.Email)
				return true;
			var email = manager.Email;
			var title = "Проверка перед сохранением";
			if (5>email.length)
			{
				h_msgbox.ShowModal({ title: title, width:410, html: "Длина E-mail должна быть не менее 5 символов!" });
				return false;
			}
			if (-1==email.indexOf('@'))
			{
				h_msgbox.ShowModal({ title: title, width: 410, html: "E-mail должен содержать символ '@'!" });
				return false;
			}
			return true;
		}

		, EditProfile: function (sel, ContractNumber, base_url, id_Manager, on_done, manager)
		{
			var c_manager = c_cust_manager({ sel: sel, base_url: base_url, CustomerContract: ContractNumber });
			var self = this;
			c_manager.SetFormContent(manager);
			var btnOk = 'Сохранить свойства АУ';
			h_msgbox.ShowModal
			({
				title: 'Настройка свойств АУ'
				, controller: c_manager
				, buttons: [btnOk, 'Отмена']
				, id_div: "cpw-form-ama-datamart-manager"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var manager = c_manager.GetFormContent();
						if (!self.OkToSave(manager))
							return false;
						self.UpdateProfile(base_url, id_Manager, on_done, manager);
					}
				}
			});
		}

		, UpdateProfile: function (base_url, id_Manager, on_done, manager)
		{
			var self = this;
			var ajaxurl = base_url + '?action=manager.ru&cmd=update&id=' + id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача информации об АУ на сервер", ajaxurl);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: manager
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok)
					{
						if (null != on_done)
							on_done();
					}
					else
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка сохранения свойств АУ'
							, width: 400
							, html: '<span>Не удалось сохранить свойства АУ по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
				}
			});
		}
	};
});