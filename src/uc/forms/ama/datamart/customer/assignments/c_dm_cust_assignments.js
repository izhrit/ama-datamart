define([
	'forms/base/fastened/c_fastened'
, 'tpl!forms/ama/datamart/customer/assignments/e_dm_cust_assignments.html'
, 'forms/ama/datamart/customer/starts/c_dm_cust_starts'
, 'forms/ama/datamart/customer/mrequests/c_dm_cust_mrequests'
, 'forms/ama/datamart/customer/debtors/c_dm_cust_debtors'
, 'forms/ama/datamart/customer/escorts/c_dm_cust_escorts'
],
function (c_fastened, tpl, c_dm_cust_starts, c_dm_cust_mrequests, c_dm_cust_debtors, c_dm_cust_escorts)
{
return function (options_arg)
{
	var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

	var options = {
		field_spec:
			{
				Запросы: { controller: function () { return c_dm_cust_mrequests({ base_url: base_url }); }, render_on_activate:true }
				, Согласия: { controller: function () { return c_dm_cust_starts({ base_url: base_url }); }, render_on_activate:true }
				, В_суд: { controller: function () { return c_dm_cust_debtors({ base_url: base_url }); }, render_on_activate:true }
				, Сопровождающие: { controller: function () { return c_dm_cust_escorts({ base_url: base_url }); }, render_on_activate:true }
			}
	};

	var controller = c_fastened(tpl, options);
	var base_Render = controller.Render;
	controller.Render = function (sel)
	{
		base_Render.call(this, sel);

		var self = this;
		$(sel + ' div#cpw-form-ama-dm-customer-assignments-tabs').on('tabsactivate', function (event, ui) { self.OnActivateTab(event, ui); });
	}

	controller.OnActivateTab = function (event, ui)
	{
		var tab_id= ui.newPanel.attr('id');
		switch (tab_id)
		{
		case 'cpw-form-ama-dm-customer-assignments-cnsnts':
			this.ReloadConsents();
			break;
		case 'cpw-form-ama-dm-customer-assignments-rqsts':
			this.ReloadRequests();
			break;
		case 'cpw-form-ama-dm-customer-assignments-to_court':
			this.ReloadToCourt();
			break;
		}
	}

	controller.ReloadConsents = function(){
		var fastening= this.fastening;
		var c_consents= fastening.get_fc_controller('Согласия');
		if(c_consents.fastening)
		c_consents.ReloadGrid();
	}

	controller.ReloadRequests = function(){
		var fastening= this.fastening;
		var c_requests= fastening.get_fc_controller('Запросы');
		if(c_requests.fastening)
			c_requests.ReloadGrid();
	}
	
	controller.ReloadToCourt = function() {
		var fastening= this.fastening;
		var c_to_court= fastening.get_fc_controller('В_суд');
		if(c_to_court.fastening)
			c_to_court.ReloadGrid();
	}

	controller.GoToTab = function (id_tab)
	{
		var sel= this.fastening.selector;
		$(sel + ' #cpw-form-ama-dm-customer-assignments-tabs > ul > li > a[href="#' + id_tab + '"]').click();
	}

	return controller;
}
});