define(['forms/ama/datamart/admin/root/c_datamart_admin'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'datamart_admin'
		, Title: 'Кабинет администратора витрины данных ПАУ'
	};
	return form_spec;
});
