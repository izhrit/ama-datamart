define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/admin/root/e_datamart_admin.html'
	, 'forms/ama/datamart/login/admin/c_admin_login'
	, 'forms/ama/datamart/admin/main/c_admin_cabinet'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, c_login, c_admin_cabinet, h_msgbox)
{
	return function(options_arg)
	{
		var options = {
			base_url: options_arg && options_arg.base_url ? options_arg.base_url : 'ama/datamart'
			,field_spec:
				{
					ФормаВхода: { controller: function () { return c_login(options_arg); } }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			var login_controller = this.fastening.fc_data[0].controller;
			login_controller.OnLogin = function (login_data) { self.OnLogin(login_data); };
		}

		controller.OnLogin= function(login_data)
		{
			this.OnLogged(login_data);
		}

		controller.OnLogged= function(logged_data)
		{
			var self = this;
			var sel = this.fastening.selector;
			var login_sel = sel + ' > div > div.login';
			$(login_sel).hide();
			$(login_sel + ' input').val('');

			var logged_sel = sel + ' > div > div.logged';
			$(logged_sel).show();
			if (logged_data.Администратор)
			{
				this.logged_controller = c_admin_cabinet({ base_url: this.fastening.options.base_url });
				this.logged_controller.SetFormContent(logged_data.Администратор);
			}
			this.logged_controller.OnLogout = function () { self.OnLogout(); }
			this.logged_controller.Edit(logged_sel);
		}

		controller.OnLogout= function()
		{
			var sel = this.fastening.selector;

			var logged_sel = sel + ' > div > div.logged';
			/* disable for popover */
			$(document).off('click');
			
			this.logged_controller.Destroy();
			var logged = $(logged_sel);
			logged.hide();
			logged.html('');
			this.logged_controller = null;

			$(sel + ' > div > div.login').show();

			var v_ajax = h_msgbox.ShowAjaxRequest("Завершение сессии на сервере", options.base_url + '?action=logout');
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function ()
				{
				}
			});
		}

		return controller;
	}
});
