define([
	'forms/base/h_spec'
],
function (h_spec)
{
	var dm_admin_specs = {

		controller:
		{
			"dm_admin_main":
			{ 
				path: 'forms/ama/datamart/admin/main/c_admin_cabinet' 
				,title: 'Главная форма администратора'
			}
			,"dm_admin_root":
			{ 
				path: 'forms/ama/datamart/admin/root/c_datamart_admin' 
				,title: 'Страница администратора (с авторизацией)'
			}
			,"dm_job_monitor":
			{ 
				path: 'forms/ama/datamart/admin/jmon/c_job_monitor' 
				,title: 'Монитор выполненных фоновых асинхронных работ'
			}
			,"dm_admin_emails":
			{ 
				path: 'forms/ama/datamart/admin/email/table/c_email_table' 
				,title: 'Отправленные email - мониторинг'
			}
			,"dm_admin_email":
			{ 
				path: 'forms/ama/datamart/admin/email/record/c_email_record'
				,title: 'Отправленный email - мониторинг'
			}
			,"dm_admin_out":
			{ 
				path: 'forms/ama/datamart/admin/out/c_dm_admin_out' 
				,title: 'Мониторинг исходящих'
			}
			,"dm_admin_access_log":
			{ 
				path: 'forms/ama/datamart/admin/access_log/table/c_access_log_table'
				,title: 'Журнал работы'
			}
			,"dm_admin_access_log_record":
			{ 
				path: 'forms/ama/datamart/admin/access_log/record/c_access_log_record'
				,title: 'Запись журнала работы'
			}
			,"dm_admin_pushes":
			{ 
				path: 'forms/ama/datamart/admin/push/table/c_push_table' 
				,title: 'Уведомления - мониторинг'
			}
			, "dm_test_push": {
				path: 'forms/ama/datamart/admin/push/test_push/c_test_push_main'
				, title: 'Тестовая отправка пользователю сообщения в мобильное приложение'
			}
			, "dm_all_last_result": {
				path: 'forms/ama/datamart/admin/last_result/c_all_last_result'
				, title: 'Все последние результаты'
			}
		}

		, content: {
			 "dm_job_monitor-example1":
			{
				path: 'txt!forms/ama/datamart/admin/jmon/tests/contents/example1.json.txt' 
				,title: 'Монитор 1'
			}
			,"dm_job_monitor-example2":
			{
				path: 'txt!forms/ama/datamart/admin/jmon/tests/contents/example2.json.txt' 
				,title: 'Монитор 2'
			}
			,"dm_job_monitor-example3":
			{
				path: 'txt!forms/ama/datamart/admin/jmon/tests/contents/example3-bad.json.txt' 
				,title: 'Монитор 3 (с исключением)'
			}
			,"dm_job_monitor-example4":
			{
				path: 'txt!forms/ama/datamart/admin/jmon/tests/contents/example4-bad2.json.txt' 
				,title: 'Монитор 4 (не запустилось)'
			}
			,"dm_job_monitor-example5":
			{
				path: 'txt!forms/ama/datamart/admin/jmon/tests/contents/example5.json.txt' 
				,title: 'Монитор 5 (на выравнивание)'
			}
			,"dm_job_monitor-example6":
			{
				path: 'txt!forms/ama/datamart/admin/jmon/tests/contents/example6.json.txt' 
				,title: 'Монитор 6 (на previous)'
			}
			,"dm_job_monitor-example7":
			{
				path: 'txt!forms/ama/datamart/admin/jmon/tests/contents/example7.json.txt' 
				,title: 'Монитор 7 (на synchronized)'
			}
			,"dm_job_monitor-example8":
			{
				path: 'txt!forms/ama/datamart/admin/jmon/tests/contents/example8.json.txt' 
				,title: 'Монитор 8 (на synchronized 2)'
			}
			,"dm_job_monitor-example9":
			{
				path: 'txt!forms/ama/datamart/admin/jmon/tests/contents/example9.json.txt' 
				,title: 'Монитор 9'
			}
		}
	};

	return h_spec.combine(dm_admin_specs);
});