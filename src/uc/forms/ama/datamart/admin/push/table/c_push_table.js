define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/ama/datamart/admin/push/table/e_push_table.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/datamart/admin/push/record/c_push_record'
	, 'forms/ama/datamart/base/h_message_types_short_description'
],
function (c_fastened, tpl, h_msgbox, c_push_record, h_message_types_short_description)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		controller.base_grid_url = controller.base_url + '?action=push.jqgrid&admin=true';
		controller.base_crud_url = controller.base_url + '?action=push.get';

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);
			this.RenderGrid();

			var self= this;
			this.AddButtonToPagerLeft('cpw-form-push-grid-pager', 'Обновить', function () { self.ReloadGrid(); });
		}

		var transportFormatter = function (cellvalue, options, rowObject)
		{
			switch (cellvalue)
			{
				case 'c':
					return 'email';
				case 'a':
					return 'push';
				default:
					return '?';
			}
		}

		var messageTypeFormatter = function (cellvalue, options, rowObject)
		{
			var res= h_message_types_short_description[cellvalue];
			if (!res)
				res= '?';
			return res;
		}

		controller.colModel =
		[
			  { name: 'id_Push', hidden:true }
			, { name: 'receiver', label: 'уведомляемый' }
			, { name: 'Transport', label: 'как', width:30, formatter: transportFormatter }
			, { name: 'info', label:'о чём', width: 65 }
			, { name: 'msg_Number', label:'на ефрсб №', width: 50 }
			, { name: 'msg_Type', label:'на тему', width: 50, formatter: messageTypeFormatter }
			, { name: 'Time_dispatched', label:'на отправку', width: 65 }
			, { name: 'Time_pushed', label:'отправлено', width: 65 }
			, { name: 'error', label:'ошибка', width: 35, search: false }
		];

		controller.PrepareUrl = function ()
		{
			return this.base_grid_url;
		}

		controller.RenderGrid= function()
		{
			var sel = this.fastening.selector;
			var self= this;

			var url= self.PrepareUrl();
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Записи {0} - {1} из {2}'
				, emptyText: 'Нет записей для просмотра'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				// , rowList: [5, 10, 15]
				, pager: '#cpw-form-push-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, multiSort: true
				, onSelectRow: function (id, s, e) 
				{ 
					self.onSelectRow_if_no_menu(id, s, e, function () { self.OnOpen(); });
				}
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка писем", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnOpen= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self= this;
			var id_Push= rowdata.id_Push;
			var ajaxurl_get= self.base_crud_url + '&cmd=get&id=' + id_Push + '&';
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение записи об уведомлении с сервера", ajaxurl_get);
			v_ajax.ajax
			({
				dataType: "json", type: 'GET', cache: false
				, success: function (push, textStatus)
				{
					if (null == push)
					{
						v_ajax.ShowAjaxError(push, textStatus);
					}
					else
					{
						self.DoOpen(id_Push,push);
					}
				}
			});
		}

		controller.DoOpen = function (id_Push,push)
		{
			var cc_push_record= c_push_record();
			cc_push_record.SetFormContent(push);
			h_msgbox.ShowModal
			({
				  title:'Уведомление ' + id_Push
				, controller: cc_push_record, id_div:'cpw-form-admin-push-view', buttons:['Закрыть']
			});
		}

		return controller;
	}
});