﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/admin/jmon/e_job_monitor.html'

	, 'tpl!forms/ama/datamart/admin/jmon/e_job_monitor_details.html'
	, 'tpl!forms/ama/datamart/admin/jmon/e_job_monitor_details_site.html'
	, 'tpl!forms/ama/datamart/admin/jmon/e_job_monitor_details_part.html'

	, 'forms/base/h_numbering'
	, 'forms/base/h_msgbox'

	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'txt!forms/ama/datamart/admin/jmon/s_job_monitor.css'

	, 'forms/base/h_times'

	, 'forms/ama/datamart/admin/last_result/h_all_last_result'
],
	function (c_fastened, tpl, e_job_monitor_details, e_job_monitor_details_site, e_job_monitor_details_part, h_numbering, h_msgbox, h_codec_datetime, css, h_times, h_all_last_result)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var log_date_time_format= {
			date_time_splitter: ' '
			, date: { splitter: '-', parts: ['YYYY', 'MM', 'DD'] }
			, time: { splitter: ':', parts: ['hh', 'mm', 'ss'], default_value: '00' }
		};

		var txt_date_time_format= {
			date_time_splitter: ' '
			, date: { splitter: '.', parts: ['DD', 'MM', 'YYYY' ] }
			, time: { splitter: ':', parts: ['hh', 'mm', 'ss' ], default_value: '00' }
		};

		var txt_time_span_base = function (dt1, dt2, txt_time_span_format)
		{
			var s1= dt1.getTime();
			var s2= dt2.getTime();
			var ss= Math.floor((s2-s1)/1000);
			var seconds_in_minute= 60;
			var seconds_in_hour= seconds_in_minute*60;
			var seconds_in_day= seconds_in_hour*24;

			var sd= Math.floor(ss/seconds_in_day);
			var ss= ss%seconds_in_day;

			var sh= Math.floor(ss/seconds_in_hour);
			var ss= ss%seconds_in_hour;

			var sm= Math.floor(ss/seconds_in_minute);
			var ss= ss%seconds_in_minute;

			return txt_time_span_format(sd,sh,sm,ss);
		}

		var txt_time_span_full_format= function (sd, sh, sm, ss)
		{
			var txt = '';
			if (sd > 0)
				txt += sd + ' ' + h_numbering(sd, 'день', 'дня', 'дней') + ' ';
			if (sh > 0)
				txt += sh + ' ' + h_numbering(sh, 'час', 'часа', 'часов') + ' ';
			if (sm > 0)
				txt += sm + ' ' + h_numbering(sm, 'минута', 'минуты', 'минут') + ' ';
			if (ss > 0)
				txt += ss + ' ' + h_numbering(ss, 'секунда', 'секунды', 'секунд') + ' ';
			if ('' == txt)
				txt = ' менее секунды ';
			return txt;
		}

		var txt_time_span_short_format= function (sd, sh, sm, ss)
		{
			var txt = '';
			if (sd > 0)
				txt += sd + 'д ';
			if (sh > 0)
				txt += sh + 'ч ';
			if (0 == sd) {
				if (sm > 0)
					txt += sm + 'мин ';
				if (0 == sh && ss > 0)
					txt += ss + 'сек ';
			}
			if ('' == txt)
				txt = '0сек ';
			return txt;
		};

		var txt_status = function(status)
		{
			switch (status)
			{
				case '1000': 
					return 'прервано исключительной ситуацией';
				case '0': case 0:
					return 'выполнено без критических нареканий';
				default:
					return 'выполнено с КРИТИЧЕСКИМИ ошибками (код возврата:' +  status + ')';
			}
		}

		var started_format= {
			date_time_splitter: ' '
			, date: { splitter: '.', parts: ['DD', 'MM' ] }
			, time: { splitter: ':', parts: ['hh', 'mm', 'ss' ], default_value: '00' }
		};

		var finished_format= {
			date_time_splitter: ' '
			, date: { splitter: '.', parts: [ ] }
			, time: { splitter: ':', parts: ['hh', 'mm', 'ss' ], default_value: '00' }
		};

		var prep_job_txt_time_return_mark_red= function (log_record, txt_time, started_format, finished_format, txt_time_span_format)
		{
			var mark_red= false;
			if (log_record && null!=log_record && log_record.started && null!=log_record.started && ''!=log_record.started)
			{
				var started= h_codec_datetime.Encode(log_record.started,log_date_time_format);
				txt_time.started= h_codec_datetime.Decode(started,started_format);

				txt_time.span= 'НЕ завершено!';
				if (!log_record.finished || null==log_record.finished || ''==log_record.finished)
				{
					mark_red= true;
				}
				else
				{
					var finished= h_codec_datetime.Encode(log_record.finished,log_date_time_format);
					txt_time.finished= h_codec_datetime.Decode(finished,finished_format);

					var dt_started= new Date(log_record.started.replace(/ /,'T'));
					var dt_finished= new Date(log_record.finished.replace(/ /,'T'));

					txt_time.span= txt_time_span_base(dt_started,dt_finished,txt_time_span_format);
				}
			}
			return mark_red;
		}

		var prep_job_part_view_details= function(job_part)
		{
			var view_details= job_part.view_details;
			prep_job_txt_time_return_mark_red(job_part.last, view_details.txt_time, started_format, finished_format, txt_time_span_short_format);
			if (job_part.previous)
				prep_job_txt_time_return_mark_red(job_part.previous, view_details.txt_previous_time, started_format, finished_format, txt_time_span_short_format);
			view_details.mark_red= ('0'!=job_part.last.status && (!job_part.previous || '0'!=job_part.previous.status));
			return view_details;
		}

		var prep_job_view_details= function(job)
		{
			var view_details= job.view_details;

			view_details.mark_red= prep_job_txt_time_return_mark_red(job.last,view_details.txt_time,started_format,finished_format,txt_time_span_short_format) || view_details.mark_red;
			if (job.previous)
				view_details.mark_red= prep_job_txt_time_return_mark_red(job.previous,view_details.txt_previous_time,started_format,finished_format,txt_time_span_short_format);

			for (var ipart= 0; ipart < job.parts.length; ipart++)
			{
				if (job.parts[ipart].view_details.mark_red)
				{
					view_details.mark_red= true;
					break;
				}
			}

			return view_details;
		}

		var prep_site_view_details= function(site)
		{
			if (site.synchronized)
			{
				var synchronized = h_codec_datetime.Encode(site.synchronized, log_date_time_format);
				site.view_details.txt_time = { synchronized: h_codec_datetime.Decode(synchronized, started_format) };
			}
			for (var ijob= 0; ijob < site.jobs.length; ijob++) 
			{
				if (site.jobs[ijob].view_details.mark_red)
				{
					site.view_details.mark_red= true;
					break;
				}
			}
		}

		var options= { 
			 css: css

			, prep_job_part_view_details: prep_job_part_view_details
			, prep_job_view_details: prep_job_view_details
			, prep_site_view_details: prep_site_view_details
		};

		var controller = c_fastened(tpl, options);

		var base_Render= controller.Render;
		controller.Render= function (sel)
		{
			if (null != this.model)
			{
				var cur_mysql_time= h_codec_datetime.Date2mysql(h_times.safeDateTime()).replace(/T/,' ');
				var o= this.model;
				for (var isite = 0; isite < o.length; isite++)
				{
					var site= o[isite];
					if (!site.synchronized || null==site.synchronized)
						site.synchronized= cur_mysql_time;
				}
			}
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' div.switcher').click(function (e) { self.OnOpenSwitcher(e); });
			$(sel + ' div.site > div.header div.content').click(function (e) { self.OnOpenSiteContent(e); });
			$(sel + ' div.job > div.header div.content').click(function (e) { self.OnOpenJobContent(e); });
			$(sel + ' div.part').click(function (e) { self.OnOpenPartContent(e); });
			$(sel + ' div.part').click(function (e) { self.OnOpenPartContent(e); });

			$(sel + ' button.refresh').click(function (e) { self.OnRefresh(e); });

			if (null==this.model)
				this.OnRefresh();
		}

		controller.OpenAllLastResult = function (obj)
		{
			h_all_last_result.AllLastResult(base_url, this.model, obj);
        }

		controller.OnRefresh= function()
		{
			var self= this;
			var sel= this.fastening.selector;
			var url = base_url + '?action=job-monitor';
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение статистику по выполняемым работам с сервера", url, 'cpw-ama-dm-job-monitor-load');
			v_ajax.ajax({
				dataType: "json", type: 'GET', cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else
					{
						self.model= responce;
						self.Render(sel);
					}
				}
			});
		}

		controller.OnOpenSwitcher = function (e)
		{
			var to_switch= $(e.target).parent();
			while (!to_switch.hasClass('to-switch'))
				to_switch= to_switch.parent();
			if (to_switch.hasClass('open'))
			{
				to_switch.removeClass('open');
			}
			else
			{
				to_switch.addClass('open');
			}
		}

		controller.prep_site= function(item,obj)
		{
			var site_item= item.parents('div.site');
			obj.isite= site_item.attr('data-isite');
			obj.site= this.model[obj.isite];
		}

		controller.prep_job= function(item,obj)
		{
			this.prep_site(item,obj);
			var job_item= item.parents('div.job');
			obj.ijob= job_item.attr('data-ijob');
			obj.job= obj.site.jobs[obj.ijob];
		}

		controller.prep_part= function(item,obj)
		{
			this.prep_job(item,obj);
			var job_item= item.parents('div.part');
			obj.ipart= job_item.attr('data-ipart');
			obj.part= obj.job.parts[obj.ipart];
		}

		controller.OnOpenSiteContent = function (e)
		{
			var options= { 
				prep_site_txt_time: function (site,txt_time)
				{
					if (site.synchronized)
					{
						var synchronized= h_codec_datetime.Encode(site.synchronized,log_date_time_format);
						txt_time.synchronized= h_codec_datetime.Decode(synchronized,txt_date_time_format);
					}
				}
			};
			var obj = { options: options };
			this.prep_site($(e.target),obj);
			if (obj.site && null != obj.site)
			{
				var sel = this.fastening.selector;
				var html = e_job_monitor_details_site(obj);
				$(sel + ' div.details').html(html);
			}
		}

		controller.OnOpenJobContent = function (e)
		{
			var options= { 
				  h_numbering: h_numbering
				, txt_status: txt_status
				, prep_job_txt_time: function (job,txt_time)
				{
					var log_record= job.previous ? job.previous : job.last;
					prep_job_txt_time_return_mark_red(log_record,txt_time,txt_date_time_format,txt_date_time_format,txt_time_span_full_format);
				}
			};
			var obj= { options: options };
			this.prep_job($(e.target),obj);
			if (obj.job && null != obj.job)
			{
				var sel = this.fastening.selector;
				var html = e_job_monitor_details(obj);
				$(sel + ' div.details').html(html);
			}
		}

		controller.OnOpenPartContent = function (e)
		{
			var options= { 
				  txt_status: txt_status
				, prep_job_part_txt_time: function (job_part,txt_time)
				{
					var log_record= job_part.previous ? job_part.previous : job_part.last;
					prep_job_txt_time_return_mark_red(log_record,txt_time,txt_date_time_format,txt_date_time_format,txt_time_span_full_format);
				}
			};
			var obj = { options: options };
			this.prep_part($(e.target), obj);
			if (obj.part && null != obj.part)
			{
				var sel= this.fastening.selector;
				var html= e_job_monitor_details_part(obj);
				$(sel + ' div.details').html(html);
				
				var self = this;
				$(sel + ' div.cpw-job-monitor-job-part-details-form > div.button_open button#open_all_last_result').click(function () { self.OpenAllLastResult(obj); });
			}
		}

		return controller;
	}
});