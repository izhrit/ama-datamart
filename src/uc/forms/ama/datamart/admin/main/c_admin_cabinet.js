﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/datamart/admin/main/e_admin_cabinet.html'
	, 'forms/ama/datamart/handbook/main/c_dm_handbook'
	, 'forms/ama/datamart/common/cust_query/main/c_cust_query_main'
	, 'forms/ama/datamart/admin/jmon/c_job_monitor'
	, 'forms/ama/datamart/admin/out/c_dm_admin_out'
	, 'forms/ama/datamart/admin/access_log/table/c_access_log_table'
],
function (c_fastened, tpl, c_dm_handbook, c_cust_query_main, c_job_monitor, c_dm_admin_out, c_access_log_table)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var options = {
			field_spec:
				{
					Справочники: { controller: function () { return c_dm_handbook({ base_url: base_url }); }, render_on_activate:true }
					, Запросы: { controller: function () { return c_cust_query_main({ base_url: base_url }); }, render_on_activate: true }
					, Исходящие: { controller: function () { return c_dm_admin_out({ base_url: base_url }); }, render_on_activate: true }
					, Фоновые_процессы: { controller: function () { return c_job_monitor({ base_url: base_url }); }, render_on_activate: true }
					, Журнал: { controller: function () { return c_access_log_table({ base_url: base_url }); }, render_on_activate: true }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;

			$(sel + " .jquery-menu").menu();

			$(sel + " .profile-dropdown").click(function(){
				$(this).find("ul.jquery-menu").toggle();
			})
			
			//close menu
			$(document).on("click", function (e) {
				/*menu buttons*/
				if($(e.target).hasClass("logout")) { if (self.OnLogout) { self.OnLogout(); return false; } }

				if(!$(e.target).hasClass("profile-dropdown") && !$(e.target).parent().hasClass("profile-dropdown")) {
					$(sel + " .profile-dropdown ul.jquery-menu").hide();
				}
			});

			$(sel + ' ul.ui-tabs-nav > li > a').click(function(){ $(window).resize(); });
		}

		return controller;
	}
});