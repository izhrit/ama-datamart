﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/award/signed/e_award_signed.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);
		return controller;
	}
});