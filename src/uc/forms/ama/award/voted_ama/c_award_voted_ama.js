﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/award/voted_ama/e_award_voted_ama.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.back').click(function () { self.OnBack(); });
			$(sel + ' button.to-voting').click(function () { self.OnToVoting(); });
		}

		controller.OnBack= function()
		{
			if (this.OnBackToLogin)
				this.OnBackToLogin();
		}

		controller.OnToVoting = function ()
		{
			if (this.OnGotoVoting)
				this.OnGotoVoting();
		}

		return controller;
	}
});