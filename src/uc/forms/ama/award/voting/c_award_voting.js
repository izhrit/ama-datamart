﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/award/voting/e_award_voting.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.size = {width:800,height:400};

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' i.info').click(function (e) { e.preventDefault(); self.OnNominee(e); });

			$(sel + ' button.back').click(function () { self.OnBack(); });
			$(sel + ' button.to-signing').click(function () { self.OnSigning(); });
			$(sel + ' li').click(function(e) {
				var targetNode = e.target.nodeName;
				if(targetNode == "I") return false;
				
				var radioButton = $(this).find("input");

				$(sel + ' li').removeClass("selected-candidate");

				if(radioButton.attr('checked')) {
					if(targetNode != "INPUT") $(radioButton).attr('checked', false)
				} else {
					$(this).addClass("selected-candidate");
					if(targetNode != "INPUT") $(radioButton).attr('checked', 'checked');
				}

				if(targetNode == "INPUT" && radioButton.attr('checked')) $(this).addClass("selected-candidate");
			})

			if (this.model && this.model.Голосующий && this.model.Голосующий.ПроголосовалЗа)
			{
				for (var i= 0; i<this.model.Кандидаты.length; i++)
				{
					var кандидат = this.model.Кандидаты[i];
					if (кандидат.ИНН == this.model.Голосующий.ПроголосовалЗа.ИНН)
					{
						var iradio= $(sel + ' [data-array-index="' + i + '"] input[type="radio"]');
						iradio.attr('checked','checked');
						$(sel + ' [data-array-index="' + i + '"]').addClass("selected-candidate");
					}
				}
			}
		}

		controller.OnBack = function ()
		{
			if (this.OnBackToLogin)
				this.OnBackToLogin();
		}

		controller.OnNominee = function (e)
		{
			var ia = $(e.target);
			var iarrayitem = ia.parents('[data-fc-type="array-item"]');
			var index = iarrayitem.attr('data-array-index');
			this.ShowNomineeByIndex(index);
		}

		controller.ShowNomineeByIndex = function (index)
		{
			var Кандидат = this.model.Кандидаты[index];
			if (Кандидат.URL && null != Кандидат.URL)
			{
				window.open(Кандидат.URL, 'ama-award-nominee',
					'width=1000,height=700,resizable=yes,scrollbars=yes,menubar=yes,toolbar=yes');
			}
		}

		controller.ПроголосовалЗа= function()
		{
			var sel = this.fastening.selector;
			var ichecked_option = $(sel + ' input[type="radio"]::checked');
			if (0==ichecked_option.length)
			{
				return null;
			}
			else
			{
				var iarrayitem = ichecked_option.parents('[data-fc-type="array-item"]');
				var index = iarrayitem.attr('data-array-index');
				var Кандидат = this.model.Кандидаты[index];
				return { ИНН: Кандидат.ИНН };
			}
		}

		controller.OnSigning= function()
		{
			var title = "Укажите данные для голосования";
			var ПроголосовалЗа = this.ПроголосовалЗа();
			if (null == ПроголосовалЗа)
			{
				h_msgbox.ShowModal({ title: title, width: 410, html: "Выберите кандидата, за которого вы хотите отдать свой голос" });
			}
			else
			{
				var sel = this.fastening.selector;
				var email = $(sel + ' input[type="text"].email').val();
				if (5 > email.length)
				{
					h_msgbox.ShowModal({ title: title, width: 410, html: "Длина E-mail должна быть не менее 5 символов!" });
				}
				else if (-1 == email.indexOf('@'))
				{
					h_msgbox.ShowModal({ title: title, width: 410, html: "E-mail должен содержать символ '@'!" });
				}
				else
				{
					if (this.OnGotoSigning)
						this.OnGotoSigning(email, ПроголосовалЗа);
				}
			}
		}

		return controller;
	}
});