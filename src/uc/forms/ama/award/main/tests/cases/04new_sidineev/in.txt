﻿include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "Здравствуйте"

shot_check_png ..\..\shots\02new.png

js wbt.SetModelFieldValue("manager", "Сидинеев");

shot_check_png ..\..\shots\04new_selected_sideneev.png

je $('.login').click();

shot_check_png ..\..\shots\04new_sideneev.png

je $('.back').click();
je wbt_FocusSelect2('div.select-manager');
shot_check_png ..\..\shots\04new_selected_sideneev.png

je $('.login').click();

shot_check_png ..\..\shots\04new_sideneev.png

je $('.to-signing').click();
wait_text "Укажите данные для голосования"
shot_check_png ..\..\shots\04new_sideneev_no_vote.png
wait_click_text "OK"
wait_text_disappeared "Укажите данные для голосования"

je $($('div[data-fc-selector="Кандидаты"] input')[1]).attr('checked','checked');
je $('.to-signing').click();
wait_text "Укажите данные для голосования"
shot_check_png ..\..\shots\04new_sideneev_empty_email.png
wait_click_text "OK"
wait_text_disappeared "Укажите данные для голосования"

je $('input.email').val('1234');
je $('.to-signing').click();
wait_text "Укажите данные для голосования"
shot_check_png ..\..\shots\04new_sideneev_empty_email2.png
wait_click_text "OK"
wait_text_disappeared "Укажите данные для голосования"

je $('input.email').val('12345');
je $('.to-signing').click();
wait_text "Укажите данные для голосования"
shot_check_png ..\..\shots\04new_sideneev_bad_email.png
wait_click_text "OK"
wait_text_disappeared "Укажите данные для голосования"

je $('input.email').val('1@234');
shot_check_png ..\..\shots\04new_nominee0.png
je $('.to-signing').click();
shot_check_png ..\..\shots\04new_nominee0_signing.png

je $('.back').click();
shot_check_png ..\..\shots\04new_nominee0.png

je $('.to-signing').click();
shot_check_png ..\..\shots\04new_nominee0_signing.png

je $('.sign').click();
shot_check_png ..\..\shots\04new_nominee0_signed.png

exit