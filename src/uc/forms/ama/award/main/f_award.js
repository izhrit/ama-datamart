define(['forms/ama/award/main/c_award'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'award'
		, Title: 'Голосование арбитражный управляющий года'
	};
	return form_spec;
});
