﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/award/signing/e_award_signing.html'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/ama/award/signing/v_wrong_components.html'
	, 'tpl!forms/ama/award/signing/v_wrong_certificate.html'
],
function (c_fastened, tpl, h_msgbox, v_wrong_components, v_wrong_certificate)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.back').click(function () { self.OnBack(); });
			$(sel + ' button.sign').click(function () { self.OnSign(); });
			self.RememberCurrentPage();
		}

		controller.OnBack = function ()
		{
			if (this.OnBackToVoting)
			{
				this.ForgetCurrentPage();
				this.OnBackToVoting();
			}
		}

		var getNameFromCert= function (subjectNameFromCert)
		{
			var sn = subjectNameFromCert;
			var sn_split = sn.split(',');

			for (var i = 0; i < sn_split.length; i++)
			{
				var item= sn_split[i];
				var item_split = item.split('=');
				if ("CN"==item_split[0].trim())
					return item_split[1];
			}
		}

		window.cpw_ama_datamart_award_signer = {
			use_activex: window.use_activex
			, check_cert_name: true
			, CheckOkComponents: function () { return ('ActiveXObject' in window); }
			, getDialogBoxCertificates: function() { return $.capicom.getDialogBoxCertificates(); }
			, signBase64: function (a1, a2, a3) { return $.capicom.signBase64(a1, a2, a3); }
		};

		controller.Sign= function(На_подписание)
		{
			var use_activex = window.cpw_ama_datamart_award_signer.use_activex;
			var check_cert_by_name = window.cpw_ama_datamart_award_signer.check_cert_name;
			if (!use_activex)
			{
				return { cms: "test-cms" };
			}
			else if (!window.cpw_ama_datamart_award_signer.CheckOkComponents())
			{
				h_msgbox.ShowModal({title: "Недоступны инструменты электронной подписи", width: 640, html: v_wrong_components() });
				return null;
			}
			else
			{
				var cert = window.cpw_ama_datamart_award_signer.getDialogBoxCertificates();
				if (null==cert)
					return null;
				if (check_cert_by_name)
				{
					var cert_name = getNameFromCert(cert.SubjectName);
					var au_name = this.model.АУ.Фамилия + " " + this.model.АУ.Имя + " " + this.model.АУ.Отчество;
					if (cert_name != au_name)
					{
						h_msgbox.ShowModal({ title: "Выбран неправильный сертификат электронной подписи", width: 640, 
							html: v_wrong_certificate({cert_name:cert_name,au_name:au_name}) });
						return null;
					}
				}
				var cms = window.cpw_ama_datamart_award_signer.signBase64(На_подписание, false, cert.thumbprint);
				if (cms===false)
					return null;
				return { cms: cms };
			}
		}

		controller.OnSign= function()
		{
			var signed = this.Sign(this.model.На_подписание);
			if (null != signed)
				this.OnSigned(signed);
		}

		controller.OnSigned = function (signed)
		{
			var self = this;
			var url = this.base_url + '?action=award.vote';
			var v_ajax = h_msgbox.ShowAjaxRequest("Сохранение информации об отданном голосе", url);
			var data = {
				Голосующий: this.model.АУ
				, ПроголосовалЗа: this.model.ПроголосовалЗа
				, cms: signed.cms
				, Бюллетень: this.model.На_подписание
			};
			data.Голосующий.ИНН = this.model.АУ.inn;
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: data
				, cache: false
				, success: function (data, textStatus)
				{
					if (self.GotoSigned)
					{
						self.ForgetCurrentPage();
						self.GotoSigned();
					}
				}
			});
		}

		controller.ForgetCurrentPage = function ()
		{
			$.cookie("current_page", null);
		}

		controller.RememberCurrentPage = function ()
		{
			$.cookie("current_page", "award_singning");
		}

		return controller;
	}
});