﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/award/admin/login/e_award_admin_login.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/award/admin/main/c_award_admin'
],
function (c_fastened, tpl, h_msgbox, c_award_admin)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.login').click(function () { self.OnLogin(); });
			$(sel + ' button.logout').click(function () { self.OnLogout(); });
		}

		controller.OnLogin= function()
		{
			var self = this;
			var sel= this.fastening.selector;
			var login = $(sel + ' select').val();
			var password = $(sel + ' input[type="password"]').val();
			var url = this.base_url + '?action=award.login';
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос авторизации", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: {login:login,password:password}
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						if (true == data.ok)
						{
							self.OnLogged(login);
						}
						else
						{
							h_msgbox.ShowModal({
								title:"Неудачная авторизация", width: 400
								, html:"Вы ввели неправильные данные для входа"
							});
						}
					}
				}
			});
		}

		controller.OnLogged = function (login)
		{
			var sel = this.fastening.selector;
			$(sel + ' div.auth').show();
			$(sel + ' div.login').hide();
			$(sel + ' div.auth span.name').text(login);

			this.main = c_award_admin({ base_url: this.base_url });
			this.main.CreateNew(sel + ' div.main');
		}

		controller.OnLogout= function ()
		{
			var sel = this.fastening.selector;
			$(sel + ' div.auth').hide();
			$(sel + ' div.login').show();

			this.main.Destroy();
			delete this.main;
			this.main = null;
			$(sel + ' div.main').html('');

			$(sel + ' select').val('');
			$(sel + ' input[type="password"]').val('');
		}

		return controller;
	}
});