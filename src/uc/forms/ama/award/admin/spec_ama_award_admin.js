define(function ()
{

	return {

		controller: {
		  "award_results":     { path: 'forms/ama/award/admin/results/c_award_results' }
		, "award_admin":
		{ 
			path: 'forms/ama/award/admin/main/c_award_admin'
			,title: 'результаты голосования на премию'
		}
		, "award_votes":       { path: 'forms/ama/award/admin/votes/c_award_votes' }
		, "award_admin_login":
		{
			path: 'forms/ama/award/admin/login/c_award_admin_login'
			,title:'вход в результаты голосования на премию'
		}
		}

		, content: {
			"award_results_example1": { path: 'txt!forms/ama/award/admin/results/tests/contents/example1.json.txt' }
		}

	};

});