﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/award/admin/votes/e_award_votes.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		controller.base_grid_url = controller.base_url + '?action=award.jqgrid';

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.colModel =
		[
			  { name: 'id_AwardVote', hidden: true }
			, { label: 'АУ', name: 'АУ', width: 200, sortable: false }
			, { label: 'СРО', name: 'СРО', width: 200, sortable: false }
			, { label: 'Время', name: 'VoteTime', width: 100, search: false, sortable: false }
			, { label: 'За', name: 'За', width: 200, sortable: false }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = this.base_grid_url;
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка зарегистрированных голосов..'
				, recordtext: 'Голоса {0} - {1} из {2}'
				, emptyText: 'Нет зарегистрированных голосов для отображения'
				, rownumbers: false
				, rowNum: 10
				, rowList: [5, 10, 15]
				, pager: '#cpw-cpw-ama-dm-award-votes-pager'
				, viewrecords: true
				, autowidth: true
				, multiselect: false
				, multiboxonly: false
				, height: '270'
				, ignoreCase: true
				, shrinkToFit: true
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка зарегистрированных голосов", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
			grid.jqGrid("navGrid", "#cpw-cpw-ama-dm-award-votes-pager",
				{ edit: false, add: false, del: false, search: false, refresh: true, refreshtext: 'обновить', refreshstate: 'current' }
			);
		}

		return controller;
	}
});