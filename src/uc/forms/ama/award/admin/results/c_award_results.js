﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/award/admin/results/e_award_results.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			if (this.model)
			{
				base_Render.call(this,sel);
			}
			else
			{
				var self = this;
				this.RequestResults(function (results)
				{
					self.model = results;
					base_Render.call(self, sel);
					self.bind_refresh();
				});
			}
		}

		controller.RequestResults= function(on_result)
		{
			var self = this;
			var url = this.base_url + '?action=award.results';
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных о результатах голосования", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						on_result(data);
					}
				}
			});
		}

		controller.bind_refresh = function ()
		{
			var self = this;
			var sel = this.fastening.selector;
			$(sel + ' button.refresh').click(function () { self.OnRefresh(); });
		}


		controller.OnRefresh= function()
		{
			var self = this;
			var sel = this.fastening.selector;
			this.RequestResults(function (results)
			{
				self.model = results;
				$(sel).html('');
				$(sel).html(tpl({ value: function () { return results; } }));
				self.bind_refresh();
			});
		}

		return controller;
	}
});