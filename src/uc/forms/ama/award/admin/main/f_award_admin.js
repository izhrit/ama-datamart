define(['forms/ama/award/admin/login/c_award_admin_login'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'award_results'
		, Title: 'Результаты голосования'
	};
	return form_spec;
});
