﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/award/contract/e_award_contract.html'
	, 'forms/ama/award/base/h_award'
	, 'forms/base/h_msgbox'
	, 'forms/ama/award/voting/c_award_voting'
	, 'forms/base/ql'
	, 'forms/base/codec/url/codec.url.args'
	, 'forms/base/codec/url/codec.url'
	, 'tpl!forms/ama/award/base/p_bulletin.txt'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/h_times'
],
function (c_fastened, tpl, h_award, h_msgbox, c_award_voting, ql, codec_url_args, codec_url, p_bulletin, h_codec_datetime, h_times)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl, { h_award: h_award });

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' div.am-card').click(function (e) { e.preventDefault(); self.OnVoting(e); });
			$(sel + ' button.vote').click(function (e) { e.preventDefault(); self.OnVote(); });
			$(sel + ' button.back').click(function (e) { e.preventDefault(); self.OnBack(); });
		}

		controller.OnVoting = function (e)
		{
			var ia = $(e.target);
			var iarrayitem = ia.parents('[data-fc-type="array-item"]');
			var index = iarrayitem.attr('data-array-index');
			this.ShowVotingByIndex(index);
		}

		controller.ShowVotingByIndex = function (index)
		{
			var Голосующий = this.model.Голосующие[index];
			var self = this;

			try {
				if (Голосующий.ПроголосовалЗа) {
					var url = self.base_url + '?action=award.votes&operations=IsSignedVoice&inn=' + Голосующий.ИНН;
					var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных об отданном голосе", url);
					v_ajax.ajax({
						dataType: "json"
						, type: 'GET'
						, cache: false
						, success: function (data, textStatus) {
							debugger;
							if (data == '1') {
								self.WarningMsg();
							} else {
								h_award.RequestNominees(self.base_url, function (Кандидаты) {
									self.ShowVoting(Кандидаты, Голосующий, index);
								});
							}
						}
					});
				} else {
					h_award.RequestNominees(self.base_url, function (Кандидаты) {
						self.ShowVoting(Кандидаты, Голосующий, index);
					});
				}
			} catch (ex) {
				h_award.RequestNominees(self.base_url, function (Кандидаты) {
					self.ShowVoting(Кандидаты, Голосующий, index);
				});
			}

			// h_award.RequestNominees(this.base_url, function (Кандидаты)
			// {
			// 	self.ShowVoting(Кандидаты, Голосующий, index);
			// });
		}

        controller.WarningMsg = function()
        {
            alert("Ваш голос уже был учтён голосованием при помощи электронной подписи");
        }

		controller.ShowVoting = function (Кандидаты, Голосующий, index)
		{
			var sel = this.fastening.selector;
			var self = this;
			var cvoting = c_award_voting({ base_url: this.base_url });
			this.Кандидаты = Кандидаты;
			this.index = index;
			this.Голосующий = Голосующий;
			cvoting.SetFormContent({ Кандидаты: Кандидаты, Голосующий: Голосующий });
			cvoting.Edit(sel + ' div.voting div.placer');
			this.cvoting = cvoting;

			$(sel + ' div.main').hide();
			$(sel + ' div.voting').show();
		}

		controller.SafeDestroyPlacedController = function ()
		{
			if (null != this.cvoting)
			{
				var sel = this.fastening.selector;
				this.cvoting.Destroy();
				$(sel + ' div.voting div.placer').html('');
				this.cvoting = null;
				$(sel + ' div.main').show();
				$(sel + ' div.voting').hide();
			}
		}

		controller.OnBack = function ()
		{
			if (null != this.cvoting)
				this.SafeDestroyPlacedController();
		}

		controller.OnVote = function ()
		{
			if (null != this.cvoting)
			{
				var ПроголосовалЗа = this.cvoting.ПроголосовалЗа();
				if (null != ПроголосовалЗа)
				{
					this.СохранитьРезультат(this.Голосующий, ПроголосовалЗа, this.index, this.Кандидаты);
					this.SafeDestroyPlacedController();
				}
				else
				{
					var title = "Проверка перед сохранением";
					h_msgbox.ShowModal({ title: title, width: 410, html: "Выберите кандидата, за которого вы хотите отдать свой голос" });
				}
			}
		}

		controller.СохранитьРезультат = function (Голосующий, ПроголосовалЗа, index, Кандидаты)
		{
			var decoded_url = codec_url().Decode(window.location);
			var args = codec_url_args().Decode(decoded_url);
			var license_id = args.license_id;

			var dt_codec = h_codec_datetime.ru_legal_txt2dt();
			var barg = {
				Голосующий: Голосующий
				, ПроголосовалЗа: ПроголосовалЗа
				, Кандидаты: Кандидаты
				, Время: dt_codec.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()))
			};
			var bulletin = p_bulletin(barg);
			bulletin = bulletin.replace(/<br\/>/gi, "\r\n");

			var sel = this.fastening.selector;
			var url = this.base_url + '?action=award.vote';
			var v_ajax = h_msgbox.ShowAjaxRequest("Сохранение информации об отданном голосе", url);
			var data = { Голосующий: Голосующий, ПроголосовалЗа: ПроголосовалЗа, license_id: license_id, Бюллетень: bulletin };
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: data
				, cache: false
				, success: function (data, textStatus)
				{
					Голосующий.ПроголосовалЗа = ql.find_first_or_null(Кандидаты, function (k)
					{
						return k.ИНН == ПроголосовалЗа.ИНН;
					});
					var html = h_award.ЗаКогоПроголосовал(Голосующий);

					$(sel + ' [data-array-index="' + index + '"] div.am-card').html(html);
				}
			});
		}

		return controller;
	}
});