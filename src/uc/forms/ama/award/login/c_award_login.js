﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/award/login/e_award_login.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var options = {
			field_spec: {
				manager: {
					ajax: {
						dataType: 'json'
						, url: base_url + '?action=award.managers'
					}
				}
			}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			
			var self = this;
			$(sel + ' div.select-manager').on('change', function () { self.OnSelect(); });
			$(sel + ' button.login').click(function () { self.OnLogin(); });
		}

		controller.OnSelect = function ()
		{
			var sel = this.fastening.selector;
			var iselect = $(sel + ' div.select-manager');
			var selected = iselect.select2('data');
			this.SelectManager((!selected || null == selected) ? null : selected.data);
		}

		controller.SelectManager= function(m)
		{
			this.selected_manager = m;
			if (null == m)
				m = {};
			var sel = this.fastening.selector;
			$(sel + ' input.firstname').val(m.Имя);
			$(sel + ' input.surname').val(m.Фамилия);
			$(sel + ' input.patronymicname').val(m.Отчество);
			$(sel + ' input.inn').val(m.inn);
			$(sel + ' input.sro').val(m.СРО);
			if (m.manager)
				$(sel + ' div.select-manager').select2('data', m.manager);
		}

		controller.OnLogin = function ()
		{
			if (!this.selected_manager || null==this.selected_manager)
			{
				h_msgbox.ShowModal({
					title: 'Нельзя проголосовать анонимно'
					, width: 410
					, html: 'Выберите пожалуйста Арбитражного управляющего из списка, от лица которого вы хотите проголосовать, прежде чем перейти к голосованию!'
				});
			}
			else
			{
				try {
					var self = this;
					var sel = this.fastening.selector;
					var m = this.selected_manager;
					var url = base_url + '?action=award.managers_server&operation=checkmanagerinserver&efrsbNumber=' 
						+ m.efrsbNumber + '&inn=' + this.selected_manager.inn + '&managerName=' + m.Фамилия + ' ' + m.Имя + ' ' + m.Отчество;
					var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных о голосовании АУ", url);
					v_ajax.ajax({
						dataType: "json"
						, type: 'GET'
						, cache: false
						, success: function (result, textStatus) {
							if (result == 1) {
								if (self.OnOkLogin && null != self.OnOkLogin)
									self.OnOkLogin(self.GetFormContent(), true);
							} else {
								if (self.OnOkLogin && null != self.OnOkLogin)
									self.OnOkLogin(self.GetFormContent(), false);
							}
						}
					});
				} catch (e) {
					var sel = this.fastening.selector;
					if (self.OnOkLogin && null != self.OnOkLogin)
						self.OnOkLogin(self.GetFormContent());
				}
				// var sel = this.fastening.selector;
				// if (this.OnOkLogin && null != this.OnOkLogin)
				// 	this.OnOkLogin(this.GetFormContent());
			}
		}

		return controller;
	}
});