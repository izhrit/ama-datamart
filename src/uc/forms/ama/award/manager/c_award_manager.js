﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/award/manager/e_award_manager.html'
	, 'forms/ama/award/base/h_award'
	, 'forms/base/h_msgbox'
	, 'forms/ama/award/voting/c_award_voting'
	, 'forms/base/ql'
	, 'forms/base/codec/url/codec.url.args'
	, 'forms/base/codec/url/codec.url'
],
function (c_fastened, tpl, h_award, h_msgbox, c_award_voting, ql, codec_url_args, codec_url)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl, { h_award: h_award });

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;

			if (typeof self.model.ПроголосовалЗа == "undefined")
			if (!self.model.ПроголосовалЗа)
				self.OnVoting()

			try {
				if (self.model.ПроголосовалЗа) {
					var url = self.base_url + '?action=award.votes&operations=IsSignedVoice&inn=' + self.model.ИНН;
					var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных об отданном голосе", url);
					v_ajax.ajax({
						dataType: "json"
						, type: 'GET'
						, cache: false
						, success: function (data, textStatus) {
							if (data == '1') {
								$(sel + ' div.am-card').click(function (e) { e.preventDefault(); self.WarningMsg(); });
							} else {
								$(sel + ' div.am-card').click(function (e) { e.preventDefault(); self.OnVoting(); });
							}
						}
					});
				} else {
					$(sel + ' div.am-card').click(function (e) { e.preventDefault(); self.OnVoting();});
				}
			} catch (ex) { $(sel + ' div.am-card').click(function (e) { e.preventDefault(); self.OnVoting(); }); }

			// $(sel + ' div.am-card').click(function (e) { e.preventDefault(); self.OnVoting(); });
			$(sel + ' button.vote').click(function (e) { e.preventDefault(); self.OnVote(); });
			$(sel + ' button.back').click(function (e) { e.preventDefault(); self.OnBack(); });
		}

		controller.WarningMsg = function()
		{
			alert("Ваш голос уже был учтён голосованием при помощи электронной подписи");
		}

		controller.OnVoting = function ()
		{
			var self = this;
			h_award.RequestNominees(this.base_url, function (Кандидаты)
			{
				self.ShowVoting(Кандидаты, self.model);
			});
		}

		controller.ShowVoting = function (Кандидаты, Голосующий, index)
		{
			var sel = this.fastening.selector;
			var self = this;
			var cvoting = c_award_voting({ base_url: this.base_url });
			this.Кандидаты = Кандидаты;
			this.Голосующий = Голосующий;
			cvoting.SetFormContent({ Кандидаты: Кандидаты, Голосующий: Голосующий });
			cvoting.Edit(sel + ' div.voting div.placer');
			this.cvoting = cvoting;

			$(sel + ' div.main').hide();
			$(sel + ' div.voting').show();
		}

		controller.SafeDestroyPlacedController = function ()
		{
			if (null != this.cvoting)
			{
				var sel = this.fastening.selector;
				this.cvoting.Destroy();
				$(sel + ' div.voting div.placer').html('');
				this.cvoting = null;
				$(sel + ' div.main').show();
				$(sel + ' div.voting').hide();
			}
		}

		controller.OnVote= function()
		{
			if (null != this.cvoting)
			{
				var ПроголосовалЗа = this.cvoting.ПроголосовалЗа();
				if (null != ПроголосовалЗа)
				{
					this.СохранитьРезультат(this.Голосующий, ПроголосовалЗа, this.Кандидаты);
					this.SafeDestroyPlacedController();
				}
				else
				{
					var title = "Проверка перед сохранением";
					h_msgbox.ShowModal({ title: title, width: 410, html: "Выберите кандидата, за которого вы хотите отдать свой голос" });
				}
			}
		}

		controller.OnBack = function ()
		{
			if (null != this.cvoting)
				this.SafeDestroyPlacedController();
		}

		controller.СохранитьРезультат = function (Голосующий, ПроголосовалЗа, Кандидаты)
		{
			var decoded_url = codec_url().Decode(window.location);
			var args = codec_url_args().Decode(decoded_url);
			var license_id = args.license_id;

			var sel = this.fastening.selector;
			var self = this;
			var url = this.base_url + '?action=award.vote';
			var v_ajax = h_msgbox.ShowAjaxRequest("Сохранение информации об отданном голосе", url);
			var data = { Голосующий: Голосующий, ПроголосовалЗа: ПроголосовалЗа, license_id: license_id };
			v_ajax.ajax({
				dataType: "json"
					, type: 'POST'
					, data: data
					, cache: false
					, success: function (data, textStatus)
					{
						Голосующий.ПроголосовалЗа = ql.find_first_or_null(Кандидаты, function (k)
						{
							return k.ИНН == ПроголосовалЗа.ИНН;
						});
						var html = h_award.ЗаКогоПроголосовал(Голосующий);
						$(sel + ' div.am-card').html(html);
					}
			});
		}

		return controller;
	}
});