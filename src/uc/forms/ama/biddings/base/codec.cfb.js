define([
	  'forms/base/codec/xml/codec.xsd.xml'
	, 'forms/base/log'
	, 'forms/ama/biddings/base/codec.cfb.scheme'
	, 'txt!forms/ama/biddings/base/cfb.xsd'
	, 'forms/base/codec/codec.copy'
],
function (BaseCodec, GetLogger, scheme, cfb_xsd, ccopy)
{
	var log = GetLogger('codec.cfb');
	return function ()
	{
		var codec = BaseCodec();
		codec.schema = scheme;
		codec.GetScheme = function ()
		{
			var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
			xs.add(this.GetRootNamespaceURI(), this.LoadXsd(cfb_xsd));
			return xs;
		}
		codec.GetRootNamespaceURI = function () { return "http://probili.ru/bidding/transit"; }

		var fix_fields= function(data,fix_field)
		{
			var fixed_data = data;
			fixed_data = fix_field(fixed_data, 'Определение_участников');

			fixed_data = fix_field(fixed_data, 'окончание_прёма_заявок');
			fixed_data = fix_field(fixed_data, 'начало_приема_заявок');
			fixed_data = fix_field(fixed_data, 'начало_торгов');

			fixed_data = fix_field(fixed_data, 'Процедура.Должник.Дата_рождения');
			fixed_data = fix_field(fixed_data, 'Процедура.Должник.Регистрация_по_месту_жительства');
			fixed_data = fix_field(fixed_data, 'Процедура.Должник.снилс');
			fixed_data = fix_field(fixed_data, 'Процедура.Должник.Место_рождения');
			fixed_data = fix_field(fixed_data, 'Процедура.Должник.Почта');

			fixed_data = fix_field(fixed_data, 'Лоты.[Лот].Объект_конкурсной_массы');
			fixed_data = fix_field(fixed_data, 'Лоты.[Лот].ОКДП');

			fixed_data = fix_field(fixed_data, 'Процедура.Арбитражный_управляющий.Моб_телефон');
			fixed_data = fix_field(fixed_data, 'Процедура.Арбитражный_управляющий.ИОФамилия');
			fixed_data = fix_field(fixed_data, 'Процедура.Арбитражный_управляющий.ФИО_род');
			fixed_data = fix_field(fixed_data, 'Процедура.Арбитражный_управляющий.Должность_род');
			fixed_data = fix_field(fixed_data, 'Процедура.Арбитражный_управляющий.Должность_твор');
			fixed_data = fix_field(fixed_data, 'Процедура.Арбитражный_управляющий.Город');
			fixed_data = fix_field(fixed_data, 'Процедура.Арбитражный_управляющий.Почта');
			fixed_data = fix_field(fixed_data, 'Процедура.АС_род');
			fixed_data = fix_field(fixed_data, 'Процедура.АС_твор');
			fixed_data = fix_field(fixed_data, 'Процедура.Текущая_дата');
			fixed_data = fix_field(fixed_data, 'Процедура.Дата_решения');
			return fixed_data;
		}

		var base_Decode = codec.Decode;

		var remove_field = function (model, field_name)
		{
			var fixed_model = model;
			var name_parts = field_name.split('.');
			for (var i = 0; i < name_parts.length - 1; i++)
			{
				var name_part = name_parts[i];
				if (!fixed_model[name_part])
					return model;
				fixed_model = fixed_model[name_part];
			}
			var last_name = name_parts[name_parts.length - 1];
			if (fixed_model[last_name])
				delete fixed_model[last_name];
			return model;
		}

		var base_Encode = codec.Encode;
		codec.Encode= function(data)
		{
			var data_copy = ccopy().Encode(data);
			var fixed_data = fix_fields( data_copy, remove_field );

			if ( fixed_data.Лоты && null != fixed_data.Лоты )
			{
				for ( var i = 0; i < fixed_data.Лоты.length; i++ )
				{
					var Лот = fixed_data.Лоты[i];
					if ( Лот.Объект_конкурсной_массы )
						delete Лот.Объект_конкурсной_массы;
					if ( Лот.ОКДП )
						delete Лот.ОКДП;
				}
			}
			if ( fixed_data )
				delete fixed_data.bidding_id;
			
			if ( fixed_data.Процедура.Должник && null != fixed_data.Процедура.Должник )
			{
				fixed_data.Процедура.Должник = {};
				if ( data.Процедура.Должник.Наименование )
					fixed_data.Процедура.Должник.Наименование = data.Процедура.Должник.Наименование;
				if ( data.Процедура.Должник.ИНН )
					fixed_data.Процедура.Должник.ИНН = data.Процедура.Должник.ИНН;
				if ( data.Процедура.Должник.ОГРН )
					fixed_data.Процедура.Должник.ОГРН = data.Процедура.Должник.ОГРН;
				if ( data.Процедура.Должник.Адрес )
					fixed_data.Процедура.Должник.Адрес = data.Процедура.Должник.Адрес;
			}

			if ( fixed_data.Процедура.Арбитражный_управляющий && null != fixed_data.Процедура.Арбитражный_управляющий )
			{
				fixed_data.Процедура.Арбитражный_управляющий = {};
				if ( data.Процедура.Арбитражный_управляющий.Фамилия )
					fixed_data.Процедура.Арбитражный_управляющий.Фамилия = data.Процедура.Арбитражный_управляющий.Фамилия;
				if ( data.Процедура.Арбитражный_управляющий.Имя )
					fixed_data.Процедура.Арбитражный_управляющий.Имя = data.Процедура.Арбитражный_управляющий.Имя;
				if ( data.Процедура.Арбитражный_управляющий.Отчество )
					fixed_data.Процедура.Арбитражный_управляющий.Отчество = data.Процедура.Арбитражный_управляющий.Отчество;
				if ( data.Процедура.Арбитражный_управляющий.ИНН )
					fixed_data.Процедура.Арбитражный_управляющий.ИНН = data.Процедура.Арбитражный_управляющий.ИНН;
				if ( data.Процедура.Арбитражный_управляющий.Должность )
					fixed_data.Процедура.Арбитражный_управляющий.Должность = data.Процедура.Арбитражный_управляющий.Должность;
				if ( data.Процедура.Арбитражный_управляющий.Почтовый_адрес )
					fixed_data.Процедура.Арбитражный_управляющий.Почтовый_адрес = data.Процедура.Арбитражный_управляющий.Почтовый_адрес;
			}

			var encoded_txt = base_Encode.call(this, fixed_data);
			base_Decode.call(this, encoded_txt);
			return encoded_txt;
		}

		var remove_simple_tag = function (txt, tname)
		{
			var rx_txt = '\\n[ \\t]*<' + tname + '>([\\s\\S]*)<\\/' + tname + '>\\r*\\n+';
			var txt_without_field = txt.replace(new RegExp(rx_txt, 'g'), '\n');
			return txt_without_field;
		}

		var remove_tag_part = function (txt, parts, ipart, part)
		{
			var tag_start = '<' + part + '>';
			var tag_close = '</' + part + '>';

			var istart = 0;
			var fixed_txt = '';

			for (var i= 0; i<3; i++)
			{
				var pos_tag_start = txt.indexOf(tag_start,istart);
				if (-1 == pos_tag_start)
				{
					break;
				}
				else
				{
					var before_open_tag = txt.substring(istart, pos_tag_start);
					fixed_txt += before_open_tag;
					var pos_tag_close = txt.indexOf(tag_close, pos_tag_start);
					if (-1 != pos_tag_close)
					{
						var istop = pos_tag_close + tag_close.length;
						var to_fix = txt.substring(pos_tag_start, istop);
						istart = istop;
						var fixed_tag_content = remove_tag_parts(to_fix, parts, ipart + 1);
						fixed_txt += fixed_tag_content;
					}
				}
			}

			var after_stop_tag = txt.substring(istart, txt.length);
			fixed_txt += after_stop_tag;

			return fixed_txt;
		}

		var remove_tag_parts = function (txt, parts, ipart)
		{
			if (ipart == (parts.length - 1))
			{
				return remove_simple_tag(txt, parts[ipart]);
			}
			else
			{
				var part = parts[ipart];
				if ('[' == part.charAt(0))
					part = part.substring(1, part.length - 1);
				return remove_tag_part(txt, parts, ipart, part);
			}
			
		}

		var remove_tag = function (txt, tname)
		{
			var parts = tname.split('.');
			if (null==parts || 0==parts.length)
			{
				return txt;
			}
			else if (1==parts.length)
			{
				return remove_simple_tag(txt, tname);
			}
			else
			{
				return remove_tag_parts(txt, parts, 0);
			}
		}

		codec.Decode= function(txt)
		{
			var fixed_txt = fix_fields(txt, remove_tag);
			var res = base_Decode.call(this, fixed_txt);
			return res;
		}

		return codec;
	}
});
