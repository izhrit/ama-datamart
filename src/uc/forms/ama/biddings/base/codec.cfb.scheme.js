define(function ()
{
	return {
		tagName: 'Объявление_о_торгах'
		, fields: 
		{
			Наименование: { type: 'string' }

			, URL: { type: 'string' }

			, Начало_приёма_заявок: { fields: { дата: { type: 'string' }, время: { type: 'string' } } }
			, Окончание_приёма_заявок: { fields: { дата: { type: 'string' }, время: { type: 'string' } } }
			, Начало_торгов: { fields: { дата: { type: 'string' }, время: { type: 'string' } } }

			, Объявление:
				{
					fields:
						{
							  ЕФРСБ: { fields: { Номер: { type: 'string' } } }
							, ФедеральноеСМИ: { fields: { дата: { type: 'string' }, номер_тиража: { type: 'string' } } }
							, МестноеСМИ: { fields: { Наименование: { type: 'string' }, дата: { type: 'string' }, номер_тиража: { type: 'string' } } }
						}
				}

			, Форма_торгов: { type: 'string' }

			, ЭТП:
				{
					fields:
						{
							text: { type: 'string' }
							, url: { type: 'string' }
							, operator: { type: 'string' }
							, uploadurl: { type: 'string' }
							, format: { type: 'string' }
						}
				}



			, Лоты: {
				type: 'array', item: {
					tagName: 'Лот'
					, fields:
						{
							  Наименование: { type: 'string' }
							, Начальная_цена: { type: 'string' }
							, Размер_задатка: { type: 'string' }
							, Размер_задатка_ед: { type: 'string' }
							, Шаг_аукциона: { type: 'string' }
							, Шаг_аукциона_ед: { type: 'string' }
							, Краткое_описание: { type: 'string' }
							, Точный_адрес: { type: 'string' }
							, Порядок_ознакомления: { type: 'string' }
						}
				}
			}

			, Процедура: {
				fields:
					{
						Должник: {
							fields: {
								  Наименование: { type: 'string' }
								, ИНН: { type: 'string' }
								, ОГРН: { type: 'string' }
							}
						}
						, Арбитражный_управляющий: {
							fields: {
								Фамилия: { type: 'string' }
								, Имя: { type: 'string' }
								, Отчество: { type: 'string' }
								, ИНН: { type: 'string' }
							}
						}
						, СРО: { type: 'string' }
						, АС: { type: 'string' }
						, Номер_дела_о_банкротстве: { type: 'string' }
					}
			}
		}
	};
});
