define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/biddings/lots/e_lots.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/biddings/lot/c_lot'
	, 'forms/base/h_validation_msg'
	, 'txt!forms/ama/biddings/lots/s_lots.css'
],
function (c_fastened, tpl, h_msgbox, c_lot, h_validation_msg, css)
{
	return function()
	{
		var BuildClassesForItem= function(lots,i)
		{
			var lot = lots[i];
			var res = '';
			if (!lot.Начальная_цена || '' == lot.Начальная_цена || null == lot.Начальная_цена)
				res += ' empty-price-start';
			if (!lot.Размер_задатка || '' == lot.Размер_задатка || null == lot.Размер_задатка)
				res += ' empty-price-0';
			if (!lot.Шаг_аукциона || '' == lot.Шаг_аукциона || null == lot.Шаг_аукциона)
				res += ' empty-price-step';
			if (1 == lots.length)
				res += ' single-lot';
			if ( 1 < lots.length )
				res += ' not-single-lot';
			return res;
		}

		var controller = c_fastened(tpl, { css: css });

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$( sel + ' a.ama-icon.ama-icon-plus' ).click( function( e ) { e.preventDefault(); self.OnAdd(); } );

			var $lots= $(sel);
			$lots.on('delete', function (e,i) { self.OnDelete(i); });
			$lots.on('edit', function (e,i) { self.OnEdit(i); });

			this.FixItemClasses();
		}

		controller.FixItemClasses= function(model)
		{
			var sel= this.fastening.selector;
			var lots= model ? model : this.model;
			if (lots && null!=lots)
			{
				for (var i= 0; i< lots.length; i++)
				{
					var classes= BuildClassesForItem(lots,i);
					var $item= $(sel + ' [data-array-index="' + i + '"]');
					$item.removeClass();
					$item.addClass(classes);
				}
			}
		}

		controller.OnAdd= function()
		{
			var fastening= this.fastening;
			var sel = fastening.selector;
			var editor = c_lot();
			var SaveButtonName = 'Сохранить параметры лота';
			editor.SetFormContent({});
			var self = this;
			h_msgbox.ShowModal
				({
					controller: editor
					, title: 'Добавление выставляемого на торги лота'
					, width: editor.size.width
					, height: editor.size.height
					, resizable: false
					, id_div: 'cpw-ama-bidding-modal-lot'
					, buttons: [SaveButtonName, 'Отмена']
					, onclose: function (bname, dlg)
					{
						if (SaveButtonName == bname)
						{
							h_validation_msg.IfOkWithValidateResult(editor.Validate(), function ()
							{
								
								var lot = editor.GetFormContent();
								var fc_dom_item= $(sel);
								var model= fastening.get_fc_model_value(fc_dom_item);
								if (!model || null == model)
								{
									model = [lot];
								}
								else
								{
									model.push(lot);
								}
								fastening.set_fc_model_value(fc_dom_item, model);
								self.FixItemClasses(model);
								if (self.IncrementVersion)
									self.IncrementVersion();
								$(dlg).dialog('close');
							});
							return false;
						}
					}
				});
		}

		controller.OnEdit = function (i_item)
		{
			var fastening= this.fastening;
			var sel= fastening.selector;
			var fc_dom_item= $(sel);
			var model= fastening.get_fc_model_value(fc_dom_item);
			var lot= model[i_item];

			var editor = c_lot();

			var SaveButtonName = 'Сохранить параметры лота';

			editor.SetFormContent(lot);

			var self = this;
			h_msgbox.ShowModal
				({
					controller: editor
					, title: 'Редактирование лота № ' + (parseInt(i_item) + 1) + ' "' + lot.Наименование + '"'
					, width: editor.size.width
					, height: editor.size.height
					, resizable: false
					, id_div: 'cpw-ama-bidding-modal-lot'
					, buttons: [SaveButtonName, 'Отмена']
					, onclose: function (bname, dlg)
					{
						if (SaveButtonName == bname)
						{
							h_validation_msg.IfOkWithValidateResult(editor.Validate(), function ()
							{
								model[i_item]= editor.GetFormContent();
								fastening.set_fc_model_value(fc_dom_item, model);
								self.FixItemClasses(model);
								if (self.IncrementVersion)
									self.IncrementVersion();
								$(dlg).dialog("close");
							});
							return false;
						}
					}
				});
		}

		controller.OnDelete = function (i_item)
		{
			var fastening= this.fastening;
			var sel= fastening.selector;
			var fc_dom_item= $(sel);
			var model= fastening.get_fc_model_value(fc_dom_item);
			var lot = model[i_item];
			var delete_item = parseInt(i_item) + 1;
			var self = this;
			var OkButtonName = 'Да, удалить лот';
			h_msgbox.ShowModal
				({
					html: 'Вы действительно хотите удалить лот № ' + delete_item + ' "' + lot.Наименование + '"?'
					, title: 'Подтверждение удаления лота'
					, width: 500, height: 200
					, buttons: [OkButtonName, 'Отмена']
					, onclose: function (bname)
					{
						if (OkButtonName == bname)
						{
							model.splice(i_item, 1);
							fastening.set_fc_model_value(fc_dom_item, model);
							self.FixItemClasses(model);
							if (self.IncrementVersion)
								self.IncrementVersion();
						}
					}
				});
		}

		return controller;
	}
});
