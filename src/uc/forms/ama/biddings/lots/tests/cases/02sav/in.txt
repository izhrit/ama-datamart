include ..\..\..\..\lot\tests\cases\in.lib.txt quiet

wait_text "Лоты"
shot_check_png ..\..\shots\0new.png

click_text "Добавить"
wait_click_text "Отмена"
play_stored_lines lot_fields_1
click_text "Сохранить параметры лота"

shot_check_png ..\..\shots\02sav_1.png

click_text "Добавить"
wait_click_text "Отмена"
play_stored_lines lot_fields_2
click_text "Сохранить параметры лота"
shot_check_png ..\..\shots\02sav.png

wait_click_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02sav.json.result.txt

exit