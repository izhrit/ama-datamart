include ..\..\..\..\lot\tests\cases\in.lib.txt quiet

wait_text "Лоты"
shot_check_png ..\..\shots\02sav.png

je $('[data-array-index="0"] a.ama-icon-remove').click();
wait_text "Вы действительно хотите"
shot_check_png ..\..\shots\02sav_ask_to_delete.png
click_text "Отмена"

shot_check_png ..\..\shots\02sav.png

je $('[data-array-index="0"] a.ama-icon-remove').click();
wait_text "Вы действительно хотите"
shot_check_png ..\..\shots\02sav_ask_to_delete.png
click_text "Да, удалить лот"

shot_check_png ..\..\shots\02sav_deleted.png

je $('[data-array-index="0"] a.ama-icon-edit').click();
wait_text "Редактирование"
shot_check_png ..\..\shots\02sav_edit.png
js wbt.SetModelFieldValue("Наименование", "Байк!");
shot_check_png ..\..\shots\02sav_edit_changed.png
click_text "Отмена"
shot_check_png ..\..\shots\02sav_deleted.png

je $('[data-array-index="0"] a.ama-icon-edit').click();
wait_text "Редактирование"
shot_check_png ..\..\shots\02sav_edit.png
js wbt.SetModelFieldValue("Наименование", "Байк!");
shot_check_png ..\..\shots\02sav_edit_changed.png
click_text "Сохранить параметры лота"

shot_check_png ..\..\shots\02sav_edited.png

wait_click_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\04sav.json.result.txt

exit