define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/biddings/kmobjects/e_kmobjects.html'
	, 'txt!forms/ama/biddings/kmobjects/s_kmobjects.css'
	, 'tpl!forms/ama/biddings/kmobjects/v_kmobject_select2_item.html'
	, 'tpl!forms/ama/biddings/kmobjects/v_kmobject_confirm_delete.html'
	, 'forms/base/h_msgbox'
	, 'forms/base/codec/codec.copy'
],
function (c_fastened, tpl, css, v_kmobject_select2_item, v_kmobject_confirm_delete, h_msgbox, copy_codec)
{
	return function(options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/external');

		var find_km_index= function(kmobjects,kmobject_to_find)
		{
			if (kmobjects && null!=kmobjects && kmobjects.length)
			{
				for (var i= 0; i < kmobjects.length; i++)
				{
					var kmobject= kmobjects[i];
					if (kmobject.id==kmobject_to_find.id)
						return i;
				}
			}
			return -1;
		};

		var options = {
			css:css
			, field_spec:
			{
				Объект_КМ_для_добавления:
				{
					ajax: {
						dataType: 'json'
						, url: base_url + '?action=km.select2'
					}
					, placeholder: 'Выбрать объект конкурсной массы для добавления'
					, formatResult: function (item)
					{
						var obj = { kmobject: item.kmobject };
						/* TODO: возможна проблема использования статического объекта.. !!! */
						var model= controller.GetFormContent();
						if (model && model.Объекты_КМ)
							obj.selected= -1!=find_km_index(model.Объекты_КМ,item.kmobject );
						return v_kmobject_select2_item(obj);
					}
					,allowClear: true
				}
			}
		};
		var controller= c_fastened(tpl, options);

		controller.size = {width:800,height:600};

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' [data-fc-selector="Объект_КМ_для_добавления"]').on("change", function (e) { self.OnAdd(e); });

			var $kmobjects= $(sel + ' [data-fc-selector="Объекты_КМ"]');
			$kmobjects.on('delete', function (e,i) { self.OnDelete(i); });
		}

		controller.OnAdd= function(e)
		{
			var added= e.added;
			if (added)
			{
				var kmobject = added.kmobject;

				var fastening= this.fastening;
				var sel = fastening.selector;
				$(sel + ' [data-fc-selector="Объект_КМ_для_добавления"]').select2('data', null);

				var fc_dom_item= $(sel + ' [data-fc-selector="Объекты_КМ"]');
				var model= fastening.get_fc_model_value(fc_dom_item);
				if (!model || null==model)
					model= [];
				if (-1==find_km_index(model,kmobject))
				{
					model.push(kmobject);
					fastening.set_fc_model_value(fc_dom_item, model);
					$(sel).trigger('model_change');
				}
			}
		}

		controller.OnDelete= function(i)
		{
			var fastening= this.fastening;
			var sel = fastening.selector;

			var fc_dom_item= $(sel + ' [data-fc-selector="Объекты_КМ"]');
			var kmobjects= fastening.get_fc_model_value(fc_dom_item);
			if (kmobjects && null!=kmobjects)
			{
				var to_delete = copy_codec().Copy(kmobjects[i]);
				var btnOK= 'Да, удалить объект конкурсной массы из лота';
				h_msgbox.ShowModal
				({
					title: 'Подтверждение удаления объекта конкурсной массы из лота'
					, html: v_kmobject_confirm_delete(to_delete)
					, buttons: [btnOK,'Отмена'], width: 700
					, id_div: "cpw-form-ama-datamart-msg-delete-kmobject-from-lot"
					, onclose: function (btn)
					{
						if (btnOK == btn)
						{
							kmobjects.splice(i, 1);
							fastening.set_fc_model_value(fc_dom_item, kmobjects);
							$(sel).trigger('model_change');
						}
					}
				});
			}
		}

		var codec = {
			Encode: function(data)
			{
				var res= [];
				if (data && data.Объекты_КМ)
					res= data.Объекты_КМ;
				return res;
			}
			,Decode: function(data)
			{
				if (!data)
				{
					return data;
				}
				else
				{
					if ('string'==typeof data)
						data= JSON.parse(data);
					return {Объекты_КМ:data};
				}
			}
		}

		controller.UseCodec(codec);

		return controller;
	}
});
