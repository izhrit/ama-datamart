define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/biddings/recipients/e_recipients.html'
	, 'forms/base/h_msgbox'
	, 'txt!forms/ama/biddings/recipients/s_recipients.css'
],
function (c_fastened, tpl, h_msgbox, css)
{
	return function(options_arg)
	{
		var to_select= !options_arg || !options_arg.to_select ? [] : options_arg.to_select
		var for_start= !options_arg || !options_arg.for_start ? false : options_arg.for_start;
		var options= { css:css, to_select: to_select, for_start: for_start };
		var controller= c_fastened(tpl, options);

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			if (for_start && !this.model)
				this.model = {Transmit:'Yes'};

			base_Render.call(this,sel);

			var self= this;
			$(sel + ' div.hint.all').click(function (e) { self.OnClickHint(e); });
			$(sel + ' div.hint.selected').click(function (e) { self.OnClickHint(e); });

			$(sel + ' input[data-fc-selector="ToSelectedPro"]').change(function () { self.OnChangePro(); });
			$(sel + ' input[data-fc-selector="ToAll"]').change(function () { self.OnChangeAll(); });
			$(sel + ' div.list input').change(function (e) { self.OnChangeProItem(e); });
		}

		controller.OnClickHint= function(e)
		{
			var $hint= $(e.target);
			var title= $hint.attr('title');
			var title_parts= title.split(':');

			h_msgbox.ShowModal({
				title: title_parts[0]
				,html: '<p>' + title_parts[0] + ':</p><p style="font-weight:bold;padding-left:20px">' + title_parts[1] + '</p>'
				,width: 500
				, dialogClass: 'cpw-hint-dialog'
			});
		}

		controller.OnChangeAll= function()
		{
			var sel= this.fastening.selector;
			if ('checked'==$(sel + ' input[data-fc-selector="ToAll"]').attr('checked'))
			{
				$(sel + ' input[data-fc-selector="Transmit"][value="Yes"]').attr('checked','checked');
			}
		}

		controller.OnChangePro= function()
		{
			var sel= this.fastening.selector;
			var ToSelectedPro_checked= 'checked'==$(sel + ' input[data-fc-selector="ToSelectedPro"]').attr('checked');
			if (ToSelectedPro_checked)
			{
				$(sel + ' input[data-fc-selector="Transmit"][value="Yes"]').attr('checked','checked');
				if (0 == $(sel + ' div.list input:checked').length)
					$(sel + ' div.list input').attr('checked', 'checked');
			}
		}

		controller.OnChangeProItem= function(e)
		{
			var sel= this.fastening.selector;
			var $checkbox= $(e.target);
			if ('checked' == $checkbox.attr('checked'))
			{
				$(sel + ' input[data-fc-selector="Transmit"][value="Yes"]').attr('checked','checked');
				$(sel + ' input[data-fc-selector="ToSelectedPro"]').attr('checked', 'checked');
			}
		}

		var base_GetFormContent= controller.GetFormContent;
		controller.GetFormContent= function()
		{
			var sel= this.fastening.selector;
			var res= base_GetFormContent.call(this);
			if ('Yes'!=res.Transmit)
			{
				res = {Transmit:false,ToAll:false,ToSelectedPro:false};
			}
			else
			{
				res.Transmit= true;
				res.SelectedPro= [];
				for (var i= 0; i < to_select.length; i++)
				{
					var item= to_select[i];
					var $cbox= $(sel + ' div.list input[data-id="' + item.id + '"]');
					if ('checked'==$cbox.attr('checked'))
						res.SelectedPro.push(item);
				}
			}
			return res;
		}

		var base_SetFormContent= controller.SetFormContent;
		controller.SetFormContent= function(model)
		{
			if ('string'==typeof model)
				model= JSON.parse(model);
			model.Transmit= (true==model.Transmit) ? 'Yes' : 'No';
			base_SetFormContent.call(this,model);
		}

		return controller;
	}
});