define(['forms/ama/biddings/recipients/c_recipients'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'property_recipients'
		, Title: 'Выбор адресатов экспозиции КМ'
	};
	return form_spec;
});
