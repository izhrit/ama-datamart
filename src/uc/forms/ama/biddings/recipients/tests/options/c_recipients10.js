define([
	'forms/ama/biddings/recipients/c_recipients'
],
function (c_recipients)
{
	return function () {
		var to_select = [
			{
				id:"1"
				,name:"Банкрот форум"
				,url:"https://bankrotforum.ru/"
				,descr:"Помощь в организации торгов"
			}
			,{
				id:"3"
				,name:"Агентство независимых экспертиз «Гранд Истейт»"
				,url:"https://grand-ocenka.ru/"
				,descr:"Оценка имущества"
			}
			,{
				id:"2"
				,name:"Портал ДА"
				,url:"https://portal-da.ru/"
				,descr:"Публикация информации об имуществе в наличии широкому (неопределённому) кругу лиц"
			}
			,{ id:"4", name:"Профи 4", url:"" }
			,{ id:"5", name:"Профи 5", url:"" }
			,{ id:"6", name:"Профи 6", url:"" }
			,{ id:"7", name:"Профи 7", url:"" }
			,{ id:"8", name:"Профи 8", url:"" }
			,{ id:"9", name:"Профи 9", url:"" }
			,{ id:"10", name:"Профи 10", url:"" }
		];
		var controller = c_recipients({to_select:to_select, for_start: true});
		return controller;
	}
});