define([
	'forms/ama/biddings/recipients/c_recipients'
],
function (c_recipients)
{
	return function () {
		var to_select = [
			{
				id:"3"
				,name:"Агентство независимых экспертиз «Гранд Истейт»"
				,url:"https://grand-ocenka.ru/"
				,descr:"Оценка имущества"
			}
			,{
				id:"1"
				,name:"Банкрот форум"
				,url:"https://bankrotforum.ru/"
				,descr:"Помощь в организации торгов"
			}
			,{
				id:"2"
				,name:"Портал ДА"
				,url:"https://portal-da.ru/"
				,descr:"Публикация информации об имуществе в наличии максимально широкому, неопределённому кругу лиц"
			}
		];
		var controller = c_recipients({to_select:to_select});
		return controller;
	}
});