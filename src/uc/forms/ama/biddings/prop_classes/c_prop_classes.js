define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/biddings/prop_classes/e_prop_classes.html'
	, 'forms/ama/biddings/efrsb_classes/choose/c_choose_efrsb_classes'
	, 'forms/ama/biddings/efrsb_classes/base/h_for_a_modal'
	, 'forms/ama/biddings/prop_classes/h_category'
],
function (c_fastened, tpl, c_choose_efrsb_classes, efrsb_classes_for_a_modal, h_category)
{
	return function()
	{
		var options =
		{
			h_category: h_category
			,field_spec:
			{
				КлассификацияЕФРСБ:
				{
					title:'Классификация имущества в соответствии с ЕФРСБ'
					,controller: c_choose_efrsb_classes
					,text:efrsb_classes_for_a_modal.text
				}
			}
		};

		var controller= c_fastened(tpl, options);

		return controller;
	}
});