﻿define([
	  'forms/base/codec/xml/codec.xsd.xml'
	, 'txt!forms/ama/biddings/bidding/sber-PurchaseCreate.xsd'
	, 'forms/base/log'
],
function (BaseCodec, sber_PurchaseCreate_xsd, GetLogger)
{
	var log = GetLogger('x_bidding_sber_PurchaseCreate');
	return function ()
	{
		log.Debug('Create {');
		var codec = BaseCodec();

		/*
		Поля которые требует Сбербанк АСТ, но их нет в АМА:
		- efrpublicdate
		- registrationdocuments
		- bidregion
		- bidcategory
		*/

		codec.GetScheme = function ()
		{
			var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
			xs.add("http://www.norbit.ru/XMLSchema", this.LoadXsd(sber_PurchaseCreate_xsd));
			return xs;
		}

		codec.GetRootNamespaceURI = function () { return "http://www.norbit.ru/XMLSchema"; }

		codec.schema =
			{
				tagName: 'purchase'
				, fields:
					{
						bids: { type:'array', item: { tagName: 'bid' } }
					}
			};

		codec.Encode_debtorinfo = function (data)
		{
			var должник = data.Процедура.Должник;
			var debtorinfo = { // Сведения о должнике, его имуществе 
					  personphis: 'No'
					, debtorname: должник.Наименование
					, debtorinn: должник.ИНН
					, debtorogrn: должник.ОГРН
				};
			return debtorinfo;
		}

		codec.Decode_debtorinfo = function (sber_data, data)
		{
			var debtorinfo = sber_data.debtorinfo;
			if (null != debtorinfo)
			{
				var должник = data.Процедура.Должник;
				должник.Наименование = debtorinfo.debtorname;
				должник.ИНН = debtorinfo.debtorinn;
				должник.ОГРН = debtorinfo.debtorogrn;
			}
		}

		codec.Encode_purchaseinfo = function (data)
		{
			var purchaseinfo =
				{
					  idefrsb: data.Объявление.ЕФРСБ.Номер
					, purchasename: data.Наименование
					, purchasetypeinfo:
					{
						purchasetypename: data.Форма_торгов
					}
					, sitepublicdate: data.Объявление.ФедеральноеСМИ.дата
					, paperpublicdate: data.Объявление.МестноеСМИ.дата
					, efrpublicdate: '10.10.2017'
				};
			return purchaseinfo;
		}

		codec.Decode_purchaseinfo = function (sber_data, data)
		{
			var purchaseinfo = sber_data.purchaseinfo;
			data.Объявление.ЕФРСБ.Номер = purchaseinfo.idefrsb;
			data.Наименование = purchaseinfo.purchasename;
			data.Форма_торгов = purchaseinfo.purchasetypeinfo.purchasetypename;
			data.Объявление.ФедеральноеСМИ.дата = purchaseinfo.sitepublicdate;
			data.Объявление.МестноеСМИ.дата = purchaseinfo.paperpublicdate;
		}

		codec.trim= function(s)
		{
			return (!s || null == s || '' == s) ? '' : s.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
		}

		codec.Encode_Дата_Время = function (dt) { return dt.дата + ' ' + dt.время; }
		codec.Decode_Дата_Время = function (dt)
		{
			var parts = dt.split(' ');
			return { дата: this.trim(parts[0]), время: this.trim(parts[1]) };
		}

		codec.Encode_requestinfo = function (data)
		{
			var requestinfo= // Порядок представления заявок на участие в торгах (предложений о цене) 
				{
					requeststartdate: this.Encode_Дата_Время(data.Начало_приёма_заявок)
					, requeststopdate: this.Encode_Дата_Время(data.Окончание_приёма_заявок)
					, registrationdocuments: ' ' //Обязательное поле
				}
			return requestinfo;
		}

		codec.Decode_requestinfo = function (sber_data, data)
		{
			var requestinfo = sber_data.requestinfo;
			if (null != requestinfo)
			{
				data.Начало_приёма_заявок = this.Decode_Дата_Время(requestinfo.requeststartdate);
				data.Окончание_приёма_заявок = this.Decode_Дата_Время(requestinfo.requeststopdate);
			}
		}

		codec.Encode_terms = function (data)
		{
			var terms = // Порядок проведения торгов
			{
				purchaseauctionstartdate: this.Encode_Дата_Время(data.Начало_торгов)
			};
			return terms;
		}

		codec.Decode_terms = function (sber_data, data)
		{
			var terms = sber_data.terms;
			if (null != terms)
			{
				data.Начало_торгов = this.Decode_Дата_Время(terms.purchaseauctionstartdate);
			}
		}

		codec.EncodeFIO= function(fio)
		{
			return fio.Фамилия + ' ' + fio.Имя + ' ' + fio.Отчество;
		}

		codec.DecodeFIO = function (fio,АУ)
		{
			var parts = fio.split(' ');
			АУ.Фамилия= parts[0];
			АУ.Имя = 2 > parts.length ? '' : parts[1];
			АУ.Отчество = 3 > parts.length ? '' : parts[2];
		}

		codec.Encode_businesinfo = function (data)
		{
			var businesinfo= // Сведения об арбитражном суде
			{
				businessname: data.Процедура.АС
				, businessno: data.Процедура.Номер_дела_о_банкротстве
				, businessreason: ' ' //Обязательное поле
			};
			return businesinfo;
		}

		codec.Decode_businesinfo = function (sber_data, data)
		{
			var businesinfo= sber_data.businesinfo;
			if (null != businesinfo)
			{
				data.Процедура.АС = businesinfo.businessname;
				data.Процедура.Номер_дела_о_банкротстве = businesinfo.businessno;
			}
		}

		codec.Encode_crisicmanagerinfo = function (data)
		{
			var АУ = data.Процедура.Арбитражный_управляющий;
			var crisicmanagerinfo=  // Сведения об арбитражном управляющем 
			{
				crisicmanagerfullname: this.EncodeFIO(АУ)
				, crisismanagerinn: АУ.ИНН
				, arbitrageorganizationpanel: {arbitrageorganization: data.Процедура.СРО }
			};
			return crisicmanagerinfo;
		}

		codec.Decode_crisicmanagerinfo = function (sber_data, data)
		{
			var АУ = data.Процедура.Арбитражный_управляющий;
			var crisicmanagerinfo = sber_data.crisicmanagerinfo;
			if (null!=crisicmanagerinfo)
			{
				this.DecodeFIO(crisicmanagerinfo.crisicmanagerfullname, АУ)
				data.Процедура.Арбитражный_управляющий.ИНН = crisicmanagerinfo.crisismanagerinn;
				data.Процедура.СРО = crisicmanagerinfo.arbitrageorganizationpanel.arbitrageorganization;
			}
		}

		codec.Encode_data_without_bids = function (data)
		{
			var sber_data =
			{
				  purchaseinfo: this.Encode_purchaseinfo(data)
				, debtorinfo: this.Encode_debtorinfo(data)// Сведения о должнике, его имуществе 
				, bids: []
				, crisicmanagerinfo: this.Encode_crisicmanagerinfo(data) // Сведения об арбитражном управляющем 
				, businesinfo: this.Encode_businesinfo(data) // Сведения об арбитражном суде
				, requestinfo: this.Encode_requestinfo(data) // Порядок представления заявок на участие в торгах (предложений о цене) 
				, terms: this.Encode_terms(data)// Порядок проведения торгов
			};
			return sber_data;
		}

		codec.debtorbidname_address_splitter = '\nТочный адрес: ';
		codec.Encode_data_bid_debtorbidname = function (Лот)
		{
			return Лот.Краткое_описание + ('' == Лот.Точный_адрес ? '' : this.debtorbidname_address_splitter + Лот.Точный_адрес);
		}

		codec.Decode_data_bid_debtorbidname = function (debtorbidname,Лот)
		{
			var parts = debtorbidname.split(this.debtorbidname_address_splitter)
			Лот.Краткое_описание = parts[0];
			Лот.Точный_адрес = parts.length < 2 ? '' : parts[1];
		}

		codec.Encode_data_bids = function (data,sber_data)
		{
			var bids = sber_data.bids;

			for (var i = 0; i < data.Лоты.length; i++)
			{
				var Лот = data.Лоты[i];

				bids.push({
					bidpanel:
						{
							bidinfo:
								{
									bidno: i + ''
									, bidname: Лот.Наименование
								}
							, biddebtorinfo:
								{
									debtorbidname: this.Encode_data_bid_debtorbidname(Лот)
									, bidregion: 'Удмуртия'
									, bidcategory: '01'
									, bidinventoryresearchtype: 0 == Лот.Порядок_ознакомления.length ? 'Обычный порядок' : Лот.Порядок_ознакомления
								}
							, bidtenderinfo:
								{
									bidprice: Лот.Начальная_цена
									, bidauctionsteppercent: Лот.Шаг_аукциона
								}
							, biddepositinfo:
								{
									isdepositinpercent: Лот.Размер_задатка_ед == '%' ? 'Yes' : 'No'
									, biddeposit: Лот.Размер_задатка
								}
						}
				});
			};
		}

		codec.Decode_data_bids = function (sber_data, data)
		{
			var Лоты = data.Лоты;
			var bids = sber_data.bids;
			if (('object' == typeof bids) && bids.bid)
			{
				bids = bids.bid;
			}
			for (var i = 0; i < bids.length; i++)
			{
				var bidpanel = bids[i].bidpanel;
				var Лот=
					{
						Наименование: bidpanel.bidinfo.bidname
					, Начальная_цена: bidpanel.bidtenderinfo.bidprice
					, Размер_задатка: !bidpanel.biddepositinfo || null == bidpanel.biddepositinfo ? '' : bidpanel.biddepositinfo.biddeposit
					, Размер_задатка_ед: (!bidpanel.biddepositinfo || null == bidpanel.biddepositinfo  || 'Yes' == bidpanel.biddepositinfo.isdepositinpercent) ? '%' : 'руб.'
					, Шаг_аукциона: bidpanel.bidtenderinfo.bidauctionsteppercent
					, Шаг_аукциона_ед: '%'
					, Краткое_описание: ''
					, Точный_адрес: ''
					, Порядок_ознакомления: 'Обычный порядок' == bidpanel.biddebtorinfo.bidinventoryresearchtype ? '' : bidpanel.biddebtorinfo.bidinventoryresearchtype
					};
				this.Decode_data_bid_debtorbidname(bidpanel.biddebtorinfo.debtorbidname, Лот);
				Лоты.push(Лот);
			}
		}

		codec.Encode_data = function (data)
		{
			var sber_data = this.Encode_data_without_bids(data);
			this.Encode_data_bids(data, sber_data);
			return sber_data;
		}

		var base_Encode = codec.Encode;
		codec.Encode = function (data)
		{
			var sber_data = this.Encode_data(data);

			var xml_string = base_Encode.call(this, sber_data);
			var decoded_data = this.Decode(xml_string);
			return xml_string;
		}

		codec.Decode_data = function (sber_data)
		{
			var data = {
				Наименование: (null == sber_data || !sber_data.purchaseinfo || null == sber_data.purchaseinfo) ? '' : sber_data.purchaseinfo.purchasename
				, URL: ''
				, Начало_приёма_заявок: {}
				, Окончание_приёма_заявок: {}
				, Начало_торгов: {}
				, Форма_торгов: ''
				, ЭТП: {
					id: 32
					, text: "ЗАО «Сбербанк-АСТ»"
					, url: "utp.sberbank-ast.ru/Bankruptcy/"
					, operator: "ЗАО «Сбербанк-АСТ»"
					, uploadurl: "http://trust.dev/ama-biddings-transit/to-etp.php?etp=sberbank-ast&test-mode=on&bidding_token="
					, format: "sberbank-ast"
				}
				, Объявление:
					{
						ЕФРСБ: {}
						, ФедеральноеСМИ: { дата: '', номер_тиража: '' }
						, МестноеСМИ: { Наименование: '', дата: '', номер_тиража: '' }
					}
				, Процедура:
					{
						Должник: {}
						, Арбитражный_управляющий: {}
					}
				, Лоты: []
			};
			this.Decode_requestinfo(sber_data, data);
			this.Decode_terms(sber_data, data);
			this.Decode_purchaseinfo(sber_data, data);
			this.Decode_data_bids(sber_data, data);
			this.Decode_businesinfo(sber_data, data);
			this.Decode_crisicmanagerinfo(sber_data, data);
			this.Decode_debtorinfo(sber_data, data);
			return data;
		}

		var base_Decode = codec.Decode;
		codec.Decode = function (txt)
		{
			var sber_data = base_Decode.call(this, txt);
			return this.Decode_data(sber_data);
		}

        codec.checkStringErrors_sber = function (cfbdata) {

            var stringErrors = '';

            var errorMess1 = '<p>Шаг торгов необходимо указывать в процентах. </p>';
            var errorMess2 = '<p>Шаг торгов должен быть от 5% до 10% от начальной цены. </p>';
            var errorMess3 = '<p>Размер задатка должен быть до 20% от начальной цены. </p>';

            for (var i = 0; i < cfbdata.Лоты.length; i++) {
                var Лот = cfbdata.Лоты[i];

                if ((Лот.Шаг_аукциона_ед != '%') && (stringErrors.indexOf(errorMess1) == '-1')) {
                    stringErrors += errorMess1;
                }
                if (Лот.Шаг_аукциона_ед == '%') {
                    if (((Лот.Шаг_аукциона < 5) || (Лот.Шаг_аукциона > 10)) && (stringErrors.indexOf(errorMess2) == '-1')) {
                        stringErrors += errorMess2;
                    }
                }
                if ((Лот.Размер_задатка_ед == '%') && (Лот.Размер_задатка > 20) && (stringErrors.indexOf(errorMess3) == '-1')) stringErrors += errorMess3;
            }

            if (stringErrors != '') {
                return stringErrors;
            }
            return null;
        }

        log.Debug('Create }');
        return codec;
    }
});