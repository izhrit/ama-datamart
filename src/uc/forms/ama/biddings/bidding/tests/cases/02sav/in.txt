include ..\..\..\..\bidding\tests\cases\in.lib.txt quiet
include ..\..\..\..\lot\tests\cases\in.lib.txt quiet

wait_text "Лоты"
shot_check_png ..\..\shots\0new.png

click_text "Начало приёма заявок"
exit
play_stored_lines bidding_fields_1
click_text "Сохранить параметры торгов"

click_text "Добавить"
je $("#cpw-ama-bidding-modal-kmobjects").dialog('close');
play_stored_lines lot_fields_1
click_text "Сохранить параметры лота"

click_text "Добавить"
je $("#cpw-ama-bidding-modal-kmobjects").dialog('close');
play_stored_lines lot_fields_2
click_text "Сохранить параметры лота"

exit