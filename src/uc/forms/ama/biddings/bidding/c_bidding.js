define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/biddings/bidding/e_bidding.html'
	, 'forms/ama/biddings/lots/c_lots'
	, 'tpl!forms/ama/biddings/bidding/v_bidding_head.html'
	, 'txt!forms/ama/biddings/bidding/s_bidding_head.css'
	, 'forms/ama/biddings/bidding_params/c_bidding_params'
	, 'forms/base/h_msgbox'

	, 'forms/base/h_validation_msg'
	, 'tpl!forms/ama/biddings/bidding/v_bidding_proposal.html'
	, 'tpl!forms/ama/biddings/bidding/v_deposit_contract.html'
	, 'forms/ama/biddings/toetp/c_toetp'

	, 'forms/ama/biddings/base/codec.cfb'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/h_times'
	, 'forms/base/log'
	, 'forms/ama/biddings/bidding/x_bidding_sber_PurchaseCreate'
],
function (c_fastened, tpl, c_lots, head_tpl, head_css, c_bidding_params, h_msgbox,
	h_validation_msg, v_bidding_proposal, v_deposit_contract, c_toetp,
	codec_cfb, codec_copy, h_times, GetLogger, x_bidding_sber_PurchaseCreate)
{
	return function(options)
	{
		var log= GetLogger('c_bidding_main');

		var controller= c_fastened(tpl, {
			field_spec: { Лоты: { controller: c_lots } }
			, head_tpl: function (obj) { return '<style>' + head_css + '</style>' + head_tpl(obj); }
		});

		controller.base_transit_url = options && options.base_transit_url
			? options.base_transit_url
			//: 'http://trust.dev/ama-biddings-transit/';
			: 'https://bankrot.me/bidding-transit/';

		var test_external_data =
			{
				Должник:
					{
						Наименование: "ООО Роги и ноги"
						, ИНН: "6449013711"
						, ОГРН: "1026402000657"
						, Адрес: "УР, г. Ижевск, ул. Кирова, 172, блок 14"
					}
				, Арбитражный_управляющий:
					{
						Фамилия: "Парамонов"
						, Имя: "Парамон"
						, Отчество: "Парамонович"
						, ИНН: "5460000016"
						, ИОФамилия: "П. П. Парамонов"
						, Моб_телефон: "89091232343"
						, ФИО_род: "Парамонова Парамона Парамоновича"
						, Должность: "Конкурсный управляющий"
						, Должность_род: "конкурсного управляющего"
						, Должность_твор: "конкурсным управляющим"
						, Город: "г. _________"
						, Почтовый_адрес: "УР, г. _________"
					}
				, СРО: "НП СРО АУ Левое"
				, АС: "Левый арбитражный суд Тофаларской автономной области"
				, АС_род: "Левого арбитражного суда Тофаларской автономной области"
				, Номер_дела_о_банкротстве: "234534345"
				, Текущая_дата: "«27» декабря 2016г."
				, Дата_решения: "«1» августа 2014г."
				, Тип_процедуры: "конкурсное производство"
				, highlightReplacement: true
			};

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			//this.binding.controls.Лоты.controller.IncrementVersion = function () { self.SafeIncrementVersion(); }

			var sOnEdit = function (e) {e.preventDefault(); self.OnEdit(e);};
			var on_print = function (e) {self.OnPrint(e);}
			var on_toetp = function (e) {self.OnToEtp(e);}
			var on_toefrsb = function (e) { self.OnToEfrsb(e); }

			if ('undefined' != typeof global_external_AMA_GetProcedureData)
			{
				var процедураИнфо = global_external_AMA_GetProcedureData();
				if (процедураИнфо.Процедура.видПроцедуры == "конкурсное производство")
				{
					var ul = document.getElementById("list_name_document");
					var li = document.createElement("li");
					li.appendChild( document.createTextNode("Заявка-договор на публикацию сообщения"));
					li.setAttribute("cmd", "Заявка_договор");
					ul.appendChild(li);
				}
			}

			this.LinkHeadControls= function()
			{
				this.ReClick($(sel + ' div.ama-biddings-bidding-main-head div.clickable'), sOnEdit);

				if (this.model || this.model != null)
				{
					var окончание_прёма_заявок = !this.model.Окончание_приёма_заявок || null == this.model.Окончание_приёма_заявок ? '' : дата_время_dt(this.model.Окончание_приёма_заявок);
					var начало_приема_заявок = !this.model.Начало_приёма_заявок || null == this.model.Начало_приёма_заявок ? '' : дата_время_dt(this.model.Начало_приёма_заявок);
					var начало_торгов = !this.model.Начало_торгов || null == this.model.Начало_торгов ? '' : дата_время_dt(this.model.Начало_торгов);

					$(sel + ' div.ama-biddings-bidding-main-head span.begin_of_order_taking').text(начало_приема_заявок);
					$(sel + ' div.ama-biddings-bidding-main-head span.end_of_participants').text(окончание_прёма_заявок);
					$(sel + ' div.ama-biddings-bidding-main-head span.beginning_of_the_auction').text(начало_торгов);
				}

				if (this.model && this.model.URL != "")
				{
					$( "#bidding_main_url" ).prop( "disabled", false );
				}
				else
				{
					$( "#bidding_main_url" ).prop( "disabled", true );
				}

				$(sel + ' div.ama-biddings-bidding-main-head a.to-etp').button().click(on_toetp);
				$(sel + ' div.ama-biddings-bidding-main-head a.to-efrsb').button().click(on_toefrsb);

				$(sel + " div.ama-biddings-bidding-main-head > div.head li").click(on_print);
				$(sel + ' div.ama-biddings-bidding-main-head a.print').click(on_print)
				.next().button({text: false, icons: {primary: "ui-icon-triangle-1-s"}})
				.click( function() {
					var menu = $(this).parent().next().show().position
					({
						my: "right top",
						at: "right bottom",
						of: this
					});
					$(document).one("click", function() {menu.hide();});
					return false;
				})
				.parent().buttonset()
				.next().hide().menu()
				;
			}
			this.LinkHeadControls();
		}

		controller.SafeIncrementVersion= function()
		{
			if (this.IncrementVersion)
				this.IncrementVersion();
		}

		controller.ReClick = function (item, func)
		{
			item.off('click', func);
			item.click(func);
		}

		controller.OnPrint= function(e)
		{
			e.preventDefault();
			var cmd = $(e.target).attr('cmd');

			var report_title = 'Документ на печать';
			var report_html= '';
			switch (cmd)
			{
				case 'Положение_о_торгах':
					report_html = this.BuildHtmlReport('proposal');
					report_title= 'Положение о торгах';
					break;
				case 'Договор_о_задатке':
					report_html = this.BuildHtmlReport( 'deposit' );
					report_title= 'Договор о задатке';
					break;
				case 'Заявка_договор':
					report_title = 'Заявка-договор на публикацию';
					break;
				default:
					report_html = this.BuildHtmlReport('proposal');
					report_title = 'Положение о торгах';
					break;
			}

			if (true != window.cpw_bidding_demo_print)
			{
				window.external.openBiddingProposalToWord(report_html, report_title);
			}
			else
			{
				if ('' == report_html)
					report_html = 'Документ не доступен в тестовой версии';
				h_msgbox.ShowModal({ html: report_html, width: 1000, height: 600, title: report_title });
			}
		}

		controller.BuildHtmlReport = function( report_name )
		{
			log.Debug('BuildHtmlReport');
			var bidding = ( !this.binding ) ? this.model : this.GetFormContent();

			if ( 'undefined' == typeof global_external_AMA_GetProcedureData )
			{
				bidding.Процедура = test_external_data;
			}
			else
			{
				var процедураИнфо = global_external_AMA_GetProcedureData();
				bidding.Процедура = getProcedureInfo( процедураИнфо );
			}

			log.Debug('BuildHtmlReport apply template..');
			switch ( report_name )
			{
				case 'proposal': return v_bidding_proposal( bidding );
				case 'deposit': return v_deposit_contract( bidding );
			}
		}

		controller.Build_cfb_xml= function(format)
		{
			var res = null;
			h_msgbox.Show( {
				width: 500
				, title: 'Подготовка данных для передачи'
				, html: '<center>Готовим данные для передачи на сайт посредник<br/> ' + options.base_transit_url + '</center>'
			} );
			try
			{
				var model_to_etp = codec_copy().Copy(this.model);
				if ('undefined' == typeof global_external_AMA_GetProcedureData)
				{
					model_to_etp.Процедура = test_external_data;
				}
				else
				{
					var процедураИнфо = global_external_AMA_GetProcedureData();
					model_to_etp.Процедура = getProcedureInfo( процедураИнфо );
				}
				
				switch (format)
				{
					case 'sberbank-ast':
						res= window.cpw_forms_ama_bidding_using_client_sber_format
							? x_bidding_sber_PurchaseCreate().Encode(model_to_etp)
							: codec_cfb().Encode(model_to_etp);
					default:
						res= codec_cfb().Encode(model_to_etp);
				}
				h_msgbox.Close();
			}
			catch (ex)
			{
				h_msgbox.Close();
				var msgtxt = (true === ex.parseException) ? ex.readablemsg : ex.toString();
				h_msgbox.ShowModal({
					  width: 500
					, title: 'Исключительная ситуация при подготовке данных для передачи'
					, html: msgtxt
				});
			}
			return res;
		}

		controller.SetBiddingUrl= function(url)
		{
			this.model.URL = url;

			var sel = this.binding.form_div_selector;
			$(sel + ' a.bidding-page').attr('href',url);
		}

		controller.OnToEfrsb= function(e)
		{
			e.preventDefault();
			if (true != window.cpw_bidding_demo_print)
			{
				window.external.messageToEfrsb(JSON.stringify(this.GetFormContent(), null, '\t'));
			}
			else
			{
				h_msgbox.ShowModal({
					width: 500
					, title: 'Размещение объявления на ЕФРСБ'
					, html: 'Данная функция недоступна в тестовой версии'
				});
			}
		}

		controller.BuildEtpId= function(etp)
		{
			var res = etp.url;

			var iSlash = res.indexOf('/');
			if (-1 != iSlash)
				res = res.substring(0, iSlash);

			var iDot = res.lastIndexOf('.');
			if (-1 != iDot)
				res = res.substring(0, iDot);

			var iDot = res.lastIndexOf('.');
			if (-1 != iDot)
				res = res.substring(iDot+1, res.length);

			return res;
		}

		controller.BuildUploadUrlById= function(id_etp)
		{
			return this.base_transit_url + 'to-etp.php?etp=' + id_etp + '&bidding_token=';
		}

		controller.OnToEtp = function (e)
		{
			e.preventDefault();
			var cmd = $(e.target).attr('cmd');
			var bidding = this.GetFormContent();

			var id_etp = !bidding || !bidding.ЭТП || !bidding.ЭТП.url ? '' : this.BuildEtpId(bidding.ЭТП);

			var self = this;
			var editor = c_toetp({ base_transit_url: this.base_transit_url });
			editor.SetFormContent({
				uploadurl: !bidding || !bidding.ЭТП || !bidding.ЭТП.uploadurl ? this.BuildUploadUrlById(id_etp) : bidding.ЭТП.uploadurl
				, format: id_etp
			});
			editor.Build_cfb_xml = function () { return self.Build_cfb_xml(id_etp); }
			editor.SetBiddingUrl = function (url) { self.SetBiddingUrl(url); }
			editor.OnOk = function (func_after)
			{
				$('#cpw-ama-bidding-to-etp').dialog('close');
				if (func_after && null != func_after)
					func_after();
			}
			h_msgbox.ShowModal({
				controller: editor
				, width: 850
				, height: 472
				, resizable: false
				, title: 'Передача объявления на ЭТП'
				, buttons: ['Отмена']
				, id_div: 'cpw-ama-bidding-to-etp'
			});
		}

		controller.OnEdit = function ()
		{
			var obj = this.binding;
			var editor = c_bidding_params();
			var SaveButtonName = 'Сохранить параметры торгов';

			editor.SetFormContent(obj.model);

			var self = this;
			h_msgbox.ShowModal
				({
					controller: editor
					, title: 'Редактирование параметров торгов'
					, width: 860
					, height: 549
					, resizable: false
					, id_div: 'cpw-ama-bidding-modal-bidding'
					, buttons: [SaveButtonName, 'Отмена']
					, onclose: function (bname, dlg)
					{
						if (SaveButtonName == bname)
						{
							h_validation_msg.IfOkWithValidateResult(editor.Validate(), function ()
							{
								var old_lots = !obj.model ? null : obj.model.Лоты;
								obj.model = editor.GetFormContent();
								self.model = obj.model;
								obj.model.Лоты = old_lots;

								var sel = obj.form_div_selector;
								$(sel + ' div.ama-biddings-bidding-main > div.head').html(head_tpl(obj.model));
								self.LinkHeadControls();
								self.SafeIncrementVersion();
								$(dlg).dialog('close');
							});
							return false;
						}
					}
				});
		}

		function getProcedureInfo( procedureInfo ) {
			var templateProcedureInfo = {
				Должник:
				{
					Наименование: procedureInfo.Процедура["видПроцедуры"] == "реализация имущества" ? procedureInfo.Должник["полноеИмяРодПадеж"] : procedureInfo.Должник["сокрИмя"]
					, ИНН: procedureInfo.Должник["инн"]
					, ОГРН: procedureInfo.Должник["огрн"]
					, Адрес: procedureInfo.Должник["почтАдрес"]
					, Дата_рождения: procedureInfo.Должник["датаРожд"]
					, Регистрация_по_месту_жительства: procedureInfo.Должник.юрАдрес["полныйАдрес"]
					, снилс: procedureInfo.Должник["снилс"]
					, Место_рождения: procedureInfo.Должник["местоРожд"]
				}
					, Арбитражный_управляющий:
					{
						Фамилия: procedureInfo.Управляющий["фамилия"]
						, Имя: procedureInfo.Управляющий["имя"]
						, Отчество: procedureInfo.Управляющий["отчество"]
						, ИНН: procedureInfo.Управляющий["инн"]
						, ИОФамилия: procedureInfo.Управляющий["сокрИмя"]
						, Моб_телефон: procedureInfo.Управляющий["телефон"]
						, ФИО_род: procedureInfo.Управляющий["полноеИмяРодПадеж"]
						, Должность: procedureInfo.Процедура["managerTitle"]
						, Должность_род: procedureInfo.Процедура["managerTitleRodPad"]
						, Должность_твор: procedureInfo.Процедура["managerTitleTvorPad"]
						, Город: procedureInfo.Управляющий.юрАдрес.город["text"]
						, Почтовый_адрес: procedureInfo.Управляющий["почтовыйАдрес"]
						, Почта: procedureInfo.Управляющий["элПочта"]
					}
					, СРО: procedureInfo.Управляющий.сро["имя"]
					, АС: procedureInfo.Процедура["наименованиеСуда"]
					, АС_род: procedureInfo.Процедура["наименованиеСудРодитПадеж"]
					, АС_твор: procedureInfo.Процедура["наименованиеСудТворПадеж"]
					, Номер_дела_о_банкротстве: procedureInfo.Процедура["номерОпределения"]
					, Текущая_дата: procedureInfo["Текущая_дата"]
					, Дата_решения: procedureInfo.Процедура["датаВведенияПроцедуры"]
					, Тип_процедуры: procedureInfo.Процедура["видПроцедуры"]
					, highlightReplacement: true
			};
			return templateProcedureInfo;
		}
		return controller;
	}

	function дата_время_dt(dt)
	{
		return дата_время(dt.дата, dt.время);
	}

	function дата_время(дата, время)
	{
		var dateArray = дата.split( /[:,.]/ );
		var dateTime = "";
		if ( дата != "" && дата != null )
		{
			var month = dateArray[1].replace( /^0/, '' );
			var date = new Date( dateArray[2], dateArray[1] - 1, dateArray[0] );

			dateTime += dateArray[0] + " " + h_times.RodRussianMonthNameByDate( date ) + " " +
			  dateArray[2] + " г. ";
		}
		if ( время != "" && время != null )
		{
			dateTime += "в " + время;
		}
		
		return dateTime;
	}
});