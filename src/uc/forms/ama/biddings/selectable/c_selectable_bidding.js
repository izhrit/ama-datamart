define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/biddings/biddings/e_biddings.html'
	, 'forms/ama/biddings/main/c_bidding_main'
	, 'forms/base/b_collection'
	, 'forms/ama/biddings/lot/c_lot'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
	, 'forms/ama/biddings/bidding/h_bidding'
	, 'forms/base/store_status/c_store_status'
],
function (c_fastened, tpl, c_bidding_main, b_collection, c_lot, h_msgbox, h_validation_msg, h_bidding, c_store_status)
{
	return function (options)
	{
		var controller = c_fastened(tpl, { CreateBinding: b_collection });

		controller.base_transit_url = options && options.base_transit_url
			? options.base_transit_url
			: 'http://trust.dev/ama-biddings-transit/';

		controller.c_store_status = c_store_status();

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.c_store_status.CreateNew(sel + ' div.store-status');

			this.edited_bidding = { index: this.GetIndexOfActiveBidding(), controller: c_bidding_main({ base_transit_url: this.base_transit_url }) };
			this.SafeEditCurrentBidding();

			this.UpdateSelect();

			var self = this;
			self.edited_bidding.controller.IncrementVersion = function() { self.OnChangeBidding(); }
			$(sel + ' select.selector').on('change', function (e) { e.preventDefault(); self.OnSelect(); });
			$(sel + ' .bidding-cmd a.edit').click(function(e) { e.preventDefault(); self.edited_bidding.controller.OnEdit(); })
			$(sel + ' .bidding-cmd a.add').click(function(e) { e.preventDefault(); self.OnAdd(); })
			$(sel + ' .bidding-cmd a.delete').click(function(e) { e.preventDefault(); self.OnDelete(); })

			if ( this.model == null || ''==this.model)
				self.OnAdd();
		}

		controller.GetIndexOfActiveBidding= function()
		{
			if (this.model)
			{
				for (var i= 0; i<this.model.length; i++)
				{
					var bidding = this.model[i];
					if (true == bidding.isActive || 'true' == bidding.isActive)
						return i;
				}
			}
			return 0;
		}

		controller.OnChangeBidding = function ()
		{
			this.UpdateSelect();
			this.OnChange();
		}

		controller.OnChange= function()
		{
			this.c_store_status.OnIncrementRevision();
			this.Save();
		}

		controller.UpdateSelect = function ()
		{
			if (this.model && null != this.model)
			{
				this.model.isActive = true;
				var sel = this.binding.form_div_selector;
				var options = $(sel + ' select.selector option');
				options.addClass('deprecated');
				for (var i = 0; i < this.model.length; i++)
				{
					if (i < options.length)
					{
						var option = $(options[i]);
					}
					else
					{
						var option = $('<option></option>');
						$(sel + ' select.selector').append(option);
					}
					option.attr('value', i);
					option.text(this.model[i].Наименование);
					option.removeClass('deprecated');
					if (i == this.edited_bidding.index)
					{
						option.attr('selected', 'selected');
					}
					else
					{
						option.removeAttr('selected');
					}
				}
				$(sel + ' select.selector option.deprecated').remove();
			}
		}

		controller.SafeEditCurrentBidding = function ()
		{
			if (this.binding.model)
			{
				var sel = this.binding.form_div_selector;
				var controller = this.edited_bidding.controller;
				var bidding = this.binding.model[this.edited_bidding.index];
				controller.SetFormContent(bidding);
				controller.Edit(sel + ' > div.ama-biddings-biddings > div.bidding');
			}
		}

		controller.OnSelect = function ()
		{
			var sel = this.binding.form_div_selector;
			var selected_index = $(sel + ' select.selector').val();
			if (selected_index != this.edited_bidding.index)
			{
				this.edited_bidding.controller.Destroy();
				this.edited_bidding.index = selected_index;
				this.SafeEditCurrentBidding();
				this.OnChange();
			}
		}

		controller.StoreLotDuringAddingOfBidding= function(lot,bidding)
		{
			if (null != bidding)
			{
				h_bidding.AddLotToBiddingOnCreate(bidding, lot);
			}
			else
			{
				bidding = h_bidding.BuildBiddingByLot(lot);
				if (!this.model || null == this.model)
					this.model = this.binding.model = [];
				this.model.push(bidding);
				this.edited_bidding.index = this.model.length - 1;
			}
			this.UpdateSelect();
			this.SafeEditCurrentBidding();
			this.OnChange();
			return bidding;
		}

		controller.OnAdd = function ()
		{
			var lot_editor = c_lot();
			lot_editor.SetFormContent({});

			var bidding = null;
			var self = this;

			var saveToNextLot = 'Сохранить и к следующему';
			var saveToBidding = 'Сохранить и к торгам';
			var id_div = 'cpw-ama-bidding-modal-lot';

			h_msgbox.ShowModal
			({
				controller: lot_editor,
				title: 'Формирование выставляемого на торги лота',
				width: lot_editor.size.width,
				height: lot_editor.size.height,
				id_div: id_div,
				buttons: [saveToNextLot, saveToBidding, 'Отмена'],
				onclose: function (bname)
				{
					switch (bname)
					{
						case saveToNextLot:
						{
							h_validation_msg.IfOkWithValidateResult(lot_editor.Validate(), function ()
							{
								bidding = self.StoreLotDuringAddingOfBidding(lot_editor.GetFormContent(), bidding);
								lot_editor.Destroy();
								lot_editor.SetFormContent({});
								lot_editor.Edit('#' + id_div);
							});
							return false;
						}
						case saveToBidding:
						{
							h_validation_msg.IfOkWithValidateResult(lot_editor.Validate(), function ()
							{
								bidding = self.StoreLotDuringAddingOfBidding(lot_editor.GetFormContent(), bidding);
								$('#' + id_div).dialog('close');
								self.edited_bidding.controller.OnEdit();
							});
							return false;
						}
					}
				}
			});
		}

			controller.OnDelete = function ()
			{
				if (this.model && null != this.model && 0 != this.model.length)
				{
					var bidding = this.binding.model[this.edited_bidding.index]
					var self = this;
					var OkButtonName = 'Да, удалить данные о торгах';
					h_msgbox.ShowModal
					({
						html: '<center>Вы действительно хотите удалить данные о торгах<br/> <b>"' + bidding.Наименование + '"</b> ?</center>'
						, title: 'Подтверждение удаления торгов'
						, width: 500, height: 200
						, buttons: [OkButtonName, 'Отмена']
						, onclose: function (bname, dlg)
						{
							if (OkButtonName == bname)
							{
								self.edited_bidding.controller.Destroy();
								self.model.splice(self.edited_bidding.index, 1);

								self.UpdateSelect();

								if (self.edited_bidding.index >= self.model.length)
									self.edited_bidding.index--;

								var sel = self.binding.form_div_selector;
								if (0 == self.model.length)
								{
									$(sel + ' div.bidding').html('');
								}
								else
								{
									self.SafeEditCurrentBidding();
								}

								self.OnChange();
							}
						}
					});
				}
			}

			var base_GetFormContent = controller.GetFormContent;
			controller.GetFormContent= function()
			{
				this.c_store_status.OnGetFormContent();
				var content = base_GetFormContent.call(this);
				for (var i = 0; i < content.length; i++)
				{
					content[i].isActive = i == this.edited_bidding.index;
				}
				return content;
			}

			var base_SetFormContent = controller.SetFormContent;
			controller.SetFormContent = function (content)
			{
				this.c_store_status.OnSetFormContent();
				base_SetFormContent.call(this, content);
			}

			controller.Save = function ()
			{
				var content = this.GetFormContent();
				if (global_external_AMA_SaveBiddings && 'undefined' != typeof global_external_AMA_SaveBiddings)
					global_external_AMA_SaveBiddings(content);
			}

			return controller;
		}
});
