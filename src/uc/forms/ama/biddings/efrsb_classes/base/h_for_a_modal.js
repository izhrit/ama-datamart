define(function ()
{
	var helper = {};

	helper.text= function(classes)
	{
		if (!classes || !classes.length)
		{
			return 'Не выбран ни один класс! Кликните, чтобы указать..'
		}
		else
		{
			var res= [];
			for (var i= 0; i<classes.length; i++)
			{
				res.push(classes[i].name);
			}
			return ' • ' + res.join(';\r\n • ') + ';';
		}
	}

	return helper;
})