define(['forms/ama/biddings/efrsb_classes/choose/c_choose_efrsb_classes'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'choose_efrsb_classes'
		, Title: 'Выбор классов по классификации ЕФРСБ'
	};
	return form_spec;
});
