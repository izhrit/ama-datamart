define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/biddings/efrsb_classes/choose/e_choose_efrsb_classes.html'
	, 'forms/ama/biddings/efrsb_classes/base/h_efrsb_classes'
	, 'tpl!forms/ama/biddings/efrsb_classes/choose/v_choose_efrsb_select2_item.html'
	, 'txt!forms/ama/biddings/efrsb_classes/choose/s_choose_efrsb_classes.css'
	, 'forms/base/codec/codec.copy'
	, 'tpl!forms/ama/biddings/efrsb_classes/choose/v_choose_efrsb_classes_confirm_delete.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_efrsb_classes, v_choose_efrsb_select2_item, css, codec_copy, v_choose_efrsb_classes_confirm_delete, h_msgbox)
{
	return function()
	{
		var class_choosed= function(code,choosed_classes_arr)
		{
			var res= false;
			if (choosed_classes_arr && null!=choosed_classes_arr && choosed_classes_arr.length)
			{
				for (var i= 0; i< choosed_classes_arr.length; i++)
				{
					var codei= choosed_classes_arr[i].code;
					if (0==codei.indexOf(code))
					{
						if (code.length==codei.length)
						{
							return true;
						}
						else
						{
							res= 'subitem';
						}
					}
				}
			}
			return res;
		}

		var options =
		{
			css:css
			,h_efrsb_classes:h_efrsb_classes
			,class_choosed:class_choosed
			,field_spec:
			{
				Выбранный_класс:
				{
					query: function (cb)
					{
						var results= h_efrsb_classes.find_classes_by_prefix(cb.term);
						cb.callback({ results: results });
					}
					, formatResult: function (item) { return v_choose_efrsb_select2_item({item:item,h_efrsb_classes:h_efrsb_classes}) }
					, placeholder:'Выберите класс для добавления'
				}
			}
		};

		var controller= c_fastened(tpl, options);

		controller.size = {width:1015,height:530};

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' [data-fc-selector="Выбранный_класс"]').on("change", function (e) { self.OnAdd(e.added); });

			var $классы= $(sel + ' [data-fc-selector="Классы"]');
			$классы.on('delete', function (e,i) { self.OnDelete(i); });
			$классы.on('class-click', function (e,i) { self.OnChoosedClassClick(i); });

			$(sel + ' input[type="checkbox"]').on("change", function (e) { self.OnInputChange(e); });
			$(sel + ' input.search-tree').on("change", function () { self.OnTreeSearchTextChange(); });
		}

		controller.OnChoosedClassClick= function(i_choosed_class)
		{
			var fastening= this.fastening;
			var sel = fastening.selector;
			var fc_dom_item= $(sel + ' [data-fc-selector="Классы"]');
			var model= fastening.get_fc_model_value(fc_dom_item);
			if (model && null != model && i_choosed_class<model.length)
			{
				var cls= model[i_choosed_class];
				var $leaf= $(sel + ' div.header[data-code="' + cls.code + '"]');
				$leaf.parents('div.to-switch').addClass('open');
				if (0<$leaf.length)
					$leaf[0].scrollIntoView();
			}
		}

		controller.OnTreeSearchTextChange= function()
		{
			var fastening= this.fastening;
			var sel = fastening.selector;

			var txt= ($(sel + ' input.search-tree').val() + '').toLowerCase();
			if (''==txt)
				txt= 'не может быть такого текста';
			var first_code_to_highlight= null;
			for (var i= 0; i< h_efrsb_classes.classes_arr_arr.length; i++)
			{
				var arr = h_efrsb_classes.classes_arr_arr[i];
				var code= arr[0];
				var class_text= arr[1].toLowerCase();
				var highlight= (-1!=class_text.indexOf(txt));
				var attr_value= !highlight ? null : 'true';
				$(sel + ' div.header[data-code="' + code + '"]').attr('data-highlight',attr_value);
				if (highlight)
				{
					if (null==first_code_to_highlight)
						first_code_to_highlight= code;
					var $leaf= $(sel + ' div.header[data-code="' + code + '"]');
					$leaf.parents('div.to-switch').addClass('open');
				}
			}
			if (null!=first_code_to_highlight)
			{
				var $leaf= $(sel + ' div.header[data-code="' + first_code_to_highlight + '"]');
				if (0<$leaf.length)
					$leaf[0].scrollIntoView();
			}
		}

		controller.OnInputChange= function(e)
		{
			var $input= $(e.target);
			var checked= $input.attr('checked');
			var name= $input.attr('name');
			var code= name.substring(5);
			var item= h_efrsb_classes.get_class_by_id(code);
			if ('checked'==checked)
			{
				this.Add({code:item.id,name:item.text});
			}
			else
			{
				var fastening= this.fastening;
				var sel = fastening.selector;
				var fc_dom_item= $(sel + ' [data-fc-selector="Классы"]');
				var model= fastening.get_fc_model_value(fc_dom_item);
				if (model && null != model)
				{
					for (var i= 0; i<model.length; i++)
					{
						var cls= model[i];
						if (code==cls.code)
						{
							this.OnDelete(i,/*on_cancel=*/function () {
								$input.attr('checked','checked');
							});
							break;
						}
					}
				}
			}
		}

		var find_class= function(arr,code)
		{
			for (var i= 0; i<arr.length; i++)
			{
				var cls= arr[i];
				if (code==cls.code)
					return cls;
			}
			return null;
		}

		controller.OnChange= function()
		{
			var fastening= this.fastening;
			var sel = fastening.selector;
			$(sel + ' div.header').attr('data-choosed','');
			$(sel + ' div.header input').attr('checked',null);

			var fc_dom_item= $(sel + ' [data-fc-selector="Классы"]');
			var choosed_classes_arr= fastening.get_fc_model_value(fc_dom_item);
			for (var i= 0; i<choosed_classes_arr.length; i++)
			{
				var codei= choosed_classes_arr[i].code;
				$(sel + ' div.header[data-code="' + codei + '"]').attr('data-choosed','true');
				$(sel + ' div.header[data-code="' + codei + '"] input').attr('checked','checked');
				if (codei.length>2)
				{
					var codei_cl1= codei.substring(0,2);
					$(sel + ' div.header[data-code="' + codei_cl1 + '"]').attr('data-choosed','sub-item');
					if (codei.length > 4)
					{
						var codei_cl2= codei.substring(0,4);
						$(sel + ' div.header[data-code="' + codei_cl2 + '"]').attr('data-choosed','sub-item');
					}
				}
			}
		}

		controller.Add= function(cls)
		{
			var fastening= this.fastening;
			var sel = fastening.selector;
			$(sel + ' [data-fc-selector="Выбранный_класс"]').select2('data', null);

			var fc_dom_item= $(sel + ' [data-fc-selector="Классы"]');
			var model= fastening.get_fc_model_value(fc_dom_item);
			if (!model || null==model)
				model= [];
			if (null==find_class(model,cls.code))
			{
				model.push(cls);
				fastening.set_fc_model_value(fc_dom_item, model);
				this.OnChange();
				$(sel).trigger('model_change');
			}
		}

		controller.OnAdd= function(item)
		{
			if (item)
				this.Add({code:item.id,name:item.text});
		}

		controller.OnDelete= function(i_item_to_delete, on_cancel)
		{
			var fastening= this.fastening;
			var sel = fastening.selector;

			var fc_dom_item= $(sel + ' [data-fc-selector="Классы"]');
			var model= fastening.get_fc_model_value(fc_dom_item);
			if (model && null!=model)
			{
				var self= this;
				var to_delete = codec_copy().Copy(model[i_item_to_delete]);
				var btnOK= 'Да, удалить класс из списка';
				h_msgbox.ShowModal
				({
					title: 'Подтверждение удаления класса из списка'
					, html: v_choose_efrsb_classes_confirm_delete(to_delete)
					, buttons: [btnOK,'Отмена'], width: 700
					, id_div: "cpw-form-ama-datamart-msg-delete-efrsb-class-from-list"
					, onclose: function (btn)
					{
						if (btnOK != btn)
						{
							if (on_cancel)
								on_cancel();
						}
						else
						{
							model.splice(i_item_to_delete, 1);
							fastening.set_fc_model_value(fc_dom_item, model);
							self.OnChange();
							$(sel).trigger('model_change');
						}
					}
				});
			}
		}

		controller.UseCodec({
			Encode: function (data) { return data.Классы; }
			, Decode: function (data)
			{
				if ('string'==typeof data)
					data= JSON.parse(data);
				return {Классы:data};
			}
		});

		return controller;
	}
});