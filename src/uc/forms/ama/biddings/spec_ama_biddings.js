define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var biddings_specs = {

		controller: {
			  "kmobjects": { path: 'forms/ama/biddings/kmobjects/c_kmobjects' }
			, "lot": { path: 'forms/ama/biddings/lot/c_lot' }
			, "lots": { path: 'forms/ama/biddings/lots/c_lots' }
			, "bidding_params": { path: 'forms/ama/biddings/bidding_params/c_bidding_params' }
			, "toetp": { path: 'forms/ama/biddings/toetp/c_toetp' }
			, "bidding": { path: 'forms/ama/biddings/bidding/c_bidding' }
			, "choose-efrsb-classes": { path: 'forms/ama/biddings/efrsb_classes/choose/c_choose_efrsb_classes' }
			, "prop-classes": { path: 'forms/ama/biddings/prop_classes/c_prop_classes' }
			, "prop-recipients": { path: 'forms/ama/biddings/recipients/c_recipients' }
			, "prop-recipients2": { path: 'forms/ama/biddings/recipients/tests/options/c_recipients2' }
			, "prop-recipients10": { path: 'forms/ama/biddings/recipients/tests/options/c_recipients10' }
			/*, "selectable_bidding": { path: 'forms/ama/biddings/selectable/c_selectable_bidding' }*/
		}

		, content: {
			  "kmobjects-full": { path: 'txt!forms/ama/biddings/kmobjects/tests/contents/full.json.etalon.txt' }
			, "kmobjects-02sav": { path: 'txt!forms/ama/biddings/kmobjects/tests/contents/02sav.json.etalon.txt' }

			, "lot_01sav": { path: 'txt!forms/ama/biddings/lot/tests/contents/01sav.json.etalon.txt' }

			, "lots1": { path: 'txt!forms/ama/biddings/lots/tests/contents/lots1.json.txt' }
			, "lots3": { path: 'txt!forms/ama/biddings/lots/tests/contents/lots3.json.txt' }
			, "lots_02sav": { path: 'txt!forms/ama/biddings/lots/tests/contents/02sav.json.etalon.txt' }

			, "bidding_params_01sav": { path: 'txt!forms/ama/biddings/bidding_params/tests/contents/01sav.json.etalon.txt' }

			, "bidding1": { path: 'txt!forms/ama/biddings/bidding/tests/contents/bidding1.json.txt' }

			, "efrsb-classes-auto-moto": { path: 'txt!forms/ama/biddings/efrsb_classes/choose/tests/contents/auto-moto.json.etalon.txt' }
			, "efrsb-classes-long": { path: 'txt!forms/ama/biddings/efrsb_classes/choose/tests/contents/long.json.etalon.txt' }

			, "prop-classes-auto-moto": { path: 'txt!forms/ama/biddings/prop_classes/tests/contents/auto-moto.json.etalon.txt' }
		}

	};

	return h_spec.combine(biddings_specs);
});