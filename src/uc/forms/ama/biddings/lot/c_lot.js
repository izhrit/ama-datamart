define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/biddings/lot/e_lot.html'
	, 'forms/ama/biddings/kmobjects/c_kmobjects'
	, 'txt!forms/ama/biddings/lot/s_lot.css'
],
function (c_fastened, tpl, c_kmobjects, css)
{
	return function()
	{
		var options = {
			css: css
			,field_spec:
			{
				Объект_конкурсной_массы: {
					text: function (obj) { return 'Кликните, чтобы выбрать соответствующие объекты конкурсной массы'; }
					,controller: c_kmobjects
					,title:'Объекты конкурсной массы выставляемые на торги единым лотом'
				}
			}
		};

		var controller = c_fastened(tpl, options);

		controller.size = { width: 710, height: 520 };

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			$(sel + ' input[data-fc-selector="Наименование"]').focus();

			var self = this;
			$(sel + ' a.related-objects').click(function (e) { e.preventDefault(); self.OnRelations() });

			if (!this.model || null == this.model)
				this.model = {}
			if (!this.model.Наименование || null == this.model.Наименование || '' == this.model.Наименование)
				$(sel + ' a[data-fc-selector="Объект_конкурсной_массы"]').click();
		}

		return controller;
	}
});
