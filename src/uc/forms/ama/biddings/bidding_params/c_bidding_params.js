define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/biddings/bidding_params/e_bidding_params.html'
	, 'txt!forms/ama/biddings/bidding_params/s_bidding_params.css'
],
function (c_fastened, tpl, css)
{
	return function(options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/external');

		var options=
		{
			css: css
			,field_spec:
			{
				ЭТП: 
				{
					ajax: {
						dataType: 'json'
						, url: base_url + '?action=etp.select2'
					}
					, placeholder: 'Выбрать электронную торговую площадку'
				}
			}
		};
		var controller= c_fastened(tpl, options);

		return controller;
	}
});
