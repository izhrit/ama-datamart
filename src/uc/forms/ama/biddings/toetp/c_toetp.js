define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/biddings/toetp/e_toetp.html'
	, 'txt!forms/ama/biddings/toetp/s_toetp.css'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, css, h_msgbox)
{
	return function(options)
	{
		var controller = c_fastened(tpl, {css:css});

		controller.base_transit_url = options && options.base_transit_url 
			? options.base_transit_url
			//: 'http://trust.dev/ama-biddings-transit/';
			: 'https://bankrot.me/bidding-transit/';

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.upload').click(function (e) { e.preventDefault(); self.OnUpload(); })
			$(sel + ' button.download').click(function (e) { e.preventDefault(); self.OnDownload(); })

			var download_action = self.base_transit_url + 'transit.php?action=ping';
			$(sel + ' form.download').attr('action', download_action);
		}

		controller.OnUpload= function()
		{
			var cfb_data = this.Build_cfb_xml();
			if ( null == cfb_data )
				return;

			h_msgbox.Show({
				width: 500
				, title: 'Сохранение данных на сайте посреднике'
				, html: '<center>Сохраняем данные о торгах на сайте посреднике ' + options.base_transit_url + '</center>'
			});

			var sel = this.binding.form_div_selector;
			var etpurl = $(sel + ' input.url').val();
			var self = this;

			var ajaxurl = self.base_transit_url + 'transit.php?action=upload';
			$.ajax
			({
				url: ajaxurl
				, dataType: "json"
				, type: 'POST'
				, data: { cfb_data: cfb_data, etp: this.model.format }
				, cache: false
				, error: function (data, textStatus)
				{
					h_msgbox.Close();
					h_msgbox.ShowAjaxError('Отправка данных на транзитный сервер', ajaxurl, data, textStatus);
				}
				, success: function (data, textStatus)
				{
					h_msgbox.Close();
					if (null == data || !data.token_bidding)
					{
						h_msgbox.ShowAjaxError('Отправка данных на транзитный сервер', ajaxurl, data, textStatus);
					}
					else
					{
						self.OnUploadToken(data.token_bidding, etpurl);
					}
				}
			});
		}

		controller.OnUploadToken = function (token, etpurl)
		{
			var self = this;
			self.SetBiddingUrl(self.base_transit_url + 'transit.php?action=redirect&token=' + token);

			self.OnOk(function ()
			{
				window.open(etpurl + token, 'etp_from_ama',
					'width=1024,height=768,menubar=yes,toolbar=yes,location=yes,status=yes,resizable=yes,scrollbars=yes');
			});
		}

		controller.OnDownload = function ()
		{
			var data_to_etp = this.Build_cfb_xml();
			var sel = this.binding.form_div_selector;
			$(sel + ' input[name="cfb_data"]').val(data_to_etp);
			$(sel + ' form').submit();
			this.OnOk();
		}

		return controller;
	}
});
