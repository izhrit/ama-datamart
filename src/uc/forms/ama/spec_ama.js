define([
	  'forms/base/h_spec'
	, 'forms/ama/award/spec_award'
	, 'forms/ama/cabinetcc/spec_ama_cabinetcc'
	, 'forms/ama/datamart/spec_ama_dm'
	, 'forms/ama/application/spec_application'
	, 'forms/ama/biddings/spec_ama_biddings'
]
, function (h_spec, award_spec, cabinetcc_spec, datamart_spec, spec_application, spec_ama_biddings)
{
	var datamart_specs = {

		controller: {
			"dm_login": {
				path: 'forms/ama/datamart/login/main/c_login'
				, title: 'вход на витрину'
			}
			,"dm_admin_login": {
				path: 'forms/ama/datamart/login/admin/c_admin_login'
				, title: 'административный вход на витрину'
			}
			, "dm_login_form": {
				path: 'forms/ama/datamart/login/form/c_login_form'
				, title: 'заготовка (закладка) входа'
			}
			, "datamart": {
				path: 'forms/ama/datamart/main/c_datamart'
				, title: 'витрина'
			}
			, "datamart_testdb": {
				path: 'forms/ama/datamart/main_testdb/c_datamart_testdb'
				, title: 'витрина с тестовой базой'
			}
			, "application": {
				path: 'forms/ama/application/main/c_application'
				, title: 'заявления РТК'
			}
		}

	};

	return h_spec.combine(award_spec, cabinetcc_spec, datamart_spec, datamart_specs, spec_application, spec_ama_biddings);
});