define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var bfl_specs = {

		controller: {
			"aview": {
				path: 'forms/bfl/view/c_bfl_anketa_view'
				, title: 'Анкета физ лица для заявления о банкротстве'
			}
		}

		, content: {
			"anketanp-example1": {
				path: 'txt!forms/bfl/view/tests/contents/anketanp-example.xml'
				, title: 'Пример анкеты физ лица для заявления о банкротстве'
			}
		}
	};

	return h_spec.combine(bfl_specs);
});