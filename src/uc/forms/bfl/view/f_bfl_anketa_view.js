define(['forms/bfl/view/c_bfl_anketa_view'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'anketa_view'
		, Title: 'Анкета для банкротства гражданина'
	};
	return form_spec;
});
