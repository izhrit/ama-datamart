﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/bfl/view/e_bfl_anketa_view.html'
	, 'forms/base/codec/xml/codec.xml'
],
function (c_fastened, tpl, codec_xml)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		controller.UseCodec(codec_xml());

		return controller;
	}
});