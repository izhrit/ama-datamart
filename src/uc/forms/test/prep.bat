@pushd %~dp0
@call %~dp0..\..\scripts\tests.bat encode_module %~dp0..\..\ forms/base/codec/test/codec.index forms/test/spec_test %~dp0\index.js
@call %~dp0..\..\scripts\tests.bat encode_module %~dp0..\..\ forms/base/codec/test/codec.contents forms/test/spec_test %~dp0\contents.js
@popd
exit /B