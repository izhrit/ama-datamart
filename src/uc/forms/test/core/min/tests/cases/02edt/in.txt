include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
shot_check_png ..\..\shots\01sav.png
type_id test-test-form-edit 3
shot_check_png ..\..\shots\02edt.png
wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02edt.result.txt
wait_click_full_text "Редактировать модель в элементе управления"
shot_check_png ..\..\shots\02edt.png
exit