define(['forms/base/controller'], function (BaseFormController)
{
	return function ()
	{
		var edit_id_spec = '#test-test-form-edit';
		var controller = BaseFormController();

		var RenderForm = function (sel, value)
		{
			var form_div = $(sel);
			var style= 'border-color:green;border-style:solid;border-width:1px;padding:5px;margin:5px;width:520px;display:inling-block';
			var content = '<div style="' + style + '">'
			content += '<span>тестовая форма с одним текстовым полем:</span>';
			content += '<input tyle="edit" id="test-test-form-edit" value="' + value + '"/>';
			content += '</div>';
			form_div.append(content);
			$(edit_id_spec).focus();
		}

		controller.CreateNew = function (sel) { RenderForm(sel, ""); };

		controller.Edit = function (sel) { RenderForm(sel, this.form_content); };

		controller.GetFormContent = function () { return $(edit_id_spec).val(); };

		controller.SetFormContent = function (form_content)
		{
			var isnum = /^\d+$/.test(form_content);
			if (!isnum)
			{
				return "Документ должен содержать только цифры!";
			}
			else
			{
				this.form_content = form_content;
				return null;
			}
		};

		controller.Validate = function ()
		{
			var form_content = $(edit_id_spec).val();
			var isnum = /^\d+$/.test(form_content);
			return isnum ? null : "Документ должен содержать только цифры!";
		};

		return controller;
	}
});
