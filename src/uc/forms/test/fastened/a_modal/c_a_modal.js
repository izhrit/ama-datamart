﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/fastened/a_modal/e_a_modal.html'
	, 'forms/test/fastened/simple/c_simple'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, c_simple, h_msgbox)
{
	return function ()
	{
		var options= {
			field_spec:
			{
				Подшефный:
				{
					  controller: c_simple
					, title: 'Редактирование подшефного'
					, width: 400, height:260
					, text: function(п)
					{
						return !п || null==п 
						? 'Кликните, чтобы указать данные..'
						: п.Ученик + '(алгебра:' + п.Оценки.алгебра + ', пение:' + п.Оценки.пение + ')';
					}
				}
			}
		};
		var controller = c_fastened(tpl, options);
		return controller;
	}
});