include ..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\simple\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Подшефный"
shot_check_png ..\..\shots\00new.png

wait_click_text "Кликните"
play_stored_lines simple_fields_1
shot_check_png ..\..\shots\01sav_modal.png
wait_click_full_text "Сохранить"

shot_check_png ..\..\shots\01sav.png

wait_click_text "Иванов"
shot_check_png ..\..\shots\01sav_modal.png
wait_click_text "Отмена"

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01sav.json.result.txt
exit