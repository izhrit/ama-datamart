﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/complex/view/e_test_complex_view.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.add.star').click(function (e) { e.preventDefault(); self.OnAddStar(e); });
		}

		controller.OnAddStar = function (e)
		{
			console.log(1);
			var fc_dom_item = $(e.target);
			console.log(fc_dom_item);
			console.log(2);
			var model = this.fastening.get_fc_model_value(fc_dom_item);
			console.log(model);
			console.log(3);
			model.push({});
			console.log(4);
			console.log(model);
			this.fastening.set_fc_model_value(fc_dom_item, model);
			console.log(5);
		}

		return controller;
	}
});