﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mpl/asset/classification/e_mpl_asset_classification.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		return controller;
	}
});