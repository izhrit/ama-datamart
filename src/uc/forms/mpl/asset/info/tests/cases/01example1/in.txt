wait_text "зимняя резина"
shot_check_png ..\..\shots\01example1.png
wait_text "b.gif"
wait_text "a.jpg"

shot_hide_class asset-img
shot_hide_class asset-img-preview

jw 1==$('a:contains("a.jpg")').length
wait_click_text "a.jpg"

wait_text "Изображение к объекту"
shot_check_png ..\..\shots\01example1_img1.png
wait_click_text "Close"
wait_text_disappeared "Изображение к объекту"

jw 1==$('a:contains("b.gif")').length
wait_click_text "b.gif"

wait_text "Изображение к объекту"
shot_check_png ..\..\shots\01example1_img2.png
wait_click_text "Close"
wait_text_disappeared "Изображение к объекту"

exit