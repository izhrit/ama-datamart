﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mpl/asset/info/e_mpl_asset_info.html'
	, 'forms/mpl/asset/img/c_mpl_asset_img'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, c_mpl_asset_img, h_msgbox)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/marketplace');

		var options = {
			attachment_url_prefix: base_url + '?action=asset.attachment&id_AssetAttachment='
		};

		var controller = c_fastened(tpl, options);

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' a.asset-img-app').click(function (e) { self.OnImagePreview(e); });
		}

		controller.OnImagePreview= function(e)
		{
			var $a= $(e.target);
			if (!$a.hasClass('asset-img-app'))
				$a= $a.parent('.asset-img-app');
			var id_assetattachment= $a.attr('data-id_assetattachment');

			var img= null;
			for (var i= 0; i<this.model.Приложения.length; i++)
			{
				var a= this.model.Приложения[i];
				if (id_assetattachment==a.id_AssetAttachment)
				{
					img= a;
					break;
				}
			}

			var c_img= c_mpl_asset_img({ base_url: base_url });
			c_img.SetFormContent(img);
			h_msgbox.ShowModal
			({
				title: 'Изображение к объекту конкурсной массы на экспозиции'
				, controller: c_img
				, buttons: ['Закрыть']
				, id_div: "cpw-form-ama-marketplace-img-form"
			});
		}

		return controller;
	}
});