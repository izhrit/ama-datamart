﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mpl/asset/log/e_mpl_asset_log.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		return controller;
	}
});