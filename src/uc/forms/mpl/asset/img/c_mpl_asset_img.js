﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mpl/asset/img/e_mpl_asset_img.html'
],
function (c_fastened, tpl)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/marketplace');

		var options = {
			attachment_url_prefix: base_url + '?action=asset.attachment&id_AssetAttachment='
		};

		var controller = c_fastened(tpl, options);

		controller.size = {width:800,height:600};

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' button.download').click(function () { self.OnDownloadImage(); });
		}

		controller.OnDownloadImage= function()
		{
			console.log('OnDownloadImage');
		}

		return controller;
	}
});