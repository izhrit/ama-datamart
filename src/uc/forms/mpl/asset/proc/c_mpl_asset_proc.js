﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mpl/asset/proc/e_mpl_asset_proc.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		return controller;
	}
});