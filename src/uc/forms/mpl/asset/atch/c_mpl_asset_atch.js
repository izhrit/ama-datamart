﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mpl/asset/atch/e_mpl_asset_atch.html'
],
function (c_fastened, tpl)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/marketplace');

		var options = {
			attachment_url_prefix: base_url + '?action=asset.attachment&id_AssetAttachment='
		};

		var controller = c_fastened(tpl, options);

		return controller;
	}
});