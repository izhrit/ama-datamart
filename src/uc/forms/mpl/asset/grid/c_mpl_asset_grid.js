﻿define([
	  'forms/ama/datamart/base/c_adapted_grid'
	, 'tpl!forms/mpl/asset/grid/e_mpl_asset_grid.html'
	, 'forms/base/h_msgbox'
	, 'forms/mpl/filter/c_mpl_filter'
	, 'forms/mpl/filter/h_mpl_filter'
	, 'forms/mpl/asset/item/c_mpl_asset_item'
	, 'forms/mpl/base/h_asset_state'
],
function (c_fastened, tpl, h_msgbox, c_mpl_filter, h_mpl_filter, c_mpl_asset_item, h_asset_state)
{
	return function (options_arg)
	{
		var base_url= ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/marketplace');

		var options = {
			field_spec:
				{
					'Фильтр':
					{
						controller: function () { return c_mpl_filter({ base_url: base_url }); }
						,text:h_mpl_filter.text
					}
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
			var self = this;
			this.AddButtonToPagerLeft('cpw-marketplace-assets-pager', 'Обновить', function () { self.ReloadGrid(); });
			$(sel + ' [data-fc-selector="Фильтр"]').on('change', function () { self.ReloadGrid(); });
		}

		controller.base_url = base_url;
		controller.base_grid_url = controller.base_url + '?action=asset.jqgrid';
		controller.base_r_url = controller.base_url + '?action=asset.r';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			return url;
		}

		controller.PrepareFilterField= function(fieldname)
		{
			var self= this;
			return function()
			{
				var model= self.GetFormContent();
				if (!model.Фильтр)
				{
					return null;
				}
				else
				{
					var value= model.Фильтр[fieldname];
					if (null==value)
					{
						return null;
					}
					else if ('string'==typeof value)
					{
						return value;
					}
					else
					{
						var items= [];
						for (var i= 0; i<value.length; i++)
						{
							var v= value[i];
							var item= v.id ? v.id : v.code;
							items.push(item);
						}
						return items.join('|');
					}
				}
			}
		}

		controller.PreparePostData= function()
		{
			return { 
				Слова: this.PrepareFilterField('Слова')
				, Регион: this.PrepareFilterField('Регион')
				, Должник: this.PrepareFilterField('Должник')
				, АУ: this.PrepareFilterField('АУ')
				, КлассификацияЕФРСБ: this.PrepareFilterField('КлассификацияЕФРСБ')
				, Стадия: this.PrepareFilterField('Стадия')
				, Категория: this.PrepareFilterField('Категория')
			};
		}

		var assetStateFormatter = function (cellvalue, options, rowObject)
		{
			var res= h_asset_state.Asset_state_by_code[cellvalue];
			return !res ? '?' : res.title;
		}

		var State_search_values= {'null':''};
		for (var i= 0; i<h_asset_state.Asset_State_spec.length; i++)
		{
			var spec= h_asset_state.Asset_State_spec[i];
			State_search_values[spec.code]= spec.title;
		}

		controller.colModel =
		[
			  { name: 'id_Asset', hidden: true }
			, { label: 'Дата', name:'TimeChanged', width:80, sortable:false, search:false }
			, { label: 'Имущество', name:'Title', width:200, sortable:false }
			, { label: 'Категория', name:'Category', width:100, sortable:false }
			, { label: 'Место нахождения', name:'Address', width:200, sortable:false }
			, { label: 'Стадия', name:'State', formatter: assetStateFormatter, width:100, sortable:false, stype : "select", editoptions:{value:State_search_values} }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = self.PrepareUrl();
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, mtype:'POST'
				, postData: self.PreparePostData()
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показано объектов {1} из {2}'
				, emptyText: 'Нет объектов для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 12
				, pager: '#cpw-marketplace-assets-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true 
				, onSelectRow: function () { self.OnAsset(); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка объектов конкурсной массы", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnAsset = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;
			var ajaxurl = this.base_r_url + '&cmd=get&id=' + rowdata.id_Asset;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных объекта конкурсной массы с сервера", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (asset, textStatus)
				{
					if (null == asset)
					{
						v_ajax.ShowAjaxError(asset, textStatus);
					}
					else
					{
						self.OpenAsset(asset);
					}
				}
			});
		}

		controller.OpenAsset= function(asset)
		{
			var c_asset= c_mpl_asset_item({ base_url: this.base_url });
			c_asset.SetFormContent(asset);
			h_msgbox.ShowModal
			({
				title: 'Информация об объекте конкурсной массы на экспозиции'
				, controller: c_asset
				, buttons: ['Закрыть']
				, id_div: "cpw-form-ama-marketplace-item-form"
			});
		}

		return controller;
	}
});