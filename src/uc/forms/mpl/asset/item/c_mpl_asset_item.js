﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mpl/asset/item/e_mpl_asset_item.html'
	, 'forms/mpl/asset/info/c_mpl_asset_info'
	, 'forms/mpl/asset/classification/c_mpl_asset_classification'
	, 'forms/mpl/asset/proc/c_mpl_asset_proc'
	, 'forms/mpl/asset/log/c_mpl_asset_log'
	, 'forms/mpl/asset/atch/c_mpl_asset_atch'
],
function (c_fastened, tpl, c_mpl_asset_info, c_mpl_asset_classification, c_mpl_asset_proc, c_mpl_asset_log, c_mpl_asset_atch)
{
	return function (options_arg)
	{
		var options = {
			field_spec:
				{
					  Главное: { controller: function () { return c_mpl_asset_info(options_arg); } }
					, Классификация: { controller: c_mpl_asset_classification }
					, Стадия: { controller: c_mpl_asset_log }
					, Детали_процедуры: { controller: c_mpl_asset_proc }
					, Приложения: { controller: function () { return c_mpl_asset_atch(options_arg); } }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.size = {width:900,height:500};

		return controller;
	}
});