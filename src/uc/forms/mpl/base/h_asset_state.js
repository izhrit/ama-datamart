define(function ()
{
	var helper = {};

	helper.Asset_State_spec = [
		  { code: 'a', name:'Инвентаризация', title: 'Инвентаризация'}
		, { code: 'b', name:'Отсутствие', title: 'Зарегистрировано отсутствие' }
		, { code: 'c', name:'Выбыло', title: 'Выбыло из конкурсной массы' }
		, { code: 'd', name:'На_продажу', title: 'Определение условий продажи' }
		, { code: 'e', name:'Без_торгов', title: 'Продажа без торгов' }
		, { code: 'f', name:'Без_торгов_покупается', title: 'Определён покупатель без торгов' }
		, { code: 'g', name:'Продано', title: 'Продано' }
		, { code: 'h', name:'Торги1', title: 'Первые торги' }
		, { code: 'i', name:'Торги1_завершены', title: 'Первые торги завершены' }
		, { code: 'j', name:'Торги1_покупается', title: 'Определён покупатель в первых торгах' }
		, { code: 'k', name:'Торги2', title: 'Повторные торги' }
		, { code: 'l', name:'Торги2_завершены', title: 'Повторные торги завершены' }
		, { code: 'm', name:'Торги2_покупается', title: 'Определён покупатель в повторных торгах' }
		, { code: 'n', name:'Торги3', title: 'Публичное предложение' }
		, { code: 'o', name:'Торги3_завершены', title: 'Публичное предложение завершено' }
		, { code: 'p', name:'Торги3_покупается', title: 'Определён покупатель в публичном предложении' }
	];

	helper.Asset_state_by_code = {};
	for (var i = 0; i < helper.Asset_State_spec.length; i++)
	{
		var spec= helper.Asset_State_spec[i];
		if (helper.Asset_state_by_code[spec.code])
			throw "дублируется код состояния КМ " + spec.code;
		helper.Asset_state_by_code[spec.code]= spec;
	}

	return helper;
})