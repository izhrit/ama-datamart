﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mpl/main/e_mpl_cabinet.html'
	, 'forms/mpl/asset/grid/c_mpl_asset_grid'
],
function (c_fastened, tpl, c_mpl_assets)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'ama/marketplace' : options_arg.base_url;

		var options = {
			field_spec:
				{
					Конкурсная_масса: { controller: function () { return c_mpl_assets({ base_url: base_url }); }, render_on_activate:true }
				}
		};

		var controller = c_fastened(tpl, options);

		return controller;
	}
});