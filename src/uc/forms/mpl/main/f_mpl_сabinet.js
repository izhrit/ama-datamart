define(['forms/mpl/main/c_mpl_cabinet'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'marketplace'
		, Title: 'Экспозиция конкурсной массы'
	};
	return form_spec;
});
