define([
	  'forms/base/codec/codec'
	, 'forms/mpl/base/h_asset_state'
	, 'forms/ama/datamart/tests/constants/h_constants_codec'
],
function (BaseCodec, h_asset_state, h_constants_codec)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.Encode = function (data)
		{
			var res = '<?php\r\n\r\n';

			res += h_constants_codec.PhpCommentsHeader('codec.asset_state.php');

			var Asset_State_spec_byCode = {};
			var Asset_State_spec_byTitle = {};
			var Asset_State_spec_byName = {};
			res += '$Asset_State_spec= array(\r\n';
			for (var i = 0; i < h_asset_state.Asset_State_spec.length; i++)
			{
				var s = h_asset_state.Asset_State_spec[i];

				if (Asset_State_spec_byCode[s.code])
					throw 'ununique code of Asset_State ' + s.code;
				if (Asset_State_spec_byTitle[s.title])
					throw 'ununique title of Asset_State ' + s.title;
				if (Asset_State_spec_byName[s.name])
					throw 'ununique name of Asset_State ' + s.name;

				res += (0 == i) ? '   ' : '  ,';
				res += 'array(';
				res += "'code'=>'" + s.code + "'";
				res += ", 'title'=>'" + s.title + "'";
				res += ", 'name'=>'" + s.name + "'";
				res += ')\r\n';

				Asset_State_spec_byCode[s.code] = s;
				Asset_State_spec_byTitle[s.title] = s;
				Asset_State_spec_byName[s.name] = s;
			}
			res += ');\r\n';

			return res;
		}

		return codec;
	}
});
