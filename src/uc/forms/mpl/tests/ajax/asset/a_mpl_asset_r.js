﻿define([
	'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
]
, function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('ama/marketplace?action=asset.r');

	service.read= function(id)
	{
		var rows= ql.select(function(r){return {
				id_Asset: r.a.id_Asset
				,Краткое_наименование: r.a.Title
				,Развёрнутое_описание: r.a.Description
				,Адрес: r.a.Address
				,Категория: r.g.Title
				, КлассификацияЕФРСБ: ql.select(function (rr) { return {
						Класс:rr.e.Code
						,Наименование:rr.e.Title
					}; })
					.from(db.EfrsbAssetClass,'e')
					.inner_join(db.Asset_EfrsbAssetClass, 'ae', function (rr) { return rr.e.id_EfrsbAssetClass==rr.ae.id_EfrsbAssetClass; })
					.where(function (rr) { return rr.ae.id_Asset==r.a.id_Asset;})
					.exec()
				, Приложения: ql.select(function (rr) { return {
						  id_AssetAttachment: rr.c.id_AssetAttachment
						, FileName:rr.c.FileName
					}; })
					.from(db.AssetAttachment,'c')
					.where(function (rr) { return rr.c.id_Asset==r.a.id_Asset;})
					.exec()

			};})
			.from(db.Asset, 'a')
			.left_join(db.AssetGroup, 'g', function (r) { return r.a.id_AssetGroup==r.g.id_AssetGroup; })
			.where(function (r) { return id == r.a.id_Asset; })
			.exec();
		return rows[0];
	};

	return service;
}
);