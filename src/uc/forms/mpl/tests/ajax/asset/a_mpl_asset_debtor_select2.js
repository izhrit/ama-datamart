﻿define([
	'forms/base/ajax/ajax-select2'
	,'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ql'
]
, function (ajax_select2, db, ql)
{
	var service= ajax_select2('ama/datamart?action=asset.debtor.select2');

	var substringMatcher = function (records) {
		return function findMatches(q, cb) {
			var matches, substrRegex;

			matches = [];

			substrRegex = new RegExp(q, 'i');
			
			$.each(records, function (i, record) {
				if (substrRegex.test(record.text)) {
					matches.push(record);
				}
			});
			cb(matches);
		};
	};

	service.query = function (q, page, args)
	{
		var items= ql.select(function (r) { return {
			id: r.d.id_Debtor
			,text: r.d.Name
		};})
		.from(db.Debtor, 'd')
		.exec();
		var res = []
		substringMatcher(items)(args.q, function (matches) { res = matches });
		return { results: res };
	}

	return service;
});