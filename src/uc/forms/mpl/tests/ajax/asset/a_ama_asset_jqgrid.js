﻿define([
	  'forms/ama/datamart/tests/d_datamart'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
], function (db, ajax_jqGrid, ql)
{
	var service= ajax_jqGrid('ama/datamart?action=asset.jqgrid');

	service.rows_all = function ()
	{
		return ql.select(function (r) { return {
				id_Asset: r.a.id_Asset
				, Title: r.a.Title
				, Category: r.g.Title
				, Address: r.r.Name
				, TimeChanged: r.a.TimeChanged
				, State: r.a.State
			};})
			.from(db.Asset, 'a')
			.left_join(db.AssetGroup, 'g', function (r) { return r.a.id_AssetGroup==r.g.id_AssetGroup;})
			.left_join(db.region, 'r', function (r) { return r.a.id_Region==r.r.id_Region;})
			.exec();
	}

	return service;
});