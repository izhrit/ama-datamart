define
(
	[
		  'forms/base/ajax/ajax-collector'

		, 'forms/mpl/tests/ajax/asset/a_mpl_asset_jqgrid'
		, 'forms/mpl/tests/ajax/asset/a_ama_asset_jqgrid'
		, 'forms/mpl/tests/ajax/asset/a_mpl_asset_r'
		, 'forms/mpl/tests/ajax/asset/a_mpl_asset_manager_select2'
		, 'forms/mpl/tests/ajax/asset/a_mpl_asset_debtor_select2'
		, 'forms/mpl/tests/ajax/asset/a_mpl_asset_category_select2'
	],
	function (ajax_collector)
	{
		return ajax_collector(Array.prototype.slice.call(arguments, 1));
	}
);