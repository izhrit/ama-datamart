define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mpl/filter/e_mpl_filter.html'
	, 'forms/ama/biddings/efrsb_classes/choose/c_choose_efrsb_classes'
	, 'forms/ama/biddings/efrsb_classes/base/h_for_a_modal'
	, 'forms/ama/datamart/base/h_region'
	, 'txt!forms/mpl/filter/s_mpl_filter.css'
	, 'forms/mpl/base/h_asset_state'
],
function (c_fastened, tpl, c_choose_efrsb_classes, efrsb_classes_for_a_modal, h_region, css, h_asset_state)
{
	return function(options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');
		var base_url_manager_select2 = base_url + '?action=asset.manager.select2'
		var base_url_debtor_select2 = base_url + '?action=asset.debtor.select2'
		var base_url_category_select2 = base_url + '?action=asset.category.select2'

		var options =
		{
			css: css
			,regions: h_region
			,asset_state: h_asset_state.Asset_State_spec
			,field_spec:
			{
				КлассификацияЕФРСБ:
				{
					title:'Классификация имущества в соответствии с ЕФРСБ'
					,controller: c_choose_efrsb_classes
					,text:efrsb_classes_for_a_modal.text
					,id_div:'cpw-mpl-asset-filter-form'
				}
				, АУ: { ajax: { url: base_url_manager_select2, dataType: 'json' }, multiple:true }
				, Должник: { ajax: { url: base_url_debtor_select2, dataType: 'json' }, multiple:true }
				, Категория: { ajax: { url: base_url_category_select2, dataType: 'json' }, multiple:true }
			}
		};

		var controller= c_fastened(tpl, options);

		controller.size = {width:800,height:500};

		var Fix_multi_select_value= function(items)
		{
			var arr= [];
			for (var i= 0; i< items.length; i++)
			{
				var r= items[i];
				arr.push({id:r.id,text:r.text});
			}
			return arr;
		}

		var base_GetFormContent= controller.GetFormContent;
		controller.GetFormContent= function()
		{
			var model= base_GetFormContent.call(this);
			model.Регион= Fix_multi_select_value(model.Регион);
			model.Стадия= Fix_multi_select_value(model.Стадия);
			return model;
		}

		return controller;
	}
});