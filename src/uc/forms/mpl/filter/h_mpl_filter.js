define([
	'forms/ama/datamart/base/h_region'
]
,function (h_region)
{
	var helper = {};

	helper.safe_push_multiselect_condition= function(conditions,prefix,items,field_name)
	{
		if (items && null!=items && items.length && 0!=items.length)
		{
			if (!field_name)
				field_name= 'text';
			var arr= [];
			for (var i= 0; i<items.length; i++)
				arr.push(items[i][field_name]);
			conditions.push(prefix + arr.join(' или '));
		}
	}

	helper.text= function(f)
	{
		var conditions= [];
		if (f.Слова && null!=f.Слова && ''!=f.Слова)
			conditions.push('описание содержит: "' + f.Слова + '"');
		helper.safe_push_multiselect_condition(conditions,'регион: ',f.Регион);
		helper.safe_push_multiselect_condition(conditions,'категория: ',f.Категория);
		helper.safe_push_multiselect_condition(conditions,'стадия: ',f.Стадия);
		helper.safe_push_multiselect_condition(conditions,'класс: ',f.КлассификацияЕФРСБ,'name');
		helper.safe_push_multiselect_condition(conditions,'собственник: ',f.Должник);
		helper.safe_push_multiselect_condition(conditions,'арбитражный управляющий: ',f.АУ);
		return 0==conditions.length 
			? 'Вся конкурсная масса:'
			:  conditions.join(', ');
	}

	return helper;
});