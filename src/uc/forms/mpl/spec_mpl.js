define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var marketplace_specs = {

		controller: {
			"filter": {
				path: 'forms/mpl/filter/c_mpl_filter'
				, title: 'Фильтр имущества в marketplace'
			}
			,"main": {
				path: 'forms/mpl/main/c_mpl_cabinet'
				, title: 'Главная форма marketplace'
			}
			,"assets": {
				path: 'forms/mpl/asset/grid/c_mpl_asset_grid'
				, title: 'Таблица объектов конкурсной массы на экспозиции'
			}
			,"asset": {
				path: 'forms/mpl/asset/item/c_mpl_asset_item'
				, title: 'Объект конкурсной массы на экспозиции'
			}
			,"asset_info": {
				path: 'forms/mpl/asset/info/c_mpl_asset_info'
				, title: 'Главная информация об объекте конкурсной массы на экспозиции'
			}
			,"asset_classification": {
				path: 'forms/mpl/asset/classification/c_mpl_asset_classification'
				, title: 'Классификация объекта конкурсной массы на экспозиции'
			}
			,"asset_log": {
				path: 'forms/mpl/asset/log/c_mpl_asset_log'
				, title: 'Стадии экспозиции объекта конкурсной массы'
			}
			,"asset_proc": {
				path: 'forms/mpl/asset/proc/c_mpl_asset_proc'
				, title: 'Детали процедуры экспозиции и продажи объекта конкурсной массы'
			}
		}

		, content: {
			"filter-01sav": {
				path: 'txt!forms/mpl/filter/tests/contents/01sav.json.etalon.txt'
				, title: 'Настройки фильтрации экспонируемого имущества'
			}
			,"asset-example1": {
				path: 'txt!forms/mpl/asset/tests/contents/example.json.txt'
				, title: 'Пример объекта экспонируемого объекта КМ 1'
			}
		}

	};

	return h_spec.combine(marketplace_specs);
});