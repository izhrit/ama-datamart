call :build-datamart
call :build-datamart-admin
call :build-datamart-orpau
call :build-datamart-bfl
call :build-datamart-report
call :build-datamart-debtor
call :build-ama-controls
call :build-marketplace

copy built\ama-datamart.js        ..\srv\web\js\ 
copy built\ama-datamart-admin.js  ..\srv\web\js\ 
copy built\ama-datamart-orpau.js  ..\srv\web\js\ 
copy built\ama-datamart-bfl.js    ..\srv\web\anketanp\js\ 
copy built\ama-datamart-debtor.js ..\srv\web\js\ 
copy built\ama-marketplace.js     ..\srv\web\js\ 
copy css\datamart.css             ..\srv\web\css\ 
copy built\ama-datamart-report.js ..\clnt\test-client-ama\report\js\

exit /B

rem -----------------------------------------------------------
:build-marketplace
call build\build-js.bat ama-marketplace build-marketplace
if NOT 0==%ERRORLEVEL% exit
exit /B

rem -----------------------------------------------------------
:build-ama-controls
call build\build-js.bat ama-controls build-ama-controls
if NOT 0==%ERRORLEVEL% exit
exit /B

rem -----------------------------------------------------------
:build-datamart
del built\ama-datamart.js
node optimizers\r.js -o conf\build-ama-datamart.js
if NOT 0==%ERRORLEVEL% exit
exit /B

rem -----------------------------------------------------------
:build-datamart-admin
del built\ama-datamart-admin.js
node optimizers\r.js -o conf\build-ama-datamart-admin.js
if NOT 0==%ERRORLEVEL% exit
exit /B

rem -----------------------------------------------------------
:build-datamart-orpau
del built\ama-datamart-orpau.js
node optimizers\r.js -o conf\build-ama-datamart-orpau.js
if NOT 0==%ERRORLEVEL% exit
exit /B

rem -----------------------------------------------------------
:build-datamart-bfl
del built\ama-datamart-bfl.js
node optimizers\r.js -o conf\build-ama-datamart-bfl.js
if NOT 0==%ERRORLEVEL% exit
exit /B

rem -----------------------------------------------------------
:build-datamart-report
del built\ama-datamart-report.js
node optimizers\r.js -o conf\build-ama-datamart-report.js
if NOT 0==%ERRORLEVEL% exit
exit /B

rem -----------------------------------------------------------
:build-datamart-debtor
del built\ama-datamart-debtor.js
node optimizers\r.js -o conf\build-ama-datamart-debtor.js
if NOT 0==%ERRORLEVEL% exit
exit /B
