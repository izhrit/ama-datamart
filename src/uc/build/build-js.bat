@rem %1 - name of result js file
@rem %2 - name of config    file
pushd %~dp0..
del built\%1.js
node optimizers\r.js -o conf\%2.js
set result=%ERRORLEVEL%
popd
exit /B %result%