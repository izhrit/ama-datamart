app.cryptoapi = {

	mock_InternalChooseCertificateInn: '364484332544'

	,mock_InternalChooseCertificate: function ()
	{
		var cert = {
			IssuerName:'CN=Тестовый центр сертифкации'
			,SerialNumber:'1'
			,SubjectName: (this.mock_InternalChooseCertificateInn && null!=this.mock_InternalChooseCertificateInn)
				? 'ИНН=' + this.mock_InternalChooseCertificateInn
				: 'X=Y'
		};
		if (-1 != window.location.search.indexOf('use-real-choose-certificate')) // В случае параметра в строке поиска, то вызывается диалоговое окно с выбором сертификата.
			cert = $.capicom.getDialogBoxCertificates();
		return cert;
	}

	,ChooseCertificateAnd: function (do_with_certificate)
	{
		var cert = this.mock_InternalChooseCertificate();

		console.log('choosed certificate:');
		console.log('\tIssuerName  :' + cert.IssuerName);
		console.log('\tSerialNumber:' + cert.SerialNumber);
		console.log('\tSubjectName :' + cert.SubjectName);

		do_with_certificate(cert);
	}

	, SignBase64: function (rawData, detached, cert)
	{
		var signature= 'test_signature_for_' + rawData;

		console.log('prepared signature:');
		console.log(signature);

		return  signature;
	}
};
