cpw_ArgumentsProcessor =
{
	getJsonFromUrl: function (txt_arguments)
	{
		if (!txt_arguments)
			txt_arguments = location.search.substr(1);
		var arguments = {};
		var txt_argument_parts = txt_arguments.split('&');
		for (var i = 0; i < txt_argument_parts.length; i++)
		{
			var txt_argument_part = txt_argument_parts[i];
			if (0 != txt_argument_part)
			{
				var txt_argument_part_parts = txt_argument_part.split('=');
				if (1 >= txt_argument_part_parts.length)
				{
					arguments[txt_argument_part] = true;
				}
				else
				{
					arguments[txt_argument_part_parts[0]] = decodeURIComponent(txt_argument_part_parts[1]);
				}
			}
		}
		return arguments;
	}

	, select_form: function (id_form)
	{
		$("#cpw-form-select").select2('data', { text: id_form, id: id_form });
	}

	, CreateNew: function (id_form, sel)
	{
		if (id_form.indexOf('#') != -1)
			id_form = id_form.substring(0, id_form.length - 1);
		this.select_form(id_form);
		var form_spec = GetSelectedFormSpecificationFor_id_Form(id_form);
		if (!form_spec)
		{
			alert('can not find specification for form \"' + id_form + '\"');
		}
		else
		{
			CreateNewFormWithSpecFunc(form_spec, sel);
		}
	}

	, ProcessContentArgument: function (id_form_content, do_for_form_spec)
	{
		var arg_parts = id_form_content.split('.');
		var id_content = arg_parts[0] + '.' + arg_parts[2];
		var id_form = arg_parts[0] + '.' + arg_parts[1];

		$('#selected_content_id').text(id_content);

		var do_on_load_content= function()
		{
			cpw_ArgumentsProcessor.select_form(id_form);
			var form_spec_func = GetSelectedFormSpecificationFor_id_Form(id_form);
			if (!form_spec_func)
			{
				alert('can not find specification for form \"' + id_form + '\"');
			}
			else
			{
				do_for_form_spec(form_spec_func);
			}
		}

		var content = GetSelectedSpecificationFor_id(CPW_contents, id_content);
		if (!content)
		{
			alert('can not find content for id \"' + id_content + '\"');
			do_on_load_content();
		}
		else
		{
			var content_area = $('#form-content-text-area');
			if ('string' == typeof content)
			{
				$('#form-content-text-area').val(content);
				do_on_load_content();
			}
			else
			{
				content().done(function (required_content)
				{
					$('#form-content-text-area').val(required_content);
					do_on_load_content();
				});
			}
		}
	}

	, ProcessArguments: function ()
	{
		var args = this.getJsonFromUrl();

		if (args.clear_profile)
		{
			app.profile = {};
		}

		if (args.hide_mock_header)
		{
			$('div.wbt-mock-header-old').hide();
		}

		if (args.use_test_timer)
		{
			app.cpw_Now = function () { return new Date(2015, 6, 26, 17, 36, 0, 0); };
		}

		if (args.view)
		{
			this.ProcessContentArgument(args.edit, EditFormWithSpecFunc);
		}
		else if (args.edit)
		{
			this.ProcessContentArgument(args.edit, EditFormWithSpecFunc);
		}
		else if (args.createnew)
		{
			this.CreateNew(args.createnew);
		}
	}

	, StartAnotherTestSession: function(name,args)
	{
		if (!this.sessions)
		{
			this.sessions = ['root'];
			this.controllers = [app.current_form_spec];
		}
		this.icurrent_session = this.sessions.length;
		this.sessions.push(name);
		this.ShowSessions();

		var reditor0 = $('div#report-editor');
		var id = 'report-editor' + this.icurrent_session;
		var reditor = reditor0.clone();
		reditor.attr('id', id);
		reditor.text("Здесь должен появиться новый элемент " + name);
		reditor0.after(reditor);
		reditor0.hide();
		if (1 != this.icurrent_session)
			$('div#report-editor' + (this.icurrent_session-1)).hide();
		reditor.show();

		var args = this.getJsonFromUrl(args);
		if (args.edit)
		{
			this.ProcessContentArgument(args.edit, function (form_spec_func)
			{
				EditFormWithSpecFunc(form_spec_func, '#'+id);
			});
		}
		else if (args.createnew)
		{
			this.CreateNew(args.createnew, '#' + id);
		}
		this.controllers.push(app.current_form_spec);
	}

	, SetNameForCurrentTestSession: function (name)
	{
		if (!this.sessions)
		{
			this.icurrent_session = 0;
			this.sessions = ['root'];
			this.controllers = [app.current_form_spec];
		}
		this.sessions[this.icurrent_session] = name;
		this.ShowSessions();
	}

	, ShowSessions: function()
	{
		$('span.test-sessions').html();
		if (this.sessions)
		{
			var html = '&nbsp;&nbsp;&nbsp;&nbsp;';
			for (var i = 0; i < this.sessions.length; i++)
			{
				if (0 != i)
					html += '&nbsp;|&nbsp;';
				html += '<a href="#null"';
				if (i == this.icurrent_session)
					html += ' style="font-weight:bold;"';
				html += ' onclick="cpw_ArgumentsProcessor.OnSession(' + i + ')"';
				html += '>';
				html += this.sessions[i];
				html += '</a>';
			}
			$('span.test-sessions').html(html);
		}
	}

	, OnSession: function(isession)
	{
		this.icurrent_session = isession;
		app.current_form_spec = this.controllers[isession];
		this.ShowSessions();
		for (var i = 0; i < this.sessions.length; i++)
		{
			var sel = (0 == i) ? 'div#report-editor' : 'div#report-editor' + i;
			if (isession==i)
			{
				$(sel).show();
			}
			else
			{
				$(sel).hide();
			}
		}
	}
};
