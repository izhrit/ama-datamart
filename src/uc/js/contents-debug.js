require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	map:
	{
		'*':
		{
			txt: 'js/libs/txt'
		}
	}
});

var namespace = {
	  'ama': 'forms/ama/spec_ama'
	, 'mpl': 'forms/mpl/spec_mpl'
	, 'bfl': 'forms/bfl/spec_bfl'
	, 'test': 'forms/test/spec_test'
};

var namespaces_path = [];
var namespace_name = [];
for (var name in namespace)
{
	namespace_name.push(name);
	namespaces_path.push(namespace[name]);
}

require(namespaces_path, function ()
{
	aCPW_contents = {};
	var CPW_contents_specs = {};
	for (var i = 0; i < arguments.length; i++)
	{
		var spec = arguments[i];
		var contents = {};
		var specs = {};
		for (var name in spec.content)
		{
			contents[name] = (function (path)
			{
				return function ()
				{
					var dfd = $.Deferred();
					require([path], function (content_text)
					{
						dfd.resolve(content_text);
					});
					return dfd.promise();
				};
			})(spec.content[name].path);
			specs[name] = spec.content[name];
		}
		aCPW_contents[namespace_name[i]] = contents;
		CPW_contents_specs[namespace_name[i]] = specs;
	}

	CPW_contents = aCPW_contents;

	$(function ()
	{
		var content_selector = $('#cpw-content-select');
		content_selector.append($('<option>', {
			value: '',
			text: ''
		}));
		for (var content_namespace in CPW_contents)
		{
			var namespace_specs = CPW_contents_specs[content_namespace];
			for (var content_name in CPW_contents[content_namespace])
			{
				var id_content = content_namespace + '.' + content_name;
				var option_attrs = {
					value: id_content,
					text: id_content
				};
				if (namespace_specs[content_name])
				{
					var spec = namespace_specs[content_name];
					if (spec.keywords)
					{
						option_attrs['data-keywords'] = spec.keywords;
						option_attrs.text += ' ' + spec.keywords;
					}
					if (spec.title)
					{
						option_attrs['data-title'] = spec.title;
						option_attrs.text += ' ' + spec.title;
					}
				}
				content_selector.append($('<option>', option_attrs));
			}
		}

		if (OnLoadContents)
			OnLoadContents();

		$('#contents_loaded_span').show();
	});
});
