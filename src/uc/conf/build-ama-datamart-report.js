// node optimizers\r.js -o baseUrl=. name=optimizers/almond include=forms/fms/extension out=..\built\fms.js wrap=true
({
    baseUrl: "..",
    include: ['forms/ama/datamart/procedure/section/report/e_ama_dm_report.js'],
    name: "optimizers/almond",
    out: "..\\built\\ama-datamart-report.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl'
      }
    }
})