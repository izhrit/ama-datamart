({
    baseUrl: "..",
    include: ['js/wbt'],
    name: "optimizers/almond",
    out: "..\\built\\wbt.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})