// node optimizers\r.js -o baseUrl=. name=optimizers/almond include=forms/fms/extension out=..\built\fms.js wrap=true
({
    baseUrl: "..",
    include: ['forms/bfl/e_bfl'],
    name: "optimizers/almond",
    out: "..\\built\\ama-datamart-bfl.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})