@rem **************************************************************************
@rem
@rem подготовка файлов, зависящих от локальных настроек конкретной рабочей
@rem папки разработчика в соответствии с settings.txt
@rem
@rem **************************************************************************

@echo %TIME%
@pushd %~dp0

@mkdir built-local

@call CScript //nologo %~dp0\scripts\prep.js

copy built-local\config.php ..\srv\assets\
copy built-local\mock_efrsb_config.php ..\srv\tests\mock-web\efrsb\assets\config.php
copy built-local\mock_lics_config.php ..\srv\tests\mock-web\lics\assets\config.php

@popd
@echo %TIME%
