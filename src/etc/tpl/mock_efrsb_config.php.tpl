<?

global $default_db_options;
$default_db_options= (object)array(
	'host'=> 'localhost'
	,'user'=>'<%= DBUser %>'
	,'password'=> '<%= DBPassword %>'
	,'dbname'=> '<%= DBName %>'
	,'charset'=> 'utf8'
);

global $log_file_name, $trace_methods, $use_pretty_json_print;
$log_file_name= '<%= ROOT_PROJ_DIR %>src\srv\tests\mock-web\efrsb\log.txt';
$trace_methods= true;
$use_pretty_json_print= true;

global $tbl_prefix; 
$tbl_prefix= 'mock_efrsb_';
