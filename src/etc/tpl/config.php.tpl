<?

global $default_db_options;
$default_db_options= (object)array(
	'host'=> 'localhost'
	,'user'=>'<%= DBUser %>'
	,'password'=> '<%= DBPassword %>'
	,'dbname'=> '<%= DBName %>'
	,'charset'=> 'utf8'
);

global $crm2_db_options;
$crm2_db_options= (object)array(
	'host'=> 'localhost'
	,'user'=>'<%= DBUser %>'
	,'password'=> '<%= DBPassword %>'
	,'dbname'=> '<%= DBName %>'
	,'charset'=> 'utf8'

	,'Contract_table_name'=>'mock_crm2_Contract'
);

global $email_settings;
$email_settings= (object)array(
	 'smtp_server'=>     '?????'
	,'smtp_user'=>       '?????'
	,'smtp_password'=>   '?????'
	,'smtp_port'=>       25
	,'smtp_from_email'=> 'info@russianit.ru'
	,'smtp_from_name'=>  '���'
	,'enabled'=>false
);

global $sms_settings;
$sms_settings= (object)array(
	 'HTTPS_LOGIN'=>'???'
	,'HTTPS_PASSWORD'=>'???'
	,'HTTPS_ADDRESS'=>'???'

	,'FROM'=>'PAU'

	,'enabled'=>false
);

global $operator_pem_file_path;
//$operator_pem_file_path= "file://???????/datamart/ignored/certs/AmaDatamartOperatorRIT.pem";
$operator_pem_file_path= null;

global $log_file_name, $trace_methods, $use_pretty_json_print;
$log_file_name= '<%= ROOT_PROJ_DIR %>src\srv\log.txt';
$trace_methods= true;
$use_pretty_json_print= true;

global $session_token_salt;
$session_token_salt= '000102030405060702390a0b0c0d0e0f10fe12131415161718191a1b1c1d1e1f';

global $password_length;
$password_length= 8;

global $base_apps_url;
$base_apps_url= 'http://local.test/dm/';

global $datamart_api_url, $datamart_bck_url, $datamart_ui_url, $datamart_cabinetcc_url, $datamart_application_url;
$datamart_api_url= $base_apps_url.'api.php';
$datamart_bck_url= $base_apps_url.'ui-backend.php';
$datamart_ui_url=  $base_apps_url.'ui.php';
$datamart_cabinetcc_url= $base_apps_url.'cabinetcc.php';
$datamart_application_url= $base_apps_url.'application.php';
$datamart_debtor_url= $base_apps_url.'debtor.php';

global $partner_accounts;
$partner_accounts= array(array('login'=>'ctb', 'password'=>'Hn51Ldt456Abn'));

global $casebook_account;
$casebook_account= array('login'=>'clubau','password'=>'RxVTQJlmQO');

global $signature_options;
$signature_options= array('key'=>'DITVFNEWNPHFADIADKOPAAAW','iv'=>'');

global $bt_options;
$bt_options= array('key'=>'DITVFNEWNPMCPDIADKOPAAAW','iv'=>'');

global $autologin_auth_options;
$autologin_auth_options= array('key'=>'DTLAH59GNPMCPDIADKOPAAAW','iv'=>'');

global $autologin_fauth_options;
$autologin_fauth_options= array('key'=>'DTLPKCRMNPMCPDIADKOPAAAW','iv'=>'');

global $api_autologin_auth_options;
$api_autologin_auth_options= array('key'=>'DTLAH59GNPMCUA6ADKOPAAAW','iv'=>'');

global $bt_password_options;
$bt_password_options= array('key'=>'DITHTBEWNPMCPDIADKOPAAAW','iv'=>'');

global $auth_max_session_seconds, $auth_max_session_idle_seconds;
$auth_max_session_seconds= 60*60;
$auth_max_session_idle_seconds= 10*60;

global $temp_zip_filepath;
$temp_zip_filepath= '<%= ROOT_PROJ_DIR %>src\srv\tmp.zip';

global $use_activex, $use_validate_certificate;
$use_activex = false;
$use_validate_certificate = false;

global $award_admin_users;
$award_admin_users= array(
(object)array('login'=>'Дмитрий Коновалов', 'password'=>'3g4T5gLo6')
,(object)array('login'=>'Игорь Вышегородцев', 'password'=>'12Hj7ty5')
,(object)array('login'=>'Иван Рыков', 'password'=>'527Ghnfty')
,(object)array('login'=>'Сергей Завьялов', 'password'=>'5Hnldsf6Hbt')
,(object)array('login'=>'Светлана Карелина', 'password'=>'196Ftd5ff5')
,(object)array('login'=>'Евгений Исаков', 'password'=>'38gVrdk62gF')
);

global $admin_users;
$admin_users= array(
(object)array('login'=>'admin', 'password'=>'111','id_Admin'=>1,'Name'=>'Аристарх')
);

global $use_efrsb_service_url;
//$use_efrsb_service_url= 'http://probili.ru/efrsb';
$use_efrsb_service_url= 'http://local.test/dm-mock/efrsb/web';

global $use_server_license_url;
//$use_server_license_url= "https://dlicense.rsit.ru/ama/"
$use_server_license_url= 'http://local.test/dm-mock/lics/web/api.php';

global $use_casebook_api, $use_casebook_auth;
//$use_casebook_api='http://local.test:8080/cryptoapi/Service.svc';
$use_casebook_api='http://local.test/dm-mock/casebook/web/mock.php';
$use_casebook_auth= 'test1';

$use_casebook_api='http://local.test/dm-mock/casebook/web/mock.php';

global $base_cryptoapi_url;
$base_cryptoapi_url='http://local.test/dm-mock/cryptoapi/mock.php';

global $test_settings;
$test_settings= (object)array(
	'use_test_time'=>true
	,'show_autologin_errors'=>true
);

global $test_bt_account;
$test_bt_account= (object)array
(
	'domain_name'=>'russianit'
	,'login'=>'vva@russianit.ru'
	,'password'=>'KoranVirus2020'
);

global $dadata_API_KEY,$dadata_SECRET_KEY;
$dadata_API_KEY='ebb2c76cb157b8b4553e98f43c48ce3598d2fccc';
$dadata_SECRET_KEY='ffc7796c5510f0e88249c4d5b07c0951a86cee1c';
//$use_dadata_suggestions_api= 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address';
$use_dadata_suggestions_api= 'http://local.test/dm-mock/dadata/suggestions/mock.php';

global $custom_query_directory_path;
$custom_query_directory_path= '<%= ROOT_PROJ_DIR %>src\srv\cust_query';

global $enable_viewer_registration;
$enable_viewer_registration= true;

global $firebase_auth_headers;
$firebase_auth_headers = array('Authorization:key = AAAA81smKNI:APA91bEMhdpRusjFwUVhTSt98CzcpGe6ICgiIZJiwoeWdAKZLu5_g5u75H1VXMh44CIc2YY3pP1vPmY593wGyY5JRxNBpRxx7PKS3oTZn45EdfUD-boEInn2BH996gi8TjHnWPHEOiEF','Content-Type: application/json');

global $job_params;
$job_params->site_name= 'datamart';
$job_params->starts_max_portion_size_select= 100;

$google_calendar_options= (object)array
(
	'CLIENT_ID'=>'934784121167-jgh60353fn71cnghi674dfdks0mcmv1g.apps.googleusercontent.com'
	,'CLIENT_REDIRECT_URL'=>$datamart_bck_url . '?action=google.calendar.update.token' 
);

$google_calendar_secret = 'bMbhhJNYzhcwxd1RXcZ4SLKl';

global $exposure_recipiet_pro_list;
$exposure_recipiet_pro_list= array(
(object)array('id'=>11,'name'=>'Банкрот форум','url'=>'https://bankrotforum.ru/')
,(object)array('id'=>12,'name'=>'Портал ДА','url'=>'https://portal-da.ru/')
);