<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/job.php';

require_once '../assets/libs/starts/casebookapi.php';
require_once '../assets/libs/log/access_log_cli.php';

function prepare_CasebookAPI()
{
	global $use_casebook_api, $use_casebook_auth;
	return new CasebookAPI((object)array('base_url'=>$use_casebook_api,'auth'=>$use_casebook_auth));
}

function get_cases_to_find_out($max_portion_size,$max_minutes_between_check)
{
	$current_time= job_time();
	$current_time= date_create_from_format('Y-m-d\TH:i:s',$current_time);
	$time_to_check= date_sub($current_time, date_interval_create_from_date_string($max_minutes_between_check.' minutes'));
	$time_to_check= date_format($time_to_check,'Y-m-d H:i:s');
	$time_of_create= date_sub($current_time, date_interval_create_from_date_string(($max_minutes_between_check*4*24*7).' minutes')); // неделю.. 168-672 раз..
	$time_of_create= date_format($time_of_create,'Y-m-d H:i:s');

	$txt_query="select 
	  id_ProcedureStart id
	, DebtorName, DebtorINN, DebtorOGRN, DebtorSNILS
	, CasebookTag
	from ProcedureStart p
	inner join Court c on c.id_Court=p.id_Court
	where (p.CaseNumber is null or ''=p.CaseNumber)
	and (TimeOfLastChecking is null or TimeOfLastChecking<=?)
	and TimeOfCreate>?
	order by TimeOfLastChecking
	limit $max_portion_size
	";
	echo $txt_query . "\r\n";
	echo "time_to_check=$time_to_check\r\n";
	echo "time_of_create=$time_of_create\r\n";
	$rows= execute_query($txt_query,array('ss',$time_to_check,$time_of_create));

	$cases= array();
	foreach ($rows as $row)
	{
		$cases[]= array(
			'id'=>$row->id
			,'debtor'=>array(
				'Name'=>$row->DebtorName
				,'INN'=>$row->DebtorINN
				,'OGRN'=>$row->DebtorOGRN
				,'SNILS'=>$row->DebtorSNILS
			)
			,'CourtTag'=>$row->CasebookTag
		);
	}

	return $cases;
}

function update_procedure_start($case,$details_by_id)
{
	$id= $case['id'];
	$dc= null;
	if (isset($details_by_id[$id]))
		$dc= $details_by_id[$id];
	$txt_query= 'update ProcedureStart set TimeOfLastChecking= ?';
	$first_arg= 's';
	$args= array($first_arg,date_format(date_create_from_format('Y-m-d\TH:i:s',job_time()),'Y-m-d H:i:s'));
	$found= false;
	if (null!=$dc)
	{
		if (null!=$dc->CaseNumber)
		{
			$txt_query.= ', CaseNumber=?';
			$first_arg.= 's';
			$args[]= $dc->CaseNumber;
			$found= true;
		}
		if (null!=$dc->NextSessionDate)
		{
			$dt= date_create_from_format('d.m.Y H:i',$dc->NextSessionDate);
			if (false==$dt)
				$dt= date_create_from_format('d.m.Y H:i:s',$dc->NextSessionDate);
			if (false==$dt)
			{
				echo job_time() . " .. can not create date from NextSessionDate {$dc->NextSessionDate}\r\n";
			}
			else
			{
				$txt_query.= ', NextSessionDate=?';
				$first_arg.= 's';
				$args[]= date_format($dt,'Y-m-d\TH:i:s');
				$found= true;
			}
		}
	}
	$txt_query.= ' where id_ProcedureStart=?;';
	$first_arg.= 's';
	$args[]= $id;
	$args[0]= $first_arg;
	execute_query_no_result($txt_query,$args);
	if ($found)
		write_to_access_log_id('robot/found-start-case',$id);
}

function update_starts_cb_fields($active_started_last_30_days= true, $max_to_check= 100, $max_minutes_between_check= 15)
{
	global $job_params;
	$casebookapi= prepare_CasebookAPI();
	$max_portion_size_select= $job_params->starts_max_portion_size_select;
	$checked= 0;
	$total_updated= 0;
	$count_exceptions= 0;

	while ($checked<$max_to_check)
	{
		echo job_time() . " query for $max_portion_size_select procedure starts to find out the case ..\r\n";
		$cases= get_cases_to_find_out($max_portion_size_select,$max_minutes_between_check);
		$count_cases= count($cases);
		echo job_time() . " .. got $count_cases procedure starts to find out the case\r\n";
		if (0==$count_cases)
			break;
		$checked+= $count_cases;

		$details= array();
		echo job_time() . " query for details ..\r\n";
		try
		{
			$details= $casebookapi->FindOutDetailsOfTheCases($cases);
		}
		catch (Exception $ex)
		{
			echo job_time() . ' catched exception: ' . get_class($ex) . ' - ' . $ex->getMessage() . "\r\n";
			if (is_a($ex,'Rest_exception'))
			{
				echo job_time() . ' used url: ' . $casebookapi->get_last_url() . "\r\n";
				echo job_time() . ' responce: ' . $ex->responce . "\r\n";
			}
			$count_exceptions++;
		}
		$count_details= count($details);
		echo job_time() . " .. got $count_details details ..\r\n";

		$details_by_id= prep_details_by_id($details);

		$count_updated=0;
		foreach ($cases as $case)
		{
			update_procedure_start($case,$details_by_id);
			$count_updated++;
		}
		echo job_time() . " updated $count_updated procedures\r\n";
		$total_updated+= $count_updated;
	}
	$res_msg= "проверено согласий: $checked,\r\n получена новая информация для: $total_updated";
	return (0==$count_exceptions) 
		? $res_msg
		: (object)array('status'=>1,'results'=>$res_msg.",\r\nпроизошло исключительных ситуаций: $count_exceptions");
}