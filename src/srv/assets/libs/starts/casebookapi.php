<?php 

require_once '../assets/config.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/client.rest.php';

class CasebookAPI extends Rest_client
{
	public $base_url= null;
	public $auth= null;

	public function __construct($args)
	{
		parent::__construct();
		$this->base_url= $args->base_url;
		$this->auth= $args->auth;
	}

	function FindOutDetailsOfTheCases($cases,$current_unix_time= 0)
	{
		$args= array(
			'auth_token'=>$this->auth
			,'cases'=>$cases
			,'current_unix_time'=>$current_unix_time
		);
		$url= $this->base_url."/FindOutDetailsOfTheCases";
		write_to_log("url=$url");
		$request_result= $this->request(array(
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_HTTPGET => false, CURLOPT_POST => true, CURLOPT_PUT=> false,
			CURLOPT_POSTFIELDS => json_encode($args)
		));

		return json_decode($request_result);
	}
}

function prep_details_by_id($details)
{
	$details_by_id= array();
	if (0!=count($details))
	{
		foreach ($details as $dc)
			$details_by_id[$dc->id]= $dc;
	}
	return $details_by_id;
}