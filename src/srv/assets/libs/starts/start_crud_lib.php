<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/qb.php';

class start_query_builder extends query_builder
{
	private function Должник_физ_лицо_comma_separated_params($Должник, $num='')
	{
		$this->comma_par('DebtorCategory' . $num,'n');
		$Физ_лицо =  (isset($Должник->Физ_лицо)) ? (object) $Должник->Физ_лицо : (object) array();
		$DebtorName= '';
		if (isset($Физ_лицо->Фамилия))
			$DebtorName.= $Физ_лицо->Фамилия;
		if (isset($Физ_лицо->Имя))
			$DebtorName.= ' '.$Физ_лицо->Имя;
		if (isset($Физ_лицо->Отчество))
			$DebtorName.= ' '.$Физ_лицо->Отчество;
		$this->comma_par('DebtorName' . $num, !isset($Физ_лицо->Фамилия) ? null : $DebtorName);
		$this->comma_par('DebtorSnils' . $num, !isset($Физ_лицо->СНИЛС) ? null : $Физ_лицо->СНИЛС);
	}

	private function Должник_общие_данные_comma_separated_params($Должник, $num='')
	{
		$this->comma_par('DebtorINN' . $num, !isset($Должник->ИНН) ? null : $Должник->ИНН);
		$this->comma_par('DebtorOGRN' . $num, !isset($Должник->ОГРН) ? null : $Должник->ОГРН);
		$this->comma_par('DebtorAddress' . $num, !isset($Должник->Адрес) ? null : $Должник->Адрес);
	}

	private function Должник_comma_separated_params($Должник)
	{
		switch ($Должник->Тип)
		{
			case 'Физ_лицо':
				$this->Должник_физ_лицо_comma_separated_params($Должник);
				$this->Должник_общие_данные_comma_separated_params($Должник);
				$this->Должник_физ_лицо_comma_separated_params((object) array(), 2);
				$this->Должник_общие_данные_comma_separated_params((object) array(), 2);
				break;
			case 'Юр_лицо':
				$this->comma_par('DebtorCategory','l');
				$this->comma_par('DebtorName',$Должник->Юр_лицо['Наименование']);
				$this->Должник_общие_данные_comma_separated_params($Должник);
				$this->Должник_физ_лицо_comma_separated_params((object) array(), 2);
				$this->Должник_общие_данные_comma_separated_params((object) array(), 2);
				break;
			case 'Супруги':
				$this->Должник_физ_лицо_comma_separated_params((object) $Должник->Супруги['Должник1']);
				$this->Должник_общие_данные_comma_separated_params((object) $Должник->Супруги['Должник1']);
				$this->Должник_физ_лицо_comma_separated_params((object) $Должник->Супруги['Должник2'], 2);
				$this->Должник_общие_данные_comma_separated_params((object) $Должник->Супруги['Должник2'], 2);
				break;
		}
	}

	private function Заявитель_comma_separated_params($Заявитель)
	{
		if(isset($Заявитель->Заявитель_должник) && $Заявитель->Заявитель_должник==='true') {
			$this->comma_par('ApplicantCategory', null);
			$this->comma_par('ApplicantName',null);
			$this->comma_par('ApplicantSnils',null);
			$this->comma_par('ApplicantINN',null);
			$this->comma_par('ApplicantOGRN',null);
			$this->comma_par('ApplicantAddress',null);

			return;
		}
		switch ($Заявитель->Тип)
		{
			case 'Физ_лицо':
				$this->comma_par('ApplicantCategory','n');
				$Физ_лицо= (object)$Заявитель->Физ_лицо;
				$ApplicantName= $Физ_лицо->Фамилия;
				if (isset($Физ_лицо->Имя))
					$ApplicantName.= ' '.$Физ_лицо->Имя;
				if (isset($Физ_лицо->Отчество))
					$ApplicantName.= ' '.$Физ_лицо->Отчество;
				$this->comma_par('ApplicantName',$ApplicantName);
				$this->safe_comma_field_par('ApplicantSnils',$Физ_лицо,'СНИЛС');
				break;
			case 'Юр_лицо':
				$this->comma_par('ApplicantCategory','l');
				$this->comma_par('ApplicantName',$Заявитель->Юр_лицо['Наименование']);
				break;
		}
		$this->safe_comma_field_par('ApplicantINN',$Заявитель,'ИНН');
		$this->safe_comma_field_par('ApplicantOGRN',$Заявитель,'ОГРН');
		$this->safe_comma_field_par('ApplicantAddress',$Заявитель,'Адрес');
	}

	private function Заявление_comma_separated_params($заявление)
	{
		$this->comma_select2('id_Court',$заявление->В_суд);
		if (isset($заявление->Дата_подачи))
			$this->comma_ru_date('DateOfApplication',$заявление->Дата_подачи);
		if (isset($заявление->Дата_рассмотрения))
			$this->comma_ru_legal_date_time('NextSessionDate',$заявление->Дата_рассмотрения);
		if (isset($заявление->Дата_принятия))
			$this->comma_ru_date('DateOfAcceptance',$заявление->Дата_принятия);
	}

	private function Запрос_comma_separated_params($запрос)
	{	
		if(isset($запрос->Время)) {
			$время = (object)$запрос->Время;
			if (isset($время->акта)) {
				$this->comma_ru_date('DateOfRequestAct', $время->акта);
			}
		}

		if(isset($запрос->Ссылка))
			$this->comma_par('CourtDecisionURL',$запрос->Ссылка);

		if(isset($запрос->Дополнительная_информация))
			$this->comma_par('CourtDecisionAddInfo',$запрос->Дополнительная_информация);
	}

	private function Назначение_comma_separated_params($назначение)
	{
		if(isset($назначение->Время)) {
			$время = (object)$назначение->Время;
			if (isset($время->акта)) {
				$this->comma_ru_date('DateOfPrescription', $время->акта);
			}
		}

		if(isset($назначение->Ссылка))
			$this->comma_par('PrescriptionURL',$назначение->Ссылка);
		
		if(isset($назначение->Дополнительная_информация))
			$this->comma_par('PrescriptionAddInfo',$назначение->Дополнительная_информация);
	}

	private function ЕФРСБ_comma_separated_params($ефрсб)
	{
		if(isset($ефрсб->Номер))
			$this->comma_par('efrsbPrescriptionNumber',$ефрсб->Номер);
		if(isset($ефрсб->Дата_публикации))
			$this->comma_ru_date('efrsbPrescriptionPublishDate',$ефрсб->Дата_публикации);
		if(isset($ефрсб->ID))
			$this->comma_par('efrsbPrescriptionID',$ефрсб->ID);
		if(isset($ефрсб->Дополнительная_информация))
			$this->comma_par('efrsbPrescriptionAddInfo',$ефрсб->Дополнительная_информация);
	}

	public function comma_separated_params($start)
	{
		if (isset($start->Должник))
			$this->Должник_comma_separated_params((object)$start->Должник);
		
		if(isset($start->Заявитель))
			$this->Заявитель_comma_separated_params((object)$start->Заявитель);

		if (isset($start->Заявление_на_банкротство))
			$this->Заявление_comma_separated_params((object)$start->Заявление_на_банкротство);

		if (isset($start->Запрос))
			$this->Запрос_comma_separated_params((object)$start->Запрос);

		if (isset($start->Назначение))
			$this->Назначение_comma_separated_params((object)$start->Назначение);

		if(isset($start->ЕФРСБ))
			$this->ЕФРСБ_comma_separated_params((object)$start->ЕФРСБ);

		if (isset($start->АУ))
			$this->comma_select2('id_Manager',$start->АУ);

		if (isset($start->Показывать_для_регистрации_в_ПАУ))
		{
			$ReadyToRegistrate= $start->Показывать_для_регистрации_в_ПАУ;
			$this->comma_par('ReadyToRegistrate',$ReadyToRegistrate && 'false'!=$ReadyToRegistrate,'i');
		}

		if (isset($start->Показывать_СРО))
		{
			$ShowToSRO= $start->Показывать_СРО;
			$this->comma_par('ShowToSRO',$ShowToSRO && 'false'!=$ShowToSRO,'i');
		}

		if (isset($start->Номер_судебного_дела))
			$this->comma_par('CaseNumber',$start->Номер_судебного_дела);
		if (isset($start->TimeOfLastChecking))
			$this->comma_DateTime('TimeOfLastChecking',$start->TimeOfLastChecking);
	}
}

class start_helper
{
	function start_row_to_debtor($row, $num='')
	{
		$debtor= (object)array('ИНН'=> $row->{'DebtorINN' . $num}, 'ОГРН'=> $row->{'DebtorOGRN' . $num}, 'Адрес'=> $row->{'DebtorAddress' . $num});
		if ('n'!=$row->{'DebtorCategory' . $num})
		{
			$debtor->Тип= 'Юр_лицо';
			$debtor->Юр_лицо= array('Наименование'=> $row->{'DebtorName' . $num});
		}
		else
		{
			$np= explode(' ',$row->{'DebtorName' . $num});
			$debtor->Тип= 'Физ_лицо';
			$debtor->Физ_лицо= array('Фамилия'=> $np[0], 'Имя'=> $np[1], 'Отчество'=> $np[2], 'СНИЛС'=> $row->{'DebtorSNILS' . $num});
		}
		return $debtor;
	}

	function start_row_to_applicant($row)
	{
		if(isset($row->ApplicantCategory)) {
			$applicant= (object)array('ИНН'=> $row->ApplicantINN, 'ОГРН'=> $row->ApplicantOGRN, 'Адрес'=> $row->ApplicantAddress);
			if ('n'!=$row->ApplicantCategory)
			{
				$applicant->Тип= 'Юр_лицо';
				$applicant->Юр_лицо= array('Наименование'=> $row->ApplicantName);
			}
			else
			{
				$np= explode(' ',$row->ApplicantName);
				$applicant->Тип= 'Физ_лицо';
				$applicant->Физ_лицо= array('Фамилия'=> $np[0], 'Имя'=> $np[1], 'Отчество'=> $np[2], 'СНИЛС'=> $row->ApplicantSNILS);
			}
			return $applicant;
		}else {
			return null;
		}
	}

	function check_court_decision($start, $key)
	{
		if(isset($start[$key])) {
			$Судебный_акт = (object)$start[$key];
			if(isset($Судебный_акт->Время)) {
				$время = (object)$Судебный_акт->Время;
				if((isset($время->акта) && ''!==trim($время->акта) && 'null'!==trim($время->акта)) && (!isset($Судебный_акт->Ссылка) || ''==trim($Судебный_акт->Ссылка))) {
					exit_bad_request("can not create/update ProcedureStart: {$key}['Ссылка] required with {$key}['Время']['акта']!");
				}
			}
			if(isset($Судебный_акт->Ссылка) && ''!==trim($Судебный_акт->Ссылка)) {
				if(!isset($Судебный_акт->Время)) {
					exit_bad_request("can not create/update ProcedureStart: {$key}['Время']['акта'] required with {$key}['Ссылка]!");
				} else {
					$время = (object)$Судебный_акт->Время;
					if(!isset($время->акта) || ''==trim($время->акта) && 'null'!==trim($время->акта)) {
						exit_bad_request("can not create/update ProcedureStart: {$key}['Время']['акта'] required with {$key}['Ссылка]!");
					}
				}
			}
		}
	}

	function check_efrsb($field, $fields)
	{
		if(isset($ЕФРСБ->{$field}) && ''!==trim($ЕФРСБ->{$field})) {
			foreach($fields as $key) {
				if(!isset($ЕФРСБ->{$key}) || ''===trim($ЕФРСБ->{$key}) || 'null'===trim($ЕФРСБ->{$key}))
				exit_bad_request("can not {$action} ProcedureStart required ЕФРСБ[{$key}] with ЕФРСБ[{$field}]!");
			}
		}
	}
}

global $start_helper;
$start_helper = new start_helper();

function create_start($start, $id_Contract)
{
	$uiqb= new start_query_builder('insert into ProcedureStart set ');
	$uiqb->comma_text('TimeOfCreate=now()');
	$uiqb->comma_separated_params((object)$start);
	$uiqb->comma_par('id_Contract',$id_Contract);
	return array('id' => $uiqb->execute_query_get_last_insert_id());
}
function update_start_courtDecisionAddInfo($id_ProcedureStart, $дополнительная_информация, $id_Contract, $id_Manager)
{
	$txt_query = "update ProcedureStart set CourtDecisionAddInfo=? 
		where id_ProcedureStart=? and AddedBySRO=0 and (1=0";
	$params = array("si", $дополнительная_информация, $id_ProcedureStart);
	if (null!=$id_Contract) {
		$txt_query.= ' or id_Contract=?';
		$params[0].= 'i';
		$params[]= $id_Contract;
	}
	if (null!=$id_Manager) {
		$txt_query.= ' or id_Manager=?';
		$params[0].= 'i';
		$params[]= $id_Manager;
	}
	$txt_query.= ')';
	return execute_query_get_affected_rows($txt_query,$params);
}

function update_start($id_ProcedureStart,$start,$id_Contract,$id_Manager)
{
	if(count($start) === 1 && isset($start["Дополнительная_информация"])) {
		return update_start_courtDecisionAddInfo($id_ProcedureStart, $start["Дополнительная_информация"], $id_Contract, $id_Manager);
	}
	$txt_query= 'select id_MRequest from ProcedureStart where 
				id_ProcedureStart=? and (1=0';
	$params= array('i', $id_ProcedureStart);
	if (null!=$id_Contract) {
		$txt_query.= ' or id_Contract=?';
		$params[0].= 'i';
		$params[]= $id_Contract;
	}
	if (null!=$id_Manager) {
		$txt_query.= ' or id_Manager=?';
		$params[0].= 'i';
		$params[]= $id_Manager;
	}
	$txt_query.= ')';
	$rows= execute_query($txt_query,$params);
	if(0!=count($rows) && isset($rows[0]->id_MRequest)){
		return 'can not update ProcedureStart, id_MRequest is not null';
	}else {
		$uiqb= new start_query_builder('update ProcedureStart set ');
		$uiqb->comma_separated_params((object)$start);
		$uiqb->parametrized("\r\nwhere id_ProcedureStart=?",$id_ProcedureStart);
		$uiqb->simple_condition("\r\nand id_MRequest is null");
		if (null!=$id_Contract)
			$uiqb->parametrized("\r\nand id_Contract=?",$id_Contract);
		if (null!=$id_Manager)
			$uiqb->parametrized("\r\nand id_Manager=?",$id_Manager);
		return $uiqb->execute_query_get_affected_rows();
	}
}

function delete_starts($ids_ProcedureStart,$id_Contract,$id_Manager)
{
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	foreach ($ids_ProcedureStart as $id_ProcedureStart)
	{
		$txt_query= 'select id_MRequest from ProcedureStart where 
				id_ProcedureStart=? and id_Contract=?';
		$params= array('ii', $id_ProcedureStart, $id_Contract);
		if (null!=$id_Manager) {
			$txt_query.= ' and id_Manager=?';
			$params[0].= 'i';
			$params[]= $id_Manager;
		}
		$rows= execute_query($txt_query,$params);
		if(0!=count($rows) && isset($rows[0]->id_MRequest)){
			$connection->rollback();
			return 'can not delete ProcedureStart, id_MRequest is not null';
		}else {
			$uiqb= new start_query_builder('delete from ProcedureStart where');
			$uiqb->parametrized(' id_ProcedureStart=?',$id_ProcedureStart,'i');
			$uiqb->simple_condition("\r\nand id_MRequest is null");
			$uiqb->parametrized(' and id_Contract=?',$id_Contract);
			if (null!=$id_Manager)
				$uiqb->parametrized(' and id_Manager=?',$id_Manager);
			$affected= $uiqb->execute_query_get_affected_rows($connection);
			if (1!=$affected)
			{
				$connection->rollback();
				write_to_log("can not delete ProcedureStart where id_ProcedureStart=$id_ProcedureStart (affected $affected rows)");
				return false;
			}
		}	
	}
	$connection->commit();
	return true;
}

function read_start($id_ProcedureStart)
{
	global $start_helper;
	global $auth_info;

	$txt_query= "select ps.DebtorName ,ps.DebtorSNILS, ps.DebtorINN, ps.DebtorOGRN, ps.DebtorAddress, ps.DebtorCategory
			, ps.DebtorName2 ,ps.DebtorSNILS2, ps.DebtorINN2, ps.DebtorOGRN2, ps.DebtorAddress2, ps.DebtorCategory2
			, ps.ApplicantName ,ps.ApplicantSNILS, ps.ApplicantINN, ps.ApplicantOGRN, ps.ApplicantAddress, ps.ApplicantCategory
			,DATE_FORMAT(ps.DateOfApplication, '%d.%m.%Y') DateOfApplication
			,c.id_Court, c.Name, ps.CaseNumber
			,DATE_FORMAT(ps.TimeOfLastChecking, '%d.%m.%Y %H:%i:%s') TimeOfLastChecking
			,DATE_FORMAT(ps.NextSessionDate, '%d.%m.%Y %H:%i') NextSessionDate
			,DATE_FORMAT(ps.DateOfAcceptance, '%d.%m.%Y') DateOfAcceptance
			,DATE_FORMAT(ps.DateOfRequestAct, '%d.%m.%Y') DateOfRequestAct, ps.CourtDecisionURL, ps.CourtDecisionAddInfo
			,DATE_FORMAT(ps.DateOfPrescription, '%d.%m.%Y') DateOfPrescription, ps.PrescriptionURL ,ps.PrescriptionAddInfo
			,concat(m.lastName,' ',m.firstName,' ',m.middleName,' ') ManagerName
			,ps.efrsbPrescriptionNumber, ps.efrsbPrescriptionID, ps.efrsbPrescriptionAddInfo
			,DATE_FORMAT(ps.efrsbPrescriptionPublishDate, '%d.%m.%Y') efrsbPrescriptionPublishDate
			,ps.id_Manager id_Manager, ps.ReadyToRegistrate, ps.ShowToSRO
			,mr.DateOfResponce
			,ps.id_SRO
		from ProcedureStart ps
		inner join Court c on c.id_Court=ps.id_Court
		left join Manager m on m.id_Manager=ps.id_Manager
		left join MRequest mr on mr.id_MRequest=ps.id_MRequest
	";
	$where= 'where ps.id_ProcedureStart=? and (1=0';
	$params= array('s',$id_ProcedureStart);
	if (isset($auth_info->id_Contract))
	{
		$where.= ' or ps.id_Contract=? or m.id_Contract=?';
		$params[]= $auth_info->id_Contract;
		$params[]= $auth_info->id_Contract;
		$params[0].='ss';
	}
	if (isset($auth_info->id_Manager))
	{
		$where.= ' or ps.id_Manager=?';
		$params[]= $auth_info->id_Manager;
		$params[0].='s';
	}
	$where.= ')';
	$rows= execute_query($txt_query.$where,$params);
	if (0==count($rows))
	{
		return null;
	}
	else
	{
		$row= $rows[0];
		$start= array(
			'Заявитель'=>$start_helper->start_row_to_applicant($row)
			, 'Заявление_на_банкротство'=>array(
				'Дата_подачи'=> $row->DateOfApplication
				,'Подано'=>(null!=$row->DateOfApplication)
				,'В_суд'=>(null==$row->id_Court)?null:array('id'=>$row->id_Court,'text'=>$row->Name)
				,'Дата_рассмотрения'=> $row->NextSessionDate
				,'Дата_принятия'=> $row->DateOfAcceptance
			)
			, 'Номер_судебного_дела'=> $row->CaseNumber
			, 'Запрос'=> array(
				"Ссылка"=> $row->CourtDecisionURL
				,'Время'=> array(
					'акта'=> $row->DateOfRequestAct
				)
				,"Дополнительная_информация" => $row->CourtDecisionAddInfo
			)
			, 'Запрос_дополнительная_информация' => array(
				'Дополнительная_информация' => $row->CourtDecisionAddInfo
			)
			, 'id_ProcedureStart'=> $id_ProcedureStart
			, 'Время_сверки'=> $row->TimeOfLastChecking
			, 'АУ'=>(null==$row->id_Manager) ? null : array('id'=>$row->id_Manager, 'text'=>$row->ManagerName)
			, 'Показывать_для_регистрации_в_ПАУ'=>(0!=$row->ReadyToRegistrate)
			, 'Показывать_СРО'=>(0!=$row->ShowToSRO)
			, 'Назначение' => array(
				'Ссылка' => $row->PrescriptionURL
				,'Время' => array(
					'акта' => $row->DateOfPrescription
				)
				,'Дополнительная_информация' => $row->PrescriptionAddInfo
			)
			, 'Назначение_дополнительная_информация' => array(
				'Дополнительная_информация' => $row->PrescriptionAddInfo
			)
			, 'ЕФРСБ' => array(
				'Номер' => $row->efrsbPrescriptionNumber
				,'Дата_публикации' => $row->efrsbPrescriptionPublishDate
				,'ID' => $row->efrsbPrescriptionID
				,'Дополнительная_информация' => $row->efrsbPrescriptionAddInfo
			)
			, 'ЕФРСБ_дополнительная_информация' => array(
				'Дополнительная_информация' => $row->efrsbPrescriptionAddInfo
			)
			, 'isChoosedInRequest'=>(null!=$row->DateOfResponce) && (null!=$row->id_SRO)
		);

		if(isset($row->DebtorName2)) {
			$start['Должник'] = array(
				'Тип' => 'Супруги'
				, 'Супруги' => array(
					'Должник1' => $start_helper->start_row_to_debtor($row)
					, 'Должник2' => $start_helper->start_row_to_debtor($row, 2)
				)
				);
		} else {
			$start['Должник'] = $start_helper->start_row_to_debtor($row);
		}

		return $start;
	}
}

function check_court_decision($start, $key)
{
	if(isset($start->{$key})) {
		$Судебный_акт = (object)$start->{$key};
		if(isset($Судебный_акт->Время)) {
			$время = (object)$Судебный_акт->Время;
			if((isset($время->акта) && ''!==trim($время->акта)) && (!isset($Судебный_акт->Ссылка) || ''==trim($Судебный_акт->Ссылка))) {
				exit_bad_request("can not create/update ProcedureStart: {$key}['Ссылка] required with {$key}['Время']['акта']!");
			}
		}
		if(isset($Судебный_акт->Ссылка) && ''!==trim($Судебный_акт->Ссылка)) {
			if(!isset($Судебный_акт->Время)) {
				exit_bad_request("can not create/update ProcedureStart: {$key}['Время']['акта'] required with {$key}['Ссылка]!");
			} else {
				$время = (object)$Судебный_акт->Время;
				if(!isset($время->акта) || ''==trim($время->акта) && 'null'!==trim($время->акта)) {
					exit_bad_request("can not create/update ProcedureStart: {$key}['Время']['акта'] required with {$key}['Ссылка]!");
				}
			}
		}
	}
}

function check_efrsb($field, $fields)
{
	if(isset($ЕФРСБ->{$field}) && ''!==trim($ЕФРСБ->{$field})) {
		foreach($fields as $key) {
			if(!isset($ЕФРСБ->{$key}) || ''===trim($ЕФРСБ->{$key}) || 'null'===trim($ЕФРСБ->{$key}))
			exit_bad_request("can not {$action} ProcedureStart required ЕФРСБ[{$key}] with ЕФРСБ[{$field}]!");
		}
	}
}

function validate_citizen_debtor($Должник) {
	if (!isset($Должник->Физ_лицо['Фамилия']))
	exit_bad_request("can not create ProcedureStart without DebtorName!");
	if (''==trim($Должник->Физ_лицо['Фамилия']))
		exit_bad_request("can not create ProcedureStart with empty DebtorName!");
}
function validate_start($start)
{
	$start= (object)$start;

	if (!isset($start->Заявление_на_банкротство))
		exit_bad_request("can not create ProcedureStart without Court!");
	$start->Заявление_на_банкротство= (object)$start->Заявление_на_банкротство;
	if (!isset($start->Заявление_на_банкротство->В_суд['id']))
		exit_bad_request("can not create ProcedureStart without Court!");

	if (!isset($start->Должник))
		exit_bad_request("can not create ProcedureStart without Debtor!");
	$start->Должник= (object)$start->Должник;
	$Должник= $start->Должник;
	if (!isset($Должник->Тип))
		exit_bad_request("can not create ProcedureStart without Debtor Category!");
	$Тип= $Должник->Тип;
	switch ($Тип)
	{
		case 'Юр_лицо':
			if (!isset($Должник->Юр_лицо['Наименование']))
				exit_bad_request("can not create ProcedureStart without DebtorName!");
			if (''==trim($Должник->Юр_лицо['Наименование']))
				exit_bad_request("can not create ProcedureStart with empty DebtorName!");
			break;
		case 'Физ_лицо':
			validate_citizen_debtor($Должник);
			break;
		case 'Супруги':
			validate_citizen_debtor((object)$Должник->Супруги['Должник1']);
			validate_citizen_debtor((object)$Должник->Супруги['Должник2']);
			break;
		default:
			exit_bad_request("can not create ProcedureStart with unknown DebtorCategory!");
	}

	check_court_decision($start, 'Запрос');
	check_court_decision($start, 'Назначение');

	if(isset($request->ЕФРСБ)) {
		$ЕФРСБ = (object)$request->ЕФРСБ;
		$consent_helper->check_efrsb('Номер', array('Дата_публикации', 'ID', 'Дополнительная_информация'));
		$consent_helper->check_efrsb('Дата_публикации', array('Номер', 'ID', 'Дополнительная_информация'));
		$consent_helper->check_efrsb('ID', array('Номер', 'Дата_публикации', 'Дополнительная_информация'));
		$consent_helper->check_efrsb('Дополнительная_информация', array('Номер', 'ID', 'Дата_публикации'));
	}

	return $start;
}

