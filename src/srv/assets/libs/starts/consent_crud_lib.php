<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/qb.php';

class consent_helper
{
	function date_time($value)
	{
		if(trim($value)==='' || $value === 'null')
			return null;
		$mask_from= 'd.m.Y';
		$mask_to= 'Y-m-d';
		$date_time_value= date_create_from_format($mask_from,$value);
		return date_format($date_time_value,$mask_to);
	}

	function getValueOrNull($obj, $key)
	{
		if(isset($obj->{$key}) && trim($obj->{$key})!=="") return $obj->{$key};
		return null;
	}

	function debtorToSQLFields($Должник, $num = '') {
		global $new_consent;
		if(isset($Должник->Тип)) {
			switch ($Должник->Тип) {
				case 'Физ_лицо':
					$new_consent->{'DebtorCategory' . $num} = 'n';
					$Физ_лицо =  (isset($Должник->Физ_лицо)) ? (object) $Должник->Физ_лицо : (object) array();
					$DebtorName = $Физ_лицо->Фамилия;
					if (isset($Физ_лицо->Имя))
						$DebtorName .= ' ' . $Физ_лицо->Имя;
					if (isset($Физ_лицо->Отчество))
						$DebtorName .= ' ' . $Физ_лицо->Отчество;
					$new_consent->{'DebtorName' . $num} = $DebtorName;
					$new_consent->{'DebtorSNILS' . $num} = $this->getValueOrNull($Физ_лицо, 'СНИЛС');
					break;
				case 'Юр_лицо':
					$new_consent->DebtorCategory = 'l';
					$new_consent->DebtorName = !isset($Должник->Юр_лицо['Наименование']) ? null : $Должник->Юр_лицо['Наименование'];
					break;
			}
		}

		$new_consent->{'DebtorINN' . $num} = $this->getValueOrNull($Должник, 'ИНН');
		$new_consent->{'DebtorOGRN' . $num} = $this->getValueOrNull($Должник, 'ОГРН');
		$new_consent->{'DebtorAddress' . $num} = $this->getValueOrNull($Должник, 'Адрес');
	}
	
	function consentToSQLFields($consent) {
		global $new_consent;
		if(isset($consent->Должник)){
			$Должник = (object)$consent->Должник;

			if(isset($Должник->Тип)) {
				switch ($Должник->Тип) {
					case 'Физ_лицо':
					case 'Юр_лицо':
						$this->debtorToSQLFields($Должник);
						$this->debtorToSQLFields((object) array(), 2);
						break;
					case 'Супруги':
						$this->debtorToSQLFields((object) $Должник->Супруги['Должник1']);
						$this->debtorToSQLFields((object) $Должник->Супруги['Должник2'], 2);
						break;
				}
			}
		}

		if(isset($consent->Заявитель)){
			$Заявитель = (object)$consent->Заявитель;
			if(isset($Заявитель->Тип)) {
				switch ($Заявитель->Тип) {
					case 'Физ_лицо':
						$new_consent->ApplicantCategory = 'n';
						if(isset($Заявитель->Физ_лицо)) {
							$Физ_лицо= (object)$Заявитель->Физ_лицо;
							$ApplicantName= $Физ_лицо->Фамилия;
							if (isset($Физ_лицо->Имя))
								$ApplicantName.= ' '.$Физ_лицо->Имя;
							if (isset($Физ_лицо->Отчество))
								$ApplicantName.= ' '.$Физ_лицо->Отчество;
							$new_consent->ApplicantName = $ApplicantName;
							$new_consent->ApplicantSNILS = $this->getValueOrNull($Физ_лицо, 'СНИЛС');
						}
						break;
					case 'Юр_лицо':
						$new_consent->ApplicantCategory = 'l';
						$new_consent->ApplicantName = $Заявитель->Юр_лицо['Наименование'];
						break;
				}
			}
			$new_consent->ApplicantINN = $this->getValueOrNull($Заявитель, 'ИНН');
			$new_consent->ApplicantOGRN = $this->getValueOrNull($Заявитель, 'ОГРН');
			$new_consent->ApplicantAddress = $this->getValueOrNull($Заявитель, 'Адрес');
		}
		if(isset($consent->Запрос)){
			$Запрос = (object)$consent->Запрос;
			if(isset($Запрос->Время)) {
				$Время = (object)$Запрос->Время;
				$new_consent->DateOfRequestAct = $this->date_time($Время->акта);
			}
			$new_consent->ApplicantINN = !isset($Заявитель->ИНН) ? null : $Заявитель->ИНН;;
			$new_consent->ApplicantOGRN = !isset($Заявитель->ОГРН) ? null : $Заявитель->ОГРН;
			$new_consent->ApplicantAddress = !isset($Заявитель->Адрес) ? null : $Заявитель->Адрес;
		}
		if(isset($consent->Запрос)){
			$Запрос = (object)$consent->Запрос;
			if(isset($Запрос->Время)) {
				$Время = (object)$Запрос->Время;
				$new_consent->DateOfRequestAct = self::date_time($Время->акта);
			}
	
			if(isset($Запрос->Ссылка))
				$new_consent->CourtDecisionURL = $Запрос->Ссылка;

			$new_consent->CourtDecisionURL = $this->getValueOrNull($Запрос, 'Ссылка');

			$new_consent->CourtDecisionAddInfo = $this->getValueOrNull($Запрос, 'Дополнительная_информация');
		}
		if(isset($consent->Назначение)){
			$Назначение = (object)$consent->Назначение;
			if(isset($Назначение->Время)) {
				$Время = (object)$Назначение->Время;
				$new_consent->DateOfPrescription = $this->date_time($Время->акта);
			}

			$new_consent->PrescriptionURL = $this->getValueOrNull($Назначение, 'Ссылка');
			$new_consent->PrescriptionAddInfo = $this->getValueOrNull($Назначение, 'Дополнительная_информация');
		}
		if(isset($consent->ЕФРСБ)){
			$ЕФРСБ = (object)$consent->ЕФРСБ;
			$new_consent->efrsbPrescriptionNumber = $this->getValueOrNull($ЕФРСБ, 'Номер');
			$new_consent->efrsbPrescriptionPublishDate = $this->date_time($ЕФРСБ->Дата_публикации);
			$new_consent->efrsbPrescriptionID = $this->getValueOrNull($ЕФРСБ, 'ID');
			$new_consent->efrsbPrescriptionAddInfo = $this->getValueOrNull($ЕФРСБ, 'Дополнительная_информация');
		}
		if(isset($consent->Заявление_на_банкротство)){
			$Заявление_на_банкротство = (object)$consent->Заявление_на_банкротство;
			$new_consent->DateOfApplication = !isset($Заявление_на_банкротство->Дата_подачи) ? null : $this->date_time($Заявление_на_банкротство->Дата_подачи);
			if(isset($Заявление_на_банкротство->В_суд)) {
				$В_суд = (object)$Заявление_на_банкротство->В_суд;
				$new_consent->id_Court = $this->getValueOrNull($В_суд, 'id');
			}
			$new_consent->NextSessionDate = (!$this->getValueOrNull($Заявление_на_банкротство, 'Дата_рассмотрения')) 
											? null 
											: date_format(date_create_from_format('d.m.Y H:i',$Заявление_на_банкротство->Дата_рассмотрения),'Y-m-d H:i:00');
			$new_consent->DateOfAcceptance = (!$this->getValueOrNull($Заявление_на_банкротство, 'Дата_принятия'))
											? null: $this->date_time($Заявление_на_банкротство->Дата_принятия);
		}
		$new_consent->CaseNumber = $this->getValueOrNull($consent, 'Номер_судебного_дела');
		$new_consent->id_Manager = (!isset($consent->АУ) || null==$consent->АУ || 'null'==$consent->АУ) ? null : $consent->АУ['id'];
		$new_consent->ReadyToRegistrate = isset($consent->Показывать_для_регистрации_в_ПАУ) && $consent->Показывать_для_регистрации_в_ПАУ && 'false'!=$consent->Показывать_для_регистрации_в_ПАУ && 1;
		$new_consent->TimeOfLastChecking = (!$this->getValueOrNull($consent, 'Время_сверки'))
											? null 
											: date_format(date_create_from_format('d.m.Y H:i:s',$consent->Время_сверки),'Y-m-d H:i:s');

		return array('params'=>'ssssss' . 'ssssss' . 'ssssss' . 'sss' . 'siss' . 'sss' . 'ssss' . 'siis',
			'data'=>array(
				$this->getValueOrNull($new_consent, 'DebtorCategory'),
				$this->getValueOrNull($new_consent, 'DebtorName'),
				$this->getValueOrNull($new_consent, 'DebtorSNILS'),
				$this->getValueOrNull($new_consent, 'DebtorINN'),
				$this->getValueOrNull($new_consent, 'DebtorOGRN'),
				$this->getValueOrNull($new_consent, 'DebtorAddress'),

				$this->getValueOrNull($new_consent, 'DebtorCategory2'),
				$this->getValueOrNull($new_consent, 'DebtorName2'),
				$this->getValueOrNull($new_consent, 'DebtorSNILS2'),
				$this->getValueOrNull($new_consent, 'DebtorINN2'),
				$this->getValueOrNull($new_consent, 'DebtorOGRN2'),
				$this->getValueOrNull($new_consent, 'DebtorAddress2'),

				$this->getValueOrNull($new_consent, 'ApplicantCategory'),
				$this->getValueOrNull($new_consent, 'ApplicantName'),
				$this->getValueOrNull($new_consent, 'ApplicantSNILS'),
				$this->getValueOrNull($new_consent, 'ApplicantINN'),
				$this->getValueOrNull($new_consent, 'ApplicantOGRN'),
				$this->getValueOrNull($new_consent, 'ApplicantAddress'),

				$this->getValueOrNull($new_consent, 'DateOfRequestAct'),
				$this->getValueOrNull($new_consent, 'CourtDecisionURL'),
				$this->getValueOrNull($new_consent, 'CourtDecisionAddInfo'),

				$this->getValueOrNull($new_consent, 'DateOfApplication'),
				$this->getValueOrNull($new_consent, 'id_Court'),
				$this->getValueOrNull($new_consent, 'NextSessionDate'),
				$this->getValueOrNull($new_consent, 'DateOfAcceptance'),
	
				$this->getValueOrNull($new_consent, 'DateOfPrescription'),
				$this->getValueOrNull($new_consent, 'PrescriptionURL'),
				$this->getValueOrNull($new_consent, 'PrescriptionAddInfo'),

				$this->getValueOrNull($new_consent, 'efrsbPrescriptionNumber'),
				$this->getValueOrNull($new_consent, 'efrsbPrescriptionPublishDate'),
				$this->getValueOrNull($new_consent, 'efrsbPrescriptionID'),
				$this->getValueOrNull($new_consent, 'efrsbPrescriptionAddInfo'),
				
				$new_consent->CaseNumber,
				$new_consent->id_Manager,
				$new_consent->ReadyToRegistrate,
				$new_consent->TimeOfLastChecking
			),
			'fields'=>   'DebtorCategory=?, DebtorName=?, DebtorSNILS=?, DebtorINN=?, DebtorOGRN=?, DebtorAddress=?
						, DebtorCategory2=?, DebtorName2=?, DebtorSNILS2=?, DebtorINN2=?, DebtorOGRN2=?, DebtorAddress2=?
						, ApplicantCategory=?, ApplicantName=?, ApplicantSNILS=?, ApplicantINN=?, ApplicantOGRN=?, ApplicantAddress=?
						, DateOfRequestAct=?, CourtDecisionURL=?, CourtDecisionAddInfo=?
						, DateOfApplication=?, id_Court=?, NextSessionDate=?, DateOfAcceptance=?
						, DateOfPrescription=?, PrescriptionURL=?, PrescriptionAddInfo=?
						, efrsbPrescriptionNumber=?, efrsbPrescriptionPublishDate=?, efrsbPrescriptionID=?, efrsbPrescriptionAddInfo=?
						, CaseNumber=?, id_Manager=?, ReadyToRegistrate=?, TimeOfLastChecking=?');
	}

	function consent_row_to_debtor($row, $num='')
	{
		$debtor= (object)array('ИНН'=> $row->{'DebtorINN' . $num}, 'ОГРН'=> $row->{'DebtorOGRN' . $num}, 'Адрес'=> $row->{'DebtorAddress' . $num});
		if ('n'!=$row->{'DebtorCategory' . $num})
		{
			$debtor->Тип= 'Юр_лицо';
			$debtor->Юр_лицо= array('Наименование'=> $row->{'DebtorName' . $num});
		}
		else
		{
			$np= explode(' ',$row->{'DebtorName' . $num});
			$debtor->Тип= 'Физ_лицо';
			$debtor->Физ_лицо= array('Фамилия'=> $np[0], 'Имя'=> $np[1], 'Отчество'=> $np[2], 'СНИЛС'=> $row->{'DebtorSNILS' . $num});
		}
		return $debtor;
	}

	function consent_row_to_applicant($row)
	{
		if(isset($row->ApplicantCategory)) {
			$applicant= (object)array('ИНН'=> $row->ApplicantINN, 'ОГРН'=> $row->ApplicantOGRN, 'Адрес'=> $row->ApplicantAddress);
			if ('n'!=$row->ApplicantCategory)
			{
				$applicant->Тип= 'Юр_лицо';
				$applicant->Юр_лицо= array('Наименование'=> $row->ApplicantName);
			}
			else
			{
				$np= explode(' ',$row->ApplicantName);
				$applicant->Тип= 'Физ_лицо';
				$applicant->Физ_лицо= array('Фамилия'=> $np[0], 'Имя'=> $np[1], 'Отчество'=> $np[2], 'СНИЛС'=> $row->ApplicantSNILS);
			}
			return $applicant;
		}else {
			return null;
		}
	}

	function check_court_decision($request, $key)
	{
		if(isset($request->{$key})) {
			$Судебный_акт = (object)$request->{$key};
			if(isset($Судебный_акт->Время)) {
				$время = (object)$Судебный_акт->Время;
				if((isset($время->акта) && ''!==trim($время->акта) && 'null'!==trim($время->акта)) && (!isset($Судебный_акт->Ссылка) || ''==trim($Судебный_акт->Ссылка))) {
					exit_bad_request("can not create/update ProcedureStart: {$key}['Ссылка'] required with {$key}['Время']['акта']!");
				}
			}
			if(isset($Судебный_акт->Ссылка) && ''!==trim($Судебный_акт->Ссылка)) {
				if(!isset($Судебный_акт->Время)) {
					exit_bad_request("can not create/update ProcedureStart: {$key}['Время']['акта'] required with {$key}['Ссылка]!");
				} else {
					$время = (object)$Судебный_акт->Время;
					if(!isset($время->акта) || ''==trim($время->акта) && 'null'!==trim($время->акта)) {
						exit_bad_request("can not create/update ProcedureStart: {$key}['Время']['акта'] required with {$key}['Ссылка]!");
					}
				}
			}
		}
	}

	function check_efrsb($field, $fields)
	{
		if(isset($ЕФРСБ->{$field}) && ''!==trim($ЕФРСБ->{$field})) {
			foreach($fields as $key) {
				if(!isset($ЕФРСБ->{$key}) || ''===trim($ЕФРСБ->{$key}) || 'null'===trim($ЕФРСБ->{$key}))
				exit_bad_request("can not {$action} ProcedureStart required ЕФРСБ[{$key}] with ЕФРСБ[{$field}]!");
			}
		}
	}

	function nullToEmptyString($v)
	{
		return (is_null($v)) ? '' : $v;
	}
}

global $consent_helper;
$consent_helper = new consent_helper();

function create_au_if_not_isset($ArbitrManagerId) {
	global $auth_info;
	$txt_query= "select m.id_Manager from Manager m where m.ArbitrManagerId = ?";

	$rows= execute_query($txt_query,array('s',$ArbitrManagerId));

	$count= count($rows);
	if (1!=$count)
	{
		$txt_query= '
			insert into Manager
			(id_SRO, firstName, lastName, middleName, efrsbNumber,
			INN, ArbitrManagerID, TimeCreated)
			select
			?, e.FirstName, e.LastName, e.MiddleName, e.RegNum, e.INN, ?, now()
			from efrsb_manager e
			where e.ArbitrManagerID = ?
			';
		$id_Manager = execute_query_get_last_insert_id($txt_query,array('iii',$auth_info->id_SRO,$ArbitrManagerId, $ArbitrManagerId));
		return $id_Manager;
	}
	else{
		return $rows[0]->id_Manager;
	}
}

function create_consent($consent)
{
	global $consent_helper;
	global $auth_info;

	$consent= $consent_helper->consentToSQLFields($consent);

	$txt_query= "insert into ProcedureStart set
				".$consent['fields'].", TimeOfCreate=now(), AddedBySRO=1";
	$params= array($consent['params']);
	$params= array_merge($params, $consent['data']);

	return execute_query_get_last_insert_id($txt_query,$params);
}

function read_consent($id_ProcedureStart) {
	global $consent_helper;
	global $auth_info;

	$txt_query= "select ps.DebtorName ,ps.DebtorSNILS, ps.DebtorINN, ps.DebtorOGRN, ps.DebtorAddress, ps.DebtorCategory
		, ps.DebtorName2 ,ps.DebtorSNILS2, ps.DebtorINN2, ps.DebtorOGRN2, ps.DebtorAddress2, ps.DebtorCategory2
		, ps.ApplicantName ,ps.ApplicantSNILS, ps.ApplicantINN, ps.ApplicantOGRN, ps.ApplicantAddress, ps.ApplicantCategory
		,DATE_FORMAT(ps.DateOfApplication, '%d.%m.%Y') DateOfApplication
		,c.id_Court, c.Name, ps.CaseNumber
		,DATE_FORMAT(ps.TimeOfLastChecking, '%d.%m.%Y %H:%i:%s') TimeOfLastChecking
		,DATE_FORMAT(ps.NextSessionDate, '%d.%m.%Y %H:%i') NextSessionDate
		,DATE_FORMAT(ps.DateOfAcceptance, '%d.%m.%Y') DateOfAcceptance
		,DATE_FORMAT(ps.DateOfRequestAct, '%d.%m.%Y') DateOfRequestAct, ps.CourtDecisionURL, ps.CourtDecisionAddInfo
		,DATE_FORMAT(ps.DateOfPrescription, '%d.%m.%Y') DateOfPrescription, ps.PrescriptionURL ,ps.PrescriptionAddInfo
		,concat(m.lastName,' ',m.firstName,' ',m.middleName,' ') ManagerName
		,ps.efrsbPrescriptionNumber, ps.efrsbPrescriptionID, ps.efrsbPrescriptionAddInfo
		,DATE_FORMAT(ps.efrsbPrescriptionPublishDate, '%d.%m.%Y') efrsbPrescriptionPublishDate
		,ps.id_Manager id_Manager, ps.ReadyToRegistrate
		,m.ArbitrManagerId ArbitrManagerId
	from ProcedureStart ps
	inner join Court c on c.id_Court=ps.id_Court
	left join Manager m on m.id_Manager=ps.id_Manager
	where ps.id_ProcedureStart=? and (m.id_SRO=? and (ps.ShowToSRO!=0 or ps.AddedBySRO=1))
	";
	$params= array('ii',$id_ProcedureStart, $auth_info->id_SRO);
	$rows= execute_query($txt_query,$params);
	if (0==count($rows))
	{
		return null;
	}
	else
	{
		$row= $rows[0];
		$consent= array(
			'Заявитель'=>$consent_helper->consent_row_to_applicant($row)
			, 'Заявление_на_банкротство'=>array(
				'Дата_подачи'=> $row->DateOfApplication
				,'Подано'=>(null!=$row->DateOfApplication)
				,'В_суд'=>(null==$row->id_Court)?null:array('id'=>$row->id_Court,'text'=>$row->Name)
				,'Дата_рассмотрения'=> $row->NextSessionDate
				,'Дата_принятия'=> $row->DateOfAcceptance
			)
			, 'Номер_судебного_дела'=> $row->CaseNumber
			, 'Запрос'=> array(
				'Ссылка' => $row->CourtDecisionURL
				,'Время' => array(
					'акта' => $row->DateOfRequestAct
				)
				,'Дополнительная_информация' => $row->CourtDecisionAddInfo
			)
			, 'Запрос_дополнительная_информация' => array(
				'Дополнительная_информация' => $row->CourtDecisionAddInfo
			)
			, 'id_ProcedureStart'=> $id_ProcedureStart
			, 'Время_сверки'=> $row->TimeOfLastChecking
			, 'АУ'=>(null==$row->ArbitrManagerId) ? null : array('id'=>$row->ArbitrManagerId, 'text'=>$row->ManagerName, 'id_Manager'=>$row->id_Manager)
			, 'Показывать_для_регистрации_в_ПАУ'=>(0!=$row->ReadyToRegistrate)
			, 'Назначение' => array(
				'Ссылка' => $row->PrescriptionURL
				,'Время' => array(
					'акта' => $row->DateOfPrescription
				)
				,'Дополнительная_информация' => $row->PrescriptionAddInfo
			)
			, 'Назначение_дополнительная_информация' => array(
				'Дополнительная_информация' => $row->PrescriptionAddInfo
			)
			, 'ЕФРСБ' => array(
				'Номер' => $row->efrsbPrescriptionNumber
				,'Дата_публикации' => $row->efrsbPrescriptionPublishDate
				,'ID' => $row->efrsbPrescriptionID
				,'Дополнительная_информация' => $row->efrsbPrescriptionAddInfo
			)
			, 'ЕФРСБ_дополнительная_информация' => array(
				'Дополнительная_информация' => $row->efrsbPrescriptionAddInfo
			)
		);

		if(isset($row->DebtorName2)) {
			$consent['Должник'] = array(
				'Тип' => 'Супруги'
				, 'Супруги' => array(
					'Должник1' => $consent_helper->consent_row_to_debtor($row)
					, 'Должник2' => $consent_helper->consent_row_to_debtor($row, 2)
				)
				);
		} else {
			$consent['Должник'] = $consent_helper->consent_row_to_debtor($row);
		}

		return $consent;
	}
}

function update_consent($id_ProcedureStart,$consent) {
	global $consent_helper;
	global $auth_info;

	$consent= $consent_helper->consentToSQLFields((object)$consent);
	$txt_query= "update ProcedureStart ps set ".$consent['fields']."
		where id_ProcedureStart=? and ps.AddedBySRO=1 and exists (
			select m.id_Manager 
			from Manager m 
			where m.id_Manager=ps.id_Manager and m.id_SRO=?
		)";
	$params= array($consent['params'].'ii');
	$params= array_merge($params, $consent['data']);
	$params= array_merge($params, array($id_ProcedureStart,$auth_info->id_SRO));

	return execute_query_get_affected_rows($txt_query,$params);
}

function delete_consent($ids_ProcedureStart) {
	global $consent_helper;
	global $auth_info;

	$result= true;
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	foreach ($ids_ProcedureStart as $id_ProcedureStart)
	{
		$txt_query= "select ps.id_ProcedureStart, mr.id_Manager from ProcedureStart ps
		left join MRequest mr on ps.id_MRequest = mr.id_MRequest
		where ps.id_ProcedureStart=? and ps.AddedBySRO=1 and exists (
			select m.id_Manager 
			from Manager m 
			where m.id_Manager=ps.id_Manager and m.id_SRO=?
		)";
		$params= array('ii',$id_ProcedureStart,$auth_info->id_SRO);
		$rows= execute_query($txt_query,$params);

		if(0==count($rows)){
			$connection->rollback();
			write_to_log("can not delete ProcedureStart where id_ProcedureStart=$id_ProcedureStart (affected 0 rows)");
			return 'no ProcedureStart found for this sro';
		}else if($rows[0]->id_Manager !== null) {
			$connection->rollback();
			write_to_log("can not delete ProcedureStart where id_ProcedureStart=$id_ProcedureStart (affected 0 rows)");
			return 'cant delete ProcedureStart with choosed manager in MRequest';
		}
	}
	foreach ($ids_ProcedureStart as $id_ProcedureStart) {
		$txt_query= "delete ps from ProcedureStart ps
		left join MRequest mr on ps.id_MRequest = mr.id_MRequest
		where ps.id_ProcedureStart=? and mr.id_Manager is null and ps.AddedBySRO=1 and exists (
			select m.id_Manager 
			from Manager m 
			where m.id_Manager=ps.id_Manager and m.id_SRO=?
		)";
		$params= array('ii',$id_ProcedureStart,$auth_info->id_SRO);
		$affected= $connection->execute_query_get_affected_rows($txt_query,$params);
		if (0==$affected)
		{
			$connection->rollback();
			write_to_log("can not delete ProcedureStart where id_ProcedureStart=$id_ProcedureStart (affected $affected rows)");
			return false;
		}
	}
	$connection->commit();
	return true;
}

function validate_citizen_debtor($Должник, $action) {
	if (!isset($Должник->Физ_лицо['Фамилия']))
	exit_bad_request("can not {$action} ProcedureStart without DebtorName!");
	if (''==trim($Должник->Физ_лицо['Фамилия']))
		exit_bad_request("can not {$action} ProcedureStart with empty DebtorName!");
}
function validate_consent($consent, $action) {
	global $consent_helper;

	$consent = (object)$consent;

	if (!isset($consent->Заявление_на_банкротство))
		exit_bad_request("can not {$action} ProcedureStart without Court!");

	$consent->Заявление_на_банкротство = (object)$consent->Заявление_на_банкротство;
	$consent->Заявление_на_банкротство->В_суд = (object)$consent->Заявление_на_банкротство->В_суд;
	if (!isset($consent->Заявление_на_банкротство->В_суд->id))
		exit_bad_request("can not {$action} ProcedureStart without Court!");

	if (!isset($consent->Должник))
		exit_bad_request("can not {$action} ProcedureStart without Debtor!");

	$consent->Должник= (object)$consent->Должник;
	$Должник= $consent->Должник;

	if (!isset($Должник->Тип))
		exit_bad_request("can not {$action} ProcedureStart without Debtor Category!");

	$Тип= $Должник->Тип;
	switch ($Тип)
	{
		case 'Юр_лицо':
			if (!isset($Должник->Юр_лицо['Наименование']))
				exit_bad_request("can not {$action} ProcedureStart without DebtorName!");
			if (''==trim($Должник->Юр_лицо['Наименование']))
				exit_bad_request("can not {$action} ProcedureStart with empty DebtorName!");
			break;
		case 'Физ_лицо':
			validate_citizen_debtor($Должник, $action);
			break;
		case 'Супруги':
			validate_citizen_debtor((object)$Должник->Супруги['Должник1'], $action);
			validate_citizen_debtor((object)$Должник->Супруги['Должник2'], $action);
			break;
		default:
			exit_bad_request("can not {$action} ProcedureStart with unknown DebtorCategory!");
	}

	$consent_helper->check_court_decision($consent, 'Запрос');
	$consent_helper->check_court_decision($consent, 'Назначение');

	if(isset($request->ЕФРСБ)) {
		$ЕФРСБ = (object)$request->ЕФРСБ;
		$consent_helper->check_efrsb('Номер', array('Дата_публикации', 'ID', 'Дополнительная_информация'));
		$consent_helper->check_efrsb('Дата_публикации', array('Номер', 'ID', 'Дополнительная_информация'));
		$consent_helper->check_efrsb('ID', array('Номер', 'Дата_публикации', 'Дополнительная_информация'));
		$consent_helper->check_efrsb('Дополнительная_информация', array('Номер', 'ID', 'Дата_публикации'));
	}

	return $consent;
}