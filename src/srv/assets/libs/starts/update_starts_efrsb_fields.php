<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/job.php';

function new_min_max()
{
	return (object)array('min'=>null,'max'=>null);
}

function update_min_max_sql_time($mm,$sql_time)
{
	$d= date_create_from_format('Y-m-d H:i:s',$sql_time);
	if (null==$mm->max || $d>$mm->max)
		$mm->max= $d;
	if (null==$mm->min || $d<$mm->min)
		$mm->min= $d;
}

function update_min_max_sql_time_for_rows($mm,$rows,$field_name)
{
	foreach ($rows as $row)
	{
		update_min_max_sql_time($mm,$row->$field_name);
	}
}

function Request_ProcedureStart_to_update_from_db($max_portion_size, $start_sql_time)
{
	echo job_time() . " query for $max_portion_size procedure starts after $start_sql_time to update efrsb fields ..\r\n";

	$current_time= job_time();
	$current_time= date_create_from_format('Y-m-d\TH:i:s',$current_time);
	$time_of_create= date_sub($current_time, date_interval_create_from_date_string('60 day')); // неделю.. 168-672 раз..
	$time_of_create= date_format($time_of_create,'Y-m-d H:i:s');

	$txt_query="select
	 id_ProcedureStart
	,DebtorINN, DebtorSNILS, DebtorOGRN
	,if(DateOfApplication is null,TimeOfCreate,DateOfApplication) DateOfApplication
	,TimeOfLastCheckingEFRSB
	from ProcedureStart p
	where p.TimeOfCreate > ?
	and (DebtorINN is not null and ''<>DebtorINN and not (DebtorINN regexp '[^0-9]') or 
	     DebtorSNILS is not null and ''<>DebtorSNILS and not (DebtorSNILS regexp '[^0-9]') or 
	     DebtorOGRN is not null and ''<>DebtorOGRN and not (DebtorOGRN regexp '[^0-9]'))
	and ((p.efrsbPrescriptionPublishDate is null)
	  or (p.efrsbPrescriptionNumber is null or ''=p.efrsbPrescriptionNumber)
	  or (p.efrsbPrescriptionID is null or ''=p.efrsbPrescriptionID))
	and (p.TimeOfLastCheckingEFRSB is null or p.TimeOfLastCheckingEFRSB < ? )
	order by ifnull(p.TimeOfLastCheckingEFRSB,'2000-01-01'), p.TimeOfCreate
	limit $max_portion_size
	";
	$rows= execute_query($txt_query,array('ss',$time_of_create,$start_sql_time));
	$cases= array();
	$mm= new_min_max();
	foreach ($rows as $row)
	{
		update_min_max_sql_time($mm,$row->TimeOfLastCheckingEFRSB);
		$case= (object)array('DateOfApplication'=>$row->DateOfApplication);
		if (null!=$row->DebtorINN && ''!=$row->DebtorINN)
			$case->INN= $row->DebtorINN;
		if (null!=$row->DebtorOGRN && ''!=$row->DebtorOGRN)
			$case->OGRN= $row->DebtorOGRN;
		if (null!=$row->DebtorSNILS && ''!=$row->DebtorSNILS)
			$case->SNILS= $row->DebtorSNILS;
		$cases[$row->id_ProcedureStart]= $case;
	}
	$count_cases= count($cases);
	if (0==$count_cases)
	{
		echo job_time() . " .. got $count_cases starts to update efrsb fields\r\n";
	}
	else
	{
		$min= null==$mm->min ? 'null' : date_format($mm->min,'d.m.Y H:i:s');
		$max= null==$mm->max ? 'null' : date_format($mm->max,'d.m.Y H:i:s');
		echo job_time() . " .. got $count_cases starts (checked $min - $max) to update efrsb fields\r\n";
	}
	return $cases;
}

function update_procedure_start_efrsb_fields($id_ProcedureStart,$message)
{
	$current_time= job_time();
	$current_time= date_create_from_format('Y-m-d\TH:i:s',$current_time);
	$current_time= date_format($current_time,'Y-m-d H:i:s');

	$txt_query= 'update ProcedureStart set 
		efrsbPrescriptionPublishDate = ?, efrsbPrescriptionNumber = ?, efrsbPrescriptionID = ?, TimeOfLastCheckingEFRSB=?
		where id_ProcedureStart = ?';
	execute_query_no_result($txt_query,
		array('sssss',$message->PublishDate,$message->Number,$message->MessageGUID,$current_time,$id_ProcedureStart));
}

function update_procedure_start_efrsb_verification_time($id_ProcedureStart)
{
	$current_time= job_time();
	$current_time= date_create_from_format('Y-m-d\TH:i:s',$current_time);
	$current_time= date_format($current_time,'Y-m-d H:i:s');

	$txt_query= 'update ProcedureStart set TimeOfLastCheckingEFRSB=? where id_ProcedureStart = ?';
	$affected_rows= execute_query_get_affected_rows($txt_query,array('ss',$current_time,$id_ProcedureStart));
}

function Request_MessagesInfo_from_efrsb($cases)
{
	$postfields = json_encode($cases);

	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/dm-api.php?action=start-info';

	echo job_time() . " request for efrsb messages $url ..\r\n";

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL,$url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);

	$result= curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request events for start info (code $httpcode for url \"$url\")");

	$messages_from_efrsb= ('{}'==$result) ? array() : json_decode($result,/*associative=*/true);
	if (null===$messages_from_efrsb)
	{
		echo job_time() . " result: \"$result\"\r\n";
		throw new Exception("can not parse responce of events for start info (for url \"$url\")");
	}
	$messages_from_efrsb_count = count($messages_from_efrsb);

	echo job_time() . " .. got $messages_from_efrsb_count efrsb messages to update procedure starts\r\n";

	return $messages_from_efrsb;
}

function update_starts_efrsb_fields($max_to_update= 100)
{
	$start_sql_time= date_format(date_create_from_format('Y-m-d\TH:i:s',job_time()),'Y-m-d H:i:s');
	$checked= 0;
	$total_updated= 0;
	$mm= new_min_max();
	while ($checked < 1000)
	{
		$cases= Request_ProcedureStart_to_update_from_db($max_to_update, $start_sql_time);
		update_min_max_sql_time_for_rows($mm,$cases,'DateOfApplication');
		$count_cases= count($cases);
		if (0==$count_cases)
		{
			break;
		}
		else
		{
			$checked+= $count_cases;
			$messages_from_efrsb= Request_MessagesInfo_from_efrsb($cases);
			$count_updated=0;
			if (0==count($messages_from_efrsb))
			{
				echo job_time() . " did not find efrsb messages!\r\n";
			}
			else
			{
				foreach ($messages_from_efrsb as $id_ProcedureStart => $message)
				{
					update_procedure_start_efrsb_fields($id_ProcedureStart,(object)$message);
					unset($cases[$id_ProcedureStart]);
					$count_updated++;
				}
			}
			foreach ($cases as $id_ProcedureStart => $case)
			{
				update_procedure_start_efrsb_verification_time($id_ProcedureStart);
			}
			echo job_time() . " updated $count_updated procedures\r\n";
			$total_updated+= $count_updated;
		}
	}
	$min= date_format($mm->min,'d.m.Y');
	$max= date_format($mm->max,'d.m.Y');
	return "проверены выдвижения ($min - $max): $checked,\r\n получена информация о сообщениях ЕФРСБ для: $total_updated";
}