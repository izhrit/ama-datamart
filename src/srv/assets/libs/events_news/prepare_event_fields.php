<?php
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/sax.php';
function message_short_time($time)
{
	return substr($time, 0, strpos($time, "."));
}

class Event_fields_parser extends AFields_parser_named_combination
{
	private function prepare_named_parsers($message_type)
	{
		$this->message_type= $message_type;

		switch ($message_type)
		{
			case 'a': // ArbitralDecree
				return array(
					'DecisionDate_parser' => new Field_parser(array('MessageData','MessageInfo', 'CourtDecision', 'NextCourtSessionDate'))
					,'ProcedureProlongationNumber_parser' => new Field_parser(array('MessageData','MessageInfo', 'CourtDecision', 'ProcedureProlongation', 'MessageNumber'))
					,'ProcedureProlongationDate_parser' => new Field_parser(array('MessageData','MessageInfo', 'CourtDecision', 'ProcedureProlongation', 'Date'))
					,'ProcedureProlongationMonths_parser' => new Field_parser(array('MessageData','MessageInfo', 'CourtDecision', 'ProcedureProlongation', 'Months'))
					,'CourtDecisionDate' => new Field_parser(array('MessageData','MessageInfo', 'CourtDecision', 'CourtDecree', 'DecisionDate'))
				);
			case 'b': // Auction
				return array(
					'DecisionDate_parser' => new Field_parser(array('MessageData','MessageInfo', 'Auction', 'Date'))
					,'DecisionDate_parser_begin' => new Field_parser(array('MessageData','MessageInfo', 'Auction', 'Application', 'TimeBegin'))
				);
			case 'c': // Meeting
				return array(
					'DecisionDate_parser'=> new Field_parser(array('MessageData','MessageInfo', 'Meeting', 'MeetingDate'))
					,'DecisionDate_parser_begin'=> new Field_parser(array('MessageData','MessageInfo', 'Meeting', 'MeetingDateBegin'))
					,'DecisionDate_parser_time'=> new Field_parser(array('MessageData','MessageInfo', 'Meeting', 'MeetingTimeBegin'))
				);
			case 'q': // Committee
				return array('DecisionDate_parser'=> new Field_parser(array('MessageData','MessageInfo', 'Committee', 'MeetingDate')));
			case '6': // MeetingParticipantsBuilding
				return array('DecisionDate_parser'=> new Field_parser(array('MessageData','MessageInfo', 'MeetingParticipantsBuilding', 'MeetingDate')));
			case 'о': // MeetingWorker
				return array(
					'DecisionDate_parser' => new Field_parser(array('MessageData','MessageInfo', 'MeetingWorker', 'MeetingDate'))
					,'BallotsReceptionEndDate_parser' => new Field_parser(array('MessageData','MessageInfo', 'MeetingWorker', 'BallotsReceptionEndDate'))
				);
			default:
				return array();
		}
	}

	public function __construct($message_type)
	{
		parent::__construct($this->prepare_named_parsers($message_type));
	}

	public function value()
	{
		$p= $this->named_parsers;
		$fields = array();

		$fields['prolongation_number'] = null;
		$fields['event_time'] = null;

		if (!empty($p->DecisionDate_parser->value))
		{
			$fields['event_time'] = $p->DecisionDate_parser->value;
		}else {
			switch ($this->message_type)
				{
				case 'b': /* Auction */ 
					$fields['event_time'] = $p->DecisionDate_parser_begin->value;
					break;
				case 'c': /* Meeting */
					$meeting_time= message_short_time($p->DecisionDate_parser_time->value);
					$fields['event_time'] = (''==$meeting_time) ? $p->DecisionDate_parser_begin->value : $p->DecisionDate_parser_begin->value."T".$meeting_time;
					break;
				case 'о': /* MeetingWorker */ 
					$fields['event_time'] = $p->BallotsReceptionEndDate_parser->value;
					break;
			}
		}

		//ProcedureProlongation
		if (!empty($p->ProcedureProlongationNumber_parser->value))
		{
			$fields['prolongation_number'] = $p->ProcedureProlongationNumber_parser->value;

			if(!empty($p->ProcedureProlongationDate_parser->value)){
				$fields['event_time'] = $p->ProcedureProlongationDate_parser->value;
			}
			else if(!empty($p->ProcedureProlongationMonths_parser->value) && !empty($p->CourtDecisionDate->value)){
				
				$court_decision_date = $p->CourtDecisionDate->value;
				$prolongation_months = $p->ProcedureProlongationMonths_parser->value;

				$date_at = strtotime("+{$prolongation_months} MONTH", strtotime($court_decision_date));
				$new_date = date('Y-m-d', $date_at);

				$fields['event_time'] = $new_date;

			}else {
				$fields['event_time'] = null;
			}
		}
		
		return $fields;
	}
}

function get_event_fields_parser_by_message_type($message_type)
{
	return new Event_fields_parser($message_type);
}

function get_event_fields($msg, $message_type)
{
	$event_fields_parser= get_event_fields_parser_by_message_type($message_type);

	try
	{
		parse_fields($msg,$event_fields_parser->get_parsers());
	}
	catch (Exception $ex)
	{
		write_to_log('Unhandled exception occurred during parsing message for fields: ' . get_class($ex) . ' - ' . $ex->getMessage());
		write_to_log('Body:');
		write_to_log($msg);
		return null;
	}
	return $event_fields_parser->value();
}
