<?php
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/sax.php';
require_once '../assets/libs/efrsb.php';
require_once '../assets/libs/time.php';


function parse_event_time($msg, $message_type)
{

	$ArbitralDecree = false;
	$Auction = false;
	$Meeting = false;
	$Committee = false;
	$MeetingParticipantsBuilding = false;
	$MeetingWorker = false;

	switch ($message_type) {
		case 'a': //ArbitralDecree
			$ArbitralDecree = true;
			$DecisionDate_parser = new Field_parser(array('MessageData','MessageInfo', 'CourtDecision', 'CourtDecree', 'DecisionDate'));
			break;
		case 'b'://Auction
			$Auction = true;
			$DecisionDate_parser = new Field_parser(array('MessageData','MessageInfo', 'Auction', 'Date'));
			$DecisionDate_parser_begin = new Field_parser(array('MessageData','MessageInfo', 'Auction', 'Application', 'TimeBegin'));
			break;

		case 'c': //Meeting
			$Meeting = true;
			$DecisionDate_parser = new Field_parser(array('MessageData','MessageInfo', 'Meeting', 'MeetingDate'));
			$DecisionDate_parser_begin = new Field_parser(array('MessageData','MessageInfo', 'Meeting', 'MeetingDateBegin'));
		    $DecisionDate_parser_time = new Field_parser(array('MessageData','MessageInfo', 'Meeting', 'MeetingTimeBegin'));
			break;
		case 'q': //Committee
			$Committee = true;
			$DecisionDate_parser = new Field_parser(array('MessageData','MessageInfo', 'Committee', 'MeetingDate'));
			break;

		case '6': //MeetingParticipantsBuilding
			$MeetingParticipantsBuilding = true;
			$DecisionDate_parser = new Field_parser(array('MessageData','MessageInfo', 'MeetingParticipantsBuilding', 'MeetingDate'));
			break;

		case 'о': //MeetingWorker
			$MeetingWorker = true;
			$DecisionDate_parser = new Field_parser(array('MessageData','MessageInfo', 'MeetingWorker', 'MeetingDate'));
			$DecisionDate_publish_parser = new Field_parser(array('MessageData','PublishDate'));
			break;
		default:
			$DecisionDate_parser = new Field_parser(array('MessageData','PublishDate'));
		break;
			
	}


	$msg_parser = new XMLParserArray(
		array($DecisionDate_parser)
	);

	if($Auction) {
		$msg_parser = new XMLParserArray(
			array($DecisionDate_parser, $DecisionDate_parser_begin)
		);
	}
	if($Meeting) {
		$msg_parser = new XMLParserArray(
			array($DecisionDate_parser, $DecisionDate_parser_begin, $DecisionDate_parser_time)
		);
	}

	if($MeetingWorker) {
		$msg_parser = new XMLParserArray(
			array($DecisionDate_parser, $DecisionDate_publish_parser)
		);
	}

	$xml_parser = xml_parser_create();
	$msg_parser->bind($xml_parser);
	if (!xml_parse($xml_parser, $msg, /* is_final= */TRUE))
	{
		$ex_text= sprintf("Ошибка XML: %s на строке %d",
						xml_error_string(xml_get_error_code($xml_parser)),
						xml_get_current_line_number($xml_parser));
		xml_parser_free($xml_parser);
		throw new Exception($ex_text);
	}
	xml_parser_free($xml_parser);

	$parsed_msg= (object)array();
	$parsed_msg->DecisionDate= $DecisionDate_parser->value;

	//Дополнительные условия
	switch ($message_type) {
		case 'a': //ArbitralDecree
			break;

		case 'b'://Auction
			if(empty($DecisionDate_parser->value)) {
				$parsed_msg->DecisionDate = $DecisionDate_parser_begin->value;
			}
			break;

		case 'c': //Meeting
			if(empty($DecisionDate_parser->value)) {
				$parsed_msg->DecisionDate = $DecisionDate_parser_begin->value."T".message_short_time($DecisionDate_parser_time->value);
			}
			break;
		case 'q': //Committee
			break;

		case '6': //MeetingParticipantsBuilding
			break;

		case 'о': //MeetingWorker
			if(empty($DecisionDate_parser->value)) {
				$parsed_msg->DecisionDate = message_short_time($DecisionDate_publish_parser->value);
			}
			break;
			
	}

	return $parsed_msg->DecisionDate;
}
