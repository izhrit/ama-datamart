<?

function fix_news_row($row)
{
	$row->time= $row->PublishDate;
	unset($row->PublishDate);

	$row->debtor= $row->Debtor;
	unset($row->Debtor);

	$row->brief= $row->Brief;
	unset($row->Brief);

	$row->type= $row->MessageType;
	unset($row->MessageType);
}

function fix_news_rows($efrsb_news)
{
	if (!isset($efrsb_news->rows) || !is_array($efrsb_news->rows))
		throw new Exception('bad format ot news!');
	foreach ($efrsb_news->rows as $row)
		fix_news_row($row);
}
