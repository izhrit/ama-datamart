<?

function collect_ids($rows,$id_name)
{
	$ids= "$id_name=";
	$count= 0;
	foreach ($rows as $row) 
	{
		if (null != $row->$id_name)
		{
			if (0!=$count)
				$ids.= ',';
			$ids.= $row->$id_name;
			$count++;
		}
	}
	return 0==$count ? null : $ids;
}

function prepare_manager_debtor_ids_url_argument($auth_info)
{
	switch ($auth_info->category)
	{
		case 'customer':
			$rows= execute_query("SELECT ArbitrManagerID FROM Manager where id_Contract=? AND ArbitrManagerID is not null;", 
								array("s", $auth_info->id_Contract));
			return 0==count($rows) ? null : collect_ids($rows,'ArbitrManagerID');

		case 'manager':
			return !isset($auth_info->ArbitrManagerID) || null==$auth_info->ArbitrManagerID 
					? null : "ArbitrManagerID=$auth_info->ArbitrManagerID";

		case 'viewer':
			$txt_query= "
				select d.BankruptId 
				from MUser u
					inner join ManagerUser mu on u.id_MUser=mu.id_MUser
					inner join MProcedureUser mpu on mpu.id_ManagerUser=mu.id_ManagerUser
					inner join MProcedure mp on mp.id_MProcedure=mpu.id_MProcedure
					inner join Debtor d on d.id_Debtor=mp.id_Debtor
				where u.id_MUser=?
				union
				select d.BankruptId 
				from MUser u
					inner join Request re on re.id_MUser=u.id_Muser
					inner join Debtor d on d.id_Debtor=re.id_Debtor
				where u.id_MUser=?;";
			$rows= execute_query($txt_query, array("ss", $auth_info->id_MUser, $auth_info->id_MUser));
			return 0==count($rows) ? null : collect_ids($rows,'BankruptId');
	}
}
