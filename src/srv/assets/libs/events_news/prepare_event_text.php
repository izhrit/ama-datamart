<?php

require_once '../assets/libs/events_news/debtor.php';
require_once '../assets/libs/message/MessageType.php';

global $event_type_descr, $MessageType_desciptions;
$MessageType_desciptions = $MessageInfo_MessageType_desciptions;
$event_type_descr= array(
	'a'=> array('prefix'=>'Суд: ', 'title'=>'судебное заседание', 'title_message'=>'о судебном акте', 'title_app'=>'Судебное заседание')
	,'b'=> array('prefix'=>'Торги: ', 'title'=>'торги', 'title_message'=>'о проведении торгов', 'title_app'=>'Торги')
	,'q'=> array('prefix'=>'КК: ', 'title'=>'заседание комитета кредиторов', 'title_message'=>'о заседании комитета кредиторов', 'title_app'=>'Комитет кредиторов')
	,'c'=> array('prefix'=>'СК: ', 'title'=>'собрание кредиторов', 'title_message'=>'о собрании кредиторов', 'title_app'=>'Собрание кредиторов')
	,'6'=> array('prefix'=>'СД: ', 'title'=>'собрание участников строительства (дольщиков)', 'title_message'=>'о собрании участников строительства (дольщиков)', 'title_app'=>'Собрание дольщиков')
	,'о'=> array('prefix'=>'СР: ', 'title'=>'собрание работников и бывших работников', 'title_message'=>'о собрании работников и бывших работников', 'title_app'=>'Собрание работников')
);

global $days_of_week, $days_of_week_single, $monthes;
$days_of_week   = array('в воскресенье','в понедельник','во вторник','в среду','в четверг','в пятницу','в субботу');
$days_of_week_single = array('воскресенье','понедельник','вторник','среда','четверг','пятница','суббота');
$monthes= array('','января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря');

function get_type_description_for_message($type) {
	global $MessageType_desciptions;

	foreach ($MessageType_desciptions as $value) {
		if($value['db_value'] == $type) {
			return $value['Readable_short_about'];
		}
	}
}

function prepare_event_text($event,$event_res)
{
	global $event_type_descr;

	$description = null;
	$title= null;

	if (isset($event->номер_продления))
	{
		$title = "Завершение: ";

		if (isset($event->должник_фио))
		{
			$title.= prepare_fio($event->должник_фио);
		}
		else if (isset($event->должник))
		{
			$title.= prepare_debtor($event->должник);
		}else {
			$title.= '?';
		}

		$description= "Для ";

		if (isset($event->должник_фио))
		{
			$description.= $event->должник_фио;
		}
		else if (isset($event->должник))
		{
			$description.= $event->должник;
		}

		$date_parts= explode('.',$event->time);
		$etime= $date_parts[0];
		if (false==mb_strpos($etime,':'))
			$etime.= ' 00:00:00';
		$date= date_create_from_format('Y-m-d\TH:i:s',$etime);
		if (false===$date)
			$date= date_create_from_format('Y-m-d H:i:s',$etime);

		if (false!==$date)
		{
			global $days_of_week;
			$date_d_txt= date_format($date, 'd.m.Y');
			$description.= "\r\nсрок процедуры завершается $date_d_txt";
		}

		$event_res->title= $title;
		$event_res->description= $description;
		return;
	}
	

	$title= !isset($event_type_descr[$event->type]) ? 'событие: ' : $event_type_descr[$event->type]['prefix'];

	if (isset($event->должник_фио))
	{
		$title.= prepare_fio($event->должник_фио);
	}
	else if (isset($event->должник))
	{
		$title.= prepare_debtor($event->должник);
	}
	else
	{
		$title.= '?';
	}

	$description= !isset($event_type_descr[$event->type]) ? 'событие' : $event_type_descr[$event->type]['title'];
	$description.= ' ';

	$date_parts= explode('.',$event->time);
	$etime= $date_parts[0];
	if (false==mb_strpos($etime,':'))
		$etime.= ' 00:00:00';
	$date= date_create_from_format('Y-m-d\TH:i:s',$etime);
	if (false===$date)
		$date= date_create_from_format('Y-m-d H:i:s',$etime);

	if (false!==$date)
	{
		global $days_of_week, $monthes;
		$description.= $days_of_week[date_format($date, 'w')];
		$description.= ' ' . date_format($date, 'j');
		$description.= ' ' . $monthes[date_format($date, 'n')];
		$t= date_format($date, 'H:i');
		if ('00:00'!=$t)
			$description.= ' в ' . $t;
	}
		

	if (isset($event->должник_фио))
	{
		$description.= "\r\nдолжник: ".$event->должник_фио;
	}
	else if (isset($event->должник))
	{
		$description.= "\r\nдолжник: ".$event->должник;
	}

	if (isset($event->где))
		$description.= "\r\nместо проведения: {$event->где}";
	if (isset($event->форма))
		$description.= "\r\nформа: {$event->форма}";

	$description.= "\r\n(из объявления ефрсб № $event->number)";

	$event_res->title= $title;
	$event_res->description= $description;
}


function prepare_event_email_text($event,$event_res)
{
	global $event_type_descr;
	$description = null;
	$title= null;

	if (isset($event->номер_продления))
	{
		$title = "Напоминаем: ";

		
		$date_parts= explode('.',$event->time);
		$etime= $date_parts[0];
		if (false==mb_strpos($etime,':'))
			$etime.= ' 00:00:00';
			$date= date_create_from_format('Y-m-d\TH:i:s',$etime);
		if (false===$date)
			$date= date_create_from_format('Y-m-d H:i:s',$etime);

		if (false!==$date)
		{
			global $days_of_week;
			$date_d_txt= date_format($date, 'd.m.Y');
			$title.= $date_d_txt;

			$description= $date_d_txt;
			
			$description.= ' ('.$days_of_week[date_format($date, 'w')].')';
		}

		$title.= " завершение процедуры ";
		
		if (isset($event->должник_фио))
		{
			$title.= "для ".prepare_fio($event->должник_фио);
		}
		else if (isset($event->должник))
		{
			$title.= "для ".prepare_debtor($event->должник);
		}else {
			$title.= '?';
		}


		$description.= " завершается срок продления процедуры для ";

		if (isset($event->должник_фио))
		{
			$description.= $event->должник_фио;
		}
		else if (isset($event->должник))
		{
			$description.= $event->должник;
		}

		$event_res->title= $title;
		$event_res->description= $description;
		return;
	}

	if (!isset($event_type_descr[$event->type]))
	{
		$title= 'Событие';
	}
	else
	{
		$pparts= explode(':',$event_type_descr[$event->type]['prefix']);
		$title= 'Напоминаем: '.$pparts[0];
	}

	$date_parts= explode('.',$event->time);
	$etime= $date_parts[0];
	if (false==mb_strpos($etime,':'))
		$etime.= ' 00:00:00';
	$date= date_create_from_format('Y-m-d\TH:i:s',$etime);
	if (false===$date)
		$date= date_create_from_format('Y-m-d H:i:s',$etime);

	if (false!==$date)
	{
		global $days_of_week;
		$date_d_txt= date_format($date, 'd.m.Y');
		$date_t_txt= date_format($date, 'H:i');
		$title.= ' ' . $date_d_txt;
		if ('00:00'!=$date_t_txt)
			$title.= ' ' . $date_t_txt;

		$description = $date_d_txt;
		$description.= ' ('.$days_of_week[date_format($date, 'w')].')';
		if($date_t_txt != '00:00')
			$description.= ' в '.$date_t_txt;
	}

	if (isset($event->должник_фио))
	{
		$title.= ' для '.prepare_fio($event->должник_фио);
	}
	else if (isset($event->должник))
	{
		$title.= ' для '.prepare_debtor($event->должник);
	}

	if (isset($event_type_descr[$event->type]['title']))
			$description.= " запланировано\r\n". $event_type_descr[$event->type]['title'];

	if (isset($event->должник_фио))
	{
		$description.= " в процедуре\r\n".$event->должник_фио;
	}
	else if (isset($event->должник))
	{
		$description.= " в процедуре\r\n".$event->должник;
	}
	
	if (isset($event->где))
		$description.= "\r\nпо адресу {$event->где}";
	
	$event_res->title= $title;
	$event_res->description= $description;
}

function prepare_event_email_for_message($event,$event_res)
{
	global $event_type_descr;

	$description = null;
	$title= null;

	if (isset($event->номер_продления))
	{
		if (empty($event_type_descr[$event->type]))
		{
			$title= 'Событие';
		}
		else
		{
			$title = 'Новое на ЕФРСБ: ';
			$description = "На ЕФРСБ опубликовано для\r\n\r\n";
			if (isset($event->должник_фио))
			{
				$title.= prepare_fio($event->должник_фио);
				$description.= $event->должник_фио."\r\nОбъявление о продлении процедуры";
			}
			else if (isset($event->должник))
			{
				$title.= $event->должник;
				$description.= $event->должник."\r\nОбъявление о продлении процедуры";
			}

			$title.= ' - продление процедуры';
		
		}

		$event_res->title= $title;
		$event_res->description= $description;
		return;
	}

	$title = 'Новое на ЕФРСБ: ';
	$description = "На ЕФРСБ опубликовано для\r\n\r\n";
	$title_message = get_type_description_for_message($event->type);

	if (isset($event->должник_фио))
	{
		$title.= prepare_fio($event->должник_фио);
		$description.= $event->должник_фио."\r\nОбъявление ".$title_message;
	}
	else if (isset($event->должник))
	{
		$title.= $event->должник;
		$description.= $event->должник."\r\nОбъявление ".$title_message;
	}

	if(isset($title_message)) {
		$title.= ' - '.$title_message;
	}	

	$event_res->title= $title;
	$event_res->description= $description;
}


function prepare_event_app_text($event,$event_res)
{
	global $event_type_descr;
	
	$description = null;
	$title= null;

	if (isset($event->номер_продления))
	{
		if (!isset($event_type_descr[$event->type]))
		{
			$title= 'Событие';
		}
		else
		{
			$title= "Завершение процедуры, ";
		}
		$description = "Завершение продления процедуры";

		$date_parts= explode('.',$event->time);
		$etime= $date_parts[0];
		if (false==mb_strpos($etime,':'))
			$etime.= ' 00:00:00';
		$date= date_create_from_format('Y-m-d\TH:i:s',$etime);
		if (false===$date)
			$date= date_create_from_format('Y-m-d H:i:s',$etime);

		if (false!==$date)
		{
	
			global $days_of_week, $days_of_week_single, $monthes;

			$date_d_txt= date_format($date, 'd.m.Y');

			$title.= $date_d_txt;

			$days_of_week_single_res = $days_of_week_single[date_format($date, 'w')];
			$title.= ' ('.$days_of_week_single_res.')';

			$description.= ' '.$days_of_week[date_format($date, 'w')];
			$description.= ' ' . date_format($date, 'j');
			$description.= ' ' . $monthes[date_format($date, 'n')];
			$t= date_format($date, 'H:i');
			if ('00:00'!=$t)
				$description.= ' в ' . $t;

			$title .= ', ';
		
		}
	
		if (isset($event->должник_фио))
		{
		
			$title.= prepare_fio($event->должник_фио);
			$description .= "\r\nдолжник:".$event->должник_фио;
		}
		else if (isset($event->должник))
		{
			$title.= prepare_debtor($event->должник);
			$description .= "\r\nдолжник: ".$event->должник;
		}

		if (isset($event->где))
			$description.= "\r\nместо проведения: {$event->где}";

		if (isset($event->number))
			$description.= "\r\n(из объявления ефрсб № {$event->number})";
		$event_res->title= $title;
		$event_res->description= $description;
		return;
	}

	$date_parts= explode('.',$event->time);
	$etime= $date_parts[0];
	if (false==mb_strpos($etime,':'))
		$etime.= ' 00:00:00';
	$date= date_create_from_format('Y-m-d\TH:i:s',$etime);
	if (false===$date)
		$date= date_create_from_format('Y-m-d H:i:s',$etime);

	$title= null;
	if (!isset($event_type_descr[$event->type]))
	{
		$title= 'Событие';
	}
	else
	{
		$pparts= explode(':',$event_type_descr[$event->type]['prefix']);
		$title= $pparts[0].", ";

		$description = $event_type_descr[$event->type]['title_app'];
	}



	

	if (false!==$date)
	{
	
		global $days_of_week, $days_of_week_single, $monthes;
		

		$date_d_txt= date_format($date, 'd.m.Y');
		$date_t_txt= date_format($date, 'H:i');

		$title.= $date_d_txt;
		
		if($date_t_txt != '00:00')
			$title.= ' в '.$date_t_txt;

		$days_of_week_single_res = $days_of_week_single[date_format($date, 'w')];
		$title.= ' ('.$days_of_week_single_res.')';

		$description.= ' '.$days_of_week[date_format($date, 'w')];
		$description.= ' ' . date_format($date, 'j');
		$description.= ' ' . $monthes[date_format($date, 'n')];
		$t= date_format($date, 'H:i');
		if ('00:00'!=$t)
			$description.= ' в ' . $t;

		$title .= ', ';
		
	}
	
	if (isset($event->должник_фио))
	{
		
		$title.= prepare_fio($event->должник_фио);
		$description .= " \r\nдолжник:".$event->должник_фио;
	}
	else if (isset($event->должник))
	{
		$title.= prepare_debtor($event->должник);
		$description .= " \r\nдолжник: ".$event->должник;
	}

	if (isset($event->где))
		$description.= " \r\nместо проведения: {$event->где}";

	if (isset($event->number))
		$description.= " \r\n(из объявления ефрсб № {$event->number})";
	
	
	$event_res->title= $title;
	$event_res->description= $description;

}

function prepare_event_app_text_for_message($event,$event_res)
{
	global $event_type_descr;
	$description = null;
	$title= null;
	if (isset($event->номер_продления))
	{
		if (empty($event_type_descr[$event->type]))
		{
			$title= 'Событие';
		}
		else
		{
			$title = 'Новое на ЕФРСБ: ';
			$description = "в процедуре над ";
			if (isset($event->должник_фио))
			{
				$title.= prepare_fio($event->должник_фио);
				$description.= $event->должник_фио;
				$description.= "\r\n на ЕФРСБ опубликовано\r\nОбъявление о продлении процедуры";
			}
			else if (isset($event->должник))
			{
				$title.= prepare_debtor($event->должник);
				$description.= $event->должник;
				$description.= "\r\n на ЕФРСБ опубликовано\r\nОбъявление о продлении процедуры";
			}

			$title.= ' - продление процедуры';
		
		}

		$event_res->title= $title;
		$event_res->description= $description;
		return;
	}
	$title_message = get_type_description_for_message($event->type);
	if (empty($title_message))
	{
		$title= 'Событие';
	}
	else
	{
		$title = 'Новое на ЕФРСБ: ';
		$description = "в процедуре над ";
		if (isset($event->должник_фио))
		{
			$title.= prepare_fio($event->должник_фио);
			$description.= $event->должник_фио;
			$description.= "\r\n на ЕФРСБ опубликовано \r\nОбъявление ".$title_message;
		}
		else if (isset($event->должник))
		{
			$title.= prepare_debtor($event->должник);
			$description.= $event->должник;
			$description.= "\r\n на ЕФРСБ опубликовано \r\nОбъявление ".$title_message;
		}

		$title.= ' '.$title_message;
		
	}

	$event_res->title= $title;
	$event_res->description= $description;
}

function prepare_event_structured_fields($event,$event_res)
{
	global $event_type_descr;

	$event_res->event=  !isset($event_type_descr[$event->type]) ? 'событие' : $event_type_descr[$event->type]['title'];

	$event_res->efrsb_number= $event->number;

	if (isset($event->где))
		$event_res->location= $event->где;
	if (isset($event->форма))
		$event_res->form= $event->форма;

	if (isset($event->должник_фио))
	{
		$event_res->debtor_fio= $event->должник_фио;
	}
	else if (isset($event->должник))
	{
		$event_res->debtor= $event->должник;
	}
}