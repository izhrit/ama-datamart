<?php

require_once '../assets/helpers/json.php';

global $exposure_object_fields_spec;
$exposure_object_fields_spec= array(
	array('name'=>'ID_Object','default'=>'')

	,array('name'=>'Краткое_наименование','default'=>'')
	,array('name'=>'Развёрнутое_описание','default'=>'')

	,array('name'=>'Адрес','default'=>'')
	,array('name'=>'ОКАТО','default'=>'')

	,array('name'=>'Категория','default'=>'')
	,array('name'=>'Стадия','default'=>'')
	,array('name'=>'Круг_лиц_с_доступом','default'=>array(
			'Transmit'=>false
			,'ToAll'=>false
			,'ToSelectedPro'=>false
			,'SelectedPro'=>array()
		)
		,'fix'=>function($Круг_лиц_с_доступом)
		{
			$SelectedPro= array();
			foreach ($Круг_лиц_с_доступом->SelectedPro as $sp)
				$SelectedPro[]= (object)array('id'=>$sp->id.'','name'=>null,'url'=>null,'descr'=>null);
			usort($SelectedPro,function($a,$b){return strcmp($a->id,$b->id);});
			$r= (object)array(
				'Transmit'=> !isset($Круг_лиц_с_доступом->Transmit) ? false : $Круг_лиц_с_доступом->Transmit
				,'ToAll'=> !isset($Круг_лиц_с_доступом->ToAll) ? false : $Круг_лиц_с_доступом->ToAll
				,'ToSelectedPro'=> !isset($Круг_лиц_с_доступом->ToSelectedPro) ? false : $Круг_лиц_с_доступом->ToSelectedPro
				,'SelectedPro'=> $SelectedPro
			);
			return $r;
		}
	)
	,array('name'=>'КлассификацияЕФРСБ','default'=>array()
		,'fix'=>function($КлассификацияЕФРСБ)
		{
			$res= array();
			foreach ($КлассификацияЕФРСБ as $ce)
				$res[]= (object)array('code'=>$ce->code.'','name'=>null);
			usort($res,function($a,$b){return strcmp($a->code,$b->code);});
			return $res;
		}
	)
);

function fix_exposure_object($ao)
{
	$o= (object)array();
	global $exposure_object_fields_spec;
	foreach ($exposure_object_fields_spec as $spec)
	{
		$fname= $spec['name'];
		if (!isset($ao->$fname))
		{
			$o->$fname= $spec['default'];
		}
		else
		{
			$fvalue= $ao->$fname;
			if (isset($spec['fix']))
			{
				$fix= $spec['fix'];
				$fvalue= $fix($fvalue);
			}
			$o->$fname= $fvalue;
		}
	}
	return $o;
}

function asset_text_for_md5($ao)
{
	$o= fix_exposure_object($ao);
	$txt= json_encode($o);
	return fix_readable_utf8($txt);
}

function asset_md5($ao)
{
	$txt= asset_text_for_md5($ao);
	$md5hash= md5($txt);
	return $md5hash;
}
