<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

function request_remote_job_log($url)
{
	echo job_time() . " request for remote job logs ..\r\n";

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL,$url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
	$result= curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request remote job logs (code $httpcode for url \"$url\")");

	echo job_time() . " .. got remote job logs responce.\r\n";

	$job_logs= json_decode($result);

	$count_job_logs= count($job_logs);
	echo job_time() . " received logs:$count_job_logs\r\n";

	return $job_logs;
}

function store_remote_job_part($id_Job,$job_part)
{
	echo job_time() . "      + job part:{$job_part->title}\r\n";

	$id_JobPart= null;

	echo job_time() . "           request for job part row ..\r\n";
	$rows= execute_query('select id_JobPart, Description, Enabled from JobPart where Name=? and id_Job=?',
		array('ss',$job_part->title,$id_Job));
	$count_rows= count($rows);
	echo job_time() . "           .. got $count_rows job part rows ..\r\n";
	if (0==$count_rows)
	{
		echo job_time() . "           insert new job part row ..\r\n";
		$id_JobPart= execute_query_get_last_insert_id('insert into JobPart set id_Job=?, Name=?, Description=?;',
			array('sss',$id_Job,$job_part->title,$job_part->description));
		echo job_time() . "           .. inserted new job part row with id_JobPart=$id_JobPart\r\n";
	}
	else
	{
		$row= $rows[0];
		$id_JobPart= $row->id_JobPart;
		if ($job_part->description!=$row->Description || !$row->Enabled)
		{
			echo job_time() . "           change db JobPart Description\r\n from \"{$row->Description}\"\r\n   to \"{$job_part->description}\"\r\n";
			$affected_rows= execute_query_get_affected_rows('update JobPart set Description=?, Enabled=1 where id_JobPart=?;',
				array('ss',$job_part->description,$id_JobPart));
			echo job_time() . "           .. changed db JobPart Description (affected $affected_rows rows).\r\n";
		}
	}

	return $id_JobPart;
}

function store_remote_job_part_log($id_JobPart,$id_JobLog,$jp_log,$part_number,$title)
{
	echo job_time() . "           $title:\r\n";
	echo job_time() . "                request for job part log row ..\r\n";
	$rows= execute_query('select id_JobPartLog from JobPartLog where id_JobLog=? and id_JobPart=?',array('ss',$id_JobLog,$id_JobPart));
	$count_rows= count($rows);
	echo job_time() . "                .. got $count_rows job part log rows ..\r\n";

	$started= !isset($jp_log->started)?null:$jp_log->started;
	$finished= !isset($jp_log->finished)?null:$jp_log->finished;
	$status= !isset($jp_log->status)?null:$jp_log->status;
	$results= !isset($jp_log->results)?null:$jp_log->results;

	if (0<$count_rows)
	{
		echo job_time() . "                update job part log row ..\r\n";
		$affected_rows= execute_query_get_affected_rows('update JobPartLog set Started=?, Finished=?, Status=?, Results=? where id_JobPartLog=?;',
			array('sssss',$started,$finished,$status,$results,$rows[0]->id_JobPartLog));
		echo job_time() . "                .. updated $affected_rows job part log rows.\r\n";
	}
	else
	{
		echo job_time() . "                insert new job part log row ..\r\n";
		$id_JobPartLog= execute_query_get_last_insert_id
			('insert into JobPartLog set id_JobPart=?, id_JobLog=?, Started=?, Number=?, Finished=?, Status=?, Results=?;'
			,array('sssisis',$id_JobPart,$id_JobLog,$started,$part_number,$finished,$status,$results));
		echo job_time() . "                .. inserted new job part log row with id_JobPartLog=$id_JobPartLog.\r\n";
	}
}

function store_remote_job($id_JobSite,$job)
{
	echo job_time() . " * job:{$job->title}\r\n";

	$id_Job= null;

	echo job_time() . "      request for job row ..\r\n";
	$rows= execute_query('select id_Job, Description, MaxAgeMinutes from Job where Name=? and id_JobSite=?',
		array('ss',$job->title,$id_JobSite));
	$count_rows= count($rows);
	echo job_time() . "      .. got $count_rows job rows ..\r\n";
	if (0==$count_rows)
	{
		echo job_time() . "      insert new job row ..\r\n";
		$id_Job= execute_query_get_last_insert_id('insert into Job set id_JobSite=?, Name=?, Description=?, MaxAgeMinutes= ?;',
			array('sssi',$id_JobSite,$job->title,$job->description,$job->max_age_minutes));
		echo job_time() . "      .. inserted new job row with id_Job=$id_Job\r\n";
	}
	else
	{
		$row= $rows[0];
		$id_Job= $row->id_Job;
		if ($job->description!=$row->Description || $job->max_age_minutes!=$row->MaxAgeMinutes)
		{
			echo job_time() . "      change db Job Description\r\n from \"{$row->Description}\"\r\n"
				."   to \"{$job->description}\"\r\n and MaxAgeMinutes from {$row->MaxAgeMinutes} to {$job->max_age_minutes}\r\n";
			$affected_rows= execute_query_get_affected_rows('update Job set Description=?, MaxAgeMinutes= ? where id_Job=?;',
				array('sis',$job->description,$job->max_age_minutes,$id_Job));
			echo job_time() . "      .. changed db Job Description and MaxAgeMinutes (affected $affected_rows rows).\r\n";
		}
	}

	return $id_Job;
}

function store_remote_job_log($id_Job,$j_log,$title)
{
	$id_JobLog= null;
	echo job_time() . "      $title:\r\n";
	echo job_time() . "           request for job log row ..\r\n";
	$rows= execute_query('select id_JobLog from JobLog where id_Job=? and Started=?',array('ss',$id_Job,$j_log->started));
	$count_rows= count($rows);
	echo job_time() . "           .. got $count_rows job log rows ..\r\n";
	if (0==$count_rows)
	{
		echo job_time() . "           insert new job log row ..\r\n";
		$id_JobLog= execute_query_get_last_insert_id('insert into JobLog set id_Job=?, Started=?, Finished=?, Status=?;',
			array('sssi',$id_Job,$j_log->started,$j_log->finished,$j_log->status));
		echo job_time() . "           .. inserted new job log row with id_JobLog=$id_JobLog.\r\n";
	}
	else
	{
		$id_JobLog= $rows[0]->id_JobLog;
		echo job_time() . "           update job log row ..\r\n";
		$affected_rows= execute_query_get_affected_rows('update JobLog set Finished=?, Status=? where id_JobLog=?;',
			array('sss',$j_log->finished,$j_log->status,$id_JobLog));
		echo job_time() . "           .. updated $affected_rows job log rows.\r\n";
	}
	return $id_JobLog;
}

function store_remote_job_with_logs($id_JobSite,$job)
{
	$id_Job= store_remote_job($id_JobSite,$job);

	$id_JobLog_last= store_remote_job_log($id_Job,$job->last,'last');
	$id_JobLog_previous= !isset($job->previous) ? null : store_remote_job_log($id_Job,$job->previous,'previous');

	if ($id_JobLog_last==$id_JobLog_previous)
		throw new Exception('previous and last the same id_JobLog');

	$part_number= 0;
	$part_ids= array();
	foreach ($job->parts as $job_part)
	{
		$id_JobPart= store_remote_job_part($id_Job,$job_part);
		store_remote_job_part_log($id_JobPart,$id_JobLog_last,$job_part->last,$part_number,'last');
		if (null!=$id_JobLog_previous)
			store_remote_job_part_log($id_JobPart,$id_JobLog_previous,$job_part->previous,$part_number,'previous');
		$part_ids[]= $id_JobPart;
		$part_number++;
	}
	$count_part_ids= count($part_ids);

	if (0!=$count_part_ids)
	{
		$part_ids= implode(',',$part_ids);
		echo job_time() . " disable skpipped jobs parts (except $count_part_ids)..\r\n";
		$affected_rows= execute_query_get_affected_rows("update JobPart set Enabled=0 where id_Job=? && id_JobPart not in ($part_ids);",
					array('s',$id_Job));
		echo job_time() . " .. disabled $affected_rows job parts.\r\n";
	}
}

function store_remote_service_job_log($site)
{
	echo job_time() . " service:{$site->title}\r\n";

	global $current_date_time;
	$time_start= !isset($current_date_time) || null==$current_date_time ? date_create() : $current_date_time;
	$time_start_sql= date_format($time_start,'Y-m-d\TH:i:s');

	$id_JobSite= null;

	echo job_time() . " request for job site row ..\r\n";
	$rows= execute_query('select id_JobSite, Description from JobSite where Name=?',array('s',$site->title));
	$count_rows= count($rows);
	echo job_time() . " .. got $count_rows job site rows ..\r\n";
	if (0==$count_rows)
	{
		echo job_time() . " insert new job site row ..\r\n";
		$id_JobSite= execute_query_get_last_insert_id('insert into JobSite set Name=?, Description=?, SyncTime= ?;',
			array('sss',$site->title,$site->description,$time_start_sql));
		echo job_time() . " .. inserted new job site row with id_JobSite=$id_JobSite\r\n";
	}
	else
	{
		$row= $rows[0];
		$id_JobSite= $row->id_JobSite;
		$msg_line= " change db Site SyncTime";
		$msg_line.= ($site->description==$row->Description) 
			? " ..\r\n" 
			: " and Description ..\r\n from \"{$row->Description}\"\r\n   to \"{$site->description}\"\r\n";
		echo job_time() . $msg_line;
		$affected_rows= execute_query_get_affected_rows('update JobSite set Description=?, SyncTime= ? where id_JobSite=?;',
			array('sss',$site->description,$time_start_sql,$row->id_JobSite));
		echo job_time() . " .. changed db Site SyncTime (affected $affected_rows rows).\r\n";
	}

	foreach ($site->jobs as $job)
		store_remote_job_with_logs($id_JobSite,$job);
}

function sync_remote_job_log($url)
{
	$job_logs= request_remote_job_log($url);
	$count_job_logs= count($job_logs);

	if (0!=$count_job_logs)
	{
		foreach ($job_logs as $service_job_log)
			store_remote_service_job_log($service_job_log);
	}

	return "загружены логи для информационных сервисов: $count_job_logs";
}

function sync_remote_efrsb_job_log()
{
	global $use_efrsb_service_url, $remote_api_settings;
	// http://probili.ru/efrsb/dm-api.php/job-monitor?auth-token=234678ghjsadf89HJihdsdsjk98
	return sync_remote_job_log("$use_efrsb_service_url/dm-api.php/job-monitor?auth-token={$remote_api_settings->efrsb->token}");
}

function sync_remote_probili_job_log()
{
	global $use_efrsb_service_url, $remote_api_settings;
	return sync_remote_job_log("$use_efrsb_service_url/dm-api.php/probili.job-monitor?auth-token={$remote_api_settings->efrsb->token}");
}

