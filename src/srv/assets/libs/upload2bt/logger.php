<?php

class Logger
{
	private $current_prefix= '';
	private $prefix_stack= array();

	public $log_txt= null;

	public function __construct()
	{
		$this->log_txt= function($txt)
		{
			echo "$txt\r\n";
		};
	}

	public function base_log_txt($txt)
	{
		$log_txt= $this->log_txt;
		$log_txt($txt);
	}

	public function use_log_txt($log_txt)
	{
		$this->current_prefix= '';
		$this->prefix_stack= array();
		$previous_log_txt= $this->log_txt;
		$this->log_txt= $log_txt;
		return $previous_log_txt;
	}

	public function microtime_as_float()
	{
		return microtime(/* get_as_float= */true);
	}

	public function log($data)
	{
		$txt= is_string($data) ? $data : print_r($data,true);
		$this->base_log_txt($this->current_prefix.$txt);
	}

	public function push($data, $prefix= ' ')
	{
		$sm = $this->microtime_as_float();
		$this->log($data);
		$this->prefix_stack[]= (object)array('sm'=>$sm,'prefix'=>$this->current_prefix);
		$this->current_prefix.= $prefix;
	}

	public function pop_($data= null)
	{
		$fm = $this->microtime_as_float();
		$l= array_pop($this->prefix_stack);
		$this->current_prefix= $l->prefix;
		if (null!=$data)
		{
			$spent= number_format(($fm-$l->sm), 3, ',', ' ');
			$fixed_txt= (!is_string($data)) 
				? $this->log($data) 
				: str_replace("%timespan%", "$spent сек.", $data);
			$this->log($fixed_txt);
		}
	}
}
