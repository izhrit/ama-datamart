<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/codec.xml.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';

require_once '../assets/libs/codecs/registry.codec.php';
require_once 'upload_registry.php';

global $bt_upload_log, $bt_log_time;
$bt_log_time= true;

function log_upload_string($txt)
{
	global $bt_upload_log, $bt_log_time;
	$line= "$txt\r\n";
	if ($bt_log_time)
		$line= job_time()." $line";
	$bt_upload_log.= $line;
	echo $line;
}

function log_upload($data)
{
	log_upload_string(is_string($data) ? $data : print_r($data,true));
}

function log_upload_first_lines($data,$lines_count)
{
	$txt= is_string($data) ? $data : print_r($data,true);

	$parts= explode("\n",$txt);
	if ($lines_count<count($parts))
		$parts= array_slice($parts,0,$lines_count);

	log_upload(implode("\n",$parts));
}

function Store_StartUpload($row)
{
	global $bt_upload_log;
	$bt_upload_log= '';
	$query_text= "UPDATE MProcedure SET last_update_start = now() WHERE id_MProcedure = ?;";
	$affected= execute_query_get_affected_rows($query_text,array('s',$row->id_MProcedure));
	if (1!=$affected)
		throw new Exception("updated $affected rows to set last_update_start for id_MProcedure=$row->id_MProcedure");
}

function Store_FinishUpload($row,$id_BankroTech)
{
	global $bt_upload_log;
	$query_text= "UPDATE MProcedure mp inner join Manager m on mp.id_Manager=m.id_Manager 
	SET mp.last_update_finish=now(), mp.last_update_log=?, mp.ctb_bankrotech_id=?, mp.ctb_revision_posted=?, m.BankroTechAccVerified=1 
	WHERE id_MProcedure = ?;";
	$affected= execute_query_get_affected_rows($query_text
		,array('ssss',$bt_upload_log,$id_BankroTech,$row->ctb_revision,$row->id_MProcedure));
	$bt_upload_log= '';
	if (2!=$affected && 1!=$affected)
		throw new Exception("updated $affected rows to set last_update_finish for id_MProcedure=$row->id_MProcedure");
}

function Store_ErrorUpload($row,$bad_account= false,$ctb_account_limit= false)
{
	global $bt_upload_log;
	$query_text= 
			"UPDATE MProcedure mp inner join Manager m on mp.id_Manager=m.id_Manager SET 
			mp.last_update_finish=now(), mp.ctb_update_error=1, mp.last_update_log=? 
			, m.BankroTechAccVerified=?, m.ctb_account_limit=?
			WHERE id_MProcedure=?;";
	$BankroTechAccVerified= $bad_account ? 0 : 1;
	if (0==$BankroTechAccVerified)
		log_upload("Сохраняем в записи АУ флаг \"аккаунт bankro.tech проверен\"=$BankroTechAccVerified");
	log_upload("Сохраняем в записи процедуры флаг \"ошибка при выгрузке на bt\"=1");
	$ctb_account_limit= $ctb_account_limit ? 1 : 0;
	$affected= execute_query_get_affected_rows
		($query_text,array('siis',$bt_upload_log,$BankroTechAccVerified,$ctb_account_limit,$row->id_MProcedure));
	$bt_upload_log= '';
	if (2!=$affected && 1!=$affected)
		throw new Exception("updated $affected rows to set error and last_update_finish for id_MProcedure=$row->id_MProcedure");
}

function GetZipFileContent($zip_content,$filename_in_zip)
{
	$temp_file = null;
	$zip= null;
	try
	{
		$temp_file= tmpfile();
		fwrite($temp_file, $zip_content);
		fflush($temp_file);
		$temp_file_metadata= stream_get_meta_data($temp_file);
		$temp_file_path= $temp_file_metadata['uri'];
		$zip = new ZipArchive();
		$zip->open($temp_file_path);
		$res= $zip->getFromName($filename_in_zip);
		$zip->close();
		unset($zip);
		fclose($temp_file);
		return $res;
	}
	catch (Exception $ex)
	{
		if (null!=$zip)
		{
			$zip->close();
			unset($zip);
		}
		if (null!=$temp_file)
			fclose($temp_file);
		throw $ex;
	}
}

global $registry_codec;
$registry_codec= new Registry_codec();

function Подготовить_параметры_доступа($row)
{
	if (null==$row->BankroTechAcc)
	{
		return (object)array('domain_name'=>'','login'=>'','password'=>'');
	}
	else
	{
		global $bt_password_options;
		$cr= json_decode($row->BankroTechAcc);
		$cr->password= sync_decrypt(base64_decode($cr->password),$bt_password_options);
		return $cr;
	}
}

function UploadPData($row, $uploader)
{
	global $test_bt_account;
	$параметры_доступа= (isset($test_bt_account)) 
		? $test_bt_account : Подготовить_параметры_доступа($row);

	$logger= $uploader->logger();
	$previous_log_txt= $logger->use_log_txt('log_upload');
	$logger->push('начали загрузку в bankro.tech ..');

	$fake_password= str_repeat('*',strlen($параметры_доступа->password));
	$logger->log("для загрузки в домен \"$параметры_доступа->domain_name\" от пользователя \"$параметры_доступа->login\" с паролем \"$fake_password\"");

	$zip_length= strlen($row->fileData);
	$logger->push("получен zip файл размером $zip_length байт, распаковываем его..");
	$registry_xml= GetZipFileContent($row->fileData,'registry.xml');
	$registry_xml_length= strlen($registry_xml);
	$logger->pop_(".. распаковали РТК в виде xml текста из $registry_xml_length символов за %timespan%");

	//log_upload($registry_xml);

	global $registry_codec;
	$registry= $registry_codec->Decode($registry_xml);

	/*log_upload("прочитали из xml:");
	log_upload(nice_json_encode($registry));*/

	$res= UploadRegistry($registry,$uploader,$параметры_доступа);

	$logger->pop_(".. закончили загрузку в дело $res за %timespan%.");

	$logger->use_log_txt($previous_log_txt);

	return $res;
}

function log_upload_exception($ex)
{
	$txt= $ex->getTraceAsString();

	$txt= preg_replace('/ (.+)assets/i', ' assets', $txt);
	$txt= preg_replace('/ (.+)tests/i', ' tests', $txt);

	global $test_bt_account;
	if (isset($test_bt_account))
		$txt= preg_replace('/\.php\((\d+)\):/i', '.php:', $txt);

	log_upload_first_lines($txt,40);
}

function log_exception(Exception $ex, $row, $bad_account= false)
{
	$exception_class= get_class($ex);
	$exception_Message= $ex->getMessage();
	log_upload("\r\ncatched Exception with class=$exception_class and message:$exception_Message");

	if (!$bad_account)
		log_upload_exception($ex);

	if ('Rest_exception'==$exception_class && isset($ex->responce))
		log_upload_first_lines($ex->responce,40);

	log_upload("on row:");
	
	$row->fileData= null;
	log_upload_first_lines($row,40);
}

function is_bad_account(Exception $ex)
{
	$msg= $ex->getMessage();
	$unsuccessful_pos= mb_strpos($msg,'unsuccessful login');
	$bad_account= (false!==$unsuccessful_pos && 0==$unsuccessful_pos);
	return $bad_account;
}

function process_Upload_exception_return_bad_account(Exception $ex, $row)
{
	$bad_account= is_bad_account($ex);
	log_exception($ex, $row,$bad_account);
	$ctb_account_limit=
	(
		'Rest_exception'==get_class($ex) 
		&& isset($ex->responce) 
		&& 0===strpos($ex->getMessage(),'got HTTP_CODE=400')
		&& 0===strpos($ex->responce,'{"Error":"На вашем аккаунте достигнуто ограничение по количеству активных дел.')
	);
	Store_ErrorUpload($row,$bad_account,$ctb_account_limit);
	return $bad_account;
}

function UploadPData_and_StoreResults_bad_account($irow, $row, $uploader)
{
	try
	{
		log_upload("     $irow) id_Manager:$row->id_Manager, id_MProcedure:$row->id_MProcedure, $row->Name (inn:$row->DebtorINN,ogrn:$row->OGRN,snils:$row->SNILS)");
		log_upload("------------------------------------------- upload {");
		Store_StartUpload($row);
		$id_BankroTech= UploadPData($row, $uploader);
		Store_FinishUpload($row,$id_BankroTech);
		log_upload("------------------------------------------- upload }");
		log_upload("           ok upload id_BankroTech=$id_BankroTech");
		return false;
	}
	catch (Exception $ex)
	{
		return process_Upload_exception_return_bad_account($ex, $row);
	}
}

/*

заново всё:

update Manager set BankroTechAccVerified=null;

update MProcedure
set ctb_revision_posted=null
, ctb_update_error=0
, last_update_log=null
, last_update_start=null
, last_update_finish=null
where ctb_update_error=1
;

*/

function UploadPortion($max_portion_size, $uploader, $trace_db_select_query= false, $id_Manager= null)
{
	$id_Manager_cond= '';
	if (null!=$id_Manager)
	{
		$id_Manager= intval($id_Manager);
		$id_Manager_cond= " and m.id_Manager=$id_Manager ";
	}
	log_upload(" query $max_portion_size records from PData to upload");
	$query_text= "SELECT 
			mp.id_MProcedure, mp.casenumber, mp.ctb_revision,
			m.id_Manager, m.firstName, m.lastName, m.middleName,
			m.BankroTechAcc, m.ArbitrManagerID, m.INN as ManagerINN,
			d.INN as DebtorINN, d.OGRN, d.SNILS, d.Name, d.BankruptId,
			pd.fileData
		FROM MProcedure mp
		INNER JOIN Manager m on m.id_Manager = mp.id_Manager
		INNER JOIN PData pd on mp.id_MProcedure = pd.id_MProcedure
		INNER JOIN Debtor d on mp.id_Debtor = d.id_Debtor

		WHERE mp.ctb_revision = pd.revision 
		AND (ctb_revision_posted is null or ctb_revision_posted != mp.ctb_revision) 
		AND ctb_update_error = 0

		and 'v'=m.VerificationState and 'v'=mp.VerificationState and 'v'=d.VerificationState

		and not m.id_Manager in (401,535,102986,167692,15526) $id_Manager_cond

		-- and (m.BankroTechAccVerified is null or m.BankroTechAccVerified<>0)
		and (m.BankroTechAccVerified is null)

		and (last_update_start is null && last_update_finish is null || last_update_start < last_update_finish)
		and m.BankroTechAcc is not null
		ORDER BY ctb_publicDate ASC
		LIMIT ?;";
	if ($trace_db_select_query)
		echo "$query_text\r\n";
	$bad_id_Managers= array();
	$rows = execute_query($query_text,array('s',$max_portion_size));
	$count_rows= count($rows);
	log_upload("   got $count_rows records");
	if ($count_rows > 0)
	{
		$irow= 1;
		foreach ($rows as $row)
		{
			if (!isset($bad_id_Managers[$row->id_Manager]))
			{
				$bad_account= UploadPData_and_StoreResults_bad_account($irow, $row, $uploader);
				if ($bad_account)
					$bad_id_Managers[$row->id_Manager]= $row->id_Manager;
				$irow++;
			}
		}
	}
	return $count_rows;
}

function upload($uploader, $portion_size= 30, $max_count_to_update= null, $trace_db_select_query= false, $id_Manager= null)
{
	global $echo_errors;
	$echo_errors= true;

	$txt_max_interval_time= '30 minutes';
	log_upload("upload data to Bankro.Tech (for $txt_max_interval_time, by $portion_size to $max_count_to_update) {");
	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string($txt_max_interval_time));
	$total_uploaded= 0;
	while (date_create() < $time_start_to_stop)
	{
		$uploaded= UploadPortion($portion_size,$uploader,$trace_db_select_query,$id_Manager);
		if (0==$uploaded)
		{
			log_upload('stop because of zero upload!');
			break;
		}
		$total_uploaded+= $uploaded;
		if (null!=$max_count_to_update && $total_uploaded>=$max_count_to_update)
			break;
	}
	if (date_create() >= $time_start_to_stop)
		log_upload("stop because of time is over ($txt_max_interval_time)!");
	log_upload('upload data to Bankro.Tech }');

	return "выгружено процедур: $total_uploaded";
}