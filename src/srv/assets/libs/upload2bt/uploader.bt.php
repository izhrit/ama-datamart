<?php

require_once 'uploader.mock.php';

class Registry2bt_uploader extends Registry_uploader
{
	public function __construct($bt_client)
	{
		$this->bt_client= $bt_client;
	}

	public function logger()
	{
		return $this->bt_client->logger();
	}

	public $bt_client= null;
	private $используемые_параметры_доступа= null;

	private $кэш_для_параметров_доступа= array();
	private $кэш_для_используемых_параметров_доступа= null;

	static private function key_for_параметры_доступа($параметры_доступа)
	{
		return $параметры_доступа->login.'@'.$параметры_доступа->domain_name;
	}

	static private function новый_кэш_для_параметров_доступа()
	{
		return (object)array
		(
			'Процедуры'=>(object)array(
				'by_id'=> array()
				,'by_Номер_судебного_дела'=> array()
			)
			,'Участники'=>(object)array(
				'by_id'=> array()
				,'by_ИНН'=> array()
				,'by_DisplayName'=> array()
			)
		);
	}

	static private function новый_кэш_для_project($project)
	{
		return (object)array
		(
			'Id'=>$project->Id
			,'Name'=>$project->Name
			,'Обезличенные_физ_лица'=>array()
		);
	}

	static private function новый_кэш_для_project_из_CreateProject($project_from_CreateProject)
	{
		$res= self::новый_кэш_для_project($project_from_CreateProject);
		$res->from_CreateProject= $project_from_CreateProject;
		return $res;
	}

	static private function новый_кэш_для_project_из_GroupedProjects($project_from_GroupedProjects)
	{
		$res= self::новый_кэш_для_project($project_from_GroupedProjects);
		$res->from_GroupedProjects= $project_from_GroupedProjects;
		return $res;
	}

	private function Создать_сессию($параметры_доступа)
	{
		$this->используемые_параметры_доступа= $параметры_доступа;
		$this->bt_client->login($параметры_доступа->domain_name,$параметры_доступа->login,$параметры_доступа->password);
		$key= self::key_for_параметры_доступа($параметры_доступа);
		if (isset($this->кэш_для_параметров_доступа[$key]))
		{
			$this->кэш_для_используемых_параметров_доступа= $this->кэш_для_параметров_доступа[$key];
		}
		else
		{
			$this->кэш_для_используемых_параметров_доступа= self::новый_кэш_для_параметров_доступа();
			$this->кэш_для_параметров_доступа[$key]= $this->кэш_для_используемых_параметров_доступа;
		}
	}

	public function Использовать_сессию_для($параметры_доступа)
	{
		$logger= $this->logger();
		if (null==$this->используемые_параметры_доступа)
		{
			$logger->log('сессия не создана, создадим новую!');
			$this->Создать_сессию($параметры_доступа);
		}
		else
		{
			$used_domain= $this->используемые_параметры_доступа->domain_name;
			$used_login= $this->используемые_параметры_доступа->login;
			$logger->log("текущая сессия открыта для $used_domain, login:$used_login!");
			if ($used_domain==$параметры_доступа->domain_name && $used_login==$параметры_доступа->login)
			{
				$logger->log('используем текущую сессию!');
			}
			else
			{
				$logger->log('создаём новую сессию!');
				$this->Создать_сессию($параметры_доступа);
			}
		}
	}

	public function Очистить_кэш()
	{
		$logger= $this->logger();
		$this->кэш_для_используемых_параметров_доступа= self::новый_кэш_для_параметров_доступа();
		$logger->log('Очистили кэш!');
	}

	public function Загрузить_перечень_дел()
	{
		$logger= $this->logger();

		$logger->push("Загружаем перечень дел с Bankro.TECH ..");
		$кэш= $this->кэш_для_используемых_параметров_доступа;
		$project_groups= $this->bt_client->GetGroupedProjects_NotArchived();
		foreach ($project_groups as $project_group)
		{
			foreach ($project_group->Projects as $project)
			{
				if (!isset($кэш->Процедуры->by_id[$project->Id]))
				{
					$кэш->Процедуры->by_id[$project->Id]= self::новый_кэш_для_project_из_GroupedProjects($project);
				}
				else
				{
					$кэш->Процедуры->by_id[$project->Id]->from_GroupedProjects= $project;
				}
			}
		}
		$count= count($кэш->Процедуры->by_id);
		$logger->pop_("Загрузили перечень дел с Bankro.TECH в количестве $count за %timespan%");
	}

	public function Определяем_номер_судебного_дела_для_дел_из_перечня_пока_не_найдём_искомое($Номер_судебного_дела)
	{
		$logger= $this->logger();
		$кэш= $this->кэш_для_используемых_параметров_доступа;

		$logger->push("Определяем номер судебного дела для дел из перечня пока не найдём искомое");
		foreach ($кэш->Процедуры->by_id as $project_id => $project)
		{
			if (!isset($project->Номер_судебного_дела))
			{
				$logger->push("определяем номер судебного дела для проекта id=\"$project_id\":");
				$project_details= $this->bt_client->GetProject_for_projectId($project_id);
				$project->Номер_судебного_дела= $project_details->CasebookNumber;
				$logger->pop_("определили номер судебного дела \"{$project->Номер_судебного_дела}\".");
				if (''!=$project->Номер_судебного_дела)
				{
					$кэш->Процедуры->by_Номер_судебного_дела[$project->Номер_судебного_дела]= $project;
					if ($project->Номер_судебного_дела==$Номер_судебного_дела)
					{
						$logger->pop_("Нашли в перечне судебных дел дела с искомым номером, потратили %timespan%");
						return $project;
					}
				}
			}
		}
		$logger->pop_("НЕ нашли в перечне судебных дел дела с искомым номером, потратили %timespan%");
		return null;
	}

	public function Найти_project_по_номеру_судебного_дела($Номер_судебного_дела)
	{
		$logger= $this->logger();
		$кэш= $this->кэш_для_используемых_параметров_доступа;

		$project= null;
		$logger->push("Ищем дело по номеру \"$Номер_судебного_дела\" ..");
		if (isset($кэш->Процедуры->by_Номер_судебного_дела[$Номер_судебного_дела]))
		{
			$logger->log("Дело с искомым номером есть в кэше!");
			$project= $кэш->Процедуры->by_Номер_судебного_дела[$Номер_судебного_дела];
		}
		else
		{
			$this->Загрузить_перечень_дел();
			$project= $this->Определяем_номер_судебного_дела_для_дел_из_перечня_пока_не_найдём_искомое($Номер_судебного_дела);
		}
		if (null==$project)
		{
			$logger->pop_("НЕ нашли дело по номеру, на поиск потратили %timespan%");
		}
		else
		{
			$project_id= $project->Id;
			$logger->pop_("Нашли дело по номеру, его id=\"$project_id\", за %timespan%");
		}
		return $project;
	}

	public function Очистить_требования_для_id_Процедуры($id_project)
	{
		$кэш= $this->кэш_для_используемых_параметров_доступа;
		$project= $кэш->Процедуры->by_id[$id_project];
		$this->Очистить_требования_для_project($project);
	}

	function Очистить_требования_для_project($project)
	{
		$logger= $this->logger();
		$logger->push("Чистим старые требования перед загрузкой..");

		$logger->push("Запрашиваем старый перечень требований:");
		$creditors= $this->bt_client->GetRequirementsByProjectId($project->Id);
		$logger->pop_("Получили перечень старых требований за %timespan%");
		$creditors_count= count($creditors);
		$logger->log("количество кредиторов в полученном перечне:$creditors_count.");

		if (0==$creditors_count)
		{
			$logger->log("Нет требований для удаления.");
		}
		else
		{
			$array_of_requirement_id= array();
			foreach ($creditors as $creditor)
			{
				foreach ($creditor->Requirements as $requirement)
				{
					$array_of_requirement_id[]= $requirement->Id;
				}
			}
			$array_of_requirement_id_array_count= count($array_of_requirement_id);
			$logger->log("    требований:$array_of_requirement_id_array_count.");

			$logger->push("Удаляем старые требования:");
			$creditors= $this->bt_client->Requirements_BulkDelete($array_of_requirement_id);
			$logger->pop_("Удалили старые требования за %timespan%");
		}
		$logger->pop_("Очистили старые требования перед загрузкой за %timespan%");
	}

	function Кэшировать_созданную_процедуру($created_project,$Номер_судебного_дела)
	{
		$project= $this->новый_кэш_для_project_из_CreateProject($created_project);
		$кэш= $this->кэш_для_используемых_параметров_доступа;
		$кэш->Процедуры->by_Номер_судебного_дела[$Номер_судебного_дела]= $project;
		$кэш->Процедуры->by_id[$created_project->Id]= $project;
	}

	function добавить_участника_в_кэш($participant,$inn= null)
	{
		$кэш= $this->кэш_для_используемых_параметров_доступа;
		if (null!=$inn)
			$кэш->Участники->by_ИНН[$inn]= $participant;
		$кэш->Участники->by_id[$participant->Id]= $participant;
		$кэш->Участники->by_DisplayName[$participant->DisplayName]= $participant;
	}

	public function Подготовить_участника_организацию_по_ИНН($участника,$параметры_участника,$Организация)
	{
		$logger= $this->logger();
		$кэш= $this->кэш_для_используемых_параметров_доступа;
		$inn= $Организация->ИНН;
		$logger->push("Готовим $участника ИНН:$inn (\"{$Организация->Наименование}\") ..");
		if (isset($кэш->Участники->by_ИНН[$inn]))
		{
			$logger->log('Участник найден в кэше.');
			$logger->pop_("Подготовили $участника за %timespan%");
			return $кэш->Участники->by_ИНН[$inn];
		}
		else
		{
			$logger->push("Ищем участника по ИНН $inn:");
			$participants= $this->bt_client->GetParticipants(array('INNs'=>array($inn)));
			$participants_count= count($participants);
			$logger->pop_("Нашли участников в количестве $participants_count.");
			if (1==$participants_count)
			{
				$participant= $participants[0];
				$this->добавить_участника_в_кэш($participant,$inn);
				$logger->pop_("Подготовили $участника за %timespan%");
				return $participant;
			}
			else if (0==$participants_count)
			{
				$phones= !isset($параметры_участника->Контактные_телефоны)?null:$параметры_участника->Контактные_телефоны;
				$participant_to_create= participantBody_Юридическое_лицо($Организация,$phones);
				$participant= $this->bt_client->PutParticipant($participant_to_create);
				$this->добавить_участника_в_кэш($participant,$inn);
				$logger->pop_("Подготовили $участника за %timespan%");
				return $participant;
			}
		}
	}

	public function Подготовить_участника_организацию($участника,$параметры_участника,$Организация)
	{
		$logger= $this->logger();

		if (isset($Организация->ИНН))
		{
			return $this->Подготовить_участника_организацию_по_ИНН($участника,$параметры_участника,$Организация);
		}
		else
		{
			$logger->push("Готовим $участника (\"{$Организация->Наименование}\") ..");
			$logger->log("НЕ указан ИНН $участника!");
			$кэш= $this->кэш_для_используемых_параметров_доступа;
			$name= $Организация->Наименование;

			if (isset($кэш->Участники->by_DisplayName[$name]))
			{
				$logger->log('Участник найден в кэше.');
				$logger->pop_("Подготовили $участника за %timespan%");
				return $кэш->Участники->by_DisplayName[$name];
			}
			else
			{
				$logger->push("Ищем участника по наименованию:");
				$participants= $this->bt_client->GetParticipants_by_ContextSearchString($Организация->Наименование);
				$participants_count= count($participants);
				$logger->pop_("Нашли участников в количестве $participants_count.");
				if (1==$participants_count)
				{
					$participant= $participants[0];
					$this->добавить_участника_в_кэш($participant);
					$logger->pop_("Подготовили $участника за %timespan%");
					return $participant;
				}
				else if (0==$participants_count)
				{
					$phones= !isset($параметры_участника->Контактные_телефоны)?null:$параметры_участника->Контактные_телефоны;
					$participant_to_create= participantBody_Юридическое_лицо($Организация,$phones);
					$participant= $this->bt_client->PutParticipant($participant_to_create);
					$this->добавить_участника_в_кэш($participant);
					$logger->pop_("Подготовили $участника за %timespan%");
					return $participant;
				}
			}
		}
	}

	public function Подготовить_участника_физическое_лицо($участника,$параметры_участника,$Физическое_лицо,$id_Процедуры,$i)
	{
		$кэш= $this->кэш_для_используемых_параметров_доступа;
		$logger= $this->logger();

		$logger->push("Готовим $участника (\"Физ лицо {$i}й\") ..");

		$project= $кэш->Процедуры->by_id[$id_Процедуры];

		if (isset($project->Обезличенные_физ_лица[$i]))
		{
			$logger->log('Участник найден в кэше.');
			$logger->pop_("Подготовили $участника за %timespan%");
			return $project->Обезличенные_физ_лица[$i];
		}
		else
		{
			$logger->push("Ищем кредитора физ лицо с отчеством \"{$i}й\":");
			$participants= $this->bt_client->GetRolledParticipants_Кредитор_физ_лицо($id_Процедуры,$i);
			$participants_count= count($participants);
			$logger->pop_("Нашли кредиторов в количестве $participants_count.");

			if (1==$participants_count)
			{
				$participant= $participants[0];
				$кэш->Участники->by_id[$participant->Id]= $participant;
				$project->Обезличенные_физ_лица[$i]= $participant;
				$logger->pop_("Подготовили $участника за %timespan%");
				return $participant;
			}
			else if (0==$participants_count)
			{
				$fields= (object)array('Фамилия'=>'Физ', 'Имя'=>'лицо', 'Отчество'=>"{$i}й");
				if (isset($параметры_участника->Адрес_почтовых_уведомлений))
					$fields->Адрес= $параметры_участника->Адрес_почтовых_уведомлений;
				$participant_to_create= participantBody_Физическое_лицо($fields);
				if (isset($параметры_участника->Контактные_телефоны))
					$participant_to_create->ContactDetail->Phone= $параметры_участника->Контактные_телефоны;
				$participant= $this->bt_client->PutParticipant($participant_to_create);
				$кэш->Участники->by_id[$participant->Id]= $participant;
				$project->Обезличенные_физ_лица[$i]= $participant;

				$logger->pop_("Подготовили $участника за %timespan%");

				return $participant;
			}
		}
	}

	public function Подготовить_участника($участника,$параметры_участника,$id_Процедуры,$iКредитор)
	{
		$кэш= $this->кэш_для_используемых_параметров_доступа;
		$logger= $this->logger();

		if (isset($параметры_участника->Организация))
		{
			$Организация= $параметры_участника->Организация;
			return $this->Подготовить_участника_организацию($участника,$параметры_участника,$Организация);
		}
		else if (isset($параметры_участника->Физическое_лицо))
		{
			$Физическое_лицо= $параметры_участника->Физическое_лицо;
			return $this->Подготовить_участника_физическое_лицо($участника,$параметры_участника,$Физическое_лицо,$id_Процедуры,$iКредитор);
		}
	}

	public function Подготовить_должника_физическое_лицо($должник)
	{
		$logger= $this->logger();
		$участника= 'должника';

		if (!isset($должник->ИНН))
		{
			$logger->log("НЕ указан ИНН должника!");
			return null;
		}

		$кэш= $this->кэш_для_используемых_параметров_доступа;

		$inn= $должник->ИНН;
		$logger->push("Готовим $участника ИНН:$inn (\"{$должник->Наименование}\") ..");
		if (isset($кэш->Участники->by_ИНН[$inn]))
		{
			$logger->log('Участник найден в кэше.');
			$logger->pop_("Подготовили $участника за %timespan%");
			return $кэш->Участники->by_ИНН[$inn];
		}
		else
		{
			$logger->push("Ищем участника по ИНН $inn:");
			$participants= $this->bt_client->GetParticipants(array('INNs'=>array($inn)));
			$participants_count= count($participants);
			$logger->pop_("Нашли участников в количестве $participants_count.");
			if (1==$participants_count)
			{
				$participant= $participants[0];
				$this->добавить_участника_в_кэш($participant,$inn);
				$logger->pop_("Подготовили $участника за %timespan%");
				return $participant;
			}
			else if (0==$participants_count)
			{
				$parts= explode(' ',$должник->Наименование);
				$count_parts= count($parts);
				$np= (object)array('Фамилия'=>$parts[0],'ИНН'=>$должник->ИНН);
				if ($count_parts>1)
					$np->Имя= $parts[1];
				if ($count_parts>2)
					$np->Отчество= $parts[2];
				if (isset($должник->ОГРН))
					$np->ОГРН= $должник->ОГРН;
				$participant_to_create= participantBody_Физическое_лицо($np);
				$participant= $this->bt_client->PutParticipant($participant_to_create);
				$this->добавить_участника_в_кэш($participant,$inn);
				$logger->pop_("Подготовили $участника за %timespan%");
				return $participant;
			}
		}
	}

	public function Подготовить_должника($параметры_должника,$Тип_процедуры)
	{
		if (Процедура_физического_лица($Тип_процедуры))
		{
			return $this->Подготовить_должника_физическое_лицо($параметры_должника);
		}
		else
		{
			$параметры_участника= (object)array
			(
				'Организация'=>(object)array
				(
					'Наименование'=>$параметры_должника->Наименование
					,'ИНН'=>$параметры_должника->ИНН
					,'ОГРН'=>!isset($параметры_должника->ОГРН)?null:$параметры_должника->ОГРН
				)
			);
			return $this->Подготовить_участника('должника',$параметры_участника,null,null);
		}
	}

	function Создать_процедуру($параметры_процедуры)
	{
		$logger= $this->logger();
		$Номер_судебного_дела= $параметры_процедуры->Номер_судебного_дела;

		$logger->push("Создаём дело \"$Номер_судебного_дела\" ({$параметры_процедуры->Тип_процедуры})..");
		$UserProfile= $this->bt_client->UserProfiles_Get();

		$Должник= $this->Подготовить_должника($параметры_процедуры->Должник, $параметры_процедуры->Тип_процедуры);

		$dname= beautify_debtorName($Должник->DisplayName);
		$project_name= "Дело №$Номер_судебного_дела, должник: {$dname}";

		$project_to_create= projectBody($параметры_процедуры->Тип_процедуры,$project_name,$UserProfile);

		$created_project= $this->bt_client->CreateProject($project_to_create);

		$this->Кэшировать_созданную_процедуру($created_project,$Номер_судебного_дела);

		$isNaturalPerson= Процедура_физического_лица($параметры_процедуры->Тип_процедуры);
		$blocks= blocksForNewProject($Должник,$created_project,$project_name,$UserProfile,$isNaturalPerson,$параметры_процедуры);

		$this->bt_client->UpdateProjectWithBlocks($blocks);

		$logger->pop_("Создали дело за %timespan%");
		return $created_project->Id;
	}

	public function Подготовить_процедуру($параметры_процедуры)
	{
		$Номер_судебного_дела= $параметры_процедуры->Номер_судебного_дела;

		$project= $this->Найти_project_по_номеру_судебного_дела($Номер_судебного_дела);
		if (null==$project)
		{
			return $this->Создать_процедуру($параметры_процедуры);
		}
		else
		{
			$this->Очистить_требования_для_project($project);
			return $project->Id;
		}
	}

	public function Подготовить_кредитора($id_Процедуры,$параметры_кредитора,$iКредитор)
	{
		$participant= $this->Подготовить_участника('кредитора',$параметры_кредитора,$id_Процедуры,$iКредитор);
		return null==$participant ? null : $participant->Id;
	}

	static function Participant_to_Creditor($participant)
	{
		return array(
			'Id'=>$participant->Id
			,'Name'=>$participant->DisplayName
			,'Email'=>null
			,'Description'=>$participant->INN
			,'TypeName'=>$participant->Type->Name
			,'TypeId'=>$participant->Type->Id
			,'RegistrationNumber'=>$participant->RegistrationNumber
			,'CreationDate'=>$participant->CreationDate
		);
	}

	static function Prepare_requirement_Type($Id,$Name,$SysName)
	{
		return array(
			"Id" => $Id
			,"ParentId" => null
			,"Name" => $Name
			,"SysName" => $SysName
			,"IsCustom" => false
			,"Author" => null
			,"AuthorId" => null
			,"CreationDate" => null
			,"LastModifedByUserId" => null
			,"LastChangeDate" => null
			,"IsLeaf" => null
			,"Items" => null
			,"Opened" => false
			,"Level" => 0
		);
	}

	function Очередь_to_requirement_Type($Очередь)
	{
		switch ($Очередь)
		{
			case '3.1': return self::Prepare_requirement_Type(
				'746b5c88-42fc-e711-80bd-0cc47a7b67ab'
				,'3.1 Залоговые требования'
				,'Collateral'
			);
			case '3': return self::Prepare_requirement_Type(
				'756b5c88-42fc-e711-80bd-0cc47a7b67ab'
				,'3.2 Незалоговые требования'
				,'NoCollateral'
			);
			default: 
				return null;
		}
	}

	static function Requirement_State_Установлено_судом()
	{
		return array
		(
			"Id" => "7b6b5c88-42fc-e711-80bd-0cc47a7b67ab",
			"ParentId" => null,
			"Name" => "Установлено судом",
			"SysName" => "EstablishedByCourt",
			"IsCustom" => false,
			"Author" => null,
			"AuthorId" => null,
			"CreationDate" => null,
			"LastModifedByUserId" => null,
			"LastChangeDate" => null,
			"IsLeaf" => null,
			"Items" => null,
			"Opened" => false,
			"Level" => 0
		);
	}

	static function Safe_Реестр_Внесено_Дата_to_requirement_Date($Дата)
	{
		return date_format(date_create_from_format("d.m.Y", $Дата),'Y-m-d');
	}

	function Prepare_requirement($параметры_требования)
	{
		$logger= $this->logger();
		$requirement_Type= self::Очередь_to_requirement_Type($параметры_требования->Очередь);
		if (null==$requirement_Type)
		{
			$logger->log("Не поддерживается номер очереди \"{$параметры_требования->Очередь}\"!");
			return null;
		}
		else if (!isset($параметры_требования->Реестр->Внесено->Дата))
		{
			$logger->log('Не определена дата внесения в реестр!');
			return null;
		}
		else
		{
			$requirement= array(
				'Type'=>$requirement_Type
				,'State'=>self::Requirement_State_Установлено_судом()
				,'Amount'=>$параметры_требования->Размер
				,'Date'=>self::Safe_Реестр_Внесено_Дата_to_requirement_Date($параметры_требования->Реестр->Внесено->Дата)
			);
			return $requirement;
		}
	}

	public function Добавить_требование_вернуть_requirement($id_Процедуры,$id_Кредитора,$параметры_требования,$номер_требования)
	{
		$кэш= $this->кэш_для_используемых_параметров_доступа;
		$project= $кэш->Процедуры->by_id[$id_Процедуры];
		$participant= $кэш->Участники->by_id[$id_Кредитора];

		$requirement_to_create= $this->Prepare_requirement($параметры_требования);
		if (null==$requirement_to_create)
		{
			return null;
		}
		else
		{
			$fields= (object)array(
				'Project'=>array('Id'=>$id_Процедуры,'Name'=>$project->Name)
				,'Creditor'=>self::Participant_to_Creditor($participant)
				,'Number'=>$номер_требования
				,'Items'=>array($requirement_to_create)
			);
			if (isset($параметры_требования->Погашения))
			{
				$Payments= array();
				foreach ($параметры_требования->Погашения as $погашение)
				{
					$Payments[]= (object)array
					(
						'Date'=>self::Safe_Реестр_Внесено_Дата_to_requirement_Date($погашение->Дата)
						,'Amount'=>$погашение->Сумма
					);
				}
				$fields->Payments= $Payments;
			}
			return $this->bt_client->PutRequirement($fields);
		}
	}

	public function Добавить_требование($id_Процедуры,$id_Кредитора,$параметры_требования,$номер_требования)
	{
		$requirement= $this->Добавить_требование_вернуть_requirement($id_Процедуры,$id_Кредитора,$параметры_требования,$номер_требования);
		return null==$requirement ? null : $requirement->Id;
	}

	public function Добавить_имущество($id_Процедуры,$id_Кредитора,$id_Требования,$параметры_залога)
	{
		$properties= $this->bt_client->GetProperties($id_Процедуры);

		foreach ($properties as $property)
		{
			if ($параметры_залога->Предмет->Текстом==$property->Name)
				return $property;
		}

		return $this->bt_client->PutProperty((object)array
		(
			'Name'=>$параметры_залога->Предмет->Текстом
			,'Number'=>count($properties)+1
			,'Project'=>(object)array
			(
				'Id'=>$id_Процедуры
			)
			,'PropertyContracts'=>array
			(
				(object)array
				(
					'NameBasisCollateralSum'=>$параметры_залога->Основания
					,'CollateralSum'=>$параметры_залога->Размер_залогового_обеспечения
				)
			)
			,'Type'=>(object)array
			(
				'Id'=>'4828769c-42fc-e711-80bd-0cc47a7b67ab'
				,'Name'=>'Залоговое'
				,'SysName'=>'Collateral'
			)
		));
	}

	public function Добавить_залог($id_Процедуры,$id_Кредитора,$id_Требования,$параметры_залога)
	{
		$property= $this->Добавить_имущество($id_Процедуры,$id_Кредитора,$id_Требования,$параметры_залога);
		$this->bt_client->SaveRelatedObjects_RequirementToProperties($id_Требования,$property->Id,$property->Name);
	}
}
