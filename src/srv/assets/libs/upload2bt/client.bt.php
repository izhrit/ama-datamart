<?php

class Bankro_tech_client
{
	private $rest;
	private $domain;

	public function __construct($rest)
	{
		$this->rest= $rest;
	}

	public function logger()
	{
		return $this->rest->logger;
	}

	public function stop_logging()
	{
		$this->rest->logger->log_txt= function($txt){};
	}

	private function ParseProcessRequestResponce($equest_responce)
	{
		$parsed_responce= json_decode($equest_responce);
		if (1!=$parsed_responce->IsSuccess)
			throw new Exception("got unsuccess responce:\r\n".$equest_responce);
		return $parsed_responce;
	}

	private function ProcessRequestResponce($equest_responce)
	{
		$parsed_responce= $this->ParseProcessRequestResponce($equest_responce);
		return $parsed_responce->Result;
	}

	public function login($domain,$login,$password)
	{
		$this->rest->init_curl();
		$url_prefix= "https://$domain.bankro.tech";
		$this->domain= $url_prefix;
		$request_parameters= array(
			CURLOPT_URL =>  "$url_prefix/authentication/account/login",
			CURLOPT_POST => true, CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array(
				'login' => $login,
				'password' => $password
			)
		);
		try
		{
			$request_result= $this->rest->safe_request($request_parameters);
		}
		catch (Exception $ex)
		{
			$parts= explode("\n",$ex->getMessage());
			if (200<count($parts))
				$parts= array_slice($parts,0,3);
			$msg= implode("\n",$parts);
			throw new Exception("unsuccessful login: $msg",0,$ex);
		}
		$parsed_responce= json_decode($request_result);
		if (1!=$parsed_responce->Succeeded)
			throw new Exception("unsuccessful login got responce:\r\n".$request_result);
		return $parsed_responce;
	}

	function std_GET($url)
	{
		$domain= $this->domain;
		return $request_result= $this->rest->request(array(
			CURLOPT_URL => "$domain$url",
			CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPGET => true, CURLOPT_POST => false, CURLOPT_PUT=> false
		));
	}

	static function report_fields_on_exception($fields)
	{
		echo "fields:\r\n";
		print_r($fields);
		echo "\r\n";
	}

	function std_POST($url,$fields)
	{
		$domain= $this->domain;
		try
		{
			return $request_result= $this->rest->request(array(
				CURLOPT_URL => "$domain$url",
				CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_HTTPGET => false, CURLOPT_POST => true, CURLOPT_PUT=> false,
				CURLOPT_POSTFIELDS => json_encode($fields)
			));
		}
		catch (Exception $ex)
		{
			self::report_fields_on_exception($fields);
			throw $ex;
		}
	}

	function std_PUT($url,$fields= null)
	{
		$domain= $this->domain;
		$request_params= array(
			CURLOPT_URL => "$domain$url",
			CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
			CURLOPT_CUSTOMREQUEST => 'PUT'
		);
		if (null!=$fields)
			$request_params[CURLOPT_POSTFIELDS]= json_encode($fields);
		try
		{
			return $request_result= $this->rest->request($request_params);
		}
		catch (Exception $ex)
		{
			self::report_fields_on_exception($fields);
			throw $ex;
		}
	}

	function std_DELETE($url)
	{
		$domain= $this->domain;
		return $request_result= $this->rest->request(array(
			CURLOPT_URL => "$domain$url",
			CURLOPT_CUSTOMREQUEST => 'DELETE',
			CURLOPT_HTTPGET => false, CURLOPT_POST => false, CURLOPT_PUT=> false
		));
	}

	public function GetGroupedProjects_NotArchived($FullSearchString= '')
	{
		$request_result= $this->std_POST('/api/Projects/GetGroupedProjects',array
		(
			'FullSearchString'=>$FullSearchString
			,'FolderId'=>null
			,'Page'=>1
			,'PageSize'=>999
			,'Filters'=>array(
				array(
					'DataField'=>array('Id'=>'5bb8ab03-2bdc-e411-9674-902b343a9588','SysName'=>'Project_IsArchive')
					,'SearchValues'=>array(false)
				)
			)
		));
		return $this->ProcessRequestResponce($request_result);
	}

	public function GetProject($params)
	{
		$request_result= $this->std_GET("/api/Projects/GetProject$params&api-version=40");
		return $this->ProcessRequestResponce($request_result);
	}

	public function GetProject_for_projectId($id)
	{
		$request_result= $this->std_GET("/api/Projects/GetProject?ProjectId=$id");
		return $this->ProcessRequestResponce($request_result);
	}

	public function ProjectCustomValues_for_projectId($id)
	{
		$request_result= $this->std_GET("/api/ProjectCustomValues/?request.projectId=$id");
		return $this->ProcessRequestResponce($request_result);
	}

	public function UserProfiles_GetId()
	{
		$request_result= $this->std_GET('/api/UserProfiles/GetId');
		return $this->ProcessRequestResponce($request_result);
	}

	public function UserProfiles_Get()
	{
		$request_result= $this->std_GET('/api/UserProfiles/Get');
		return $this->ProcessRequestResponce($request_result);
	}

	public function CreateProject($p)
	{
		$request_result= $this->std_POST('/api/Projects/CreateProject',$p);
		return $this->ProcessRequestResponce($request_result);
	}

	public function ArchiveProject($id)
	{
		$request_result= $this->std_PUT("/api/projects/Archive?Id=$id");
		$parsed_responce= json_decode($request_result);
		if (true!=$parsed_responce->IsSuccess)
			throw new Exception("got unsuccess responce:\r\n".$equest_responce);
	}

	public function DeleteProject($id)
	{
		$request_result= $this->std_DELETE("/api/projects/DeleteProject?Id=$id");
		$parsed_responce= json_decode($request_result);
		if (true!=$parsed_responce->IsSuccess)
			throw new Exception("got unsuccess responce:\r\n".$equest_responce);
	}

	public function GetRequirementsByProjectId($id)
	{
		$request_result= $this->std_POST('/api/Requirements/GetRequirementsByProjectId',array('projectId' => $id));
		return $this->ProcessRequestResponce($request_result);
	}

	public function Requirements_BulkDelete($array_of_requirement_id)
	{
		$request_result= $this->std_POST('/api/Requirements/BulkDelete',array('RequirementIds' => $array_of_requirement_id));
		$this->ParseProcessRequestResponce($request_result);
	}

	public function GetParticipant($participant_id)
	{
		$request_result= $this->std_GET("/api/Participants/GetParticipant?participantId=$participant_id");
		return $this->ProcessRequestResponce($request_result);
	}

	public function GetParticipants($fields)
	{
		$request_result= $this->std_POST("/api/Participants/GetParticipants",$fields);
		return $this->ProcessRequestResponce($request_result);
	}

	public function PutRequirement($fields)
	{
		$request_result= $this->std_PUT('/api/Requirements/PutRequirement',$fields);
		return $this->ProcessRequestResponce($request_result);
	}

	public function GetRequirement($requirement_id)
	{
		$request_result= $this->std_GET("/api/Requirements/GetRequirement?Id=$requirement_id");
		return $this->ProcessRequestResponce($request_result);
	}

	public function UpdateProjectWithBlocks($fields)
	{
		$request_result= $this->std_PUT('/api/Projects/UpdateProjectWithBlocks',$fields);
		return $this->ProcessRequestResponce($request_result);
	}

	public function PutParticipant($fields)
	{
		$request_result= $this->std_PUT('/api/Participants/PutParticipant',$fields);
		return $this->ProcessRequestResponce($request_result);
	}

	public function PutProperty($fields)
	{
		$request_result= $this->std_PUT('/api/Properties/PutProperty',$fields);
		return $this->ProcessRequestResponce($request_result);
	}

	public function GetProperty($property_id)
	{
		$request_result= $this->std_GET("/api/Properties/GetProperty?Id=$property_id");
		return $this->ProcessRequestResponce($request_result);
	}

	public function GetParticipants_by_ContextSearchString($ContextSearchString)
	{
		$fields= (object)array
		(
			'ContextSearchString'=>$ContextSearchString
			,'Page'=>1
			,'PageSize'=>999
			,'Projects'=>array()
			,'RoleIds'=>array()
			,'Types'=>array()
		);
		$request_result= $this->std_POST('/api/participants/GetParticipants',$fields);
		return $this->ProcessRequestResponce($request_result);
	}

	public function GetRolledParticipants_Кредитор_физ_лицо($project_id,$i)
	{
		$fields= (object)array
		(
			'ContextSearchString'=>"{$i}й"
			,'Page'=>1
			,'PageSize'=>999
			,'Projects'=>array($project_id)
			,'RoleIds'=>array('8d7b1aad-50fc-e711-80bd-0cc47a7b67ab'/* кредиторы */)
			,'Types'=>array('c260f96f-45fc-e711-80bd-0cc47a7b67ab'/* физическое лицо */)
		);
		$request_result= $this->std_POST('/api/participants/GetRolledParticipants',$fields);
		return $this->ProcessRequestResponce($request_result);
	}

	public function GetProperties($project_id)
	{
		$request_result= $this->std_POST("/api/Properties/GetPropertiesByFilter",array
		(
			'TypeIds'=>array()
			,'MarketPriceMin'=>null
			,'MarketPriceMax'=>null
			,'ProjectId'=>$project_id
			,'Page'=>1
			,'PageSize'=>999
		));
		return $this->ProcessRequestResponce($request_result);
	}

	public function SaveRelatedObjects($fields)
	{
		$request_result= $this->std_PUT("/api/RelatedObjects/SaveRelatedObjects",$fields);
		$this->ParseProcessRequestResponce($request_result);
	}

	public function SaveRelatedObjects_RequirementToProperties($requirement_id,$property_id,$property_name)
	{
		$this->SaveRelatedObjects((object)array
		(
			'Id'=>$requirement_id
			,'RelatedObjects'=>array
			(
				(object)array
				(
					'Description'=>'Залоговое'
					,'Id'=>$property_id
					,'Name'=>$property_name
				)
			)
			,'Type'=>'RequirementToProperties'
		));
	}

	public function GetRelatedObjects_RequirementToProperties($property_id)
	{
		$request_result= $this->std_GET("/api/RelatedObjects/GetRelatedObjects?Criterion.id=$property_id&Criterion.type=RequirementToProperties");
		return $this->ProcessRequestResponce($request_result);
	}

	public function FindRtkVersions($project_id)
	{
		$request_result= $this->std_POST("/api/Requirements/FindRtkVersions",array
		(
			'Name'=>''
			,'Source'=>array
			(
				'SysName'=>'CurrentVersion'
				,'Id'=>'f99b8327-c618-e911-80bd-0cc47ac5977f'
				,'Name'=>'Рабочая версия РТК'
			)
			,'ProjectId'=>$project_id
			,'Page'=>1
			,'PageSize'=>20
		));
		return $this->ProcessRequestResponce($request_result);
	}

	public function DeleteParticipant($id)
	{
		$domain= $this->domain;
		$request_result= $this->rest->request(array(
			CURLOPT_URL =>  "$domain/api/participants/DeleteParticipant/$id",
			CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
			CURLOPT_CUSTOMREQUEST => 'DELETE'
		));
		return $this->ParseProcessRequestResponce($request_result);
	}
}

global $ProjectType;
$ProjectType= (object)array
(
	'Банкротство_организаций_и_КФХ' => array
	(
		"SysName" => "Bankruptcy",
		"Id" => "e1ad4a7a-e23a-4531-8b91-96c8c7a9ce90",
		"Name" => "Банкротство организаций и КФХ"
	)
	,'Банкротство_граждан' => array
	(
		"SysName" => "BankruptcyPerson",
		"Id" => "c8fb540d-1d86-4205-939c-b9fc2ea9740c",
		"Name" => "Банкротство граждан"
	)
);

function projectBody($ptype,$ProjectName,$Responsible_UserProfile)
{
	global $ProjectType;

	$pptype= null;
	switch ($ptype)
	{
		case 'РД':
		case 'РИ':
		case 'Реструктуризация долгов':
		case 'Реализация имущества':
			$pptype= $ProjectType->Банкротство_граждан;
			break;
		case 'Н':
		case 'КП':
		case 'Наблюдение':
		case 'Конкурсное производство':
			$pptype= $ProjectType->Банкротство_организаций_и_КФХ;
			break;
	}

	return array
	(
		'ProjectGroup' => null,
		'Responsible' => array
		(
			'Id' => $Responsible_UserProfile->Id,
			'Name' => $Responsible_UserProfile->DisplayName
		),
		'Name' => $ProjectName,
		'ProjectType'=>$pptype
	);
}

function Тип_процедуры_value($Тип_процедуры)
{
	switch ($Тип_процедуры)
	{
		case 'Наблюдение': 
		case 'Н': 
			return (object)array
			(
				'Id'=>'ef2878a2-42fc-e711-80bd-0cc47a7b67ab'
				,'SysName'=>'Observation'
				,'Name'=>'Наблюдение'
			);
		case 'Конкурсное производство': 
		case 'КП': 
			return (object)array
			(
				'Id'=>'f22878a2-42fc-e711-80bd-0cc47a7b67ab'
				,'SysName'=>'Receivership'
				,'Name'=>'Конкурсное производство'
			);
		case 'Реструктуризация долгов гражданина': 
		case 'Реструктуризация долгов': 
		case 'РД': 
			return (object)array
			(
				'Id'=>'3cf5c078-b7b4-e811-80bd-0cc47ac5977f'
				,'SysName'=>'RestructuringCitizenDebts'
				,'Name'=>'Реструктуризация долгов гражданина'
			);
		case 'Реализация имущества гражданина': 
		case 'Реализация имущества': 
		case 'РИ': 
			return (object)array
			(
				'Id'=>'3df5c078-b7b4-e811-80bd-0cc47ac5977f'
				,'SysName'=>'RealizationPropertyCitizen'
				,'Name'=>'Реализация имущества гражданина'
			);
	}
}

function project_VisualBlock($параметры_процедуры)
{
	$res= (object)array
	(
		'VisualBlockId' => 'bea76ebc-4da7-4b55-9570-8061bb786a92'
		,'Lines'=>array
		(
			/*array
			(
				'BlockLineId'=>'3da720bd-dc39-4861-9bad-82db25a5c6ee'
				,'Values'=>array
				(
					array
					(
						'VisualBlockProjectFieldId'=>'27615afc-91bc-4a77-884b-01b9729d8cbf'
						,'Value' => $параметры_процедуры->Номер_судебного_дела
					)
				)
			)*/
		)
	);
	if (isset($параметры_процедуры->Тип_процедуры))
	{
		$Тип_процедуры= $параметры_процедуры->Тип_процедуры;
		$res->Lines[]= array
			(
				'BlockLineId'=>'a5315715-7647-458e-826e-b1b36b2b917b'
				,'Values'=>array
				(
					array
					(
						'VisualBlockProjectFieldId'=>'fd3c9482-fd03-4766-89e9-d0bfe6856cdd'
						,'Value' => Тип_процедуры_value($Тип_процедуры)
					)
				)
			);
	}
	return $res;
}

function fields_from_ProjectCustomValues($ProjectCustomValues)
{
	$Blocks= $ProjectCustomValues->Blocks;
	if (0==count($Blocks))
	{
		return null;
	}
	else
	{
		$Lines= $Blocks[0]->Lines;
		if (0==count($Lines))
		{
			return null;
		}
		else
		{
			$res= (object)array();
			foreach ($Lines as $line)
			{
				switch ($line->BlockLineId)
				{
					case '3da720bd-dc39-4861-9bad-82db25a5c6ee':
						if (!isset($line->Values[0]->Value))
							throw new Exception('can not get Номер_судебного_дела!');
						$res->Номер_судебного_дела= $line->Values[0]->Value;
						break;
					case 'a5315715-7647-458e-826e-b1b36b2b917b':
						if (!isset($line->Values[0]->Value->Name))
							throw new Exception('can not get Тип_процедуры!');
						$res->Тип_процедуры= $line->Values[0]->Value->Name;
						break;
				}
			}
			return $res;
		}
	}
}

function Номер_дела_из_ProjectCustomValues($ProjectCustomValues)
{
	$fields= fields_from_ProjectCustomValues($ProjectCustomValues);
	return null!=$fields && isset($fields->Номер_судебного_дела) ? $fields->Номер_судебного_дела
		: (isset($ProjectCustomValues->CasebookNumber) ? $ProjectCustomValues->CasebookNumber
		: '');
}

function fix_participantBody($лицо,$participantBody)
{
	if (isset($лицо->ОГРН))
		$participantBody->OGRN= $лицо->ОГРН;
	if (isset($лицо->ИНН))
		$participantBody->INN= $лицо->ИНН;

	if (isset($лицо->Адрес))
	{
		$ContactDetail= (object)array();
		if (isset($лицо->Адрес))
			$ContactDetail->Address= $лицо->Адрес;
		$participantBody->ContactDetail= $ContactDetail;
	}

	return $participantBody;
}

function participantBody_Юридическое_лицо($Организация,$phones= null)
{
	$participantBody= (object)array
	(
		'Type' => array
		(
			'NameEn' => 'company'
			,'Id' => 'c160f96f-45fc-e711-80bd-0cc47a7b67ab'
			,'Name' => 'Организация'
			,'template' => 'participants/company_form.html'
			,'action' => 'add'
		)
		,'IncludeInProjectId' => null
		,"Organization" => $Организация->Наименование
	);

	$res= fix_participantBody($Организация,$participantBody);

	if (null!=$phones)
	{
		if (!isset($res->ContactDetail))
		{
			$res->ContactDetail= (object)array('Phone'=>$phones);
		}
		else
		{
			$res->ContactDetail->Phone= $phones;
		}
	}

	return $res;
}

function participantBody_Физическое_лицо($Физическое_лицо)
{
	$participantBody= (object)array
	(
		'Type' => array
		(
			'NameEn' => 'individual'
			,'Id' => 'c260f96f-45fc-e711-80bd-0cc47a7b67ab'
			,'Name' => 'Физическое лицо'
			,'template' => 'participants/individual_form.html'
			,'action' => 'add'
		)
		,'IncludeInProjectId' => null
		,"LastName" => $Физическое_лицо->Фамилия
		,'FirstName' => $Физическое_лицо->Имя
		,'MiddleName' => $Физическое_лицо->Отчество
	);

	return fix_participantBody($Физическое_лицо,$participantBody);
}

function Процедура_физического_лица($Тип_процедуры)
{
	return in_array($Тип_процедуры,array(
		'РД'
		,'Реструктуризация долгов'
		,'Реструктуризация долгов гражданина'
		,'Реализация имущества гражданина'
		,'Реализация имущества'
		,'РИ'));
}

function blocksForNewProject($Должник,$created_project,$project_name,$UserProfile,$isNaturalPerson,$параметры_процедуры)
{
	$blocks= array(
		 'Client'=>array('Id'=>$Должник->Id,'Name'=>$Должник->DisplayName)
		,'Id'=>$created_project->Id
		,'Name' => $project_name
		,'CasebookNumber'=>$параметры_процедуры->Номер_судебного_дела
		,'Responsible'=>$UserProfile
		,'Stage'=>
			array('Id'=>$isNaturalPerson
						? 'e12e8921-c618-e911-80bd-0cc47ac5977f'
						: 'c12e8921-c618-e911-80bd-0cc47ac5977f'
					,'Name' => 'Начальная')
		,'Blocks'=>array(project_VisualBlock($параметры_процедуры))
		,'Images'=>array()
	);
	return $blocks;
}