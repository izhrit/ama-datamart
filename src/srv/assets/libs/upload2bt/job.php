<?php

require_once '../assets/config.php';

global $debtorName_beauty_prefixes;
require_once '../assets/libs/upload2bt/logger.php';
require_once '../assets/helpers/client.rest.php';
require_once '../assets/libs/upload2bt/client.bt.php';
require_once '../assets/actions/backend/constants/debtorName.etalon.php';
require_once '../assets/actions/backend/constants/alib_constants.php';
require_once '../assets/libs/upload2bt/uploader.bt.php';
require_once '../assets/libs/upload2bt/upload_from_db.php';
require_once '../assets/libs/upload2bt/uploader.mock.php';
require_once '../assets/libs/codecs/registry.codec.php';
require_once '../assets/libs/upload2bt/upload_registry.php';

function upload_to_bankrotech()
{
	global $default_db_options;
	echo job_time()."   host:{$default_db_options->host}, user:{$default_db_options->user}, dbname:{$default_db_options->dbname}\r\n";
	$uploader= new Registry2bt_uploader(new Bankro_tech_client(new Logged_rest_client(new Logger(), /*$atimeout_sec= */30)));
	return upload($uploader);
}