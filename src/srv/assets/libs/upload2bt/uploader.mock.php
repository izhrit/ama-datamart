<?php

class Uploader
{
	private $_logger= null;

	public function __construct($logger)
	{
		$this->_logger= $logger;
	}

	public function logger()
	{
		return $this->_logger;
	}

	public function Использовать_сессию_для($параметры_доступа)
	{
		global $test_bt_account;
		if (isset($test_bt_account))
		{
			if ('unexisted-login'==$параметры_доступа->login)
				throw new Exception("unsuccessful login not equal_test_account");
			if ('exceptioned-login'==$параметры_доступа->login)
				throw new Exception("test exception!");
			if ('limited-account'==$параметры_доступа->login)
			{
				throw new Rest_exception
				(
					"got HTTP_CODE=400, curl_error=",
					'{"Error":"На вашем аккаунте достигнуто ограничение по количеству активных дел. Улучшите вашу подписку для увеличения количества активных дел в сервисе.\r\nИзменить подписку можно в Настройках аккаунта","ErrorHeader":"Невозможно создать дело","ErrorType":"Subscription","Warning":null,"WarningHeader":null,"WarningType":null,"IsSuccess":false,"Errors":[],"ValidationErrors":[],"ValidationWarnings":[]}'
				);
			}
		}
		return null;
	}
}

class Registry_uploader extends Uploader
{
	private $id_generated= 0;

	public function Подготовить_процедуру($параметры_процедуры)
	{
		$this->id_generated++;
		return $this->id_generated;
	}

	public function Подготовить_кредитора($id_Процедуры,$параметры_кредитора,$iКредитор)
	{
		$this->id_generated++;
		return $this->id_generated;
	}

	public function Добавить_требование($id_Процедуры,$id_Кредитора,$параметры_требования,$номер_требования)
	{
		$this->id_generated++;
		return $this->id_generated;
	}

	public function Добавить_залог($id_Процедуры,$id_Кредитора,$id_Требования,$параметры_залога)
	{
	}
}
