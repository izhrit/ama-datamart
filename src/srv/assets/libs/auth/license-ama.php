<?php

function Получить_авторизационные_данные_абонента_из_БД_а_если_надо_зарегистрировать($ContractNumber)
{
	$txt_query= "select ContractNumber, id_Contract from Contract where ContractNumber=?;";
	$rows= execute_query($txt_query,array('s',$ContractNumber));
	$rows_count= count($rows);
	if (1==$rows_count)
	{
		return $rows[0];
	}
	else if (0!=$rows_count)
	{
		write_to_log("found $rows_count rows for ContractNumber=$ContractNumber");
		return null;
	}
	else
	{
		$txt_query= "insert into Contract set ContractNumber=?;";
		$id_Contract= execute_query_get_last_insert_id($txt_query,array('s',$ContractNumber));
		return (object)array('ContractNumber'=>$ContractNumber,'id_Contract'=>$id_Contract);
	}
}

function Получить_все_возможные_авторизационные_записи_АУ_из_БД($ContractNumber,$manager)
{
	$txt_query= "select 
		  m.firstName firstName
		, m.lastName lastName
		, m.middleName middleName
		, m.efrsbNumber efrsbNumber
		, m.INN INN
		, m.id_Manager id_Manager
		, m.id_Contract id_Contract
		, m.ArbitrManagerID ArbitrManagerID
		, m.HasIncoming
		, c.ContractNumber ContractNumber
	from Manager m
	inner join Contract c on c.id_Contract=m.id_Contract
	where c.ContractNumber=?";

	$args= array('s',$ContractNumber);
/*	if (isset($manager->inn) && null!=$manager->inn && ''!=$manager->inn)
	{
		$txt_query.= ' and m.INN=?';
		$args[]= $manager->inn;
		$args[0].= 's';
	}
*/
	if (isset($manager->efrsb_number) && null!=$manager->efrsb_number && ''!=$manager->efrsb_number)
	{
		$txt_query.= ' and m.efrsbNumber=?';
		$args[]= $manager->efrsb_number;
		$args[0].= 's';
	}
	if (count($args)<3)
	{
		write_to_log("too little info to auto login for manager:");
		write_to_log($manager);
		return null;
	}
	$txt_query.= ';';
	return execute_query($txt_query,$args);
}

function Зарегистрировать_авторизационные_данные_АУ_в_БД($ContractNumber,$manager)
{
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		$args= array('s',$ContractNumber);
		$rows= $connection->execute_query("select id_Contract from Contract where ContractNumber=?;",$args);
		$rows_count= count($rows);

		$id_Contract= (1==$rows_count) ? $rows[0]->id_Contract
			: $connection->execute_query_get_last_insert_id("insert into Contract set ContractNumber=?;",$args);

		$txt_query= "insert into Manager set id_Contract=?, efrsbNumber=?, INN=?, firstName=?, lastName=?, middleName=?;";
		$id_Manager= $connection->execute_query_get_last_insert_id($txt_query,
			array('ssssss',$id_Contract,$manager->efrsb_number,$manager->inn,$manager->name,$manager->surname,$manager->patronymic));

		$connection->commit();

		return (object)array
		(
			'id_Contract'=>$id_Contract
			,'ContractNumber'=>$ContractNumber
			,'id_Manager'=>$id_Manager
			,'firstName'=>$manager->name
			,'lastName'=>$manager->surname
			,'middleName'=>$manager->patronymic
			,'INN'=>$manager->inn
			,'ArbitrManagerID'=>null
			,'efrsbNumber'=>$manager->efrsb_number
		);
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
}

function Получить_авторизационные_данные_АУ_из_БД_а_если_надо_зарегистрировать($ContractNumber,$manager)
{
	$rows= Получить_все_возможные_авторизационные_записи_АУ_из_БД($ContractNumber,$manager);
	if (null===$rows)
		return null;
	$rows_count= count($rows);
	if (1==$rows_count)
	{
		return $rows[0];
	}
	else if (0!=$rows_count)
	{
		write_to_log("found $rows_count rows for ContractNumber=$ContractNumber and Manager:");
		write_to_log($manager);
		return null;
	}
	else
	{
		return Зарегистрировать_авторизационные_данные_АУ_в_БД($ContractNumber,$manager);
	}
}

function Сервер_лицензий_по_токену_подтверждает_информацию($license_token, $ContractNumber, $manager= null)
{
	global $use_server_license_url;

	$ip= getRealIPAddr();
	$url= $use_server_license_url 
		. "?action=validateTokenForDataMart"
		. "&license_token=$license_token"
		. "&contract=$ContractNumber"
		. "&ip=$ip"
	;

	if (null==$manager)
	{
		$url.= '&user=customer';
	}
	else
	{
		$url.= "&user=manager"
			. "&lastName=$manager->surname"
			. "&firstName=$manager->name"
			. "&middleName=$manager->patronymic"
			. "&inn=$manager->inn"
			. "&efrsb_number=$manager->efrsb_number"
		;
	}

	write_to_log("get $url");
	$response_txt = file_get_contents($url);
	write_to_log("got $response_txt");
	$response=json_decode($response_txt);
	if (isset($response->status) && 'true'==$response->status)
	{
		return true;
	}
	else 
	{
		write_to_log("server license url:\r\n$url");
		write_to_log("returns:");
		write_to_log($response);
		return false;
	}
}

function Авторизоваться_по_информации_токена_короткоживущей_лицензиии($auth_args)
{
	if (!isset($auth_args->manager))
	{
		if (!Сервер_лицензий_по_токену_подтверждает_информацию($auth_args->license_token, $auth_args->ContractNumber))
			auto_login_fail('can not check admin license token with license server!',$auth_args);
		$auth_info_to_login= Получить_авторизационные_данные_абонента_из_БД_а_если_надо_зарегистрировать($auth_args->ContractNumber);
		if (null==$auth_info_to_login)
			auto_login_fail('can not find or create contract in the db!',$auth_args);
		auto_login_ok($auth_info_to_login,'customer');
	}
	else 
	{
		$auth_args_manager= $auth_args->manager;
		if (!Сервер_лицензий_по_токену_подтверждает_информацию($auth_args->license_token, $auth_args->ContractNumber, $auth_args_manager))
			auto_login_fail('can not check manager license token with license server!',$auth_args);
		$auth_info_to_login= Получить_авторизационные_данные_АУ_из_БД_а_если_надо_зарегистрировать($auth_args->ContractNumber,$auth_args_manager);
		if (null==$auth_info_to_login)
			auto_login_fail('can not find or create manager in the db!',$auth_args);
		auto_login_ok($auth_info_to_login,'manager');
	}
}
