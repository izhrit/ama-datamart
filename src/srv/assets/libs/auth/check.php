<?

function SafeGetAuth()
{
	session_start();
	return (!isset($_SESSION['auth_info'])) ? null : $_SESSION['auth_info'];
}

function CheckAuth()
{
	$auth= SafeGetAuth();
	if (null==$auth)
	{
		header("HTTP/1.1 401 Unauthorized");
		write_to_log("wrong access without auth_info");
		write_to_log_named('SESSION',$_SESSION);
		write_to_log_named('$_SERVER[SERVER_NAME]',$_SERVER['SERVER_NAME']);
		exit;
	}
	return $auth;
}

function write_to_log_auth_info()
{
	global $auth_info;
	write_to_log('for auth info:');
	write_to_log($auth_info);
}

function CheckAuth_cmd_Category($cmd_category)
{
	global $auth_info;
	if (!isset($auth_info))
		$auth_info= CheckAuth();
	if (!in_array($auth_info->category,$cmd_category[$_GET['cmd']]))
		exit_unauthorized('bad cmd!');
	return $auth_info;
}

function CheckAuthCategory($category)
{
	global $auth_info;
	if (!isset($auth_info))
		$auth_info= CheckAuth();
	if (!in_array($auth_info->category,$category))
		exit_unauthorized('bad role!');
	return $auth_info;
}

function CheckAuthCustomer()
{
	return CheckAuthCategory(array('customer'));
}

function CheckAuthManager()
{
	return CheckAuthCategory(array('manager'));
}

function CheckAuthSRO()
{
	return CheckAuthCategory(array('sro'));
}

function CheckAuthDebtor()
{
	return CheckAuthCategory(array('debtor'));
}

function CheckAuthCustomerOrManager()
{
	return CheckAuthCategory(array('manager','customer'));
}

function CheckAuthCustomerOrManagerOrAdmin()
{
	return CheckAuthCategory(array('manager','customer','admin'));
}

function CheckAuthCustomerOrManagerOrSRO()
{
	return CheckAuthCategory(array('manager','customer','sro'));
}
function CheckAuthCustomerOrManagerOrSROOrViewer()
{
	return CheckAuthCategory(array('manager','customer','sro','viewer'));
}

function CheckAuthCustomerOrSRO()
{
	return CheckAuthCategory(array('customer','sro'));
}

function CheckAuthCustomerOrViewer()
{
	return CheckAuthCategory(array('customer','viewer'));
}

function CheckAuthAdmin()
{
	return CheckAuthCategory(array('admin'));
}

function CheckAuthCustomerOrManagerOrViewer()
{
	return CheckAuthCategory(array('manager','customer','viewer'));
}

function CheckAuthViewer()
{
	return CheckAuthCategory(array('viewer'));
}

function CheckAuthViewerOrManager()
{
	return CheckAuthCategory(array('viewer','manager'));
}

function CheckCustomerAccessToContract($auth_info, $id_Contract)
{
	if ('customer'!=$auth_info->category || $id_Contract != $auth_info->id_Contract)
		exit_unauthorized("wrong access to id_Contract=$id_Contract");
}

function CheckAccessToManagerIfManager($auth_info, $id_Manager)
{
	if ('manager'==$auth_info->category && $id_Manager != $auth_info->id_Manager)
		exit_unauthorized("wrong access to id_Manager=$id_Manager");
}

function CheckAccessToViewerIfViewer($auth_info, $id_MUser)
{
	if ('viewer'==$auth_info->category && $id_MUser != $auth_info->id_MUser)
		exit_unauthorized("wrong access to id_MUser=$id_MUser");
}

function CheckAccessToSROIfSRO($auth_info, $id_SRO)
{
	if ('sro'==$auth_info->category && $id_SRO != $auth_info->id_SRO)
		exit_unauthorized("wrong access to id_SRO=$id_SRO");
}

function CheckAccessToSROIfSROByRegNum($auth_info, $id_SRO)
{
	if ('sro'==$auth_info->category && $id_SRO != $auth_info->RegNum)
		exit_unauthorized("wrong access to id_SRO=$id_SRO");
}