<?php

function Получить_авторизационные_данные_абонента_из_БД($viewer_email)
{
	$txt_query= "select UserName Имя, UserEmail Email, id_MUser id_MUser, HasOutcoming from MUser where UserEmail=?;";
	$rows= execute_query($txt_query,array('s',$viewer_email));
	$rows_count= count($rows);
	if (1==$rows_count)
	{
		return $rows[0];
	}
	else
	{
		write_to_log("found $rows_count rows for UserEmail=$viewer_email");
		return null;
	}
}

function Сервер_лицензий_по_токену_подтверждает_информацию_фа($license_token, $email)
{
	global $use_server_license_url;
	$ip= getRealIPAddr();
	$url= $use_server_license_url 
		. "?product=FA"
		. "&action=validateTokenForDataMart"
		. "&license_token=$license_token"
		. "&email=$email"
		. "&ip=$ip"
	;

	write_to_log("get $url");
	$response_txt = file_get_contents($url);
	write_to_log("got $response_txt");
	$response=json_decode($response_txt);
	if (isset($response->status) && 'true'==$response->status)
	{
		return true;
	}
	else 
	{
		write_to_log("server license url:\r\n$url");
		write_to_log("returns:");
		write_to_log($response);
		return false;
	}
}

function Авторизоваться_по_информации_токена_короткоживущей_лицензиии($auth_args)
{
	if (!Сервер_лицензий_по_токену_подтверждает_информацию_фа($auth_args->license_token, $auth_args->email))
		auto_login_fail('can not check viewer license token with license server!',$auth_args);
	$auth_info_to_login= Получить_авторизационные_данные_абонента_из_БД($auth_args->email);
	if (null==$auth_info_to_login)
		auto_login_fail('can not find viewer in the db!',$auth_args);
	auto_login_ok($auth_info_to_login,'viewer');
}
