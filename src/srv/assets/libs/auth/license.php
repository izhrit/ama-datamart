<?php

function decrypt_auth($auth, $autologin_auth_options)
{
	$bt_urldecoded= urldecode($auth);
	$bt= base64_decode(str_replace(' ','+',$bt_urldecoded));

	$decrypted_text= sync_decrypt($bt,$autologin_auth_options);
	if (!$decrypted_text)
		return null;
	$auth_info= json_decode($decrypted_text);
	return $auth_info;
}

function Проверить_актуальность_авторизационных_данных_для_входа($auth_args)
{
	$current_time= safe_date_create();
	$auth_time= new DateTime($auth_args->time, new DateTimeZone('UTC'));

	$auth_age= abs($current_time->format('U') - $auth_time->format('U'));
	if ($auth_age > (60*15)) // UNIX секунды..
	{
		$current_time_txt= date_format($current_time,'Y-m-d\TH:i:s');
		auto_login_fail("time is wrong! server time is $current_time_txt, auth age is $auth_age",$auth_args);
	}
}

