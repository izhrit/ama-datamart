<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/job.php';
require_once '../assets/actions/backend/alib_emails.php';

function deliver_manager_request($row, $requests)
{
	$letter= (object)array('subject'=>'Уведомление о запросах');

	$ФИО   = $row->lastName. ' ' . $row->firstName . ' ' . $row->middleName;
	$СРО   = 'СРО "'.$row->ShortTitle.'"';
	$После =  isset($row->timeAftereMail) ? $row->timeAftereMail : $row->timeAfter;

	ob_start();
	include "../assets/libs/requests/letters/manager_requests_letter.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();
	if (false==PostLetter($letter, $row->ManagerEmail, $row->firstName, 'уведомление о запросах в сро', $row->id_Manager))
	{
		echo job_time() . "can not post letter to {$row->id_Manager}!\r\n";
	}else {
		return 1;
	}
}

function deliver_manager_requests($static_date_time= null)
{
	if (null==$static_date_time)
		return 'отключено';

	echo job_time() . " query from Manager to send requests (where DateOfCreation > TimeDispatch IS NULL)..\r\n";

	$manager_requests_rows = get_manager_requests();

	$counted_rows = count($manager_requests_rows);
	echo job_time() . " got $counted_rows rows from Mrequest to send\r\n";
	
	$prepared_to_send = 0;
	foreach ($manager_requests_rows as $key=>$value)
	{

		echo job_time() . " query for {$manager_requests_rows[$key]->id_Manager} id_Manager ..\r\n";

		try
		{
			$CaseNumbers = explode("++", $manager_requests_rows[$key]->CaseNumbers);
			$DebtorNames = explode("++", $manager_requests_rows[$key]->DebtorNames);
			$CourtDecisionURLs = explode("++", $manager_requests_rows[$key]->CourtDecisionURLs);
			$reqests = array();
			for($i = 0; $i < count($CaseNumbers); ++$i) {
				$reqests[$i][] = $DebtorNames[$i];
				$reqests[$i][] = $CaseNumbers[$i];
				$reqests[$i][] = $CourtDecisionURLs[$i];
			}
			$prepared_to_send += deliver_manager_request($manager_requests_rows[$key], $reqests);
		}
		catch (Exception $ex)
		{
			echo job_time() . ' catched exception: ' . get_class($ex) . ' - ' . $ex->getMessage() . "\r\n";
		}
	}
	echo job_time() . " $prepared_to_send prepared messages to send ..\r\n";
}

function get_manager_requests()
{
	$time= job_time();
	$time= date_create_from_format('Y-m-d\TH:i:s',$time);
	$time= date_format($time,'Y-m-d H:i:s');
	$time= "'$time'";
	$txt_query  = "select 
					(select max(e.TimeDispatch) 
					from SentEmail e 
					where m.id_Manager = e.RecipientId 
					and e.RecipientType = 'm' and e.EmailType = 'y') timeAftereMail
					,$time - interval 1 day timeAfter
					,m.id_Manager
					,m.firstName
					,m.lastName
					,m.middleName
					,m.ManagerEmail
					,sro.ShortTitle
					,group_concat(mr.CaseNumber ORDER BY mr.CaseNumber DESC SEPARATOR '++' ) CaseNumbers
					,group_concat(mr.DebtorName ORDER BY mr.DebtorName DESC SEPARATOR '++' ) DebtorNames
					,group_concat(mr.CourtDecisionURL ORDER BY mr.CourtDecisionURL DESC SEPARATOR '++' ) CourtDecisionURLs
				from Manager m
				inner join efrsb_sro sro on m.id_SRO = sro.id_SRO
				inner join mrequest mr on sro.id_SRO = mr.id_SRO
				where mr.DateOfCreation > (select max(e.TimeDispatch) 
					from SentEmail e 
					where m.id_Manager = e.RecipientId 
					and e.RecipientType = 'm' and e.EmailType = 'y') 
				OR mr.DateOfCreation > $time - interval 1 day
				group by m.id_Manager;";

	$rows= execute_query($txt_query, array());
	return $rows;
}
