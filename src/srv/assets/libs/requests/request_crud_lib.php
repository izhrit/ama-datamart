<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/qb.php';
require_once '../assets/helpers/time.php';

class manager_request_query_builder extends query_builder
{
	private function Должник_физ_лицо_comma_separated_params($Должник, $num='')
	{
		$this->comma_par('DebtorCategory' . $num,'n');
		$Физ_лицо =  (isset($Должник->Физ_лицо)) ? (object) $Должник->Физ_лицо : (object) array();
		$DebtorName= '';
		if (isset($Физ_лицо->Фамилия))
			$DebtorName.= $Физ_лицо->Фамилия;
		if (isset($Физ_лицо->Имя))
			$DebtorName.= ' '.$Физ_лицо->Имя;
		if (isset($Физ_лицо->Отчество))
			$DebtorName.= ' '.$Физ_лицо->Отчество;
		$this->comma_par('DebtorName' . $num, !isset($Физ_лицо->Фамилия) ? null : $DebtorName);
		$this->comma_par('DebtorSnils' . $num, !isset($Физ_лицо->СНИЛС) ? null : $Физ_лицо->СНИЛС);
	}

	private function Должник_общие_данные_comma_separated_params($Должник, $num='')
	{
		$this->comma_par('DebtorINN' . $num, !isset($Должник->ИНН) ? null : $Должник->ИНН);
		$this->comma_par('DebtorOGRN' . $num, !isset($Должник->ОГРН) ? null : $Должник->ОГРН);
		$this->comma_par('DebtorAddress' . $num, !isset($Должник->Адрес) ? null : $Должник->Адрес);
	}

	private function Должник_comma_separated_params($Должник)
	{
		switch ($Должник->Тип)
		{
			case 'Физ_лицо':
				$this->Должник_физ_лицо_comma_separated_params($Должник);
				$this->Должник_общие_данные_comma_separated_params($Должник);
				$this->Должник_физ_лицо_comma_separated_params((object) array(), 2);
				$this->Должник_общие_данные_comma_separated_params((object) array(), 2);
				break;
			case 'Юр_лицо':
				$this->comma_par('DebtorCategory','l');
				$this->comma_par('DebtorName',$Должник->Юр_лицо['Наименование']);
				$this->Должник_общие_данные_comma_separated_params($Должник);
				$this->Должник_физ_лицо_comma_separated_params((object) array(), 2);
				$this->Должник_общие_данные_comma_separated_params((object) array(), 2);
				break;
			case 'Супруги':
				$this->Должник_физ_лицо_comma_separated_params((object) $Должник->Супруги['Должник1']);
				$this->Должник_общие_данные_comma_separated_params((object) $Должник->Супруги['Должник1']);
				$this->Должник_физ_лицо_comma_separated_params((object) $Должник->Супруги['Должник2'], 2);
				$this->Должник_общие_данные_comma_separated_params((object) $Должник->Супруги['Должник2'], 2);
				break;
		}
	}

	private function Заявление_comma_separated_params($заявление)
	{
		$this->comma_select2('MRequest.id_Court',(object)$заявление->В_суд);
		if (isset($заявление->Дата_рассмотрения))
			$this->comma_ru_legal_date_time('MRequest.NextSessionDate',$заявление->Дата_рассмотрения);
	}

	private function Запрос_comma_separated_params($запрос)
	{
		if(isset($запрос->Время)) {
			$время = (object)$запрос->Время;
			if (isset($время->акта))
				$this->comma_ru_date('MRequest.DateOfRequestAct',$время->акта);
			if (isset($время->получения))
				$this->comma_ru_date('MRequest.DateOfRequest',$время->получения);
			if (isset($время->опроса))
				$this->comma_ru_date('MRequest.DateOfOffer',$время->опроса);
			if (isset($время->конец_опроса))
				$this->comma_ru_date('MRequest.DateOfOfferTill',$время->конец_опроса);
			if (isset($время->ответа))
				$this->comma_ru_date('MRequest.DateOfResponce',$время->ответа);

		}

		if(isset($запрос->Ссылка)){ 
			$this->comma_par('CourtDecisionURL',$запрос->Ссылка);
		}

		if(isset($запрос->Дополнительная_информация)){ 
			$this->comma_par('CourtDecisionAddInfo',$запрос->Дополнительная_информация);
		}
	}

	public function Согласия_parametrized($согласия, $mode) {
		if(is_array($согласия)) {
			$согласия_length = count($согласия);
		if($согласия_length === 1) {
			$согласие =(object)$согласия[0];
			if($mode == 'update') {
				$this->parametrized("\r\nand ProcedureStart.id_ProcedureStart=?", $согласие->id_ProcedureStart,'i');
			}else if($mode == 'create') {
				$this->parametrized("\r\nwhere ProcedureStart.id_ProcedureStart=?", $согласие->id_ProcedureStart,'i');
			}
		}
			
		if($согласия_length > 1) {
			foreach ($согласия as $k=>$согласие){
				$согласие = (object)$согласие;
				if($k == 0) {
					if($mode == 'update') {
						$this->parametrized("\r\nand (ProcedureStart.id_ProcedureStart=?", $согласие->id_ProcedureStart,'i');
					}else if($mode == 'create') {
						$this->parametrized("\r\nwhere (ProcedureStart.id_ProcedureStart=?", $согласие->id_ProcedureStart,'i');
					}
				}else if($k == $согласия_length-1) {
					$this->parametrized("\r\nor ProcedureStart.id_ProcedureStart=?)", $согласие->id_ProcedureStart,'i');
				} else {
					$this->parametrized("\r\nor ProcedureStart.id_ProcedureStart=?", $согласие->id_ProcedureStart,'i');
				}
			}
		}
		}
	}

	private function Заявитель_comma_separated_params($Заявитель)
	{
		if(isset($Заявитель->Заявитель_должник) && $Заявитель->Заявитель_должник==='true') {
			$this->comma_par('ApplicantCategory', null);
			$this->comma_par('ApplicantName',null);
			$this->comma_par('ApplicantSnils',null);
			$this->comma_par('ApplicantINN',null);
			$this->comma_par('ApplicantOGRN',null);
			$this->comma_par('ApplicantAddress',null);
		}
		else
		{
			switch ($Заявитель->Тип)
			{
				case 'Физ_лицо':
					$this->comma_par('MRequest.ApplicantCategory','n');
					$Физ_лицо= (object)$Заявитель->Физ_лицо;
					$ApplicantName= $Физ_лицо->Фамилия;
					if (isset($Физ_лицо->Имя))
						$ApplicantName.= ' '.$Физ_лицо->Имя;
					if (isset($Физ_лицо->Отчество))
						$ApplicantName.= ' '.$Физ_лицо->Отчество;
					$this->comma_par('MRequest.ApplicantName',$ApplicantName);
					$this->safe_comma_field_par('MRequest.ApplicantSnils',$Физ_лицо,'СНИЛС');
					break;
				case 'Юр_лицо':
					$this->comma_par('MRequest.ApplicantCategory','l');
					$this->comma_par('MRequest.ApplicantName',$Заявитель->Юр_лицо['Наименование']);
					break;
			}
			$this->safe_comma_field_par('MRequest.ApplicantINN',$Заявитель,'ИНН');
			$this->safe_comma_field_par('MRequest.ApplicantOGRN',$Заявитель,'ОГРН');
			$this->safe_comma_field_par('MRequest.ApplicantAddress',$Заявитель,'Адрес');
		}
	}

	public function comma_separated_params($request)
	{
		if (isset($request->Должник))
			$this->Должник_comma_separated_params((object)$request->Должник);

		if(isset($request->Заявитель))
			$this->Заявитель_comma_separated_params((object)$request->Заявитель);
			
		if (isset($request->Заявление_на_банкротство))
			$this->Заявление_comma_separated_params((object)$request->Заявление_на_банкротство);
		
		if (isset($request->Номер_судебного_дела))
			$this->comma_par('MRequest.CaseNumber',$request->Номер_судебного_дела);

		if (isset($request->Запрос))
			$this->Запрос_comma_separated_params((object)$request->Запрос);
			
		if(isset($request->Согласия_АУ)) {
			$Согласия_АУ = (object)$request->Согласия_АУ;
			if(isset($Согласия_АУ->В_отчет) && isset($Согласия_АУ->Согласия) && is_array($Согласия_АУ->Согласия)) {
				foreach($Согласия_АУ->Согласия as $согласие) {
					$согласие = (object) $согласие;
					if($согласие->id_ProcedureStart == $Согласия_АУ->В_отчет)
						$this->comma_par('MRequest.id_Manager',$согласие->АУ['id'],'i');
				}
			}else {
				$this->comma_par('MRequest.id_Manager',null);
			}
		}else {
			$this->comma_par('MRequest.id_Manager',null);
		}

	}
}

class request_helper
{
	function update_consents($id_SRO, $id_MRequest, $request)
	{
		$id_choosed_ProcedureStart= null;
		$ids_ProcedureStart= null;
		if (isset($request->Согласия_АУ))
		{
			$выбор= (object)$request->Согласия_АУ;
			if (isset($выбор->В_отчет))
				$id_choosed_ProcedureStart= $выбор->В_отчет;
			if (isset($выбор->Согласия) && is_array($выбор->Согласия) && 0!=count($выбор->Согласия))
			{
				$ids_ProcedureStart= array();
				foreach ($выбор->Согласия as $согласие)
					$ids_ProcedureStart[]= intval($согласие['id_ProcedureStart']);
				$ids_ProcedureStart= implode($ids_ProcedureStart,',');
			}
		}
		if (null==$ids_ProcedureStart)
		{
			$txt_query= "update ProcedureStart set id_SRO=null, id_MRequest= null where id_MRequest=?;";
			$affected_rows= execute_query_get_affected_rows($txt_query,array('s',$id_MRequest));
		}
		else if (null==$id_choosed_ProcedureStart)
		{
			$txt_query= "update ProcedureStart set 
				id_SRO= null, id_MRequest= if(not id_ProcedureStart in ($ids_ProcedureStart),null,?)
			where id_MRequest=? or id_ProcedureStart in ($ids_ProcedureStart);";
			$affected_rows= execute_query_get_affected_rows($txt_query,
				array('ss',$id_MRequest,$id_MRequest));
		}
		else
		{
			$txt_query= "update ProcedureStart set 
				id_SRO=if(id_ProcedureStart<>?,null,?) 
				,id_MRequest= if(not id_ProcedureStart in ($ids_ProcedureStart),null,?)
			where id_MRequest=? or id_ProcedureStart in ($ids_ProcedureStart);";
			$affected_rows= execute_query_get_affected_rows($txt_query,
				array('ssss',$id_choosed_ProcedureStart,$id_SRO,$id_MRequest,$id_MRequest));
		}
	}

	function get_consents_of_request($id_MRequest) 
	{
		$txt_query= "select m.id_Manager
				, concat(m.lastName,' ',left(m.firstName,1),'. ',left(m.middleName,1),'.') ManagerName
				, ps.id_ProcedureStart
				, DATE_FORMAT(ps.DateOfApplication, '%d.%m.%Y') DateOfApplication
				, ps.CaseNumber 
				, ps.id_SRO
			from ProcedureStart ps
			left join Manager m on ps.id_Manager=m.id_Manager
			where ps.id_MRequest=?
			order by ps.TimeOfCreate desc, ps.id_ProcedureStart
		";
		$params= array('i',$id_MRequest);
		$rows= execute_query($txt_query,$params);
		if (0==count($rows))
		{
			return array('Согласия'=>array());
		}
		else
		{
			$id_ProcedureStartToResponce = null;
			$consents = array();
			foreach($rows as $row) {
				$consent = array(
					'АУ'=>(null==$row->id_Manager) ? null : array('id'=>$row->id_Manager, 'text'=>$row->ManagerName)
					, 'id_ProcedureStart'=> $row->id_ProcedureStart
					, 'Заявление_на_банкротство'=> array(
						'Дата_подачи'=> $row->DateOfApplication
					)
					, 'Номер_судебного_дела'=> $row->CaseNumber
				);
				
				if(isset($row->id_SRO) && ''!=$row->id_SRO) {
					$id_ProcedureStartToResponce = $row->id_ProcedureStart;
					$consent['В_ответе'] =  $row->id_ProcedureStart;
				}
				array_push($consents, $consent);
			}
			
			return array(
				'Согласия' => $consents,
				'В_отчет' => $id_ProcedureStartToResponce
			);
		}
	}

	function request_row_to_debtor($row, $num='')
	{
		$debtor= (object)array('ИНН'=> $row->{'DebtorINN' . $num}, 'ОГРН'=> $row->{'DebtorOGRN' . $num}, 'Адрес'=> $row->{'DebtorAddress' . $num});
		if ('n'!=$row->{'DebtorCategory' . $num})
		{
			$debtor->Тип= 'Юр_лицо';
			$debtor->Юр_лицо= array('Наименование'=> $row->{'DebtorName' . $num});
		}
		else
		{
			$np= explode(' ',$row->{'DebtorName' . $num});
			$debtor->Тип= 'Физ_лицо';
			$debtor->Физ_лицо= array('Фамилия'=> $np[0], 'Имя'=> $np[1], 'Отчество'=> $np[2], 'СНИЛС'=> $row->{'DebtorSNILS' . $num});
		}
		return $debtor;
	}

	function request_row_to_applicant($row)
	{
		if(isset($row->ApplicantCategory)) {
			$applicant= (object)array('ИНН'=> $row->ApplicantINN, 'ОГРН'=> $row->ApplicantOGRN, 'Адрес'=> $row->ApplicantAddress);
			if ('n'!=$row->ApplicantCategory)
			{
				$applicant->Тип= 'Юр_лицо';
				$applicant->Юр_лицо= array('Наименование'=> $row->ApplicantName);
			}
			else
			{
				$np= explode(' ',$row->ApplicantName);
				$applicant->Тип= 'Физ_лицо';
				$applicant->Физ_лицо= array('Фамилия'=> $np[0], 'Имя'=> $np[1], 'Отчество'=> $np[2], 'СНИЛС'=> $row->ApplicantSNILS);
			}
			return $applicant;
		}else {
			return null;
		}
	}

	function check_court_decision($request, $key)
	{
		if(isset($request->{$key})) {
			$Судебный_акт = (object)$request->{$key};
			if(isset($Судебный_акт->Время)) {
				$время = (object)$Судебный_акт->Время;
				if((isset($время->акта) && ''!== trim($время->акта) && 'null'!==trim($время->акта)) && (!isset($Судебный_акт->Ссылка) || ''==trim($Судебный_акт->Ссылка))) {
					exit_bad_request("can not create/update MRequest: {$key}['Ссылка] required with {$key}['Время']['акта']!");
				}
			}
			if(isset($Судебный_акт->Ссылка) && ''!==trim($Судебный_акт->Ссылка)) {
				if(!isset($Судебный_акт->Время)) {
					exit_bad_request("can not create/update MRequest: {$key}['Время']['акта'] required with {$key}['Ссылка]!");
				} else {
					$время = (object)$Судебный_акт->Время;
					if(!isset($время->акта) || ''==trim($время->акта) && 'null'!==trim($время->акта)) {
						exit_bad_request("can not create/update MRequest: {$key}['Время']['акта'] required with {$key}['Ссылка]!");
					}
				}
			}
		}
	}
}

global $request_helper;
$request_helper = new request_helper();

function create_request($request)
{
	write_to_log('create_request {');
	global $request_helper;
	global $auth_info;
	$request = (object) $request;

	switch ($auth_info->category)
	{
		case 'sro': 
			$id_SRO = $auth_info->id_SRO;
			break;
		case 'manager':
		case 'customer':
			if(!isset($request->id_SRO))
				exit_bad_request("can't create MRequest by customer or manager without id_SRO");
			$id_SRO = $request->id_SRO;
			break;
	}

	$uiqb = new manager_request_query_builder('insert into MRequest set DateOfCreation=now(),');
	$uiqb->comma_separated_params($request);
	$uiqb->comma_par('id_SRO',$id_SRO);

	$id_MRequest= $uiqb->execute_query_get_last_insert_id();

	$request_helper->update_consents($id_SRO, $id_MRequest, $request);

	$create_request_result = array('id'=>$id_MRequest);
	write_to_log('create_request }');
	return $create_request_result;
}

function update_request($id_MRequest,$request)
{
	global $request_helper;
	global $auth_info;
	$id_SRO = $auth_info->id_SRO;

	$request = (object) $request;
	$request_helper->update_consents($id_SRO, $id_MRequest, $request);

	$uiqb= new manager_request_query_builder('update MRequest set');
	$uiqb->comma_separated_params((object)$request);
	if (isset($request->Согласия_АУ))
		$Согласия_АУ = (object)$request->Согласия_АУ;
	$uiqb->parametrized("\r\nwhere MRequest.id_MRequest=?",$id_MRequest,'i');
	$affected= $uiqb->execute_query_get_affected_rows();

	$update_request_result = array('affected'=>$affected);

	return $update_request_result;
}

function delete_requests($ids_MRequest)
{
	global $auth_info;
	$id_SRO = $auth_info->id_SRO;

	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	foreach ($ids_MRequest as $id_MRequest)
	{
		$uiqb= new manager_request_query_builder('update ProcedureStart set ');
		$uiqb->comma_par('id_MRequest', null);
		$uiqb->comma_par('id_SRO', null);
		$uiqb->comma_par('ReadyToRegistrate', 0, 'i');
		$uiqb->parametrized("\r\nwhere id_MRequest=?",$id_MRequest,'i');
		$detached_consents= $uiqb->execute_query_get_affected_rows($connection);

		$uiqb= new manager_request_query_builder('delete from MRequest');
		$uiqb->parametrized("\r\nwhere id_MRequest=?",$id_MRequest,'i');
		$uiqb->parametrized("\r\nand id_SRO=?",$id_SRO, 'i');
		$affected= $uiqb->execute_query_get_affected_rows($connection);
		if (1!=$affected)
		{
			$connection->rollback();
			write_to_log("can not delete MRequest where id_MRequest=$id_MRequest (affected $affected rows)");
			return false;
		}
	}
	$connection->commit();
	return true;
}

function read_request($id_MRequest)
{
	global $request_helper;
	global $auth_info;

	$txt_query= "select mr.DebtorName ,mr.DebtorSNILS, mr.DebtorINN, mr.DebtorOGRN, mr.DebtorAddress, mr.DebtorCategory
			, mr.DebtorName2 ,mr.DebtorSNILS2, mr.DebtorINN2, mr.DebtorOGRN2, mr.DebtorAddress2, mr.DebtorCategory2
			, mr.ApplicantName, mr.ApplicantSNILS, mr.ApplicantINN, mr.ApplicantOGRN, mr.ApplicantAddress, mr.ApplicantCategory
			,DATE_FORMAT(mr.DateOfRequest, '%d.%m.%Y') DateOfRequest
			,DATE_FORMAT(mr.DateOfRequestAct, '%d.%m.%Y') DateOfRequestAct
			,mr.CourtDecisionURL, mr.CourtDecisionAddInfo
			,DATE_FORMAT(mr.DateOfOffer, '%d.%m.%Y') DateOfOffer
			,DATE_FORMAT(mr.DateOfOfferTill, '%d.%m.%Y') DateOfOfferTill
			,DATE_FORMAT(mr.DateOfResponce, '%d.%m.%Y') DateOfResponce
			,c.id_Court, c.Name, mr.CaseNumber
			,DATE_FORMAT(mr.NextSessionDate, '%d.%m.%Y %H:%i') NextSessionDate
			,concat(m.lastName,' ',m.firstName,' ',m.middleName,' ') ManagerName
			,mr.id_Manager id_Manager
			,mr.id_MRequest id_MRequest
			,mr.id_SRO id_SRO
		from MRequest mr
		inner join Court c on mr.id_Court=c.id_Court
		left join Manager m on mr.id_Manager=m.id_Manager
		where mr.id_MRequest=?
	";
	$params= array('i',$id_MRequest);
	if($auth_info->category === 'sro') {
		$txt_query.= 'and mr.id_SRO=?';
		$params[0].= 'i';
		$params[]= $auth_info->id_SRO;
	}
	$rows= execute_query($txt_query,$params);
	
	if (0==count($rows))
	{
		return null;
	}
	else
	{
		$row= $rows[0];
		$consents_of_request = $request_helper->get_consents_of_request($id_MRequest);
		$request= array(
			'Заявитель'=>$request_helper->request_row_to_applicant($row)
			, 'id_MRequest'=>$row->id_MRequest
			, 'id_SRO'=>$row->id_SRO
			, 'Заявление_на_банкротство'=>array(
				'В_суд'=>(null==$row->id_Court)?null:array('id'=>$row->id_Court,'text'=>$row->Name)
				,'Дата_рассмотрения'=> $row->NextSessionDate
			)
			, 'Номер_судебного_дела'=> $row->CaseNumber
			, 'Запрос'=> array(
				"Ссылка"=>$row->CourtDecisionURL
				,'Время'=> array(
					'акта'=>$row->DateOfRequestAct
					, 'получен'=> null!=$row->DateOfRequest
					, 'получения'=>$row->DateOfRequest
					, 'опрошен_ау'=> null!=$row->DateOfOffer
					, 'опроса'=>$row->DateOfOffer
					, 'конец_опроса'=>$row->DateOfOfferTill
					, 'отвечен'=> null!=$row->DateOfResponce
					, 'ответа'=>$row->DateOfResponce
				)
				,'Дополнительная_информация' => $row->CourtDecisionAddInfo
			)
			, 'Запрос_дополнительная_информация' => array(
				'Дополнительная_информация' => $row->CourtDecisionAddInfo
			)
			, 'Согласия_АУ'=> array(
				'Согласия'=> $consents_of_request['Согласия']
			)
		);

		if(isset($row->DebtorName2)) {
			$request['Должник'] = array(
				'Тип' => 'Супруги'
				, 'Супруги' => array(
					'Должник1' => $request_helper->request_row_to_debtor($row)
					, 'Должник2' => $request_helper->request_row_to_debtor($row, 2)
				)
				);
		} else {
			$request['Должник'] = $request_helper->request_row_to_debtor($row);
		}
	
		if(isset($consents_of_request['В_отчет']))
			$request['Согласия_АУ']['В_отчет'] = $consents_of_request['В_отчет'];
		return $request;
	}
}

function validate_citizen_debtor($Должник) {
	if (!isset($Должник->Физ_лицо['Фамилия']))
	exit_bad_request("can not create ProcedureStart without DebtorName!");
	if (''==trim($Должник->Физ_лицо['Фамилия']))
		exit_bad_request("can not create ProcedureStart with empty DebtorName!");
}

function validate_request($request)
{
	global $request_helper;
	write_to_log('validate_request {');
	$request= (object)$request;

	if (!isset($request->Заявление_на_банкротство))
		exit_bad_request("can not create MRequest without Court!");
	
	if (!isset($request->Запрос))
		exit_bad_request("can not create MRequest without DateOfResponce!");

	$request->Запрос = (object)$request->Запрос;
	$request->Запрос->Время = (object)$request->Запрос->Время;
	if (!isset($request->Запрос->Время->получения))
		exit_bad_request("can not create MRequest without DateOfResponce!");

	$request->Заявление_на_банкротство = (object)$request->Заявление_на_банкротство;
	$request->Заявление_на_банкротство->В_суд = (object)$request->Заявление_на_банкротство->В_суд;
	if (!isset($request->Заявление_на_банкротство->В_суд->id))
		exit_bad_request("can not create MRequest without Court!");

	if (!isset($request->Должник))
		exit_bad_request("can not create MRequest without Debtor!");

	$request->Должник= (object)$request->Должник;
	$Должник= $request->Должник;

	if (!isset($Должник->Тип))
		exit_bad_request("can not create MRequest without Debtor Category!");

	$Тип= $Должник->Тип;
	switch ($Тип)
	{
		case 'Юр_лицо':
			if (!isset($Должник->Юр_лицо['Наименование']))
				exit_bad_request("can not create ProcedureStart without DebtorName!");
			if (''==trim($Должник->Юр_лицо['Наименование']))
				exit_bad_request("can not create ProcedureStart with empty DebtorName!");
			break;
		case 'Физ_лицо':
			validate_citizen_debtor($Должник);
			break;
		case 'Супруги':
			validate_citizen_debtor((object)$Должник->Супруги['Должник1']);
			validate_citizen_debtor((object)$Должник->Супруги['Должник2']);
			break;
		default:
			exit_bad_request("can not create ProcedureStart with unknown DebtorCategory!");
	}

	$request_helper->check_court_decision($request, 'Запрос');

	write_to_log('validate_request }');
	return $request;
}