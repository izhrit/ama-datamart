<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/job.php';

function delete_old_PData($max_PData_days_age= 60)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);
	$time_later_than= date_sub($current_date_time, date_interval_create_from_date_string($max_PData_days_age.' days'));
	$sql_time_later_than= date_format($time_later_than,'Y-m-d H:i:s');

	echo job_time() . " delete unused PData older than $max_PData_days_age days \r\n";
	echo job_time() . "  where publicDate < $sql_time_later_than days ..\r\n";
	$txt_query= 'delete pd
from PData pd
inner join MProcedure on pd.id_MProcedure=MProcedure.id_MProcedure
left join Leak on Leak.id_PData=pd.id_PData
left join PData pd1 on pd1.id_MProcedure=pd.id_MProcedure 
                   and month(pd.publicDate)=month(pd1.publicDate) 
                   and pd1.publicDate>pd.publicDate
where pd.revision<>MProcedure.revision 
   && pd.revision<>MProcedure.ctb_revision
   && id_Leak is null
   && pd.publicDate > ?
   && pd1.id_PData is not null;';
	$affected_rows= execute_query_get_affected_rows($txt_query,array('s',$sql_time_later_than));
	echo job_time() . " .. deleted $affected_rows PData rows.\r\n";

	return "удалено записей об устаревших выгрузках на витрину\r\n ранее $sql_time_later_than: $affected_rows";
}