<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/job.php';

function move_procedure_using_to_indexed()
{
	echo job_time() . " insert ProcedureUsing into ProcedureUsing_indexed ..\r\n";
	$txt_query= 'insert into ProcedureUsing_indexed 
		  (ContractNumber,INN,IP,OGRN,SNILS,Section,UsingTime)
	select ContractNumber,INN,IP,OGRN,SNILS,Section,UsingTime from ProcedureUsing;';
	$inserted_rows= execute_query_get_affected_rows($txt_query,array());
	echo job_time() . " .. inserted $inserted_rows rows.\r\n";

	echo job_time() . " delete from ProcedureUsing ..\r\n";
	$txt_query= 'delete from ProcedureUsing;';
	$deleted_rows= execute_query_get_affected_rows($txt_query,array());
	echo job_time() . " .. deleted $deleted_rows rows.\r\n";

	return "перенесено $inserted_rows из $deleted_rows";
}
