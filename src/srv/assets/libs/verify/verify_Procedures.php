<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/job.php';

require_once 'curl.php';

function SetProcedureVerifyState($id_MProcedure, $state)
{
	$query_text = "update MProcedure set VerificationState=?, TimeVerified=now() where id_MProcedure=?;";
	$affected= execute_query_get_affected_rows($query_text,array('ss',$state,$id_MProcedure));
	if (1!=$affected)
		throw new Exception("updated $affected rows to set VerificationState '$state' for id_MProcedure=$id_MProcedure");
}

function FixVerifiedDebtorManager($row, $efrsb_row)
{
	$state= /*state verified:*/'v';
	$query_text = "update MProcedure set 
		TimeVerified=now()
		, VerificationState=?
		, id_Debtor_Manager=?
	where id_MProcedure=?;";
	$affected= execute_query_get_affected_rows($query_text,array('sss',$state,
		$efrsb_row->id_Debtor_Manager,
		$row->id_MProcedure));
	if (1!=$affected)
		throw new Exception("updated $affected rows to set VerificationState '$state' for id_MProcedure=$row->id_MProcedure");
}

function findEfrsbDebtorManager($row, $efrsb_debtor_managers)
{
	$res= array();
	foreach ($efrsb_debtor_managers as $efrsb_debtor_manager)
	{
		if ($row->BankruptId==$efrsb_debtor_manager->BankruptId && $row->ArbitrManagerID==$efrsb_debtor_manager->ArbitrManagerID)
			$res[]= $efrsb_debtor_manager;
	}
	return $res;
}

function VerifyProcedure($row, $efrsb_debtor_managers, $irow)
{
	echo job_time()."     $irow) id_MProcedure:$row->id_MProcedure (BankruptId:$row->BankruptId, ArbitrManagerID:$row->ArbitrManagerID)\r\n";
	$found= findEfrsbDebtorManager($row, $efrsb_debtor_managers);
	$count_found= count($found);
	if (1==$count_found)
	{
		echo job_time()."        found 1 debtor_manager on efrsb: ".fix_readable_utf8(json_encode($found))."\r\n";
		FixVerifiedDebtorManager($row, $found[0]);
		echo job_time()."        verified!\r\n";
	}
	else if (0==$count_found)
	{
		if (false)
		{
			SetProcedureVerifyState($row->id_MProcedure, /*state rejected:*/'r'); 
			echo job_time()."        rejected!\r\n";
		}
		else
		{
			SetProcedureVerifyState($row->id_MProcedure, /*state skipped:*/'s'); 
			echo job_time()."        skipped!\r\n";
		}
	}
	else
	{
		echo job_time()."        found $count_found debtor_managers on efrsb: ".fix_readable_utf8(json_encode($found))."\r\n";
		SetProcedureVerifyState($row->id_MProcedure, /*state ambiguous:*/'a'); 
		echo job_time()."        ambiguous!\r\n";
	}
}

function findEfrsbDebtorManagers_url($rows)
{
	global $use_efrsb_service_url;

	$ArbitrManagerID= array();
	$BankruptId= array();
	foreach ($rows as $row)
	{
		if (isset($row->ArbitrManagerID) && null!=$row->ArbitrManagerID &&
			isset($row->BankruptId) && null!=$row->BankruptId)
		{
			$ArbitrManagerID[]= $row->ArbitrManagerID;
			$BankruptId[]= $row->BankruptId;
		}
	}
	$ArbitrManagerID= implode(',',$ArbitrManagerID);
	$BankruptId= implode(',',$BankruptId);

	$url= "$use_efrsb_service_url/dm-api.php/find-debtor-manager?ArbitrManagerID=$ArbitrManagerID&BankruptId=$BankruptId";

	return $url;
}

function findEfrsbDebtorManagers($rows)
{
	$url= findEfrsbDebtorManagers_url($rows);
	echo job_time()."     request $url\r\n";
	$curl_response= null;
	$httpcode= null;
	$curl = curl_init($url);
	try
	{
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, 1);
		$curl_response= curl_exec($curl);
		$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
	}
	catch (Exception $ex)
	{
		curl_close($curl);
		throw $ex;
	}

	if (200!=$httpcode)
		throw new Exception("got httpcode=$httpcode for url \"$url\"!");

	$curl_response= get_responce_body($curl_response);
	$efrsb_debtor_manager_rows= json_decode($curl_response);
	$count_efrsb_debtor_manager_rows= count($efrsb_debtor_manager_rows);
	echo job_time()."       got $count_efrsb_debtor_manager_rows efrsb Debtor_Managers\r\n";

	return $efrsb_debtor_manager_rows;
}

function VerifyProceduresPortion($max_portion_size)
{
	echo job_time()." query $max_portion_size records from MProcedure to verify\r\n";
	$query_text = "select 
				mp.id_MProcedure
				, d.BankruptId
				, m.ArbitrManagerID
				, mp.TimeCreated from MProcedure mp
			inner join Manager m on m.id_Manager=mp.id_Manager
			inner join Debtor d on d.id_Debtor=mp.id_Debtor
			where (mp.VerificationState is null
			or mp.VerificationState='s' && mp.TimeVerified < date_sub(now(), interval 1 hour))
			and d.BankruptId is not null and m.ArbitrManagerID is not null
			order by mp.TimeCreated LIMIT ?";
	$rows = execute_query($query_text,array('s',$max_portion_size));
	$count_rows= count($rows);
	echo job_time()."   got $count_rows records\r\n";
	if ($count_rows > 0)
	{
		$efrsb_debtor_managers= findEfrsbDebtorManagers($rows);
		$irow= 1;
		foreach ($rows as $row)
		{
			try
			{
				VerifyProcedure($row, $efrsb_debtor_managers, $irow);
			}
			catch (Exception $ex)
			{
				$exception_class= get_class($ex);
				$exception_Message= $ex->getMessage();
				echo "\r\nexception occurred: $exception_class - $exception_Message\r\n";
				echo 'catched Exception:';
				print_r($ex);
				echo "\r\non row\r\n";
				print_r($row);
				SetProcedureVerifyState($row->id_MProcedure, /*state error:*/'e');
			}
			$irow++;
		}
	}
	return $count_rows;
}


function verify_Procedures()
{
	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));
	$total_verified= 0;
	while (date_create() < $time_start_to_stop)
	{
		$verified= VerifyProceduresPortion(30);
		if (0==$verified)
			break;
		$total_verified+= $verified;
	}
	return "проверено процедур: $total_verified";
}

