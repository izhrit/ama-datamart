<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/job.php';

require_once 'curl.php';

function SetManagerVerifyState($id_Manager, $state)
{
	$query_text = "update Manager set VerificationState=?, TimeVerified=now() where id_Manager=?;";
	$affected= execute_query_get_affected_rows($query_text,array('ss',$state,$id_Manager));
	if (1!=$affected)
		throw new Exception("updated $affected rows to set VerificationState '$state' for id_Manager=$id_Manager");
}

function FixVerifiedManager($row, $efrsb_row)
{
	$state= /*state verified:*/'v';
	$query_text= 'update Manager set 
		TimeVerified=now()
		, VerificationState=?
		, ArbitrManagerID=?, firstName=?, lastName=?, middleName=?
		, id_SRO=(select id_SRO from efrsb_sro where efrsb_sro.RegNum=?)';
	$params= array('sssssss',$state,
		$efrsb_row->ArbitrManagerID, $efrsb_row->FirstName, $efrsb_row->LastName, $efrsb_row->MiddleName, $efrsb_row->SRORegNum);
	if (null!=$efrsb_row->INN)
	{
		$query_text.= ', INN=?';
		$params[]= $efrsb_row->INN;
		$params[0].= 's';
	}
	$query_text.= ' where id_Manager=?;';
	$params[]= $row->id_Manager;
	$affected= execute_query_get_affected_rows($query_text,$params);
	if (1!=$affected)
		throw new Exception("updated $affected rows to set VerificationState '$state' for id_Manager=$id_Manager");
}

function append_efrsb_managers(&$a1,$a2)
{
	foreach ($a2 as $row)
	{
		$a1[$row->ArbitrManagerID]= $row;
	}
}

function findEfrsbManager($row, $efrsb_managers)
{
	$by_ArbitrManagerID= array();
	if (isset($efrsb_managers->by_inn[$row->INN]))
		append_efrsb_managers($by_ArbitrManagerID,$efrsb_managers->by_inn[$row->INN]);
	if (isset($efrsb_managers->by_efrsb[$row->efrsbNumber]))
		append_efrsb_managers($by_ArbitrManagerID,$efrsb_managers->by_efrsb[$row->efrsbNumber]);
	$res= array();
	foreach ($by_ArbitrManagerID as $ArbitrManagerID => $efrsb_manager)
		$res[]= $efrsb_manager;
	return $res;
}

function VerifyManager($row, $efrsb_managers, $irow)
{
	echo job_time()."     $irow) id_Manager:$row->id_Manager, $row->lastName $row->firstName $row->middleName (inn:$row->INN,efrsb:$row->efrsbNumber)\r\n";
	$found= findEfrsbManager($row, $efrsb_managers);
	$count_found= count($found);
	if (1==$count_found)
	{
		echo job_time()."        found 1 manager on efrsb: ".fix_readable_utf8(json_encode($found))."\r\n";
		FixVerifiedManager($row, $found[0]);
		echo job_time()."        verified!\r\n";
	}
	else if (0==$count_found)
	{
		if (false)
		{
			SetManagerVerifyState($row->id_Manager, /*state rejected:*/'r'); 
			echo job_time()."        rejected!\r\n";
		}
		else
		{
			SetManagerVerifyState($row->id_Manager, /*state skipped:*/'s'); 
			echo job_time()."        skipped!\r\n";
		}
	}
	else
	{
		echo job_time()."        found $count_found managers on efrsb: ".fix_readable_utf8(json_encode($found))."\r\n";
		SetManagerVerifyState($row->id_Manager, /*state ambiguous:*/'a'); 
		echo job_time()."        ambiguous!\r\n";
	}
}

function findEfrsbManagers_url($rows)
{
	global $use_efrsb_service_url;

	$inn= array();
	$efrsb_num= array();
	foreach ($rows as $row)
	{
		if (isset($row->INN) && null!=$row->INN)
			$inn[]= trim($row->INN);
		if (isset($row->efrsbNumber) && null!=$row->efrsbNumber)
			$efrsb_num[]= trim($row->efrsbNumber);
	}
	$inn= implode(',',$inn);
	$efrsb_num= implode(',',$efrsb_num);

	$url= "$use_efrsb_service_url/dm-api.php/find-manager?inn=$inn&efrsb_num=$efrsb_num";

	return $url;
}

function index_efrsb_manager_row_by_field($row,&$by_field,$field_name)
{
	if (isset($row->$field_name) && null!=$row->$field_name)
	{
		$field_value= $row->$field_name;
		if (null!=$field_value)
		{
			if (!isset($by_field[$field_value]))
			{
				$by_field[$field_value]= array($row);
			}
			else
			{
				$by_field[$field_value][]= $row;
			}
		}
	}
}

function index_efrsb_manager_rows($efrsb_manager_rows)
{
	$by_inn= array();
	$by_efrsb= array();
	foreach ($efrsb_manager_rows as $row)
	{
		index_efrsb_manager_row_by_field($row,$by_inn,'INN');
		index_efrsb_manager_row_by_field($row,$by_efrsb,'RegNum');
	}
	$efrsb_managers= (object)array('by_inn'=>$by_inn,'by_efrsb'=>$by_efrsb,'rows'=>$efrsb_manager_rows);
	return $efrsb_managers;
}

function findEfrsbManagers($rows)
{
	$url= findEfrsbManagers_url($rows);
	echo job_time()."     request $url\r\n";
	$curl_response= null;
	$httpcode= null;
	$curl = curl_init($url);
	try
	{
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, 1);
		$curl_response= curl_exec($curl);
		$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
	}
	catch (Exception $ex)
	{
		curl_close($curl);
		throw $ex;
	}

	if (200!=$httpcode)
		throw new Exception("got httpcode=$httpcode for url \"$url\"!");

	$curl_response= get_responce_body($curl_response);
	$efrsb_manager_rows= json_decode($curl_response);
	$count_efrsb_managers= count($efrsb_manager_rows);
	echo job_time()."       got $count_efrsb_managers efrsb Managers\r\n";

	return index_efrsb_manager_rows($efrsb_manager_rows);
}

function VerifyManagersPortion($max_portion_size)
{
	echo job_time()." query $max_portion_size records from Manager to verify\r\n";
	$query_text = "select id_Manager, firstName, lastName, middleName, trim(INN) INN, trim(efrsbNumber) efrsbNumber, TimeCreated from Manager m 
			where VerificationState is null
			or VerificationState='s' && TimeVerified < date_sub(now(), interval 1 hour)
			order by TimeCreated LIMIT ?";
	$rows = execute_query($query_text,array('s',$max_portion_size));
	$count_rows= count($rows);
	echo job_time()."   got $count_rows records\r\n";
	if ($count_rows > 0)
	{
		$efrsb_managers= findEfrsbManagers($rows);
		$irow= 1;
		foreach ($rows as $row)
		{
			try
			{
				VerifyManager($row, $efrsb_managers, $irow);
			}
			catch (Exception $ex)
			{
				$exception_class= get_class($ex);
				$exception_Message= $ex->getMessage();
				echo "\r\nexception occurred: $exception_class - $exception_Message\r\n";
				echo 'catched Exception:';
				print_r($ex);
				echo "\r\non row\r\n";
				print_r($row);
				SetManagerVerifyState($row->id_Manager, /*state error:*/'e');
			}
			$irow++;
		}
	}
	return $count_rows;
}


function verify_Managers()
{
	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));
	$total_verified= 0;
	while (date_create() < $time_start_to_stop)
	{
		$verified= VerifyManagersPortion(30);
		if (0==$verified)
			break;
		$total_verified+= $verified;
	}
	return "проверено АУ: $total_verified";
}

