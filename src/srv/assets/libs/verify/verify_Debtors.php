<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/job.php';
require_once 'curl.php';

function append_efrsb_debtors(&$a1,$a2)
{
	foreach ($a2 as $row)
	{
		$a1[$row->BankruptId]= $row;
	}
}

function findEfrsbDebtor($row, $efrsb_debtors)
{
	$by_BankruptId= array();
	if (isset($efrsb_debtors->by_inn[$row->INN]))
		append_efrsb_debtors($by_BankruptId,$efrsb_debtors->by_inn[$row->INN]);
	if (isset($efrsb_debtors->by_ogrn[$row->OGRN]))
		append_efrsb_debtors($by_BankruptId,$efrsb_debtors->by_ogrn[$row->OGRN]);
	if (isset($efrsb_debtors->by_snils[$row->SNILS]))
		append_efrsb_debtors($by_BankruptId,$efrsb_debtors->by_snils[$row->SNILS]);
	$res= array();
	foreach ($by_BankruptId as $BankruptId => $efrsb_debtor)
		$res[]= $efrsb_debtor;
	return $res;
}

function FixVerifiedDebtor($row, $efrsb_row)
{
	$query_text = "call UpdateVerifiedDebtor(?,?,?,?,?,?);";
	execute_query_no_result($query_text,array('ssssss',
		$row->id_Debtor, $efrsb_row->INN, $efrsb_row->OGRN, $efrsb_row->SNILS, $efrsb_row->Name,$efrsb_row->BankruptId));
}

function VerifyDebtor($row, $efrsb_debtors, $irow)
{
	echo job_time()."     $irow) id_Debtor:$row->id_Debtor, \"$row->Name\" (inn:$row->INN,ogrn:$row->OGRN,snils:$row->SNILS)\r\n";
	$found= findEfrsbDebtor($row, $efrsb_debtors);
	$count_found= count($found);
	if (1==$count_found)
	{
		echo job_time()."        found 1 debtor on efrsb: ".fix_readable_utf8(json_encode($found))."\r\n";
		FixVerifiedDebtor($row, $found[0]);
		echo job_time()."        verified!\r\n";
	}
	else if (0==$count_found)
	{
		if (false)
		{
			SetDebtorVerifyState($row->id_Debtor, /*state rejected:*/'r'); 
			echo job_time()."        rejected!\r\n";
		}
		else
		{
			SetDebtorVerifyState($row->id_Debtor, /*state skipped:*/'s'); 
			echo job_time()."        skipped!\r\n";
		}
	}
	else
	{
		echo job_time()."        found $count_found debtors on efrsb: ".fix_readable_utf8(json_encode($found))."\r\n";
		SetDebtorVerifyState($row->id_Debtor, /*state ambiguous:*/'a'); 
		echo job_time()."        ambiguous!\r\n";
	}
}

function SetDebtorVerifyState($id_Debtor, $state)
{
	$query_text = "update Debtor set VerificationState=?, TimeVerified=now() where id_Debtor=?;";
	$affected= execute_query_get_affected_rows($query_text,array('ss',$state,$id_Debtor));
	if (1!=$affected)
		throw new Exception("updated $affected rows to set VerificationState '$state' for id_Debtor=$id_Debtor");
}

function findEfrsbDebtors_url($rows)
{
	global $use_efrsb_service_url;

	$inn= array();
	$ogrn= array();
	$snils= array();
	foreach ($rows as $row)
	{
		if (isset($row->INN) && null!=$row->INN)
			$inn[]= $row->INN;
		if (isset($row->OGRN) && null!=$row->OGRN)
			$ogrn[]= $row->OGRN;
		if (isset($row->SNILS) && null!=$row->SNILS)
			$snils[]= $row->SNILS;
	}
	$inn= implode(',',$inn);
	$ogrn= implode(',',$ogrn);
	$snils= implode(',',$snils);

	$url= "$use_efrsb_service_url/dm-api.php/find-debtor?inn=$inn&ogrn=$ogrn&snils=$snils";

	return $url;
}

function index_efrsb_debtor_row_by_field($row,&$by_field,$field_name)
{
	if (isset($row->$field_name) && null!=$row->$field_name)
	{
		$field_value= $row->$field_name;
		if (null!=$field_value)
		{
			if (!isset($by_field[$field_value]))
			{
				$by_field[$field_value]= array($row);
			}
			else
			{
				$by_field[$field_value][]= $row;
			}
		}
	}
}

function index_efrsb_debtor_rows($efrsb_debtor_rows)
{
	$by_inn= array();
	$by_ogrn= array();
	$by_snils= array();
	foreach ($efrsb_debtor_rows as $row)
	{
		index_efrsb_debtor_row_by_field($row,$by_inn,'INN');
		index_efrsb_debtor_row_by_field($row,$by_snils,'SNILS');
		index_efrsb_debtor_row_by_field($row,$by_ogrn,'OGRN');
	}
	$efrsb_debtors= (object)array('by_inn'=>$by_inn,'by_ogrn'=>$by_ogrn,'by_snils'=>$by_snils,'rows'=>$efrsb_debtor_rows);
	return $efrsb_debtors;
}

function findEfrsbDebtors($rows)
{
	$url= findEfrsbDebtors_url($rows);
	echo job_time()."     request $url\r\n";
	$curl_response= null;
	$httpcode= null;
	$curl = curl_init($url);
	try
	{
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, 1);
		$curl_response= curl_exec($curl);
		$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
	}
	catch (Exception $ex)
	{
		curl_close($curl);
		throw $ex;
	}

	if (200!=$httpcode)
		throw new Exception("got httpcode=$httpcode for url \"$url\"!");

	$curl_response= get_responce_body($curl_response);
	$efrsb_debtor_rows= json_decode($curl_response);
	$count_efrsb_debtors= count($efrsb_debtor_rows);
	echo job_time()."       got $count_efrsb_debtors efrsb Debtors\r\n";

	return index_efrsb_debtor_rows($efrsb_debtor_rows);
}

function VerifyDebtorPortion($max_portion_size)
{
	echo job_time()." query $max_portion_size records from Debtor to verify\r\n";
	$query_text = "select id_Debtor, Name, 
			trim(INN) INN, 
			trim(OGRN) OGRN, 
			replace(replace(SNILS,' ',''),'\t','') SNILS, 
			TimeCreated from Debtor d 
			where VerificationState is null
			or VerificationState='s' && TimeVerified < date_sub(now(), interval 1 hour)
			order by TimeCreated LIMIT ?";
	$rows = execute_query($query_text,array('s',$max_portion_size));
	$count_rows= count($rows);
	echo job_time()."   got $count_rows records\r\n";
	if ($count_rows > 0)
	{
		$efrsb_debtors= findEfrsbDebtors($rows);
		$irow= 1;
		foreach ($rows as $row)
		{
			try
			{
				VerifyDebtor($row, $efrsb_debtors, $irow);
			}
			catch (Exception $ex)
			{
				$exception_class= get_class($ex);
				$exception_Message= $ex->getMessage();
				echo "\r\nexception occurred: $exception_class - $exception_Message\r\n";
				echo 'catched Exception:';
				print_r($ex);
				echo "\r\non row\r\n";
				print_r($row);
				SetDebtorVerifyState($row->id_Debtor, /*state error:*/'e');
			}
			$irow++;
		}
	}
	return $count_rows;
}

function verify_Debtors()
{
	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));
	$total_verified= 0;
	while (date_create() < $time_start_to_stop)
	{
		$verified= VerifyDebtorPortion(30);
		if (0==$verified)
			break;
		$total_verified+= $verified;
	}
	return "проверено должников: $total_verified";
}

