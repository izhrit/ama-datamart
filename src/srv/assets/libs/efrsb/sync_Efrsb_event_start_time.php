<?php

function local_events_start_time($current_date_time= null, $max_event_days_age= 40)
{
	return date_sub($current_date_time, date_interval_create_from_date_string($max_event_days_age.' days'));;
}
