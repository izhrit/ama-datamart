<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/job.php';

require_once '../assets/libs/efrsb/sync_Efrsb_event_start_time.php';

function request_efrsb_events($max_revision, $time_later_than= null, $portion_size= null)
{
	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/replication-api.php/event?revision-greater-than='.$max_revision;

	if (null!=$time_later_than)
		$url.= '&later-than-time='.date_format($time_later_than,'Y-m-d\TH:i:s');
	if (null!=$portion_size)
		$url.= '&portion-size='.$portion_size;

	echo job_time() . " request..\r\n$url\r\n";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL,$url);
	$curl_response=curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request events (code $httpcode for url \"$url\")");

	$events= json_decode($curl_response);

	if (null===$events)
	{
		echo "got responce:\r\n".$curl_response."\r\n";
		throw new Exception("can not parse json responce!");
	}

	$count_events = count($events);
	echo job_time() . " .. got $count_events events.\r\n";

	return $events;
}

function insert_event_rows_get_max_Revision($events,&$count_inserted,&$count_updated)
{
	$count_inserted_rows= 0;
	$count_updated_rows= 0;
	foreach ($events as $e)
	{
		$affected_rows= execute_query_get_affected_rows(
			'insert into event 
			set efrsb_id=?, ArbitrManagerID=?, BankruptId=?, EventTime=?, MessageType=?, MessageNumber=?, MessageGuid=?, Revision=? 
			on duplicate key update 
			Revision=?, isActive=?'
			,array('sssssssssi'
			,$e->efrsb_id, $e->ArbitrManagerID, $e->BankruptId, $e->EventTime, $e->MessageInfo_MessageType, $e->Number, $e->MessageGUID, $e->Revision
			,$e->Revision, $e->isActive));
		switch ($affected_rows)
		{
			case 1: case '1': $count_inserted_rows++; break;
			case 2: case '2': $count_updated_rows++; break;
			default:
				//print_r($affected_rows);
				echo job_time() . " bad sync event for efrsb_id={$e->efrsb_id}, Revision={$e->Revision} (affected_rows=$affected_rows)!!!!!!\r\n";
				break;
		}
	}
	$msg= "inserted $count_inserted_rows events";
	if (0!=$count_updated_rows)
		$msg.= ", updated $count_updated_rows events";
	echo job_time() . " $msg\r\n";
	$count_inserted+= $count_inserted_rows;
	$count_updated+= $count_updated_rows;
}

function get_max_Revision($events)
{
	$max_Revision= -1;
	foreach ($events as $e)
	{
		if ($max_Revision<$e->Revision)
			$max_Revision= $e->Revision;
	}
	return $max_Revision;
}

function new_min_max_e_PublishDate()
{
	return (object)array('min'=>null,'max'=>null);
}

function update_min_max_e_PublishDate($min_max_PublishDate,$messages)
{
	foreach ($messages as $m)
	{
		$txt_PublishDate= $m->PublishDate;
		$d= date_create_from_format('Y-m-d H:i:s',$txt_PublishDate);

		$min= $min_max_PublishDate->min;
		if (null==$min || $d<$min)
			$min_max_PublishDate->min= $d;

		$max= $min_max_PublishDate->max;
		if (null==$max || $d>$max)
			$min_max_PublishDate->max= $d;
	}
}

function sync_events($portion_size= 200, $max_to_insert= null, $max_event_days_age= 40)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);
	$time_later_than= date_sub($current_date_time, date_interval_create_from_date_string($max_event_days_age.' days'));

	echo job_time() . " query for max_revision from event ..\r\n";
	$rows= execute_query('SELECT MAX(e.Revision) AS max_revision from event e;',array());
	$max_revision = empty($rows[0]->max_revision) ? 0 : $rows[0]->max_revision;
	echo job_time() . ".. got max_revision=$max_revision.\r\n";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));

	$read= 0;
	$inserted= 0;
	$updated= 0;
	$min_max= new_min_max_e_PublishDate();
	for ($events= request_efrsb_events($max_revision, $time_later_than, $portion_size), $events_count= count($events);
		 0!=$events_count;
		 $events= request_efrsb_events($max_revision, $time_later_than, $portion_size), $events_count= count($events))
	{
		insert_event_rows_get_max_Revision($events,$inserted,$updated);
		update_min_max_e_PublishDate($min_max,$events);
		$max_revision= get_max_Revision($events);
		$read+= $events_count;
		if (!(date_create() < $time_start_to_stop && (null==$max_to_insert || ($inserted+$updated)<$max_to_insert)))
			break;
	}
	$count_events= count($events);
	if (0==$count_events)
		echo job_time() . " no events to insert!\r\n";

	$res= "Загружено новых событий с ЕФРСБ: $read";
	if (0!=$read)
	{
		$d1= null==$min_max->min ? '?' : date_format($min_max->min,'Y-m-d H:i:s');
		$d2= null==$min_max->max ? '?' : date_format($min_max->max,'Y-m-d H:i:s');
		$res.= "
  с $d1 
 по $d2, 
 добавлено: $inserted";
		if (0!=$updated)
		{
			$res.= ",
 обновлено: $updated";
		}
	}
	return $res;
}

function delete_old_events($max_event_days_age= 40)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);
	$time_later_than= local_events_start_time($current_date_time, $max_event_days_age);
	$sql_time_later_than= date_format($time_later_than,'Y-m-d H:i:s');

	echo job_time() . " delete from event where EventTime < $sql_time_later_than (older than $max_event_days_age days) ..\r\n";
	$affected_rows= execute_query_get_affected_rows('delete e 
		from event e
		left join Pushed_event pe on pe.id_Event=e.id_Event
		where e.EventTime < ? and pe.Time_dispatched is null;',array('s',$sql_time_later_than));
	echo job_time() . " .. deleted $affected_rows event rows.\r\n";

	return "удалено событий из ЕФРСБ ранее $sql_time_later_than: $affected_rows";
}
