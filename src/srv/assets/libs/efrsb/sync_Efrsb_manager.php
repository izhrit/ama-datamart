<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/job.php';

function request_efrsb_managers($max_revision, $portion_size= null)
{
	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/replication-api.php/manager?revision-greater-than='.$max_revision;

	if (null!=$portion_size)
		$url.= '&portion-size='.$portion_size;

	echo job_time() . " request..\r\n$url\r\n";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL,$url);
	$curl_response=curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request managers (code $httpcode for url \"$url\")");

	$managers= json_decode($curl_response);

	if (null===$managers)
	{
		echo "got responce:\r\n".$curl_response."\r\n";
		throw new Exception("can not parse json responce!");
	}

	$count_managers = count($managers);
	echo job_time() . " .. got $count_managers managers.\r\n";

	return $managers;
}

function insert_managers($managers)
{
	$res= -1;
	$count_inserted= 0;
	foreach ($managers as $e)
	{
		if ($res<$e->Revision)
			$res= $e->Revision;
		try
		{
			execute_query_no_result(
				'call SafeUpdateEfrsbManager(?,?,?,?,?,?,?,?,?,?,?,?);'
				,array('ssssssssssss',
					$e->id_Manager, $e->ArbitrManagerID, $e->SRORegNum
					, $e->FirstName, $e->MiddleName, $e->LastName
					, $e->OGRNIP, $e->INN
					, $e->SRORegDate, $e->RegNum
					, $e->CorrespondenceAddress
					, $e->Revision));
			$count_inserted++;
		}
		catch (Exception $ex)
		{
			echo job_time() . 'Unhandled exception occurred: ' . get_class($ex) . ' - ' . $ex->getMessage();
		}
	}
	echo job_time() . " inserted $count_inserted managers\r\n";
	return $res;
}

function sync_managers($portion_size= 200, $max_to_insert= null)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);

	echo job_time() . " query for max_revision from efrsb_manager ..\r\n";
	$rows= execute_query('SELECT MAX(d.Revision) AS max_revision from efrsb_manager d;',array());
	$max_revision = empty($rows[0]->max_revision) ? 0 : $rows[0]->max_revision;
	echo job_time() . ".. got max_revision=$max_revision.\r\n";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));

	$inserted= 0;
	for ($managers= request_efrsb_managers($max_revision,$portion_size), $managers_count= count($managers);
		 0!=$managers_count;
		 $managers= request_efrsb_managers($max_revision,$portion_size), $managers_count= count($managers))
	{
		$max_revision= insert_managers($managers); 
		$inserted+= $managers_count;
		if (!(date_create() < $time_start_to_stop && (null==$max_to_insert || $inserted<$max_to_insert)))
			break;
	}
	
	if (0==count($managers))
		echo job_time() . " no managers to insert!\r\n";

	return "загружено $inserted изменёний АУ";
}