<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/job.php';

function request_efrsb_sros($max_revision, $portion_size= null)
{
	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/replication-api.php/sro?revision-greater-than='.$max_revision;

	if (null!=$portion_size)
		$url.= '&portion-size='.$portion_size;

	echo job_time() . " request..\r\n$url\r\n";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL,$url);
	$curl_response=curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request sros (code $httpcode for url \"$url\")");

	$sros= json_decode($curl_response);

	if (null===$sros)
	{
		echo "got responce:\r\n".$curl_response."\r\n";
		throw new Exception("can not parse json responce!");
	}

	$count_sros = count($sros);
	echo job_time() . " .. got $count_sros sros.\r\n";

	return $sros;
}

function insert_sros($sros)
{
	$res= -1;
	foreach ($sros as $e)
	{
		if ($res<$e->Revision)
			$res= $e->Revision;
		execute_query_no_result('insert into efrsb_sro set 
				id_SRO=?, OGRN=?, RegNum=?, INN=?, Name=?, ShortTitle=?, Title=?, UrAdress=?, Revision=?
			ON DUPLICATE KEY UPDATE
				OGRN=?, INN=?, Name=?, ShortTitle=?, Title=?, UrAdress=?, Revision=?
			'
			,array('ssssssssssssssss',
				$e->id_SRO, $e->OGRN, $e->RegNum, $e->INN, $e->Name, $e->ShortTitle, $e->Title, $e->UrAdress, $e->Revision,
				$e->OGRN, $e->INN, $e->Name, $e->ShortTitle, $e->Title, $e->UrAdress, $e->Revision));
	}
	$count_inserted = count($sros);
	echo job_time() . " inserted $count_inserted sros\r\n";
	return $res;
}

function sync_sros($portion_size= 200, $max_to_insert= null)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);

	echo job_time() . " query for max_revision from efrsb_sro ..\r\n";
	$rows= execute_query('SELECT MAX(d.Revision) AS max_revision from efrsb_sro d;',array());
	$max_revision = empty($rows[0]->max_revision) ? 0 : $rows[0]->max_revision;
	echo job_time() . ".. got max_revision=$max_revision.\r\n";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));

	$inserted= 0;
	for ($sros= request_efrsb_sros($max_revision,$portion_size), $sros_count= count($sros);
		 0!=$sros_count;
		 $sros= request_efrsb_sros($max_revision,$portion_size), $sros_count= count($sros))
	{
		$max_revision= insert_sros($sros); 
		$inserted+= $sros_count;
		if (!(date_create() < $time_start_to_stop && (null==$max_to_insert || $inserted<$max_to_insert)))
			break;
	}
	
	if (0==count($sros))
		echo job_time() . " no sros to insert!\r\n";

	return "загружено $inserted изменёний СРО";
}