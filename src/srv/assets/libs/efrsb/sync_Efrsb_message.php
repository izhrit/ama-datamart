<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/job.php';

function request_efrsb_messages($max_revision, $time_later_than= null, $portion_size= null)
{
	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/replication-api.php/message?revision-greater-than='.$max_revision;

	if (null!=$time_later_than)
		$url.= '&later-than-time='.date_format($time_later_than,'Y-m-d\TH:i:s');
	if (null!=$portion_size)
		$url.= '&portion-size='.$portion_size;

	echo job_time() . " request..\r\n$url\r\n";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL,$url);
	$curl_response=curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request messages (code $httpcode for url \"$url\")");

	$messages= json_decode($curl_response);

	if (null===$messages)
	{
		echo "got responce:\r\n".$curl_response."\r\n";
		throw new Exception("can not parse json responce!");
	}

	$count_messages = count($messages);
	echo job_time() . " .. got $count_messages messages.\r\n";

	return $messages;
}

function insert_messages($messages)
{
	$count_inserted= 0;
	$res= -1;
	foreach ($messages as $e)
	{
		if ($res<$e->Revision)
			$res= $e->Revision;
		try
		{
			execute_query_no_result('insert into Message set 
					efrsb_id=?
					, ArbitrManagerID=?
					, BankruptId=?
					, INN=?
					, SNILS=?
					, OGRN=?
					, PublishDate=?
					, MessageInfo_MessageType=?
					, Number=?
					, MessageGUID=?
					, Revision=?
					, Body=?'
				,array('sssssssssssb',
					$e->efrsb_id
					, $e->ArbitrManagerID
					, $e->BankruptId
					, $e->INN
					, $e->SNILS
					, $e->OGRN
					, $e->PublishDate
					, $e->MessageInfo_MessageType
					, $e->Number
					, $e->MessageGUID
					, $e->Revision
					, base64_decode($e->Body)));
				$count_inserted++;
		}
		catch (Exception $exception)
		{
			echo job_time() . 'Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage();
		}
	}
	echo job_time() . " inserted $count_inserted messages\r\n";
	return $res;
}

function new_min_max_PublishDate()
{
	return (object)array('min'=>null,'max'=>null);
}

function update_min_max_PublishDate($min_max_PublishDate,$messages)
{
	foreach ($messages as $m)
	{
		$txt_PublishDate= $m->PublishDate;
		$d= date_create_from_format('Y-m-d H:i:s',$txt_PublishDate);

		$min= $min_max_PublishDate->min;
		if (null==$min || $d<$min)
			$min_max_PublishDate->min= $d;

		$max= $min_max_PublishDate->max;
		if (null==$max || $d>$max)
			$min_max_PublishDate->max= $d;
	}
}

function sync_messages($portion_size= 200, $max_to_insert= null, $max_message_days_age= 60)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);
	$time_later_than= date_sub($current_date_time, date_interval_create_from_date_string($max_message_days_age.' days'));

	echo job_time() . " query for max_revision from Message ..\r\n";
	$rows= execute_query('SELECT MAX(m.Revision) AS max_revision from Message m;',array());
	$max_revision = empty($rows[0]->max_revision) ? 0 : $rows[0]->max_revision;
	echo job_time() . ".. got max_revision=$max_revision.\r\n";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));

	$read= 0;
	$inserted= 0;
	$min_max= new_min_max_PublishDate();
	for ($messages= request_efrsb_messages($max_revision, $time_later_than, $portion_size), $messages_count= count($messages);
		 0!=$messages_count;
		 $messages= request_efrsb_messages($max_revision, $time_later_than, $portion_size), $messages_count= count($messages))
	{
		update_min_max_PublishDate($min_max,$messages);
		$read+= $messages_count;
		$max_revision= insert_messages($messages); 
		$inserted+= $messages_count;
		if (!(date_create() < $time_start_to_stop && (null==$max_to_insert || $inserted<$max_to_insert)))
			break;
	}
	
	$count_messages= count($messages);
	if (0==$count_messages)
		echo job_time() . " no messages to insert!\r\n";

	$res= "Загружено новых сообщений ЕФРСБ: $read";
	if ($read>0)
	{
		$d1= null==$min_max->min ? '?' : date_format($min_max->min,'Y-m-d H:i:s');
		$d2= null==$min_max->max ? '?' : date_format($min_max->max,'Y-m-d H:i:s');
		$res.= "
  с $d1 
 по $d2, 
 сохранено : $inserted";
	}
	return $res;
}

function delete_old_messages($max_message_days_age= 60)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);
	$time_later_than= date_sub($current_date_time, date_interval_create_from_date_string($max_message_days_age.' days'));
	$sql_time_later_than= date_format($time_later_than,'Y-m-d H:i:s');

	echo job_time() . " delete from Message with no actual event \r\n";
	echo job_time() . "   where PublishDate < $sql_time_later_than (older than $max_message_days_age days) ..\r\n";
	$txt_query= 'delete Message 
		from Message 
		left join event on event.efrsb_id=Message.efrsb_id
		where event.efrsb_id is null && PublishDate < ?;';
	$affected_rows= execute_query_get_affected_rows($txt_query,array('s',$sql_time_later_than));
	echo job_time() . " .. deleted $affected_rows Message rows.\r\n";

	return "удалено сообщений ранее $sql_time_later_than: $affected_rows";
}