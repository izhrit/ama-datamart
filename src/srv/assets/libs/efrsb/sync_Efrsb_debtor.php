<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/job.php';

function request_efrsb_debtors($max_revision, $portion_size= null)
{
	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/replication-api.php/debtor?revision-greater-than='.$max_revision;

	if (null!=$portion_size)
		$url.= '&portion-size='.$portion_size;

	echo job_time() . " request..\r\n$url\r\n";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL,$url);
	$curl_response=curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request debtors (code $httpcode for url \"$url\")");

	$debtors= json_decode($curl_response);

	if (null===$debtors)
	{
		echo "got responce:\r\n".$curl_response."\r\n";
		throw new Exception("can not parse json responce!");
	}

	$count_debtors = count($debtors);
	echo job_time() . " .. got $count_debtors debtors.\r\n";

	return $debtors;
}

function insert_debtors($debtors)
{
	$res= -1;
	foreach ($debtors as $d)
	{
		if ($res<$d->Revision)
			$res= $d->Revision;
		execute_query_no_result(
			'insert into efrsb_debtor set 
				id_Debtor=?,   BankruptId=?
				,ArbitrManagerID=?,   INN=?,   SNILS=?,   Name=?,   OGRN=?,   Revision=?, Archive=?, LastMessageDate=?
			on duplicate key update
				 ArbitrManagerID=?,   INN=?,   SNILS=?,   Name=?,   OGRN=?,   Revision=?, Archive=?, LastMessageDate=?'
			,array('ssssssssisssssssis'
				,$d->id_Debtor, $d->BankruptId
				,$d->ArbitrManagerID, $d->INN, $d->SNILS, $d->Name, $d->OGRN, $d->Revision, $d->Archive, $d->LastMessageDate
				,$d->ArbitrManagerID, $d->INN, $d->SNILS, $d->Name, $d->OGRN, $d->Revision, $d->Archive, $d->LastMessageDate));
	}
	$count_inserted = count($debtors);
	echo job_time() . " inserted $count_inserted debtors\r\n";
	return $res;
}

function sync_debtors($portion_size= 200, $max_to_insert= null)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);

	echo job_time() . " query for max_revision from efrsb_debtor ..\r\n";
	$rows= execute_query('SELECT MAX(d.Revision) AS max_revision from efrsb_debtor d;',array());
	$max_revision = empty($rows[0]->max_revision) ? 0 : $rows[0]->max_revision;
	echo job_time() . ".. got max_revision=$max_revision.\r\n";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));

	$inserted= 0;
	for ($debtors= request_efrsb_debtors($max_revision,$portion_size), $debtors_count= count($debtors);
		 0!=$debtors_count;
		 $debtors= request_efrsb_debtors($max_revision,$portion_size), $debtors_count= count($debtors))
	{
		$max_revision= insert_debtors($debtors); 
		$inserted+= $debtors_count;
		if (!(date_create() < $time_start_to_stop && (null==$max_to_insert || $inserted<$max_to_insert)))
			break;
	}
	
	if (0==count($debtors))
		echo job_time() . " no debtors to insert!\r\n";

	return "загружено $inserted изменёний должников";
}