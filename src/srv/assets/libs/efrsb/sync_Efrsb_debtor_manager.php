<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/job.php';

function request_efrsb_debtor_managers($max_revision, $portion_size= null)
{
	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/replication-api.php/debtor_manager?revision-greater-than='.$max_revision;

	if (null!=$portion_size)
		$url.= '&portion-size='.$portion_size;

	echo job_time() . " request..\r\n$url\r\n";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL,$url);
	$curl_response=curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request debtor_managers (code $httpcode for url \"$url\")");

	$debtor_managers= json_decode($curl_response);

	if (null===$debtor_managers)
	{
		echo "got responce:\r\n".$curl_response."\r\n";
		throw new Exception("can not parse json responce!");
	}

	$count_debtor_managers = count($debtor_managers);
	echo job_time() . " .. got $count_debtor_managers debtor_managers.\r\n";

	return $debtor_managers;
}

function insert_debtor_managers($debtor_managers)
{
	$res= -1;
	foreach ($debtor_managers as $e)
	{
	
		if ($res<$e->Revision)
			$res= $e->Revision;
		execute_query_no_result(
			'insert into efrsb_debtor_manager set 
				id_Debtor_Manager=?
				,BankruptId=?, ArbitrManagerID=?, Revision=?
			on duplicate key update
				 BankruptId=?, ArbitrManagerID=?, Revision=?;'
			,array('sssssss',
				$e->id_Debtor_manager
				,$e->BankruptId, $e->ArbitrManagerID, $e->Revision
				,$e->BankruptId, $e->ArbitrManagerID, $e->Revision));
	}
	$count_inserted = count($debtor_managers);
	echo job_time() . " inserted $count_inserted debtor_managers\r\n";
	return $res;
}

function sync_debtor_managers($portion_size= 200, $max_to_insert= null)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);

	echo job_time() . " query for max_revision from efrsb_debtor_manager ..\r\n";
	$rows= execute_query('SELECT MAX(d.Revision) AS max_revision from efrsb_debtor_manager d;',array());
	$max_revision = empty($rows[0]->max_revision) ? 0 : $rows[0]->max_revision;
	echo job_time() . ".. got max_revision=$max_revision.\r\n";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));

	$inserted= 0;
	for ($debtor_managers= request_efrsb_debtor_managers($max_revision,$portion_size), $debtor_managers_count= count($debtor_managers);
		 0!=$debtor_managers_count;
		 $debtor_managers= request_efrsb_debtor_managers($max_revision,$portion_size), $debtor_managers_count= count($debtor_managers))
	{
		$max_revision= insert_debtor_managers($debtor_managers); 
		$inserted+= $debtor_managers_count;
		if (!(date_create() < $time_start_to_stop && (null==$max_to_insert || $inserted<$max_to_insert)))
			break;
	}
	
	if (0==count($debtor_managers))
		echo job_time() . " no debtor_managers to insert!\r\n";

	return "загружено $inserted изменёний процедур";
}