<?

function prep_mrequest_tpl_par_СРО_Руководитель($row)
{
	$tpl_par_СРО_руководитель= (object)array('Должность'=>'Единоличный руководитель');
	if (null!=$row->sro_CEOName)
	{
		$parts= explode(' ',$row->sro_CEOName);
		$count_parts= count($parts);
		$tpl_par_СРО_руководитель->Фамилия= $parts[0];
		if ($count_parts>1)
		{
			$tpl_par_СРО_руководитель->Имя= $parts[1];
			if ($count_parts>2)
				$tpl_par_СРО_руководитель->Отчество= $parts[2];
		}
	}
	return $tpl_par_СРО_руководитель;
}

function prep_mrequest_tpl_par_СРО($row)
{
	$tpl_par_СРО= (object)array(
		'Наименование'=>$row->sro_ShortTitle
		,'ПолноеНаименование'=>$row->sro_Title
		,'email'=>$row->sro_Email
		,'Руководитель'=> prep_mrequest_tpl_par_СРО_Руководитель($row)
		,'RegNum'=>$row->sro_RegNum
		,'id_SRO'=>$row->id_SRO
	);
	return $tpl_par_СРО;
}

function prep_mrequest_tpl_par_Должник($row)
{
	$tpl_par_Должник= (object)array(
		'Наименование'=> $row->DebtorName
		,'Адрес'=> $row->DebtorAddress
		,'ИНН'=> $row->DebtorINN
	);
	return $tpl_par_Должник;
}

function prep_mrequest_tpl_par_Заявитель($row, $tpl_par_Должник)
{
	$tpl_par_Заявитель= null==$row->ApplicantName 
		? clone $tpl_par_Должник 
		: (object)array(
			'Наименование'=> $row->ApplicantName
			,'Адрес'=> $row->ApplicantAddress
		);
	return $tpl_par_Заявитель;
}

function prep_mrequest_tpl_par_АУ($row)
{
	$tpl_par_АУ= (object)array(
		'Фамилия'=> $row->ManagerLastName
		,'Имя'=> $row->ManagerFirstName
		,'Отчество'=> $row->ManagerMiddleName
		,'ИНН'=> $row->ManagerINN
		,'НомерВЕФРСБ'=> $row->efrsbNumber
		,'Адрес'=> $row->CorrespondenceAddress
		,'id_Manager'=> $row->id_Manager
	);
	return $tpl_par_АУ;
}

$mrequest_tpl_params_txt_query= 'select
  mr.CaseNumber
, mr.ApplicantName
, mr.ApplicantAddress
, mr.DebtorName
, mr.DebtorINN
, mr.DebtorAddress
, mr.DateOfRequestAct
, mr.DateOfResponce
, mr.NextSessionDate

, c.Name CourtName
, c.Address CourtAddress

, m.lastName ManagerLastName
, m.firstName ManagerFirstName
, m.middleName ManagerMiddleName
, m.INN ManagerINN
, m.efrsbNumber
, m.ProtocolNum
, m.ProtocolDate
, m.id_Manager
, em.CorrespondenceAddress

, es.ShortTitle sro_ShortTitle
, es.Title sro_Title
, es.SROEmail sro_Email
, es.UrAdress sro_UrAdress
, es.CEOName sro_CEOName
, es.RegNum sro_RegNum
, es.id_SRO

from MRequest mr
inner join Court c on mr.id_Court=c.id_Court
inner join efrsb_sro es on mr.id_SRO=es.id_SRO
left join ProcedureStart ps on ps.id_MRequest=mr.id_MRequest && ps.id_SRO=mr.id_SRO
left join Manager m on ps.id_Manager=m.id_Manager
left join efrsb_manager em on em.RegNum=m.efrsbNumber
where mr.id_MRequest=?';

function prepare_mrequest_tpl_params($id_MRequest)
{
	write_to_log("prepare_mrequest_tpl_params(id_MRequest=$id_MRequest) { ");
	global $auth_info, $mrequest_tpl_params_txt_query;
	$txt_query= $mrequest_tpl_params_txt_query;
	$params= array('s',$id_MRequest);
	if (isset($auth_info->id_SRO))
	{
		$params[0].= 's';
		$params[]= $auth_info->id_SRO;
		$txt_query.= ' and mr.id_SRO=?';
	}
	if (isset($auth_info->id_Contract))
	{
		$params[0].= 's';
		$params[]= $auth_info->id_Contract;
		$txt_query.= ' and m.id_Contract=?';
	}
	if (isset($auth_info->id_Manager))
	{
		$params[0].= 's';
		$params[]= $auth_info->id_Manager;
		$txt_query.= ' and m.id_Manager=?';
	}
	$rows= execute_query($txt_query,$params);
	$count_rows= count($rows);

	if (1!=$count_rows)
		exit_not_found("found $count_rows rows MRequest where id_MRequest=$id_MRequest for current auth!");

	$row= $rows[0];

	$tpl_par_Должник= prep_mrequest_tpl_par_Должник($row);

	$current_time= safe_date_create();
	$DateOfResponce= null!=$row->DateOfResponce
		? $row->DateOfResponce 
		: date_format($current_time,'Y-m-d\TH:i:s');

	$tpl_par= (object)array(
		 'Заявитель'=> prep_mrequest_tpl_par_Заявитель($row, $tpl_par_Должник)
		,'Должник'=> $tpl_par_Должник
		,'Дата_заявления'=>$DateOfResponce
		,'Суд'=> (object)array(
			'Наименование'=> $row->CourtName
			,'Адрес'=> $row->CourtAddress
			,'Дело'=>(object)array(
				'Номер'=>$row->CaseNumber
				,'Время_следующего_заседания'=>$row->NextSessionDate
			)
		)
		,'СРО'=>prep_mrequest_tpl_par_СРО($row)
		,'Дата_запроса'=>$row->DateOfRequestAct
		,'ЗаседаниеКК'=>(object)array(
			'Время'=> $DateOfResponce
			,'Номер_протокола'=>"ПКК/$id_MRequest"
		)
		,'АУ'=> prep_mrequest_tpl_par_АУ($row)
		,'Исходящее'=> (object)array(
			'Номер'=>"ОЗК/$id_MRequest"
			,'Дата'=> $DateOfResponce
		)
	);
	write_to_log($tpl_par);
	write_to_log("prepare_mrequest_tpl_params }");
	return $tpl_par;
}

$procedure_start_tpl_params_txt_query= 'select
  ps.CaseNumber
, ps.ApplicantName
, ps.ApplicantAddress
, ps.DebtorName
, ps.DebtorINN
, ps.DebtorAddress
, ps.NextSessionDate

, c.Name CourtName
, c.Address CourtAddress

, m.lastName ManagerLastName
, m.firstName ManagerFirstName
, m.middleName ManagerMiddleName
, m.INN ManagerINN
, m.efrsbNumber
, m.ProtocolNum
, m.ProtocolDate
, m.id_Manager
, em.CorrespondenceAddress

, es.ShortTitle sro_ShortTitle
, es.Title sro_Title
, es.SROEmail sro_Email
, es.UrAdress sro_UrAdress
, es.CEOName sro_CEOName
, es.RegNum sro_RegNum
, es.id_SRO

, mr.DateOfResponce

from ProcedureStart ps
inner join Court c on ps.id_Court=c.id_Court
left join Manager m on ps.id_Manager=m.id_Manager
left join efrsb_sro es on ps.id_SRO=es.id_SRO
left join efrsb_manager em on em.RegNum=m.efrsbNumber
left join MRequest mr on ps.id_MRequest=mr.id_MRequest
where ps.id_ProcedureStart=?';

function prepare_procedure_start_tpl_params($id_ProcedureStart)
{
	global $auth_info, $procedure_start_tpl_params_txt_query;
	$params= array('s',$id_ProcedureStart);
	$txt_query= $procedure_start_tpl_params_txt_query;
	if (isset($auth_info->id_SRO))
	{
		$params[0].= 's';
		$params[]= $auth_info->id_SRO;
		$txt_query.= ' and m.id_SRO=?';
	}
	if (isset($auth_info->id_Contract))
	{
		$params[0].= 's';
		$params[]= $auth_info->id_Contract;
		$txt_query.= ' and m.id_Contract=?';
	}
	if (isset($auth_info->id_Manager))
	{
		$params[0].= 's';
		$params[]= $auth_info->id_Manager;
		$txt_query.= ' and m.id_Manager=?';
	}
	write_to_log($txt_query);
	write_to_log($params);
	$rows= execute_query($txt_query,$params);

	$count_rows= count($rows);

	if (1!=$count_rows)
		exit_not_found("found $count_rows rows ProcedureStart where id_ProcedureStart=$id_ProcedureStart for current auth!");

	$row= $rows[0];

	$tpl_par_Должник= prep_mrequest_tpl_par_Должник($row);

	$current_time= safe_date_create();
	$DateOfResponce= null!=$row->DateOfResponce
		? $row->DateOfResponce 
		: date_format($current_time,'Y-m-d\TH:i:s');

	$tpl_par= (object)array(
		 'Заявитель'=> prep_mrequest_tpl_par_Заявитель($row, $tpl_par_Должник)
		,'Должник'=> $tpl_par_Должник
		,'Дата_заявления'=>$DateOfResponce
		,'Суд'=> (object)array(
			'Наименование'=> $row->CourtName
			,'Адрес'=> $row->CourtAddress
			,'Дело'=>(object)array(
				'Номер'=>$row->CaseNumber
				,'Время_следующего_заседания'=>$row->NextSessionDate
			)
		)
		,'Дата_выписки'=>$DateOfResponce
		,'СРО'=>prep_mrequest_tpl_par_СРО($row)
		,'АУ'=> prep_mrequest_tpl_par_АУ($row)
	);
	return $tpl_par;
}

function приготовить_факсимиле_подписи_АУ_для_документа($id_Manager)
{
	$txt_query= "select id_ManagerDocument, FileName, PubLock, Body
	from ManagerDocument where id_Manager=? and DocumentType = 'f';";
	$rows= execute_query($txt_query,array('s',$id_Manager));
	$count_rows= count($rows);

	if (1==$count_rows)
	{
		$row= $rows[0];
		$pass= publicate_locked_ManagerDocument($rows[0]);
		$url= "ui-backend.php?action=manager.document.dwnld&id_ManagerDocument={$row->id_ManagerDocument}&pass=$pass";
		set_error_handler(function() { /* ignore errors */ });
		$im= imagecreatefromstring($row->Body);
		restore_error_handler();
		if (false!=$im)
		{
			$width= imagesx($im);
			$height= imagesy($im);
			return (object)array('url'=> $url, 'width'=> $width, 'height'=> $height);
		}
	}
	return null;
}

function приготовить_факсимиле_СРО_для_документа($id_SRO, $doc_type)
{
	$txt_query= "select id_SRODocument, FileName, PubLock, Body
	from SRODocument where id_SRO=? and DocumentType = ?;";
	$rows= execute_query($txt_query,array('ss',$id_SRO,$doc_type));
	$count_rows= count($rows);

	if (1==$count_rows)
	{
		$row= $rows[0];
		$pass= publicate_locked_SRODocument($rows[0]);
		$url= "ui-backend.php?action=sro.document.dwnld&id_SRODocument={$row->id_SRODocument}&pass=$pass";
		set_error_handler(function() { /* ignore errors */ });
		$im= imagecreatefromstring($row->Body);
		restore_error_handler();
		if (false!=$im)
		{
			$width= imagesx($im);
			$height= imagesy($im);
			return (object)array('url'=> $url, 'width'=> $width, 'height'=> $height);
		}
	}
	return null;
}

