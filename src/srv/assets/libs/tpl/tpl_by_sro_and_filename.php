<?

$tpl_root_path= __DIR__.'../../../tpl';
$default_sro_folder_name= 'default';
$sro_props_file_name= 'props.json';

function get_tpl_path_for_sro_tpl_folder($sro_tpl_folder, $tpl_name)
{
	global $default_sro_folder_name, $tpl_root_path;
	$sro_tpl_file_path= "$tpl_root_path/$sro_tpl_folder/$tpl_name.html";
	if (file_exists($sro_tpl_file_path))
		return $sro_tpl_file_path;
	return "$tpl_root_path/$default_sro_folder_name/$tpl_name.html";
}

function get_sro_params_path_for_sro_tpl_folder($sro_tpl_folder)
{
	global $default_sro_folder_name, $tpl_root_path, $sro_props_file_name;
	return "$tpl_root_path/$sro_tpl_folder/$sro_props_file_name";
}

function update_tpl_par_for_sro($sro_tpl_folder, $tpl_par)
{
	$get_sro_params_path= get_sro_params_path_for_sro_tpl_folder($sro_tpl_folder);
	$sro_params_txt= file_get_contents($get_sro_params_path);
	$p= json_decode($sro_params_txt);
	if (null==$p)
	{
		write_to_log("can not decode $get_sro_params_path!");
		write_to_log($sro_params_txt);
	}
	else
	{
		if (!isset($tpl_par->СРО))
		{
			$tpl_par->СРО= $p;
		}
		else
		{
			$sro= $tpl_par->СРО;
			if (!isset($sro->Комитет) && isset($p->Комитет))
				$sro->Комитет= $p->Комитет;
			if (!isset($sro->Руководитель) && isset($p->Руководитель))
				$sro->Руководитель= $p->Руководитель;
			if (isset($sro->Руководитель) && isset($p->Руководитель))
			{
				$sro_Руководитель= $sro->Руководитель;
				$p_Руководитель= $p->Руководитель;
				if (isset($p_Руководитель->Должность))
					$sro_Руководитель->Должность= $p_Руководитель->Должность;
				if (isset($p_Руководитель->Фамилия) && !isset($sro_Руководитель->Фамилия))
					$sro_Руководитель->Фамилия=$p_Руководитель->Фамилия;
				if (isset($p_Руководитель->Имя) && !isset($sro_Руководитель->Имя))
					$sro_Руководитель->Имя=$p_Руководитель->Имя;
				if (isset($p_Руководитель->Отчество) && !isset($sro_Руководитель->Отчество))
					$sro_Руководитель->Отчество=$p_Руководитель->Отчество;
			}
			if (!isset($sro->Адрес) && isset($p->Адрес))
				$sro->Адрес= $p->Адрес;
		}
	}
}

function get_sro_tpl_folder_by_RegNum($RegNum)
{
	switch ($RegNum)
	{
		case '0015': return 'strategy';
		default: return 'default';
	}
}

function clear_Document_locks($txt_locks, $current_time)
{
	$locks= array();
	if (null!=$txt_locks && ''!=$txt_locks)
	{
		$current_time_seconds= $current_time->format('U');
		$read_locks= json_decode($txt_locks);
		foreach ($read_locks as $lock)
		{
			write_to_log($lock);
			$lock_time= date_create_from_format('Y-m-d\TH:i:s',$lock->time);
			$lock_age= abs($current_time_seconds - $lock_time->format('U'));
			if ($lock_age <= (60*15)) // UNIX секунды..
				$locks[]= $lock;
		}
	}
	return $locks;
}

function publicate_locked_ManagerDocument($doc)
{
	global $email_settings;
	$current_time= safe_date_create();
	$locks= clear_Document_locks($doc->PubLock, $current_time);
	$pass= (true==$email_settings->enabled) ? generate_password(8) 
				: 'test_pass_'.$current_time->format('U');
	$locks[]= (object)array(
		'ip'=>getRealIPAddr()
		, 'pass'=>$pass
		, 'time'=>date_format($current_time,'Y-m-d\TH:i:s')
	);
	$txt_query= "update ManagerDocument set PubLock=? where id_ManagerDocument=?";
	execute_query_no_result($txt_query,array('ss',json_encode($locks), $doc->id_ManagerDocument));
	return $pass;
}

function publicate_locked_SRODocument($doc)
{
	global $email_settings;
	$current_time= safe_date_create();
	$locks= clear_Document_locks($doc->PubLock, $current_time);
	$pass= (true==$email_settings->enabled) ? generate_password(8) 
				: 'test_pass_'.$current_time->format('U');
	$locks[]= (object)array(
		'ip'=>getRealIPAddr()
		, 'pass'=>$pass
		, 'time'=>date_format($current_time,'Y-m-d\TH:i:s')
	);
	$txt_query= "update SRODocument set PubLock=? where id_SRODocument=?";
	execute_query_no_result($txt_query,array('ss',json_encode($locks), $doc->id_SRODocument));
	return $pass;
}

