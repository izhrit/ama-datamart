<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

require_once '../assets/libs/log/access_log.php';

function safe_prep_MProcedure($connection,$Должник,$id_Manager,$procedure_type, $case_number,$error_msg_prefix)
{
	$txt_query='call Add_MProcedure(?,?,?,?,?,?,null,?,@lid_MProcedure);';
	$qarguments= array('issssss',
		$id_Manager,
		$Должник->ИНН,$Должник->Наименование,$Должник->ОГРН,$Должник->СНИЛС,
		$case_number,$procedure_type
	);
	$rows= $connection->execute_query($txt_query,$qarguments);
	if (0==count($rows))
	{
		$rows= $connection->execute_query('select @lid_MProcedure as id_MProcedure;',array());
		$count_rows= count($rows);
		if (1!=count($rows))
			throw new Exception("got $count_rows @lid_MProcedure after call Add_MProcedure!");
		return $rows[0]->id_MProcedure;
	}
	else
	{
		$connection->rollback();
		require_once '../assets/libs/constraints/constraints_details.php';
		$error_text= contraint_error_text_by_rows($rows);
		$constraint_parameters_problems= array(
			'text'=>"$error_msg_prefix:$error_text"
			,'parameters'=>array('debtor'=>$Должник)
			,'problems'=>contraint_problems_by_rows($rows)
		);
		$res= array(
			'ok'=>false
			,'message'=>'НЕ удалось зарегистрировать процедуру на витрине!'.$error_text
			,'constraints'=>$constraint_parameters_problems
		);
		echo nice_json_encode($res);
		write_error_to_access_log('НЕ удалось зарегистрировать процедуру');
		write_constraint_error_to_access_log($constraint_parameters_problems);
		exit();
	}
}
