<?php

require_once '../assets/libs/efrsb/sync_Efrsb_event.php';
require_once '../assets/libs/efrsb/sync_Efrsb_message.php';

require_once '../assets/libs/push/dispatch_efrsb_events.php';
require_once '../assets/libs/push/dispatch_efrsb_messages.php';

require_once '../assets/libs/starts/update_starts_cb_fields.php';
require_once '../assets/libs/starts/update_starts_efrsb_fields.php';

require_once '../assets/libs/remote_jobs/update_remote_job_logs.php';

$hourly_job_parts= array
(
	 array('exec'=>'sync_messages',               'title'=>'загрузка новых сообщений из базы ЕФРСБ')
	,array('exec'=>'update_starts_cb_fields',     'title'=>'сверка модуля назначений c kad.arbitr.ru')
	,array('exec'=>'update_starts_efrsb_fields',  'title'=>'сверка модуля назначений c ефрсб')
	,array('exec'=>'dispatch_efrsb_events',       'title'=>'уведомления о событиях ЕФРСБ, запланированных в будующем')
	,array('exec'=>'dispatch_efrsb_messages',     'title'=>'уведомления о новых опубликованных сообщениях ЕФРСБ')
	,array('exec'=>'sync_events',                 'title'=>'загрузка новых событий из базы ЕФРСБ')
	,array('exec'=>'sync_remote_efrsb_job_log',   'title'=>'загрузка последних записей фоновых регламентных работ ЕФРСБ')
	,array('exec'=>'sync_remote_probili_job_log', 'title'=>'загрузка последних записей фоновых регламентных работ probili.ru')
);
