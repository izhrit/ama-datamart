<?php

require_once '../assets/libs/verify/verify_Debtors.php';
require_once '../assets/libs/verify/verify_Managers.php';
require_once '../assets/libs/verify/verify_Procedures.php';

require_once '../assets/libs/efrsb/sync_Efrsb_event.php';
require_once '../assets/libs/efrsb/sync_Efrsb_message.php';

require_once '../assets/libs/efrsb/sync_Efrsb_sro.php';
require_once '../assets/libs/efrsb/sync_Efrsb_manager.php';

require_once '../assets/libs/efrsb/sync_Efrsb_debtor.php';
require_once '../assets/libs/efrsb/sync_Efrsb_debtor_manager.php';

require_once '../assets/libs/push/delete_old_pushed_efrsb_events.php';
require_once '../assets/libs/push/delete_old_pushed_efrsb_messages.php';

require_once '../assets/libs/upload2bt/job.php';
require_once '../assets/libs/truncate_PData.php';

require_once '../assets/libs/using/using_procedure_index.php';

require_once '../assets/libs/requests/deliver_manager_requests.php';

require_once '../assets/libs/archivate_procedures.php';

$nightly_job_parts= array
(
	 array('exec'=>'delete_old_events',                'title'=>'удаление старых событий ЕФРСБ - старее 40 дней')
	,array('exec'=>'delete_old_messages',              'title'=>'удаление старых сообщений ЕФРСБ - более 60 дней')
	,array('exec'=>'delete_old_pushed_efrsb_events',   'title'=>'удаление из журнала уведомлений о событиях ЕФРСБ - старее 30 дней')
	,array('exec'=>'delete_old_pushed_efrsb_messages', 'title'=>'удаление из журнала уведомлений о сообщениях ЕФРСБ - старее 30 дней')

	,array('exec'=>'sync_messages',                    'title'=>'загрузка новых сообщений из базы ЕФРСБ')
	,array('exec'=>'sync_events',                      'title'=>'загрузка новых событий из базы ЕФРСБ')

	,array('exec'=>'sync_sros',                        'title'=>'загрузка новых СРО из базы ЕФРСБ')
	,array('exec'=>'sync_managers',                    'title'=>'загрузка новых АУ из базы ЕФРСБ')
	,array('exec'=>'sync_debtors',                     'title'=>'загрузка новых должников из базы ЕФРСБ')
	,array('exec'=>'sync_debtor_managers',             'title'=>'загрузка новых процедур из базы ЕФРСБ')

	,array('exec'=>'delete_old_PData',                 'title'=>'удаление записей об устаревших выгрузках на витрину')
	,array('exec'=>'archivate_procedures',             'title'=>'архивирование старых процедур')

	,array('exec'=>'move_procedure_using_to_indexed',  'title'=>'перенос журнала ПАУ в индексированную таблицу')

	,array('exec'=>'deliver_manager_requests',         'title'=>'отправка уведомлений о запросах в сро ау')
);

global $job_params;

if (isset($job_params->verify_with_efrsb) && true==$job_params->verify_with_efrsb)
{
	$nightly_job_parts= array_merge($nightly_job_parts,array(
	 array('exec'=>'verify_Managers',            'title'=>'проверка записей об АУ по базе ЕФРСБ')
	,array('exec'=>'verify_Debtors',             'title'=>'проверка записей о должниках по базе ЕФРСБ')
	,array('exec'=>'verify_Procedures',          'title'=>'проверка записей об процедурах по базе ЕФРСБ')
	));
}

if (isset($job_params->upload_to_bankrotech) && true==$job_params->upload_to_bankrotech)
	$nightly_job_parts[]= array('exec'=>'upload_to_bankrotech', 'title'=>'выгрузка реестров на bankro.tech');

