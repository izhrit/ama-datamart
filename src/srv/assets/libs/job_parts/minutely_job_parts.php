<?php

require_once '../assets/libs/push/send/push_send_method.php';

require_once '../assets/libs/push/deliver_efrsb_events.php';
require_once '../assets/libs/push/deliver_efrsb_messages.php';

require_once '../assets/actions/backend/alib_emails.php';

$minutely_job_parts= array
(
	 array('exec'=>'deliver_efrsb_events',   'title'=>'отправка push-уведомлений о событиях ЕФРСБ')
	,array('exec'=>'deliver_efrsb_messages', 'title'=>'отправка push-уведомлений о публикации сообщений ЕФРСБ')
	,array('exec'=>'deliver_emails',         'title'=>'отправка электронных писем')
);

