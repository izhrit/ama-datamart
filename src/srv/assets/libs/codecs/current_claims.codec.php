<?php

require_once '../assets/helpers/codec.xml.php';

class Current_claims_codec extends Xml_codec
{
	public $schema= array(
		'tagName'=>'Оперативно_рабочая_информация_о_текущих_требованиях'
		,'fields'=> array
		(
			'Кредиторы'=> array
			(
				'type'=>'array'
				,'item'=>array
				(
					'tagName'=>'Кредитор'
					,'fields'=>array
					(
						'Требования'=>array
						(
							'type'=> 'array'
							,'item'=>array
							(
								'tagName'=> 'Требование'
							)
						)
					)
				)
			)
		)
	);

	public $options= LIBXML_NOWARNING;
}
