<?php

require_once '../assets/helpers/codec.xml.php';

class Registry_codec extends Xml_codec
{
	public $schema= array(
		'tagName'=>'Оперативно_рабочая_информация_о_требованиях_кредиторов'
		,'fields'=> array
		(
			'Кредиторы'=> array
			(
				'type'=>'array'
				,'item'=>array
				(
					'tagName'=>'Кредитор'
					,'fields'=>array
					(
						'Требования'=>array
						(
							'type'=> 'array'
							,'item'=>array
							(
								'tagName'=> 'Требование'
								, 'fields'=>array
								(
									'Документы'=> array( 'type'=> 'array', 'item'=>array( 'tagName'=> 'Документ' ) )
									, 'Список_процентов_за_время_процедур'=> array( 'type'=> 'array', 'item'=>array( 'tagName'=> 'Проценты_за_время_процедур' ) )
									, 'Залоговые_обязательства'=> array( 'type'=> 'array', 'item'=>array( 'tagName'=> 'Залог' ) )
									, 'Погашения'=> array( 'type'=> 'array', 'item'=>array( 'tagName'=> 'Погашение' ) )
								)
							)
						)
					)
				)
			)
		)
	);

	public $options= LIBXML_NOWARNING;
}
