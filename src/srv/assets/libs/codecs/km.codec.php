<?php

require_once '../assets/helpers/codec.xml.php';

class KM_codec extends Xml_codec
{
	public $schema= array(
		'tagName'=>'Оперативно_рабочая_информация_о_конкурсной_массе'
		,'fields'=> array
		(
			'Конкурсная_масса'=> array(
				'type'=>'array'
				,'item'=>array(
					'tagName'=>'Категория'
					,'fields'=>array(
						'Объекты'=>array('type'=>'array','item'=>array('tagName'=>'Объект'))
					)
				)
			)
		)
	);

	public $options= LIBXML_NOWARNING;
}
