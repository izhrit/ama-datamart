<?php

require_once '../assets/helpers/codec.xml.php';

class Report_codec extends Xml_codec
{
	public $schema= array(
		'tagName'=>'Оперативно_рабочая_информация_для_отчёта_АУ_по_процедуре'
		,'fields'=> array
		(
			'КПиРИ'=> array(
				'fields'=>array(
					'Расходы'=>array(
						'type'=>'array'
						,'item'=>array(
							'tagName'=>'Группа_расходов'
							,'fields'=>array(
								'Расходы_группы'=>array('type'=>'array','item'=>array('tagName'=>'Расход'))
							)
						)
					)
					,'Дебиторская_задолженность'=> array('type'=>'array','item'=>array('tagName'=>'Задолженность'))
					,'Работники'=> array(
						'fields'=>array(
							'Работающие'=>array('type'=>'array','item'=>array('tagName'=>'Работник'))
							,'Уволенные'=>array('type'=>'array','item'=>array('tagName'=>'Работник'))
						)
					)
					,'Поступившие_денежные_средства'=> array('type'=>'array','item'=>array('tagName'=>'Поступление'))
					,'Субсидиарка'=> array('type'=>'array','item'=>array('tagName'=>'Субсидиарка'))
				)
			)
			,'Привлеченные_специалисты'=> array('type'=>'array','item'=>array('tagName'=>'Специалист'))
			,'Жалобы_На_Действия_АУ'=> array(
				'type'=>'array'
				,'item'=>array(
					'tagName'=>'Жалоба'
					,'fields'=>array(
						'Рассмотрение'=>array(
							'fields'=>array(
								'Организация'=>array('type'=>'string')
								,'Дата'=>array('type'=>'string')
								,'Решение'=>array('type'=>'string')
							)
						)
					)
				)
			)
			,'Меры'=> array(
				'type'=>'array'
				,'item'=>array(
					'tagName'=>'Группа_мер'
					,'fields'=>array(
						'Меры_в_группе'=> array('type'=>'array','item'=>array('tagName'=>'Мера'))
					)
				)
			) // todo!
		)
	);

	public $options= LIBXML_NOWARNING;
}
