<?php

class Deals_codec extends Xml_codec
{
	public $schema= array(
		'tagName'=>'Оперативно_рабочая_информация_о_сделках'
		,'fields'=> array
		(
			'Сделки'=> array(
				'type'=>'array'
				,'item'=>array(
					'tagName'=>'Сделка'
					,'fields'=>array(
						'Исполнения'=>array('type'=>'array','item'=>array('tagName'=>'Исполнение'))
					)
				)
			)
		)
	);

	public $options= LIBXML_NOWARNING;
}
