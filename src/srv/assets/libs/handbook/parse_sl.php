<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/time.php';

require_once '../assets/libs/auth/check.php';

//$auth_info= CheckAuthAdmin();

function write_log_text($txt)
{
	echo job_time()." $txt\r\n";
}

function write_log($a)
{
	write_log_text(is_string($a) ? $a : print_r($a,true));
}

function get_td_content($td_part)
{
	$marker_start= '>';
	$marker_start_len= strlen($marker_start);
	$marker_start_pos= mb_strpos($td_part,$marker_start);
	$td_part= mb_substr($td_part,$marker_start_pos+$marker_start_len);

	$marker_stop= '</td>';
	$marker_stop_len= strlen($marker_stop);
	$marker_stop_pos= mb_strpos($td_part,$marker_stop);

	$td_part= mb_substr($td_part,0,$marker_stop_pos);

	return trim($td_part);
}

$tr_start= '<tr';
$tr_start_len= strlen($tr_start);
$td_start_marker= '<td';
$href_marker_start= 'href="';
$href_marker_start_len= strlen($href_marker_start);
$href_marker_stop= '">';
$href_marker_stop_len= mb_strlen($href_marker_stop);
$a_marker_stop= '</a>';
$a_marker_stop_len= mb_strlen($a_marker_stop);

function parse_page_row($tr_part)
{
	global $tr_start, $tr_start_len, $td_start_marker, 
		$href_marker_start, $href_marker_start_len, $href_marker_stop,
		$href_marker_stop_len, $a_marker_stop, $a_marker_stop_len;

	$tr_start_pos= mb_strpos($tr_part,$tr_start);
	$part= mb_substr($tr_part,$tr_start_pos+$tr_start_len);

	$td_parts= explode($td_start_marker,$tr_part);

	$values= array();

	$first_td= true;
	foreach ($td_parts as $td_part)
	{
		$v= get_td_content($td_part);

		if (''!=$v || !$first_td)
		{
			$href_marker_pos= mb_strpos($v,$href_marker_start);
			if (FALSE===$href_marker_pos)
			{
				$values[]= trim(str_replace('</a>','',$v));
			}
			else
			{
				$v= mb_substr($v,$href_marker_pos+$href_marker_start_len);
				$href_marker_stop_pos= mb_strpos($v,$href_marker_stop);

				$url= mb_substr($v,0,$href_marker_stop_pos);
				$values[]= $url;
				$v= mb_substr($v,$href_marker_stop_pos+$href_marker_stop_len);

				$a_marker_stop_pos= mb_strpos($v,$a_marker_stop);
				$text= mb_substr($v,0,$a_marker_stop_pos);
				$text= trim($text);

				$values[]= $text;
			}
		}
		$first_td= false;
	}

	return $values;
}

$кварталы= array('1'=>'01.01.','2'=>'01.04.','3'=>'01.07.','4'=>'01.10.');

function parse_page($page,$html)
{
	global $кварталы;
	$marker_start= 'Квартал год</td>';
	$pos_marker_start= mb_strpos($html,$marker_start);
	$html= mb_substr($html,$pos_marker_start);

	$marker_stop= '</table>';
	$pos_marker_stop= mb_strpos($html,$marker_stop);
	$html= mb_substr($html,0,$pos_marker_stop);

	$marker_separator= '</tr>';
	$parts= explode($marker_separator,$html);

	$lines= array();

	$first_row= true;
	foreach ($parts as $part)
	{
		if (!$first_row)
		{
			$parsed_row= parse_page_row($part);
			//print_r($parsed_row);
			if (0!=count($parsed_row))
			{
				$line= (object)array
				(
					'lines'=>$parsed_row
					,'квартал'=>$parsed_row[0]
					,'Прожиточный_минимум'=>(object)array(
						'Подушевой'=>$parsed_row[1]
						,'Трудоспособных'=>$parsed_row[2]
						,'Пенсионеров'=>$parsed_row[3]
						,'Несровершеннолетних'=>$parsed_row[4]
					)
				);
				$Регламентирующий_акт= 6==count($parsed_row) 
					? (object)array('Номер_дата'=>$parsed_row[5])
					: (object)array('URL'=>$parsed_row[5], 'Номер_дата'=>$parsed_row[6]);
				$Номер_дата_parts= explode(' от ',$Регламентирующий_акт->Номер_дата);
				$Регламентирующий_акт->Название= trim($Номер_дата_parts[0]);
				$Регламентирующий_акт->Дата= trim($Номер_дата_parts[1]);
				$line->Регламентирующий_акт= $Регламентирующий_акт;

				$квартал_parts =explode(' ',$line->квартал);
				$line->Дата= $кварталы[$квартал_parts[0]].$квартал_parts[2];

				$lines[]= $line;
			}
		}
		$first_row= false;
	}

	return $lines;
}

function parse_pages($pages)
{
	$sql= "insert into parsed_Sub_level\r\n (OKATO,    StartDate,    Sum_Common,\tSum_Employable,\tSum_Pensioner,\tSum_Infant,\tReglament_Date, Reglament_Title, Reglament_Url)\r\nvalues\r\n";
	$count= 0;
	foreach ($pages as $page)
	{
		//if ('Башкортостан'!=$page->text) continue;
		//write_log("download url:\"{$page->url}\" ..");
		$html= file_get_contents("http://potrebkor.ru/{$page->url}");
		//write_log(".. downloaded!");
		//write_log("parse ..");
		$lines= parse_page($page,$html);
		//write_log(".. parsed!");
		//print_r($lines);
		$sql.= " -- {$page->text}\r\n";
		foreach ($lines as $line)
		{
			$sql.= 0==$count ? ' (' : ',(';
			$sql.= "'{$page->ОКАТО}', ";
			$sql.= "'{$line->Дата}', ";

			$sql.= "{$line->Прожиточный_минимум->Подушевой},\t\t";
			$sql.= "{$line->Прожиточный_минимум->Трудоспособных},\t\t";
			$sql.= "{$line->Прожиточный_минимум->Пенсионеров},\t\t";
			$sql.= "{$line->Прожиточный_минимум->Несровершеннолетних},\t\t";

			$sql.= "'{$line->Регламентирующий_акт->Дата}',   ";
			$sql.= "'{$line->Регламентирующий_акт->Название}', \t ";
			$URL= !isset($line->Регламентирующий_акт->URL) ? '' : $line->Регламентирующий_акт->URL;
			$sql.= "'$URL'";

			$sql.= ")\r\n";
			$count++;
		}
		$sql.= "\r\n";
		//break;
	}
	echo $sql;
}

$pages= array(
	(object)array('ОКАТО'=>'79 000',	"text"=>"Адыгея",			"url"=> "minimum-adigeia.html"),
	(object)array('ОКАТО'=>'84 000',	"text"=>"Алтай",			"url"=> "minimum-altai.html"),
	(object)array('ОКАТО'=>'01 000',	"text"=>"Алтайский край",	"url"=> "minimum-altaiskii.html"),
	(object)array('ОКАТО'=>'10 000',	"text"=>"Амурская",		"url"=> "minimum-amurskaia.html"),
	(object)array('ОКАТО'=>'11 000',	"text"=>"Архангельская",	"url"=> "minimum-arhangelskaia.html"),
	(object)array('ОКАТО'=>'12 000',	"text"=>"Астраханская",	"url"=> "minimum-astrahanskaia.html"),
	(object)array('ОКАТО'=>'80 000',	"text"=>"Башкортостан",	"url"=> "minimum-bashkortostan.html"),
	(object)array('ОКАТО'=>'14 000',	"text"=>"Белгородская",	"url"=> "minimum-belgorodskaia.html"),
	(object)array('ОКАТО'=>'15 000',	"text"=>"Брянская",		"url"=> "minimum-brianskaia.html"),
	(object)array('ОКАТО'=>'81 000',	"text"=>"Бурятия",		"url"=> "minimum-buriatiia.html"),
	(object)array('ОКАТО'=>'17 000',	"text"=>"Владимирская",	"url"=> "minimum-vladimirskaia.html"),
	(object)array('ОКАТО'=>'18 000',	"text"=>"Волгоградская",	"url"=> "minimum-volgogradskaia.html"),
	(object)array('ОКАТО'=>'19 000',	"text"=>"Вологодская",	"url"=> "minimum-vologodskaia.html"),
	(object)array('ОКАТО'=>'20 000',	"text"=>"Воронежская",	"url"=> "minimum-voronejskaia.html"),
	(object)array('ОКАТО'=>'82 000',	"text"=>"Дагестан",		"url"=> "minimum-dagestan.html"),
	(object)array('ОКАТО'=>'99 000',	"text"=>"Еврейская",		"url"=> "minimum-evreiskaia.html"),
	(object)array('ОКАТО'=>'76 000',	"text"=>"Забайкальский",	"url"=> "minimum-zabaikalskii.html"),
	(object)array('ОКАТО'=>'24 000',	"text"=>"Ивановская",		"url"=> "minimum-ivanovskaia.html"),
	(object)array('ОКАТО'=>'26 000',	"text"=>"Ингушетия",		"url"=> "minimum-ingushetiia.html"),
	(object)array('ОКАТО'=>'25 000',	"text"=>"Иркутская",		"url"=> "minimum-irkutskaia.html"),
	(object)array('ОКАТО'=>'83 000',	"text"=>"Кабардино-Балкарская","url"=> "minimum-kabardino-balkarskaia.html"),
	(object)array('ОКАТО'=>'27 000',	"text"=>"Калининградская","url"=> "minimum-kaliningradskaia.html"),
	(object)array('ОКАТО'=>'85 000',	"text"=>"Калмыкия",		"url"=> "minimum-kalmikiia.html"),
	(object)array('ОКАТО'=>'29 000',	"text"=>"Калужская",		"url"=> "minimum-kalujskaia.html"),
	(object)array('ОКАТО'=>'30 000',	"text"=>"Камчатский",		"url"=> "minimum-kamchatskii.html"),
	(object)array('ОКАТО'=>'91 000',	"text"=>"Карачаево-Черкесская","url"=> "minimum-karachaevo-cherkesskaia.html"),
	(object)array('ОКАТО'=>'86 000',	"text"=>"Карелия",		"url"=> "minimum-kareliia.html"),
	(object)array('ОКАТО'=>'32 000',	"text"=>"Кемеровская",	"url"=> "minimum-kemerovskaia.html"),
	(object)array('ОКАТО'=>'33 000',	"text"=>"Кировская",		"url"=> "minimum-kirovskaia.html"),
	(object)array('ОКАТО'=>'87 000',	"text"=>"Коми",			"url"=> "minimum-komi.html"),
	(object)array('ОКАТО'=>'34 000',	"text"=>"Костромская",	"url"=> "minimum-kostromskaia.html"),
	(object)array('ОКАТО'=>'03 000',	"text"=>"Краснодарский",	"url"=> "minimum-krasnodarskiy-krai.html"),
	(object)array('ОКАТО'=>'04 000',	"text"=>"Красноярский",	"url"=> "minimum-krasnoiarskii.html"),
	(object)array('ОКАТО'=>'35 000',	"text"=>"Крым",			"url"=> "minimum-krim.html"),
	(object)array('ОКАТО'=>'37 000',	"text"=>"Курганская",		"url"=> "minimum-kurganskaia.html"),
	(object)array('ОКАТО'=>'38 000',	"text"=>"Курская",		"url"=> "minimum-kurskaia.html"),
	(object)array('ОКАТО'=>'41 000',	"text"=>"Ленинградская",	"url"=> "minimum-leningradskaia-oblast.html"),
	(object)array('ОКАТО'=>'42 000',	"text"=>"Липецкая",		"url"=> "minimum-lipetckaia.html"),
	(object)array('ОКАТО'=>'44 000',	"text"=>"Магаданская",	"url"=> "minimum-magadanskaia.html"),
	(object)array('ОКАТО'=>'88 000',	"text"=>"Марий Эл",		"url"=> "minimum-marii-el.html"),
	(object)array('ОКАТО'=>'89 000',	"text"=>"Мордовия",		"url"=> "minimum-mordoviia.html"),
	(object)array('ОКАТО'=>'45 000',	"text"=>"Москва",			"url"=> "minimum-moskva.html"),
	(object)array('ОКАТО'=>'46 000',	"text"=>"Московская",		"url"=> "minimum-moskovskaia-oblast.html"),
	(object)array('ОКАТО'=>'47 000',	"text"=>"Мурманская",		"url"=> "minimum-murmanskaia.html"),
	(object)array('ОКАТО'=>'11 100',	"text"=>"Ненецкий",		"url"=> "minimum-nenetckii.html"),
	(object)array('ОКАТО'=>'22 000',	"text"=>"Нижегородская",	"url"=> "minimum-nijegorodskaia.html"),
	(object)array('ОКАТО'=>'49 000',	"text"=>"Новгородская",	"url"=> "minimum-novgorodskaia.html"),
	(object)array('ОКАТО'=>'50 000',	"text"=>"Новосибирская",	"url"=> "minimum-novosibirskaia.html"),
	(object)array('ОКАТО'=>'52 000',	"text"=>"Омская",			"url"=> "minimum-omskaia.html"),
	(object)array('ОКАТО'=>'53 000',	"text"=>"Оренбургская",	"url"=> "minimum-orenburgskaia.html"),
	(object)array('ОКАТО'=>'54 000',	"text"=>"Орловская",		"url"=> "minimum-orlovskaia.html"),
	(object)array('ОКАТО'=>'56 000',	"text"=>"Пензенская",		"url"=> "minimum-penzenskaia.html"),
	(object)array('ОКАТО'=>'57 000',	"text"=>"Пермский",		"url"=> "minimum-permskii.html"),
	(object)array('ОКАТО'=>'05 000',	"text"=>"Приморский",		"url"=> "minimum-primorskii.html"),
	(object)array('ОКАТО'=>'58 000',	"text"=>"Псковская",		"url"=> "minimum-pskovskaia.html"),
	(object)array('ОКАТО'=>'00 000',	"text"=>" Российская Федерация","url"=> "minimum-russia.html"),
	(object)array('ОКАТО'=>'60 000',	"text"=>"Ростовская",		"url"=> "minimum-rostovskaia.html"),
	(object)array('ОКАТО'=>'61 000',	"text"=>"Рязанская",		"url"=> "minimum-riazanskaia.html"),
	(object)array('ОКАТО'=>'36 000',	"text"=>"Самарская",		"url"=> "minimum-samarskaia.html"),
	(object)array('ОКАТО'=>'40 000',	"text"=>"Санкт-Петербург","url"=> "minimum-saint-petersburg.html"),
	(object)array('ОКАТО'=>'63 000',	"text"=>"Саратовская",	"url"=> "minimum-saratovskaia.html"),
	(object)array('ОКАТО'=>'98 000',	"text"=>"Саха (Якутия)",	"url"=> "minimum-saha-iakutiia.html"),
	(object)array('ОКАТО'=>'64 000',	"text"=>"Сахалинская",	"url"=> "minimum-sahalinskaia.html"),
	(object)array('ОКАТО'=>'65 000',	"text"=>"Свердловская",	"url"=> "minimum-sverdlovskaia.html"),
	(object)array('ОКАТО'=>'67 000',	"text"=>"Севастополь",	"url"=> "minimum-sevastopol.html"),
	(object)array('ОКАТО'=>'90 000',	"text"=>"Северная Осетия-Алания","url"=> "minimum-severnaia-osetiia-alaniia.html"),
	(object)array('ОКАТО'=>'66 000',	"text"=>"Смоленская",		"url"=> "minimum-smolenskaia.html"),
	(object)array('ОКАТО'=>'07 000',	"text"=>"Ставропольский",	"url"=> "minimum-stavropolskii.html"),
	(object)array('ОКАТО'=>'68 000',	"text"=>"Тамбовская",		"url"=> "minimum-tambovskaia.html"),
	(object)array('ОКАТО'=>'92 000',	"text"=>"Татарстан",		"url"=> "minimum-tatarstan.html"),
	(object)array('ОКАТО'=>'28 000',	"text"=>"Тверская",		"url"=> "minimum-tverskaia.html"),
	(object)array('ОКАТО'=>'69 000',	"text"=>"Томская",		"url"=> "minimum-tomskaia.html"),
	(object)array('ОКАТО'=>'70 000',	"text"=>"Тульская",		"url"=> "minimum-tulskaia.html"),
	(object)array('ОКАТО'=>'93 000',	"text"=>"Тыва",			"url"=> "minimum-tiva.html"),
	(object)array('ОКАТО'=>'71 000',	"text"=>"Тюменская",		"url"=> "minimum-tiumenskaia.html"),
	(object)array('ОКАТО'=>'94 000',	"text"=>"Удмуртская",		"url"=> "minimum-udmurtskaia.html"),
	(object)array('ОКАТО'=>'73 000',	"text"=>"Ульяновская",	"url"=> "minimum-ulianovskaia.html"),
	(object)array('ОКАТО'=>'08 000',	"text"=>"Хабаровский",	"url"=> "minimum-habarovskii.html"),
	(object)array('ОКАТО'=>'95 000',	"text"=>"Хакасия",		"url"=> "minimum-hakasiia.html"),
	(object)array('ОКАТО'=>'71 100',	"text"=>"Ханты-Мансийский Югра","url"=> "minimum-hanti-mansiiskii.html"),
	(object)array('ОКАТО'=>'75 000',	"text"=>"Челябинская",	"url"=> "minimum-cheliabinskaia.html"),
	(object)array('ОКАТО'=>'96 000',	"text"=>"Чеченская",		"url"=> "minimum-chechenskaia.html"),
	(object)array('ОКАТО'=>'97 000',	"text"=>"Чувашская",		"url"=> "minimum-chuvashskaia.html"),
	(object)array('ОКАТО'=>'77 000',	"text"=>"Чукотский",		"url"=> "minimum-chukotskii.html"),
	(object)array('ОКАТО'=>'71 100',	"text"=>"Югра Ханты-Мансийский","url"=> "minimum-hanti-mansiiskii.html"),
	(object)array('ОКАТО'=>'98 000',	"text"=>"Якутия (Саха)",	"url"=> "minimum-saha-iakutiia.html"),
	(object)array('ОКАТО'=>'71 140',	"text"=>"Ямало-Ненецкий",	"url"=> "minimum-iamalo-nenetckii.html"),
	(object)array('ОКАТО'=>'78 000',	"text"=>"Ярославская",	"url"=> "minimum-iaroslavskaia.html")
);



function Parse_handbook_sl()
{
	global $pages;
	parse_pages($pages);
}