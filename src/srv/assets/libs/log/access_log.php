<?php

require_once 'access_log_event_types.php';
require_once '../assets/helpers/realip.php';

function write_to_access_log_fields($type,$fields=null)
{
	try
	{
		global $log_event_type;
		$id_type= $log_event_type[$type];
		$ip= getRealIPAddr();
		$txt_query= "insert into access_log set id_Log_type=?, IP=?";
		$parameters0= 'ss';
		$parameters= array($parameters0,$id_type,$ip);
		if (null!=$fields)
		{
			foreach ($fields as $field_name => $field_value)
			{
				$txt_query.= ", $field_name=?";
				$parameters0.= 's';
				$parameters[]= $field_value;
			}
		}
		$parameters[0]= $parameters0;
		execute_query_no_result($txt_query,$parameters);
	}
	catch (Exception $ex)
	{
		write_to_log('Unhandled exception during writing to access_log occurred: ' . get_class($ex) . ' - ' . $ex->getMessage());
		write_to_log('$_GET:');
		write_to_log($_GET);
	}
}

function write_to_access_log($type)
{
	write_to_access_log_fields($type);
}

function write_to_access_log_id($type,$id)
{
	write_to_access_log_fields($type,array('Id'=>$id));
}

function write_to_access_log_details($type,$details)
{
	write_to_access_log_fields($type,array('Details'=>$details));
}
