<?php

global $log_event_type;
$log_event_type= array
(
  'login/manager/web' => 1
, 'login/viewer/web' => 2
, 'login/customer/web' => 3
, 'login/manager/android' => 4
, 'login/viewer/android' => 5
, 'login/customer/android' => 6
, 'login/manager/ios' => 7
, 'login/viewer/ios' => 8
, 'login/customer/ios' => 9
, 'login/manager' => 10
, 'login/viewer' => 11
, 'login/customer' => 12
, 'login' => 13
, 'login/customer/ama' => 14
, 'login/manager/ama' => 15

, 'auth/manager' => 16
, 'auth/customer' => 17
, 'auth/partner' => 18
, 'auth/customer/ama' => 19
, 'auth/manager/ama' => 20

, 'login/admin' => 21
, 'login/viewer/fa' => 22

, 'sublevel-info' => 23

, 'push/unsubscribe' => 24

, 'login/sro' => 25
, 'robot/found-start-case' => 26

, 'electrokk/create/ama' => 27
, 'electrokk/open/ama' => 28
, 'electrokk/open/ama/error' => 29
, 'electrokk/create/ama/error' => 30

, 'constraint/api/manager/error' => 31
, 'constraint/api/customer/error' => 32
, 'constraint/api/error' => 33

, 'lytdybr'=>-1

, 'login/debtor/web' => 34
, 'login/debtor/ios' => 35
, 'login/debtor/android' => 36
, 'login/debtor' => 37

, 'login/customer/ama/error' => 38
, 'login/manager/ama/error' => 39
, 'login/ama/error' => 40

, 'electrokk/open/ama/ok' => 41

, 'exposure/store/asset/start' => 42
, 'exposure/store/asset/finish' => 43
, 'exposure/store/document/start' => 44
, 'exposure/store/document/finish' => 45
);
