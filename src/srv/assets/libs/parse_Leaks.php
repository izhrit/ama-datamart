<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/zip.php';
require_once '../assets/helpers/job.php';

function SetPDataProcessed($id_PData, $processed)
{
	$query_text= "UPDATE PData SET Processed=? WHERE id_PData=?";
	execute_query_no_result($query_text,array('ii',$processed, $id_PData));
}

function processPDataSpecs($id_PData, $specs, $id_MProcedure)
{
	$con= default_dbconnect();
	$con->begin_transaction();
	try
	{
		$query_text = "DELETE Leak.* FROM Leak
			inner JOIN PData ON PData.id_PData=Leak.id_PData
			WHERE PData.id_MProcedure=?";
		execute_query_no_result($query_text,array('i',$id_MProcedure));
		foreach ($specs as $spec)
		{
			$isAccredited= !isset($spec->Аккредитация) ? 0 : (($spec->Аккредитация->__toString() != "отсутствует")?1:0);
			$LeakSum= !isset($spec->Оплата->Размер) ? 0 : $spec->Оплата->Размер->__toString();
			if (''==trim($LeakSum))
				$LeakSum= 0;
			$LeakDate= date_format(date_create_from_format('d.m.Y', $spec->Договор->Срок->Конец->Дата->__toString()),'Y-m-d');
			if (''==trim($LeakDate))
				$LeakDate= null;
			$ContragentName= $spec->Наименование->__toString();
			$ContragentInn= $spec->ИНН->__toString();
			$query_text= "insert into `Leak` 
				   (id_PData,  isAccredited,  LeakSum,  LeakDate,  ContragentName,  ContragentInn)
			values (?,         ?,             ?,        ?,         ?,               ?)";
			execute_query_no_result($query_text,array("iissss",
				    $id_PData, $isAccredited, $LeakSum, $LeakDate, $ContragentName, $ContragentInn));
		}
		execute_query_no_result('UPDATE PData SET Processed=1 WHERE id_PData=?', array('i',$id_PData));
		$con->commit();
	}
	catch (Exception $ex)
	{
		$con->rollback();
		throw $ex;
	}
}

function ProcessPDataContent($row,$report_content)
{
	if (null!=$report_content)
	{
		$report_xml= new SimpleXMLElement(str_replace('xmlns="myNamespace"', '', $report_content));
		if (isset($report_xml->Привлеченные_специалисты->Специалист))
		{
			$specs= $report_xml->Привлеченные_специалисты->Специалист;
			processPDataSpecs($row->id_PData, $specs,$row->id_MProcedure);
			return true;
		}
	}
	return false;
}

function ProcessPDataPortion($max_portion_size)
{
	echo job_time()." query $max_portion_size records from PData to parse\r\n";
	$query_text = "select fileData, id_PData, id_MProcedure from PData pd 
			where pd.Processed is null order by revision LIMIT ?";
	$rows = execute_query($query_text,array('s',$max_portion_size));
	$count_rows= count($rows);
	echo job_time()."   got $count_rows records\r\n";
	if ($count_rows > 0)
	{
		echo job_time()." process reports of the id_PData";
		foreach ($rows as $row)
		{
			global $temp_zip_filepath;
			try
			{
				echo ' '.$row->id_PData;
				if (' '==$row->fileData)
				{
					SetPDataProcessed($row->id_PData, 0);
				}
				else
				{
					file_put_contents($temp_zip_filepath, $row->fileData);
					$report_content= ReadZippedFile($temp_zip_filepath,'report.xml');
					unlink($temp_zip_filepath);
					if (!ProcessPDataContent($row,$report_content))
						SetPDataProcessed($row->id_PData, 0);
				}
			}
			catch(Exception $ex)
			{
				echo "\r\non id_PData= $row->id_PData\r\n";
				$exception_class= get_class($ex);
				$exception_Message= $ex->getMessage();
				echo "\r\nexception occurred: $exception_class - $exception_Message\r\n";
				echo 'catched Exception:';
				print_r($ex);
				unlink($temp_zip_filepath);
				SetPDataProcessed($row->id_PData, 0);
			}
		}
		echo "\r\n";
	}
	return $count_rows;
}

function ProcessPData()
{
	global $default_db_options;
	echo job_time()." parse Leaks {\r\n";
	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));
	while (date_create() < $time_start_to_stop && 0!=ProcessPDataPortion(100));
	echo job_time()." parse Leaks }\r\n";
}

