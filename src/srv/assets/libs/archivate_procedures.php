<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/job.php';

function sync_efrsb_archive_to_procedures($state= null)
{
	$start_efrsb_debtor_revision= 0;
	if (null!=$state)
	{
		$parsed_state= json_decode($state);
		if (null==$parsed_state)
		{
			echo job_time() . " can not parse state:\r\n";
			print_r($state);
		}
		if (isset($parsed_state->start_efrsb_debtor_revision))
			$start_efrsb_debtor_revision= $parsed_state->start_efrsb_debtor_revision;
	}

	echo job_time() . " sync efrsb_debtor.Archive for revision=$start_efrsb_debtor_revision\r\n";

	echo job_time() . " get max revision of efrsb_debtor ..\r\n";
	$txt_query= 'select max(Revision) max_Revision from efrsb_debtor;';
	$rows= execute_query($txt_query,array());
	$max_efrsb_debtor_revision= 0==count($rows) ? 0 : $rows[0]->max_Revision;
	if (null==$max_efrsb_debtor_revision)
		$max_efrsb_debtor_revision= 0;
	echo job_time() . " .. got max revision of efrsb_debtor $max_efrsb_debtor_revision\r\n";

	echo job_time() . " sync MProcedure Archive from efrsb_debtor..\r\n";
	$txt_query= 'update MProcedure mp
inner join Debtor d on d.id_Debtor=mp.id_Debtor
inner join efrsb_debtor ed on ed.BankruptId= d.BankruptId
set mp.Archive=ed.Archive
where ed.Revision>?;';
	$synced_procedures= execute_query_get_affected_rows($txt_query,array('i',$start_efrsb_debtor_revision));
	echo job_time() . " synced $synced_procedures MProcedure Archive from efrsb_debtor\r\n";

	return (object)array('synced'=>$synced_procedures,'new_state'=>array('start_efrsb_debtor_revision'=>$max_efrsb_debtor_revision));
}

function archivate_duplicated_procedures()
{
	echo job_time() . " set Arhcive = 1 to all Procedures except last for each Debtor \r\n";
	
	$txt_query= 'update MProcedure mp
left join MProcedure mpo on mp.id_Debtor= mpo.id_Debtor and mpo.id_MProcedure<>mp.id_MProcedure and mpo.TimeCreated>mp.TimeCreated
set mp.Archive=1
where 0=mp.Archive and mpo.id_MProcedure is not null;';

	$archivated_procedures= execute_query_get_affected_rows($txt_query,array(''));

	echo job_time() . " .. updated $archivated_procedures MProcedure rows.\r\n";

	return $archivated_procedures;
}

function archivate_procedures($state= null)
{
	$sync_result= sync_efrsb_archive_to_procedures($state);
	$synced_procedures= $sync_result->synced;

	$archivated_procedures= archivate_duplicated_procedures();

	return (object)array(
		'results'=>"Синхронизирована архивация для процедур: $synced_procedures,
  помечено в архив перекрытых процедур: $archivated_procedures"
		,'state'=>json_encode($sync_result->new_state)
	);
}
