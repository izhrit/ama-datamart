<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/sax.php';

require_once 'MessageType.php';

function parse_for_Message_table($msg)
{
	global $MessageInfo_MessageType_desciptions_by_api_name;

	$PublishDate_parser= new Field_parser(array('MessageData','PublishDate'));
	$Person_INN_parser= new Field_parser(array('MessageData','BankruptInfo','BankruptPerson','INN'));
	$Firm_INN_parser= new Field_parser(array('MessageData','BankruptInfo','BankruptFirm','INN'));
	$OGRN_parser= new Field_parser(array('MessageData','BankruptInfo','BankruptFirm','@OGRN'));
	$SNILS_parser= new Field_parser(array('MessageData','BankruptInfo','BankruptPerson','SNILS'));
	$Id_parser= new Field_parser(array('MessageData','Id'));
	$ArbitrManagerID_parser= new Field_parser(array('MessageData','PublisherInfo','ArbitrManager','@ID'));
	$BankruptId_parser= new Field_parser(array('MessageData','BankruptId'));
	$MessageInfo_MessageType_parser= new Field_parser(array('MessageData','MessageInfo','@MESSAGETYPE'));
	$Number_parser= new Field_parser(array('MessageData','Number'));
	$MessageGUID_parser= new Field_parser(array('MessageData','MessageGUID'));

	// new parsers (v2):
	$v2_Firm_INN_parser= new Field_parser(array('MessageData','Bankrupt','Inn'));
	$v2_Firm_OGRN_parser= new Field_parser(array('MessageData','Bankrupt','Ogrn'));
	$v2_Firm_SNILS_parser= new Field_parser(array('MessageData','Bankrupt','Snils'));

	$msg_parser = new XMLParserArray(array(
		$PublishDate_parser
		,$Person_INN_parser
		,$Firm_INN_parser
		,$OGRN_parser
		,$SNILS_parser
		,$Id_parser
		,$ArbitrManagerID_parser
		,$BankruptId_parser
		,$MessageInfo_MessageType_parser
		,$Number_parser
		,$MessageGUID_parser

		,$v2_Firm_INN_parser
		,$v2_Firm_OGRN_parser
		,$v2_Firm_SNILS_parser

		//,new XMLParser_echor()
	));

	$xml_parser = xml_parser_create();
	$msg_parser->bind($xml_parser);
	if (!xml_parse($xml_parser, $msg, /* is_final= */TRUE))
	{
		$ex_text= sprintf("Ошибка XML: %s на строке %d",
						xml_error_string(xml_get_error_code($xml_parser)),
						xml_get_current_line_number($xml_parser));
		xml_parser_free($xml_parser);
		throw new Exception($ex_text);
	}
	xml_parser_free($xml_parser);

	$parsed_msg= (object)array();
	$parsed_msg->INN= any_not_null_value(array($Person_INN_parser,$Firm_INN_parser,$v2_Firm_INN_parser));
	$parsed_msg->OGRN= any_not_null_value(array($OGRN_parser,$v2_Firm_OGRN_parser));
	$parsed_msg->SNILS= any_not_null_value(array($SNILS_parser,$v2_Firm_SNILS_parser));

	$parsed_msg->PublishDate= $PublishDate_parser->value;
	$parsed_msg->Id= $Id_parser->value;
	$parsed_msg->ArbitrManagerID= $ArbitrManagerID_parser->value;
	$parsed_msg->MessageInfo_MessageType= $MessageInfo_MessageType_parser->value;
	$parsed_msg->BankruptId= $BankruptId_parser->value;
	$parsed_msg->Number= $Number_parser->value;
	$parsed_msg->MessageGUID= $MessageGUID_parser->value;

	return $parsed_msg;
}