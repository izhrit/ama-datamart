<?php

$MessageInfo_MessageType_desciptions= array
(
array
(
	'db_value'=>'a'
	,'api_name'=>'ArbitralDecree'
	,'Readable'=>'Сведения о судебном акте'
	,'TextTag'=>'CourtDecision'
	,'Readable_short_about'=>'о судебном акте'
)
,array
(
	'db_value'=>'b'
	,'api_name'=>'Auction'
	,'Readable'=>'Объявление о проведении торгов'
	,'Readable_short_about'=>'о торгах'
)
,array
(
	'db_value'=>'c'
	,'api_name'=>'Meeting'
	,'Readable'=>'Сведения о собрании кредиторов'
	,'Readable_short_about'=>'об СК'
)
,array
(
	'db_value'=>'d'
	,'api_name'=>'MeetingResult'
	,'Readable'=>'Сведения о результатах проведения собрания кредиторов'
	,'Readable_short_about'=>'о результатах СК'
)
,array
(
	'db_value'=>'e'
	,'api_name'=>'TradeResult'
	,'Readable'=>'Сведения о результатах торгов'
	,'Readable_short_about'=>'о результатах торгов'
)
,array
(
	'db_value'=>'f'
	,'api_name'=>'Other'
	,'Readable'=>'Иные сведения'
	,'Readable_short_about'=>'об ином'
)
,array
(
	'db_value'=>'g'
	,'api_name'=>'AppointAdministration'
	,'Readable'=>'Сведения о решении о назначении временной администрации'
	,'Readable_short_about'=>'о назначении временной администрации'
)
,array
(
	'db_value'=>'h'
	,'api_name'=>'ChangeAdministration'
	,'Readable'=>'Сведения об изменении состава временной администрации'
	,'Readable_short_about'=>'об изменении временной администрации'
)
,array
(
	'db_value'=>'i'
	,'api_name'=>'TerminationAdministration'
	,'Readable'=>'Сведения о прекращении деятельности временной администрации'
	,'Readable_short_about'=>'о расформировании временной администрации'
)
,array
(
	'db_value'=>'j'
	,'api_name'=>'BeginExecutoryProcess'
	,'Readable'=>'Сведения о начале исполнительного производства'
	,'Readable_short_about'=>'о начале исполнительного производства'
)
,array
(
	'db_value'=>'k'
	,'api_name'=>'TransferAssertsForImplementation'
	,'Readable'=>'Сведения о передаче имущества на реализацию'
	,'Readable_short_about'=>'о передаче имущества на реализацию'
)
,array
(
	'db_value'=>'l'
	,'api_name'=>'Annul'
	,'Readable'=>'Сведения об аннулировании ранее опубликованных сообщений'
	,'Readable_short_about'=>'об аннулировании ранее опубликованных сообщений'
)
,array
(
	'db_value'=>'m'
	,'api_name'=>'PropertyInventoryResult'
	,'Readable'=>'Сведения о результатах инвентаризации имущества должника'
	,'Readable_short_about'=>'о результатах инвентаризации'
)
,array
(
	'db_value'=>'n'
	,'api_name'=>'PropertyEvaluationReport'
	,'Readable'=>'Сведения об отчете оценщика, об оценке имущества должника'
	,'Readable_short_about'=>'об оценке'
)
,array
(
	'db_value'=>'o'
	,'api_name'=>'SaleContractResult'
	,'Readable'=>'Сведения о заключении договора купли-продажи'
	,'Readable_short_about'=>'о заключении договора купли-продажи'
)
,array
(
	'db_value'=>'p'
	,'api_name'=>'SaleContractResult2'
	,'Readable'=>'Сведения о заключении договора купли-продажи'
	,'Readable_short_about'=>'о заключении договора купли-продажи'
)
,array
(
	'db_value'=>'q'
	,'api_name'=>'Committee'
	,'Readable'=>'Сведения о проведении комитета кредиторов'
	,'Readable_short_about'=>'о заседании КК'
)
,array
(
	'db_value'=>'r'
	,'api_name'=>'CommitteeResult'
	,'Readable'=>'Сообщение о результатах проведения комитета кредиторов'
	,'TextTag'=>'Other'
	,'Readable_short_about'=>'о результатах заседания КК'
)
,array
(
	'db_value'=>'s'
	,'api_name'=>'SaleOrderPledgedProperty'
	,'Readable'=>'Об определении начальной продажной цены, утверждении порядка и условий проведения торгов по реализации предмета залога, порядка и условий обеспечения сохранности предмета залога'
	,'Readable_short_about'=>'о порядке работы с предметом залога'
)
,array
(
	'db_value'=>'t'
	,'api_name'=>'ReceivingCreditorDemand'
	,'Readable'=>'Сведения о получении требования кредитора'
	,'Readable_short_about'=>'о получении ТК'
)
,array
(
	'db_value'=>'u'
	,'api_name'=>'DemandAnnouncement'
	,'Readable'=>'Извещение о возможности предъявления требований'
	,'TextTag'=>'Other'
	,'Readable_short_about'=>'о возможности предъявления требований'
)
,array
(
	'db_value'=>'v'
	,'api_name'=>'CourtAssertAcceptance'
	,'Readable'=>'Объявление о принятии арбитражным судом заявления'
	,'TextTag'=>'Other'
	,'Readable_short_about'=>'о принятии судом заявления'
)
,array
(
	'db_value'=>'w'
	,'api_name'=>'FinancialStateInformation'
	,'Readable'=>'Информация о финансовом состоянии'
	,'TextTag'=>'Other'
	,'Readable_short_about'=>'о финансовом состоянии'
)
,array
(
	'db_value'=>'x'
	,'api_name'=>'BankPayment'
	,'Readable'=>'Объявление о выплатах Банка России'
	,'TextTag'=>'Other'
	,'Readable_short_about'=>'о выплатах Банка России'
)
,array
(
	'db_value'=>'y'
	,'api_name'=>'AssetsReturning'
	,'Readable'=>'Объявление о возврате ценных бумаг и иного имущества'
	,'TextTag'=>'Other'
	,'Readable_short_about'=>'о возврате ценных бумаг и имущества'
)
,array
(
	'db_value'=>'z'
	,'api_name'=>'CourtAcceptanceStatement'
	,'Readable'=>'Сведения о принятии заявления о признании должника банкротом'
	,'TextTag'=>'Other'
	,'Readable_short_about'=>'о признании банкротом'
)
,array
(
	'db_value'=>'а'
	,'api_name'=>'DeliberateBankruptcy'
	,'Readable'=>'Сообщение о наличии или об отсутствии признаков преднамеренного или фиктивного банкротства'
	,'Readable_short_about'=>'о результатах проверки на фиктивность/предумышленность'
)
,array
(
	'db_value'=>'б'
	,'api_name'=>'IntentionCreditOrg'
	,'Readable'=>'Сообщение о намерении исполнить обязательства кредитной организации'
	,'Readable_short_about'=>'о намерении исполнить обязательства кредитной организации'
)
,array
(
	'db_value'=>'в'
	,'api_name'=>'LiabilitiesCreditOrg'
	,'Readable'=>'Сообщение о признании исполнения заявителем обязательств кредитной организации несостоявшимся'
	,'Readable_short_about'=>'о неисполнении обязательств кредитной организации'
)
,array
(
	'db_value'=>'г'
	,'api_name'=>'PerformanceCreditOrg'
	,'Readable'=>'Сообщение об исполнении обязательств кредитной организации'
	,'Readable_short_about'=>'об исполнении обязательств кредитной организации'
)
,array
(
	'db_value'=>'д'
	,'api_name'=>'BuyingProperty'
	,'Readable'=>'Сообщение о преимущественном праве выкупа имущества'
	,'Readable_short_about'=>'о преимущественном праве выкупа имущества'
)
,array
(
	'db_value'=>'е'
	,'api_name'=>'DeclarationPersonDamages'
	,'Readable'=>'Заявление о привлечении контролирующих должника лиц, а также иных лиц, к ответственности в виде возмещения убытков'
	,'Readable_short_about'=>'о привлечении лиц к возмещению убытков'
)
,array
(
	'db_value'=>'ё'
	,'api_name'=>'ActPersonDamages'
	,'Readable'=>'Судебный акт по результатам рассмотрения заявления о привлечении контролирующих должника лиц, а также иных лиц, к ответственности в виде возмещения убытков'
	,'Readable_short_about'=>'о судебном акте про привлечение к возмещению убытков'
)
,array
(
	'db_value'=>'ж'
	,'api_name'=>'ActReviewPersonDamages'
	,'Readable'=>'Судебный акт по результатам пересмотра рассмотрения заявления о привлечении контролирующих должника лиц, а также иных лиц, к ответственности в виде возмещения убытков'
	,'Readable_short_about'=>'о судебном акте пересмотра заявления о привлечении к возмещению убытков'
)
,array
(
	'db_value'=>'з'
	,'api_name'=>'DealInvalid'
	,'Readable'=>'Заявление о признании сделки должника недействительной'
	,'Readable_short_about'=>'о признании сделки недействительной'
)
,array
(
	'db_value'=>'и'
	,'api_name'=>'ActDealInvalid'
	,'Readable'=>'Судебный акт по результатам рассмотрения заявления об оспаривании сделки должника'
	,'Readable_short_about'=>'о судебном акте об оспаривании сделки'
)
,array
(
	'db_value'=>'й'
	,'api_name'=>'ActDealInvalid2'
	,'Readable'=>'Судебный акт по результатам рассмотрения заявления об оспаривании сделки должника'
	,'Readable_short_about'=>'о судебном акте об оспаривании сделки'
)
,array
(
	'db_value'=>'к'
	,'api_name'=>'ActReviewDealInvalid'
	,'Readable'=>'Судебный акт по результатам пересмотра рассмотрения заявления об оспаривании сделки должника'
	,'Readable_short_about'=>'о судебном акте пересмотра оспаривания сделки'
)
,array
(
	'db_value'=>'л'
	,'api_name'=>'DeclarationPersonSubsidiary'
	,'Readable'=>'Заявление о привлечении контролирующих должника лиц к субсидиарной ответственности'
	,'Readable_short_about'=>'о привлечении к субсидиарной ответственности'
)
,array
(
	'db_value'=>'м'
	,'api_name'=>'ActPersonSubsidiary'
	,'Readable'=>'Судебный акт по результатам рассмотрения заявления о привлечении контролирующих должника лиц к субсидиарной ответственности'
	,'Readable_short_about'=>'о судебном акте о привлечении к субсидиарной ответственности'
)
,array
(
	'db_value'=>'н'
	,'api_name'=>'ActReviewPersonSubsidiary'
	,'Readable'=>'Судебный акт по результатам пересмотра рассмотрения заявления о привлечении контролирующих должника лиц к субсидиарной ответственности'
	,'Readable_short_about'=>'о судебном акте по пересмотру привлечения к субсидиарной ответственности'
)
,array
(
	'db_value'=>'о'
	,'api_name'=>'MeetingWorker'
	,'Readable'=>'Уведомление о проведении собрания работников, бывших работников должника'
	,'Readable_short_about'=>'о собрании работников'
)
,array
(
	'db_value'=>'п'
	,'api_name'=>'MeetingWorkerResult'
	,'Readable'=>'Сведения о решениях, принятых собранием работников, бывших работников должника'
	,'Readable_short_about'=>'о результатах собрания работников'
)
,array
(
	'db_value'=>'р'
	,'api_name'=>'ViewDraftRestructuringPlan'
	,'Readable'=>'Сведения о порядке и месте ознакомления с проектом плана реструктуризаци'
	,'Readable_short_about'=>'об ознакомлении с проектом реструктуризации'
)
,array
(
	'db_value'=>'с'
	,'api_name'=>'ViewExecRestructuringPlan'
	,'Readable'=>'Сведения о порядке и месте ознакомления с отчетом о результатах исполнения плана реструктуризации'
	,'Readable_short_about'=>'об ознакомлении с отчетом по плану реструктуризации'
)
,array
(
	'db_value'=>'т'
	,'api_name'=>'TransferOwnershipRealEstate'
	,'Readable'=>'Сообщение о переходе права собственности на объект незавершенного строительства и прав на земельный участок'
	,'Readable_short_about'=>'о переходе права собственности'
)
,array
(
	'db_value'=>'у'
	,'api_name'=>'CancelAuctionTradeResult'
	,'Readable'=>'Сообщение об отмене сообщения об объявлении торгов или сообщения о результатах торгов'
	,'Readable_short_about'=>'об отмене сообщения о торгах или результатах'
)
,array
(
	'db_value'=>'ф'
	,'api_name'=>'CancelDeliberateBankruptcy'
	,'Readable'=>'Сообщение об отмене сообщения о наличии или об отсутствии признаков преднамеренного или фиктивного банкротства'
	,'Readable_short_about'=>'об отмене сообщения о проверке на фиктивность'
)
,array
(
	'db_value'=>'х'
	,'api_name'=>'ChangeAuction'
	,'Readable'=>'Сообщение об изменении объявления о проведении торгов'
	,'Readable_short_about'=>'об изменении объявления о торгах'
)
,array
(
	'db_value'=>'ц'
	,'api_name'=>'ChangeDeliberateBankruptcy'
	,'Readable'=>'Сообщение об изменении сообщения о наличии или об отсутствии признаков преднамеренного или фиктивного банкротства'
	,'Readable_short_about'=>'об изменении сообщения о проверке на фиктивность'
)
,array
(
	'db_value'=>'ч'
	,'api_name'=>'ReducingSizeShareCapital'
	,'Readable'=>'Сообщение об уменьшении размера уставного капитала банка'
	,'Readable_short_about'=>'об уменьшении уставного капитала'
)
,array
(
	'db_value'=>'ш'
	,'api_name'=>'SelectionPurchaserAssets'
	,'Readable'=>'Сведения о проведении отбора приобретателей имущества (активов) и обязательств кредитной организации'
	,'Readable_short_about'=>'об отборе приобретателей для кредитной организации'
)
,array
(
	'db_value'=>'щ'
	,'api_name'=>'EstimatesCurrentExpenses'
	,'Readable'=>'Сведения о смете текущих расходов кредитной организации'
	,'Readable_short_about'=>'о смете расходов кредитной организации'
)
,array
(
	'db_value'=>'ъ'
	,'api_name'=>'OrderAndTimingCalculations'
	,'Readable'=>'Сведения о порядке и сроках расчетов с кредиторами'
	,'Readable_short_about'=>'о порядке и сроках расчетов с кредиторами'
)
,array
(
	'db_value'=>'ы'
	,'api_name'=>'InformationAboutBankruptcy'
	,'Readable'=>'Информация о ходе конкурсного производства'
	,'Readable_short_about'=>'о ходе КП'
)
,array
(
	'db_value'=>'ь'
	,'api_name'=>'EstimatesAndUnsoldAssets'
	,'Readable'=>'Сведения об исполнении сметы текущих расходов и стоимости нереализованного имущества кредитной организации'
	,'Readable_short_about'=>'об исполнении сметы и стоимости нереализованного для кредитной организации'
)
,array
(
	'db_value'=>'э'
	,'api_name'=>'RemainingAssetsAndRight'
	,'Readable'=>'Объявление о наличии у кредитной организации оставшегося имущества и праве ее учредителей(участников) получить указанное имущество'
	,'Readable_short_about'=>'о наличии у кредитной организации оставшегося имущества и праве его получить'
)
,array
(
	'db_value'=>'ю'
	,'api_name'=>'ImpendingTransferAssets'
	,'Readable'=>'Сообщение о предстоящей передаче приобретателю имущества(активов) и обязательств кредитной организации или их части'
	,'Readable_short_about'=>'о передаче приобретателю имущества и обязательств кредитной организации или их части'
)
,array
(
	'db_value'=>'я'
	,'api_name'=>'TransferAssets'
	,'Readable'=>'Сообщение о передаче приобретателю имущества и обязательств кредитной организации'
	,'Readable_short_about'=>'о передаче приобретателю имущества и обязательств кредитной организации'
)
,array
(
	'db_value'=>'0'
	,'api_name'=>'TransferInsurancePortfolio'
	,'Readable'=>'Уведомление о передаче страхового портфеля страховой организации'
	,'Readable_short_about'=>'о передаче страхового портфеля'
)
,array
(
	'db_value'=>'1'
	,'api_name'=>'BankOpenAccountDebtor'
	,'Readable'=>'Сведения о кредитной организации, в которой открыт специальный банковский счет должника'
	,'Readable_short_about'=>'о том, где открыт специальный банковский счет'
)
,array
(
	'db_value'=>'2'
	,'api_name'=>'ProcedureGrantingIndemnity'
	,'Readable'=>'Предложение о погашении требований кредиторов путем предоставления отступного'
	,'Readable_short_about'=>'о погашении требований предоставлением отступного'
)
,array
(
	'db_value'=>'3'
	,'api_name'=>'RightUnsoldAsset'
	,'Readable'=>'Объявление о наличии непроданного имущества и праве собственника имущества должника – унитарного предприятия, учредителей (участников) должника получить такое имущество'
	,'Readable_short_about'=>'о непроданном и праве собственника получить его'
)
,array
(
	'db_value'=>'4'
	,'api_name'=>'TransferResponsibilitiesFund'
	,'Readable'=>'Решение о передаче обязанности по  выплате пожизненных негосударственных пенсий и средств пенсионных резервов другому негосударственному пенсионному фонду'
	,'Readable_short_about'=>'о передаче обязанности негосударственному ПФ'
)
,array
(
	'db_value'=>'5'
	,'api_name'=>'ExtensionAdministration'
	,'Readable'=>'Продление срока деятельности временной администрации'
	,'Readable_short_about'=>'о продлении временной администрации'
)
,array
(
	'db_value'=>'6'
	,'api_name'=>'MeetingParticipantsBuilding'
	,'Readable'=>'Уведомление о проведении собрания участников строительства'
	,'Readable_short_about'=>'о собрании участников строительства'
)
,array
(
	'db_value'=>'7'
	,'api_name'=>'MeetingPartBuildResult'
	,'Readable'=>'Сообщение о результатах проведения собрания участников строительства'
	,'Readable_short_about'=>'о результатах собрания участников строительства'
)
,array
(
	'db_value'=>'8'
	,'api_name'=>'PartBuildMonetaryClaim'
	,'Readable'=>'Извещение участникам строительства о возможности предъявления денежного требования'
	,'Readable_short_about'=>'о возможности денежного требования'
)
,array
(
	'db_value'=>'9'
	,'api_name'=>'StartSettlement'
	,'Readable'=>'Сообщения о начале расчетов'
	,'Readable_short_about'=>'о начале расчетов'
)
,array
(
	'db_value'=>'A'
	,'api_name'=>'ProcessInventoryDebtor'
	,'Readable'=>'Сведения о ходе инвентаризации имущества должника'
	,'Readable_short_about'=>'о ходе инвентаризации'
)
,array
(
	'db_value'=>'B'
	,'api_name'=>'Rebuttal'
	,'Readable'=>'Опровержение по решению суда опубликованных ранее сведений'
	,'Readable_short_about'=>'об опровержении ранее опубликованного'
)
,array
(
	'db_value'=>'C'
	,'api_name'=>'CreditorChoiceRightSubsidiary'
	,'Readable'=>'Сообщение о праве кредитора выбрать способ распоряжения правом требования о привлечении к субсидиарной ответственности'
	,'Readable_short_about'=>'о выборе способа распоряжения требованием о привлечении'
)
,array
(
	'db_value'=>'D'
	,'api_name'=>'AccessionDeclarationSubsidiary'
	,'Readable'=>'Предложение о присоединении к заявлению о привлечении контролирующих лиц должника к субсидиарной ответственности'
	,'Readable_short_about'=>'о присоединении к заявлению о привлечении к ответственности'
)
,array
(
	'db_value'=>'E'
	,'api_name'=>'DisqualificationArbitrationManager'
	,'Readable'=>'Сообщение о дисквалификации арбитражного управляющего'
	,'Readable_short_about'=>'о дисквалификации АУ'
)
,array
(
	'db_value'=>'F'
	,'api_name'=>'DisqualificationArbitrationManager2'
	,'Readable'=>'Сообщение о дисквалификации арбитражного управляющего (версия 2)'
	,'Readable_short_about'=>'о дисквалификации АУ (версия 2)'
)
,array
(
	'db_value'=>'G'
	,'api_name'=>'ChangeEstimatesCurrentExpenses'
	,'Readable'=>'Сведения о скорректированной смете текущих расходов кредитной организации или иной финансовой организации'
	,'Readable_short_about'=>'о скорректированной смете расходов кредитной организации'
)

,array
(
	'db_value'=>'H'
	,'api_name'=>'UnexistedType'
	,'Readable'=>'Непредусмотренный тип сообщения'
	,'Readable_short_about'=>'о непредусмотренном'
)

,array
(
	'db_value'=>'I'
	,'api_name'=>'ActReviewDealInvalid2'
	,'Readable'=>'Судебный акт по результатам пересмотра рассмотрения заявления об оспаривании сделки должника'
	,'Readable_short_about'=>'о судебном акте про пересмотр оспаривания сделки'
)
,array
(
	'db_value'=>'J'
	,'api_name'=>'ActReviewPersonSubsidiary2'
	,'Readable'=>'Судебный акт по результатам пересмотра рассмотрения заявления о привлечении контролирующих должника лиц к субсидиарной ответственности'
	,'Readable_short_about'=>'о судебном акте про пересмотр привлечения к ответственности'
)
,array
(
	'db_value'=>'K'
	,'api_name'=>'StartOfExtrajudicialBankruptcy'
	,'Readable'=>'Сообщение о возбуждении процедуры внесудебного банкротства гражданина'
	,'Readable_short_about'=>'о внесудебном банкротстве'
)
,array
(
	'db_value'=>'L'
	,'api_name'=>'ReturnOfApplicationOnExtrajudicialBankruptcy'
	,'Readable'=>'Сообщение о возврате гражданину поданного им заявления о признании гражданина банкротом во внесудебном порядке'
	,'Readable_short_about'=>'о возврате заявления во внесудебном'
)
,array
(
	'db_value'=>'M'
	,'api_name'=>'TerminationOfExtrajudicialBankruptcy'
	,'Readable'=>'Сообщение о прекращении процедуры внесудебного банкротства гражданина'
	,'Readable_short_about'=>'о прекращении внесудебного'
)
,array
(
	'db_value'=>'N'
	,'api_name'=>'CompletionOfExtrajudicialBankruptcy'
	,'Readable'=>'Сообщение о завершении процедуры внесудебного банкротства гражданина'
	,'Readable_short_about'=>'о завершении внесудебного'
)
);

function prepare_MessageInfo_MessageType_desciptions_by_api_name($MessageInfo_MessageType_desciptions)
{
	$MessageInfo_MessageType_desciptions_by_api_name= array();
	foreach ($MessageInfo_MessageType_desciptions as $d)
	{
		$api_name= $d['api_name'];
		if (isset($MessageInfo_MessageType_desciptions_by_api_name[$api_name]))
			throw new Exception("duplicate api_name \"$api_name\"!");
		$MessageInfo_MessageType_desciptions_by_api_name[$api_name]= $d;
	}
	return $MessageInfo_MessageType_desciptions_by_api_name;
}

$MessageInfo_MessageType_desciptions_by_api_name= prepare_MessageInfo_MessageType_desciptions_by_api_name($MessageInfo_MessageType_desciptions);

