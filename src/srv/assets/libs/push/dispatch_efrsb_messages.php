<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/job.php';

global $max_message_day_age_to_dispatch;
$max_message_day_age_to_dispatch= 15;
function dispatch_efrsb_messages()
{
	global $max_message_day_age_to_dispatch;
	$total_dispatched= 0;
	$time= job_time();
	$time= date_create_from_format('Y-m-d\TH:i:s',$time);
	date_modify($time, "-1 hour");
	$time= date_format($time,'Y-m-d H:i:s');
	$time= "'$time'";
	
	$txt_query = "
	insert ignore into Pushed_message (id_Message, id_Push_receiver, Time_dispatched)
	select mes.id_Message, pr.id_Push_receiver, $time + interval pr.Timezone hour
	from	   Message mes
	inner join Manager man on man.ArbitrManagerID = mes.ArbitrManagerID
	inner join Push_receiver pr on pr.Category='m' && pr.id_Manager_Contract_MUser = man.id_Manager
	left  join Pushed_message pm on mes.id_Message = pm.id_Message && pm.id_Push_receiver = pr.id_Push_receiver
	where  pr.For_Message_after is not null 
	&& mes.PublishDate > (pr.For_Message_after - interval 1 hour) + interval pr.Timezone hour
	&& mes.PublishDate > ($time - interval $max_message_day_age_to_dispatch day)
	&& pm.Time_dispatched is null
	&& pr.Transport <> 'd'
	&& $time + interval pr.Timezone hour between
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_start hour 
	AND
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday($time) BETWEEN 0 AND 4, true, false))
	"; 
	echo "\r\n################################## QUERY ##################################\r\n";
	echo $txt_query;
	echo "\r\n################################## END QUERY ##################################\r\n\r\n";
	echo job_time()." message notification for Manager ..\r\n";
	$affected_rows_count = execute_query_get_affected_rows($txt_query,array());
	echo job_time()." .. message notification finished with $affected_rows_count affected rows\r\n";
	$total_dispatched+= $affected_rows_count;

	$txt_query = "
	insert ignore into Pushed_message (id_Message, id_Push_receiver, Time_dispatched)
	select mes.id_Message, pr.id_Push_receiver, $time + interval pr.Timezone hour
	from	   Message mes
	inner join Manager man on man.ArbitrManagerID = mes.ArbitrManagerID
	inner join ManagerUser manu on manu.id_Manager = man.id_Manager
	inner join MUser mu on mu.id_MUser = manu.id_MUser
	inner join Push_receiver pr on pr.Category='v' && pr.id_Manager_Contract_MUser = mu.id_MUser
	left  join Pushed_message pm on mes.id_Message = pm.id_Message && pm.id_Push_receiver = pr.id_Push_receiver
	where  pr.For_Message_after is not null 
	&& mes.PublishDate > (pr.For_Message_after - interval 1 hour) + interval pr.Timezone hour
	&& mes.PublishDate > ($time - interval $max_message_day_age_to_dispatch day)
	&& pm.Time_dispatched is null
	&& pr.Transport <> 'd'
	&& $time + interval pr.Timezone hour between
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_start hour 
	AND
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday($time) BETWEEN 0 AND 4, true, false))
	";
	echo "\r\n################################## QUERY ##################################\r\n";
	echo $txt_query;
	echo "\r\n################################## END QUERY ##################################\r\n\r\n";
	echo job_time()." message notification for MUser ..\r\n";
	$affected_rows_count = execute_query_get_affected_rows($txt_query,array());
	echo job_time()." .. message notification finished with $affected_rows_count affected rows\r\n";
	$total_dispatched+= $affected_rows_count;

	$txt_query = "
	insert ignore into Pushed_message (id_Message, id_Push_receiver, Time_dispatched)
	select mes.id_Message, pr.id_Push_receiver, $time + interval pr.Timezone hour
	from	   Message mes
	inner join Manager man on man.ArbitrManagerID = mes.ArbitrManagerID
	inner join Contract c on c.id_Contract = man.id_Contract
	inner join Push_receiver pr on pr.Category='c' && pr.id_Manager_Contract_MUser = c.id_Contract
	left  join Pushed_message pm on mes.id_Message = pm.id_Message && pm.id_Push_receiver = pr.id_Push_receiver
	where  pr.For_Message_after is not null 
	&& mes.PublishDate > (pr.For_Message_after - interval 1 hour) + interval pr.Timezone hour
	&& mes.PublishDate > ($time - interval $max_message_day_age_to_dispatch day)
	&& pm.Time_dispatched is null
	&& pr.Transport <> 'd'
	&& $time + interval pr.Timezone hour between
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_start hour 
	AND
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday($time) BETWEEN 0 AND 4, true, false))
	";
	echo "\r\n################################## QUERY ##################################\r\n";
	echo $txt_query;
	echo "\r\n################################## END QUERY ##################################\r\n\r\n";
	echo job_time()." message notification for Contract ..\r\n";
	$affected_rows_count = execute_query_get_affected_rows($txt_query,array());
	echo job_time()." .. message notification finished with $affected_rows_count affected rows\r\n";
	$total_dispatched+= $affected_rows_count;

	return "отправлено уведомлений об объявлениях с ЕФРСБ: $total_dispatched";
}

/*

to stop deliver:

select id_Push_receiver, For_Message_after from Push_receiver where For_Message_after is not null;

update Push_receiver
set For_Message_after='2021-06-10 15:58:12'
where For_Message_after is not null;

update Push_receiver
set For_Message_after='2021-02-06 00:00:00'
where For_Message_after is not null;

*/
