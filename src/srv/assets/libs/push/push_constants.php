<?php

$Notify2DB = array(
	  'никогда'=> 'a'
	, 'за_1_день'=> 'b'
	, 'за_3_дня'=> 'c'
	, 'за_5_дней'=> 'd'
	, 'за_15_дней'=> 'e'
);

$Directions2DB = array(
	'в_мобильное_приложение_на_телефон'=> 'a'
	,'смс_сообщениями_на_телефон'=> 'b'
	,'на_адрес_электронной_почты'=> 'c'
	,'никуда'=> 'd'
);


function DBint2Time ($time)
{
	if(isset($time))
		return substr(('0'.$time), -2).':00';
}
function Time2DB ($time)
{
	if(isset($time))
	{
		$new_time = explode(':', $time);
		return (int)$new_time[0];
	}
}

function Timezones2DB ($timezone, $reverse)
{
	if ($reverse)
	{
		return $timezone[0] >= 0 ? 'мск+'.$timezone[0]: 'мск'.$timezone[0];
	}
	else
	{
		return (int)mb_substr($timezone,mb_strlen('мск'));
	}
}