<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/job.php';

function delete_old_pushed_efrsb_messages($max_pushed_days_age= 30)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);
	$time_later_than= date_sub($current_date_time, date_interval_create_from_date_string($max_pushed_days_age.' days'));
	$sql_time_later_than= date_format($time_later_than,'Y-m-d H:i:s');

	echo job_time() . " delete from Pushed_message\r\n";
	echo job_time() . "   where Time_pushed < $sql_time_later_than (older than $max_pushed_days_age days) ..\r\n";
	$txt_query= 'delete Pushed_message 
		from Pushed_message 
		where Time_pushed < ?;';
	$affected_rows= execute_query_get_affected_rows($txt_query,array('s',$sql_time_later_than));
	echo job_time() . " .. deleted $affected_rows Pushed_message rows.\r\n";

	return "удалено записей об отправке уведомлений об объявлениях ЕФРСБ\r\n ранее $sql_time_later_than: $affected_rows";
}