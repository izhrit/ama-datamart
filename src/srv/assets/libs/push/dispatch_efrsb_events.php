<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/job.php';

function dispatch_efrsb_events()
{
	$total_dispatched= 0;
	$time= job_time();
	$time= date_create_from_format('Y-m-d\TH:i:s',$time);
	date_modify($time, "-1 hour");
	$time= date_format($time,'Y-m-d H:i:s');
	$time= "'$time'";
	
	$txt_query = "
	insert ignore into Pushed_event (id_Event, id_Push_receiver, Time_dispatched)
	select e.id_Event, pr.id_Push_receiver, $time + interval pr.Timezone hour

	from	   event e
	inner join Manager man on man.ArbitrManagerID = e.ArbitrManagerID
	inner join Push_receiver pr on pr.Category='m' && pr.id_Manager_Contract_MUser = man.id_Manager

	where pr.Transport <> 'd'
	&& $time + interval pr.Timezone hour between
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_start hour 
	AND
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday($time) BETWEEN 0 AND 4, true, false))
	&& DATE($time + interval pr.Timezone hour) = CASE
				WHEN e.MessageType = 'a' THEN DATE(getNotifyDate(pr.For_CourtDecision, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'о' THEN DATE(getNotifyDate(pr.For_MeetingWorker, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'b' THEN DATE(getNotifyDate(pr.For_Auction, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = '6' THEN DATE(getNotifyDate(pr.For_MeetingPB, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'q' THEN DATE(getNotifyDate(pr.For_Committee, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'c' THEN DATE(getNotifyDate(pr.For_Meeting, e.EventTime, pr.Check_day_off))
				ELSE '2000-01-01'
			END
	&& e.EventTime > $time + interval pr.Timezone hour
	&& e.isActive = 1;
	";
	echo "\r\n################################## QUERY ##################################\r\n";
	echo $txt_query;
	echo "\r\n################################## END QUERY ##################################\r\n\r\n";
	echo job_time()." notification for Manager ..\r\n";
	$affected_rows_count = execute_query_get_affected_rows($txt_query,array());
	echo job_time()." .. notification finished with $affected_rows_count affected rows\r\n";
	$total_dispatched+= $affected_rows_count;

	$txt_query = "
	insert ignore into Pushed_event (id_Event, id_Push_receiver, Time_dispatched) 
	select e.id_Event, pr.id_Push_receiver, $time + interval pr.Timezone hour

	from	   event e
	inner join Manager man on man.ArbitrManagerID = e.ArbitrManagerID
	inner join ManagerUser manu on manu.id_Manager = man.id_Manager
	inner join MUser mu on mu.id_MUser = manu.id_MUser
	inner join Push_receiver pr on pr.Category='v' && pr.id_Manager_Contract_MUser = mu.id_MUser

	where pr.Transport <> 'd'
	&& $time + interval pr.Timezone hour between
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_start hour 
	AND
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday($time) BETWEEN 0 AND 4, true, false))
	&& DATE($time + interval pr.Timezone hour) = CASE
				WHEN e.MessageType = 'a' THEN DATE(getNotifyDate(pr.For_CourtDecision, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'о' THEN DATE(getNotifyDate(pr.For_MeetingWorker, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'b' THEN DATE(getNotifyDate(pr.For_Auction, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = '6' THEN DATE(getNotifyDate(pr.For_MeetingPB, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'q' THEN DATE(getNotifyDate(pr.For_Committee, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'c' THEN DATE(getNotifyDate(pr.For_Meeting, e.EventTime, pr.Check_day_off))
				ELSE '2000-01-01'
			END
	&& e.EventTime > $time + interval pr.Timezone hour
	&& e.isActive = 1;
	";
	echo "\r\n################################## QUERY ##################################\r\n";
	echo $txt_query;
	echo "\r\n################################## END QUERY ##################################\r\n\r\n";
	echo job_time()." notification for MUser ..\r\n";
	$affected_rows_count = execute_query_get_affected_rows($txt_query,array());
	echo job_time()." .. notification finished with $affected_rows_count affected rows\r\n";
	$total_dispatched+= $affected_rows_count;

	$txt_query = "
	insert ignore into Pushed_event (id_Event, id_Push_receiver, Time_dispatched) 
	select e.id_Event, pr.id_Push_receiver, $time + interval pr.Timezone hour

	from	   event e
	inner join Manager man on man.ArbitrManagerID = e.ArbitrManagerID
	inner join Contract c on c.id_Contract = man.id_Contract
	inner join Push_receiver pr on pr.Category='c' && pr.id_Manager_Contract_MUser = c.id_Contract

	where pr.Transport <> 'd'
	&& $time between
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_start hour 
	AND
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday($time) BETWEEN 0 AND 4, true, false))
	&& DATE($time + interval pr.Timezone hour) = CASE
				WHEN e.MessageType = 'a' THEN DATE(getNotifyDate(pr.For_CourtDecision, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'о' THEN DATE(getNotifyDate(pr.For_MeetingWorker, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'b' THEN DATE(getNotifyDate(pr.For_Auction, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = '6' THEN DATE(getNotifyDate(pr.For_MeetingPB, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'q' THEN DATE(getNotifyDate(pr.For_Committee, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'c' THEN DATE(getNotifyDate(pr.For_Meeting, e.EventTime, pr.Check_day_off))
				ELSE '2000-01-01'
			END
	&& e.EventTime > $time + interval pr.Timezone hour
	&& e.isActive = 1;
	";
	echo "\r\n################################## QUERY ##################################\r\n";
	echo $txt_query;
	echo "\r\n################################## END QUERY ##################################\r\n\r\n";
	echo job_time()." notification for Contract ..\r\n";
	$affected_rows_count = execute_query_get_affected_rows($txt_query,array());
	echo job_time()." .. notification finished with $affected_rows_count affected rows\r\n";
	$total_dispatched+= $affected_rows_count;

	return "отправлено напоминаний о событиях с ЕФРСБ: $total_dispatched";
}