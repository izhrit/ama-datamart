<?php

class FCM_exception extends Exception
{
	public $msg;
	public $url;
	public $md5_auth_key;

	public $POSTFIELDS;

	public $httpcode;
	public $responce= null;

	public function __construct($msg,$url,$auth_key,$POSTFIELDS, $httpcode, $responce= null)
	{
		$this->msg= $msg;
		$this->url= $url;
		$this->md5_auth_key= md5($auth_key);

		$this->POSTFIELDS= $POSTFIELDS;

		$this->httpcode= $httpcode;
		$this->responce= $responce;

		$message= "$msg, httpcode=$httpcode,
url=$url
md5(Authorization:key)={$this->md5_auth_key}
POSTFIELDS=$POSTFIELDS
httpcode=$httpcode
responce=$responce";

		parent::__construct($msg);
	}
}

function send_notification_to_firebase($app_id, $push_notification, $push_data)
{
	$url = 'https://fcm.googleapis.com/fcm/send';

	$android= array('priority'=>'high','notification'=>array('notification_priority'=>'PRIORITY_MAX','channel_id'=>'com.RITLLC.AMAMob.DefaultChanel'));
	$fields = array('to' => $app_id, 'notification' => $push_notification, 'android'=>$android, 'data' => $push_data);

	$auth_key= 'AAAA5l7EB4A:APA91bGIEHuKlS_OQUKWaALZowXKA6SBNeRBeZWHfuF8FmT5jkvDG0I8jtdlwnygFpQmSyq_6r5zmhOcS9wXhtDPlofxdol3NMitKHRfue0uPLM564E2iK0azf7jenY78bAZdujycDIg';
	// md5('AAAA5l7EB4A:APA91bGIEHuKlS_OQUKWaALZowXKA6SBNeRBeZWHfuF8FmT5jkvDG0I8jtdlwnygFpQmSyq_6r5zmhOcS9wXhtDPlofxdol3NMitKHRfue0uPLM564E2iK0azf7jenY78bAZdujycDIg')= 
	//  218f011b2f22ce22e6e41e9589439357
	$headers= array(
		"Authorization:key = $auth_key",
		'Content-Type: application/json'
	);

	$POSTFIELDS= json_encode($fields);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $POSTFIELDS);
	$responce_txt= curl_exec($ch);
	$httpcode= curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

	if (FALSE === $responce_txt || 200!=$httpcode)
		throw new FCM_exception("got bad responce on mobile app notification",$url,$auth_key,$POSTFIELDS,$httpcode,$responce_txt);

	$responce= json_decode($responce_txt);
	if (null == $responce)
		throw new FCM_exception("can not parse responce on mobile app notification",$url,$auth_key,$POSTFIELDS,$httpcode,$responce_txt);

	if (!$responce->success)
		throw new FCM_exception("failure for mobile app notification",$url,$auth_key,$POSTFIELDS,$httpcode,$responce_txt);

	return $responce;
}

