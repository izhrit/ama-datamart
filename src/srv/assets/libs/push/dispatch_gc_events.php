<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/job.php';

function dispatch_gc_events()
{
	$total_dispatched= 0;
	$time= job_time();
	$time= date_create_from_format('Y-m-d\TH:i:s',$time);
	date_modify($time, "-1 hour");
	$time= date_format($time,'Y-m-d H:i:s');
	$time= "'$time'";
	
	$txt_query = "
	insert ignore into GCPushedEvent (id_GCEvent, Time_dispatched)
	select e.id_GCEvent, $time + interval pr.Timezone hour

	from	   GCEvent e
	inner join GoogleCalendar gc on gc.id_GoogleCalendar = e.id_GoogleCalendar
	inner join Push_receiver pr on pr.Category='m' && pr.id_Manager_Contract_MUser = gc.id_Manager

	where pr.Transport <> 'd'
	&& $time + interval pr.Timezone hour between
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_start hour 
	AND
	CONCAT(date($time + interval pr.Timezone hour),\" 00:00:00\") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday($time) BETWEEN 0 AND 4, true, false))
	&& e.EventType IN ('a', 'о', 'b', '6', 'q', 'c')
	&& DATE($time + interval pr.Timezone hour) = CASE
				WHEN e.EventType = 'a' THEN DATE(getNotifyDate(pr.For_CourtDecision, e.EventTime, pr.Check_day_off))
				WHEN e.EventType = 'о' THEN DATE(getNotifyDate(pr.For_MeetingWorker, e.EventTime, pr.Check_day_off))
				WHEN e.EventType = 'b' THEN DATE(getNotifyDate(pr.For_Auction, e.EventTime, pr.Check_day_off))
				WHEN e.EventType = '6' THEN DATE(getNotifyDate(pr.For_MeetingPB, e.EventTime, pr.Check_day_off))
				WHEN e.EventType = 'q' THEN DATE(getNotifyDate(pr.For_Committee, e.EventTime, pr.Check_day_off))
				WHEN e.EventType = 'c' THEN DATE(getNotifyDate(pr.For_Meeting, e.EventTime, pr.Check_day_off))
				ELSE '2000-01-01'
			END
	&& e.EventTime > $time + interval pr.Timezone hour;
	";
	echo "\r\n################################## QUERY ##################################\r\n";
	echo $txt_query;
	echo "\r\n################################## END QUERY ##################################\r\n\r\n";
	echo job_time()." notification for Manager ..\r\n";
	$affected_rows_count = execute_query_get_affected_rows($txt_query,array());
	echo job_time()." .. notification finished with $affected_rows_count affected rows\r\n";
	$total_dispatched+= $affected_rows_count;

	return "отправлено напоминаний о событиях с Google каледаря : $total_dispatched";
}