<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/job.php';
require_once '../assets/actions/backend/alib_emails.php';

require_once '../assets/libs/push/deliver.php';

function deliver_gc_event_by_email($row)
{

	$body = json_decode($row->Body);

	$letter= (object)array('subject'=>$body->summary);

	$Пользователь = Push_receiver_name_for_email($row);
	$Описание= $body->description;
	$Ссылка= $body->htmlLink;

	global $base_apps_url;
	$СсылкаНаОтписку= $base_apps_url.'unsubscribe.php?email='.$row->Destination.'&guid='.$row->ReceiverGUID;

	ob_start();
	include "../assets/libs/push/letters/push_gc_letter.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();
	
	PostLetter($letter, $row->Destination, null, 'уведомление о событии из google календаря', $row->id_GCEvent);
}

function deliver_gc_event_to_ama_mobile_app($msg_row_fields)
{
	$body = json_decode($msg_row_fields->Body);

	$title = str_replace("\r\n", "", $body->summary);
	$description= str_replace("\r\n", "", $body->description);

	$push_notification= array("title" => $title,"body" => $description);
	$push_data= array("category" => 'GC',"url" => $body->htmlLink);
	$token = substr($msg_row_fields->Destination, 1);
	send_notification_to_firebase($token, $push_notification, $push_data);
}

function deliver_gc_events($static_date_time= null)
{
	$notificator= new Push_notificator();
	$notificator->use_for_transport('to mobile application',function ($row,$connection,$txt) { deliver_gc_event_to_ama_mobile_app($row); });
	$notificator->use_for_transport('by email',function ($row,$connection,$txt) { deliver_gc_event_by_email($row); });
	$notificator->notify
	(
		/*$get_rows_to_notify_for=*/function()
		{
			$txt_query  = "SELECT
				pe.Time_pushed
				,e.id_GCEvent, e.EventTime, e.EventSource
				,pr.Transport, pr.Destination, pr.id_Push_receiver, pr.Category, pr.id_Manager_Contract_MUser, pr.ReceiverGUID
				,uncompress(e.Body) Body, e.EventType
				,m.lastName, m.firstName, m.middleName
			FROM GCPushedEvent pe
			inner join GCEvent e on e.id_GCEvent = pe.id_GCEvent
			inner join GoogleCalendar gc on gc.id_GoogleCalendar = e.id_GoogleCalendar
			inner join Manager m on m.id_Manager = gc.id_Manager
			inner join Push_receiver pr on pr.Category='m' && pr.id_Manager_Contract_MUser = m.id_Manager
			where pe.Time_pushed IS NULL;";

			echo job_time() . " query from GCPushedEvents to send (where Time_pushed IS NULL)..\r\n";
			$rows= execute_query($txt_query, array());
			$counted_rows = count($rows);
			echo job_time() . " got $counted_rows rows from GCPushedEvent to send\r\n";
			return $rows;
		},
		/*$set_notify_status_for_row=*/function($row,$error_txt,$sent_txt= null,$response_txt= null)
		{
			$sql_time_now= date_format(date_create_from_format('Y-m-d\TH:i:s',job_time()),'Y-m-d H:i:s');
			$txt_query= "update GCPushedEvent set Time_pushed=?, ErrorText=? where id_GCEvent = ? && id_GCEvent = ?";
			execute_query_no_result($txt_query,array('ssss',$sql_time_now,$error_txt,$row->id_GCEvent, $row->id_GCEvent));
		}
	);
	$result_statistic_text= $notificator->get_statistic_text();
	echo job_time() . " sent notifications:\r\n" . $result_statistic_text;
	return "отправлено:\r\n".$result_statistic_text;
}
