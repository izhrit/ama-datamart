<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/job.php';

global $Push_transports;
$Push_transports= array(
	 'a'=>'to mobile application'
	,'b'=>'by sms'
	,'c'=>'by email'
);

function Push_receiver_name_for_email($row)
{
	switch ($row->Category)
	{
		case 'c': return 'клиент по договору №'.$row->ContractNumber;
		case 'm': return $row->lastName . ' ' . $row->firstName . ' ' . $row->middleName;
		case 'v': return $row->UserName;
		default:
			throw new Exception("can not prepare push receiver name for unknown category \"{$row->Category}\"");
	}
}

function message_url($guid)
{
	return "https://old.bankrot.fedresurs.ru/MessageWindow.aspx?ID=$guid";
}

global $Push_transports_by_readable_names;
$Push_transports_by_readable_names= array();
foreach ($Push_transports as $transport => $transport_readably_name)
	$Push_transports_by_readable_names[$transport_readably_name]= $transport;

class Push_notificator
{
	private $transport_notificators= array();

	public function use_for_transport($transport_readable_name,$transport_notificator_to_use)
	{
		global $Push_transports_by_readable_names;
		$transport= $Push_transports_by_readable_names[$transport_readable_name];
		$this->transport_notificators[$transport]= $transport_notificator_to_use;
	}

	function notify_for_row($row_to_notify_for,$connection)
	{
		$txt= (object)array('error'=>null,'sent'=>null,'response'=>null);
		try
		{
			$transport= $row_to_notify_for->Transport;
			if (!isset($this->transport_notificators[$transport]))
			{
				$txt->error= 'unexpected transport!';
			}
			else
			{
				$transort_notificator= $this->transport_notificators[$transport];
				$transort_notificator($row_to_notify_for,$connection,$txt);
				$this->increment_transport_using_counter($transport);
			}
		}
		catch (Exception $ex)
		{
			$ex_class= get_class($ex);
			$ex_Message= $ex->getMessage();
			$txt->error= "\r\nexception occurred: $ex_class - $ex_Message";
		}
		if (null!=$txt->error)
		{
			echo job_time() . " got problem \"{$txt->error}\"\r\n with notification for row:\r\n";
			$msg_to_echo= print_r($row_to_notify_for,true);
			$msg_to_echo= str_replace("\r\n","\n",$msg_to_echo);
			$msg_to_echo= str_replace("\n","\r\n",$msg_to_echo);
			echo $msg_to_echo;
		}
		return $txt;
	}

	public function get_statistic_text()
	{
		$result_text= '';
		global $Push_transports;
		foreach ($Push_transports as $transport => $transport_readably_name)
		{
			if (isset($this->transport_counters[$transport]))
			{
				$count= $this->transport_counters[$transport];
				$result_text.= "  $count $transport_readably_name\r\n";
			}
		}
		if (''==$result_text)
			$result_text= "  0\r\n";
		return $result_text;
	}

	public function notify($get_rows_to_notify_for,$set_notify_status_for_row)
	{
		$this->init_transport_using_counters();

		$rows_to_notify_for= $get_rows_to_notify_for();
		foreach ($rows_to_notify_for as $row_to_notify_for)
		{
			$txt= $this->notify_for_row($row_to_notify_for,/*$connection=*/null);
			$set_notify_status_for_row($row_to_notify_for,$txt->error,$txt->sent,$txt->response);
		}
	}

	function init_transport_using_counters()
	{
		$this->transport_counters= array();
	}

	function increment_transport_using_counter($transport)
	{
		if (!isset($this->transport_counters[$transport]))
		{
			$this->transport_counters[$transport]= 1;
		}
		else
		{
			$this->transport_counters[$transport]++;
		}
	}
}
