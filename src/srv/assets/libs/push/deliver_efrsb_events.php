<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/job.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/libs/events_news/prepare_event.php';
require_once '../assets/libs/events_news/prepare_event_text.php';

require_once '../assets/libs/push/deliver.php';

function deliver_efrsb_event_by_email($row,$txt)
{
	$event0= prepare_event($row);
	$event= (object)array();
	prepare_event_email_text($event0,$event);

	$letter= (object)array('subject'=>$event->title);

	$Пользователь = Push_receiver_name_for_email($row);
	$Описание= $event->description;
	$Ссылка= 'https://old.bankrot.fedresurs.ru/MessageWindow.aspx?ID='.$event0->guid;
	$НомерСообщения= $row->Number;
	global $base_apps_url;
	$СсылкаНаОтписку= $base_apps_url.'unsubscribe.php?email='.$row->Destination.'&guid='.$row->ReceiverGUID;

	ob_start();
	include "../assets/libs/push/letters/push_letter.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();

	$txt->sent= nice_json_encode($letter);
	$result= PostLetter($letter, $row->Destination, null, 'уведомление о событии', $row->id_Event);
	unset($result->TimeDispatch);
	$txt->response= nice_json_encode($result);
}

function deliver_efrsb_event_to_ama_mobile_app($msg_row_fields,$txt)
{
	$event0= prepare_event($msg_row_fields);
	$event= (object)array();

	prepare_event_app_text($event0,$event);

	$title = str_replace("\r\n", "", $event->title);
	$description= str_replace("\r\n", "", $event->description);

	$push_notification= array("title" => $title,"body" => $description);
	$push_data= array("category" => "efrsb","url" => message_url($msg_row_fields->MessageGUID));
	$token = substr($msg_row_fields->Destination, 1);
	$txt->sent= nice_json_encode(array('notification'=>$push_notification,'data'=>$push_data));
	try
	{
		$response= send_notification_to_firebase($token, $push_notification, $push_data);
		$txt->response= nice_json_encode($response);
	}
	catch (FCM_exception $ex)
	{
		$txt->response= $ex->responce;
		throw $ex;
	}
}

function deliver_efrsb_events($static_date_time= null)
{
	$notificator= new Push_notificator();
	$notificator->use_for_transport('to mobile application',function ($row,$connection,$txt) { deliver_efrsb_event_to_ama_mobile_app($row,$txt); });
	$notificator->use_for_transport('by email',function ($row,$connection,$txt) { deliver_efrsb_event_by_email($row,$txt); });
	$notificator->notify
	(
		/*$get_rows_to_notify_for=*/function()
		{
			$sql_time_from= date_format(date_sub(date_create_from_format('Y-m-d\TH:i:s',job_time()),date_interval_create_from_date_string("1 day")),'Y-m-d H:i:s');
			$txt_query  = "SELECT
				pe.Time_pushed
				,e.id_Event, e.EventTime
				,pr.Transport, pr.Destination, pr.id_Push_receiver, pr.Category, pr.id_Manager_Contract_MUser, pr.ReceiverGUID
				,mes.efrsb_id, uncompress(mes.Body) Body, mes.MessageInfo_MessageType, mes.MessageGUID, mes.Number
				,c.ContractNumber
				,m.lastName, m.firstName, m.middleName
				,mu.UserName
			FROM Pushed_event pe
			inner join Push_receiver pr on pr.id_Push_receiver = pe.id_Push_receiver
			inner join event e on e.id_Event = pe.id_Event
			inner join Message mes on mes.efrsb_id = e.efrsb_id
			left  join Contract c on pr.Category = 'c' AND c.id_Contract = pr.id_Manager_Contract_MUser
			left  join Manager m on pr.Category = 'm' AND m.id_Manager = pr.id_Manager_Contract_MUser
			left  join MUser mu on pr.Category = 'v' AND mu.id_MUser = pr.id_Manager_Contract_MUser
			where pe.Time_pushed IS NULL && pe.Time_dispatched > '$sql_time_from' ;";

			echo job_time() . " query from Pushed_events to send (where Time_pushed IS NULL && pe.Time_dispatched > '$sql_time_from')..\r\n";
			$rows= execute_query($txt_query, array());
			$counted_rows = count($rows);
			echo job_time() . " got $counted_rows rows from Pushed_event to send\r\n";
			return $rows;
		},
		/*$set_notify_status_for_row=*/function($row,$error_txt,$sent_txt= null,$response_txt= null)
		{
			$sql_time_now= date_format(date_create_from_format('Y-m-d\TH:i:s',job_time()),'Y-m-d H:i:s');
			$txt_query= "update Pushed_event set Time_pushed=?, ErrorText=?, SentText=?, ResponseText=? where id_Event = ? && id_Push_receiver = ?";
			execute_query_no_result($txt_query,array('ssssss',$sql_time_now,$error_txt,$sent_txt,$response_txt,$row->id_Event, $row->id_Push_receiver));
		}
	);
	$result_statistic_text= $notificator->get_statistic_text();
	echo job_time() . " sent notifications:\r\n" . $result_statistic_text;
	return "отправлено уведомлений о событиях:\r\n".$result_statistic_text;
}
