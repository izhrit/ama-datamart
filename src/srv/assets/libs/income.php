<?php

require_once '../../assets/helpers/db.php';
require_once '../../assets/helpers/validate.php';
require_once '../../assets/libs/auth/check.php';

function unzip($fileData,$main_entry_name)
{
	$res= null;

	$tf= tmpfile();
	try
	{
		fwrite($tf,$fileData);
		fflush($tf);

		$meta_data= stream_get_meta_data($tf);
		$filepath= $meta_data['uri'];

		$zip = zip_open($filepath);
		if (!is_resource($zip))
		{
			$res= 'не удалось распаковать zip архив!';
		}
		else
		{
			$res= "не удалось найти $main_entry_name в zip архиве!";
			while ($zip_entry = zip_read($zip))
			{
				$zip_entry_name= zip_entry_name($zip_entry);
				if ($zip_entry_name==$main_entry_name)
					$res= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
			}
			zip_close($zip);
		}
		fclose($tf); // Этот файл автоматически удаляется после закрытия
	}
	catch (Exception $ex)
	{
		fclose($tf); // Этот файл автоматически удаляется после закрытия
		throw $ex;
	}

	return $res;
}

function get_income_xml_file($id_MData,$main_entry_name)
{
	$auth_info= CheckAuthViewerOrManager();

	$txt_query= 'select fileData from MData
		inner join ManagerUser on MData.id_ManagerUser=ManagerUser.id_ManagerUser
		where id_MData=?';
	$params= array('ss',$id_MData);
	if ('manager'==$auth_info->category)
	{
		$txt_query.= ' && id_Manager=?';
		$params[]= $auth_info->id_Manager;
	}
	else
	{
		$txt_query.= ' && id_MUser=?';
		$params[]= $auth_info->id_MUser;
	}
	$rows= execute_query($txt_query,$params);

	$rows_count= count($rows);
	if (1!=$rows_count)
		exit_not_found("found $rows_count rows!");

	return unzip($rows[0]->fileData,$main_entry_name);
}

function prepare_xml_for_pre($xml)
{
	$xml= str_replace('&','&amp;',$xml);
	$xml= str_replace('<','&lt;',$xml);
	$xml= str_replace('>','&gt;',$xml);
	return $xml;
}

function get_debtor_by_idMData($id_MData){
	return execute_query("SELECT
							DB.Name
						FROM
							MData DT
						INNER JOIN Debtor DB ON DB.id_Debtor = DT.id_Debtor
						WHERE DT.id_MData = ?",
						array('i', $id_MData));
}

