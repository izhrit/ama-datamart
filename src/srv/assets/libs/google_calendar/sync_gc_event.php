<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/job.php';
require_once '../assets/helpers/google_calendar_api.php';

global $capi, $inserted_total, $EventKindGC2DB;

$EventKindGC2DB = array(
	 'СК'	=> 'c' // Meeting Сведения о собрании кредиторов
	,'КК'	=> 'q' // Committee Сведения о проведении комитета кредиторов
	,'СР'	=> 'о' // MeetingWorker о собрании работников
	,'СД'	=> '6' // MeetingWorker о собрании участников строительства
	,'СЗ '	=> 'a' // ArbitralDecree судебное заседание
	,'Т '	=> 'b' // Auction судебное заседание
	,'П'	=> 'п' // Прочие (что-то, что пользователь ввёл руками в пау)
	,'М'	=> 'м' // Мероприятие ПАУ - например крайняя дата какого то мероприятия
	,'t'	=> 't' // Тестовое событие
);

$capi = new GoogleCalendarApi();
$inserted_total = 0;

function sync_event_items($events, $id_GoogleCalendar)
{
	global $inserted_total, $EventKindGC2DB;

	$res= -1;
	$inserted = 0;
	foreach ($events as $e)
	{
		
		$dateStart = isset($e['start']['dateTime']) ? $e['start']['dateTime'] : $e['start']['date'];
		$dateEnd = isset($e['end']['dateTime']) ? $e['end']['dateTime'] : $e['end']['date'];
		
		if(strpos($dateStart, '+')){
			$dateStart = stristr($dateStart, '+', true);
		}
		if(strpos($dateEnd, '+')){
			$dateEnd = stristr($dateEnd, '+', true);
		}
		
		$affected_rows = execute_query_get_affected_rows('insert into GCEvent set id_GoogleCalendar=?, EventTime=?, EventTimeEnd=?, EventType=?, EventSource=?, Body=compress(?)'
			,array('isssss',$id_GoogleCalendar, $dateStart, $dateEnd, $EventKindGC2DB[$e['extendedProperties']['private']['event_kind']], $e['extendedProperties']['private']['event_source'], nice_json_encode($e)));

		$inserted += $affected_rows;
	}
	echo job_time() . " inserted $inserted events\r\n";

	$inserted_total += $inserted;
}

function check_event_items($events, $time_later_than)
{
	global $EventKindGC2DB;
	$new_events_count = count($events);

	echo job_time() . " got $new_events_count events for check\r\n";
	$checked = array();
	$cancelled = 0;

	foreach ($events as $e)
	{
		if(isset($e['status']) && $e['status'] == 'cancelled') {
			$cancelled++;
		}

		if(isset($e['extendedProperties']) &&  ( (isset($e['start']['dateTime']) && $e['start']['dateTime'] > $time_later_than) || (isset($e['start']['date']) && $e['start']['date'] > $time_later_than)) && 
		$e['extendedProperties']['private']['createdby'] == 'ama' && !empty($e['extendedProperties']['private']['event_kind']) && !empty($e['extendedProperties']['private']['event_kind'])) {
			$checked[] = $e;
		}
	}
	echo $cancelled > 0 ? job_time() . " cancelled $cancelled events\r\n" : '';

	$checked_count = count($checked);
	echo job_time() . " passed the check $checked_count events\r\n";
	return $checked;
}

function get_events_without_token($row, $new_access_token, $time_later_than_txt)
{
	global $capi;

	echo job_time() . " .. sync without nextSyncToken.\r\n";
	$data = $capi->GetCalendarEvents('primary', $new_access_token['access_token'], $time_later_than_txt);

	$count_events = count($data['items']);

	echo job_time() . " .. got $count_events events.\r\n";
				
	if(!empty($data['items'])){
		/*
		 Дополнительно проверяем события
		*/
		$events = check_event_items($data['items'], $time_later_than_txt);

		if(count($events) > 0) {
			sync_event_items($events, $row->id_GoogleCalendar);
		}else {
			echo job_time() . " there is no suit event.\r\n";
		}

		// Сохраняем nextSyncToken пользователя для следующих проверок изменений календаря 
		$nextSyncToken = $data['nextSyncToken'];
		execute_query_no_result('update GoogleCalendar set nextSyncToken=? where id_GoogleCalendar=?',array('si',$nextSyncToken, $row->id_GoogleCalendar));
	}
}

function get_events_via_token($row, $new_access_token, $time_later_than_txt)
{
	global $capi;

	echo job_time() . " .. sync via nextSyncToken.\r\n";

	$data = $capi->GetCalendarEvents('primary', $new_access_token['access_token'], null, $row->nextSyncToken);

	if(isset($data['error']) && $data['error']['code'] == 410) {
		/*
		Данная ошибка может возникнуть в случае если nextSyncToken устарел https://developers.google.com/calendar/api/v3/reference/events/list#:~:text=If%20the%20syncToken%20expires%2C%20the%20server%20will%20respond%20with%20a%20410%20GONE
		В таком случае необходимо получить события заново
		*/
		echo job_time() . " .. nextSyncToken is deprecated.\r\n";

		execute_query_no_result('update GoogleCalendar set nextSyncToken=NULL where id_GoogleCalendar=?',array('i', $row->id_GoogleCalendar));
		execute_query_no_result('delete from GCEvent where id_GoogleCalendar=?',array('i', $row->id_GoogleCalendar));
		echo job_time() . " .. trying to get events again.\r\n";
		get_events_without_token($row, $new_access_token, $time_later_than_txt);
		exit();
	}

	if($row->nextSyncToken != $data['nextSyncToken']) {
		/*
		 Токен изменился, что означает что в календаре появились изменения
		 Проверяем полученные события т.к фильтрации событий как в случае без использования токена не произошло
		*/
		$events = check_event_items($data['items'], $time_later_than_txt);

		if(count($events) > 0) {
			sync_event_items($events, $row->id_GoogleCalendar);
		}else {
			echo job_time() . " there is no suit event.\r\n";
		}
		//Перезаписываем nextSyncToken
		$nextSyncToken = $data['nextSyncToken'];
		execute_query_no_result('update GoogleCalendar set nextSyncToken=? where id_GoogleCalendar=?',array('si',$nextSyncToken, $row->id_GoogleCalendar));
	}else {
		echo job_time() . " not found new events.\r\n";
	}
}

function sync_events($max_event_days_age= 1)
{
	global $capi, $google_calendar_options, $google_calendar_secret, $inserted_total;

	$current_datetime_txt= job_time(true);
	$current_date_time= date_create_from_format('Y-m-d\TH:i:sP',$current_datetime_txt);
	$time_later_than= date_sub($current_date_time, date_interval_create_from_date_string($max_event_days_age.' days'));
	$time_later_than_txt = date_format($time_later_than,'Y-m-d\TH:i:sP');

	// logged users
	$rows= execute_query('SELECT id_GoogleCalendar, refresh_token, nextSyncToken from GoogleCalendar;',array());

	foreach ($rows as $row)
	{
		echo "\r\n################################## GET EVENTS FOR №{$row->id_GoogleCalendar} id_GoogleCalendar ##################################\r\n";
		try {
			// Обновляем и возвращаем токен доступа т.к. срок жизни токена 1 час
			$new_access_token = $capi->RefreshAccessToken($google_calendar_options->CLIENT_ID, $google_calendar_secret, $row->refresh_token);
			
			if(!isset($row->nextSyncToken)) { // Если nextSyncToken отстутствует, отправляем запрос в api GC с фильтром TimeMin и privateExtendedProperty createdby=ama
				get_events_without_token($row, $new_access_token, $time_later_than_txt);
			}else { // Иначе отправляем запрос в api GC с параметром nextSyncToken для проверки изменений/добавлений
				get_events_via_token($row, $new_access_token, $time_later_than_txt);
			}
			execute_query_no_result('UPDATE GoogleCalendar set lastUpload= ? - interval 1 hour where id_GoogleCalendar=?;'// - interval 1 hour чтобы сместить время к московскому
			,array('si', job_time(), $row->id_GoogleCalendar)); 
		}
		catch (Exception $ex)
		{
			throw $ex;
		}
	}

	
	return "Добавлено $inserted_total событий из google календаря.";
}

function delete_old_gc_events($max_event_days_age= 40)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);
	$time_later_than= date_sub($current_date_time, date_interval_create_from_date_string($max_event_days_age.' days'));
	$sql_time_later_than= date_format($time_later_than,'Y-m-d H:i:s');

	echo job_time() . " delete from event where EventTime < $sql_time_later_than (older than $max_event_days_age days) ..\r\n";
	$affected_rows= execute_query_get_affected_rows('delete e 
		from GCEvent ge
		left join GCPushedEvent pe on pe.id_GCEvent=ge.id_GCEvent
		where ge.EventTime < ? and pe.Time_dispatched is null;',array('s',$sql_time_later_than));
	echo job_time() . " .. deleted $affected_rows event rows.\r\n";

	return "удалено событий из google календаря ранее $sql_time_later_than: $affected_rows";
}
