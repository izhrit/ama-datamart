<?php

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthAdmin();

$rows_site= execute_query('select id_JobSite,          Name, Description, SyncTime from JobSite;',array());
$rows_job=  execute_query('select id_JobSite, id_Job,  Name, Description, Status, MaxAgeMinutes from Job order by MaxAgeMinutes desc;',array());
$rows_part= execute_query('select id_JobPart, id_Job,  Name, Description, Status from JobPart where 0<>Enabled;',array());

$rows_log= execute_query("select jl.id_Job, jl.Started, jl.Finished, jl.Status, jl.id_JobLog 
from JobLog jl
inner join Job j on jl.id_Job=j.id_Job 
where jl.id_JobLog= (select jl2.id_JobLog from JobLog jl2 where jl2.id_Job=j.id_Job order by jl2.Started desc limit 1);"
,array());

$rows_part_log= execute_query("select jpl.id_JobLog, jpl.id_JobPart, jpl.Started, jpl.Finished, jpl.Status, jpl.Results, jpl.id_JobPartLog, jpl.Number 
from JobPartLog jpl
inner join JobLog jl on jpl.id_JobLog=jl.id_JobLog
inner join JobPart jp on jpl.id_JobPart=jp.id_JobPart and 0<>jp.Enabled
inner join Job j on jl.id_Job=j.id_Job 
where jl.id_JobLog= (select jl2.id_JobLog from JobLog jl2 where jl2.id_Job=j.id_Job order by jl2.Started desc limit 1)
order by Number;",array());

$unfinished_id_JobLog= array();
$unfinished_id_Job= array();
foreach ($rows_log as $row_log)
{
	if (null==$row_log->Finished)
	{
		$unfinished_id_JobLog[]= $row_log->id_JobLog;
		$unfinished_id_Job[]= $row_log->id_Job;
	}
}

$sites= array();
$jobs= array();
$parts= array();

foreach ($rows_site as $row_site)
{
	$sites[$row_site->id_JobSite]= $row_site;
	$row_site->res= (object)array(
		'title'=>$row_site->Name
		,'description'=>$row_site->Description
		,'synchronized'=>$row_site->SyncTime
		,'jobs'=>array()
	);
}
	
foreach ($rows_job as $row_job)
{
	$jobs[$row_job->id_Job]= $row_job;
	$row_job->res= (object)array(
		'title'=>$row_job->Name
		,'description'=>$row_job->Description
		,'max_age_minutes'=>$row_job->MaxAgeMinutes
		,'status'=>$row_job->Status
		,'parts'=>array()
	);
}

foreach ($rows_part as $row_part)
{
	$parts[$row_part->id_JobPart]= $row_part;
	$row_part->last_Number= 1000000;
	$row_part->res= (object)array(
		'title'=>$row_part->Name
		,'description'=>$row_part->Description
		,'status'=>$row_part->Status
		,'last'=>(object)array()
	);
}

function job_log_fields($row_log)
{
	return (object)array(
		'started'=> $row_log->Started,
		'finished'=> $row_log->Finished,
		'status'=> $row_log->Status
	);
}

foreach ($rows_log as $row_log)
{
	$job= $jobs[$row_log->id_Job]->res;
	$job->last= job_log_fields($row_log);
}

function job_part_log_fields($row_part_log)
{
	return (object)array(
		'started'=> $row_part_log->Started,
		'finished'=> $row_part_log->Finished,
		'status'=> $row_part_log->Status,
		'results'=> $row_part_log->Results,
	);
}

foreach ($rows_part_log as $row_part_log)
{
	$part= $parts[$row_part_log->id_JobPart];
	$part->last_Number= $row_part_log->Number;
	$part->res->last= job_part_log_fields($row_part_log);
}

$rows_previous_log= array();
$rows_previous_part_log= array();
if (0!=count($unfinished_id_Job))
{
	$unfinished_id_JobLog= implode(',',$unfinished_id_JobLog);
	$unfinished_id_Job= implode(',',$unfinished_id_Job);
	$rows_previous_log= execute_query("select jl.id_Job, jl.Started, jl.Finished, jl.Status, jl.id_JobLog 
from JobLog jl
inner join Job j on jl.id_Job=j.id_Job 
where jl.id_JobLog= (select jl2.id_JobLog from JobLog jl2 where jl2.id_Job=j.id_Job and jl2.id_JobLog not in ($unfinished_id_JobLog) order by jl2.Started desc limit 1)
and jl.id_Job in ($unfinished_id_Job);"
,array());
	$rows_previous_part_log= execute_query("select jpl.id_JobLog, jpl.id_JobPart, jpl.Started, jpl.Finished, jpl.Status, jpl.Results, jpl.id_JobPartLog 
from JobPartLog jpl
inner join JobLog jl on jpl.id_JobLog=jl.id_JobLog
inner join JobPart jp on jpl.id_JobPart=jp.id_JobPart and 0<>jp.Enabled
inner join Job j on jl.id_Job=j.id_Job 
where jl.id_JobLog= (select jl2.id_JobLog from JobLog jl2 where jl2.id_Job=j.id_Job and jl2.id_JobLog not in ($unfinished_id_JobLog) order by jl2.Started desc limit 1)
and jl.id_Job in ($unfinished_id_Job)
order by Number;",array());

	foreach ($rows_previous_log as $row_previous_log)
	{
		$job= $jobs[$row_previous_log->id_Job]->res;
		$job->previous= job_log_fields($row_previous_log);
	}

	foreach ($rows_previous_part_log as $row_previous_part_log)
	{
		$part= $parts[$row_previous_part_log->id_JobPart]->res;
		$part->previous= job_part_log_fields($row_previous_part_log);
	}
}

usort($rows_part,function($row_part1,$row_part2)
{
	$n1= $row_part1->last_Number;
	$n2= $row_part2->last_Number;
	return $n1==$n2 ? 0 : $n1>$n2 ? 1 : -1;
});

foreach ($rows_part as $row_part)
	$jobs[$row_part->id_Job]->res->parts[]= $row_part->res;
foreach ($rows_job as $row_job)
	$sites[$row_job->id_JobSite]->res->jobs[]= $row_job->res;

$result= array();
foreach ($rows_site as $row_site)
	$result[]= $row_site->res;

header('Content-Type: application/json');
echo nice_json_encode($result);