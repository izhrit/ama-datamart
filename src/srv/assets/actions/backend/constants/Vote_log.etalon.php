<?php

/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* при помощи модуля codec.Vote_log.php.js */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */


$Vote_log_type_description= array(
   array('code'=>'n', 'descr'=>'Начало голосования')
  ,array('code'=>'s', 'descr'=>'Подписан бюллетень')
  ,array('code'=>'a', 'descr'=>'Выбран ответ')
  ,array('code'=>'c', 'descr'=>'Отправлен код')
  ,array('code'=>'b', 'descr'=>'Письмо о подписи')
  ,array('code'=>'l', 'descr'=>'Вход в кабинет')
  ,array('code'=>'r', 'descr'=>'Итоги заседания')
  ,array('code'=>'f', 'descr'=>'Не состоялось')
  ,array('code'=>'е', 'descr'=>'Приостановлено')
  ,array('code'=>'t', 'descr'=>'Продолжено')
  ,array('code'=>'z', 'descr'=>'Продолжено без вас')
  ,array('code'=>'o', 'descr'=>'Отменено')
  ,array('code'=>'w', 'descr'=>'Неверный код')
  ,array('code'=>'d', 'descr'=>'Код прислан поздно')
);

