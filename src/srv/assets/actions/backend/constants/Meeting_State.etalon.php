<?php

/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* при помощи модуля codec.Meeting_State.php.js */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */


$Meeting_State_descriptions= array(
   array('code'=>'a', 'descr'=>'Планирование')
  ,array('code'=>'b', 'descr'=>'Голосование')
  ,array('code'=>'c', 'descr'=>'Приостановлено')
  ,array('code'=>'d', 'descr'=>'Завершено')
  ,array('code'=>'e', 'descr'=>'Состоялось')
  ,array('code'=>'f', 'descr'=>'Не состоялось')
  ,array('code'=>'g', 'descr'=>'Отменено')
);
