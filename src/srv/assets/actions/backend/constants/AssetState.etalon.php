<?php

/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* при помощи модуля codec.asset_state.php.js */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */


$Asset_State_spec= array(
   array('code'=>'a', 'title'=>'Инвентаризация', 'name'=>'Инвентаризация')
  ,array('code'=>'b', 'title'=>'Зарегистрировано отсутствие', 'name'=>'Отсутствие')
  ,array('code'=>'c', 'title'=>'Выбыло из конкурсной массы', 'name'=>'Выбыло')
  ,array('code'=>'d', 'title'=>'Определение условий продажи', 'name'=>'На_продажу')
  ,array('code'=>'e', 'title'=>'Продажа без торгов', 'name'=>'Без_торгов')
  ,array('code'=>'f', 'title'=>'Определён покупатель без торгов', 'name'=>'Без_торгов_покупается')
  ,array('code'=>'g', 'title'=>'Продано', 'name'=>'Продано')
  ,array('code'=>'h', 'title'=>'Первые торги', 'name'=>'Торги1')
  ,array('code'=>'i', 'title'=>'Первые торги завершены', 'name'=>'Торги1_завершены')
  ,array('code'=>'j', 'title'=>'Определён покупатель в первых торгах', 'name'=>'Торги1_покупается')
  ,array('code'=>'k', 'title'=>'Повторные торги', 'name'=>'Торги2')
  ,array('code'=>'l', 'title'=>'Повторные торги завершены', 'name'=>'Торги2_завершены')
  ,array('code'=>'m', 'title'=>'Определён покупатель в повторных торгах', 'name'=>'Торги2_покупается')
  ,array('code'=>'n', 'title'=>'Публичное предложение', 'name'=>'Торги3')
  ,array('code'=>'o', 'title'=>'Публичное предложение завершено', 'name'=>'Торги3_завершены')
  ,array('code'=>'p', 'title'=>'Определён покупатель в публичном предложении', 'name'=>'Торги3_покупается')
);
