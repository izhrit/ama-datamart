<?php

function FindDescription($description_array,$key_name,$key_value)
{
	foreach ($description_array as $d)
	{
		if ($key_value==$d[$key_name])
			return $d;
	}
	return null;
}

function beautify_debtorName($debtorName)
{
	global $debtorName_beauty_prefixes;
	foreach ($debtorName_beauty_prefixes as $p)
	{
		$prefix= $p['prefix'];
		$prefix_len= mb_strlen($prefix);
		if ($prefix_len < mb_strlen($debtorName))
		{
			$debtorPrefix = mb_strtolower(mb_substr($debtorName, 0, $prefix_len));
			if ($debtorPrefix == $prefix)
				return $p['change_to'].mb_substr($debtorName,$prefix_len);
		}
	}
	return $debtorName;
}