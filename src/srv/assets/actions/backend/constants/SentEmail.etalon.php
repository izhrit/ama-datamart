<?php

/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* при помощи модуля codec.SentEmail.php.js */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */


global $EmailType_descriptions, $RecipientType_description;
$RecipientType_description= array(
   array('table'=>'Manager', 'code'=>'m')
  ,array('table'=>'MUser', 'code'=>'v')
  ,array('table'=>'Vote', 'code'=>'c')
  ,array('table'=>'AwardVote', 'code'=>'a')
  ,array('table'=>'Pushed_event', 'code'=>'p')
  ,array('table'=>'Pushed_message', 'code'=>'s')
  ,array('table'=>'efrsb_manager', 'code'=>'e')
  ,array('table'=>'GCPushedEvent', 'code'=>'g')
  ,array('table'=>'PreDebtor', 'code'=>'d')
);

$EmailType_descriptions= array(
   array('table'=>'Manager', 'code'=>'p', 'descr'=>'смена пароля АУ')
  ,array('table'=>'Manager', 'code'=>'b', 'descr'=>'блокирование АУ')
  ,array('table'=>'Manager', 'code'=>'d', 'descr'=>'смена пароля АУ через сро')
  ,array('table'=>'Manager', 'code'=>'y', 'descr'=>'уведомление о запросах в сро')
  ,array('table'=>'efrsb_manager', 'code'=>'q', 'descr'=>'создание АУ через сро')
  ,array('table'=>'MUser', 'code'=>'c', 'descr'=>'создание наблюдателя')
  ,array('table'=>'MUser', 'code'=>'m', 'descr'=>'смена email наблюдателя')
  ,array('table'=>'MUser', 'code'=>'h', 'descr'=>'информация об экспозиции')
  ,array('table'=>'Vote', 'code'=>'n', 'descr'=>'о заседании КК', 'Наименование_документа'=>'Уведомление о заседании', 'FileName_prefix'=>'N')
  ,array('table'=>'Vote', 'code'=>'a', 'descr'=>'о начале подписания КК', 'Наименование_документа'=>'Уведомление о голосовании', 'FileName_prefix'=>'I')
  ,array('table'=>'Vote', 'code'=>'s', 'descr'=>'о подписании КК', 'Наименование_документа'=>'Уведомление о подписи', 'FileName_prefix'=>'G')
  ,array('table'=>'Vote', 'code'=>'r', 'descr'=>'об итогах КК', 'Наименование_документа'=>'Уведомление об итогах', 'FileName_prefix'=>'R')
  ,array('table'=>'Vote', 'code'=>'f', 'descr'=>'КК не состоялось', 'Наименование_документа'=>'Уведомление о несостоявшемся', 'FileName_prefix'=>'F')
  ,array('table'=>'Vote', 'code'=>'o', 'descr'=>'КК приостановлено', 'Наименование_документа'=>'Уведомление о приостановке', 'FileName_prefix'=>'P')
  ,array('table'=>'Vote', 'code'=>'t', 'descr'=>'КК продолжено', 'Наименование_документа'=>'Уведомление о продолжении', 'FileName_prefix'=>'T')
  ,array('table'=>'Vote', 'code'=>'z', 'descr'=>'КК продолжено без вас', 'Наименование_документа'=>'Уведомление о блокировании', 'FileName_prefix'=>'Z')
  ,array('table'=>'Vote', 'code'=>'l', 'descr'=>'КК отменено', 'Наименование_документа'=>'Уведомление об отмене', 'FileName_prefix'=>'L')
  ,array('table'=>'AwardVote', 'code'=>'w', 'descr'=>'голос за премию АУ')
  ,array('table'=>'Pushed_event', 'code'=>'e', 'descr'=>'уведомление о событии')
  ,array('table'=>'Pushed_message', 'code'=>'g', 'descr'=>'уведомление о публикации на ЕФРСБ')
  ,array('table'=>'GCPushedEvent', 'code'=>'x', 'descr'=>'уведомление о событии из google календаря')
  ,array('table'=>'PreDebtor', 'code'=>'k', 'descr'=>'предоставление доступа к кабинету должника')
);
