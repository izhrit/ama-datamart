<?php

/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* при помощи модуля codec.Vote_document.php.js */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */


$Vote_document_type_description= array(
   array('code'=>'b', 'descr'=>'b', 'Наименование_документа'=>'Бюллетень голосования', 'FileName_prefix'=>'B', 'FileName_extension'=>'txt')
  ,array('code'=>'s', 'descr'=>'s', 'Наименование_документа'=>'Подпись участника под бюллетенем', 'FileName_prefix'=>'S', 'FileName_extension'=>'txt')
  ,array('code'=>'r', 'descr'=>'r', 'Наименование_документа'=>'Подпись ИС «Витрина данных ПАУ»', 'FileName_prefix'=>'O', 'FileName_extension'=>'sig')
);

