<?php

	require_once '../assets/helpers/db.php';
	require_once '../assets/helpers/log.php';
	require_once '../assets/helpers/time.php';

	/**
	 * Undocumented function
	 *
	 * @param [type] $link
	 * @param [type] $id_MProcedure
	 * @return void
	 */
	function deletDuplicateLeak($link, $id_MProcedure){
		$query = "DELETE Leak.* FROM Leak
				LEFT JOIN PData ON PData.id_PData=Leak.id_PData
				WHERE PData.id_MProcedure=?";
		$stmt = mysqli_prepare($link, $query);
		try{
			$specchar="i";
			mysqli_stmt_bind_param($stmt,$specchar, $id_MProcedure);
			mysqli_stmt_execute($stmt);
			mysqli_stmt_close($stmt);
		}
		catch (Exception $ex){
			mysqli_stmt_close($stmt);
			throw $ex;
		}
	}

	/**
	 * Добавление привлеченных специалистов в Leak
	 *
	 * @param [type] $link
	 * @param [type] $id_PData
	 * @param [type] $sum
	 * @param DateTime $date
	 * @param [type] $ContragentName
	 * @param [type] $ContragentInn
	 * @param [type] $isAccredited
	 * @return void
	 * */
	function addLeak($link, $id, $sum, DateTime $date, $Name, $Inn, $isAccredited) {
		$query = "insert into `Leak` (id_PData,LeakSum,LeakDate,ContragentName,ContragentInn,isAccredited)
							values (?,?,?,?,?,?)";
		$stmt = mysqli_prepare($link, $query);
		try{
			$specchar="isssss";
			$date = $date->format('Ymd');
			mysqli_stmt_bind_param($stmt,$specchar, $id, $sum, $date, $Name, $Inn, $isAccredited);
			mysqli_stmt_execute($stmt);
			mysqli_stmt_close($stmt);
		}
		catch (Exception $ex){
			mysqli_stmt_close($stmt);
			throw $ex;
		}
	}

	/**
	 * изменение поля Processed в таблице PData
	 *
	 * @param [type] $link
	 * @param [type] $id
	 * @return void
	 */
	function updateProcessedPData($link, $id, $value){
		$query = "UPDATE `PData` pd SET `Processed`= ? WHERE pd.id_PData=?";
		$stmt=mysqli_prepare($link,$query);
		try{
			mysqli_stmt_bind_param($stmt,"ii",$value, $id);
			mysqli_stmt_execute($stmt);
			mysqli_stmt_close($stmt);
		}
		catch (Exception $ex){
			mysqli_stmt_close($stmt);
			throw $ex;
		}
	}

	/**
	 * Выполнение заполнения таблицы Leak
	 *
	 * @param [type] $link
	 * @param [type] $id
	 * @param [type] $specs
	 * @return void
	 */
	function processPData ($link, $id, $specs, $id_MProcedure){
		mysqli_begin_transaction($link, MYSQLI_TRANS_START_WITH_CONSISTENT_SNAPSHOT);
		try{
			deletDuplicateLeak($link,$id_MProcedure);
			foreach ($specs as $spec) {
				$date = DateTime::createFromFormat('d.m.Y', $spec->Договор->Срок->Конец->Дата);
				$isAccredited = $spec->Аккредитация != "отсутствует";
				addLeak($link, $id, $spec->Оплата->Размер, $date, $spec->Наименование, $spec->ИНН, $isAccredited);
			}
			updateProcessedPData($link, $id, 1);
			mysqli_commit($link);
		}
		catch (Exception $ex){
			mysqli_commit($link);
			throw $ex;
		}
	}

		/**
	 * нахождение в zip архиве xml файла report
	 *
	 * @param [type] $zip
	 * @return void
	 */
	function unzipReportFromZip ($zip)
	{
		$xml=NULL;
		while ($zip_entry = zip_read($zip))
		{
			$entry = zip_entry_name($zip_entry); //здесь имя файла
			if (strcasecmp(pathinfo($entry, PATHINFO_FILENAME), "report") !== 0){ //проверка имени
				continue;
			}
			try{ 
				zip_entry_open($zip, $zip_entry, "r");//не удалось открыть
				$content = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));//не получено содержимое
				$content = str_replace('xmlns="myNamespace"', '', $content);
				$xml = new SimpleXMLElement($content);
				zip_entry_close($zip_entry);
			}
			catch (Exception $ex){
				zip_entry_close($zip_entry);
				throw $ex;
			}
		}
		return $xml;
	}

	/**
	 * Распаковка xml файла report
	 *
	 * @param [type] $localPath
	 * @return void
	 */
	function unzipReport ($temp_dir)
	{
		$zip = zip_open($temp_dir);
		try{
			$xml=  unzipReportFromZip($zip);
			zip_close($zip);
		}
		catch (Exception $ex){
			zip_close($zip);
			throw $ex;	
		}
		return $xml;
	}

	/**
	 * sql запрос к таблице PData
	 *
	 * @param [type] $link
	 * @return void
	 */
	function query_for_PData($link){
		$query = "select `fileData`, `id_PData`, `id_MProcedure` from PData pd 
				where pd.Processed is NULL ORDER BY `id_PData` DESC LIMIT 100";
		$res = mysqli_query($link, $query);
		return $res;
	}

	/**
	 * 
	 * @return int
	 */
	function mainAddLeak($link)
	{
		echo "\n".job_time(). ": start cheking next 100 records from PData";
		$res = query_for_PData($link);
		write_to_log($res);
		echo "\n".job_time().": got ".$res->num_rows." records";
		if($res->num_rows == 0) return 0;
		global $temp_dir;
		echo "\n".job_time().": cheking files of the id_PData = ";
		$have = 0;
		$nothave = 0;
		while($row = mysqli_fetch_array($res))
		{
			echo $row['id_PData'], ",";
			try{
				file_put_contents($temp_dir, $row['fileData']);
			}
			catch(Exception $ex){
				updateProcessedPData($link, $row['id_PData'], 0);
				echo $ex," \n";
			}
			try{
				$xml = unzipReport($temp_dir);
				$specs = $xml && isset($xml->Привлеченные_специалисты->Специалист) 
					? $xml->Привлеченные_специалисты->Специалист : [];
				if($specs){
						processPData($link, $row['id_PData'], $specs,$row['id_MProcedure']);
						$have++;
						write_to_log('$have');
						write_to_log($have);
					} 
				else{		 
						updateProcessedPData($link, $row['id_PData'], 1);
						$nothave++;
					}
			}
			catch(Exception $ex){
				unlink($temp_dir);
				updateProcessedPData($link, $row['id_PData'], 0);
				echo $ex," \n";
			}
		}
		echo "\n".job_time().": the number of records that have a report: ".$have;
		echo "\n".job_time().": the number of records that don't have a report: ".$nothave;
		return 1;
	}
