<?
$fields= "
	id_MRequest
	,DebtorCategory
	,DebtorName debtorName
	,DebtorName2 debtorName2
	,c.ShortName Court
	,DATE_FORMAT(DateOfCreation, '%d.%m.%Y') DateOfCreation
	,DATE_FORMAT(DateOfOffer, '%d.%m.%Y') DateOfOffer
	,concat(m.lastName,' ',left(m.firstName,1),'.',left(m.middleName,1),'.') Manager
	,DATE_FORMAT(DateOfResponce, '%d.%m.%Y') DateOfResponce
	,DATE_FORMAT(NextSessionDate, '%d.%m.%Y %H:%i') NextSessionDate
	,DateOfApplication
";

$from_where= "from MRequest mr 
inner join Court c on mr.id_Court=c.id_Court 
left join Manager m on mr.id_Manager=m.id_Manager
where mr.id_SRO=".$auth_info->id_SRO;

$filter_rule_builders= array(
	'debtorName'=>function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and (DebtorName like '$data%' or DebtorName2 like '$data%')";
	}
	,'Court'=>prep_std_filter_rule_builder_for_expression('c.SearchName')
	,'Manager'=>prep_std_filter_rule_builder_for_expression('m.lastName')
);

function filterByDate($customFilter, $key, $date) {
	if(isset($customFilter->{$key})) {
		if(isset($customFilter->{$key}->Include) && $customFilter->{$key}->Include)
			return ' and ' . $date . ' is not null';
		if(isset($customFilter->{$key}->Exclude) && $customFilter->{$key}->Exclude)
			return ' and ' . $date . ' is null';
	}
}

if(isset($_GET['customFilter'])) {
	$customFilter = json_decode(urldecode($_GET['customFilter']));
	$from_where .= filterByDate($customFilter, 'DateOfOffer', 'mr.DateOfOffer');
	$from_where .= filterByDate($customFilter, 'DateOfResponce', 'mr.DateOfResponce');
	if(isset($customFilter->Consents)) {
		if(isset($customFilter->Consents->Include) && $customFilter->Consents->Include)
			$from_where .= ' and exists (select ps.id_ProcedureStart from ProcedureStart ps where mr.id_MRequest=ps.id_MRequest)';
		if(isset($customFilter->Consents->Exclude) && $customFilter->Consents->Exclude)
			$from_where .= ' and not exists (select ps.id_ProcedureStart from ProcedureStart ps where mr.id_MRequest=ps.id_MRequest)';
	}
}

if (isset($_GET['sidx'])) {
	$_GET['sidx'] = str_replace('DateOfCreation','mr.DateOfCreation',$_GET['sidx']);
	$_GET['sidx'] = str_replace('DateOfResponce','mr.DateOfResponce',$_GET['sidx']);
	$_GET['sidx'] = str_replace('NextSessionDate','mr.NextSessionDate',$_GET['sidx']);
}
if (!isset($_GET['sidx']) || ''==$_GET['sidx']) {
	$_GET['sidx']= 'mr.DateOfCreation';
	$_GET['sord']= 'desc';
}

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);