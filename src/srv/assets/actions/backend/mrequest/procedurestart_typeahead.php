<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/requests/request_crud_lib.php';

global $auth_info;
$auth_info= CheckAuthSRO();
$id_SRO= $auth_info->id_SRO;
$query= CheckMandatoryGET('query');

$debtorCategory= "n";
if(isset($_GET['debtorCategory']) && $_GET['debtorCategory']=="l") 
	$debtorCategory= "l";

$isMarriedCouple = false;
if(isset($_GET['isMarriedCouple']) && $_GET['isMarriedCouple']==1) 
	$isMarriedCouple= true;

$txt_query= "
		select DebtorName, DebtorCategory, DebtorINN, DebtorOGRN, DebtorSNILS, DebtorAddress
		, DebtorName2 , DebtorSNILS2, DebtorINN2, DebtorOGRN2, DebtorAddress2, DebtorCategory2
		, ApplicantName, ApplicantCategory, ApplicantINN, ApplicantOGRN, ApplicantSNILS, ApplicantAddress
		, DATE_FORMAT(DateOfApplication, '%d.%m.%Y') DateOfApplication
		, DATE_FORMAT(NextSessionDate, '%d.%m.%Y %H:%i') NextSessionDate
		, DATE_FORMAT(DateOfRequestAct, '%d.%m.%Y') DateOfRequestAct
		, CourtDecisionURL, CourtDecisionAddInfo
		, CaseNumber, c.Name, c.id_Court

			from ProcedureStart ps
			inner join Court c on ps.id_Court=c.id_Court
			left join Manager m on ps.id_Manager=m.id_Manager
			where m.id_SRO=? and (ps.ShowToSRO!=0 or ps.AddedBySRO=1) and ps.id_MRequest is null and
			(ps.DebtorName like ? or ps.DebtorINN like ? or ps.DebtorSNILS like ? or
			ps.DebtorName2 like ? or ps.DebtorINN2 like ? or ps.DebtorSNILS2 like ?) and
			ps.DebtorCategory=? and
			";	
if($isMarriedCouple) {
	$txt_query .= " ps.DebtorName2 is not null";
} else {
	$txt_query .= " ps.DebtorName2 is null";
}
$txt_query .= " limit 20;";

$rows= execute_query($txt_query,array('isssssss', $id_SRO, $query.'%', $query.'%', $query.'%', $query.'%', $query.'%', $query.'%', $debtorCategory));

function PrepareRequest($row, $isDebtor1Overlap) {
	global $request_helper;
	$request= array(
		'Заявитель'=>$request_helper->request_row_to_applicant($row)
		, 'Заявление_на_банкротство'=>array(
			'В_суд'=>(null==$row->id_Court)?null:array('id'=>$row->id_Court,'text'=>$row->Name)
			,'Дата_рассмотрения'=> $row->NextSessionDate
		)
		, 'Номер_судебного_дела'=> $row->CaseNumber
		, 'Запрос'=> array(
			"Ссылка"=>$row->CourtDecisionURL
			,'Время'=> array(
				'акта'=>$row->DateOfRequestAct
			)
			,'Дополнительная_информация' => $row->CourtDecisionAddInfo
		)
		, 'Запрос_дополнительная_информация' => array(
			'Дополнительная_информация' => $row->CourtDecisionAddInfo
		)
		, 'Согласия_АУ'=> array(
			'Согласия'=> array()
		)
	);

	if(isset($row->DebtorName2)) {
		$request['Должник'] = array(
			'Тип' => 'Супруги'
			, 'Супруги' => array(
				'Должник1' => $request_helper->request_row_to_debtor($row, !$isDebtor1Overlap ? 2 : '' )
				, 'Должник2' => $request_helper->request_row_to_debtor($row, $isDebtor1Overlap ? 2 : '')
			)
			);
	} else {
		$request['Должник'] = $request_helper->request_row_to_debtor($row);
	}

	return $request;
}
function CheckOverlapDebtorWithQuery($DebtorName, $DebtorINN, $DebtorSNILS, $query) {
	$query_lower = mb_strtolower($query);
	$query_length = mb_strlen($query);
	$overlap_name = mb_strtolower(mb_substr($DebtorName, 0, $query_length));
	$overlap_inn = mb_strtolower(mb_substr($DebtorINN, 0, $query_length));
	$overlap_snils = mb_strtolower(mb_substr($DebtorSNILS, 0, $query_length));
	
	if($overlap_name == $query_lower || $overlap_inn == $query_lower || $overlap_snils == $query_lower)
		return true;
	
	return false;
}
$consents = array();

if(0!=count($rows)) {
	foreach($rows as $row) {
		$isDebtor1Overlap = CheckOverlapDebtorWithQuery($row->DebtorName, $row->DebtorINN, $row->DebtorSNILS, $query);
		$consent = array(
			'name'=> $isDebtor1Overlap ? $row->DebtorName : $row->DebtorName2
			, 'category'=> $isDebtor1Overlap ? $row->DebtorCategory : $row->DebtorCategory
			, 'inn'=> $isDebtor1Overlap ? $row->DebtorINN : $row->DebtorINN
			, 'ogrn'=> $isDebtor1Overlap ? $row->DebtorOGRN : $row->DebtorOGRN
			, 'snils'=> $isDebtor1Overlap ? $row->DebtorSNILS : $row->DebtorSNILS
			, 'caseNumber'=> $row->CaseNumber
			, 'courtName'=> $row->Name
			, 'Request'=> PrepareRequest($row, $isDebtor1Overlap)
		);

		$txt_query= "
				select m.id_Manager
				, concat(m.lastName,' ',m.firstName,' ',m.middleName,' ') ManagerName
				, ps.id_ProcedureStart
				, DATE_FORMAT(ps.DateOfApplication, '%d.%m.%Y') DateOfApplication
				, ps.CaseNumber
				, ps.id_Contract

					from ProcedureStart ps
					left join Manager m on ps.id_Manager=m.id_Manager
					where (m.id_SRO=? and (ps.ShowToSRO!=0 or ps.id_Contract is null)) and ps.DebtorName=?
					order by ps.TimeOfCreate desc
					limit 20
		;";
		
		$aus = execute_query($txt_query,array('is', $id_SRO, $row->DebtorName));
		foreach($aus as $k=>$au) {
			if($k == 1)
				$consent['Request']['Согласия_АУ']['В_отчет'] = $au->id_ProcedureStart;
			array_push($consent['Request']['Согласия_АУ']['Согласия'], array(
				'АУ'=> array(
					'id'=> $au->id_Manager
					, 'text'=> $au->ManagerName
				)
				, 'id_ProcedureStart'=> $au->id_ProcedureStart
				, 'Заявление_на_банкротство'=> array (
					'Дата_подачи'=> $au->DateOfApplication
				)
				, 'Номер_судебного_дела'=> $au->CaseNumber
				, 'id_Contract'=> !isset($row->id_Contract)?null:$row->id_Contract
				));
		}

		array_push($consents, $consent);
	}
}

echo nice_json_encode($consents);