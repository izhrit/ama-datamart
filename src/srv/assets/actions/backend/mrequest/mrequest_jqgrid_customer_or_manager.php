<?
require_once '../assets/helpers/json.php';

global $auth_info;
$fields= "
	id_MRequest
	,DebtorCategory DebtorCategory
	,DebtorName debtorName
	,DebtorName2 debtorName2
	,c.ShortName Court
	,DATE_FORMAT(DateOfRequestAct, '%d.%m.%Y') DateOfRequestAct
	,id_SRO
";

$from_where= 'from MRequest mr
inner join Court c on mr.id_Court=c.id_Court
where mr.DateOfOffer is not null and exists (select m.id_Manager from Manager m where m.id_SRO=mr.id_SRO';
if (isset($auth_info->id_Contract))
	$from_where.= ' and m.id_Contract='.$auth_info->id_Contract;
if (isset($auth_info->id_Manager))
	$from_where.= ' and m.id_Manager='.$auth_info->id_Manager;

$from_where.= ') ';

$filter_rule_builders= array(
	'debtorName'=>function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and (DebtorName like '$data%' or DebtorName2 like '$data%')";
	}
	,'Court'=>prep_std_filter_rule_builder_for_expression('c.SearchName')
	,'DateOfRequestAct'=>function($l,$rule)
	{	
		$txt_query = '';
		$data= mysqli_real_escape_string($l,$rule->data);
		$data_divided= explode(".", $data);
		foreach($data_divided as $k=>$data_divided_item) {
			if(is_numeric($data_divided_item)) {
				switch($k) {
					case 0:
						$txt_query.= ' and day(DateOfRequestAct)='.$data_divided_item;
					break;
					case 1:
						$txt_query.= ' and month(DateOfRequestAct)='.$data_divided_item;
					break;
					case 2:
						$txt_query.= ' and year(DateOfRequestAct)='.$data_divided_item;
					break;
				}
			}else {
				$txt_query = '';
			}
		}
		return $txt_query;
	}
);

if (isset($_GET['sidx'])) {
	$_GET['sidx'] = str_replace('DateOfRequestAct','mr.DateOfRequestAct',$_GET['sidx']);
}
if (!isset($_GET['sidx']) || ''==$_GET['sidx']) {
	$_GET['sidx']= 'mr.DateOfCreation';
	$_GET['sord']= 'desc';
}

$result = execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders,'id_MRequest');

foreach ($result['rows'] as $i=>$row)
{
	$txt_query= "select
		ps.id_Contract
		,ps.TimeOfCreate timeOfCreate
		,concat(m.lastName,' ',left(m.firstName,1),'.',left(m.middleName,1),'.') Manager
		from ProcedureStart ps
		inner join Manager m on ps.id_Manager=m.id_Manager
		where ps.id_MRequest=?
		order by ps.TimeOfCreate asc
	";
	$consent_rows= execute_query($txt_query,array('i',$row->id_MRequest));
	$consents= (object)array('amount'=> count($consent_rows),'ourPosition'=> null,'managerName'=> null);
	foreach ($consent_rows as $j=>$consent_row)
	{
		if ($consent_row->id_Contract===$auth_info->id_Contract)
		{
			$consents->ourPosition= $j+1;
			$consents->managerName= $consent_row->Manager;
			break;
		}
	}
	$row->consents= $consents;
}

header('Content-Type: text/plain');
echo nice_json_encode($result);
