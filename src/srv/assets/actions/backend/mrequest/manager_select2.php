<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthCustomer();

CheckMandatoryGET('id_SRO');
$id_SRO= intval($_GET['id_SRO']);

$txt_query= "
	select
			id_Manager id
		, concat(lastName,' ',firstName,' ',middleName) text
	from Manager
	where lastName like ? and id_Contract=? and id_SRO=?";
$txt_query.= " limit 20;";

$params = array('sss',$_GET['q'].'%',$auth_info->id_Contract,$id_SRO);

$rows= execute_query($txt_query,$params);

echo nice_json_encode(array('results'=>$rows));