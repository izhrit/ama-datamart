<?
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManagerOrSRO();

if ('sro'===$auth_info->category)
{
	require_once '../assets/actions/backend/mrequest/mrequest_jqgrid_sro.php';
}
else if('manager'===$auth_info->category || 'customer'===$auth_info->category)
{
	require_once '../assets/actions/backend/mrequest/mrequest_jqgrid_customer_or_manager.php';
}