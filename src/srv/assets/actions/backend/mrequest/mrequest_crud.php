<?
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/requests/request_crud_lib.php';

global $auth_info;
$auth_info= CheckAuth_cmd_Category(array(
	'add'=>array('sro')
	,'get'=>array('sro','manager','customer')
	,'update'=>array('sro')
	,'delete'=>array('sro')
));

class mrequest_crud extends Base_crud
{
	function create($request)
	{
		write_to_log('create {');
		global $auth_info;
		$request = validate_request($request);

		$result = create_request($request);

		$res = array(
			"ok"=>true
			, "data"=>$result
		);
		write_to_log('create }');
		echo nice_json_encode($res);
	}

	function read($id_MRequest)
	{
		$request= read_request($id_MRequest);

		if (null==$request)
		{
			write_to_log("can not find mrequest id_MRequest=$id_MRequest");
			exit_not_found("can not find mrequest id_MRequest=$id_MRequest");
		}
		else
		{
			write_to_log(nice_json_encode($request));
			echo nice_json_encode($request);
		}
	}

	function update($id_MRequest,$request)
	{
		global $auth_info;

		$result= update_request($id_MRequest,(object) $request);

		if ($result['affected']>=1)
		{
			$res = array(
				"ok"=>true
				, "data"=>$result
			);
			echo nice_json_encode($res);
		}
		else if (0==$result['affected'])
		{
			$res = array(
				"ok"=>true
				, "data"=>$result
			);
			echo nice_json_encode($res);
		}
		else
		{
			exit_bad_request("affected " . $result['affected'] . "when update id_MRequest=$id_MRequest");
		}
	}

	function delete($ids_MRequest)
	{
		global $auth_info;

		if (delete_requests($ids_MRequest))
		{
			echo '{ "ok": true }';
		}
		else
		{
			exit_not_found("can not delete MRequest!");
		}
	}
}

$crud= new mrequest_crud();
$crud->process_cmd();