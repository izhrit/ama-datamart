<?
require_once '../assets/libs/auth/check.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/time.php';
require_once '../assets/libs/tpl/tpl_by_sro_and_filename.php';
require_once '../assets/libs/tpl/tpl_mrequest_params.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManagerOrSRO();

$id_ProcedureStart= CheckMandatoryGET_id('id_ProcedureStart');

$tpl_par= prepare_procedure_start_tpl_params($id_ProcedureStart);

$sro_tpl_folder= get_sro_tpl_folder_by_RegNum($tpl_par->СРО->RegNum);
$tpl_path= get_tpl_path_for_sro_tpl_folder($sro_tpl_folder, 'discharge');
update_tpl_par_for_sro($sro_tpl_folder, $tpl_par);

$filename= 'ВАУ';
if (isset($row->DebtorName))
	$filename.= '_'.str_replace(' ','_',$row->DebtorName);
$filename.= '.doc';

if (!isset($_GET['format']) || 'html'!=$_GET['format'])
{
	header("Content-Type: application/doc");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("content-disposition: attachment;filename=" . urlencode($filename));
}
require_once $tpl_path;
