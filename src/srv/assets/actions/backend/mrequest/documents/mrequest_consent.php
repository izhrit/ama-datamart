<?
require_once '../assets/config.php';

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/realip.php';
require_once '../assets/helpers/password.php';

require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/tpl/tpl_by_sro_and_filename.php';
require_once '../assets/libs/tpl/tpl_mrequest_params.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManagerOrSRO();

if (isset($_GET['id_MRequest']))
{
	$id_MRequest= $_GET['id_MRequest'];
	$tpl_par= prepare_mrequest_tpl_params($id_MRequest);
}
else if (isset($_GET['id_ProcedureStart']))
{
	$id_ProcedureStart= $_GET['id_ProcedureStart'];
	$tpl_par= prepare_procedure_start_tpl_params($id_ProcedureStart);
}
else
{
	exit_bad_request('skipped parameter id_MRequest and id_ProcedureStart');
}

$факсимиле_подписи_АУ= приготовить_факсимиле_подписи_АУ_для_документа($tpl_par->АУ->id_Manager);
if (null!=$факсимиле_подписи_АУ)
	$tpl_par->АУ->Факсимиле= $факсимиле_подписи_АУ;

$sro_tpl_folder= get_sro_tpl_folder_by_RegNum($tpl_par->СРО->RegNum);
$tpl_path= get_tpl_path_for_sro_tpl_folder($sro_tpl_folder, 'consent');
update_tpl_par_for_sro($sro_tpl_folder, $tpl_par);

if (!isset($_GET['format']) || 'html'!=$_GET['format'])
{
	$filename= 'САУ';
	if (isset($tpl_par->Должник->Наименование))
		$filename.= '_'.str_replace(' ','_',$tpl_par->Должник->Наименование);
	$filename.= '.doc';
	header("Content-Type: application/doc");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("content-disposition: attachment;filename=" . urlencode($filename));
}

require_once $tpl_path;
