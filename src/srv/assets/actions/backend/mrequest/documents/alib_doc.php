<?

function GetCurrentDate($type, $currentDate)
{
	$date_now = isset($currentDate) && ''!==$currentDate ? date_create_from_format('d.m.Y', $currentDate) : safe_date_create();
	switch($type)
	{
		case 'fir':
			$monthes = array(".01."=>"января", ".02."=>"февраля", ".03."=>"марта", ".04."=>"апреля", ".05."=>"мая", ".06."=>"июня"
			  , ".07."=>"июля", ".08."=>"августа", ".09."=>"сентября", ".10."=>"октября", ".11."=>"ноября", ".12."=>"декабря");
			$_mD = date_format($date_now,".m.");
			return str_replace($_mD, " ".$monthes[$_mD]." ", date_format($date_now,'«d».m.Y'));
		default:
			return date_format($date_now,'d.m.Y');
	}
}