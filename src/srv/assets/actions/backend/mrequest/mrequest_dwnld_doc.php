<?

require_once '../assets/helpers/validate.php';

$doctype= CheckMandatoryGET('doctype');

write_to_log("doctype=$doctype");

function tpl_file_name_for_doctype($doctype)
{
	switch ($doctype)
	{
		case 'responce': return 'mrequest_responce';
		case 'protocol': return 'mrequest_protocol';
		case 'consent': return 'mrequest_consent';
		case 'discharge': return 'mrequest_discharge';
		default: return null;
	}
}

$tpl_file_name= tpl_file_name_for_doctype($doctype);

if (null==$tpl_file_name)
	exit_bad_request('For doctype='.$doctype.' found no templates');

require_once "../assets/actions/backend/mrequest/documents/$tpl_file_name.php";
