<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthSRO();
$id_SRO= $auth_info->id_SRO;
$query= CheckMandatoryGET('query');

write_to_log(1);

$txt_query= "
select 
   m.id_Manager
 , concat(m.lastName,' ',left(m.firstName,1),'. ',left(m.middleName,1),'. ') ManagerName
 , ps.id_ProcedureStart
 , DATE_FORMAT(ps.DateOfApplication, '%d.%m.%Y') DateOfApplication
 , ps.CaseNumber
 , ps.DebtorName
 , ps.id_Contract
from ProcedureStart ps
left join Manager m on ps.id_Manager=m.id_Manager
where (m.id_SRO=? and (ps.ShowToSRO!=0 or ps.AddedBySRO=1)) 
and ps.id_MRequest is null 
and ( m.lastName like ? or m.firstName like ? or m.middleName like ? or ps.CaseNumber like ? or ps.DebtorName like ?)
order by ManagerName
limit 20
;";

$rows= execute_query($txt_query,array('isssss', $id_SRO, $query.'%', $query.'%', $query.'%', $query.'%', $query.'%'));

write_to_log(2);

$result = array();
if (0!=count($rows))
{
	foreach($rows as $row)
	{
		$res= array
		(
			'АУ'=> array
			(
				'id'=> $row->id_Manager
				, 'text'=> $row->ManagerName
			)
			, 'id_ProcedureStart'=> $row->id_ProcedureStart
			, 'Заявление_на_банкротство' => array
			(
				'Дата_подачи'=> $row->DateOfApplication
				,'Должник'=> $row->DebtorName
			)
			, 'Номер_судебного_дела'=> $row->CaseNumber
			, 'id_Contract'=> $row->id_Contract
		);
		array_push($result, $res);
	}
}

write_to_log($result);

echo nice_json_encode($result);