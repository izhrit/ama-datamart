<?
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/log/access_log.php';

$email	= $_POST['params']['email'];
$guid	= $_POST['params']['guid'];
$report	= $_POST['form']['report'];



if (!preg_match('/^[^0-9][_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $email)) {
	exit_bad_request('Invalid email format');
}
if(!preg_match('/^[a-f0-9]{32}$/', $guid)) {
	exit_bad_request('Invalid guid format');
}



$txt_query = " 
SELECT id_Push_receiver
FROM Push_receiver
WHERE Destination = ? AND ReceiverGUID = ? AND Transport <> 'a'
";

$result = execute_query($txt_query, array('ss', $email, $guid));


if(count($result) > 0 ){
	$push_receiver = $result[0]->id_Push_receiver;
	$txt_query = " 
	UPDATE Push_receiver
	SET Transport = 'a', Destination = NULL
	WHERE id_Push_receiver = ?;
	";
	execute_query_no_result($txt_query, array('s', $push_receiver));
	if($report == 'true') {
		write_to_access_log_id('push/unsubscribe',$push_receiver);
	}

	$response = (object)array(
		'success'=>true
	);

}else {
	$response = (object)array(
		'success'=>false
	);
}

header('Content-Type: application/json');
echo nice_json_encode($response);