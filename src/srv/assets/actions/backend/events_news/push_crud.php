<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';

require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/push/push_constants.php';

$auth_info= CheckAuthCustomerOrManagerOrViewer();

$array_flip_Notify2DB= null;
function efrsb_event_settings_from_db($f)
{
	global $array_flip_Notify2DB, $Notify2DB;
	if (null==$array_flip_Notify2DB)
		$array_flip_Notify2DB= array_flip($Notify2DB);
	return array('Уведомлять'=> $array_flip_Notify2DB[$f]);
}

function efrsb_event_settings_to_db($f)
{
	global $Notify2DB;
	return $Notify2DB[$f['Уведомлять']];
}

function direction_from_db($f)
{
	global $Directions2DB;
	$array_flip_Directions2DB	= array_flip($Directions2DB);
	return $array_flip_Directions2DB[$f];
}

class push_crud extends Base_crud
{
	function read($id)
	{
		$txt_query= "select 
			id_Push_receiver
			, For_Committee, For_Auction, For_Meeting, For_MeetingWorker, For_MeetingPB, For_CourtDecision, For_Message_after
			, Check_day_off, Time_start, Time_finish, Timezone, Transport, Destination
		from Push_receiver
		where Category=? and id_Manager_Contract_MUser=?";
		$rows= execute_query($txt_query,array('ss',$_GET['Category'],$id));
		$rows_count= count($rows);
		if (0==$rows_count)
		{
			echo nice_json_encode(null);
		}
		else if (1!=$rows_count)
		{
			exit_bad_request("found $rows_count rows for Push_request for Category=$Category with id_Manager_Contract_MUser=$id");
		}
		else
		{
			$row= $rows[0];
			$push_settings= array(
				'id_Push_receiver'=>$row->id_Push_receiver
				,'О_событиях'=> array(
					  'О_проведении_торгов'=> efrsb_event_settings_from_db($row->For_Auction)
					, 'О_собраниях'=> efrsb_event_settings_from_db($row->For_Meeting)
					, 'О_судебном_заседании'=> efrsb_event_settings_from_db($row->For_CourtDecision)
					, 'Уведомлять_об_объявлениях_на_ефрсб'=> (null!=$row->For_Message_after)
				)
				,'Учитывать_выходные'=> $row->Check_day_off ? true : false
				,'Присылать_уведомления'=> array(
					'с'=> DBint2Time($row->Time_start)
					,'до'=> DBint2Time($row->Time_finish)
					,'Часовой_пояс'=> Timezones2DB(array($row->Timezone), true)
				)
				,'Направлять'=> direction_from_db($row->Transport)
				,'ПунктНазначения'=> $row->Destination
			);
			echo nice_json_encode($push_settings);
		}
	}

	function update($id,$push_settings)
	{
		global $Bool2DB, $Time2DB, $Directions2DB;
		$efrsb= (object)$push_settings['О_событиях'];
		$time= (object)$push_settings['Присылать_уведомления'];
		$params= array('ssssssiissss'
			,efrsb_event_settings_to_db($efrsb->О_собраниях)
			,efrsb_event_settings_to_db($efrsb->О_проведении_торгов)
			,efrsb_event_settings_to_db($efrsb->О_собраниях)
			,efrsb_event_settings_to_db($efrsb->О_собраниях)
			,efrsb_event_settings_to_db($efrsb->О_собраниях)
			,efrsb_event_settings_to_db($efrsb->О_судебном_заседании)
			,'true'==$push_settings['Учитывать_выходные']?1:0
			,Time2DB($time->с),Time2DB($time->до),Timezones2DB($time->Часовой_пояс, false)
			,$Directions2DB[$push_settings['Направлять']]
			,$push_settings['ПунктНазначения']
		);
		if (isset($push_settings['id_Push_receiver']))
		{
			$for_message_after= ('true'!=$efrsb->Уведомлять_об_объявлениях_на_ефрсб) ? 'null' : 'if(For_Message_after is null,now(),For_Message_after)';
			$txt_query= "update Push_receiver set 
			For_Committee=?, For_Auction=?, For_Meeting=?, For_MeetingWorker= ?, For_MeetingPB= ?, For_CourtDecision= ?, For_Message_after= $for_message_after,
			Check_day_off= ?, Time_start= ?, Time_finish= ?, Timezone= ?, Transport= ?, Destination= ?
			where id_Push_receiver=?;";
			$params[0].= 's';
			$params[]= $push_settings['id_Push_receiver'];
		}
		else
		{
			$for_message_after= ('true'!=$efrsb->Уведомлять_об_объявлениях_на_ефрсб) ? 'null' : 'now()';
			$receiver_guid = md5(uniqid(rand(), true));
			$txt_query= "
			insert into Push_receiver set 
			For_Committee=?, For_Auction=?, For_Meeting=?, For_MeetingWorker= ?, For_MeetingPB= ?, For_CourtDecision= ?, For_Message_after= $for_message_after,
			Check_day_off= ?, Time_start= ?, Time_finish= ?, Timezone= ?, Transport= ?, Destination= ?, ReceiverGUID='$receiver_guid',
			Category=?, id_Manager_Contract_MUser=?;";
			$params[0].= 'ss';
			$params[]= $_GET['Category'];
			$params[]= $id;
			
		}
		execute_query_no_result($txt_query,$params);
		echo nice_json_encode(array('ok'=>true));
	}
}

$crud= new push_crud();
$crud->process_cmd();

