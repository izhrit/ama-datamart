<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/events_news/manager_debtor_ids.php';
require_once '../assets/libs/events_news/fix_news_rows.php';

$auth_info= CheckAuthCustomerOrManagerOrViewer();
$manager_debtor_ids_url_argument= prepare_manager_debtor_ids_url_argument($auth_info);

function prepare_url($manager_debtor_ids_url_argument)
{
	$rows= !isset($_GET['rows']) ? '10' : $_GET['rows'];
	$page= !isset($_GET['page']) ? '1' : $_GET['page'];

	global $use_efrsb_service_url;
	return "$use_efrsb_service_url/dm-api.php?action=messages-brief&rows=$rows&page=$page&$manager_debtor_ids_url_argument";
}

function get_efrsb_news($url)
{
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_FAILONERROR, 1);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

	$responce= curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if (200!=$httpcode)
		exit_internal_server_error("can not get efrsb brief messages (code $httpcode for url \"$url\")");

	curl_close($curl);

	return $responce;
}

if (null!=$manager_debtor_ids_url_argument)
{
	try
	{
		$url= prepare_url($manager_debtor_ids_url_argument);
		$efrsb_news_json_text= get_efrsb_news($url);
		$efrsb_news= json_decode($efrsb_news_json_text);
		fix_news_rows($efrsb_news);
		echo nice_json_encode($efrsb_news);
		exit();
	}
	catch (Exception $ex)
	{
		write_to_log("For url \"$url\"");
		write_to_log("got responce:");
		write_to_log($efrsb_news_json_text);
		write_to_log('Unhandled exception occurred: ' . get_class($ex) . ' - ' . $ex->getMessage());
	}
}

echo '{"rows":[]}';
