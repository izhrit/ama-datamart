<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

require_once '../assets/libs/events_news/prepare_event.php';
require_once '../assets/libs/events_news/prepare_event_text.php';

if (!isset($_GET['admin']))
{
	$auth_info= CheckAuthCustomerOrManagerOrViewer();
	require_once '../assets/libs/events_news/jqgrid_for_receiver.php';
}
else
{
	$auth_info= CheckAuthAdmin();
	require_once '../assets/libs/events_news/jqgrid_for_admin.php';
}
