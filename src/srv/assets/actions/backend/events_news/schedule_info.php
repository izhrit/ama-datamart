<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/throw.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthCustomerOrManagerOrViewer();

require_once '../assets/libs/events_news/check_schedule_start_end.php';
require_once '../assets/libs/efrsb/sync_Efrsb_event_start_time.php';

$current_date_time= safe_date_create();
$time_later_than= local_events_start_time($current_date_time);

$events= null;
if ('manager'==$auth_info->category)
{
	require_once '../assets/libs/events_news/get_schedule_gc_local.php';
	$events= get_gc_events($auth_info->id_Manager);
}

if ($dstart > $time_later_than)
{
	require_once '../assets/libs/events_news/get_schedule_efrsb_local.php';
	$local_efrsb_events= get_local_efrsb_events();
	if (null==$events)
	{
		$events= $local_efrsb_events;
	}
	else
	{
		$events= array_merge($events, $local_efrsb_events);
		// todo: отсортировать бы конечно..
	}
	$json_txt_response= nice_json_encode($events);
}
else
{
	require_once '../assets/libs/events_news/manager_debtor_ids.php';
	require_once '../assets/libs/events_news/get_schedule_efrsb_remote.php';
	$manager_debtor_ids_url_argument= prepare_manager_debtor_ids_url_argument($auth_info);
	$remote_efrsb_events_json_text= get_remote_efrsb_events_json_text($manager_debtor_ids_url_argument);
	if (null==$events)
	{
		$json_txt_response= $remote_efrsb_events_json_text;
	}
	else
	{
		$remote_efrsb_events= json_decode($remote_efrsb_events_json_text);
		$events= array_merge($events, $remote_efrsb_events);
		// todo: отсортировать бы конечно..
		$json_txt_response= nice_json_encode($events);
	}
}

header('Content-Type: application/json');
echo $json_txt_response;
