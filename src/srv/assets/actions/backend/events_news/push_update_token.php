<?
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/push/push_constants.php';

$auth_info= CheckAuthCustomerOrManagerOrViewer();

try
{
	if (!isset($_GET['Category']) || !isset($_GET['id'] ))
		throw new Exception('skipped mandatory argument Category or id!');

	$id= preg_replace("/[^0-9,]/", "", $_GET['id']);
	if (''==$id)
		exit_bad_request('bad parameter id');

	$Category = $_GET['Category'];
	$Category = $Category[0];

	$_POST_text = file_get_contents("php://input");
	$data = json_decode($_POST_text, true);
	if(empty($data))
		exit_bad_request('there is no data');

	$txt_query = " 
		SELECT pr.Transport FROM Push_receiver pr
		WHERE Category = ? and id_Manager_Contract_MUser = ?;
	";

	$res = execute_query($txt_query,array('ss', $Category, $id));

	if(count($res) <= 0 )
		exit_bad_request('unknown receiver');

	$Transport	   = $res[0]->Transport;
	$DB2Directions = array_flip($Directions2DB);
	$isOk = false;

	if($Transport === 'a') {
		$txt_query = " 
			UPDATE Push_receiver
			SET Destination = ?
			WHERE Category = ? and id_Manager_Contract_MUser = ?;
		";
		
		$res = execute_query_no_result($txt_query, array('sss',$data['token'], $Category, $id));
		$isOk = true;
	}

	header('Content-Type: application/json');
	echo nice_json_encode(array('ok'=>$isOk, 'Направлять'=>$DB2Directions[$Transport]));

}
catch (Exception $exception)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not update token!");
}
