<?
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/post.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/push/send/push_send_method.php';

$auth_info= CheckAuthAdmin();

function test_push_send()
{
	$input_txt= file_get_contents('php://input');
	$input_obj= json_decode($input_txt);
	write_to_log($input_obj);

	$notification= $input_obj->notification;
	$data= $input_obj->data;
	$app_id= substr($input_obj->app_id, 1);

	$sending_responce= null;
	$sending_exception= null;
	try
	{
		$sending_responce= send_notification_to_firebase($app_id, $notification, $data);
	}
	catch (Exception $ex)
	{
		$sending_exception= $ex;
	}

	$response = (object)array('ok'=>(null!=$sending_responce),'status'=> $sending_responce);
	if (null!=$sending_exception)
	{
		$response->exception= (object)array(
			'class'=>get_class($sending_exception)
			,'Message'=>$sending_exception->getMessage()
		);
	}
	echo nice_json_encode($response);
}

function select2_receivers()
{
	$q= CheckMandatoryGET('q');
	$txt_query= "
	(
		select
			concat('m-',m.id_manager) id
			, concat(m.lastName, ' ', left(m.firstName,1), left(m.middleName,1)) text
			, pr.id_Push_receiver, pr.Transport, pr.Destination
		from Manager m
		left join Push_receiver pr on pr.Category='m' and m.id_Manager=pr.id_Manager_Contract_MUser
		where m.lastName like ? limit 20
	) union (
		select 
			concat('c-',c.id_Contract) id
			, c.ContractNumber text
			, pr.id_Push_receiver, pr.Transport, pr.Destination
		from Contract c
		left join Push_receiver pr on pr.Category='c' and c.id_Contract=pr.id_Manager_Contract_MUser
		where c.ContractNumber like ? limit 20
	) union (
		select 
			concat('v-',v.id_MUser) id
			, v.UserName text
			, pr.id_Push_receiver, pr.Transport, pr.Destination
		from MUser v
		left join Push_receiver pr on pr.Category='v' and v.id_mUser=pr.id_Manager_Contract_MUser
		where v.UserName like ? limit 20
	)";

	$rows= execute_query($txt_query,array('sss',"$q%","$q%","$q%"));

	foreach ($rows as $row)
	{
		if (null!=$row->id_Push_receiver)
		{
			$row->data= (object)array(
				'id_Push_receiver'=>$row->id_Push_receiver
				,'Transport'=>$row->Transport
				,'Destination'=>'a'!=$row->Transport ? '' : $row->Destination
			);
		}
		unset($row->id_Push_receiver);
		unset($row->Transport);
		unset($row->Destination);
	}

	echo nice_json_encode(array('results'=>$rows));
}

CheckMandatoryGET_execute_action_variant('cmd',array(
'receivers'=>function(){select2_receivers();}
,'send'=>function(){test_push_send();}
));
