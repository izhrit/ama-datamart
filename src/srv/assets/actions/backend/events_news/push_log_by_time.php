<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthCustomerOrManagerOrViewer();

try
{
	if (!isset($_GET['Category']) || !isset($_GET['id'] ))
		throw new Exception('skipped mandatory argument Category or id!');

	$id= preg_replace("/[^0-9,]/", "", $_GET['id']);
	if (''==$id)
		exit_bad_request('bad parameter id');

	$Category = $_GET['Category'];
	$Category = $Category[0];

	$txt_query = "
	SELECT * 
	  from (
		select 
		pe.Time_pushed Time_pushed
		,e.MessageGUID MessageGUID
		from Pushed_event pe 
		inner join event e on e.id_Event = pe.id_Event
		inner join Push_receiver pr on pr.id_Push_receiver=pe.id_Push_receiver
		inner join Message mes on mes.efrsb_id = e.efrsb_id
		where pr.Category=? and pr.id_Manager_Contract_MUser = ?
		UNION
		select 
		pm.Time_pushed Time_pushed
		,mes.MessageGUID MessageGUID
		from Pushed_message pm
		inner join Push_receiver pr on pr.id_Push_receiver = pm.id_Push_receiver
		inner join Message mes on mes.id_Message = pm.id_Message
		where pr.Category=? and pr.id_Manager_Contract_MUser = ?)
		tu
		where Time_pushed IS NOT NULL
	ORDER BY Time_pushed DESC, MessageGUID;
	";
	$result= execute_query($txt_query,array('ssss',$Category,$id,$Category,$id));


	echo nice_json_encode($result);
}
catch (Exception $exception)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not update token!");
}
