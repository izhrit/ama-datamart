<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/password.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/viewer/alib_viewer.php';

global $category;
$category= $_GET['as'];
$login= $_POST['Login'];
$password= $_POST['Password'];

function LogLogin($ainfo)
{
	require_once '../assets/libs/log/access_log.php';
	global $category;

	if ('sro'==$category)
	{
		write_to_access_log_id('login/sro',$ainfo->id_SRO);
	}
	else
	{
		$log_type= "login/$category";

		if (isset($_GET['platform']))
		{
			$platform= $_GET['platform'];
			if (in_array($platform,array('web','ios','android')))
				$log_type.= '/'.$platform;
		}

		switch ($category)
		{
			case 'manager': write_to_access_log_id($log_type,$ainfo->id_Manager); break;
			case 'customer': write_to_access_log_id($log_type,$ainfo->id_Contract); break;
			case 'viewer': write_to_access_log_id($log_type,$ainfo->id_MUser); break;
			case 'admin': write_to_access_log_id($log_type,$ainfo->id_Admin); break;
		}
	}
}

function OkLogin($ainfo)
{
	global $_SESSION, $category, $auth_info;
	if (!isset($auth_info))
		session_start();
	$auth_info= $ainfo;
	$auth_info->category= $category;
	$_SESSION['auth_info']= $auth_info;
	if (isset($_GET['preload']) && 'procedures'==$_GET['preload'])
	{
		global $fields, $from_where;
		if (isset($auth_info->id_Manager))
			$_GET['id_Manager']= $auth_info->id_Manager;
		if (isset($auth_info->id_Contract))
			$_GET['id_Contract']= $auth_info->id_Contract;
		if (isset($auth_info->id_MUser))
			$_GET['id_MUser']= $auth_info->id_MUser;
		require_once '../assets/actions/backend/procedure/alib_procedures.php';
		prepare_procedures_jquery_fields_from_where();
		$txt_query= "select $fields $from_where;";
		$auth_info->procedures= execute_query($txt_query,array());
		if (isset($_GET['id_Manager']))
		{
			foreach ($auth_info->procedures as $row)
			{
				if (isset($row->Viewers) && null!=$row->Viewers)
					$row->Viewers= explode('|',$row->Viewers);
			}
		}
	}

	echo nice_json_encode($auth_info);

	LogLogin($ainfo);
}
function ChangeManagerPassword($login,$connection)
{
	$txt_query= "select id_Manager, firstName, ManagerEmail from Manager where ManagerEmail=?;";
	$rows= $connection->execute_query($txt_query,array('s',$login));
	if (1==count($rows))
	{
		$u= $rows[0];
		global $datamart_ui_url, $email_settings, $email_postfix;
		$new_password= (true!=$email_settings->enabled) ? 'new_test_pass' : generate_password(8);

		$txt_query= "update Manager set ManagerPassword_Change=?, ManagerPassword_ChangeTime= adddate(now(),interval 1 hour) where id_Manager=?;";
		execute_query_no_result($txt_query,array('ss',$new_password,$u->id_Manager));

		$user_name= $u->firstName;
		$txt_email_body= 
///////////////////////////////////////////////////////////////////////////////
"Здравствуйте, $user_name.

Для использования Витрины данных ПАУ ( $datamart_ui_url )

используйте пароль $new_password

$email_postfix";
///////////////////////////////////////////////////////////////////////////////

		$letter= (object)array(
			'body_txt'=>$txt_email_body
			,'subject'=>'Учётные данные на Витрине данных ПАУ'
		);

		if (false==PostLetter($letter,$u->ManagerEmail,$user_name,'смена пароля АУ',$u->id_Manager))
		{
			$connection->rollback();
			exit_internal_server_error("can not post letter to {$u->id_Manager}");
		}
	}
	$connection->commit();
	echo json_encode($login);
}

function LoginManager($login,$password)
{
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		if (isset($_GET['passwordcmd']) && 'change'==$_GET['passwordcmd'])
		{
			ChangeManagerPassword($login,$connection);
		}
		else
		{
			LoginManager_connection($login,$password,$connection);
		}
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
	
}
function LoginManager_connection($login,$password,$connection)
{
	$txt_query= "select 
		  m.firstName firstName
		, m.lastName lastName
		, m.middleName middleName
		, m.efrsbNumber efrsbNumber
		, m.INN INN
		, m.id_Manager id_Manager
		, m.id_Contract id_Contract
		, m.id_Contract id_Contract
		, c.ContractNumber ContractNumber
		, m.ArbitrManagerID
		, m.HasIncoming
	from Manager m
	left join Contract c on c.id_Contract=m.id_Contract
	where (ManagerEmail=? && Password=?) ||
	(ManagerEmail=?        && ManagerPassword_Change=? && now() < ManagerPassword_ChangeTime && ManagerEmail_Change is null) ||
	(ManagerEmail_Change=? && ManagerPassword_Change=? && now() < ManagerPassword_ChangeTime)
	";
	$rows= $connection->execute_query($txt_query,array('ssssss',$login,$password,$login,$password,$login,$password));
	$count= count($rows);
	if (1!=$count)
	{
		$connection->rollback();
		write_to_log("wrong login \"$login\" password \"$password\" as manager - found $count rows");
		echo 'null';
	}
	else
	{
		$u= $rows[0];
		$u->Email= $login;
		$txt_query= "update Manager set
			  ManagerEmail_Change= null, ManagerEmail_ChangeTime= null, ManagerPassword_Change= null
			, ManagerPassword_ChangeTime= null, ManagerEmail=?, Password=?
		where id_Manager=?;";
		$rows= $connection->execute_query_no_result($txt_query,array('sss',$login,$password,$u->id_Manager));
		$connection->commit();
		OkLogin($u);
	}
}
function LoginSRO($login,$password)
{
	$txt_query= "select 
		  sro.Name
		, sro.id_SRO
		, sro.RegNum
		, sro.INN
		, sro.Name
		, sro.ShortTitle
		, sro.Title
	from efrsb_sro sro
	where SROEmail=? && SROPassword=?;";
	$rows= execute_query($txt_query,array('ss',$login,$password));
	$count= count($rows);
	if (1==$count)
	{
		OkLogin($rows[0]);
	}
	else
	{
		write_to_log("wrong login \"$login\" password \"$password\" as sro - found $count rows");
		echo 'null';
	}
}

function ChangeViewerPassword($login,$connection)
{
	$txt_query= "select id_MUser, UserName, UserEmail from MUser where UserEmail=?;";
	$rows= $connection->execute_query($txt_query,array('s',$login));
	if (1==count($rows))
	{
		$u= $rows[0];
		global $datamart_ui_url, $email_settings, $email_postfix;
		$new_password= (true!=$email_settings->enabled) ? 'new_test_pass' : generate_password(8);

		$txt_query= "update MUser set UserPassword_Change=?, UserPassword_ChangeTime= adddate(now(),interval 1 hour) where id_MUser=?;";
		execute_query_no_result($txt_query,array('ss',$new_password,$u->id_MUser));

		$user_name= $u->UserName;
		$txt_email_body= 
///////////////////////////////////////////////////////////////////////////////
"Здравствуйте, $user_name.

Для использования Витрины данных ПАУ ( $datamart_ui_url )

используйте пароль $new_password

$email_postfix";
///////////////////////////////////////////////////////////////////////////////

		$letter= (object)array(
			'body_txt'=>$txt_email_body
			,'subject'=>'Учётные данные на Витрине данных ПАУ'
		);

		if (false==PostLetter($letter,$u->UserEmail,$user_name,'смена email наблюдателя',$u->id_MUser))
		{
			$connection->rollback();
			exit_internal_server_error("can not post letter to {$u->id_MUser}");
		}
	}
	$connection->commit();
	echo json_encode($login);
}

function fix_MUser_ForMarketplace($u)
{
	global $exposure_recipiet_pro_list;
	foreach ($exposure_recipiet_pro_list as $erp)
	{
		if (isset($erp->id_MUser) && $erp->id_MUser==$u->id_MUser)
		{
			$u->ForMarketplace= true;
			break;
		}
	}
}

function LoginViewer_connection($login,$password,$connection)
{
	$txt_query= "select mu.UserName Имя, mu.UserEmail Email, mu.id_MUser, mu.HasOutcoming,
	exists (select cu.id_ContractUser from ContractUser cu where mu.id_MUser=cu.id_MUser) isEscort
	from MUser mu
	where (mu.UserEmail=?        && mu.UserPassword=?) ||
			(mu.UserEmail=?        && mu.UserPassword_Change=? && now() < mu.UserPassword_ChangeTime && mu.UserEmail_Change is null) ||
			(mu.UserEmail_Change=? && mu.UserPassword_Change=? && now() < mu.UserPassword_ChangeTime)
	;";
	$rows= $connection->execute_query($txt_query,array('ssssss',$login,$password,$login,$password,$login,$password));
	$count= count($rows);
	if (1!=$count)
	{
		$connection->rollback();
		write_to_log("wrong login \"$login\" password \"$password\" as viewer 
(there are $count viewers with these parameters)");
		echo 'null';
	}
	else
	{
		$u= $rows[0];
		$u->Email= $login;
		$txt_query= "update MUser set Confirmed=1
			, UserEmail_Change= null, UserEmail_ChangeTime= null, UserPassword_Change= null, UserPassword_ChangeTime= null
			, UserEmail=?, UserPassword=?
		where id_MUser=?;";
		$rows= $connection->execute_query_no_result($txt_query,array('sss',$login,$password,$u->id_MUser));
		$connection->commit();
		fix_MUser_ForMarketplace($u);
		OkLogin($u);
	}
}

function LoginViewer($login,$password)
{
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		if (isset($_GET['passwordcmd']) && 'change'==$_GET['passwordcmd'])
		{
			ChangeViewerPassword($login,$connection);
		}
		else
		{
			LoginViewer_connection($login,$password,$connection);
		}
	}
	catch (mysqli_sql_exception $ex)
	{
		$connection->rollback();
		ProcessMySqlException_check_duplicate_Viewer_login($ex, $login);
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
}

function LoginCustomer($login,$password)
{
	global $crm2_db_options;
	$crm2db= new Reusable_mysql_connection($crm2_db_options);
	$txt_query= "select 
		  ContractNumber
	from $crm2_db_options->Contract_table_name
	where ContractNumber=? && Password=?;";
	$rows= $crm2db->execute_query($txt_query,array('ss',$login,$password));
	$count= count($rows);
	if (1!=$count)
	{
		write_to_log("wrong login \"$login\" password \"$password\" as customer");
		echo 'null';
	}
	else
	{
		$txt_query= "select 
			  ContractNumber
			, id_Contract
		from Contract
		where ContractNumber=?;";
		$rows= execute_query($txt_query,array('s',$login));
		$count= count($rows);
		if (1==$count)
		{
			OkLogin($rows[0]);
		}
		else
		{
			$txt_query= "insert into Contract set ContractNumber=?";
			$id_Contract= 
				execute_query_get_last_insert_id($txt_query,array('s',$login));
			OkLogin((object)array(
				'id_Contract'=> $id_Contract
				,'ContractNumber'=> $login
			));
		}
	}
}

function LoginAdmin($login,$password)
{
	global $admin_users;
	foreach ($admin_users as $a)
	{
		if ($login==$a->login)
		{
			if ($password!=$a->password)
				break;
			OkLogin((object)array('id_Admin'=> $a->id_Admin, 'Name'=>$a->Name ));
			return;
		}
	}
	write_to_log("wrong login \"$login\" password \"$password\" as admin");
	echo 'null';
}

function TryAutoLogin($login)
{
	global $_SESSION, $category, $auth_info, $_POST;

	if (''!=$login)
		return false;

	if (!isset($_POST['id']))
		return false;

	if (!isset($_SESSION))
		session_start();

	if (!isset($_SESSION['auth_info']))
		return false;

	$auth_info= $_SESSION['auth_info'];

	if ($auth_info->category!=$category)
	{
		write_to_log("wrong auto login as $category");
		write_to_log('auth_info:');
		write_to_log($auth_info);
		return false;
	}

	$id= $_POST['id'];
	$session_id= null;

	switch ($category)
	{
		case 'manager': $session_id= $auth_info->id_Manager; break;
		case 'viewer': $session_id= $auth_info->id_MUser; break;
		case 'customer': $session_id= $auth_info->id_Contract; break;
		case 'sro': $session_id= $auth_info->id_SRO; break;
	}

	if ($id!=$session_id)
	{
		write_to_log("wrong auto login as $category with id=$id");
		write_to_log('auth_info:');
		write_to_log($auth_info);
		return false;
	}

	echo nice_json_encode($auth_info);
	return true;
}

function auto_login_fail($msg,$auth_info= null,$token_info= null)
{
	$txt= "login with auth fail: $msg";
	if (null!=$auth_info)
	{
		$txt.= "\r\nauth information:\r\n";
		$txt.= print_r($auth_info,true);
	}
	if (null!=$token_info)
	{
		$txt.= "\r\ntoken information:\r\n";
		$txt.= print_r($token_info,true);
	}
	write_to_log($txt);
	throw new Exception($txt);
}

function auto_login_ok($ainfo,$category)
{
	global $_SESSION, $auth_info;
	if (!isset($_SESSION))
	{
		write_to_log('session_start');
		session_start();
	}
	$auth_info= $ainfo;
	$auth_info->category= $category;
	$_SESSION['auth_info']= $auth_info;

	echo '{ "ok": true }';

	require_once '../assets/libs/log/access_log.php';
	switch ($category)
	{
		case 'manager': write_to_access_log_id('login/manager/ama',$auth_info->id_Manager); break;
		case 'customer': write_to_access_log_id('login/customer/ama',$auth_info->id_Contract); break;
		case 'viewer': write_to_access_log_id('login/viewer/fa',$ainfo->id_MUser); break;
	}
}

function TryAutoLoginByToken($login)
{
	global $_POST;

	if (''!=$login || !isset($_POST['fauth']))
	{
		return false;
	}
	else
	{
		try
		{
			write_to_log('with fauth');
			require_once '../assets/helpers/realip.php';
			require_once '../assets/libs/auth/license.php';
			require_once '../assets/helpers/sync_encrypt_decrypt.php';

			$auth= stripslashes($_POST['fauth']);
			global $autologin_fauth_options;
			$auth_args= decrypt_auth($auth, $autologin_fauth_options);
			if (null==$auth_args)
				auto_login_fail("can not decrypt fauth \"$auth\"!");

			if (!isset($auth_args->email))
				auto_login_fail('undefined email in fauth!',$auth_args);

			Проверить_актуальность_авторизационных_данных_для_входа($auth_args);

			require_once '../assets/libs/auth/license-fa.php';

			Авторизоваться_по_информации_токена_короткоживущей_лицензиии($auth_args);
		}
		catch (Exception $ex)
		{
			echo '{"error": "Неудачная авторизация"}';
		}
		return true;
	}
}

if (TryAutoLogin($login))
	exit;

if (TryAutoLoginByToken($login))
	exit;

switch ($category)
{
	case 'manager': LoginManager($login,$password); break;
	case 'viewer': LoginViewer($login,$password); break;
	case 'customer': LoginCustomer($login,$password); break;
	case 'admin': LoginAdmin($login,$password); break;
	case 'sro': LoginSRO($login,$password); break;
	default: 
		write_to_log("unknown category \"$category\"");
		echo '{"ok":false,"reason":"Неизвестный тип входа!"}';
}
