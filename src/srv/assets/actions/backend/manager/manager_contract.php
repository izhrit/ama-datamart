<?
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/manager/alib_manager.php';

$auth_info= CheckAuthCustomerOrManager();
CheckMandatoryGET_id('id_Manager');
$id_Manager= intval($_GET['id_Manager']);
CheckAccessToManagerIfManager($auth_info, $id_Manager);

$txt_query= "select
	id_Manager
from Manager
where id_Manager=?";
$manager= execute_query($txt_query, array('i',$id_Manager));

if (null==$manager || 1!=count($manager))
	exit_not_found("can not find manager id_Manager=$id_Manager");

$manager = $manager[0];

CheckMandatoryGET('cmd');
$cmd= $_GET['cmd'];
function RegisterViaPassword($manager, $pass){
	global $crm2_db_options, $auth_info;
	
	$new_contract= $_POST['manager_new_contract'];
	$txt_query= "select
			 c.Id
			,c.ContractNumber
		from $crm2_db_options->Contract_table_name c
		where c.ContractNumber=? && c.Password = ?;";
	$contract= execute_query($txt_query, array('is',$new_contract, $pass));
	if (null != $contract || 1==count($contract)){
		$contract0 = $contract[0];
		$txt_query= "update Manager set id_Contract= ?, id_Contract_Change = NULL where id_Manager=?";
		$affected_rows= execute_query_get_affected_rows($txt_query, array('ii',$contract0->Id, $manager->id_Manager));

		if ($affected_rows == 1){
			$auth_info->id_Contract = $contract0->Id;
			$auth_info->ContractNumber = $new_contract;
			echo '{ "ok": true }';
			exit;
		}else {
			echo '{ "ok": false }';
			exit;
		}
			
	}else {
		echo '{ "ok": false, "reason": "Неверный номер договора и/или пароль" }';
		exit;
	}
}
function CustomerAcceptRequest($manager){
	global $crm2_db_options, $auth_info;

	$new_contract= $_POST['manager_new_contract'];

	$txt_query= "select
			 c.Id
			,c.ContractNumber
		from $crm2_db_options->Contract_table_name c
		where c.ContractNumber=?;";
	$contract= execute_query($txt_query, array('i',$new_contract));

	if (null != $contract || 1==count($contract)){
		$contract0 = $contract[0];
		$txt_query= "update Manager set id_Contract= ?, id_Contract_Change = NULL where id_Manager=?";
		$affected_rows= execute_query_get_affected_rows($txt_query, array('ii',$contract0->Id, $manager->id_Manager));

		if ($affected_rows == 1){
			echo '{ "ok": true }';
			exit;
		}else {
			echo '{ "ok": false }';
			exit;
		}
			
	}else {
		echo '{ "ok": false, "reason": "Данный договор отсутствует в базе данных" }';
		exit;
	}
}
function RegisterViaRequest($manager){
	global $crm2_db_options, $auth_info;

	$new_contract= $_POST['manager_new_contract'];

	$txt_query= "select
			 c.Id
			,c.ContractNumber
		from $crm2_db_options->Contract_table_name c
		where c.ContractNumber=?;";
	$contract= execute_query($txt_query, array('i',$new_contract));

	if (null != $contract || 1==count($contract)){
		$contract0 = $contract[0];
		$txt_query= "update Manager set id_Contract= NULL, id_Contract_Change = ? where id_Manager=?";
		$affected_rows= execute_query_get_affected_rows($txt_query, array('ii',$contract0->Id, $manager->id_Manager));

		if ($affected_rows == 1){
			$auth_info->id_Contract = null;
			$auth_info->ContractNumber = null;
			echo '{ "ok": true }';

			exit;
		}else {
			echo '{ "ok": false }';
			exit;
		}
			
	}else {
		echo '{ "ok": false, "reason": "Данный договор отсутствует в базе данных" }';
		exit;
	}
}
function ContractRegister($manager)
{
	global $auth_info;

	if(!empty($_POST['manager_contract_password'])){
		RegisterViaPassword($manager, $_POST['manager_contract_password']);
	}else if($auth_info->category == 'customer'){
		CustomerAcceptRequest($manager);
	}else {
		RegisterViaRequest($manager);
	}
}

function ContractUnRegister($manager)
{
	global $auth_info;

	$txt_query= "update Manager set id_Contract= NULL, id_Contract_Change= NULL where id_Manager=?";
	$affected_rows= execute_query_get_affected_rows($txt_query, array('i',$manager->id_Manager));
	if($affected_rows == 1){
		$auth_info->id_Contract = null;
		$auth_info->ContractNumber = null;
		echo '{ "ok": true }';
		exit;
	}
	echo '{ "ok": false, "reason": "АУ уже выписан из договора!" }';
}

function ContractDecline($manager)
{

	$txt_query= "update Manager set id_Contract= NULL, id_Contract_Change= NULL where id_Manager=?";
	$affected_rows= execute_query_get_affected_rows($txt_query, array('i',$manager->id_Manager));
	if($affected_rows == 1){
		echo '{ "ok": true }';
		exit;
	}
	echo '{ "ok": false, "reason": "АУ уже выписан из договора!" }';
}



switch ($cmd)
{
	case 'register': ContractRegister($manager); break;
	case 'unregister': ContractUnRegister($manager); break;
	case 'decline': ContractDecline($manager); break;
	default: exit_bad_request("bad _GET['cmd']=$cmd:");
}
