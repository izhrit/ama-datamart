<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/manager/alib_manager.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManager();

function CopyField($from,$to,$field_name)
{
	if (isset($from->$field_name))
		$to->$field_name = $from->$field_name;
}

function PrepareField($to,$field_name)
{
	return $to->$field_name= isset($to->$field_name) ? $to->$field_name : (object)array();
}

function CopyExtraParams($from,$to)
{
	$from= (object)$from;
	$to= (object)$to;

	CopyField($from,$to,'Телефон');
	if (isset($from->Приёмное_время))
	{
		$from_t= (object)$from->Приёмное_время;
		$to_t= PrepareField($to,'Приёмное_время');
		CopyField($from_t,$to_t,'С');
		CopyField($from_t,$to_t,'До');
	}
	if (isset($from->Адрес))
	{
		$from_t= (object)$from->Адрес;
		$to_t= PrepareField($to,'Адрес');
		CopyField($from_t,$to_t,'Рабочий');
	}
}

function CloneExtraParams($from)
{
	$to= (object)array();
	CopyExtraParams($from,$to);
	return $to;
}

class Manager_crud extends Base_crud
{
	function read($id_Manager)
	{
		global $auth_info;
		CheckAccessToManagerIfManager($auth_info, $id_Manager);
		$txt_query= "
			select 
				  m.id_Manager
				,m.firstName
				,m.lastName
				,m.middleName
				,m.efrsbNumber
				,m.INN
				,IF(m.id_Contract_Change is null, c.ContractNumber, null) ContractNumber
				,IF(m.id_Contract_Change is null, null, c.ContractNumber) ContractNumberToChange	
				,m.ManagerEmail Email
				,(Password is not null) PasswordEnabled
				,uncompress(ExtraParams) ExtraParams
			from Manager m
			left join Contract c on c.id_Contract=m.id_Contract OR m.id_Contract_Change = c.id_Contract";
		
		if	(null != $auth_info->id_Contract) {
			$where = " where m.id_Manager=? && ( m.id_Contract=? OR m.id_Contract_Change = ?);";
			$txt_query .= $where;
			$rows= execute_query($txt_query,array('sii',$id_Manager,$auth_info->id_Contract,$auth_info->id_Contract));
		}
		else 
		{
			$where = " where m.id_Manager=?";
			$txt_query .= $where;
			$rows= execute_query($txt_query,array('s',$id_Manager));
		}
		
		
		$count= count($rows);
		if (1!=$count)
		{
			exit_not_found("can not find manager id_Manager=$id_Manager (found $count rows)");
		}
		else
		{
			$manager= $rows[0];
			if (null!=$manager->ExtraParams)
				CopyExtraParams(json_decode($manager->ExtraParams),$manager);
			unset($manager->ExtraParams);
			echo nice_json_encode($manager);
		}
	}

	function update($id_Manager,$manager)
	{
		global $auth_info;
		CheckAccessToManagerIfManager($auth_info, $id_Manager);

		$this->check_Object_mandatory('Email');
		$email= $manager['Email'];

		$extra_params= CloneExtraParams($manager);

		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		try
		{
			$txt_query= "update Manager set ManagerEmail=?, ExtraParams=compress(?) where id_Manager=? && id_Contract=?;";
			$affected_rows= execute_query_get_affected_rows($txt_query,
				array('ssii',$email,json_encode($extra_params),$id_Manager,$auth_info->id_Contract));
			if (1==$affected_rows)
			{
				echo '{ "ok": true }';
			}
			else if (0==$affected_rows)
			{
				echo '{ "ok": true, "details": "changed nothing" }';
			}
			else
			{
				exit_internal_server_error("affected $affected_rows rows for id_Manager=$id_Manager as update");
			}
		}
		catch (mysqli_sql_exception $ex)
		{
			ProcessMySqlException_check_duplicate_login($ex, $email);
		}
	}
}

$crud= new Manager_crud();
$crud->process_cmd();
