<?
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/requests/request_crud_lib.php';


global $auth_info;
$auth_info= CheckAuthSRO();

class manager_info_crud extends Base_crud
{
	function checkObject()
	{
		self::check_Object_mandatory('Протокол');
		if(!isset($_POST['Протокол']['Номер'])) {
			exit_bad_request("skipped _POST['Протокол']['Номер']");
		}
		if(!isset($_POST['Протокол']['Дата'])) {
			exit_bad_request("skipped _POST['Протокол']['Дата']");
		}
	}

	function date_time($value)
	{
		if ('' === trim($value)) return null;
		$mask_from= 'd.m.Y';
		$mask_to= 'Y-m-d';
		$date_time_value= date_create_from_format($mask_from,$value);
		return date_format($date_time_value,$mask_to);
	}

	function read($id_Manager)
	{
		global $auth_info;
		$id_Manager= CheckMandatoryGET_id('id');

		$txt_query = "select 
			id_Manager
			, ProtocolNum
			, DATE_FORMAT(ProtocolDate, '%d.%m.%Y') ProtocolDate
			from manager
			where id_Manager=? && id_SRO={$auth_info->id_SRO}";
		$params = array('i', $id_Manager);

		$rows = execute_query($txt_query, $params);
		$count_rows = count($rows);

		if(1!=$count_rows) {
			exit_not_found("found $count_rows managers with id_Manager={$id_Manager}");
		}

		$manager_info = $rows[0];

		$result = array(
			'id_Manager' => $manager_info->id_Manager
			, 'Протокол' => array(
				'Номер' => $manager_info->ProtocolNum
				, 'Дата' => $manager_info->ProtocolDate
			)
		);

		echo nice_json_encode($result);
	}

	function update($id_Manager,$manager_info)
	{
		global $auth_info;
		$id_Manager= CheckMandatoryGET_id('id');
		self::checkObject();

		$txt_query = "update Manager set
			ProtocolNum=?
			, ProtocolDate=?
			where id_Manager=? and id_SRO={$auth_info->id_SRO}";
		$params = array('ssi', 
			$manager_info['Протокол']['Номер'],
			self::date_time($manager_info['Протокол']['Дата']),
			$id_Manager
		);

		$affected_rows = execute_query_get_affected_rows($txt_query, $params);
		if ($affected_rows>=1)
		{
			echo '{ "ok": true }';
		}
		else if (0==$affected_rows)
		{
			echo '{ "ok": true, "affected": 0 }';
		}
		else
		{
			exit_bad_request("affected $affected_rows when update Manager id_Manager=$id_Manager");
		}
	}
}

$crud= new manager_info_crud();
$crud->process_cmd();