<?
require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/post.php';
require_once '../assets/helpers/realip.php';
require_once '../assets/helpers/time.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';

require_once '../assets/libs/cryptoapi.php';

$dn_names_to_fix = array(
	'OID.1.2.643.100.3'=>'ИНН'
);

function mb_trim($string, $trim_chars = '\s')
{
	return preg_replace('/^['.$trim_chars.']*(?U)(.*)['.$trim_chars.']*$/u', '\\1',$string);
}

function GetDnFields($text)
{
	$res= array();
	$parts= explode(',',$text);
	foreach ($parts as $part)
	{
		$pos= mb_strpos($part,'=');
		$name= mb_trim(mb_substr($part,0,$pos));

		global $dn_names_to_fix;
		if (isset($dn_names_to_fix[$name]))
			$name= $dn_names_to_fix[$name];

		$value= mb_substr($part,$pos+1);
		$res[$name]= $value;
	}
	return (object)$res;
}
function prepare_запрос_доступа($cert, $manager_fields)
{
	$current_time= safe_date_create();
	return (object)array(
		'время'=>date_format($current_time,'Y-m-d\TH:i:s')
		,'сертификат'=>(object)array(
				'IssuerName'=>$cert['Issuer']
			,'SerialNumber'=>$cert['SerialNumber']
		)
		,'роль'=>'АУ'
		,'АУ'=>(object)array(
			'Фамилия'=>$manager_fields->LastName
			,'Имя'=>$manager_fields->FirstName
			,'Отчество'=>$manager_fields->MiddleName
			,'ИНН'=>$manager_fields->INN
			,'Рег_номер_ЕФРСБ'=>$manager_fields->RegNum
		)
		,'IP'=>getRealIPAddr()
	);
}
function prepare_token($token_fields)
{
	ob_start();
	include 'auth_request.php';
	$result = ob_get_contents();
	ob_end_clean();

	return $result;
}
function prepare_token_fields($запрос_доступа_fields, $manager_fields)
{
	$отметка_сайта= null;
	global $auth_token_options;
	if (!isset($_GET['open']) || (!isset($auth_token_options->enable_open) || !$auth_token_options->enable_open))
	{
		$encrypted_token_fields= sync_encrypt(json_encode($запрос_доступа_fields),$auth_token_options);
		$отметка_сайта= md5($encrypted_token_fields);
	}else {
		$отметка_сайта = 1;
	}
	$на_витрину= array('Отметка_сайта'=>$отметка_сайта);
	$txt_query= 'insert into ManagerCertAuth set id_Manager=?, Body=compress(?), TimeStarted=now();';
	$id_ManagerCertAuth= execute_query_get_last_insert_id($txt_query,array('ss',$manager_fields->id_Manager,json_encode($на_витрину)));
	return (object)array(
		'id_Manager'=>$manager_fields->id_Manager
		,'ArbitrManagerID'=>$manager_fields->ArbitrManagerID
		,'id_ManagerCertAuth'=>$id_ManagerCertAuth
		,'Отметка_сайта'=>$отметка_сайта
	);
}

function GetTokenToSignForManager($cert,$fields)
{
	session_start();

	write_to_log('found manager:');
	write_to_log($fields);

	$запрос_доступа_fields= prepare_запрос_доступа($cert, $fields);
	global $base_apps_url;
	$token_fields= (object)array(
		'запрос_доступа'=>$запрос_доступа_fields
		,'на_сайт'=>array(
			$base_apps_url=>prepare_token_fields($запрос_доступа_fields, $fields)
		)
	);
	
	$token= prepare_token($token_fields);
	$to_session= (object)array('token_fields'=>$token_fields,'token'=>$token);
	$to_return= array('token'=>array('token'=>$token));

	$_SESSION['auth_token_info']= $to_session;

	write_to_log('returns:');
	write_to_log($to_return);

	echo nice_json_encode($to_return);
}
function GetTokenToSign($cert)
{
	write_to_log('cert:');
	write_to_log($cert);

	$subject= GetDnFields($cert['Subject']);
	
	if (!isset($subject->ИНН))
		exit_bad_request("skipped ИНН in Subject!",$cert);

	write_to_log('ИНН:'.$subject->ИНН);
	$txt_query= "select 
		  m.firstName FirstName
		, m.lastName LastName
		, m.middleName MiddleName
		, m.efrsbNumber RegNum
		, m.INN INN
		, m.id_Manager id_Manager
		, m.id_Contract id_Contract
		, c.ContractNumber ContractNumber
		, m.ArbitrManagerID
		, m.HasIncoming
	from Manager m
	left join Contract c on c.id_Contract=m.id_Contract
	where INN=?";
	$rows= execute_query($txt_query,
		array('s',$subject->ИНН));
	$count_rows= count($rows);
	
	if (1==$count_rows)
	{
		GetTokenToSignForManager($cert,$rows[0]);
	}
	else
	{
		$rows= execute_query(
		'select id_Manager, ArbitrManagerID, LastName, FirstName, MiddleName, INN, RegNum from	efrsb_manager where INN=?',
		array('s',$subject->ИНН));
		$count_rows= count($rows);

		if (1!=$count_rows)
		{
			echo '';
		}
		else 
		{
			$txt_query= "insert into Manager set efrsbNumber=?, INN=?, firstName=?, lastName=?, middleName=?, ArbitrManagerID=?;";
			$id_Manager = execute_query_get_last_insert_id($txt_query,array('ssssss',$rows[0]->RegNum, $rows[0]->INN, $rows[0]->FirstName, $rows[0]->LastName, $rows[0]->MiddleName, $rows[0]->ArbitrManagerID));
			$rows[0]->id_Manager = $id_Manager;
			GetTokenToSignForManager($cert,$rows[0]);
		}
	}
}
function CheckTokenSignature($token,$base64_encoded_signature)
{
	global $base_cryptoapi_url, $base_cryptoapi_auth;
	$cryptoapi= new CryptoAPI((object)array(
		'base_url'=>$base_cryptoapi_url
		,'auth'=>"datamart-from-repo"
	));
	$result= $cryptoapi->VerifySignatureFromCapicom((object)array(
		'document'=>$token 
		,'base64_encoded_signature'=>$base64_encoded_signature
	));
	write_to_log_named('CheckTokenSignature result',$result);
	return $result->signature_checked;
}

function AuthByTokenSignature($data_signature)
{
	global $_SESSION, $auth_info;
	write_to_log('data_signature:');
	write_to_log($data_signature);

	session_start();
	
	if (!isset($_SESSION['auth_token_info']))
	{
		exit_bad_request("undefined session auth_token_info");
	}
	else
	{
		$token_info= $_SESSION['auth_token_info'];
		if (null==$token_info)
			exit_bad_request("can not parse input: $token_info");
		write_to_log_named('на входе',$token_info);

		$token = $token_info->token;
		$base64_encoded_signature= $data_signature['base64_encoded_signature'];

		if (!CheckTokenSignature($token,$base64_encoded_signature))
		{
			exit_unauthorized("can not check signature!");
		}
		else
		{
			//write_to_log_named('session auth_token_info',$token_info);
			$token_fields= $token_info->token_fields;

			$АУ= $token_fields->запрос_доступа->АУ;
			global $base_apps_url;
			$datamart_params= $token_fields->на_сайт[$base_apps_url];
			$id_ManagerCertAuth= $datamart_params->id_ManagerCertAuth;

			$txt_query= 'select uncompress(Body) body from ManagerCertAuth where id_ManagerCertAuth=?;';
			$rows= execute_query($txt_query,array('s',$id_ManagerCertAuth));
			$count_rows= count($rows);

			if (1!=$count_rows)
			{
				exit_bad_request("found $count_rows ManagerCertAuth with id_ManagerCertAuth=$id_ManagerCertAuth");
			}
			else
			{
			
				$fields= json_decode($rows[0]->body);
				$txt_query= 'update ManagerCertAuth set Auth=? where id_ManagerCertAuth=?;';
				execute_query_no_result($txt_query,array('ss',$fields->Отметка_сайта,$id_ManagerCertAuth));

				$txt_query= "select 
						m.firstName firstName
					, m.lastName lastName
					, m.middleName middleName
					, m.efrsbNumber efrsbNumber
					, m.INN INN
					, m.id_Manager id_Manager
					, m.id_Contract id_Contract
					, c.ContractNumber ContractNumber
					, m.ArbitrManagerID
					, m.HasIncoming
				from Manager m
				left join Contract c on c.id_Contract=m.id_Contract
				where m.id_Manager=?";
				$rows= execute_query($txt_query,
					array('s',$datamart_params->id_Manager));

					$auth_info = $rows[0];
					$auth_info->category = 'manager';
					$auth_info->auth = $fields->Отметка_сайта;
					$auth_info->id_ManagerCertAuth = $id_ManagerCertAuth;

				$_SESSION['auth_info']= $auth_info;
				write_to_log_named('return',$auth_info);
				echo nice_json_encode($auth_info);
			}
		}
	}
}
function Logout()
{
	$id_ManagerCertAuth= CheckMandatoryGET_id('id');

	$txt_query= 'delete from ManagerCertAuth where id_ManagerCertAuth=?;';
	$affected_rows= execute_query_get_affected_rows($txt_query,array('s',$id_ManagerCertAuth));
	if (1!=$affected_rows)
		exit_bad_request("$affected_rows rows affected on delete from ManagerCertAuth where id_ManagerCertAuth=$id_ManagerCertAuth");

	session_start();
	global $_SESSION;
	if (isset($_SESSION['auth_token_info']))
	{
		$_SESSION['auth_token_info']= null;
		unset($_SESSION['auth_token_info']);
	}
	if (isset($_SESSION['auth_info']))
	{
		$_SESSION['auth_info']= null;
		unset($_SESSION['auth_info']);
	}
	session_destroy();
}

CheckMandatoryGET('cmd');

switch ($_GET['cmd'])
{
	case 'get-token-to-sign':
		write_to_log('cmd:get-token-to-sign');
		$data_to_auth= safe_POST($_POST);
		CheckMandatoryPOST('Subject',$data_to_auth);
		CheckMandatoryPOST('Issuer',$data_to_auth);
		CheckMandatoryPOST('SerialNumber',$data_to_auth);
		GetTokenToSign($data_to_auth);
		break;
	case 'auth-by-token-signature':
		write_to_log('cmd:auth-by-token-signature');
		$data_signature= safe_POST($_POST);
		CheckMandatoryPOST('base64_encoded_signature',$data_signature);
		AuthByTokenSignature($data_signature);
		break;
	case 'logout':
		write_to_log('cmd:logout');
		Logout();
		break;
}