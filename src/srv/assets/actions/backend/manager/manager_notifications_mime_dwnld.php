<?php

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/time.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthSRO();
$id_SentEmail= intval($_GET['id']);

$txt_query= "
	select
		  uncompress(se.Message) Message
		, octet_length(uncompress(se.Message)) length
		, uncompress(se.ExtraParams) ExtraParams
		, se.TimeSent
	from SentEmail se
	where se.id_SentEmail = ?;";
$rows= execute_query($txt_query,array('i',$id_SentEmail));
$count_rows= count($rows);

if (1!=$count_rows)
	exit_not_found("found $count_rows rows for id_SentEmail=$id_ManagerDocument");

$row0= $rows[0];
$ExtraParams = json_decode($row0->ExtraParams);
$subject = $ExtraParams->Subject;
$time_sent = $row0->TimeSent;
$content = $row0->Message; 
$content_length = $row0->length;

if (!isset($_GET['no-headers']))
{
	header("Content-type: application/octet-stream");
	header("Content-Length: $content_length");
	header("Content-Disposition: attachment; filename=$subject-$time_sent.eml");
}
echo $content;
exit;
