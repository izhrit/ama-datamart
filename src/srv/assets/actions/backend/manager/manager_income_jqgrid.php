<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthManager();

CheckMandatoryGET_id('id_Manager');
$id_Manager= intval($_GET['id_Manager']);

$fields= "
	 md.id_MData id_MData
	,md.MData_Type MData_Type
	,mu.UserName FromUser
	,d.Name ForDebtor
	,md.publicDate publicDate
";

$from_where= " from MData md 
inner join ManagerUser mu on md.id_ManagerUser=mu.id_ManagerUser
left join Debtor d on d.id_Debtor=md.id_Debtor
where mu.id_Manager=$id_Manager ";

$filter_rule_builders= array(
	'MData_Type'=>'std_filter_rule_builder'
	,'FromUser'=>prep_std_filter_rule_builder_for_expression('mu.UserName')
	,'ForDebtor'=>prep_std_filter_rule_builder_for_expression('d.Name')
	,'publicDate'=>'std_filter_rule_builder'
);
execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
