<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/manager/alib_manager.php';

$auth_info= CheckAuthCustomerOrManager();
CheckMandatoryGET_id('id_Manager');
CheckMandatoryGET('email');
$id_Manager= intval($_GET['id_Manager']);
CheckAccessToManagerIfManager($auth_info, $id_Manager);

$txt_query= "select
	ManagerEmail
	,Password
	,firstName
	,lastName
	,middleName
	,id_Contract
	,id_Manager
from Manager
where id_Manager=?";

$params = array('i',$id_Manager);

$id_Contract = $auth_info->id_Contract;

if (null!=$id_Contract) {
	$txt_query.= ' && id_Contract=?';
	$params[0].= 'i';
	$params[]= $id_Contract ;
}

$rows= execute_query($txt_query, $params);

if (null==$rows || 1!=count($rows))
	exit_not_found("can not find manager id_Manager=$id_Manager");

$manager= $rows[0];
$manager->ManagerEmail= $_GET['email'];

function StorePassword($connection,$manager,$password)
{
	$txt_query= "update Manager set Password= ?, ManagerEmail= ? where id_Manager=?";
	$params = array('ssi',$password,$manager->ManagerEmail,$manager->id_Manager);

	if (null!=$manager->id_Contract) {
		$txt_query.= ' && id_Contract=?';
		$params[0].= 'i';
		$params[]= $manager->id_Contract ;
	}

	execute_query_no_result($txt_query, $params);
}

function PrepareNewPasswordLetter($manager,$password)
{
	$letter= (object)array('subject'=>'Учётные данные на Витрине данных ПАУ');
	ob_start();
	include "../assets/actions/backend/manager/letters/password_new.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();
	return $letter;
}

function PrepareBlockPasswordLetter($manager)
{
	$letter= (object)array('subject'=>'Учётные данные на Витрине данных ПАУ');
	ob_start();
	include "../assets/actions/backend/manager/letters/password_block.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();
	return $letter;
}

function PostLetterToManager($letter_data,$manager,$EmailType_descr)
{
	$manager_name= $manager->lastName.' '.$manager->firstName.$manager->middleName;
	return PostLetter($letter_data,$manager->ManagerEmail,$manager_name,
					  $EmailType_descr,$manager->id_Manager);
}

function ChangePassword($manager)
{

	global $email_settings;
	$new_password= (true!=$email_settings->enabled) ? 'new_test_pass' : generate_password(8);
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		StorePassword($connection, $manager,$new_password);
	}
	catch (mysqli_sql_exception $ex)
	{
		$connection->rollback();
		ProcessMySqlException_check_duplicate_login($ex, $manager->ManagerEmail);
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
	$letter= PrepareNewPasswordLetter($manager,$new_password);
	if (false==PostLetterToManager($letter,$manager,'смена пароля АУ'))
	{
		$connection->rollback();
		exit_internal_server_error("Can not send email to $manager->ManagerEmail");
	}
	else
	{
		$connection->commit();
		echo '{ "ok": true }';
		exit;
	}
}

function BlockPassword($manager)
{
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		StorePassword($connection,$manager,null);
	}
	catch (mysqli_sql_exception $ex)
	{
		$connection->rollback();
		ProcessMySqlException_check_duplicate_login($ex, $manager->ManagerEmail);
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
	$letter= PrepareBlockPasswordLetter($manager);
	if (false==PostLetterToManager($letter,$manager,'блокирование АУ'))
	{
		$connection->rollback();
		exit_internal_server_error("Can not send email to $manager->ManagerEmail");
	}
	else
	{
		$connection->commit();
		echo '{ "ok": true }';
		exit;
	}
	
}

CheckMandatoryGET('cmd');
$cmd= $_GET['cmd'];
switch ($cmd)
{
	case 'change': ChangePassword($manager); break;
	case 'block': BlockPassword($manager); break;
	default: exit_bad_request("bad _GET['cmd']=$cmd:");
}

