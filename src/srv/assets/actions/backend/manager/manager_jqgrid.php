<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthCustomerOrSRO();

function manager_jqgrid_for_Customer()
{
	global $auth_info;

	$id_Contract= CheckMandatoryGET_id('id_Contract');
	CheckCustomerAccessToContract($auth_info, $id_Contract);

	$fields= '
		 m.id_Manager id_Manager
		,m.firstName firstName
		,m.lastName lastName
		,m.middleName middleName
		,m.efrsbNumber efrsbNumber
		,m.INN INN
		,IF(m.id_Contract is not null, "Прописан", "На прописку") registration
	';

	$from_where= " from Manager m where (m.id_Contract=$id_Contract OR m.id_Contract_Change= $id_Contract)";

	$filter_rule_builders= array(
		'firstName'=>'std_filter_rule_builder'
		,'lastName'=>'std_filter_rule_builder'
		,'middleName'=>'std_filter_rule_builder'
		,'efrsbNumber'=>'std_filter_rule_builder'
		,'INN'=>'std_filter_rule_builder'
	);
	$fields_in_db = array(
		'id_Manager'=> 'id_Manager'
		,'firstName'=> 'firstName'
		,'lastName'=> 'lastName'
		,'middleName'=> 'middleName'
		,'efrsbNumber'=> 'efrsbNumber'
		,'INN'=> 'INN'
	);
	execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders,$order='',$fields_in_db);
}

function manager_jqgrid_for_SRO()
{
	global $auth_info;

	$id_SRO= CheckMandatoryGET_id('id_SRO');

	CheckAccessToSROIfSRO($auth_info, $id_SRO);

	$fields= "
		 mm.id_Manager id_Manager
		,m.FirstName firstName
		,m.LastName lastName
		,m.MiddleName middleName
		,m.RegNum efrsbNumber
		,m.ArbitrManagerID ArbitrManagerID
		,m.INN INN
		,(mm.Password is not null) PasswordEnabled
		,(mm.ManagerEmail_Change is not null && mm.ManagerPassword_Change is not null) PasswordSent
		,if((mm.ManagerEmail_Change is not null && mm.ManagerPassword_Change is not null),'Отправлен',
			if((mm.Password is not null),'Получен','Еще не запрашивался')) status
	";

	$from_where= " from efrsb_manager m
	inner join efrsb_sro sro on m.SRORegNum=sro.RegNum
	left join Manager mm on m.ArbitrManagerID=mm.ArbitrManagerID
	where sro.id_SRO=$id_SRO ";

	$filter_rule_builders= array(
		'firstName'=>function($l,$rule)
		{
			$data= mysqli_real_escape_string($l,$rule->data);
			return " and m.FirstName like '$data%' ";
		}
		,'lastName'=>function($l,$rule)
		{
			$data= mysqli_real_escape_string($l,$rule->data);
			return " and m.LastName like '$data%' ";
		}
		,'middleName'=>function($l,$rule)
		{
			$data= mysqli_real_escape_string($l,$rule->data);
			return " and m.MiddleName like '$data%' ";
		}
		,'efrsbNumber'=>function($l,$rule)
		{
			$data= mysqli_real_escape_string($l,$rule->data);
			return " and m.RegNum like '$data%' ";
		}
		,'status'=>function($l,$rule)
		{
			$data= mysqli_real_escape_string($l,$rule->data);
			return " and if((mm.ManagerEmail_Change is not null && mm.ManagerPassword_Change is not null),'Отправлен',
			if((mm.Password is not null),'Получен','Еще не запрашивался')) like '$data%' ";
		}
		,'ArbitrManagerID'=>'std_filter_rule_builder'
		,'INN'=>function($l,$rule)
		{	
			$data= mysqli_real_escape_string($l,$rule->data);
			return " and m.INN like '$data%' ";
		}
	);
	execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
}

switch ($auth_info->category)
{
	case 'sro': manager_jqgrid_for_SRO(); break;
	case 'customer': manager_jqgrid_for_Customer(); break;
}