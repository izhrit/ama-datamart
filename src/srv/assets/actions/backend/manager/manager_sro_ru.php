<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/manager/alib_manager.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/helpers/password.php';

global $auth_info;
$auth_info= CheckAuthSRO();

function CheckExistEmail($ArbitrManagerId, $email)
{
	$rows= execute_query("select id_Manager from Manager where (ManagerEmail=? OR ManagerEmail_Change = ?) AND ArbitrManagerId!=?;"
	,array('ssi',$email,$email,$ArbitrManagerId));
	$count_rows= count($rows);
	if (0!=$count_rows)
	{
		echo '{ "ok": false, "reason":"В базе уже есть Арбитражный управляющий с таким адресом электронной почты!" }';
		exit;
	}
}
class Manager_crud extends Base_crud
{

	function PrepareNewManagerLetter($manager,$password)
	{
		$manager = (object)$manager;
		$letter= (object)array('subject'=>'Учётные данные на Витрине данных ПАУ');
		ob_start();
		include "../assets/actions/backend/manager/letters/password_new.php";
		$letter->body_txt= ob_get_contents();
		ob_end_clean();
		return $letter;
	}

	function GetEmailFromEfrsb($field_value)
	{
		if (null==$field_value)
			return null;

		global $use_efrsb_service_url;
		$url= $use_efrsb_service_url.'/dm-api.php?action=manager-info&ArbitrManagerID='.$field_value;

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_URL,$url);
		$result= curl_exec($curl);
		$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);

		
		$result = json_decode($result);
		if (null!=$result)
		{
			$parts= explode('|',$result->email_id_Message);
			$email= $parts[0];
			return $email;
		}else {
			return null;
		}
	}

	function read($ArbitrManagerId)
	{
		global $auth_info;
		$txt_query= "
			select  m.id_Manager
				,m.firstName
				,m.lastName
				,m.middleName
				,m.efrsbNumber
				,m.INN
				,m.ManagerEmail Email
				,m.ManagerEmail_Change EmailChange
				,e.EMail EfrsbEmail
				,m.ArbitrManagerID ArbitrManagerID
				,s.RegNum id_SRO
				,(m.Password is not null) PasswordEnabled
				,(m.ManagerEmail_Change is not null && m.ManagerPassword_Change is not null) PasswordSent
				from Manager m
				left join efrsb_manager e on m.ArbitrManagerId = e.ArbitrManagerId 
				left join efrsb_sro s on s.id_SRO = m.id_SRO 
				where m.ArbitrManagerId = ?";

		$rows= execute_query($txt_query,array('s',$ArbitrManagerId));
		
		$count= count($rows);
		if (1!=$count)
		{
			$txt_query= "
				select  e.id_Manager
					,e.FirstName firstName
					,e.LastName lastName
					,e.MiddleName middleName
					,e.RegNum efrsbNumber
					,e.INN
					,e.EMail EfrsbEmail
					,e.ArbitrManagerID ArbitrManagerID
					,e.SRORegNum id_SRO
					,false PasswordEnabled
					,false PasswordSent
					from efrsb_manager e
					where e.ArbitrManagerId = ?";

			$rows= execute_query($txt_query,array('s',$ArbitrManagerId));
			
			$count= count($rows);

			if(1!=$count)
			{
				exit_not_found("can not find manager ArbitrManagerId=$ArbitrManagerId (found $count rows)");
			}
			else
			{
				$manager= $rows[0];
				$managerEmail= self::GetEmailFromEfrsb($manager->ArbitrManagerID);
				$manager->EfrsbEmail = $managerEmail;
				echo nice_json_encode($manager);
			}
		}
		else
		{
			$manager= $rows[0];
			$managerEmail= self::GetEmailFromEfrsb($manager->ArbitrManagerID);
			$manager->EfrsbEmail = $managerEmail;
			echo nice_json_encode($manager);
		}
	}

	function update($ArbitrManagerId, $manager)
	{

		global $auth_info, $email_settings;
		CheckAccessToSROIfSROByRegNum($auth_info, $manager['id_SRO']);
		$this->check_Object_mandatory('Email');
		$email= $manager['Email'];
		CheckExistEmail($ArbitrManagerId, $email);

		$txt_query= "select m.id_Manager from Manager m where m.ArbitrManagerId = ?";

		$rows= execute_query($txt_query,array('s',$ArbitrManagerId));
		$count= count($rows);

		$new_password= (true!=$email_settings->enabled) ? 'new_test_pass' : generate_password(8);
		
		if (1!=$count)
		{
			$txt_query= '
			insert into Manager 
			(id_SRO, firstName, lastName, middleName, efrsbNumber,
			ManagerEmail_Change, ManagerEmail_ChangeTime,
			ManagerPassword_Change, ManagerPassword_ChangeTime,
			INN, ArbitrManagerID, TimeCreated)
			select 
			?, e.FirstName, e.LastName, e.MiddleName, e.RegNum, ?, adddate(now(),interval 1 hour), ?, adddate(now(),interval 1 hour), e.INN, ?, now()
			from efrsb_manager e
			where e.ArbitrManagerID = ?
			';
			$affected_rows = execute_query_get_affected_rows($txt_query,array('issii',$auth_info->id_SRO,$email,$new_password,$ArbitrManagerId, $ArbitrManagerId));
			if (1==$affected_rows)
			{
				$letter= self::PrepareNewManagerLetter($manager,$new_password);
				
				if (false==PostLetter($letter,$manager['Email'],$manager['firstName'],'создание АУ через сро',$manager['id_Manager']))
				{
					exit_internal_server_error("can not post letter to {$viewer['Email']}!");
				}
				else
				{
					echo '{ "ok": true }';
				}
			}
			else if (0==$affected_rows)
			{
				echo '{ "ok": true, "details": "changed nothing" }';
			}
			else
			{
				exit_internal_server_error("affected $affected_rows rows for ArbitrManagerId=$ArbitrManagerId as update");
			}
		}else {
		
			$id_Manager = $rows[0]->id_Manager;
			
			$txt_query= '
				update Manager set
				  ManagerEmail_Change=?,    ManagerEmail_ChangeTime= adddate(now(),interval 1 hour)
				, ManagerPassword_Change=?,	ManagerPassword_ChangeTime= adddate(now(),interval 1 hour)
				where id_Manager = ?
			';
			$affected_rows = execute_query_get_affected_rows($txt_query,
					array('ssi',$email,$new_password,$id_Manager));
						
			if (1==$affected_rows)
			{
				$letter= self::PrepareNewManagerLetter($manager,$new_password);
				
				if (false==PostLetter($letter,$manager['Email'],$manager['firstName'],'смена пароля АУ через сро',$id_Manager))
				{
					exit_internal_server_error("can not post letter to {$viewer['Email']}!");
				}
				else
				{
					echo '{ "ok": true }';
				}
			}
			else if (0==$affected_rows)
			{
				echo '{ "ok": true, "details": "changed nothing" }';
			}
			else
			{
				exit_internal_server_error("affected $affected_rows rows for ArbitrManagerId=$ArbitrManagerId as update");
			}
		}
	}
}

$crud= new Manager_crud();
$crud->process_cmd();
