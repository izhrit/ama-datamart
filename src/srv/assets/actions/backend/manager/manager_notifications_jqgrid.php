<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthSRO();
$id_sro= $auth_info->id_SRO;

$fields= "
	 se.id_SentEmail id_SentEmail
	,concat(m.lastName,' ',left(m.firstName,1),'.',left(m.middleName,1),'.') RecepientName
	,se.RecipientEmail RecipientEmail
	,se.EmailType EmailType
	,DATE_FORMAT(se.TimeDispatch, '%d.%m.%Y %H:%i') TimeDispatch
	,DATE_FORMAT(se.TimeSent, '%d.%m.%Y %H:%i') TimeSent
";

$from_where= " from SentEmail se
inner join Manager m on m.id_Manager=se.RecipientId
where m.id_SRO = $id_sro
&& se.RecipientType = 'm' && se.EmailType in ('y') ";

$filter_rule_builders= array(
	'RecepientName'=>prep_std_filter_rule_builder_for_expression('m.lastName')
	,'RecipientEmail'=>'std_filter_rule_builder'
	,'EmailType'=>'std_filter_rule_builder'
);

if (isset($_GET['sidx'])) {
	$_GET['sidx'] = str_replace('TimeDispatch','se.TimeDispatch',$_GET['sidx']);
	$_GET['sidx'] = str_replace('TimeSent','se.TimeSent',$_GET['sidx']);
}

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
