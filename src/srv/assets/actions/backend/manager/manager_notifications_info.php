<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthSRO();
CheckMandatoryGET_id('id');
$id_SentEmail= intval($_GET['id']);

$txt_query= "
	select
		  m.firstName
		, m.lastName
		, m.middleName
		, se.EmailType
		, se.RecipientEmail
		, DATE_FORMAT(se.TimeDispatch, '%d.%m.%Y %H:%i') TimeDispatch
		, DATE_FORMAT(se.TimeSent, '%d.%m.%Y %H:%i') TimeSent
		, uncompress(se.ExtraParams) ExtraParams
	from SentEmail se
	inner join Manager m on m.id_Manager=se.RecipientId
	where se.id_SentEmail = ?;";
$params = array('i',$id_SentEmail);

$rows= execute_query($txt_query,$params);

$rows0 = $rows[0];

$ExtraParams = json_decode($rows0->ExtraParams);

$rows0->Subject = $ExtraParams->Subject;
$rows0->Body = $ExtraParams->Body;
unset($rows0->ExtraParams);

echo nice_json_encode($rows0);