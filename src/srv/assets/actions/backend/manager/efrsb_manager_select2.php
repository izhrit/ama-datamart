<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManagerOrSRO();

$txt_query= "
		select
			  ArbitrManagerID id
			, concat(LastName,' ',FirstName,' ',MiddleName) text
		from efrsb_manager
		where LastName like ? and SRORegNum=?
		limit 20
	;";
$params = array('si',$_GET['q'].'%',$auth_info->RegNum);

$rows= execute_query($txt_query,$params);

echo nice_json_encode(array('results'=>$rows));