<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthSRO();

class Manager_documents_crud extends Base_crud
{
	function create($name)
	{
		$type= CheckMandatoryGET('type');
		$id_Manager= CheckMandatoryGET('id_Manager');

		if (!isset($_FILES['File']))
			exit_internal_server_error("Файл не приложен");

		$file= $_FILES['File'];

		if (!empty($file['error']))
			exit_internal_server_error('Не удалось загрузить файл, error:'.$file['error']);

		if (empty($file['tmp_name']))
			exit_internal_server_error("Не удалось загрузить файл (tmp_name is empty)");

		$tmp_name= $file['tmp_name'];
		if ('none'==$tmp_name || !is_uploaded_file($tmp_name))
			exit_internal_server_error("Не удалось загрузить файл $tmp_name");

		if (strlen($type) > 3)
			exit_internal_server_error("Слшком длинный тип '$type'");

		write_to_log($file);

		$file_content= file_get_contents($tmp_name);
		$txt_query= "INSERT INTO ManagerDocument set id_Manager=?, Body=?, FileName=?, DocumentType=?;";
		$id_ManagerDocument = execute_query_get_last_insert_id($txt_query,array('isss',$id_Manager, $file_content,$file['name'], $type ));
		echo nice_json_encode(array('ok'=>true, 'id'=>$id_ManagerDocument));
	}
	
	// ВНИМАНИЕ! тут конечно возвращается не документ, а документы АУ, вот такой получился костыль
	function read($id_Manager)
	{
		global $auth_info;
		$txt_query= "
			select  
				d.id_ManagerDocument id_ManagerDocument
				,d.FileName FileName
				,d.DocumentType DocumentType
				from ManagerDocument d
				inner join Manager m on m.id_Manager = d.id_Manager
				where m.id_Manager = ?";

		$rows= execute_query($txt_query,array('s',$id_Manager));
		echo nice_json_encode($rows);
	}

	function delete($id_ManagerDocument)
	{
		$txt_query= "delete d
			from ManagerDocument d 
			where d.id_ManagerDocument=?";
		$affected_rows= execute_query_get_affected_rows($txt_query,array('i',$id_ManagerDocument[0]));
		if (0!=$affected_rows)
		{
			echo '{ "ok": true }';
		}
		else
		{
			echo '{ "ok": false }';
		}
	}


}

$crud= new Manager_documents_crud();
$crud->process_cmd();
