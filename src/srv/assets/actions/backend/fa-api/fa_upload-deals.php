<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/helpers/validate.php';

global $auth_info;
$auth_info= CheckAuthViewer();

$debtor_inn= CheckMandatoryGET('debtor-inn');
$manager_inn= CheckMandatoryGET('manager-inn');

function ReadZip()
{
	switch ($_SERVER["CONTENT_TYPE"])
	{
		case 'application/zip': 
			$result= file_get_contents('php://input');
			return $result;
		case 'application/null': 
			return null;
		case 'application/xml':
			$tmp_file_path = tempnam("tmp", "zip");
			$zip = new ZipArchive();
			$zip->open($tmp_file_path, ZipArchive::OVERWRITE);
			$xml_content= file_get_contents('php://input');
			$zip->addFromString('registry.xml', $xml_content);
			$zip->close();
			$result= file_get_contents($tmp_file_path);
			unlink($tmp_file_path);
			return $result;
		case 'application/x-www-form-urlencoded':
			return '';
	}
	throw new Exception("unknown request content-type ".$_SERVER["CONTENT_TYPE"]);
}

$content= ReadZip();

$rows= execute_query('call SafeUploadDeals(?,?,?,?,?);',array('sssss',
	$manager_inn,
	$auth_info->id_MUser,
	$debtor_inn,
	$content,
	md5($content)
));

$count_rows= count($rows);

if (0==$count_rows)
{
	echo '{ "ok": true }';
}
else
{
	$message= '';
	foreach ($rows as $row)
		$message.= $row->message;
	echo nice_json_encode(array('ok'=>false, 'message'=>$message));
}
