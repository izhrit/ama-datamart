<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/helpers/validate.php';

global $auth_info;
$auth_info= CheckAuthViewer();

$debtor_inn= CheckMandatoryGET('debtor-inn');

$txt_query= 'select distinct
	m.lastName `Фамилия`
	,m.firstName `Имя`
	,m.middleName `Отчество`
	,m.INN `ИНН`
	,m.efrsbNumber `Номер_ЕФРСБ`
from ManagerUser mu
inner join Manager m on mu.id_Manager=mu.id_Manager
inner join MProcedure p on p.id_Manager=m.id_Manager
inner join Debtor d on d.id_Debtor=p.id_Debtor
where d.INN=?
  and mu.id_MUser=?
;';

$rows= execute_query($txt_query,array('ss',$debtor_inn,$auth_info->id_MUser));

echo nice_json_encode($rows);

