<?

require_once '../assets/actions/backend/constants/Vote_document.etalon.php';
require_once '../assets/actions/backend/constants/Vote_log.etalon.php';
require_once '../assets/actions/backend/constants/alib_constants.php';

$Параметры_смены_состояния_заседания= array
(
	'Начало голосования'=> (object)array(
		  'Начало_темы_письма' => 'Заседание комитета'
		, 'Шаблон_письма' => 'b_starts'
		, 'Тип_письма'=>'о заседании КК'
	)
	,'Итоги заседания'=> (object)array(
		  'Начало_темы_письма' => 'Результаты заседания'
		, 'Шаблон_письма' => 'e_took_place_fix_results'
		, 'Тип_письма'=>'об итогах КК'
	)
	,'Не состоялось'=> (object)array(
		  'Начало_темы_письма' => 'НЕ состоялось заседание'
		, 'Шаблон_письма' => 'f_did_not_take_place'
		, 'Тип_письма'=>'КК не состоялось'
	)
	,'Приостановлено'=> (object)array(
		  'Начало_темы_письма' => 'Приостановлено заседание'
		, 'Шаблон_письма' => 'c_paused'
		, 'Тип_письма'=>'КК приостановлено'
	)
	,'Продолжено'=> (object)array(
		  'Начало_темы_письма' => 'Возобновлено заседание'
		, 'Шаблон_письма' => 'b_starts_again'
		, 'Тип_письма'=>'КК продолжено'
	)
	,'Продолжено без вас'=> (object)array(
		  'Начало_темы_письма' => 'Без вас возобновлено заседание'
		, 'Шаблон_письма' => 'b_starts_again_without'
		, 'Тип_письма'=>'КК продолжено без вас'
	)
	,'Отменено'=> (object)array(
		  'Начало_темы_письма' => 'Отменено заседание'
		, 'Шаблон_письма' => 'g_stop'
		, 'Тип_письма'=>'КК отменено'
	)

	,'Письмо о подписи'=> (object)array(
		  'Начало_темы_письма' => 'Бюллетень комитета'
		, 'Шаблон_письма' => 'bc_get_code'
		, 'Тип_письма'=>'о начале подписания КК'
	)
	,'Подписан бюллетень'=> (object)array(
		  'Начало_темы_письма' => 'Подтверждение голосования'
		, 'Шаблон_письма' => 'bc_got_code'
		, 'Тип_письма'=>'о подписании КК'
	)
);

function endsWith($haystack, $needle)
{
	$length = strlen($needle);
	if ($length == 0)
		return true;

	return (substr($haystack, -$length) === $needle);
}

function fix_end_of_lines($str)
{
	return str_replace("\n","\r\n",str_replace("\r\n","\n",$str));
}

function Направить_email_участнику_КК($row, $участник, $ФИО_участника, $тип_события, $дополнительно= null)
{
	global $Параметры_смены_состояния_заседания;
	$p= $Параметры_смены_состояния_заседания[$тип_события];

	$параметры= !isset($дополнительно['параметры']) ? null : $дополнительно['параметры'];

	$letter= (object)array('subject'=>"{$p->Начало_темы_письма} {$row->Date} {$row->debtorName}");
	ob_start();
	include "../assets/actions/backend/meeting/views/letters/{$p->Шаблон_письма}.php";
	$letter->body_txt= fix_end_of_lines(ob_get_contents());
	ob_end_clean();

	if (null!=$дополнительно && isset($дополнительно['вложения']))
	{
		$attachments= array();
		foreach ($дополнительно['вложения'] as $row_Vote_document)
		{
			$attachments[]= array(
				'filename'=>$row_Vote_document->FileName
				, 'content'=>$row_Vote_document->Body
			);
		}
		$letter->attachments= $attachments;
	}

	$row_SentEmail= PostLetter($letter,$участник->email,$ФИО_участника,$p->Тип_письма,$участник->id_Vote);
	$row_SentEmail->row_Vote_log= PrepareLog($участник->id_Vote,$тип_события,$участник->email);
	return $row_SentEmail;
}


function findByNumber($questions,$number)
{
	$i= 1;
	foreach ($questions as $question)
	{
		if (isset($question->Номер) && $number==$question->Номер || $i==$number)
			return $question;
		$i++;
	}
	return null;
}

function Голосование($id_Meeting)
{
	$txt_query= "select 
			id_Vote
			, date_format(CabinetTime,'%d.%m.%Y %H:%i') CabinetTime
			, uncompress(Member) Member
			, Answers
		from Vote
		where id_Meeting=?
	";
	$rows= execute_query($txt_query,array('i',$id_Meeting));

	$Голосование= array();
	if (0!=count($rows))
	{
		foreach ($rows as $v)
		{
			$vote_member= json_decode($v->Member);
			$vote= (object)array(
				'ФИО'=> $vote_member->Фамилия.' '.$vote_member->Имя.' '.$vote_member->Отчество
				, 'email'=> $vote_member->email
				, 'Уведомлен'=> $v->CabinetTime
				, 'id_Vote'=> $v->id_Vote
			);
			if (isset($vote_member->Нет_в_списках))
				$vote->Нет_в_списках= $vote_member->Нет_в_списках;
			if (null!=$v->Answers)
			{
				$answers= json_decode($v->Answers);
				$answers_count= count($answers);
				$vote->Ответов= $answers_count;
				if ($answers_count>0 && isset($answers[0]->Подписано))
				{
					$vote->Проголосовал= $answers[0]->Подписано;
					$vote->Ответы= $answers;
				}
			}
			$Голосование[]= $vote;
		}
	}
	return $Голосование;
}

function build_Meeting_data($row_Meeting)
{
	$meeting= json_decode($row_Meeting->Body);
	$meeting->id_Meeting= $row_Meeting->id_Meeting;

	$date= date_create_from_format('Y-m-d H:i:s',$row_Meeting->Date);
	$meeting->Дата_заседания= date_format($date,'d.m.Y');
	$meeting->Время_заседания= date_format($date,'H:i');

	if (!isset($meeting->Должник))
		$meeting->Должник= array();
	$Должник= (object)$meeting->Должник;
	if (!isset($row_Meeting->debtorName))
		exit_internal_server_error("can not get $row_Meeting->debtorName");
	$Должник->Наименование = $row_Meeting->debtorName;
	$Должник->ИНН = $row_Meeting->debtorInn;
	$Должник->ОГРН = $row_Meeting->debtorOgrn;
	$Должник->СНИЛС = $row_Meeting->debtorSnils;
	$meeting->Должник= $Должник;

	$meeting->Голосование= Голосование($row_Meeting->id_Meeting);

	if(isset($meeting->Участники) && $meeting->Участники)
		prepare_phone_numbers($meeting);
	return $meeting;
}

function cmp_documents($a, $b)
{
	$ta= $a->time;
	$tb= $b->time;
	return ($ta < $tb) ? -1 : 1;
}

function build_FileName_for_SentEmail($row)
{
	global $EmailType_descriptions;
	if (!isset($row->TimeSent) || null==$row->TimeSent)
	{
		return '';
	}
	else
	{
		$d= FindDescription($EmailType_descriptions,'code',$row->EmailType);
		$filename= (null==$d || !isset($d['FileName_prefix'])) ? 'Unknown' : $d['FileName_prefix'];
		$filename.= '.';
		$TimeSent= date_create_from_format('Y-m-d H:i:s',$row->TimeDispatch);
		$filename.= date_format($TimeSent,'YmdHis');
		$filename.= '.eml';
		return $filename;
	}
}

$txt_query_select_Vote_document= "select 
	Vote_document_time
	, Vote_document_type
	, FileName
	, id_Vote_document
	, if(Body is null,null,md5(uncompress(Body))) doc_md5
	, if(Body is null,null,length(uncompress(Body))) `Размер`
from Vote_document 
where id_Vote= ?";

$txt_query_select_SentEmail= "select 
	EmailType
	, RecipientEmail
	, TimeDispatch
	, TimeSent
	, if(Message is null,null,md5(uncompress(Message))) doc_md5
	, if(Message is null,null,length(uncompress(Message))) `Размер`
	, id_SentEmail
	, uncompress(ExtraParams) ExtraParams
from SentEmail 
where RecipientId= ? && RecipientType='c';";

function Load_ЖурналДокументы(&$vote_cabinet)
{
	global $txt_query_select_Vote_document, $txt_query_select_SentEmail;
	$txt_query= "select Vote_log_type, Vote_log_time, body from Vote_log where id_Vote= ?";
	$rows= execute_query($txt_query,array('i',$vote_cabinet['id_Vote']));
	foreach ($rows as $row)
	{
		$st= $row->Vote_log_time;
		$t= date_create_from_format('Y-m-d H:i:s',$st);
		$st= date_format($t,'d.m.Y H:i:s');
		$row->Vote_log_time= $st;
	}
	$vote_cabinet['Журнал']= $rows;

	$rows_Vote_document= execute_query($txt_query_select_Vote_document,array('i',$vote_cabinet['id_Vote']));
	$rows_SentEmail= execute_query($txt_query_select_SentEmail,array('i',$vote_cabinet['id_Vote']));

	$documents= array_merge($rows_Vote_document, $rows_SentEmail);

	foreach ($documents as $doc)
	{
		if (!isset($doc->FileName))
			$doc->FileName= build_FileName_for_SentEmail($doc);
		$st= isset($doc->Vote_document_time) ? $doc->Vote_document_time : $doc->TimeDispatch;
		$t= date_create_from_format('Y-m-d H:i:s',$st);
		$doc->time= $t;
		$st= date_format($t,'d.m.Y H:i:s');
		if (isset($doc->Vote_document_time))
		{
			$doc->Vote_document_time= $st;
		}
		else
		{
			$doc->TimeDispatch= $st;
			if (null!=$doc->ExtraParams)
				$doc->ExtraParams= json_decode($doc->ExtraParams);
		}
	}
	usort($documents, "cmp_documents");
	foreach ($documents as $doc)
		unset($doc->time);

	$vote_cabinet['Документы']= $documents;
}

function PrepareLog($id_Vote,$descr,$body=null)
{
	global $Vote_log_type_description;
	$d= FindDescription($Vote_log_type_description,'descr',$descr);
	$code= $d['code'];
	$log_time= safe_date_create();
	$log_time_sql= date_format($log_time,'Y-m-d\TH:i:s');
	$log_time_json= date_format($log_time,'d.m.Y H:i:s');
	$txt_query= "insert into Vote_log set id_Vote=?, Vote_log_type=?, Vote_log_time=?, body=?;";
	execute_query_get_last_insert_id($txt_query,array('ssss',$id_Vote,$code,$log_time_sql,$body));
	$res= array('Vote_log_type'=>$code,'Vote_log_time'=>$log_time_json);
	if (null!=$body)
		$res['body']= $body;
	return $res;
}

function PrepareDocument($id_Vote,$log_time,$code,$body)
{
	global $operator_pem_file_path, $Vote_document_type_description;

	$log_time_sql= date_format($log_time,'Y-m-d\TH:i:s');
	$log_time_json= date_format($log_time,'d.m.Y H:i:s');

	$d= FindDescription($Vote_document_type_description,'code',$code);

	$extension= ($d['Наименование_документа']=='Подпись ИС «Витрина данных ПАУ»' && null==$operator_pem_file_path) ? 'txt' : $d['FileName_extension'];

	$filename= $d['FileName_prefix'].'.'.date_format($log_time,'YmdHis').'.'.$extension;
	$txt_query= "insert into Vote_document set id_Vote=?, Vote_document_time=?, Body=compress(?), FileName=?, Vote_document_type=?;";
	$id_Vote_document= execute_query_get_last_insert_id($txt_query,array('sssss',$id_Vote,$log_time_sql,$body,$filename,$code));

	return (object)array('id_Vote_document'=>$id_Vote_document,'Vote_document_time'=>$log_time_json,
				'Vote_document_type'=>$code,'FileName'=>$filename, 'Размер'=>strlen($body), 'doc_md5'=>md5($body), 'Body'=>$body);
}

function smime_p7s_to_sig($smime_p7s)
{
	$prefix= 'Content-Disposition: attachment; filename="smime.p7s"';
	$pos1= strpos($smime_p7s,$prefix) + strlen($prefix);
	$pos2= strpos($smime_p7s,'------',$pos1);
	$sig= substr($smime_p7s,$pos1,$pos2-$pos1);
	return trim($sig);
}

function prepareSignatureOfOperator($txt)
{
	global $operator_pem_file_path;

	if (null==$operator_pem_file_path)
	{
		return 'Заменитель подписи в тестовом режиме (настройка пути к сертификату НЕ задана)';
	}
	else
	{
		$temp_file = tmpfile();
		try
		{
			$meta_data= stream_get_meta_data($temp_file);
			$temp_file_path = $meta_data['uri'];
			$temp_file_path_sig = $temp_file_path.'.sig';
			fwrite($temp_file, $txt);
			fflush($temp_file);

			try
			{
				openssl_pkcs7_sign($temp_file_path, $temp_file_path_sig, $operator_pem_file_path,
					array($operator_pem_file_path, ""),
					array(),PKCS7_DETACHED | PKCS7_BINARY
				);
				$smime_p7s= file_get_contents($temp_file_path_sig);
				unlink($temp_file_path_sig);
			}
			catch (Exception $ex)
			{
				unlink($temp_file_path_sig);
				throw $ex;
			}
			fclose($temp_file);
		}
		catch (Exception $ex)
		{
			fclose($temp_file);
			throw $ex;
		}
		$signature= smime_p7s_to_sig($smime_p7s);
		return $signature;
	}
}

function fix_phone_numbers(& $meeting)
{
	for($i=0;$i<count($meeting['Участники']);$i++)
	{
		if(isset($meeting['Участники'][$i]["телефон"]))
			$meeting['Участники'][$i]["телефон"]=preg_replace("/[^0-9.]/", "", $meeting['Участники'][$i]["телефон"]);
		if(strlen($meeting['Участники'][$i]["телефон"])==11)
			$meeting['Участники'][$i]["телефон"]=substr_replace($meeting['Участники'][$i]["телефон"], '8', 0, 1);
	}
}

function prepare_phone_numbers(& $meeting)
{
	for($i=0;$i<count($meeting->Участники);$i++)
	{
		if(isset($meeting->Участники[$i]->телефон) && strlen($meeting->Участники[$i]->телефон)==11)
		{
			$phone=$meeting->Участники[$i]->телефон;
			$meeting->Участники[$i]->телефон = '+7(' . substr($phone,1,3) . ')-' . substr($phone,4,3) . '-' . substr($phone,7,2) . '-' . substr($phone,9,2);
		}
	}
}
