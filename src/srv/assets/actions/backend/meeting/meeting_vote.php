<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/meeting/alib_meeting.php';

$auth_info= CheckAuthManager();
CheckMandatoryGET('id_Vote');

$txt_query= "select
		uncompress(v.Member) Member
		,uncompress(m.Body) Body
		, v.Answers Answers
	from Vote v
	inner join Meeting m on v.id_Meeting=m.id_Meeting
	where v.id_Vote= ?
";
$rows= execute_query($txt_query,array('i',$_GET['id_Vote']));

$row= $rows[0];
$Member= json_decode($row->Member);
$Body= json_decode($row->Body);

$vote= array(
	'ФИО'=> $Member->Фамилия.' '.$Member->Имя.' '.$Member->Отчество
	,'email'=>$Member->email
	,'телефон'=>$Member->телефон
	,'id_Vote'=>$_GET['id_Vote']
);

if (isset($Member->Нет_в_списках))
	$vote['Нет_в_списках']= $Member->Нет_в_списках;

$answres= null==$row->Answers ? array() : json_decode($row->Answers);

$Вопросы= array();
$i= 1;
foreach ($Body->Вопросы as $вопрос)
{
	$Номер= ($i++).'';
	$вопрос= (object)array(
		'Номер'=> $Номер
		,'На_голосование'=> array( 'Формулировка'=>$вопрос->На_голосование->Формулировка)
		,'В_повестке'=> $вопрос->В_повестке
	);
	$answer= findByNumber($answres,$Номер);
	if (null!=$answer)
	{
		$вопрос->Ответ= $answer->Ответ;
		if (isset($answer->Подписано))
			$вопрос->Подписано= $answer->Подписано;
	}
	$Вопросы[]= $вопрос;
}
$vote['Вопросы']= $Вопросы;

Load_ЖурналДокументы($vote);

echo nice_json_encode($vote);
