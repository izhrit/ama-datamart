<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
	<title>Бюллетени и подписи</title>
</head>

<body>

<h1>Бюллетени и подписи</h1>

<? foreach ($vote_rows as $vote_row) : ?>
	<h2>
		<?= $vote_row->Member->Фамилия ?> 
		<?= $vote_row->Member->Имя ?> 
		<?= $vote_row->Member->Отчество ?> 
	</h2>

	<? foreach ($Vote_document_type_description as $doc_type_descr) : ?>
		<b><?= $doc_type_descr['Наименование_документа'] ?></b><br/>
		<? $doc_code= $doc_type_descr['code']; ?>
		<? if (!isset($vote_row->Documents[$doc_code])) : ?>
			Документ отсутствует.<br/>
		<? else : ?>
			<? $doc= $vote_row->Documents[$doc_code]; ?>
			<small>Файл "<?= $doc->FileName ?>", размером <?= $doc->size ?> байт, контрольной суммой (хэш) md5 <?= $doc->doc_md5 ?></small>:
			<hr/><pre><?= $doc->Body ?></pre><hr/>
		<? endif; ?>
	<? endforeach; ?>

<? endforeach; ?>

</body>
</html>