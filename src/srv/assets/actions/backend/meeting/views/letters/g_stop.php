Уважаемый(ая) <?= $ФИО_участника ?>,

Уведомляем Вас об ОТМЕНЕ голосования комитета кредиторов 
<?= $row->debtorName ?>, 
итоги которого должны были подводиться <?= $row->Date ?>.

C Уважением, ИС «Витрина данных ПАУ», 
Арбитражный управляющий <?= $row->au_lastName ?> <?= $row->au_firstName ?> <?= $row->au_middleName ?>.
