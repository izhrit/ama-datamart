Уважаемый(ая) <?= $ФИО_участника ?>,

Уведомляем Вас о том, что заседание комитета кредиторов 
<?= $row->debtorName ?>,
назначенное на <?= $row->Date ?>, НЕ состоялось.
<? if (isset($row->meeting->Заседание_не_состоялось->Пояснения) && ''!=$row->meeting->Заседание_не_состоялось->Пояснения) : ?>

Пояснения: <?= $row->meeting->Заседание_не_состоялось->Пояснения ?>

<? endif; ?>

C Уважением, ИС «Витрина данных ПАУ», 
Арбитражный управляющий <?= $row->au_lastName ?> <?= $row->au_firstName ?> <?= $row->au_middleName ?>.