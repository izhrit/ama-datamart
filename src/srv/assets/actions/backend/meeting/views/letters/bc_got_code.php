Уважаемый(ая) <?= $участник->Фамилия ?> <?= $участник->Имя ?> <?= $участник->Отчество ?>,

В Вашем Кабинете голосования комитета кредиторов 
<?= $row->debtorName ?> 
для заседания <?= $row->Date ?> был заполнен и подписан бюллетень голосования.

Прикладываем подтверждающие материалы: 
1) Простая электронная подпись бюллетеня во вложении
     <?= $параметры->row_Vote_document_sig->FileName ?>

2) Подпись ИС «Витрина данных ПАУ» файла 
     <?= $параметры->row_Vote_document_sig->FileName ?>

   во вложении
     <?= $параметры->row_Vote_document_rit_sig->FileName ?>


C Уважением, ИС «Витрина данных ПАУ», 
Арбитражный управляющий <?= $row->au_lastName ?> <?= $row->au_firstName ?> <?= $row->au_middleName ?>.
