<?

require_once '../assets/helpers/db.php';
require_once '../assets/actions/backend/constants/Vote_document.etalon.php';
require_once '../assets/actions/backend/constants/SentEmail.etalon.php';
require_once '../assets/actions/backend/meeting/alib_meeting.php';

if (isset($_GET['id_SentEmail']) && ''!=$_GET['id_SentEmail'])
{
	$txt_query= "select uncompress(Message) Message, EmailType, TimeSent, TimeDispatch from SentEmail where id_SentEmail=?;";
	$rows= execute_query($txt_query,array('s',$_GET['id_SentEmail']));
	$m= $rows[0];
	header('Content-Type: message/rfc822');
	$filename= build_FileName_for_SentEmail($m);
	write_to_log("Content-Disposition: attachment; filename={$filename}");
	header("Content-Disposition: attachment; filename={$filename}");
	echo $m->Message;
	exit;
}
else if (isset($_GET['id_Vote_document']) && ''!=$_GET['id_Vote_document'])
{
	$txt_query= "select Vote_document_type, uncompress(Body) Body, FileName from Vote_document where id_Vote_document=?;";
	$rows= execute_query($txt_query,array('s',$_GET['id_Vote_document']));
	$d= $rows[0];
	$path_parts = pathinfo($d->FileName);
	switch ($path_parts['extension'])
	{
		case 'sig': header('Content-Type: application/x-pkcs7-signature'); break;
		case 'txt': header('Content-Type: text/plain'); break;
	}
	header("Content-Disposition: attachment; filename={$d->FileName}");
	echo $d->Body;
	exit;
}
else
{
	exit_bad_request('skipped id_SentEmail or id_Vote_document argument');
}
