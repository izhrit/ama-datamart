<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/text.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/constants/Vote_document.etalon.php';

global $auth_info;
if (isset($_GET['auth']) || isset($_GET['fauth']))
	require_once '../assets/actions/backend/auto_login.php';

$auth_info= CheckAuthManager();

$участник= (object)array(
	'Фамилия'=>stripslashes($_GET['Фамилия'])
	,'Имя'=>stripslashes($_GET['Имя'])
	,'Отчество'=>stripslashes($_GET['Отчество'])
	,'телефон'=>stripslashes($_GET['телефон'])
	,'email'=>stripslashes($_GET['email'])
);
$должник= (object)array(
	'Наименование'=>stripslashes($_GET['Должник'])
);

require_once '../assets/actions/backend/meeting/views/documents/agreement.html';
