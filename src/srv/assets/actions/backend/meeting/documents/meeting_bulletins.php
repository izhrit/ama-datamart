<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/constants/Vote_document.etalon.php';

if (isset($_GET['auth']))
{
	global $auth_info;
	require_once '../assets/actions/backend/auto_login.php';
}
$auth_info= CheckAuthManager();
CheckMandatoryGET('id_Meeting');

$txt_query= "select id_Vote, uncompress(Member) Member from Vote where id_Meeting=?;";
$vote_rows= execute_query($txt_query,array('s',$_GET['id_Meeting']));

$txt_query= "select 
	  d.id_Vote id_Vote
	, uncompress(d.Body) Body
	, d.Vote_document_type Vote_document_type
	, d.FileName FileName
	, if(d.Body is null,null,md5(uncompress(Body))) doc_md5
	, if(d.Body is null,null,length(uncompress(Body))) size
from Vote_document d
inner join Vote v on v.id_Vote=d.id_Vote 
where v.id_Meeting=?;";
$document_rows= execute_query($txt_query,array('s',$_GET['id_Meeting']));

$vote_rows_by_id_Vote= array();
foreach ($vote_rows as $vote_row)
{
	$vote_rows_by_id_Vote[$vote_row->id_Vote]= $vote_row;
	$vote_row->Member= json_decode($vote_row->Member);
	$vote_row->Documents= array();
}

foreach ($document_rows as $document_row)
{
	$vote_row= $vote_rows_by_id_Vote[$document_row->id_Vote];
	$vote_row->Documents[$document_row->Vote_document_type]= $document_row;
}

require_once '../assets/actions/backend/meeting/views/documents/bulletins.php';
