<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';

if (isset($_GET['id_SentEmail']) && ''!=$_GET['id_SentEmail'])
{
	$txt_query= "select uncompress(ExtraParams) ExtraParams from SentEmail where id_SentEmail=?;";
	$rows= execute_query($txt_query,array('s',$_GET['id_SentEmail']));
	echo nice_json_encode($rows[0]->ExtraParams);
	exit;
}
else if (isset($_GET['id_Vote_document']) && ''!=$_GET['id_Vote_document'])
{
	$txt_query= "select Vote_document_time, Vote_document_type, uncompress(Body) Body from Vote_document where id_Vote_document=?;";
	$rows= execute_query($txt_query,array('s',$_GET['id_Vote_document']));
	echo nice_json_encode($rows[0]->Body);
	exit;
}
else
{
	exit_bad_request('skipped id_SentEmail or id_Vote_document argument');
}
