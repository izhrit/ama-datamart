<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/text.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/constants/Vote_document.etalon.php';

if (isset($_GET['auth']))
{
	global $auth_info;
	require_once '../assets/actions/backend/auto_login.php';
}
$auth_info= CheckAuthManager();
CheckMandatoryGET('id_Meeting');

$txt_query= "select 
	m.id_Meeting id_Meeting
	,mp.casenumber as casenumber
	,uncompress(m.Body) m_Body
	,au.firstName au_firstName
	,au.lastName au_lastName
	,au.middleName au_middleName
from Meeting m 
inner join MProcedure mp on mp.id_MProcedure=m.id_MProcedure
inner join Manager au on au.id_Manager=mp.id_Manager
where m.id_Meeting=?
;";
$meeting_rows= execute_query($txt_query,array('s',$_GET['id_Meeting']));

$meeting_row= $meeting_rows[0];
$meeting_row->m_Body= json_decode($meeting_row->m_Body);

$txt_query= "select 
	id_Vote
	,Answers
from Vote
where id_Meeting=?
;";
$vote_rows= execute_query($txt_query,array('s',$_GET['id_Meeting']));
foreach ($vote_rows as $vote_row)
	$vote_row->Answers= json_decode($vote_row->Answers);

$номер= 1;
foreach ($meeting_row->m_Body->Вопросы as $вопрос)
{
	$вопрос->Номер= !isset($вопрос->Номер) ? $номер : $вопрос->Номер;
	if (isset($meeting_row->m_Body->Результаты))
	{
		foreach ($meeting_row->m_Body->Результаты as $результат)
		{
			if ($вопрос->Номер==$результат->По_вопросу->Номер)
			{
				$вопрос->Результат= $результат;
				break;
			}
		}
	}

	$вопрос->Ответы= array();
	$варианты= (!isset($вопрос->На_голосование->Форма_бюллетеня) || 'f1'==$вопрос->На_голосование->Форма_бюллетеня)
		? array('За','Против','Воздержался') : $вопрос->На_голосование->Варианты;
	foreach ($варианты as $вариант)
		$вопрос->Ответы[$вариант]= 0;

	foreach ($vote_rows as $vote_row)
	{
		if (isset($vote_row->Answers))
		{
			foreach ($vote_row->Answers as $ответ_на_вопрос)
			{
				if ($ответ_на_вопрос->На_вопрос->Номер==$вопрос->Номер && isset($ответ_на_вопрос->Подписано) && ''!=$ответ_на_вопрос->Подписано)
				{
					$ответ= $ответ_на_вопрос->Ответ;
					$ответы= $вопрос->Ответы;
					$вопрос->Ответы[$ответ]= $ответы[$ответ]+1;
					break;
				}
			}
		}
	}
	$номер++;
}

$meeting_row->Проголосовало= 0;
foreach ($meeting_row->m_Body->Участники as $участник)
{
	$участник->Голосовал= false;
	foreach ($vote_rows as $vote_row)
	{
		if (isset($участник->id_Vote) && $vote_row->id_Vote==$участник->id_Vote && null!=$vote_row->Answers)
		{
			foreach ($vote_row->Answers as $ответ_на_вопрос)
			{
				if (isset($ответ_на_вопрос->Подписано) && ''!=$ответ_на_вопрос->Подписано)
				{
					$участник->Голосовал= true;
					$meeting_row->Проголосовало++;
					break;
				}
			}
			break;
		}
	}
}

$дата_протокола= date_format(safe_date_create(),'d.m.Y');

require_once '../assets/actions/backend/meeting/views/documents/protocol.html';
