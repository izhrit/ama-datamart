<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/post.php';

require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/meeting/alib_meeting.php';
require_once '../assets/actions/backend/constants/Meeting_State.etalon.php';

$auth_info= CheckAuthManager();
$state= $_GET['state'];

$txt_query= "select 
	 m.id_Meeting id_Meeting
	,date_format(m.Date,'%d.%m.%Y %H:%i') Date
	,uncompress(m.Body) meeting
	,m.State State
	,d.Name debtorName
	,d.OGRN debtorOgrn
	,d.INN debtorInn
	,au.firstName au_firstName
	,au.lastName au_lastName
	,au.middleName au_middleName
from Meeting m
inner join MProcedure p on p.id_MProcedure=m.id_MProcedure
inner join Debtor d on d.id_Debtor=p.id_Debtor 
inner join Manager au on au.id_Manager=p.id_Manager
where id_Meeting=?;";
$rows= execute_query($txt_query,array('i',$_GET['id_Meeting']));

$count_rows= count($rows);
if (1!=$count_rows)
	exit_not_found("find $count_rows Meetings width id_Meeting=".$_GET['id_Meeting']);

$row= $rows[0];
$row->meeting= json_decode($row->meeting);

function UpdateMeeting($state)
{
	global $row, $Meeting_State_descriptions;
	$d= FindDescription($Meeting_State_descriptions,'code',$state);
	$row->meeting->Состояние= $d['descr'];
	$txt_query= "update Meeting set Body=compress(?), State=? where id_Meeting=?;";
	execute_query_no_result($txt_query,array('sss',nice_json_encode($row->meeting),$state,$row->id_Meeting));
}

function UpdateVoteAfterPause($vote,$участник)
{
	$txt_query= "update Vote set Answers= null, Confirmation_code= null, Confirmation_code_time= null, Member=compress(?) where id_Vote=?;";
	execute_query_no_result($txt_query,array('si',json_encode($участник),$vote->id_Vote));
}

function start_vote($row, &$участник)
{
	global $url_key, $email_settings;

	$CabinetTime= safe_date_create();
	$CabinetKey= (true!=$email_settings->enabled) ? '12345678901234567890' : generate_password(20);

	$txt_query= "insert into Vote set id_Meeting=?, CabinetTime=?, CabinetKey=?, Member=compress(?);";
	$id_Vote= execute_query_get_last_insert_id($txt_query,
		array('ssss',$_GET['id_Meeting'],date_format($CabinetTime,'Y-m-d\TH:i:s'),$CabinetKey,json_encode($участник)));

	$участник->id_Vote= $id_Vote;

	$url_key= $CabinetKey.$id_Vote;

	$ФИО_участника= $участник->Фамилия.' '.$участник->Имя.' '.$участник->Отчество;
	Направить_email_участнику_КК($row, $участник, $ФИО_участника, 'Начало голосования');

	return array(
		'id_Vote'=> $id_Vote, 'ФИО'=>$ФИО_участника, 'email'=>$участник->email
		, 'Уведомлен'=> date_format($CabinetTime,'d.m.Y H:i')
	);
}

function start_first($state,$row)
{
	if (!isset($row->meeting->Вопросы) || 0==count($row->meeting->Вопросы))
		exit_bad_request('Повестка заседания пуста!');

	$Голосование= array();
	foreach ($row->meeting->Участники as $участник)
		$Голосование[]= start_vote($row, $участник);

	UpdateMeeting($state);

	echo nice_json_encode($Голосование);
}

function start_again($state,$row)
{
	$txt_query= "select id_Vote, date_format(CabinetTime,'%d.%m.%Y %H:%i') CabinetTime, uncompress(Member) Member from Vote where id_Meeting=?;";
	$rows_Vote= execute_query($txt_query,array('i',$_GET['id_Meeting']));

	$rows_Vote_byEmail= array();
	foreach ($rows_Vote as $vote)
	{
		$vote->Member= json_decode($vote->Member);
		$rows_Vote_byEmail[$vote->Member->email]= $vote;
		$vote->Block= true;
	}

	$Голосование= array();
	foreach ($row->meeting->Участники as $участник)
	{
		if (!isset($rows_Vote_byEmail[$участник->email]))
		{
			$Голосование[]= start_vote($row, $участник);
		}
		else
		{
			$vote= $rows_Vote_byEmail[$участник->email];

			$участник->id_Vote= $vote->id_Vote;
			$ФИО_участника= $участник->Фамилия.' '.$участник->Имя.' '.$участник->Отчество;
			Направить_email_участнику_КК($row, $участник, $ФИО_участника, 'Продолжено');

			$vote->Block= false;
			$участник->Нет_в_списках= 'false';
			UpdateVoteAfterPause($vote,$участник);

			$Голосование[]= array(
				'id_Vote'=> $vote->id_Vote, 'ФИО'=>$ФИО_участника, 'email'=>$участник->email
				, 'Уведомлен'=> $vote->CabinetTime
			);
		}
	}

	foreach ($rows_Vote as $vote)
	{
		if ($vote->Block)
		{
			$участник= $vote->Member;
			$ФИО_участника= $участник->Фамилия.' '.$участник->Имя.' '.$участник->Отчество;
			if (!isset($участник->Нет_в_списках) || 'false'==$участник->Нет_в_списках)
			{
				$участник->Нет_в_списках= 'true';
				UpdateVoteAfterPause($vote,$участник);
				$участник->id_Vote= $vote->id_Vote;
				Направить_email_участнику_КК($row, $участник, $ФИО_участника, 'Продолжено без вас');
			}
			array_unshift($Голосование,array(
				'id_Vote'=> $vote->id_Vote, 'ФИО'=>$ФИО_участника, 'email'=>$участник->email
				, 'Уведомлен'=> $vote->CabinetTime, 'Нет_в_списках'=>$участник->Нет_в_списках
			));
		}
	}

	UpdateMeeting($state);

	echo nice_json_encode($Голосование);
}

function start($state,$row)
{
	if ('a'==$row->State)
	{
		start_first($state,$row);
	}
	else
	{
		start_again($state,$row);
	}
}

function took_place_fix_results($state,$row)
{
	$post_data= safe_POST($_POST);

	$row->meeting->Результаты= $post_data['результаты'];

	foreach ($row->meeting->Участники as $участник)
	{
		$ФИО_участника= $участник->Фамилия.' '.$участник->Имя.' '.$участник->Отчество;
		Направить_email_участнику_КК($row, $участник, $ФИО_участника, 'Итоги заседания');
	}

	UpdateMeeting($state);

	$res= array('ok'=>true);
	echo nice_json_encode($res);
}

function did_not_took_place($state,$row)
{
	$post_data= safe_POST($_POST);

	$row->meeting->Заседание_не_состоялось= (object)array('Пояснения'=>$post_data['Пояснения']);

	foreach ($row->meeting->Участники as $участник)
	{
		if (isset($участник->id_Vote))
		{
			$ФИО_участника= $участник->Фамилия.' '.$участник->Имя.' '.$участник->Отчество;
			Направить_email_участнику_КК($row, $участник, $ФИО_участника, 'Не состоялось');
		}
	}

	UpdateMeeting($state);

	$res= array('ok'=>true);
	echo nice_json_encode($res);
}

function pause($state,$row)
{
	foreach ($row->meeting->Участники as $участник)
	{
		if (isset($участник->id_Vote))
		{
			$ФИО_участника= $участник->Фамилия.' '.$участник->Имя.' '.$участник->Отчество;
			Направить_email_участнику_КК($row, $участник, $ФИО_участника, 'Приостановлено');
		}
	}

	UpdateMeeting($state);

	$res= array('ok'=>true);
	echo nice_json_encode($res);
}

function stop($state,$row)
{
	foreach ($row->meeting->Участники as $участник)
	{
		if (isset($участник->id_Vote))
		{
			$ФИО_участника= $участник->Фамилия.' '.$участник->Имя.' '.$участник->Отчество;
			Направить_email_участнику_КК($row, $участник, $ФИО_участника, 'Отменено');
		}
	}

	UpdateMeeting($state);

	$res= array('ok'=>true);
	echo nice_json_encode($res);
}


switch ($state)
{
	case 'b': start                   ($state,$row); break;
	case 'c': pause                   ($state,$row); break;
	case 'e': took_place_fix_results  ($state,$row); break;
	case 'f': did_not_took_place      ($state,$row); break;
	case 'g': stop                    ($state,$row); break;
	default:
		exit_bad_request("unknown cmd=$cmd");
}
