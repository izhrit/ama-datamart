<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/meeting/alib_meeting.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/SMSTransport.php';

session_start();

CheckMandatoryGET('cmd');
CheckMandatoryGET_id('id_Vote');
$cmd= $_GET['cmd'];

function store_answer()
{
	$id_Vote= $_GET['id_Vote'];
	$data= safe_POST($_POST);
	$answer= $data['На_вопрос'];
	$answer_number= $answer['Номер'];

	$txt_query= "select 
		v.Answers Answers
		,uncompress(m.Body) Body
	from Vote v
	inner join Meeting m on m.id_Meeting=v.id_Meeting
	where v.id_Vote=?";
	$rows= execute_query($txt_query,array('s',$id_Vote));

	$row= $rows[0];
	$meeting_info= json_decode($row->Body);
	$answers= null==$row->Answers ? array() : json_decode($row->Answers);

	$question= findByNumber($meeting_info->Вопросы,$answer_number);
	$answer= findByNumber($answers,$answer_number);
	if (null==$answer)
	{
		$Формулировка= $question->На_голосование->Формулировка;
		$answer= (object)array('На_вопрос'=>array('Номер'=>$answer_number,'Формулировка'=>$Формулировка));
		$answers[]= $answer;
	}
	$answer->Ответ= $data['Ответ'];

	execute_query_no_result("update Vote set Answers=? where id_Vote=?;",array('ss',nice_json_encode($answers),$id_Vote));

	$row_log= PrepareLog($id_Vote,'Выбран ответ',$answer_number);

	$row= array('Журнал'=>array($row_log));
	echo nice_json_encode($row);
	exit;
}

function readVote($id_Vote)
{
	$txt_query= "select 
		uncompress(v.Member) Member 
		,date_format(cm.Date,'%d.%m.%Y %H:%i') Date
		,uncompress(cm.Body) Body
		,d.Name debtorName
		,m.firstName au_firstName
		,m.lastName au_lastName
		,m.middleName au_middleName
		,v.Confirmation_code Confirmation_code
		,v.Confirmation_code_time Confirmation_code_time
		,v.Answers
	from Vote v
	inner join Meeting cm on v.id_Meeting=cm.id_Meeting
	inner join MProcedure p on p.id_MProcedure=cm.id_MProcedure
	inner join Debtor d on d.id_Debtor=p.id_Debtor 
	inner join Manager m on m.id_Manager=p.id_Manager
	where id_Vote=?";
	$rows= execute_query($txt_query,array('s',$id_Vote));
	$count_rows= count($rows);
	if (1!=$count_rows)
		exit_not_found("found $count_rows Votes for id_Vote=$id_Vote");
	$vote= $rows[0];
	$vote->id_Vote= $id_Vote;
	$vote->Member= json_decode($vote->Member);
	$vote->Member->id_Vote= $id_Vote;
	$vote->ФИО= $vote->Member->Фамилия.' '.$vote->Member->Имя.' '.$vote->Member->Отчество;
	$vote->АУ= $vote->au_lastName.' '.$vote->au_firstName.' '.$vote->au_middleName;
	return $vote;
}

function safeSendSmsConfirmationCode($Confirmation_code,$vote)
{
	global $sms_settings;
	if (false==$sms_settings->enabled)
	{
		return false;
	}
	else
	{
		$txt= "{$Confirmation_code} – код подтверждения подписи ИС «Витрина данных ПАУ» для заседания комитета кредиторов {$vote->debtorName} назначенного на {$vote->Date}";
		$sms = new SMSTransport($sms_settings);
		write_to_log("send sms to {$vote->Member->телефон}: $txt");
		$res= $sms->send($vote->Member->телефон,$txt,$sms_settings->FROM);
		write_to_log($res);
		return $res;
	}
}

function throw_code()
{
	global $sms_settings, $email_settings;

	$row= readVote($_GET['id_Vote']);
	$участник= $row->Member;
	$post_data= safe_POST($_POST);

	$row_Vote_doc_bulletin= PrepareDocument($row->id_Vote,safe_date_create(),'b',$post_data['text']);

	$ФИО_участника= $участник->Фамилия.' '.$участник->Имя.' '.$участник->Отчество;
	$row_SentEmail_doc= Направить_email_участнику_КК($row, $участник, $ФИО_участника, 'Письмо о подписи',array(
		'вложения'=>array($row_Vote_doc_bulletin)
		,'параметры'=>(object)array(
			'бюллетень'=> (object)array(
				'md5'=> $row_Vote_doc_bulletin->doc_md5
				,'длина'=> $row_Vote_doc_bulletin->Размер
				,'FileName'=>$row_Vote_doc_bulletin->FileName
			)
		)
	));

	$log_time= safe_date_create();
	$log_time_json= date_format($log_time,'d.m.Y H:i:s');

	$Confirmation_code=  (true!=$email_settings->enabled) 
		? '1234' : generate_password(4,array('0','1','2','3','4','5','6','7','8','9'));
	safeSendSmsConfirmationCode($Confirmation_code,$row);

	$txt_query= "update Vote set Confirmation_code=?, Confirmation_code_time=? where id_Vote=?";
	execute_query_no_result($txt_query,array('sss',$Confirmation_code,date_format($log_time,'Y-m-d\TH:i:s'),$row->id_Vote));

	$row_Vote_log_sms= PrepareLog($row->id_Vote,'Отправлен код',$участник->телефон);

	$res= array(
		  'sms'=>array('number'=>$участник->телефон, 'time'=>$log_time_json)
		, 'email'=>array('address'=>$участник->email, 'time'=>$row_SentEmail_doc->TimeDispatch)
		, 'Документы'=>array($row_Vote_doc_bulletin, $row_SentEmail_doc)
		, 'Журнал'=>array($row_SentEmail_doc->row_Vote_log,$row_Vote_log_sms)
	);
	if (false==$sms_settings->enabled)
		$res['test_code']= $Confirmation_code;
	echo nice_json_encode($res);
	exit;
}

function findSentEmail($vote,$EmailType)
{
	$count= count($vote->Документы);
	for ($i= $count-1; $i>=0; $i--)
	{
		$doc= $vote->Документы[$i];
		if (isset($doc->EmailType) && $doc->EmailType==$EmailType)
			return $doc;
	}
	return null;
}

function findVote_document($vote,$Vote_document_type)
{
	$count= count($vote->Документы);
	for ($i= $count-1; $i>=0; $i--)
	{
		$doc= $vote->Документы[$i];
		if (isset($doc->Vote_document_type) && $doc->Vote_document_type==$Vote_document_type)
			return $doc;
	}
	return null;
}

function findVote_log($vote,$Vote_log_type)
{
	$count= count($vote->Журнал);
	for ($i= $count-1; $i>=0; $i--)
	{
		$log= $vote->Журнал[$i];
		if ($log->Vote_log_type==$Vote_log_type)
			return $log;
	}
	return null;
}

function prepareSignature($vote, $log_time)
{
	global $sms_settings, $email_settings;
	$log_time_json= date_format($log_time,'d.m.Y H:i:s');

	$письмо_уведомление_о_заседании= findSentEmail($vote,'n');
	$запись_о_входе_в_кабинет= findVote_log($vote,'l');
	$запись_об_отправке_кода= findVote_log($vote,'c');
	$документ_бюллетень= findVote_document($vote,'b');
	$письмо_о_голосовании= findSentEmail($vote,'a');

	ob_start();
	include "../assets/actions/backend/meeting/views/documents/signature.php";
	$signature= fix_end_of_lines(ob_get_contents());
	ob_end_clean();

	return $signature;
}

function catch_code_ok($id_Vote,$vote,$data)
{
	$ЖурналДокументы= array('id_Vote'=>$id_Vote);
	Load_ЖурналДокументы($ЖурналДокументы);
	$vote->Журнал= $ЖурналДокументы['Журнал'];
	$vote->Документы= $ЖурналДокументы['Документы'];

	$log_time= safe_date_create();

	$txt_signature= prepareSignature($vote, $log_time);
	$row_Vote_document_sig= PrepareDocument($id_Vote,$log_time,'s',$txt_signature);

	$operator_signature= prepareSignatureOfOperator($txt_signature);
	$row_Vote_document_rit_sig= PrepareDocument($id_Vote,safe_date_create(),'r',$operator_signature);

	$участник= $vote->Member;
	$ФИО_участника= $участник->Фамилия.' '.$участник->Имя.' '.$участник->Отчество;
	$row_SentEmail_doc= Направить_email_участнику_КК($vote, $участник, $ФИО_участника, 'Подписан бюллетень',array(
		'вложения'=>array($row_Vote_document_sig,$row_Vote_document_rit_sig)
		,'параметры'=>(object)array(
			'row_Vote_document_sig'=> $row_Vote_document_sig
			,'row_Vote_document_rit_sig'=> $row_Vote_document_rit_sig
		)
	));

	$log_time_json= date_format(safe_date_create(),'d.m.Y H:i:s');

	$answers= json_decode($vote->Answers);
	foreach ($answers as $answer)
		$answer->Подписано= $log_time_json;
	execute_query_no_result("update Vote set Answers=?, Confirmation_code=null, Confirmation_code_time=null where id_Vote=?;",array('ss',nice_json_encode($answers),$id_Vote));

	$res= array('ok'=>true,'Подписано'=>$log_time_json, 'Журнал'=>array($row_SentEmail_doc->row_Vote_log), 
				'Документы'=>array($row_Vote_document_sig,$row_Vote_document_rit_sig,$row_SentEmail_doc));
	echo nice_json_encode($res);
	exit;
}

function total_minutes($date_diff)
{
	$minutes = $date_diff->days * 24 * 60;
	$minutes += $date_diff->h * 60;
	$minutes += $date_diff->i;
	return $minutes;
}

function Clear_Confirmation_code($vote)
{
	$txt_query= "update Vote set Confirmation_code=null, Confirmation_code_time=null where id_Vote=?";
	execute_query_no_result($txt_query,array('s',$vote->id_Vote));
}

function catch_code()
{
	$id_Vote= $_GET['id_Vote'];
	$vote= readVote($id_Vote);
	$posted_data= safe_POST($_POST);
	$received_code= $posted_data['code'];

	$current_time= safe_date_create();
	$Confirmation_code_time= date_create_from_format('Y-m-d H:i:s',$vote->Confirmation_code_time);
	$diff= date_diff($current_time,$Confirmation_code_time);

	$max_minutes_to_confirm_sms_code= 30;
	if (total_minutes($diff)>$max_minutes_to_confirm_sms_code)
	{
		PrepareLog($vote->id_Vote,'Код прислан поздно',"\"$received_code\"");
		Clear_Confirmation_code($vote);
		echo nice_json_encode(array('ok'=>false,'why'=>"Код подтверждения предъявлен слишком поздно (спустя более $max_minutes_to_confirm_sms_code минут )!"));
	}
	else if ($received_code!=$vote->Confirmation_code)
	{
		PrepareLog($vote->id_Vote,'Неверный код',"\"$received_code\" вместо высланного \"{$vote->Confirmation_code}\"");
		Clear_Confirmation_code($vote);
		echo nice_json_encode(array('ok'=>false,'why'=>'Указанный код подтверждения не соответствует высланному!'));
	}
	else
	{
		catch_code_ok($id_Vote,$vote,$posted_data);
	}
}

switch ($cmd)
{
	case 'store-answer': store_answer(); break;
	case 'throw-code': throw_code(); break;
	case 'catch-code': catch_code(); break;
	default:
		exit_bad_request("unknown cmd=$cmd");
}