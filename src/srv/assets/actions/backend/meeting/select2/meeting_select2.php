<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/constants/alib_constants.php';
require_once '../assets/actions/backend/constants/debtorName.etalon.php';

$auth_info= CheckAuthManager();

CheckMandatoryGET_id('id_Manager');
$id_Manager= $_GET['id_Manager'];
CheckAccessToManagerIfManager($auth_info, $id_Manager);
CheckMandatoryGET('q');

$txt_query= "select 
		  cm.id_Meeting id
		, date_format(cm.Date,'%d.%m.%Y %H:%i') Date
		, d.Name debtorName
		, ProcedureType_SafeShortForDBValue(mp.procedure_type) Type
	from Meeting cm 
	inner join MProcedure mp on cm.id_MProcedure=mp.id_MProcedure 
	inner join Debtor d on d.id_Debtor=mp.id_Debtor 
	inner join Manager m on mp.id_Manager = m.id_Manager 
	where mp.id_Manager=? 
		&& (d.Name like ? 
			|| instr(concat(date_format(cm.Date,'%d.%m.%Y %H:%i'), ' (', d.Name,', ',ProcedureType_SafeShortForDBValue(mp.procedure_type), ')'),?) > 0)
	order by cm.Date desc 
	limit 100;";
$rows= execute_query($txt_query,array('iss',$auth_info->id_Manager,$_GET['q'].'%',$_GET['q']));

foreach ($rows as $row)
{
	$row->text= $row->Date.' ('.beautify_debtorName($row->debtorName).', '.$row->Type.')';
	unset($row->Date);
	unset($row->debtorName);
	unset($row->Type);
}

$res= array('results'=>$rows);

$res['pagination']= array('more'=>true);

echo nice_json_encode($res);
