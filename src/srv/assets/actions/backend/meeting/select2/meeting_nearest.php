<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/meeting/alib_meeting.php';

$auth_info= CheckAuthManager();

CheckMandatoryGET_id('id_Manager');
$id_Manager= $_GET['id_Manager'];
CheckAccessToManagerIfManager($auth_info, $id_Manager);

$txt_query= "select 
		  cm.id_Meeting id_Meeting
		, concat(date_format(cm.Date,'%d.%m.%Y %H:%i'), ' (', d.Name,', ',ProcedureType_SafeShortForDBValue(mp.procedure_type), ')') select2_text

		, cm.Date Date
		, uncompress(cm.Body) Body

		, d.Name debtorName
		, d.INN debtorInn
		, d.OGRN debtorOgrn
		, d.SNILS debtorSnils
	from Meeting cm 
	inner join MProcedure mp on cm.id_MProcedure=mp.id_MProcedure 
	inner join Debtor d on d.id_Debtor=mp.id_Debtor 
	inner join Manager m on mp.id_Manager = m.id_Manager 
	where mp.id_Manager=?
	order by cm.Date desc 
	limit 1;";
$rows= execute_query($txt_query,array('i',$auth_info->id_Manager));

$count_count= count($rows);

if (0==$count_count)
{
	echo nice_json_encode(null);
}
else
{
	$row_Meeting= $rows[0];

	$meeting= build_Meeting_data($row_Meeting);

	$result= array
	(
		'selected_data'=> array(
			  'id'=>$row_Meeting->id_Meeting
			, 'text'=>$row_Meeting->select2_text
		)
		,'meeting_info'=> $meeting
	);

	echo nice_json_encode($result);
}

