<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/meeting/alib_meeting.php';

global $auth_info;
$auth_info= CheckAuthManager();

class Meeting_crud extends Base_crud
{
	function create($meeting)
	{
		global $auth_info;
		if(isset($meeting['Участники']) && $meeting['Участники'])
			fix_phone_numbers($meeting);
		$date= date_create_from_format('d.m.Y H:i',$meeting['Дата_заседания'].' '.$meeting['Время_заседания']);
		$date_sql= date_format($date,'Y-m-d\TH:i:s');
		$txt_query= "insert into Meeting set id_MProcedure=?, Date=?, Body=compress(?);";
		$id_Meeting= execute_query_get_last_insert_id($txt_query,
					array('sss',$meeting['id_MProcedure'],$date_sql,json_encode($meeting)));
		echo "{\"id\":$id_Meeting}";
	}

	function read($id_Meeting)
	{
		global $auth_info;
		$txt_query= "select 
			  cm.id_Meeting id_Meeting

			, cm.Date Date
			, uncompress(cm.Body) Body

			, d.Name debtorName
			, d.INN debtorInn
			, d.OGRN debtorOgrn
			, d.SNILS debtorSnils
		from Meeting cm 
		inner join MProcedure mp on cm.id_MProcedure=mp.id_MProcedure 
		inner join Debtor d on d.id_Debtor=mp.id_Debtor 
		inner join Manager m on mp.id_Manager = m.id_Manager 
		where mp.id_Manager=? && cm.id_Meeting= ?
		;";
		$rows= execute_query($txt_query,array('is',$auth_info->id_Manager,$id_Meeting));
		if (1!=count($rows))
			exit_not_found("can not find meeting id_Meeting=$id_Meeting");

		$meeting= build_Meeting_data($rows[0]);
		$json_res= nice_json_encode($meeting);
		echo $json_res;
	}

	function update($id,$meeting)
	{
		if(isset($meeting['Участники']) && $meeting['Участники'])
			fix_phone_numbers($meeting);
		$txt_query= "update Meeting set Body=compress(?) where id_Meeting=?;";
		execute_query_no_result($txt_query,array('si',json_encode($meeting),$id));
		echo '{ "ok": true }';
	}

	function delete($id_Meetings)
	{
		$txt_query= "delete m 
			from Meeting m 
			left join Vote v on m.id_Meeting=v.id_Meeting 
			where m.id_Meeting=? && v.id_Meeting is null;";
		$affected_rows= execute_query_get_affected_rows($txt_query,array('i',$id_Meetings[0]));
		if (0!=$affected_rows)
		{
			echo '{ "ok": true }';
		}
		else
		{
			echo '{ "ok": false, "reason":"В базе нет засаданий комитета кредитора с таким идентификатором, голосование для которых не начато." }';
		}
	}
}

$crud= new Meeting_crud();
$crud->process_cmd();

