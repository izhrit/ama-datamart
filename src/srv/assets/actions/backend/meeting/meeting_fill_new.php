<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/time.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/meeting/alib_meeting.php';

$auth_info= CheckAuthManager();
CheckMandatoryGET_id('id_MProcedure');
$id_MProcedure= intval($_GET['id_MProcedure']);

$txt_query= "select 
	 d.Name debtorName
	,d.INN debtorInn
	,d.OGRN debtorOgrn
	,d.SNILS debtorSnils
	,uncompress(m.ExtraParams) as m_ExtraParams
	,uncompress(mp.ExtraParams) as mp_ExtraParams
	,uncompress(c.Body) as c_Body
	,exists (select mm.id_Meeting from Meeting mm inner join MProcedure mmp on mm.id_MProcedure=mmp.id_MProcedure where mmp.id_Manager=m.id_manager) Offer_accepted
from MProcedure mp
inner join Debtor d on d.id_Debtor=mp.id_Debtor 
inner join Manager m on mp.id_Manager=m.id_manager
left join Meeting c on c.id_MProcedure=mp.id_MProcedure
where mp.id_MProcedure=?
order by c.Date desc
limit 1
";
$rows= execute_query($txt_query,array('i',$id_MProcedure));

$row = $rows[0];
$dtnow = safe_date_create();
$dtзаседания= date_add($dtnow,date_interval_create_from_date_string("15 days"));

$Время_по_умолчанию= (object)array(
	'Заседания'=>'18:00'
	,'Приёмное'=>(object)array(
		'Начало'=>'15:00'
		,'Конец'=>'16:00'
	)
);

$Заседание= (object)array(
	'Должник'=>array(
		'Наименование'=> $row->debtorName
		,'ИНН'=> $row->debtorInn
		,'ОГРН'=> $row->debtorOgrn
		,'СНИЛС'=> $row->debtorSnils
	)
	,'Время_заседания'=> $Время_по_умолчанию->Заседания
	,'Дата_заседания'=> date_format($dtзаседания,'d.m.Y')
	,'Ознакомление'=>(object)array(
		'С_материалами'=>(object)array(
			 'С_даты'=>date_format(date_add($dtзаседания,date_interval_create_from_date_string("-8 days")),'d.m.Y')
			,'Время'=> (object)array(
				 'Начало'=>$Время_по_умолчанию->Приёмное->Начало
				,'Конец'=>$Время_по_умолчанию->Приёмное->Конец
			)
		)
		,'С_результатами'=>(object)array(
			 'До_даты'=>date_format(date_add($dtзаседания,date_interval_create_from_date_string("7 days")),'d.m.Y')
			,'Время'=> (object)array(
				 'Начало'=>$Время_по_умолчанию->Приёмное->Начало
				,'Конец'=>$Время_по_умолчанию->Приёмное->Конец
			)
		)
	)
);

$Заседание->Offer_accepted= ($row->Offer_accepted) ? 'true' : 'false';

if (null!=$row->m_ExtraParams)
{
	$p= json_decode($row->m_ExtraParams);
	if (null!=$p)
	{
		if (isset($p->Телефон) && null!=$p->Телефон && ''!=$p->Телефон)
			$Заседание->Телефон_АУ = $p->Телефон;
		$С_материалами= $Заседание->Ознакомление->С_материалами;
		$С_результатами= $Заседание->Ознакомление->С_результатами;
		if (isset($p->Адрес->Рабочий) && null!=$p->Адрес->Рабочий && ''!=$p->Адрес->Рабочий)
		{
			$рабочий_адрес= $p->Адрес->Рабочий;
			$С_материалами->Адрес= $рабочий_адрес;
			$С_результатами->Адрес= $рабочий_адрес;
		}
		if (isset($p->Приёмное_время->С) && null!=$p->Приёмное_время->С && ''!=$p->Приёмное_время->С)
		{
			$начало_приёмного_времени= $p->Приёмное_время->С;
			$С_материалами->Время->Начало= $начало_приёмного_времени;
			$С_результатами->Время->Начало= $начало_приёмного_времени;
		}
		if (isset($p->Приёмное_время->До) && null!=$p->Приёмное_время->До && ''!=$p->Приёмное_время->До)
		{
			$конец_приёмного_времени= $p->Приёмное_время->До;
			$С_материалами->Время->Конец= $конец_приёмного_времени;
			$С_результатами->Время->Конец= $конец_приёмного_времени;
		}
	}
}

if (null!=$row->mp_ExtraParams)
{
	$p= json_decode($row->mp_ExtraParams);
	if (null!=$p && isset($p->Адрес) && ''!=$p->Адрес && null!=$p->Адрес)
		$Заседание->Должник= array('Местонахождение' => $p->Адрес);
}

if (null!=$row->c_Body)
{
	$p= json_decode($row->c_Body);
	if (isset($p->Участники) && ''!=$p->Участники && null!=$p->Участники)
		$Заседание->Участники= $p->Участники;
}

echo nice_json_encode($Заседание);

