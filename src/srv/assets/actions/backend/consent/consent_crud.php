<?
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/starts/consent_crud_lib.php';

global $auth_info;
$auth_info= CheckAuthSRO();

class consent_crud extends Base_crud
{
	function create($consent) {
		$consent = validate_consent($consent, 'create');

		if(isset($consent->АУ['id'])){
			$consent->АУ['id'] = create_au_if_not_isset($consent->АУ['id']); //ArbitrManagerID to id_Manager
		}

		create_consent($consent);

		echo '{ "ok": true }';
	}

	function read($id_ProcedureStart)
	{
		$consent = read_consent($id_ProcedureStart);
		if (0==$consent)
		{
			exit_not_found("can not find start id_ProcedureStart=$id_ProcedureStart");
		}
		else
		{
			echo nice_json_encode($consent);
		}
	}

	function update($id_ProcedureStart,$consent)
	{
		$consent = validate_consent($consent, 'update');
	
		if(isset($consent->АУ['id'])){
			$consent->АУ['id'] = create_au_if_not_isset($consent->АУ['id']); //ArbitrManagerID to id_Manager
		}

		$affected_rows= update_consent($id_ProcedureStart,$consent);

		if ($affected_rows>=1)
		{
			echo '{ "ok": true }';
		}
		else if (0==$affected_rows)
		{
			echo '{ "ok": true, "affected": 0 }';
		}
		else
		{
			exit_bad_request("affected $affected_rows when update id_ProcedureStart=$id_ProcedureStart");
		}
	}

	function delete($ids_ProcedureStart)
	{
		$result = delete_consent($ids_ProcedureStart);
		if($result === 'no ProcedureStart found for this sro') {
			echo '{"ok": false, "message": "Нет доступа к удалению данного согласия"}';
		} else if($result === 'cant delete ProcedureStart with choosed manager in MRequest') {
			echo '{"ok": false, "message": "АУ из данного согласия выбран в ответе на запрос АУ из суда"}';
		} else if ($result) {
			echo '{ "ok": true }';
		} else {
			exit_not_found("can not delete ProcedureStart!");
		}
	}
}
$crud= new consent_crud();
if($_GET['cmd'] !== 'actualize') {
	$crud->process_cmd();
}

