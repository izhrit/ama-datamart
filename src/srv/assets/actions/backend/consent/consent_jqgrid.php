<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthSRO();

$fields= "
	id_ProcedureStart
	,DebtorCategory
	,DebtorName debtorName
	,DebtorName2 debtorName2
	,DATE_FORMAT(ps.DateOfApplication, '%d.%m') DateOfApplication
	,c.ShortName Court
	,concat(m.lastName,' ',left(m.firstName,1),'.',left(m.middleName,1),'.') Manager
	,CaseNumber caseNumber
	,DATE_FORMAT(ps.NextSessionDate, '%d.%m %H:%i') NextSessionDate
	,ps.AddedBySRO addedBySRO
	,ps.id_MRequest
	,ReadyToRegistrate ReadyToRegistrate
";

$from_where= "from ProcedureStart ps 
inner join Court c on ps.id_Court=c.id_Court 
left join Manager m on ps.id_Manager=m.id_Manager
where (m.id_SRO=$auth_info->id_SRO and (ps.ShowToSRO!=0 or ps.AddedBySRO=1))";

$filter_rule_builders= array(
	'debtorName'=>function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and (DebtorName like '$data%' or DebtorName2 like '$data%')";
	}
	,'caseNumber'=>'std_filter_rule_builder'
	,'Court'=>prep_std_filter_rule_builder_for_expression('c.SearchName')
	,'Manager'=>prep_std_filter_rule_builder_for_expression('m.lastName')
);

if (isset($_GET['sidx'])) {
	$_GET['sidx'] = str_replace('DateOfApplication','ps.DateOfApplication',$_GET['sidx']);
	$_GET['sidx'] = str_replace('NextSessionDate','ps.NextSessionDate',$_GET['sidx']);
}
if (!isset($_GET['sidx']) || ''==$_GET['sidx'])
	$_GET['sidx']= 'ps.DateOfApplication,debtorName';

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
