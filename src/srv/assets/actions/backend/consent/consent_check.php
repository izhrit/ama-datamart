<?php

require_once '../assets/helpers/db.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/starts/consent_crud_lib.php';
require_once '../assets/libs/starts/casebookapi.php';

global $auth_info;
$auth_info= CheckAuthSRO();

function DoActualize()
{
	global $auth_info;
	write_to_log('DoActualize {');

	$Время_сверки= safe_date_create();
	$consent= safe_POST($_POST);
	$consent['TimeOfLastChecking']= $Время_сверки;
	$consent['AddedBySRO']= 1;
	
	$id_ProcedureStart= null;

	if(isset($consent['АУ']['id'])){
		$consent['АУ']['id'] = create_au_if_not_isset($consent['АУ']['id']); //ArbitrManagerID to id_Manager
	}

	if (!isset($consent['id_ProcedureStart']))
	{
		$consent = validate_consent($consent, 'create');
		$id_ProcedureStart= create_consent($consent);
	}
	else
	{
		$consent = validate_consent($consent, 'update');
		$id_ProcedureStart= $consent->id_ProcedureStart;
		update_consent($id_ProcedureStart,$consent);
	}

	echo nice_json_encode(array(
		'id_ProcedureStart'=>$id_ProcedureStart
		,'Время_сверки'=>date_format($Время_сверки,'d.m.Y H:i:s')
	));
	write_to_log('DoActualize }');
}

function prepare_cases_for_kad_arbitr_ru($consent)
{
	$Должник= (object)$consent['Должник'];
	$DebtorSnils= null;
	$DebtorName= null;
	if(isset($Должник->Тип)) {
		switch ($Должник->Тип)
		{
			case 'Физ_лицо':
				if(isset($Должник->Физ_лицо)) {
					$np= (object)$Должник->Физ_лицо;
					$DebtorName= "{$np->Фамилия} {$np->Имя} {$np->Отчество}";
					$DebtorSnils= $np->СНИЛС;
				}
				break;
			case 'Юр_лицо':
				$DebtorName= $Должник->Юр_лицо['Наименование'];
				break;
		}
	}

	$txt_query= 'select CasebookTag from Court where id_Court=?;';
	$rows= execute_query($txt_query,array('s',$consent['Заявление_на_банкротство']['В_суд']['id']));
	$row= $rows[0];

	$cases= array(
		array(
			'id'=>'1'
			,'debtor'=>array(
				'Name'=>$DebtorName
				,'INN'=>!isset($Должник->ИНН)?null:$Должник->ИНН
				,'OGRN'=>!isset($Должник->ОГРН)?null:$Должник->ОГРН
				,'SNILS'=>$DebtorSnils
			)
			,'CourtTag'=>$row->CasebookTag
		)
	);
	return $cases;
}

function prepare_CasebookAPI()
{
	global $use_casebook_api, $use_casebook_auth;
	return new CasebookAPI((object)array('base_url'=>$use_casebook_api,'auth'=>$use_casebook_auth));
}

function DoSearch()
{
	write_to_log('DoSearch {');

	global $auth_info;

	$Время_сверки= safe_date_create();
	$consent= safe_POST($_POST);

	$consent['TimeOfLastChecking']= $Время_сверки;
	if(isset($consent['АУ']['id'])){
		$consent['АУ']['id'] = create_au_if_not_isset($consent['АУ']['id']); //ArbitrManagerID to id_Manager
	}
	$consent['AddedBySRO']= 1;
	$cases= prepare_cases_for_kad_arbitr_ru($consent);
	try
	{
		$cryptoapi= prepare_CasebookAPI();
		$details= $cryptoapi->FindOutDetailsOfTheCases($cases);
		$details_by_id= prep_details_by_id($details);
		if (isset($details_by_id['1']))
		{
			$dc= $details_by_id['1'];
			$consent['Номер_судебного_дела']= $dc->CaseNumber;
			$consent['Заявление_на_банкротство']['Дата_рассмотрения']= $dc->NextSessionDate;
		}
	}
	catch (Exception $ex)
	{
		write_to_log('catched exception on FindOutDetailsOfTheCases: ' . get_class($ex) . ' - ' . $ex->getMessage());
	}

	$id_ProcedureStart= null;
	if (!isset($consent['id_ProcedureStart']))
	{
		$consent = validate_consent($consent, 'create');
		$id_ProcedureStart= create_consent($consent);
	}
	else
	{
		$consent = validate_consent($consent, 'update');
		$id_ProcedureStart= $consent->id_ProcedureStart;
		update_consent($id_ProcedureStart,$consent);
	}

	echo nice_json_encode(array(
		 'id_ProcedureStart'=>$id_ProcedureStart
		,'Время_сверки'=>date_format($Время_сверки,'d.m.Y H:i:s')
		,'Дата_рассмотрения'=>(isset($consent->Заявление_на_банкротство) && isset($consent->Заявление_на_банкротство->Дата_рассмотрения))
								? $consent->Заявление_на_банкротство->Дата_рассмотрения : null
		,'Номер_судебного_дела'=>!isset($consent->Номер_судебного_дела)?null:$consent->Номер_судебного_дела
	));

	write_to_log('DoSearch }');
}

CheckMandatoryGET_execute_action_variant('cmd',array(
	 'actualize'=> function(){DoActualize();}
	,'search'=>    function(){DoSearch();}
));
