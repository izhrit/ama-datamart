<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/starts/start_crud_lib.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManager();
$id_MRequest= CheckMandatoryGET_id('id');

if ($auth_info->category === 'customer') {
	$id_Manager= !isset($_POST['id_Manager'])?null:$_POST['id_Manager'];
}else {
	$id_Manager= !isset($auth_info->id_Manager)?null:$auth_info->id_Manager;
}
$id_Contract= null;
if(isset($auth_info->id_Contract))
	$id_Contract= $auth_info->id_Contract;
if($id_Manager==null)
	exit_unauthorized("can not create ProcedureStart based on MRequest without id_Manager!");

$txt_query= "insert into ProcedureStart (id_Court, CaseNumber,
			DebtorName, DebtorINN, DebtorSNILS, DebtorOGRN, DebtorAddress, DebtorCategory,
			DebtorName2, DebtorINN2, DebtorSNILS2, DebtorOGRN2, DebtorAddress2, DebtorCategory2,
			ApplicantName, ApplicantINN, ApplicantSNILS, ApplicantOGRN, ApplicantAddress, ApplicantCategory,
			CourtDecisionURL, DateOfRequestAct, CourtDecisionAddInfo,
			NextSessionDate,
			ShowToSRO, TimeOfCreate, id_MRequest, id_Contract, id_Manager)

			select mr.id_Court, mr.CaseNumber,
			mr.DebtorName, mr.DebtorINN, mr.DebtorSNILS, mr.DebtorOGRN, mr.DebtorAddress, mr.DebtorCategory,
			mr.DebtorName2, mr.DebtorINN2, mr.DebtorSNILS2, mr.DebtorOGRN2, mr.DebtorAddress2, mr.DebtorCategory2,
			mr.ApplicantName, mr.ApplicantINN, mr.ApplicantSNILS, mr.ApplicantOGRN, mr.ApplicantAddress, mr.ApplicantCategory,
			mr.CourtDecisionURL, mr.DateOfRequestAct, mr.CourtDecisionAddInfo,
			mr.NextSessionDate,
			1, now(), ?, ?, ?

			from MRequest as mr
			where id_MRequest=? 
			and not exists ( 
			  select 1 
			  from ProcedureStart as ps 
			  where ps.id_Manager=? and ps.id_MRequest=?)";
$params= array('iiiiii', $id_MRequest, $id_Contract, $id_Manager, $id_MRequest, $id_Manager, $id_MRequest);
$result= execute_query_get_last_insert_id($txt_query,$params);

if(0!==$result) {
	echo '{ "ok": true }';
}else {
	echo '{ "ok": false, "message": "Согласие уже подано!" }';
}
