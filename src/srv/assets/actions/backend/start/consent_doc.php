<?
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/time.php';

require_once '../assets/libs/auth/check.php';

require_once '../assets/libs/tpl/tpl_by_sro_and_filename.php';
require_once '../assets/libs/tpl/tpl_mrequest_params.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManager();

$id_ProcedureStart= CheckMandatoryGET_id('id');

$txt_query= "select

  ps.CaseNumber
, ps.ApplicantName
, ps.ApplicantAddress
, ps.DebtorName
, ps.DebtorINN
, ps.DebtorAddress
, ps.NextSessionDate

, m.lastName ManagerLastName
, m.firstName ManagerFirstName
, m.middleName ManagerMiddleName
, m.INN ManagerINN
, m.efrsbNumber
, m.id_Manager
, em.CorrespondenceAddress

, c.Name CourtName
, c.Address CourtAddress

, es.ShortTitle sro_ShortTitle
, es.Title sro_Title
, es.SROEmail sro_Email
, es.UrAdress sro_UrAdress
, es.CEOName sro_CEOName
, es.RegNum sro_RegNum
, es.id_SRO id_SRO

from ProcedureStart ps
inner join Manager m on ps.id_Manager=m.id_Manager
inner join Court c on ps.id_Court=c.id_Court
inner join efrsb_sro es on m.id_SRO=es.id_SRO
left join efrsb_manager em on em.RegNum=m.efrsbNumber
where ps.id_ProcedureStart=? and ps.id_Contract=?";
$params=array('ii',$id_ProcedureStart,$auth_info->id_Contract);
if(isset($auth_info->id_Manager)) {
    $txt_query.=' and ps.id_Manager=?';
    $params[0].='i';
    $params[]=$auth_info->id_Manager;
}
$rows= execute_query($txt_query,$params);

$count_rows= count($rows);
if (1!=$count_rows)
	exit_not_found("found $count_rows rows ProcedureStart where id_ProcedureStart=$id_ProcedureStart");

$row= $rows[0];

$tpl_par_Должник= prep_mrequest_tpl_par_Должник($row);

$current_time= safe_date_create();
$DateOfResponce= date_format($current_time,'Y-m-d\TH:i:s');

$tpl_par= (object)array(
	 'Заявитель'=> prep_mrequest_tpl_par_Заявитель($row, $tpl_par_Должник)
	,'Должник'=> $tpl_par_Должник
	,'Дата_заявления'=>$DateOfResponce
	,'Суд'=> (object)array(
		'Наименование'=> $row->CourtName
		,'Адрес'=> $row->CourtAddress
		,'Дело'=>(object)array(
			'Номер'=>$row->CaseNumber
			,'Время_следующего_заседания'=>$row->NextSessionDate
		)
	)
	,'СРО'=>prep_mrequest_tpl_par_СРО($row)
	,'АУ'=> prep_mrequest_tpl_par_АУ($row)
);

$sro_tpl_folder= get_sro_tpl_folder_by_RegNum($row->sro_RegNum);
$tpl_path= get_tpl_path_for_sro_tpl_folder($sro_tpl_folder, 'consent');
update_tpl_par_for_sro($sro_tpl_folder, $tpl_par);

$filename= 'САУ';
if (isset($row->DebtorName))
	$filename.= '_'.str_replace(' ','_',$row->DebtorName);
$filename.= '.doc';

header("Content-Type: application/doc");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=" . urlencode($filename));
require_once $tpl_path;
