<?
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/requests/request_crud_lib.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManager();

$request = json_decode(file_get_contents('php://input'), true);

function get_id_SRO_for_manager_or_customer($id_ProcedureStart)
	{
		global $auth_info;

		$txt_query = 'select m.id_SRO 
		from Manager m
		inner join ProcedureStart ps on m.id_Manager=ps.id_Manager
		where id_ProcedureStart=? and (1=0';

		$params= array('i', $id_ProcedureStart);
		if (isset($auth_info->id_Contract) && null != $auth_info->id_Contract) {
			$txt_query .= ' or m.id_Contract=?';
			$params[0] .= 'i';
			$params[] = $auth_info->id_Contract;
		}
		if (isset($auth_info->id_Manager) && null != $auth_info->id_Manager) {
			$txt_query .= ' or m.id_Manager=?';
			$params[0] .= 'i';
			$params[] = $auth_info->id_Manager;
		}
		$txt_query.= ')';

		$rows= execute_query($txt_query,$params);

		if(1!==count($rows))
			exit_bad_request('Found '.count($rows).' records in ProcedureStart with id_ProcedureStart='.$id_ProcedureStart);

		return $rows[0]->id_SRO;
	}

function check_if_exist_request_with_decision_URL($CourtDecisionURL)
{
	$txt_query = 'select id_MRequest
	from MRequest mr
	where CourtDecisionURL=?';
	$params= array('s', $CourtDecisionURL);
	$rows= execute_query($txt_query, $params);

	return 0!==count($rows);
}

$request = validate_request($request);
$isExistRequestWithURL = check_if_exist_request_with_decision_URL($request->Запрос->Ссылка);

if(!$isExistRequestWithURL) {
	if(!isset($request->id_ProcedureStart)) {
		exit_bad_request('Can\'t create MRequest as customer/manager: id_ProcedureStart required');
	}
	$request->id_SRO = get_id_SRO_for_manager_or_customer($request->id_ProcedureStart);

	create_request($request);
}

echo '{ "ok": true }';





