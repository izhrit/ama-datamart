<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManager();

$fields= "
	id_ProcedureStart
	,DebtorName debtorName
	,DebtorName2 debtorName2
	,DebtorCategory DebtorCategory
	,CaseNumber caseNumber
	,c.ShortName Court
	,DATE_FORMAT(ps.DateOfApplication, '%d.%m') DateOfApplication
	,DATE_FORMAT(ps.TimeOfLastChecking, '%d.%m %H:%i') TimeOfLastChecking
	,DATE_FORMAT(ps.NextSessionDate, '%d.%m %H:%i') NextSessionDate
	,concat(m.lastName,' ',left(m.firstName,1),'.',left(m.middleName,1),'.') Manager
	,ReadyToRegistrate ReadyToRegistrate
	,ps.AddedBySRO addedBySRO
	,ps.id_MRequest
";

$from_where= 'from ProcedureStart ps 
inner join Court c on ps.id_Court=c.id_Court 
left join Manager m on m.id_Manager=ps.id_Manager
where (1=0';
if (isset($auth_info->id_Manager))
{
	$from_where.= " or ps.id_Manager={$auth_info->id_Manager}";
}
else if (isset($auth_info->id_Contract))
{
	$from_where.= " or ps.id_Contract={$auth_info->id_Contract} or m.id_Contract={$auth_info->id_Contract}";
}
$from_where.= ')';

$filter_rule_builders= array(
	'debtorName'=>function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and (DebtorName like '$data%' or DebtorName2 like '$data%')";
	}
	,'caseNumber'=>'std_filter_rule_builder'
	,'Court'=>prep_std_filter_rule_builder_for_expression('c.SearchName')
	,'Manager'=>prep_std_filter_rule_builder_for_expression('m.lastName')
);
if (isset($_GET['sidx'])) {
	$_GET['sidx'] = str_replace('DateOfApplication','ps.DateOfApplication',$_GET['sidx']);
	$_GET['sidx'] = str_replace('TimeOfLastChecking','ps.TimeOfLastChecking',$_GET['sidx']);
	$_GET['sidx'] = str_replace('NextSessionDate','ps.NextSessionDate',$_GET['sidx']);
}

if (!isset($_GET['sidx']) || ''==$_GET['sidx'])
	$_GET['sidx']= 'ps.DateOfApplication, debtorName';

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders,'id_ProcedureStart');
