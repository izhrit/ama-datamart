<?

require_once '../assets/libs/auth/check.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManager();

$txt_query= "select ps.ShowToSRO 
			from ProcedureStart ps
			where (1=0";
if (isset($auth_info->id_Manager))
{
	$txt_query.= " or ps.id_Manager={$auth_info->id_Manager}";
}
else if (isset($auth_info->id_Contract))
{
	$txt_query.= " or ps.id_Contract={$auth_info->id_Contract}";
}
$txt_query.= ')';
$txt_query.= ' order by ps.id_ProcedureStart desc limit 1';

$rows= execute_query($txt_query,array());

if(0!==count($rows)) {
	$result = (object)array(
		"Показывать_СРО"=> (0!=$rows[0]->ShowToSRO)
	);	
}else {
	$result = (object)array(
		"Показывать_СРО"=> 0
	);
}

echo nice_json_encode($result);