<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/starts/start_crud_lib.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManager();

class start_crud extends Base_crud
{
	function create($start)
	{
		global $auth_info;
		$id_Contract= CheckMandatoryGET('id_Contract');
		$id_Contract = $id_Contract == 'undefined' ? null : $id_Contract;
		if ($id_Contract!=$auth_info->id_Contract)
			exit_unauthorized("can not create ProcedureStart for other id_Contract!");

		$start = validate_start($start);

		$result = create_start($start, $id_Contract);

		$res = array(
			"ok"=>true
			, "data"=>$result
		);
		echo nice_json_encode($res);
	}

	function read($id_ProcedureStart)
	{
		$start= read_start($id_ProcedureStart);
		if (null==$start)
		{
			exit_not_found("can not find start id_ProcedureStart=$id_ProcedureStart");
		}
		else
		{
			echo nice_json_encode($start);
		}
	}

	function update($id_ProcedureStart,$start)
	{
		global $auth_info;
		$id_Manager= !isset($auth_info->id_Manager)?null:$auth_info->id_Manager;
		unset($start['TimeOfLastChecking']);
		$result= update_start($id_ProcedureStart,$start,$auth_info->id_Contract,$id_Manager);
		if ($result === 'can not update ProcedureStart, id_MRequest is not null') 
		{
			echo '{"ok": false, "message": "Заявление выбрано в запросе кандидатур из суда"}';
		}
		else if (1==$result)
		{
			echo '{ "ok": true }';
		}
		else if (0==$result)
		{
			echo '{ "ok": true, "affected": 0 }';
		}
		else
		{
			exit_bad_request("affected $affected_rows when update id_ProcedureStart=$id_ProcedureStart");
		}
	}

	function delete($ids_ProcedureStart)
	{
		global $auth_info;
		$id_Manager= !isset($auth_info->id_Manager)?null:$auth_info->id_Manager;
		$result = delete_starts($ids_ProcedureStart,$auth_info->id_Contract,$id_Manager);
		if ($result === 'can not delete ProcedureStart, id_MRequest is not null') {
			echo '{"ok": false, "message": "Заявление выбрано в запросе кандидатур из суда"}';
		}
		else if($result){
			echo '{ "ok": true }';
		}
		else
		{
			exit_not_found("can not delete ProcedureStart!");
		}
	}
}

$crud= new start_crud();
$crud->process_cmd();

