<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/libs/starts/start_crud_lib.php';
require_once '../assets/libs/starts/casebookapi.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManager();

function DoActualize()
{
	global $auth_info;
	write_to_log('DoActualize {');

	$id_Contract= CheckMandatoryGET('id_Contract');
	$id_Contract = $id_Contract == 'undefined' ? null : $id_Contract;
	$id_Manager= !isset($auth_info->id_Manager)?null:$auth_info->id_Manager;

	$Время_сверки= safe_date_create();
	$start= safe_POST($_POST);
	
	$start['TimeOfLastChecking']= $Время_сверки;

	$id_ProcedureStart= null;
	if (!isset($start['id_ProcedureStart']))
	{
		$validated_start = validate_start($start);
		$id_ProcedureStart= create_start($validated_start, $id_Contract,$id_Manager);
	}
	else
	{
		$id_ProcedureStart= $start['id_ProcedureStart'];
		update_start($id_ProcedureStart,$start,$id_Contract,$id_Manager);
	}

	echo nice_json_encode(array(
		'id_ProcedureStart'=>$id_ProcedureStart
		,'Время_сверки'=>date_format($Время_сверки,'d.m.Y H:i:s')
	));
	write_to_log('DoActualize }');
}

function prepare_cases_for_kad_arbitr_ru($start)
{
	$Должник= (object)$start['Должник'];
	$DebtorSnils= null;
	$DebtorName= null;
	switch ($Должник->Тип)
	{
		case 'Физ_лицо':
			$np= (object)$Должник->Физ_лицо;
			$DebtorName= "{$np->Фамилия} {$np->Имя} {$np->Отчество}";
			$DebtorSnils= $np->СНИЛС;
			break;
		case 'Юр_лицо':
			$DebtorName= $Должник->Юр_лицо['Наименование'];
			break;
	}

	$txt_query= 'select CasebookTag from Court where id_Court=?;';
	$rows= execute_query($txt_query,array('s',$start['Заявление_на_банкротство']['В_суд']['id']));
	$row= $rows[0];

	$cases= array(
		array(
			'id'=>'1'
			,'debtor'=>array(
				'Name'=>$DebtorName
				,'INN'=>$Должник->ИНН
				,'OGRN'=>$Должник->ОГРН
				,'SNILS'=>$DebtorSnils
			)
			,'CourtTag'=>$row->CasebookTag
		)
	);
	return $cases;
}

function prepare_CasebookAPI()
{
	global $use_casebook_api, $use_casebook_auth;
	return new CasebookAPI((object)array('base_url'=>$use_casebook_api,'auth'=>$use_casebook_auth));
}

function DoSearch()
{
	write_to_log('DoSearch {');

	global $auth_info;
	$id_Contract= CheckMandatoryGET_id('id_Contract');
	$id_Manager= !isset($auth_info->id_Manager)?null:$auth_info->id_Manager;

	$Время_сверки= safe_date_create();
	$start= safe_POST($_POST);

	$start['TimeOfLastChecking']= $Время_сверки;

	$cases= prepare_cases_for_kad_arbitr_ru($start);
	try
	{
		$cryptoapi= prepare_CasebookAPI();
		$details= $cryptoapi->FindOutDetailsOfTheCases($cases);
		$details_by_id= prep_details_by_id($details);
		if (isset($details_by_id['1']))
		{
			$dc= $details_by_id['1'];
			$start['Номер_судебного_дела']= $dc->CaseNumber;
			$start['Заявление_на_банкротство']['Дата_рассмотрения']= $dc->NextSessionDate;
		}
	}
	catch (Exception $ex)
	{
		write_to_log('catched exception on FindOutDetailsOfTheCases: ' . get_class($ex) . ' - ' . $ex->getMessage());
	}

	$id_ProcedureStart= null;
	if (!isset($start['id_ProcedureStart']))
	{
		$validated_start = validate_start($start);
		$id_ProcedureStart= create_start($start, $id_Contract, $id_Manager);
	}
	else
	{
		$id_ProcedureStart= $start['id_ProcedureStart'];
		update_start($id_ProcedureStart,$start, $id_Contract, $id_Manager);
	}

	echo nice_json_encode(array(
		 'id_ProcedureStart'=>$id_ProcedureStart
		,'Время_сверки'=>date_format($Время_сверки,'d.m.Y H:i:s')
		,'Дата_рассмотрения'=>$start['Заявление_на_банкротство']['Дата_рассмотрения']
		,'Номер_судебного_дела'=>$start['Номер_судебного_дела']
	));

	write_to_log('DoSearch }');
}

CheckMandatoryGET_execute_action_variant('cmd',array(
	 'actualize'=> function(){DoActualize();}
	,'search'=>    function(){DoSearch();}
));
