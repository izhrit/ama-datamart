<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/viewer/alib_viewer.php';

global $auth_info;

class request_crud extends Base_crud
{
	function find_debtor_for($params, $connection)
	{
		$txt_query= "select id_Debtor 
		from Debtor where (INN=? and INN!='' and INN is not NULL) or (OGRN=? and OGRN!='' and OGRN is not NULL) or (SNILS=? and SNILS!='' and SNILS is not NULL)";
		$rows=$connection->execute_query($txt_query,array('sss',$params['ИНН'],$params['ОГРН'],$params['СНИЛС']));
		if(count($rows)!=1)
			return null;
		else return $rows[0]->id_Debtor;
	}
	function check_request($id_MUser, $id_Debtor, $connection)
	{
		$txt_query= "select id_Request from Request where id_MUser=? and id_Debtor=?";
		$rows=$connection->execute_query($txt_query,array('ss',$id_MUser,$id_Debtor));
		return count($rows)==0 ? true : false;
	}
	function check_access($id_MUser, $id_Debtor, $connection)
	{
		$txt_query= "
		select mu.id_MUser
		from MProcedure mp 
		inner join Debtor d on d.id_Debtor=mp.id_Debtor
		inner join Manager m on mp.id_manager=m.id_Manager 
		inner join MProcedureUser mpu on mp.id_MProcedure=mpu.id_MProcedure
		inner join ManagerUser mu on mu.id_ManagerUser=mpu.id_ManagerUser
		where mu.id_MUser=? and d.id_Debtor=?";
		$rows=$connection->execute_query($txt_query,array('ss',$id_MUser,$id_Debtor));
		return count($rows)==0 ? true : false;
	}

	function create($request)
	{
		$auth_info= CheckAuthViewer();
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		try
		{
			$id_Debtor= $this->find_debtor_for($request['Должник'], $connection);
			if (null == $id_Debtor)
			{
				$BankruptId=$request['Выбранная_процедура']['id'];
				$Должник= $request['Должник'];
				$id_Debtor= $connection->execute_query_get_last_insert_id
					("insert into Debtor set Name=?, INN=?, SNILS=?, OGRN=?, BankruptId=?;",
					array('sssss',$Должник['Наименование'],$Должник['ИНН'],$Должник['СНИЛС'],$Должник['ОГРН'],$BankruptId));
			}
			$body= array(
				'Процедура'=>array(
					'КогоПредставляетЗаявитель'=>$request['Процедура']['КогоПредставляетЗаявитель']
				)
				, 'Пояснения'=>$request['Пояснения']
			);

			if(!$this->check_request($auth_info->id_MUser,$id_Debtor,$connection))
				throw new Exception("duplicate_request");
			else if(!$this->check_access($auth_info->id_MUser,$id_Debtor,$connection))
				throw new Exception("duplicate_procedure");
			else 
			{
				$connection->execute_query_no_result
						("insert into Request set id_MUser=?, id_Debtor=?, managerName=?, Body=compress(?), State='a', TimeLastChange=now();",
						array('ssss',$auth_info->id_MUser,$id_Debtor,$request['Процедура']['Управляющий'],nice_json_encode($body)));
				$connection->commit();
			}
			echo '{ "ok": true }';
		}
		catch (Exception $ex)
		{
			$connection->rollback();
			if($ex->getMessage()=="duplicate_request") exit_duplicate_request();
			else if($ex->getMessage()=="duplicate_procedure") exit_duplicate_procedure();
			else throw $ex;
		}
	}

	function echo_read_row($rows)
	{
		$request= $rows[0];
		$request->Body= json_decode($request->Body);
		$res= array(
			'Должник'=> array(
				'Наименование'=> $request->Name
				, 'ИНН'=> $request->INN
				, 'ОГРН'=> $request->OGRN
				, 'СНИЛС'=> $request->SNILS
				, 'BankruptId'=> $request->BankruptId
			)
			, 'Процедура'=> array(
				'Управляющий'=> $request->managerName
				, 'КогоПредставляетЗаявитель'=> $request->Body->Процедура->КогоПредставляетЗаявитель
			)
			, 'Заявитель'=> array('Имя'=> $request->UserName,'email'=> $request->UserEmail)
			, 'Пояснения'=> $request->Body->Пояснения
			, 'TimeLastChange'=> $request->TimeLastChange
			, 'State'=> $request->State
		);
		echo nice_json_encode($res);
	}

	private static $read_query_prefix= "select 
					d.Name
				, d.INN
				, d.OGRN
				, d.SNILS
				, d.BankruptId
				, rq.managerName
				, uncompress(rq.Body) Body
				, mu.UserName
				, mu.UserEmail
				, rq.TimeLastChange
				, rq.State
			from Debtor d
			inner join Request rq on rq.id_Debtor = d.id_Debtor
			inner join MUser mu on mu.id_MUser = rq.id_MUser";

	function read_for_manager($id_Request, $auth_info_id_Manager, $connection)
	{
		$txt_query= self::$read_query_prefix." 
			inner join MProcedure p on p.id_Debtor=d.id_Debtor
			where rq.id_Request=? && p.id_Manager=?
		;";
		return execute_query($txt_query,array('ss',$id_Request,$auth_info_id_Manager));
	}

	function read_for_viewer($id_Request, $auth_info_id_MUser, $connection)
	{
		$txt_query= self::$read_query_prefix." 
			where rq.id_Request=? && rq.id_MUser=?
		;";
		return execute_query($txt_query,array('ss',$id_Request,$auth_info_id_MUser));
	}

	function read($id_Request)
	{
		$auth_info= CheckAuthViewerOrManager();
		$rows= (isset($auth_info->id_Manager))
			? $this->read_for_manager($id_Request, $auth_info->id_Manager, default_dbconnect())
			: $this->read_for_viewer($id_Request, $auth_info->id_MUser, default_dbconnect());
		if (1!=count($rows))
		{
			exit_not_found("can not find procedure id_Request=$id_Request");
		}
		else
		{
			$this->echo_read_row($rows);
		}
	}

	function update($id_Request,$request)
	{
		$auth_info= CheckAuthViewer();
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		try
		{
			$rows= $this->read_for_viewer($id_Request, $auth_info->id_MUser, $connection);
			if (1!=count($rows))
			{
				exit_not_found("can not find procedure id_Request=$id_Request");
			}
			else
			{
				$body= json_decode($rows[0]->Body);
				$body->Процедура->КогоПредставляетЗаявитель= $request['Процедура']['КогоПредставляетЗаявитель'];
				$body->Пояснения= $request['Пояснения'];
				$txt_query= "update Request set Body=compress(?) where id_Request=? && id_MUser=?;";
				$affected_rows= $connection->execute_query_get_affected_rows($txt_query,
					array('sss',nice_json_encode($body),$id_Request,$auth_info->id_MUser));
				$connection->commit();
				echo ((0!=$affected_rows) ? '{ "ok": true }' : '{ "ok": false }');
			}
		}
		catch (Exception $ex)
		{
			$connection->rollback();
			throw $ex;
		}
	}

	function delete($id_Requests)
	{
		global $auth_info;
		$auth_info= CheckAuthViewer();
		CheckMandatoryGET('id_MUser');
		$id_MUser= $_GET['id_MUser'];
		CheckAccessToViewerIfViewer($auth_info, $id_MUser);

		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		try
		{
			foreach ($id_Requests as $id_Request)
			{
				$affected= $connection->execute_query_get_affected_rows("delete from Request where id_Request=? && id_MUser=?;"
					,array('ss',$id_Request,$auth_info->id_MUser));
				if (1!=$affected)
				{
					$connection->rollback();
					exit_not_found("can not delete Request where id_Request=$id_Request && id_MUser=$id_MUser (affected $affected rows)");
				}
			}
			$connection->commit();
			echo '{ "ok": true }';
		}
		catch (Exception $ex)
		{
			$connection->rollback();
			throw $ex;
		}
	}
}

$crud= new request_crud();
$crud->process_cmd();

