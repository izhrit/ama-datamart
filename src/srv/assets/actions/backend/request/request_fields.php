<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';

$auth_info= CheckAuthViewer();

$txt_query= "select uncompress(Body) Body from Request where id_MUser=? order by TimeLastChange desc limit 1";
$rows= execute_query($txt_query,array('s',$auth_info->id_MUser));

$res= (object)array('КогоПредставляетЗаявитель'=>'','Пояснения'=>'');

if (0!=count($rows))
{
	$row= $rows[0];
	$body= json_decode($row->Body);
	$res->КогоПредставляетЗаявитель= $body->Процедура->КогоПредставляетЗаявитель;
	$res->Пояснения= $body->Пояснения;
}

echo nice_json_encode($res);
