<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';

$auth_info= CheckAuthManager();
CheckMandatoryGET_id('id_Manager');
$id_Manager= intval($_GET['id_Manager']);
CheckMandatoryGET_id('id_Request');
$id_Request= intval($_GET['id_Request']);
CheckAccessToManagerIfManager($auth_info, $id_Manager);
CheckMandatoryGET('decision');

$state= null;
$decision= $_GET['decision'];
switch ($decision)
{
case 'reject': $state= 'r'; break;
case 'approve': $state= 'p'; break;
default: 
	exit_bad_request("bat decision \'$decision\'");
}

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$connection= default_dbconnect();
$connection->begin_transaction();
try
{
	$txt_query= "update Request 
	inner join MProcedure on MProcedure.id_Debtor=Request.id_Debtor
	set Request.State=? where Request.id_Request=? && MProcedure.id_Manager=?";
	$affected= $connection->execute_query_get_affected_rows($txt_query
					,array('sss',$state,$id_Request,$id_Manager));
	if (1!=$affected)
	{
		$connection->rollback();
		exit_not_found("can not delete Request where id_Request=$id_Request && id_MUser=$id_MUser (affected $affected rows)");
	}
	if ('approve'==$decision)
	{
		$txt_query= "select MProcedure.id_MProcedure, MUser.UserName, MUser.id_MUser, ManagerUser.id_ManagerUser, MProcedureUser.id_MProcedure mpu
		from Request 
		inner join MProcedure on MProcedure.id_Debtor=Request.id_Debtor
		inner join MUser on MUser.id_MUser=Request.id_MUser
		left join ManagerUser on ManagerUser.id_Manager=MProcedure.id_Manager && ManagerUser.id_MUser=MUser.id_MUser
		left join MProcedureUser on MProcedureUser.id_MProcedure=MProcedure.id_MProcedure && MProcedureUser.id_ManagerUser=ManagerUser.id_ManagerUser
		where Request.id_Request=? && MProcedure.id_Manager=?";
		$rows= $connection->execute_query($txt_query,array('ss',$id_Request,$id_Manager));
		$row= $rows[0];
		$id_ManagerUser= (isset($row->id_ManagerUser) && null!=$row->id_ManagerUser) 
			? $row->id_ManagerUser
			: $connection->execute_query_get_last_insert_id
				("insert into ManagerUser set id_MUser=?, id_Manager=?, UserName=?, DefaultViewer=0;",
				 array('sss',$row->id_MUser,$id_Manager,$row->UserName));
		if (!isset($row->mpu) || null==$row->mpu)
		{
			$connection->execute_query_get_last_insert_id
				("insert into MProcedureUser set id_MProcedure=?, id_ManagerUser=?, id_Request=?;",
				array('sss',$row->id_MProcedure,$id_ManagerUser,$id_Request));
		}
	}
	$connection->commit();
	echo '{ "ok": true }';
}
catch (Exception $ex)
{
	$connection->rollback();
	throw $ex;
}
