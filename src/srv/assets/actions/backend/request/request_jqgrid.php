<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthManager();
CheckMandatoryGET_id('id_Manager');
$id_Manager= intval($_GET['id_Manager']);

$fields= "
	rq.id_Request id_Request
	, mu.UserName Имя
	, mu.UserEmail email
	, uncompress(rq.Body) Body
	, d.Name В_процедуре
	, rq.TimeLastChange TimeLastChange
	, rq.State state
";

$from_where= " 
from Request rq
inner join Debtor d on rq.id_Debtor = d.id_Debtor
inner join MProcedure mp on mp.id_Debtor = d.id_Debtor
inner join Manager m on m.id_Manager = mp.id_Manager
inner join MUser mu on mu.id_MUser = rq.id_MUser
where m.id_Manager = $id_Manager
";

$filter_rule_builders= array(
	'Имя'=> array('query_field'=>'mu.UserName')
	,'email'=> array('query_field'=>'mu.UserEmail')
	,'В_процедуре'=> array('query_field'=>'d.Name')
);

$result= execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders,' rq.id_Request ');

foreach ($result['rows'] as $row)
{
	if (isset($row->Body) && null!=$row->Body)
		$row->Представляет= json_decode($row->Body)->Процедура->КогоПредставляетЗаявитель;
}

echo nice_json_encode($result);

