<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthSRO();

class Props_crud extends Base_crud
{

	function read($id_SRO)
	{
		global $auth_info;
		$txt_query= "
			select uncompress(e.ExtraFields) ExtraFields from efrsb_sro e where e.id_SRO = ?";

		$rows= execute_query($txt_query,array('s',$id_SRO));
		$count= count($rows);
		if (1!=$count)
		{
			exit_not_found("can not find sro id_SRO=$id_SRO (found $count rows)");
		}
		else
		{
			$sro_ExtraFields= $rows[0]->ExtraFields;
			echo nice_json_encode($sro_ExtraFields);
		}
	}

	function update($id_SRO, $fields)
	{
		global $auth_info;

		CheckAccessToSROIfSRO($auth_info, $id_SRO);

		$props = nice_json_encode($fields);

		$txt_query= 'update efrsb_sro set ExtraFields=compress(?) where id_SRO = ?;';
		$affected= execute_query_get_affected_rows($txt_query,array('si',$props,$id_SRO));
		if (1!=$affected)
		{
			exit_not_found("can not update efrsb_sro ExtraFields where id_SRO=$id_SRO (affected $affected rows)");
		}else {
			echo '{ "ok": true }';
		}
	}
}

$crud= new Props_crud();
$crud->process_cmd();
