<?php

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/realip.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/get_mime_by_filename.php';

require_once '../assets/libs/auth/check.php';

write_to_log('-10');

$id_SRODocument= CheckMandatoryGET_id('id_SRODocument');

$auth_info= SafeGetAuth();

if (null==$auth_info && !isset($_GET['pass']))
	exit_unauthorized("can not access id_SRODocument=$id_SRODocument without session and pass!");

write_to_log('-9');

$txt_query = 'select d.Body, FileName, octet_length(uncompress(d.Body)) as length, PubLock as pub_lock
from SRODocument d
inner join efrsb_sro sro on d.id_SRO=sro.id_SRO';
if (null!=$auth_info)
{
	$category= $auth_info->category;
	switch ($auth_info->category)
	{
		case 'manager':  $txt_query.= " left join manager m on m.id_SRO=sro.id_SRO where d.id_SRODocument=? and m.id_Manager={$auth_info->id_Manager};"; break;
		case 'customer': $txt_query.= " left join manager m on m.id_SRO=sro.id_SRO where d.id_SRODocument=? and m.id_Contract={$auth_info->id_Contract};"; break;
		case 'sro':      $txt_query.= " where d.id_SRODocument=? and sro.id_SRO={$auth_info->id_SRO};"; break;
		default: exit_unauthorized("can not access id_SRODocument=$id_SRODocument with session category=$category!");
	}
}

$rows= execute_query($txt_query,array('i',$id_SRODocument));
$count_rows= count($rows);

write_to_log('-8');

if (1!=$count_rows)
	exit_not_found("found $count_rows rows for id_SRODocument=$id_SRODocument");

$row0= $rows[0];

write_to_log('1');

function document_is_available($locks_txt,$pass)
{
	write_to_log('document_is_available {');
	write_to_log_named('locks_txt',$locks_txt);
	if (null==$locks_txt || ''==$locks_txt)
	{
		write_to_log('document_is_available } return false because lock is empty');
		return false;
	}

	$current_time= safe_date_create();
	$current_time_seconds= $current_time->format('U');
	$ip= getRealIPAddr();

	$locks= json_decode($locks_txt);
	write_to_log('document_is_available 1');
	write_to_log_named('locks',$locks);
	foreach ($locks as $lock)
	{
		write_to_log('document_is_available checking lock 1');
		if ($lock->ip==$ip && $lock->pass==$pass)
		{
			write_to_log('document_is_available checking lock 2');
			$lock_time= date_create_from_format('Y-m-d\TH:i:s',$lock->time);
			write_to_log('document_is_available checking lock 3');
			$lock_age= abs($current_time_seconds - $lock_time->format('U'));
			write_to_log('document_is_available checking lock 4');
			if ($lock_age <= (60*15)) // UNIX �������..
			{
				write_to_log('document_is_available } return true');
				return true;
			}
		}
	}
	write_to_log_named('pass',$pass);
	write_to_log_named('ip',$ip);
	write_to_log_named('current_time',$current_time);
	write_to_log_named('locks',$locks_txt);
	write_to_log('document_is_available } return false');
	return false;
}

write_to_log('2');

if (null==$auth_info && !document_is_available($row0->pub_lock,$_GET['pass']))
	exit_unauthorized("can not access id_SRODocument=$id_SRODocument without session!");

write_to_log('3');

$FileName= $row0->FileName;
write_to_log($row0->FileName);
$path_parts= pathinfo($FileName);
write_to_log($path_parts);
$FileName= urlencode($FileName);
$extension= $path_parts['extension'];
$content_type = _mime_content_type($extension);
$content= $row0->Body;
$content_length= $row0->length;

if (isset($_GET['show-image']))
{
	$im = imagecreatefromstring($content);
	switch ($extension) {
		case 'png':
			imagealphablending($im, FALSE);
			imagesavealpha($im, TRUE);
			header('Content-Type: image/png');
			imagepng($im);
			break;
		case 'jpg':
			header('Content-Type: image/jpeg');
			imagejpeg($im);
			break;
		case 'jpeg':
			header('Content-Type: image/jpeg');
			imagejpeg($im);
			break;
	}
	exit();
}

if (!isset($_GET['no-headers']))
{
	header("Content-type: $content_type");
	header("Content-Length: $content_length");
	header("Content-Disposition: attachment; filename=$FileName");
}
echo $content;
exit;
