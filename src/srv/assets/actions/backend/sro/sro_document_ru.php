<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthSRO();

class SRO_documents_crud extends Base_crud
{
	function create($name)
	{
		$type= CheckMandatoryGET('type');
		$id_SRO= CheckMandatoryGET('id_SRO');

		if (!isset($_FILES['File']))
			exit_internal_server_error("Файл не приложен");

		$file= $_FILES['File'];

		if (!empty($file['error']))
			exit_internal_server_error('Не удалось загрузить файл, error:'.$file['error']);

		if (empty($file['tmp_name']))
			exit_internal_server_error("Не удалось загрузить файл (tmp_name is empty)");

		$tmp_name= $file['tmp_name'];
		if ('none'==$tmp_name || !is_uploaded_file($tmp_name))
			exit_internal_server_error("Не удалось загрузить файл $tmp_name");

		if (strlen($type) > 3)
			exit_internal_server_error("Слшком длинный тип '$type'");

		write_to_log($file);

		$file_content= file_get_contents($tmp_name);
		$txt_query= "INSERT INTO SRODocument set id_SRO=?, Body=?, FileName=?, DocumentType=?;";
		$id_SRODocument = execute_query_get_last_insert_id($txt_query,array('isss',$id_SRO, $file_content,$file['name'], $type ));
		echo nice_json_encode(array('ok'=>true, 'id'=>$id_SRODocument));
	}
	
	function read($id_SRO)
	{
		global $auth_info;
		$txt_query= "
			select  
				d.id_SRODocument id_SRODocument
				,d.FileName FileName
				,d.DocumentType DocumentType
				from SRODocument d
				inner join efrsb_sro e on e.id_SRO = d.id_SRO
				where e.id_SRO = ?";

		$rows= execute_query($txt_query,array('s',$id_SRO));
		echo nice_json_encode($rows);
	}

	function delete($id_SRODocument)
	{
		$txt_query= "delete d
			from SRODocument d
			where d.id_SRODocument=?";
		$affected_rows= execute_query_get_affected_rows($txt_query,array('i',$id_SRODocument[0]));
		if (0!=$affected_rows)
		{
			echo '{ "ok": true }';
		}
		else
		{
			echo '{ "ok": false }';
		}
	}


}

$crud= new SRO_documents_crud();
$crud->process_cmd();
