<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/application/alib_MData.php';

$id_application=CheckIdApplication();
$txt_query= "
	UPDATE application SET application_data=compress(?)
	where id_application=?
	;";
execute_query($txt_query,array('ss',urldecode($_POST['data']), $id_application));

echo '{ "status": true }';
