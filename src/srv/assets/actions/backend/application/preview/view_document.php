<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/application/preview/document.php';
require_once '../assets/actions/backend/application/alib_MData.php';

if (isset($_GET['id_MData']))
{
	$id_MData=$_GET['id_MData'];
	write_to_log("id_MData=$id_MData");
	$auth_info= CheckAuthViewer();
	$id_MUser = $auth_info->id_MUser;
	$txt_query="select uncompress(md.fileData) application_data
	from MData md
	inner join ManagerUser mu on mu.id_ManagerUser=md.id_ManagerUser
	where md.id_MData=? and mu.id_MUser=?";
		$rows=execute_query($txt_query,array('ss',$id_MData,$id_MUser));
		if(count($rows)==1)
		{
			write_to_log($rows[0]->application_data);
			$params=$rows[0]->application_data;//not work
		}
		else $params=nice_json_encode(GetParams1());
}
else 
{
	$id_application= CheckIdApplication();
	write_to_log("id_application=$id_application");
	if (''==$id_application)
	{
		write_to_log("GetParams1 1");
		$params= nice_json_encode(GetParams1());
	}
	else 
	{
		$txt_query="select uncompress(application_data) application_data from application where id_application=?";
		$rows= execute_query($txt_query,array('s',$id_application));
		if (count($rows)==1)
		{
			write_to_log($rows[0]->application_data);
			$params=$rows[0]->application_data;
		}
		else
		{
			write_to_log("GetParams1 2");
			$params=nice_json_encode(GetParams1());
		}
	}
}
write_to_log($params);
echo $params;