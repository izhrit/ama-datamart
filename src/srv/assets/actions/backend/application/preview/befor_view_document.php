<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/Library/NCLNameCaseRu.php';
require_once '../assets/actions/backend/application/preview/document.php';
require_once '../assets/actions/backend/application/alib_MData.php';

$params= GetParams($_POST);

$params->id_application=$id_application;
$params->Кредитор->email=isset($_POST['Кредитор']['email'])?$_POST['Кредитор']['email']:'';
$params->Кредитор->Телефон=isset($_POST['Кредитор']['Телефон'])?$_POST['Кредитор']['Телефон']:'';
$params->Кредитор->ИНН=$_POST['Кредитор']['ИНН'];
$params->Должник->ИНН=$_POST['Должник']['ИНН'];
$params->Должник->ОГРН=$_POST['Должник']['ОГРН'];
$params->Должник->СНИЛС=$_POST['Должник']['СНИЛС'];
$params->Должник->BankruptId=$_POST['Должник']['BankruptId'];
$id_application=CheckOrSetIdApplication();

$txt_query= "UPDATE application SET  application_data=compress(?) where id_application=?;";
execute_query($txt_query,array('ss',nice_json_encode($params),$id_application));