<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/application/preview/document.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/application/alib_MData.php';

header("Content-type:  application/msword");
header("Content-Disposition: attachment; filename=application.doc");

header("Content-type:  application/msword");
header("Content-Disposition: attachment; filename=application.doc");

if(!isset($_GET['id_MData']))
	exit_not_found('not found id_MData');//need another exeption
else
{
	$id_MData=$_GET['id_MData'];
	$auth_info= CheckAuthViewer();
	$id_MUser = $auth_info->id_MUser;
	$txt_query="select uncompress(md.fileData) application_data
	from MData md
	inner join ManagerUser mu on mu.id_ManagerUser=md.id_ManagerUser
	where md.id_MData=? and mu.id_MUser=?";
	$rows=execute_query($txt_query,array('ss',$id_MData,$id_MUser));
	if(count($rows)==1)
		$params=json_decode($rows[0]->application_data);
	else $params=GetParams1();
}
?>
<% if (!obj.preview) { %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Тестовая страница А4 альбомной ориентации</title>
	<xml xmlns:w="urn:schemas-microsoft-com:office:word">
		<w:WordDocument>
			<w:View>Print</w:View>
			<w:Zoom>100</w:Zoom>
			<w:DoNotOptimizeForBrowser/>
		</w:WordDocument>
	</xml>
<% } %>
<style>
	@page WordPageA4
	{
		size:595.3pt 841.9pt;
		mso-page-orientation:portrait;
		margin:2cm 1.5cm 1.0cm 1.5cm;
		mso-header-margin:35.4pt;
		mso-footer-margin:35.4pt;
		mso-paper-source:0;
		mso-footer:page-footer;
	}
	.test-14-albom-root div.WordPageA4
	{
		page:WordPageA4;
	}
</style>
<% if (!obj.preview) { %>
</head>
<% } %>
<% if (!obj.preview) { %>
<body class="test-14-albom-root">
<% } else { %>
<div class="test-14-albom-root">
<% } %>

	<div class="WordPageA4">
    <table style="margin-right: auto; margin-left: auto;">
		<tbody>
		<tr>
		<td style="width: 329px; vertical-align: top;">
		<div style="vertical-align:top"><p>Исх. № <?= $params->Номер_заявления?> от <?= $params->Дата_заявления?> г.</p></div>
		</td>
		<td style="width: 334px;">
		<p style="text-align: right;"><?= $params->Решение->Суд->Наименование->Полное ?></p>
		<p style="text-align: right;">Адрес: <?= $params->Решение->Суд->Адрес?></p>
		<p style="text-align: right;"><strong>Кредитор:</strong> <?=$params->Кредитор->Наименование->Именительный?></p>
		<p style="text-align: right;">Адрес: <?= $params->Кредитор->Адрес?></p>
		<p style="text-align: right;"><strong>Должник:</strong> <?= $params->Должник->Наименование->Именительный?></p>
		<p style="text-align: right;">Адрес: <?= $params->Должник->Адрес?></p>
		<p style="text-align: right;"><strong>Арбитражный управляющий:</strong> <?= $params->АУ->Наименование?></p>
		<p style="text-align: right;">Адрес: <?= $params->АУ->Адрес?></p>
		<p style="text-align: right;"><strong>Дело </strong><?= $params->Решение->Номер?></p>
		<p>&nbsp;</p>
		</td>
		</tr>
		</tbody>
	</table>
	<p style="text-align: center;">&nbsp;</p>
	<p style="text-align: center;"><strong>Заявление (требование)</strong></p>
	<p style="text-align: center;"><strong>о включении в реестр требований кредиторов <?= $params->Должник->Наименование->Именительный?></strong></p>
	<p style="text-align: justify;">&nbsp; &nbsp; Арбитражным судом <?= $params->Решение->Суд->Наименование->Короткое?> вынесено <?= $params->Решение->Тип?> по делу <?= $params->Решение->Номер?> от <?= $params->Решение->Дата?> г. <?= $params->Решение->Процедура->Короткая?> в отношении <?= $params->Должник->Наименование->Родительный?>, временным (внешним, административным, конкурсным) управляющим утвержден <?= $params->АУ->Наименование?>, член саморегулируемой организации <?= $params->АУ->СРО?>.</p>
	<p style="text-align: justify;">&nbsp; &nbsp; <?= $params->Решение->Процедура->Полное?> в отношении <?= $params->Должник->Наименование->Родительный?> опубликовано в газете &laquo;Коммерсантъ&raquo; № <?= $params->Решение->Комерсант->Номер?> от <?= $params->Решение->Комерсант->Дата?> г., в Едином Федеральном реестре сведений о банкротстве сообщение № <?= $params->Решение->Сообщение->Номер?> от <?= $params->Решение->Сообщение->Дата?>г.</p>
	<p style="text-align: justify;">&nbsp; &nbsp; В соответствии с <a href="http://www.consultant.ru/document/cons_doc_LAW_39331/143cd5c121660ad20aadff9975d22ec72d55e12f/">абз. 1 п. 6 ст. 16</a> Федерального закона &laquo;О несостоятельности (банкротстве)&raquo; № 127-ФЗ от 26.10.2002 (далее &ndash; Закон о банкротстве) требования кредиторов включаются в реестр требований кредиторов и исключаются из него арбитражным управляющим или реестродержателем исключительно на основании вступивших в силу судебных актов, устанавливающих их состав и размер.</p>
	<p style="text-align: justify;">&nbsp; &nbsp; Указанные требования направляются в арбитражный суд, должнику и арбитражному управляющему с приложением судебного акта или иных документов, подтверждающих обоснованность этих требований. Указанные требования включаются в реестр требований кредиторов на основании определения арбитражного суда о включении указанных требований в реестр требований кредиторов.</p>
	<p style="text-align: justify;">&nbsp; &nbsp; За <?= $params->Должник->Наименование->Творительный?> числится задолженность по денежным обязательствам перед <?= $params->Кредитор->Наименование->Короткое?> в размере <?= $params->Кредитор->СумДолг?> руб., которая до настоящего времени не погашена.</p>
	<p style="text-align: justify;">&nbsp; &nbsp; <?= $params->Основание?></p>
	<p style="text-align: justify;">&nbsp; &nbsp; В соответствии с <a href="http://www.consultant.ru/document/cons_doc_LAW_39331/a8949049844a8eb33dcc8f5ddb6b95e0c70112a3/">п. 4 ст. 134</a> Закона о банкротстве требования кредиторов удовлетворяются в следующей очередности:</p>
	<p style="text-align: justify;">в первую очередь производятся расчеты по требованиям граждан, перед которыми должник несет ответственность за причинение вреда жизни или здоровью, путем капитализации соответствующих повременных платежей, а также расчеты по иным установленным настоящим Федеральным законом требованиям;</p>
	<p style="text-align: justify;">во вторую очередь производятся расчеты по выплате выходных пособий и (или) оплате труда лиц, работающих или работавших по трудовому договору, и по выплате вознаграждений авторам результатов интеллектуальной деятельности;</p>
	<p style="text-align: justify;">в третью очередь производятся расчеты с другими кредиторами, в том числе кредиторами по нетто-обязательствам.</p>
	<p style="text-align: justify;">&nbsp; &nbsp; Денежные средства прошу перечислять на банковские реквизиты</srtong> <?= $params->Кредитор->Наименование->Короткое?>: <?= $params->Кредитор->Банк->Реквизиты?>.</p>
	<p style="text-align: justify;">&nbsp; &nbsp; На основании вышеизложенного, а также в соответствии с п. 6 ст. 16, ст. 134, Закона о банкротстве, <a href="http://www.consultant.ru/document/cons_doc_LAW_37800/260357f70adf93df1269b725cf458cf84ec47a3e/">ст. 223</a> Арбитражного процессуального кодекса Российской Федерации,</p>
	<p style="text-align: center;"><strong>ПРОШУ:</strong></p>
	<p style="text-align: justify;">&nbsp; &nbsp; Признать требование <?= $params->Кредитор->Наименование->Короткое?> по денежному обязательству в размере <?= $params->Кредитор->СумДолг?> руб., в том числе основной долг <?= $params->Кредитор->Долг?> руб., <?if($params->Кредитор->Санкции){?><?= $params->Кредитор->Санкции?><?} ?>обоснованным и включить в реестр требований кредиторов <?=	 $params->Должник->Наименование->Родительный?> по третьей очереди удовлетворения.</p>

	<p>Приложение:</p>
	<ol>
		<?=$params->Приложения?>
	</ol>
	<p>&nbsp; &nbsp;</p>
	<table>
		<tbody>
		<tr>
		<td width="400">
			<div><strong>Руководитель<br>
			<?= $params->Кредитор->Наименование->Короткое?><strong></div>
		</td>
		<td width="400">
			<p style="text-align: right;"><strong><?= $params->Кредитор->Руководитель?><strong></p>
		</td>
		</tr>
		</tbody>
	</table>
	</div>

<% if (obj.preview) { %>
</div>
<% } else { %>
</body>
</html>
<% } %>
