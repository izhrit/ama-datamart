<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/application/alib_MData.php';

$auth_info= CheckAuthViewer();

$MDataFile= urldecode($_POST['data']);
$params= json_decode($MDataFile);
write_to_log($params);

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$connection= default_dbconnect();
$connection->begin_transaction();
try
{
	$id_Debtor= insert_or_Find_Debtor($connection, $params);
	$id_MUser=$auth_info->id_MUser;
	$id_ManagerUser=create_or_find_ManagerUser($connection, $params, $id_MUser);
	write_to_log($_GET);
	if ($_GET['update']!='true')
	{
		$txt_query= "select md.id_MData 
		from MData md
		inner join ApplicationRTK app on app.id_MData=md.id_MData
		where md.id_ManagerUser=? and md.id_Debtor=? and app.creditor_inn=?";
		$MData= $connection->execute_query($txt_query, array('sss',$id_ManagerUser, $id_Debtor, $params->Кредитор->ИНН));
		if(count($MData)==1)
		{
			echo '{ "status": false, "notice": "duplicated_application" }';
			exit;
		}
	}
	$id_MData = create_or_update_MData($connection,$id_Debtor,$id_ManagerUser,$params->id_application,$MDataFile, $params);

	$connection->commit();
	echo '{ "status": true, "id_MData": '.$id_MData.' }';
	exit;
}
catch (mysqli_sql_exception $ex)
{
	//write_to_log('$ex from mysql: ');
	//write_to_log($ex);
	$connection->rollback();
}
catch (Exception $ex)
{
	//write_to_log('$ex from code: ');
	//write_to_log($ex);
	$connection->rollback();
	throw $ex;
}
