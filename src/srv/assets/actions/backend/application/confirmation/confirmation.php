<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/application/alib_MData.php';

$id_application=CheckIdApplication();
$confirmation_token= !isset($_GET['token']) ? '' : $_GET['token'];
$txt_query= "
	select 
		mu.id_MUser
	from MUser mu
	inner join application app on mu.id_MUser = app.id_MUser
	where app.id_application=? and mu.Confirmation_Token=?";
$rows= execute_query($txt_query,array('ss',$id_application,$confirmation_token));
if(count($rows)==1)
{
	require_once '../assets/actions/backend/application/dm_login.php';
	echo '{ "status": true }';
}
else
{
	echo '{ "status": false }';
}
