<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/application/alib_MData.php';

function PrepareConfirmationTokenLetter($confirmation_token, $link)
{
	$letter= (object)array('subject'=>'Учётные данные на Витрине данных ПАУ');
	ob_start();
	include "../assets/actions/backend/application/confirmation/leter.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();
	return $letter;
}
function register_or_find_MUser($connection, $params, $id_Debtor, $id_application, $confirmation_token,$confirmation_token_cookie,$new_password)
{
	$txt_query= "select id_MUser from application where id_application=?";
		$rows= $connection->execute_query($txt_query, array('s',$id_application));
	if(count($rows)==1 && $rows[0]->id_MUser)
	{
		$id_MUser=$rows[0]->id_MUser;
		$txt_query= "
		UPDATE MUser SET UserEmail=?, UserName=?, Confirmation_Token=?, Confirmation_Token_Cookie=?
		where id_MUser=?
		;";
		$connection->execute_query_no_result($txt_query, array('sssss',$params->Кредитор->email,$params->Кредитор->email,$confirmation_token,$confirmation_token_cookie,$id_MUser));
	}
	else
	{			
		$txt_query= "insert into MUser set UserEmail=?, UserName=?, UserPhone=?, UserPassword=?, Confirmation_Token=?, Confirmation_Token_Cookie=?, HasOutcoming=1;";
		$id_MUser= $connection->execute_query_get_last_insert_id($txt_query,
			array('ssssss',$params->Кредитор->email,$params->Кредитор->email,$params->Кредитор->Телефон,$new_password,$confirmation_token,$confirmation_token_cookie));
				
		$Body= array(
			'Процедура'=>array('КогоПредставляетЗаявитель'=>$params->Кредитор->Наименование->Именительный)
			,'Пояснения'=>'Сформировал заявлени на вступление в комитет кредиторов '. $params->Кредитор->Наименование->Родительный
		);

		$txt_query= "insert into Request set id_MUser=?, id_Debtor=?, managerName=?, State='a', TimeLastChange=now(), Body=compress(?);";
		$id_Request= $connection->execute_query_get_last_insert_id($txt_query,
		array('isss',$id_MUser,$id_Debtor,$params->АУ->Наименование,nice_json_encode($Body)));

		$txt_query= "
			UPDATE application SET id_MUser=?
			where id_application=?
			;";
		$connection->execute_query_no_result($txt_query, array('ss',$id_MUser,$id_application));
	}
	return $id_MUser;
}
try
{
	$id_application=CheckIdApplication();
	$txt_query= "
		select 
			uncompress(a.application_data) data
		from application a
		where a.id_application=?";
	$rows= execute_query($txt_query,array('i',$id_application));
	if(count($rows)==1)
	{
		$MDataFile=$rows[0]->data;
		$arr=array();
		$params=json_decode($rows[0]->data);
	}
	else
	{
		throw new Exception('application information not found');
	}

	$txt_query= "select id_MUser from MUser where UserEmail=?";
		$rows= execute_query($txt_query, array('s',$params->Кредитор->email));
	if(count($rows)!=0)
	{
		echo '{ "status": false, "notice": "duplicated_email" }';
		exit;
	}
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		global $email_settings;
		$new_password = (true!=$email_settings->enabled) ? 'new_test_pass' : generate_password(8);
		$confirmation_token = (true!=$email_settings->enabled) ? '1234' : generate_password(4);
		$confirmation_token_cookie = (true!=$email_settings->enabled) ? '1234' : generate_password(10);

		$id_Debtor= insert_or_Find_Debtor($connection, $params);
		$id_MUser=register_or_find_MUser($connection,$params, $id_Debtor, $id_application, $confirmation_token,$confirmation_token_cookie, $new_password);
		$id_ManagerUser=create_or_find_ManagerUser($connection, $params, $id_MUser);
		$id_MData=create_or_update_MData($connection,$id_Debtor,$id_ManagerUser,$id_application,$MDataFile,$params);


		setcookie("Confirmation_token", $confirmation_token_cookie, time()+3600*24*7,"/");
		setcookie("email", $params->Кредитор->email, time()+3600*24*7,"/");
		//setcookie("id_application", $id_application, time()+3600*24*7,"/");
		write_to_log('$_SESSION');
		write_to_log($_SESSION);
		global $datamart_ui_url;
		$link=$datamart_ui_url.'?section=outcome&token='.$confirmation_token.'&download&id_MData='.$id_MData;

		$letter= PrepareConfirmationTokenLetter($confirmation_token, $link);
		if (false==PostLetter($letter,$params->Кредитор->email,$params->Кредитор->Наименование->Именительный,'создание наблюдателя',$id_MUser))
		{
			$connection->rollback();
			$email=$params->Кредитор->email;
			exit_internal_server_error("Can not send email to $email");
		}
		else
		{
			$connection->commit();
			echo '{ "status": true, "id_MData": '.$id_MData.' }';
			exit;
		}
	}
	catch (mysqli_sql_exception $ex)
	{
		$connection->rollback();
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
}
catch (Exeption $ex)
{
	throw $ex;
}