<?
function insert_or_find_Manager($connection, $params)
{
	$txt_query= "select id_Manager 
	from Manager where (INN=? and INN!='' and INN is not NULL);";
	$rows=$connection->execute_query($txt_query,array('s',$params->АУ->ИНН));
	if(count($rows)==1)
		return $rows[0]->id_Manager;
	else
	{
		return $connection->execute_query_get_last_insert_id
			("insert into Manager set firstName=?, middleName=?, lastName=?, efrsbNumber=?, INN=?;",
			array('sssss',$params->АУ->Имя,$params->АУ->Фамилия,$params->АУ->Отчество,$params->АУ->Number,$params->АУ->ИНН));
	}
}
function create_or_find_ManagerUser($connection, $params, $id_MUser)
{
	$id_Manager=insert_or_find_Manager($connection,$params);
	$txt_query= "select id_ManagerUser
	from ManagerUser where id_MUser=? and id_Manager=?;";
	$rows=$connection->execute_query($txt_query,array('ss',$id_MUser,$id_Manager));
	if (count($rows)==1)
	{
		return $rows[0]->id_ManagerUser;
	}
	else
	{
		return $connection->execute_query_get_last_insert_id
			("insert into ManagerUser (id_MUser, id_Manager, UserName, DefaultViewer)
			values (?,?,(select mu.UserName from MUser mu where mu.id_MUser=?), 0);",
			array('sss',$id_MUser,$id_Manager,$id_MUser));
	}

}
function insert_or_Find_Debtor($connection, $params)
{
	$txt_query= 'select id_Debtor from Debtor where (INN=? and INN!="" and INN is not NULL);';
	$rows=$connection->execute_query($txt_query,array('s',$params->Должник->ИНН));
	if (count($rows)==1)
	{
		return $rows[0]->id_Debtor;
	}
	else
	{
		$txt_query= 'insert into Debtor set Name=?, INN=?, SNILS=?, OGRN=?, BankruptId=?, VerificationState="v", TimeVerified=now();';
		return $connection->execute_query_get_last_insert_id($txt_query,
			array('sssss',$params->Должник->Наименование->Именительный,$params->Должник->ИНН,$params->Должник->СНИЛС,$params->Должник->ОГРН,$params->Должник->BankruptId));
	}
}
function create_or_update_MData($connection, $id_Debtor, $id_ManagerUser, $id_application,$MDataFile, $params)
{
	$txt_query= "select md.id_MData 
	from MData md
	inner join ApplicationRTK app on app.id_MData=md.id_MData
	where md.id_ManagerUser=? and md.id_Debtor=? and app.creditor_inn=?";
	$MData= $connection->execute_query($txt_query, array('sss',$id_ManagerUser, $id_Debtor, $params->Кредитор->ИНН));
	if(count($MData)==1)
	{
		$id_MData=$MData[0]->id_MData;
		$txt_query= "update MData set fileData=compress(?) where id_MData= ?;";
		$rows= $connection->execute_query($txt_query, array('ss',$MDataFile, $id_MData));

		$txt_query= "delete from MData_attachment where id_MData=?;";
		$connection->execute_query($txt_query, array('s', $id_MData));

		$txt_query= "select data, name, extension from ApplicationFile where id_Application=? ";
		$rows= $connection->execute_query($txt_query, array('s', $id_application));

		foreach($rows as $row)
		{
			$txt_query= "insert into MData_attachment set id_MData=?, FileName=?, Content=?;";
			$connection->execute_query($txt_query, array('isb', $id_MData, $row->name.'.'.$row->extension,$row->data));
		}
	}
	else
	{
		$txt_query= "select if(max(revision) is null, 1, max(revision) + 1) revision from MData;";
		$rows=$connection->execute_query($txt_query, array());
		$maxrevision=count($rows)==1?$rows[0]->revision : 1;
		$txt_query= "insert MData set id_ManagerUser=?, id_Debtor=?, fileData=compress(?), MData_Type='k',revision=?, publicDate=NOW();";
		$id_MData= $connection->execute_query_get_last_insert_id($txt_query, array('iiss',$id_ManagerUser, $id_Debtor, $MDataFile,$maxrevision));
		$txt_query= "insert ApplicationRTK set id_MData=?, creditor_inn=?;";
		$connection->execute_query($txt_query, array('is',$id_MData, $params->Кредитор->ИНН));		

		$txt_query= "select data, name, extension from ApplicationFile where id_Application=? ";
		$rows= $connection->execute_query($txt_query, array('s', $id_application));

		foreach($rows as $row)
		{
			$txt_query= "insert into MData_attachment set id_MData=?, FileName=?, Content=?;";
			$connection->execute_query($txt_query, array('isb', $id_MData, $row->name.'.'.$row->extension,$row->data));
		}
	}
	return $id_MData;
}

function CheckOrSetIdApplication()
{
	session_start();
	$id_application=(isset($_SESSION['id_application']) && $_SESSION['id_application'])?$_SESSION['id_application']:'';
	if($id_application=='') // если id_application не сущетсвует, то cоздаем новую запись
	{
		$txt_query="
		INSERT INTO application values();";
		$id_application = execute_query_get_last_insert_id($txt_query,array());
		$_SESSION['id_application']=$id_application;
	}
	return $id_application;
}

function CheckIdApplication()
{
	session_start();
	$id_application= (isset($_SESSION['id_application']) && $_SESSION['id_application'])?$_SESSION['id_application']:'';
	if($id_application=='') // если id_application не сущетсвует, то ошибка
	{
		exit_unauthorized('bad role!');
	}
	return $id_application;
}