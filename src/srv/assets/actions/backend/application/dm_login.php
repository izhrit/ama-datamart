<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
try
{
if(isset($_SESSION['id_application'])) unset($_SESSION['id_application']);
$email=$_COOKIE['email'];
$txt_query= "
	select UserName Имя, UserEmail Email, id_MUser id_MUser, HasOutcoming
	from MUser mu
	where mu.UserEmail=? and mu.Confirmation_Token_Cookie=? and mu.Confirmation_Token=?";
$rows= execute_query($txt_query,array('sss',$email,$_COOKIE['Confirmation_token'],$_GET['token']));
$rows_count=count($rows);
if($rows_count)
{
	$ainfo=$rows[0];

	global $_SESSION, $auth_info;
	if (!isset($_SESSION))
		session_start();
	$auth_info= $ainfo;
	$auth_info->category= 'viewer';
	$_SESSION['auth_info']= $auth_info;

	require_once '../assets/libs/log/access_log.php';
	write_to_access_log_id('login/viewer/fa',$ainfo->id_MUser);

	$txt_query= "update MUser set Confirmed=1 where id_MUser=?;";
	execute_query($txt_query,array('s',$ainfo->id_MUser));

	setcookie("email","",time()-3600,"/");
	setcookie("Confirmation_token","",time()-3600,"/");
}
else
{
	write_to_log("found $rows_count rows for UserEmail=$email");
	
	global $autologin_error_text;
	$autologin_error_text= 'can not find viewer in the db!';
	$txt= "autologin fail: 'can not find viewer in the db!'";
	if (null!=$auth_info)
	{
		$txt.= "\r\nauth information:\r\n";
		$txt.= print_r($auth_info,true);
	}
	if (null!=$_COOKIE)
	{
		$txt.= "\r\nCOOKIES:\r\n";
		$txt.= print_r($_COOKIE,true);
	}
	throw new Exception($txt);
}
}
catch (Exception $ex) // При любом исключении, записываем его в лог и открываем обычную страницу с логином паролем
{
	global $_SESSION, $auth_info;
	setcookie("email","",time()-3600,"/");
	setcookie("Confirmation_token","",time()-3600,"/");

	write_to_log('Unhandled exception during application/login_dm occurred: ' . get_class($ex) . ' - ' . $ex->getMessage());
}