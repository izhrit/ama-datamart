<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/application/alib_MData.php';


if($_GET['file_action']=='add')
{
	$ans=(object) array('id_file'=>'');
	// если файл не был успешно загружен, то
	if ($_FILES['file']['error'] != UPLOAD_ERR_OK)
	{
		exit_internal_server_error("Произошла обшибка при загрузке файла на сервер");
	} 
	else 
	{
		$id_application=CheckOrSetIdApplication();
		$extension_file = mb_strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));// получаем расширение исходного файла
		if($extension_file||isset($_GET['name']))
		{
			$txt_query= "INSERT INTO applicationfile set id_application=?, extension=?, name=?, data=compress(?);";
			$ans->id_file = execute_query_get_last_insert_id($txt_query,array('ssss',$id_application, $extension_file, $_GET['name'], file_get_contents($_FILES['file']['tmp_name']) ));
			echo nice_json_encode($ans);
		}
		else exit_internal_server_error("Произошла обшибка, не удалось получить название или расширение файла");
	}
}
else if($_GET['file_action']=='del')
{
	$id_application=CheckIdApplication();
	$txt_query= "
	DELETE from applicationfile where id_Application=? and id_ApplicationFile=?;
	";
	$ans=execute_query($txt_query,array('ss',$_id_application,$_GET['id_file']));
}
else if($_GET['file_action']=='edit')
{
	$id_application=CheckIdApplication();
	if ($_FILES['file']['error'] == UPLOAD_ERR_OK) 
	{
		$extension_file = mb_strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));// получаем расширение исходного файла
		if($extension_file||isset($_GET['name']))
		{
			$txt_query= "
			UPDATE applicationfile set extension=?, name=?, data=compress(?)
			where id_application=? and id_applicationFile=?
			;";
			$params=array('sssss', $extension_file, $_GET['name'], file_get_contents($_FILES['file']['tmp_name']),$id_application, $_GET['id_file'] );
			execute_query($txt_query,$params);
		}
		else exit_internal_server_error("Произошла обшибка, не удалось получить название или расширение файла");
	}
}

