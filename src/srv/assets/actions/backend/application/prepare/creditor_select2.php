<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/constants/alib_constants.php';
require_once '../assets/actions/backend/constants/debtorName.etalon.php';

function Request_to_probili($query_string)
{
	$url = "https://probili.ru/rest/company?auth_token=7b944ac60e6b04df5324565f2bd5fece&query=".urlencode($query_string);
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_FAILONERROR, 1);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

	$ans= curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if (200!=$httpcode){
		exit_internal_server_error("can not get information for creditor_select (code $httpcode for url \"$url\")");
	}
	curl_close($curl);
	return $ans;
}
CheckMandatoryGET('q');
if($_GET['q'] && $_GET['q']!='')
{
	$j = Request_to_probili($_GET['q']);
	$data = json_decode($j)->data->items;
	$len = count($data);
	for($i = 0; $i < $len; $i++){
		$data[$i]->text = ($data[$i]->stitle ? $data[$i]->stitle : $data[$i]->title);
		$data[$i]->text=$data[$i]->text.($data[$i]->inn ? ' (ИНН: '.$data[$i]->inn : '(');
		$data[$i]->text=$data[$i]->text.($data[$i]->ogrn ? ', ОГРН: '.$data[$i]->ogrn.')' : ')');
	}
	echo nice_json_encode(array('results'=>$data));
}
else {
	$result = array( 
		array (
		'id'=>1
		,'text'=>'Введите название, ИНН или ОГРН своей организации'
		,'disabled'=> true)

	);
	echo nice_json_encode(array('results'=>$result));
}