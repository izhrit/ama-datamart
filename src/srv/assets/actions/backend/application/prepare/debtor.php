<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

function Request_to_efrsb($query_string,$efrsb_service_url)
{
	$url = $efrsb_service_url."/dm-api.php?action=get-debtor-info&BankruptId=".urlencode($query_string);
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_FAILONERROR, 1);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

	$ans= curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if (200!=$httpcode){
		exit_internal_server_error("can not get information about debtor (code $httpcode for url \"$url\")");
	}
	curl_close($curl);
	return $ans;
}
CheckMandatoryGET_id('BankruptId');
$BankruptId= intval($_GET['BankruptId']);
echo Request_to_efrsb($_GET['BankruptId'],$use_efrsb_service_url);