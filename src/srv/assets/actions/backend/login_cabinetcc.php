<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/meeting/alib_meeting.php';
require_once '../assets/helpers/time.php';

global $trace_methods;
if ($trace_methods)
	write_to_log('------cabinetcc.login--------------------------');


CheckMandatoryGET('key');
$key= $_GET['key'];

$CabinetKey_length= 20;

$key_length= strlen($key);

if ($key_length<=$CabinetKey_length)
	exit_bad_request("length of key \"$key\" <= $CabinetKey_length !");

$CabinetKey= substr($key,0,$CabinetKey_length);
$id_Vote= substr($key,$CabinetKey_length);

$txt_query= "select
	v.id_Vote id_Vote
	,uncompress(v.Member) Member
	,uncompress(m.Body) Body
	,v.Answers Answers
	,date_format(m.Date,'%d.%m.%Y %H:%i') Date
	,d.Name debtorName
from Vote v
inner join Meeting m on v.id_Meeting = m.id_Meeting
inner join MProcedure mp on mp.id_MProcedure = m.id_MProcedure
inner join Debtor d on d.id_Debtor=mp.id_Debtor 
where v.id_Vote=? && v.CabinetKey=?
;";
$rows= execute_query($txt_query,array('ss',$id_Vote,$CabinetKey));

$count_rows= count($rows);
if (1!=$count_rows)
	exit_not_found("found $count_rows Vote with key=\"$key\"");

session_start();
$auth_info= (object)array();
$auth_info->category= 'vote';
$_SESSION['auth_info']= $auth_info;

safe_store_test_time();
$log_time= safe_date_create();
$row_log= PrepareLog($id_Vote,'Вход в кабинет',$_SERVER['REMOTE_ADDR']);

$row= $rows[0];

$meeting= json_decode($row->Body);

$i= 1;
foreach ($meeting->Вопросы as $вопрос)
{
	if (!isset($вопрос->Номер))
		$вопрос->Номер= $i;
	$i++;
}

$cabinetcc_model= array(
	'id_Vote'=>$row->id_Vote
	,'Кабинет'=>array(
		'Участник'=>json_decode($row->Member)
		,'Дата_заседания'=>$row->Date
		,'Должник'=>array(
			'Наименование'=>$row->debtorName
			,'Местонахождение'=>(!isset($meeting->Должник->Местонахождение)?'':$meeting->Должник->Местонахождение)
		)
	)
	,'Вопросы'=>$meeting->Вопросы
	,'Ответы'=>json_decode($row->Answers)
);

if (isset($meeting->Состояние))
	$cabinetcc_model['Состояние']= $meeting->Состояние;

if (isset($meeting->Результаты))
	$cabinetcc_model['Результаты']= $meeting->Результаты;

Load_ЖурналДокументы($cabinetcc_model);

