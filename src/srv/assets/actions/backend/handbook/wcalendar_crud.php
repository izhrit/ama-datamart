<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthCustomerOrManagerOrAdmin();

class Wcalendar_crud extends Base_crud
{
	function create($wcl)
	{
		$txt_query= "call safe_add_update_wcalendar(?, ?, ?);";

		$wcl= (object)$wcl;
		$days = nice_json_encode($wcl->Days);
		$kod_okato = $wcl->Kod_okato;

		$affected= execute_query_get_affected_rows($txt_query,array('iss',
			$wcl->Year, $days, $kod_okato
		));

		if (1!=$affected)
			exit_not_found("can not insert into wcalendar (affected $affected rows)");
		echo '{ "ok": true }';
	}

	function read($id_Wcalendar)
	{
		$txt_query= "select

			uncompress(wcl.Days) Days

		from wcalendar wcl
		where wcl.id_Wcalendar=?;";

		$rows= execute_query($txt_query,array('i',$id_Wcalendar));
		if (0==count($rows))
			exit_not_found("can not find wcalendar $id_Wcalendar=$id_Wcalendar");
		$row= $rows[0];

		$wcl= array
		(
			'Days'=>$row->Days
		);
		echo nice_json_encode($wcl);
	}

	function find_id_by_year($year)
	{
		$txt_query= "select

			wcl.id_wcalendar

		from wcalendar wcl
		where wcl.Year=?;";

		$rows= execute_query($txt_query,array('i',$year));
		if (0==count($rows))
			exit_not_found("can not find wcalendar year=$year");

		$id_Wcalendar = $rows[0]->id_wcalendar;
		return $id_Wcalendar;
	}

	function find_id_by_id($id_Wcalendar)
	{
		$txt_query= "
		select a.year year, b.OKATO okato
		from wcalendar a
		inner join region b on a.id_Region = b.id_Region
		where id_Wcalendar = ?;";

		$rows= execute_query($txt_query,array('i',$id_Wcalendar));
		if (0==count($rows))
			exit_not_found("can not find wcalendar id_Wcalendar=$id_Wcalendar");

		return $rows[0];
	}

	function update($id_Wcalendar, $wcl)
	{
		$data_from_db = self::find_id_by_id($id_Wcalendar);
		$days = nice_json_encode($wcl);
		$year = $data_from_db->year;
		$kod_okato = $data_from_db->okato;
		$txt_query= "call safe_add_update_wcalendar(?, ?, ?);";
		// $wcl= (object)$wcl;

		$affected= execute_query_get_affected_rows($txt_query,array('iss',
			$year, $days, $kod_okato
		));
		if (!($affected==1 || $affected==2))
			exit_not_found("can not update wcalendar (affected $affected rows)");
		echo '{ "ok": true }';
	}

	function delete($id_Wcalendar)
	{
		$id_Wcalendar = is_array($id_Wcalendar) ? $id_Wcalendar[0] : $id_Wcalendar;
		$txt_query= "delete from wcalendar where id_wcalendar=?
		;";
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		$affected= $connection->execute_query_get_affected_rows($txt_query,array('i',$id_Wcalendar));
		if (1!=$affected)
		{
			$connection->rollback();
			exit_not_found("can not delete wcalendar where id_Wcalendar=$id_Wcalendar (affected $affected rows)");
		}
		$connection->commit();
		echo '{ "ok": true }';
	}

}

$crud= new Wcalendar_crud();
$crud->process_cmd();

