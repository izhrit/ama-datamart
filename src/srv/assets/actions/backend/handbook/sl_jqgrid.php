<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthCustomerOrManagerOrAdmin();

function prepare_fields_from_alias_expression_map($alias_expression_map)
{
	$fields= '';

	$prefix= "\r\n   ";
	foreach ($alias_expression_map as $alias => $expression)
	{
		$fields.= $prefix."$expression $alias";
		$prefix= "\r\n  ,";
	}

	return $fields;
}

function prepare_std_filter_rule_builders_from_alias_expression_map($alias_expression_map)
{
	$filter_rule_builders= array();
	foreach ($alias_expression_map as $alias => $expression)
	{
		$filter_rule_builders[$alias]= prep_std_filter_rule_builder_for_expression($expression);
	}
	return $filter_rule_builders;
}

$alias_expression_map= array
(
	'id_Sub_level'=>'sl.id_Sub_level'
	,'Регион'=>'rg.Name'
	,'Дата'=>'date_format(sl.StartDate,\'%d.%m.%Y\')'

	,'Общий'=>'sl.Sum_Common'
	,'Трудоспособных'=>'sl.Sum_Employable'
	,'Пенсионеров'=>'sl.Sum_Pensioner'
	,'Несовершеннолетних'=>'sl.Sum_Infant'

	,'Публичность'=>'if(sl.id_Contract is not null,0,1)'
);

$from_where= " from sub_level sl 
inner join region rg on rg.id_Region=sl.id_Region";

if (isset($auth_info->id_Contract))
{
	$id_Contract= intval($auth_info->id_Contract);
	$from_where.= "
where (sl.id_Contract is null or sl.id_Contract=$id_Contract)";
}

$fields= prepare_fields_from_alias_expression_map($alias_expression_map);
$filter_rule_builders= prepare_std_filter_rule_builders_from_alias_expression_map($alias_expression_map);
$filter_rule_builders['Регион']=function($l,$rule) 
{
	$data= mysqli_real_escape_string($l,$rule->data);
	return " and (rg.Name like '$data%' or rg.Name like '% $data%') ";
};

if(isset($_GET['region'])&&!isset($_GET['filters']))
{
	$region= (object) array('field'=>'Регион','op'=>'cn','data'=>$_GET['region']);
	$filters= (object) array('rules'=>array($region));
	$_GET['filters']=nice_json_encode($filters);
}

$order=isset($_GET['sidx'])&&$_GET['sidx']!='' ? '' :' Регион asc, sl.StartDate desc, sl.id_Sub_level ';
if (isset($_GET['sidx']))
	$_GET['sidx'] = str_replace('Дата','sl.StartDate',$_GET['sidx']);
execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders,$order);
