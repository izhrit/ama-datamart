<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

CheckAuthCustomerOrManagerOrSROOrViewer();

$txt_query= "
	select 
		  id_Court id
		, ShortName text
	from Court 
	where SearchName like ?
	limit 20
;";

$rows= execute_query($txt_query,array('s',$_GET['q'].'%'));

echo nice_json_encode(array('results'=>$rows));