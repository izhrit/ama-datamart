<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

CheckAuthCustomerOrManagerOrSRO();

$txt_query= "
	select 
		  id_Court id
		, ShortName text
	from Court 
	where NumberPrefix is not null && char_length(NumberPrefix)>0 && upper(NumberPrefix)=upper(left(?,char_length(NumberPrefix)))
	limit 2
;";

$rows= execute_query($txt_query,array('s',$_GET['number']));

$count_rows= count($rows);
$res= (1!=$count_rows) ? null : $rows[0];
echo nice_json_encode($res);
