<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthCustomerOrManagerOrAdmin();

function change_empty_srtings_to_null($obj)
{
	$fixed= array();
	foreach ($obj as $name => $value)
	{
		$fixed[$name]= ''==$value ? null : $value;
	}
	return $fixed;
}

class Court_crud extends Base_crud
{
	function create($court)
	{
		CheckAuthAdmin();
		$court= change_empty_srtings_to_null($court);
		$txt_query= 'insert into Court set Name=?, ShortName=?, SearchName=?, CasebookTag=?, Address=?, NumberPrefix=?;';
		execute_query_no_result($txt_query,array('ssssss',
			$court['Name'],$court['ShortName'],$court['SearchName'],$court['CasebookTag'],$court['Address'],$court['NumberPrefix']));
		echo '{ "ok": true }';
	}

	function read($id_Court)
	{
		global $auth_info;
		$txt_query= "select
			id_Court
			,Name
			,ShortName
			,SearchName
			,CasebookTag
			,Address
			,NumberPrefix
		from Court
		where id_Court=?;";
		$rows= execute_query($txt_query,array('s',$id_Court));
		if (0==count($rows))
			exit_not_found("can not find Court id_Court=$id_Court");
		$row= $rows[0];
		echo nice_json_encode($row);
	}


	function update($id_Court,$court)
	{
		CheckAuthAdmin();
		$court= change_empty_srtings_to_null($court);
		$txt_query= 'update Court set Name=?, ShortName=?, SearchName=?, CasebookTag=?, Address=?, NumberPrefix=? where id_Court=?;';
		execute_query_no_result($txt_query,array('sssssss',
			$court['Name'],$court['ShortName'],$court['SearchName'],$court['CasebookTag'],$court['Address'],$court['NumberPrefix'],$id_Court));
		echo '{ "ok": true }';
	}

	function delete($id_Courts)
	{
		CheckAuthAdmin();
		$txt_query= 'delete from Court where id_Court=?;';
		execute_query_no_result($txt_query,array('s',$id_Courts[0]));
		echo '{ "ok": true }';
	}
}

$crud= new Court_crud();
$crud->process_cmd();

