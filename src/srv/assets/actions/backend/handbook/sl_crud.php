<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthCustomerOrManagerOrAdmin();

class Sub_level_crud extends Base_crud
{
	function create($sl)
	{
		global $auth_info;
		$txt_query= "insert into sub_level set
			id_Region=(select region.id_Region from region where OKATO=?)
			,StartDate=?
			,Sum_Common=?, Sum_Employable=?, Sum_Pensioner=?, Sum_Infant=?
			,Reglament_Title=?, Reglament_Date=?, Reglament_Url=?
			,id_Contract=?;";
		$sl= (object)$sl;
		$pmin= function($name) use ($sl)
		{
			if (!isset($sl->Прожиточный_минимум))
				return null;
			$sum= $sl->Прожиточный_минимум;
			if (!isset($sum[$name]))
				return null;
			$pm= $sum[$name];
			return !isset($pm['Задан']) || 'true'!=$pm['Задан'] ? null : $pm['Значение'];
		};
		$rg= (object)$sl->Регламентирующий_акт;
		$affected= execute_query_get_affected_rows($txt_query,array('ssssssssss',
			$sl->Регион,date_format(date_create_from_format('d.m.Y',$sl->Дата),'Y-m-d')
			,$pmin('Подушевой'),$pmin('Трудоспособных'),$pmin('Пенсионеров'),$pmin('Несовершеннолетних')
			,$rg->Название
			,(''==$rg->Дата ? null : date_format(date_create_from_format('d.m.Y',$rg->Дата),'Y-m-d'))
			,$rg->Ссылка,!isset($auth_info->id_Contract)?null:$auth_info->id_Contract
		));
		if (1!=$affected)
			exit_not_found("can not insert into sub_level (affected $affected rows)");
		echo '{ "ok": true }';
	}

	function read($id_Sub_level)
	{
		global $auth_info;
		$txt_query= "select
			date_format(sl.StartDate,'%d.%m.%Y') StartDate
			,rg.OKATO

			,sl.Sum_Common
			,sl.Sum_Employable
			,sl.Sum_Pensioner
			,sl.Sum_Infant

			,sl.Reglament_Title
			,date_format(sl.Reglament_Date,'%d.%m.%Y') Reglament_Date
			,sl.Reglament_Url

			,sl.id_Contract
			,c.ContractNumber
			,sl.id_Sub_level

		from sub_level sl 
		inner join region rg on sl.id_Region=rg.id_Region
		left join Contract c on c.id_Contract=sl.id_Contract
		where sl.id_Sub_level=?;";

		$rows= execute_query($txt_query,array('s',$id_Sub_level));
		if (0==count($rows))
			exit_not_found("can not find sub_level id_Sub_level=$id_Sub_level");
		$row= $rows[0];
		if (null!=$row->id_Contract && isset($auth_info->id_Contract) && $row->id_Contract!=$auth_info->id_Contract)
			exit_not_found("can not get access sub_level id_Sub_level=$id_Sub_level for id_Contract={$auth_info->id_Contract}");
		$sl= array
		(
			'Дата'=>$row->StartDate
			,'Регион'=> $row->OKATO
			,'Прожиточный_минимум'=> array
			(
				'Подушевой'=>array('Задан'=>null!=$row->Sum_Common, 'Значение'=>$row->Sum_Common)
				,'Трудоспособных'=>array('Задан'=>null!=$row->Sum_Employable, 'Значение'=>$row->Sum_Employable)
				,'Пенсионеров'=>array('Задан'=>null!=$row->Sum_Pensioner, 'Значение'=>$row->Sum_Pensioner)
				,'Несовершеннолетних'=>array('Задан'=>null!=$row->Sum_Infant, 'Значение'=>$row->Sum_Infant)
			)
			,'Регламентирующий_акт'=> array
			(
				'Название'=>$row->Reglament_Title
				,'Дата'=>$row->Reglament_Date
				,'Ссылка'=>$row->Reglament_Url
			)
			, 'ContractNumber'=>$row->ContractNumber
			, 'id_Sub_level'=>$row->id_Sub_level
		);
		echo nice_json_encode($sl);
	}

	static function prepare_update_delete_contract_check()
	{
		global $auth_info;
		if (!isset($auth_info->id_Contract))
		{
			return " and id_Contract is null;";
		}
		else
		{
			$id_Contract= intval($auth_info->id_Contract);
			return " and id_Contract=$id_Contract;";
		}
	}

	function update($id_Sub_level,$sl)
	{
		$txt_query= "update sub_level set
			id_Region=(select region.id_Region from region where OKATO=?)
			,StartDate=?

			,Sum_Common=?, Sum_Employable=?, Sum_Pensioner=?, Sum_Infant=?

			,Reglament_Title=?, Reglament_Date=?, Reglament_Url=?

			where id_Sub_level=?
		"
		.self::prepare_update_delete_contract_check();
		$sl= (object)$sl;
		$pmin= function($name) use ($sl)
		{
			if (!isset($sl->Прожиточный_минимум))
				return null;
			$sum= $sl->Прожиточный_минимум;
			if (!isset($sum[$name]))
				return null;
			$pm= $sum[$name];
			return !isset($pm['Задан']) || 'true'!=$pm['Задан'] ? null : $pm['Значение'];
		};
		$rg= (object)$sl->Регламентирующий_акт;
		$affected= execute_query_get_affected_rows($txt_query,array('ssssssssss',
			$sl->Регион,date_format(date_create_from_format('d.m.Y',$sl->Дата),'Y-m-d')
			,$pmin('Подушевой'),$pmin('Трудоспособных'),$pmin('Пенсионеров'),$pmin('Несовершеннолетних')
			,$rg->Название
			,(''==$rg->Дата ? null : date_format(date_create_from_format('d.m.Y',$rg->Дата),'Y-m-d'))
			,$rg->Ссылка,$id_Sub_level
		));
		if (1!=$affected)
			exit_not_found("can not update sub_level (affected $affected rows)!");
		echo '{ "ok": true }';
	}

	function delete($id_Sub_levels)
	{
		$txt_query= "delete from sub_level where id_Sub_level=?"
			.self::prepare_update_delete_contract_check();

		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		foreach ($id_Sub_levels as $id_Sub_level)
		{
			$affected= $connection->execute_query_get_affected_rows($txt_query,array('i',$id_Sub_level));
			if (1!=$affected)
			{
				$connection->rollback();
				exit_not_found("can not delete sub_level where id_Sub_level=$id_Sub_level (affected $affected rows)");
			}
		}
		$connection->commit();
		echo '{ "ok": true }';
	}
}

$crud= new Sub_level_crud();
$crud->process_cmd();

