<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthManager();

class Price_efrsb_crud extends Base_crud
{
	function create($pefrsb)
	{
		global $auth_info;
		$txt_query= "insert into price_efrsb set
			date_of_set_price=?
			, price_for_physical_entity=?
			, price_for_physical_entity_with_commission=?
			, price_for_legal_entity=?			
			, price_for_legal_entity_with_commission=?;";
		$pefrsb= (object)$pefrsb;
		$pmin= function($name) use ($pefrsb)
		{
			if (!isset($pefrsb->Сумма_за_сообщение))
				return null;
			$sum= $pefrsb->Сумма_за_сообщение;
			if (!isset($sum[$name]))
				return null;
			$pm= $sum[$name];
			return !isset($pm['Задан']) || 'true'!=$pm['Задан'] ? null : $pm['Значение'];
		};
		$affected= execute_query_get_rows($txt_query,array('sssss',
			date_format(date_create_from_format('d.m.Y',$pefrsb->Дата),'Y-m-d')
			,$pmin('Физлицо'),$pmin('Физлицо_с_комиссией'),$pmin('Юрлицо'),$pmin('Юрлицо_с_комиссией')
		));
		if (1!=$affected)
			exit_not_found("can not insert into price_efrsb (affected $affected rows)");
		echo '{ "ok": true }';
	}

	function read($id_pefrsb)
	{
		global $auth_info;
		$txt_query= "select
			id_pefrsb
			,date_format(pefrsb.date_of_set_price,'%d.%m.%Y') date_of_set_price

			,price_for_physical_entity
			,price_for_physical_entity_with_commission
			,price_for_legal_entity
			,price_for_legal_entity_with_commission
			
		from price_efrsb pefrsb 
		where id_pefrsb=?;";

		$rows= execute_query($txt_query,array('s',$id_pefrsb));
		if (0==count($rows))
			exit_not_found("can not find price_efrsb id_pefrsb=$id_pefrsb");
		$row= $rows[0];
		$pefrsb= array
		(
			'id_pefrsb'=>$row->id_pefrsb
			,'Дата'=>$row->date_of_set_price
			,'Сумма_за_сообщение'=> array
			(
				'Физлицо'=>array('Задан'=>null!=$row->price_for_physical_entity, 'Значение'=>$row->price_for_physical_entity)
				,'Физлицо_с_комиссией'=>array('Задан'=>null!=$row->price_for_physical_entity_with_commission, 'Значение'=>$row->price_for_physical_entity_with_commission)	
				,'Юрлицо'=>array('Задан'=>null!=$row->price_for_legal_entity, 'Значение'=>$row->price_for_legal_entity)				
				,'Юрлицо_с_комиссией'=>array('Задан'=>null!=$row->price_for_legal_entity_with_commission, 'Значение'=>$row->price_for_legal_entity_with_commission)							
			)			
		);
		echo nice_json_encode($pefrsb);
	}



	function update($id_pefrsb,$pefrsb)
	{

		$txt_query= "update price_efrsb set date_of_set_price=?, price_for_physical_entity=?, price_for_legal_entity=?, price_for_physical_entity_with_commission=?, price_for_legal_entity_with_commission=? where id_pefrsb=?";
		$pefrsb= (object)$pefrsb;

		$pmin= function($name) use ($pefrsb)
		{
			if (!isset($pefrsb->Сумма_за_сообщение))
				return null;
			$sum= $pefrsb->Сумма_за_сообщение;
			if (!isset($sum[$name]))
				return null;
			$pm= $sum[$name];
			return !isset($pm['Задан']) || 'true'!=$pm['Задан'] ? null : empty($pm['Значение']) ? null : $pm['Значение'];
		};
		
		execute_query_no_result($txt_query,array('ssssss',
			date_format(date_create_from_format('d.m.Y',$pefrsb->Дата),'Y-m-d')
			,$pmin('Физлицо'),$pmin('Юрлицо'),$pmin('Физлицо_с_комиссией'),$pmin('Юрлицо_с_комиссией')
			,$id_pefrsb
		));

		echo '{ "ok": true }';
	}

	function delete($id_pefrsbs)
	{
		$txt_query= "delete from price_efrsb where id_pefrsb=?";

		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		foreach ($id_pefrsbs as $id_pefrsb)
		{
			$affected= $connection->execute_query_get_affected_rows($txt_query,array('i',$id_pefrsb));
			if (1!=$affected)
			{
				$connection->rollback();
				exit_not_found("can not delete price_efrsb where id_pefrsb=$id_pefrsb (affected $affected rows)");
			}
		}
		$connection->commit();
		echo '{ "ok": true }';
	}
}

$crud= new Price_efrsb_crud();
$crud->process_cmd();
