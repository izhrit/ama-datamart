<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/helpers/log.php';

$auth_info= CheckAuthCustomerOrManagerOrAdmin();

$date = isset($_GET['date']) ? $_GET['date'] : '';
$region = isset($_GET['region']) ? $_GET['region'] : '';

$txt_query_prefix= " select
sub.id_Region
, sub.StartDate
, sub.Sum_Common Common
, sub.Sum_Employable Employable
, sub.Sum_Infant Infant
, sub.Sum_Pensioner Pensioner
, sub.Reglament_Title Title
, sub.Reglament_Date Date
, sub.Reglament_Url Url
from sub_level sub
inner join region r on sub.id_Region = r.id_Region
";
$order="order by sub.StartDate desc , sub.id_Contract desc;";
if (!isset($auth_info->id_Contract) &&!isset($auth_info->id_Manager))
{
	$from_where= "
	where r.OKATO=? and sub.StartDate <= ? ";
	$query = $txt_query_prefix . $from_where . $order;
	$rows = execute_query($query,array('ss',$region,$date));
}
else
{
	if(isset($auth_info->id_Contract))
    {
        $id_Contract = intval($auth_info->id_Contract);
        $from_where= "where (sub.id_Contract is null or sub.id_Contract=?) and r.OKATO=? and sub.StartDate <= ? 
        ";
        $paramatrs=array('sss',$id_Contract, $region,$date);
    }
    else if(isset($auth_info->id_Manager))
    {
        $id_Manager = intval($auth_info->id_Manager);
        $from_where= "
        inner join Contract c on sub.id_Contract = c.id_Contract
        inner join Manager m on c.id_Contract = m.id_Contract
        where (sub.id_Contract is null or m.id_Manager=?) and r.OKATO=? and sub.StartDate <= ? 
        ";
        $paramatrs=array('sss',$id_Manager, $region,$date);
        
    }
	$query = $txt_query_prefix . $from_where . $order;
    $rows=execute_query($query,$paramatrs);
}
$result= (object)array
(
	'Common'=>null
	,'Infant'=>null
	,'Employable'=>null
	,'Pensioner'=>null

	,'Reglaments'=>array()
);

function try_process_field($field_Name, $row, $result)
{
	if (null!=$row->$field_Name && null==$result->$field_Name)
	{
		$result->$field_Name= $row->$field_Name;
		$reglament= array('title'=>$row->Title,'date'=>$row->Date,'url'=>$row->Url);
		if (!in_array($reglament,$result->Reglaments))
			$result->Reglaments[]= $reglament;
	}
}

function finish_process_fields($result)
{
	return null!=$result->Common && null!=$result->Employable && null!=$result->Infant && null!=$result->Pensioner;
}

foreach ($rows as $row)
{
	try_process_field('Common', $row, $result);
	try_process_field('Employable', $row, $result);
	try_process_field('Infant', $row, $result);
	try_process_field('Pensioner', $row, $result);

	if (finish_process_fields($result))
		break;
}

echo(nice_json_encode($result));
