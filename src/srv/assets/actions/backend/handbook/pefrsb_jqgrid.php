<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthManager();

function prepare_fields_from_alias_expression_map($alias_expression_map)
{
	$fields= '';

	$prefix= "\r\n   ";
	foreach ($alias_expression_map as $alias => $expression)
	{
		$fields.= $prefix."$expression $alias";
		$prefix= "\r\n  ,";
	}

	return $fields;
}

function prepare_std_filter_rule_builders_from_alias_expression_map($alias_expression_map)
{
	$filter_rule_builders= array();
	foreach ($alias_expression_map as $alias => $expression)
	{
		$filter_rule_builders[$alias]= prep_std_filter_rule_builder_for_expression($expression);
	}
	return $filter_rule_builders;
}

$alias_expression_map= array
(
	'id_pefrsb'=>'id_pefrsb'
	,'Дата'=>'date_format(date_of_set_price,\'%d.%m.%Y\')'
	,'Физлицо'=>'price_for_physical_entity'
	,'Физлицо_с_комиссией'=>'price_for_physical_entity_with_commission'
	,'Юрлицо'=>'price_for_legal_entity'	
	,'Юрлицо_с_комиссией'=>'price_for_legal_entity_with_commission'	
);

$from_where= " from price_efrsb ";

$fields= prepare_fields_from_alias_expression_map($alias_expression_map);
$filter_rule_builders= prepare_std_filter_rule_builders_from_alias_expression_map($alias_expression_map);

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
