<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthCustomerOrManagerOrAdmin();

function prepare_fields_from_alias_expression_map($alias_expression_map)
{
	$fields= '';

	$prefix= "\r\n   ";
	foreach ($alias_expression_map as $alias => $expression)
	{
		$fields.= $prefix."$expression $alias";
		$prefix= "\r\n  ,";
	}

	return $fields;
}

function prepare_std_filter_rule_builders_from_alias_expression_map($alias_expression_map)
{
	$filter_rule_builders= array();
	foreach ($alias_expression_map as $alias => $expression)
	{
		$filter_rule_builders[$alias]= prep_std_filter_rule_builder_for_expression($expression);
	}
	return $filter_rule_builders;
}

$alias_expression_map= array
(
	'id_Rosreestr'=>'id_Rosreestr'
	,'Name'=>'Name'
);

$from_where= " from Rosreestr ";

$fields= prepare_fields_from_alias_expression_map($alias_expression_map);
$filter_rule_builders= prepare_std_filter_rule_builders_from_alias_expression_map($alias_expression_map);

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
