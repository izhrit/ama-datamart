<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthCustomerOrManagerOrAdmin();

function change_empty_srtings_to_null($obj)
{
	$fixed= array();
	foreach ($obj as $name => $value)
	{
		$fixed[$name]= ''==$value ? null : $value;
	}
	return $fixed;
}

class Rosreestr_crud extends Base_crud
{
	function create($rosreestr)
	{
		CheckAuthAdmin();
		$rosreestr= change_empty_srtings_to_null($rosreestr);
		$txt_query= 'insert into Rosreestr set id_Region = (select region.id_Region from region where OKATO=?) 
					, Name=?, Address=?, Latitude=?, Longitude=?;';
		execute_query_no_result($txt_query,array('sssss',
			$rosreestr['Region'],$rosreestr['Name'],$rosreestr['Address'],$rosreestr['Latitude'],$rosreestr['Longitude']));
		echo '{ "ok": true }';
	}

	function read($id_Rosreestr)
	{
		global $auth_info;
		$txt_query= "select
			 id_Rosreestr
			,Name
			,Address
			,Latitude
			,Longitude
		from Rosreestr
		where id_Rosreestr=?;";
		$rows= execute_query($txt_query,array('s',$id_Rosreestr));
		if (0==count($rows))
			exit_not_found("can not find Rosreestr id_Rosreestr=$id_Rosreestr");
		$row= $rows[0];
		echo nice_json_encode($row);
	}


	function update($id_Rosreestr,$rosreestr)
	{
		CheckAuthAdmin();
		$rosreestr= change_empty_srtings_to_null($rosreestr);
		$txt_query= 'update Rosreestr set id_Region = (select region.id_Region from region where OKATO=?) 
					, Name=?, Address=?, Latitude=?, Longitude=? where id_Rosreestr = ?;';
		execute_query_no_result($txt_query,array('ssssss',
			$rosreestr['Region'],$rosreestr['Name'],$rosreestr['Address'],$rosreestr['Latitude'],$rosreestr['Longitude'],$id_Rosreestr));
		echo '{ "ok": true }';
	}

	function delete($id_Rosreestrs)
	{
		CheckAuthAdmin();
		$txt_query= 'delete from Rosreestr where id_Rosreestr=?;';
		execute_query_no_result($txt_query,array('s',$id_Rosreestrs[0]));
		echo '{ "ok": true }';
	}
}

$crud= new Rosreestr_crud();
$crud->process_cmd();

