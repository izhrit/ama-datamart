<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/constants/alib_constants.php';
require_once '../assets/actions/backend/constants/debtorName.etalon.php';

CheckMandatoryGET('q');
$request_query_prefix= $_GET['q'];

function RequestDebtorsSelect2_efrsb($request_query_prefix)
{
	global $use_efrsb_service_url;
	$request_query_prefix= urlencode($request_query_prefix);
	$url= "$use_efrsb_service_url/dm-api.php/debtors-for-select2?q=$request_query_prefix";

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, 1);

	$curl_response= curl_exec($curl);
	$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);
	if (200!=$httpcode)
		exit_internal_server_error("can not request debtors for select2 (code $httpcode for url \"$url\")");

	$curl_response_body= substr($curl_response, $header_size);

	header('Content-Type: text/plain');
	echo $curl_response_body;
}
$request_query_prefix = str_replace('\"','"',$request_query_prefix);
RequestDebtorsSelect2_efrsb($request_query_prefix);
