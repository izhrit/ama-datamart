<?

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

CheckAuth();

function Request_to_efrsb($query_string)
{
	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/dm-api.php?action=messages&'.$query_string;
	write_to_log($url);
	$curl = curl_init();
	$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FAILONERROR, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

	$ans= curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if (200!=$httpcode)
		exit_internal_server_error("can not get messages for manager_efrsb (code $httpcode for url \"$url\")");

	curl_close($curl);

	return $ans;
}

$id= CheckMandatoryGET('id_Procedure');

switch ($id[0])
{
	case 'r': // id=id_Request - ������������� ������� �� ������ � ��������� �� �����������
		$id= substr($id,1);
		$txt_query= "select d.Bankruptid Bankruptid
			from Debtor d 
			inner join Request re on re.id_Debtor=d.id_Debtor
			where re.id_Request=?;";
		break;
	case 'e': // id=id_Debtor_Manager - ������������� ��������� � ����� ��� �� ��� ��������
		$id= substr($id,1);
		$txt_query= "select edm.Bankruptid Bankruptid
			from efrsb_debtor_manager edm
			where edm.id_Debtor_Manager=?;";
		break;
	default: // id=id_MProcedure
		$txt_query= "select d.Bankruptid Bankruptid
			from Debtor d
			inner join MProcedure mp on mp.id_Debtor=d.id_Debtor
			where mp.id_MProcedure=?;";
}

function empty_result()
{
	echo nice_json_encode(array('page'=>1,'total'=>0,'records'=>0,'rows'=>array()));
	exit;
}

$rows= execute_query($txt_query,array('i',$id));

$count_rows= count($rows);
if (0==$count_rows)
	empty_result();

if (1!=$count_rows)
	exit_internal_server_error("found $count_rows Bankruptid!");
		
$Bankruptid= $rows[0]->Bankruptid;
if (null==$Bankruptid)
	empty_result();

$extra_args= str_replace('action=efrsb.jqgrid&','',$_SERVER['QUERY_STRING']);
echo Request_to_efrsb("BankruptId=$Bankruptid&$extra_args");


