<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManager();

function jqgrid_for_manager()
{
	global $auth_info;
	global $id_Manager;
	CheckMandatoryGET_id('id_Manager');
	$id_Manager= intval($_GET['id_Manager']);
	CheckAccessToManagerIfManager($auth_info, $id_Manager);

	$fields= "
		id_ManagerUser
		,id_MUser
		,id_Manager
		,UserName
		,DefaultViewer
		,(
			select group_concat(distinct d.Name order by d.id_Debtor separator \", \")
			from MProcedureUser mpu
			inner join MProcedure mp on mpu.id_MProcedure = mp.id_MProcedure
			inner join Debtor d on d.id_Debtor=mp.id_Debtor 
			where mpu.id_ManagerUser = ManagerUser.id_ManagerUser
			&& mp.id_Manager=$id_Manager
		) Procedures
	";
	$from_where= "from ManagerUser where id_Manager=$id_Manager";
	$filter_rule_builders= array(
		'UserName'=>'std_filter_rule_builder'
		,'Procedures'=> function($l,$rule)
		{
			global $id_Manager;
			$dt= mysqli_real_escape_string($l,$rule->data);
			return " and exists
				(
					select 
						mp.id_MProcedure
					from MProcedureUser mpu
					inner join MProcedure mp on mpu.id_MProcedure = mp.id_MProcedure
					inner join Debtor d on d.id_Debtor=mp.id_Debtor 
					where mpu.id_ManagerUser = ManagerUser.id_ManagerUser
					&& mp.id_Manager=$id_Manager
					&& d.Name like '%$dt%'
				) ";
		}
	);
	execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
}

function jqgrid_for_customer()
{
	global $auth_info;
	$fields= "
		id_ContractUser
		,UserName
	";
	$from_where= "from ContractUser where id_Contract={$auth_info->id_Contract}";

	$filter_rule_builders= array(
		'UserName'=>'std_filter_rule_builder'
	);
	execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
}

switch ($auth_info->category)
{
	case 'manager': jqgrid_for_manager(); break;
	case 'customer': jqgrid_for_customer(); break;
}