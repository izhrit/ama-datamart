<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthViewer();

CheckMandatoryGET_id('id_MUser');
$id_MUser= intval($_GET['id_MUser']);

$fields= "
	 md.id_MData id_MData
	,md.MData_Type MData_Type
	,mu.UserName FromUser
	,concat(m.lastName, ' ', m.firstName, ' ', m.middleName) ToManager
	,md.publicDate publicDate
	,d.Name ForDebtor
";

$from_where= " from MData md 
inner join ManagerUser mu on md.id_ManagerUser=mu.id_ManagerUser
inner join Manager m on m.id_Manager=mu.id_Manager
left join Debtor d on d.id_Debtor=md.id_Debtor
where mu.id_MUser=$id_MUser ";

$filter_rule_builders= array(
	'MData_Type'=>'std_filter_rule_builder'
	,'FromUser'=>'std_filter_rule_builder'
	,'ToManager'=>prep_std_filter_rule_builder_for_expression('m.lastName')
	,'publicDate'=>'std_filter_rule_builder'
);
execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
