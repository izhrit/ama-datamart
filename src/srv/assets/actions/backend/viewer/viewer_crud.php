<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/viewer/alib_viewer.php';
require_once '../assets/actions/backend/viewer/viewer_crud_for_customer.php';

global $auth_info;
$auth_info= CheckAuth_cmd_Category(array(
	'add'=>array('manager','customer')
	,'get'=>array('manager','customer')
	,'update'=>array('manager','customer','viewer')
	,'delete'=>array('manager','customer')
));

class viewer_crud extends Base_crud
{
	function PrepareNewViewerLetter($user_name,$password)
	{
		$letter= (object)array('subject'=>'Учётные данные на Витрине данных ПАУ');
		ob_start();
		include "../assets/actions/backend/viewer/letters/password.php";
		$letter->body_txt= ob_get_contents();
		ob_end_clean();
		return $letter;
	}

	function insert_or_find_MUser($viewer,$connection)
	{
		$txt_query= "select id_MUser from MUser where UserEmail=?;";
		$rows= $connection->execute_query($txt_query,array('s',$viewer['Email']));
		if (0!=count($rows))
		{
			return $rows[0]->id_MUser;
		}
		else
		{
			global $email_settings;
			$new_password= (true!=$email_settings->enabled) ? 'new_test_pass' : generate_password(8);

			$txt_query= "insert into MUser set CreatedTime= now(), UserEmail=?, UserName=?, UserPassword=?;";
			$viewer['id_MUser']= $connection->execute_query_get_last_insert_id($txt_query,
					array('sss',$viewer['Email'],$viewer['Имя'],$new_password));

			$letter= self::PrepareNewViewerLetter($viewer['Имя'],$new_password);

			if (false==PostLetter($letter,$viewer['Email'],$viewer['Имя'],'создание наблюдателя',$viewer['id_MUser']))
			{
				exit_internal_server_error("can not post letter to {$viewer['Email']}!");
			}
			else
			{
				return $viewer['id_MUser'];
			}
		}
	}

	function insert_MProcedureUsers($id_Manager,$id_ManagerUser,$procedures,$connection)
	{
		foreach ($procedures as $procedure)
		{
			$id_MProcedure= $procedure['id'];
			if (0===strpos($string2, 'e'))
				continue;
			$id_Request=isset($procedure['id_Request']) ? $procedure['id_Request'] : 'null';
			$txt_query= "
			insert into MProcedureUser (
				  id_ManagerUser
				, id_MProcedure
				, id_Request
			)
			select 
				  ?
				, id_MProcedure
				"; 
			$txt_query.= $id_Request!='null' ? ', ?':", null";
			$txt_query.="
			from MProcedure
			where id_Manager=? && id_MProcedure=?;";

			$affected= $id_Request!='null' ? $connection->execute_query_get_affected_rows($txt_query,array('iiii',$id_ManagerUser,$id_Request,$id_Manager,$id_MProcedure))
			: $connection->execute_query_get_affected_rows($txt_query,array('iii',$id_ManagerUser,$id_Manager,$id_MProcedure));
			if (1!=$affected)
			{
				$connection->rollback();
				exit_not_found("can not insert MProcedureUser where id_Manager=$id_Manager && id_MProcedure=$id_MProcedure (affected $affected rows)");
			}
			$txt_query= "update Request set State='p' where id_Request=?;";
			$connection->execute_query_no_result($txt_query,array('i',$id_Request));
		}
	}

	function check_Object()
	{
		$this->check_Object_mandatory_visible_string('Имя');
		$this->check_Object_mandatory_email('Email');
	}

	function create($viewer)
	{
		global $auth_info;
		$this->check_Object();
		$this->check_Object_mandatory('id_Manager');
		$id_Manager= $viewer['id_Manager'];
		CheckAccessToManagerIfManager($auth_info, $id_Manager);

		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		try
		{
			$id_MUser= $this->insert_or_find_MUser($viewer,$connection);

			$fname= 'Давать_доступ_ко_всем_процедурам_по_умолчанию';
			$DefaultViewer= (isset($viewer[$fname])&&'true'==$viewer[$fname])?1:0;
			$txt_query= "insert into ManagerUser set UserName=?, DefaultViewer=?, id_MUser=?, id_Manager=?;";
			$id_ManagerUser= $connection->execute_query_get_last_insert_id($txt_query,array('siii',
				$viewer['Имя'],$DefaultViewer,$id_MUser,$id_Manager));

			if (isset($viewer['Доступ_к_процедурам']))
				$this->insert_MProcedureUsers($id_Manager,$id_ManagerUser,$viewer['Доступ_к_процедурам'],$connection);

			$connection->commit();

			echo '{ "ok": true }';
		}
		catch (mysqli_sql_exception $ex)
		{
			$connection->rollback();
			ProcessMySqlException_check_duplicate_Viewer_login($ex, $viewer['Email']);
		}
		catch (Exception $ex)
		{
			$connection->rollback();
			throw $ex;
		}
	}

	function read($id)
	{
		global $auth_info;
		$txt_query= "
			select 
				  mu.UserName Имя
				, u.UserEmail Email
				, mu.id_ManagerUser id_ManagerUser
				, mu.DefaultViewer Давать_доступ_ко_всем_процедурам_по_умолчанию
				, u.UserName ИзвестныйКак
				, u.Confirmed || 1<>(select count(*) from ManagerUser muu where muu.id_MUser=mu.id_MUser) Confirmed
			from ManagerUser mu
			inner join MUser u on mu.id_MUser = u.id_MUser
			where mu.id_ManagerUser=? && mu.id_Manager=?
		;";
		$rows= execute_query($txt_query,array('ss',$id,$auth_info->id_Manager));
		if (0==count($rows))
			exit_not_found("can not find viewer id_ManagerUser=$id");
		$viewer= $rows[0];
		if (1==$viewer->Давать_доступ_ко_всем_процедурам_по_умолчанию)
			$viewer->Давать_доступ_ко_всем_процедурам_по_умолчанию= true;

		$txt_query= "
			select 
				mp.id_MProcedure id
				, concat(d.Name,', ',ProcedureType_SafeShortForDBValue(mp.procedure_type)) text
				,mpu.id_Request id_Request
			from MProcedureUser mpu
			inner join MProcedure mp on mpu.id_MProcedure = mp.id_MProcedure
			inner join Debtor d on d.id_Debtor=mp.id_Debtor 
			where mpu.id_ManagerUser = ? && mp.id_Manager=?
		;";
		$rows= execute_query($txt_query,array('ss',$id,$auth_info->id_Manager));
		$viewer->Доступ_к_процедурам= $rows;
		echo nice_json_encode($viewer);
	}

	function update_MUser($id_MUser,$viewer,$connection,$row_viewer)
	{
		if ($row_viewer->UserEmail==$viewer['Email'])
		{
			$txt_query= "update MUser set UserName=? where id_MUser = ?";
			$connection->execute_query_no_result($txt_query,array('ss',$viewer['Имя'],$id_MUser));
		}
		else
		{
			global $email_settings;
			$new_password= (true!=$email_settings->enabled) ? 'new_test_pass' : generate_password(8);

			$txt_query= "update MUser set UserName=?, UserEmail=?, UserPassword=? where id_MUser = ?";
			$connection->execute_query_no_result($txt_query,array('ssss',$viewer['Имя'],$viewer['Email'],$new_password,$id_MUser));

			$letter= self::PrepareNewViewerLetter($viewer['Имя'],$new_password);
			if (false==PostLetter($letter,$viewer['Email'],$viewer['Имя'],'смена email наблюдателя',$id_MUser))
				throw new Exception("can not post letter to {$viewer['Email']}!");
		}
	}

	function update_as_manager($id_ManagerUser,$viewer,$connection)
	{
		global $auth_info;
		$this->check_Object_mandatory('id_Manager');

		$txt_query= "select 
				  mu.id_ManagerUser id_ManagerUser
				, mu.id_MUser id_MUser
				, u.Confirmed Confirmed
				, u.UserEmail UserEmail
			from ManagerUser mu 
			inner join MUser u on mu.id_MUser=u.id_MUser
			where mu.id_ManagerUser = ? && mu.id_Manager=?;";
		$rows= $connection->execute_query($txt_query,array('ss',$id_ManagerUser,$auth_info->id_Manager));
		$count_rows= count($rows);
		if (1!=$count_rows)
			exit_not_found("found $count_rows rows to update");

		$id_Manager= $viewer['id_Manager'];
		CheckAccessToManagerIfManager($auth_info, $id_Manager);

		$ManagerUser= $rows[0];
		$id_MUser= $ManagerUser->id_MUser;

		if (!$ManagerUser->Confirmed)
			self::update_MUser($id_MUser,$viewer,$connection,$rows[0]);

		$fname= 'Давать_доступ_ко_всем_процедурам_по_умолчанию';
		$DefaultViewer= (isset($viewer[$fname])&&'true'==$viewer[$fname])?1:0;
		$txt_query= "update ManagerUser set UserName=?, DefaultViewer=? where id_ManagerUser = ?";
		$connection->execute_query_no_result($txt_query,array('sii'
			,$viewer['Имя'],$DefaultViewer,$id_ManagerUser));

		$txt_query= "
			delete mpu 
			from MProcedureUser mpu 
			inner join MProcedure mp on mp.id_MProcedure=mpu.id_MProcedure
			where mp.id_Manager=? && mpu.id_ManagerUser=?;";
		$connection->execute_query_no_result($txt_query,array('ii',$viewer['id_Manager'],$id_ManagerUser));

		$txt_query= "update Request set State='r' where State='p' && id_MUser=?;";
		$connection->execute_query_no_result($txt_query,array('i',$id_MUser));

		if (isset($viewer['Доступ_к_процедурам']))
			$this->insert_MProcedureUsers($id_Manager,$ManagerUser->id_ManagerUser,$viewer['Доступ_к_процедурам'],$connection);
	}

	function update_as_viewer($id_MUser,$viewer,$connection)
	{
		global $auth_info;
		CheckAccessToViewerIfViewer($auth_info, $id_MUser);

		$rows= $connection->execute_query("select UserEmail from MUser where id_MUser=?;",array('s',$id_MUser));
		$count_rows= count($rows);
		if (1!=$count_rows)
			exit_not_found("found $count_rows rows to update");

		$row_viewer= $rows[0];
		if ($viewer['Email']==$row_viewer->UserEmail)
		{
			$connection->execute_query_no_result("update MUser set UserName=? where id_MUser=?;",
				array('ss',$viewer['Имя'],$auth_info->id_MUser));
		}
		else
		{
			$rows= $connection->execute_query("select id_MUser from MUser where id_MUser!=? && UserEmail=?;",
												array('ss',$id_MUser,$viewer['Email']));
			$count_rows= count($rows);
			if (0!=$count_rows)
			{
				write_to_log("found $count_rows viewers with email {$viewer['Email']}");
				$connection->rollback();
				exit_duplicate_viewer_login();
			}
			global $email_settings;
			$new_password= (true!=$email_settings->enabled) ? 'new_test_pass' : generate_password(8);
			$txt_query= "update MUser set 
					  UserName=?
					, UserEmail_Change=?,    UserEmail_ChangeTime= adddate(now(),interval 1 hour) 
					, UserPassword_Change=?, UserPassword_ChangeTime= adddate(now(),interval 1 hour) 
					where id_MUser=?;";
			$connection->execute_query_no_result($txt_query,
					array('ssss',$viewer['Имя'],$viewer['Email'],$new_password,$auth_info->id_MUser));

			$letter= self::PrepareNewViewerLetter($viewer['Имя'],$new_password);
			if (false==PostLetter($letter,$viewer['Email'],$viewer['Имя'],'смена email наблюдателя',$id_MUser))
				throw new Exception("can not post letter to {$viewer['Email']}!");
		}
	}

	function update($id,$viewer)
	{
		$this->check_Object();
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		try
		{
			global $auth_info;
			switch ($auth_info->category)
			{
				case 'manager': $this->update_as_manager($id,$viewer,$connection); break;
				case 'viewer': $this->update_as_viewer($id,$viewer,$connection); break;
			}
			$connection->commit();
			echo '{ "ok": true }';
		}
		catch (mysqli_sql_exception $ex)
		{
			$connection->rollback();
			ProcessMySqlException_check_duplicate_Viewer_login($ex, $viewer['Email']);
		}
		catch (Exception $ex)
		{
			$connection->rollback();
			throw $ex;
		}
	}

	function delete($id_ManagerUsers)
	{
		global $auth_info;
		CheckMandatoryGET('id_Manager');
		$id_Manager= $_GET['id_Manager'];
		CheckAccessToManagerIfManager($auth_info, $id_Manager);

		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		foreach ($id_ManagerUsers as $id_ManagerUser)
		{
			$txt_query= "delete from ManagerUser where id_Manager=? && id_ManagerUser=?;";
			$affected= $connection->execute_query_get_affected_rows($txt_query,array('ii',$id_Manager,$id_ManagerUser));
			if (1!=$affected)
			{
				$connection->rollback();
				exit_not_found("can not delete ManagerUser where id_Manager=$id_Manager && id_ManagerUser=$id_ManagerUser (affected $affected rows)");
			}
		}
		$connection->commit();
		echo '{ "ok": true }';
	}
}

$crud= new viewer_crud();

if($auth_info->category==="customer") {
	$crud= new viewer_crud_for_customer($crud);
}

$crud->process_cmd();