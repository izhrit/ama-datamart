<?

function exit_duplicate_viewer_login()
{
	echo '{ "ok": false, "reason":"В базе уже есть наблюдатель с таким адресом электронной почты!" }';
	exit;
}

function ProcessMySqlException_check_duplicate_Viewer_login($ex, $email)
{
	if ("Duplicate entry '$email' for key 'byUserEmail'" != $ex->getMessage())
	{
		throw $ex;
	}
	else
	{
		exit_duplicate_viewer_login();
	}
}

function exit_duplicate_request()
{
	echo '{ "ok": false, "reason":"Вы уже запрашивали доступ к этой процедуре" }';
	exit;
}
function exit_duplicate_procedure()
{
	echo '{ "ok": false, "reason":"У вас уже есть доступ к этой процедуре" }';
	exit;
}
