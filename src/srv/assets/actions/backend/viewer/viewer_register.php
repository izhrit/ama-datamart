<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/viewer/alib_viewer.php';

write_to_log($_POST);

$Заявитель= $_POST['Заявитель'];
$Запрос= $_POST['Запрос_на_доступ'];
$Должник= $Запрос['Должник'];

function insert_or_Find_Debtor($connection,$Должник)
{
	$txt_query= "select id_Debtor from Debtor where INN=?;";
	$rows= $connection->execute_query($txt_query,array('s',$Должник['ИНН']));
	if (0!=count($rows))
	{
		return $rows[0]->id_Debtor;
	}
	else
	{
		$txt_query= "insert into Debtor set INN=?, OGRN=?, SNILS=?, Name=?;";
		return $connection->execute_query_get_last_insert_id($txt_query,
			array('ssss',$Должник['ИНН'],$Должник['ОГРН'],$Должник['СНИЛС'],$Должник['Наименование']));
	}
}
function PreparePasswordLetter($user_name,$password)
{
	$letter= (object)array('subject'=>'Учётные данные на Витрине данных ПАУ');
	ob_start();
	include "../assets/actions/backend/viewer/letters/password.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();
	return $letter;
}
function PostLetterToViewer($letter_data,$user_name,$email,$EmailType_descr,$id_MUser)
{
	return PostLetter($letter_data,$email,$user_name,
					  $EmailType_descr,$id_MUser);
}
function find_debtor_for($params, $connection)
{
	$txt_query= "select id_Debtor 
	from Debtor where (INN=? and INN!='' and INN is not NULL) or (OGRN=? and OGRN!='' and OGRN is not NULL) or (SNILS=? and SNILS!='' and SNILS is not NULL)";
	$rows=$connection->execute_query($txt_query,array('sss',$params['ИНН'],$params['ОГРН'],$params['СНИЛС']));
	if(count($rows)!=1)
		return null;
	else return $rows[0]->id_Debtor;
}
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$connection= default_dbconnect();
$connection->begin_transaction();
try
{
	global $email_settings;
	$new_password= (true!=$email_settings->enabled) ? 'new_test_pass' : generate_password(8);

	$id_Debtor= find_debtor_for($Запрос['Должник'], $connection);
	if (null == $id_Debtor)
	{
		$BankruptId=$Запрос['Выбранная_процедура']['id'];
		$Должник= $Запрос['Должник'];
		$id_Debtor= $connection->execute_query_get_last_insert_id
			("insert into Debtor set Name=?, INN=?, SNILS=?, OGRN=?, BankruptId=?;",
			array('sssss',$Должник['Наименование'],$Должник['ИНН'],$Должник['СНИЛС'],$Должник['ОГРН'],$BankruptId));
	}

	$txt_query= "insert into MUser set CreatedTime= now(), UserEmail=?, UserName=?, UserPhone=?, UserPassword=?;";
	$id_MUser= $connection->execute_query_get_last_insert_id($txt_query,
						array('ssss',$Заявитель['Email'],$Заявитель['Имя'],$Заявитель['Телефон'],$new_password));

	//$id_Debtor= insert_or_Find_Debtor($connection,$Должник); !!! it isn't need here!!!

	$Body= array(
		'Процедура'=>array('КогоПредставляетЗаявитель'=>$Запрос['Процедура']['КогоПредставляетЗаявитель'])
		,'Пояснения'=>$Запрос['Пояснения']
	);

	$txt_query= "insert into Request set id_MUser=?, id_Debtor=?, managerName=?, State='a', TimeLastChange=now(), Body=compress(?);";
	$id_Request= $connection->execute_query_get_last_insert_id($txt_query,
				array('ssss',$id_MUser,$id_Debtor,$Запрос['Процедура']['Управляющий'],nice_json_encode($Body)));

	$letter= PreparePasswordLetter($Заявитель['Имя'],$new_password);
	if (false==PostLetterToViewer($letter,$Заявитель['Имя'],$Заявитель['Email'],'создание наблюдателя',$id_MUser))
	{
		$connection->rollback();
		exit_internal_server_error("Can not send email to ".$Заявитель['Email']);
	}
	else
	{
		$connection->commit();
		echo '{ "ok": true }';
	}
}
catch (mysqli_sql_exception $ex)
{
	$connection->rollback();
	ProcessMySqlException_check_duplicate_Viewer_login($ex, $Заявитель['Email']);
}
catch (Exception $ex)
{
	$connection->rollback();
	throw $ex;
}
