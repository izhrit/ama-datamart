<?php

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthViewer();
$id_MUser = $auth_info->id_MUser;

$fields= "
	le.LeakSum LeakSum 
	,DATE_FORMAT(le.LeakDate, '%d.%m.%Y') LeakDate
	,le.ContragentName ContragentName
	,le.isAccredited isAccredited
	,ProcedureType_SafeShortForDBValue(mp.procedure_type) `Procedure`
	,concat(m.lastName
		,if(m.firstName > '' , concat(' ',substr(m.firstName,1,1),'.'), '')
		,if(m.middleName > '', concat(' ',substr(m.middleName,1,1),'.'), '')) Manager
";
$from_where= "
		from Leak le 
		inner join PData pd on le.id_PData = pd.id_PData
		inner join MProcedure mp on pd.id_MProcedure = mp.id_MProcedure
		inner join Manager m on mp.id_Manager = m.id_Manager
		inner join MProcedureUser mpu on mp.id_MProcedure = mpu.id_MProcedure
		inner join ManagerUser mu on mpu.id_ManagerUser = mu.id_ManagerUser
		inner join MUser on mu.id_MUser = MUser.id_MUser
		where MUser.id_MUser = $id_MUser";


$filter_rule_builders= array(
	'LeakSum'=>function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		$sum=substr($data,1);
		if (strlen($sum) > 0)
		{
			$operator= substr($data,0,1);
			if (in_array($operator,array('>','<','=')))
				return " and le.LeakSum $operator $sum ";
		}
		return '';
	}
	,'LeakDate'=>function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		if (strlen($data) == 11)
		{
			$newdata=substr($data,-4). substr($data,-7,2). substr($data,-10,2);
			$operator= substr($data,0,1);
			if (in_array($operator,array('>','<','=')))
				return " and le.LeakDate $operator $newdata ";
		}
		return '';
	}
	,'isAccredited'=> 'std_filter_rule_builder'
	,'ContragentName'=> 'std_filter_rule_builder'
	,'Manager'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and m.lastName like '$data%' ";
	}
	,'Procedure'=>function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and mp.procedure_type= ProcedureType_SafeDBValueForShort('$data') ";
	}
);

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders,' le.id_Leak ');
?>