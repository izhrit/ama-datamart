<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthManager();
CheckMandatoryGET('q');
CheckMandatoryGET_id('id_Manager');
$id_Manager= intval($_GET['id_Manager']);
CheckAccessToManagerIfManager($auth_info, $id_Manager);

$txt_query= "
	select 
		  id_ManagerUser id
		, UserName text
	from ManagerUser 
	where id_Manager=? && instr(UserName,?) > 0
;";

$rows= execute_query($txt_query,array('is',$id_Manager,$_GET['q']));

echo nice_json_encode(array('results'=>$rows));
