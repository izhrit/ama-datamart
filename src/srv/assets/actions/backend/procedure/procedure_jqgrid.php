<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $fields, $from_where, $auth_info;
$auth_info= CheckAuthCustomerOrManagerOrViewer();

require_once '../assets/actions/backend/procedure/alib_procedures.php';
prepare_procedures_jquery_fields_from_where();

$filter_rule_builders= array(
	'debtorName'=> function($l,$rule)
	{
		global $auth_info;
		$data= mysqli_real_escape_string($l,$rule->data);
		$f= (isset($auth_info->id_MUser) || isset($auth_info->id_Manager)) ? 'debtorName' : 'd.Name';
		return " and ($f like '$data%')";
	}
	,'Manager'=> function($l,$rule)
	{
		global $auth_info;
		$data= mysqli_real_escape_string($l,$rule->data);
		$f= isset($auth_info->id_MUser) ? 'Manager' : 'm.lastName';
		return " and ($f like '$data%')";
	}
	,'procedure_type'=>function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and ProcedureType_SafeShortForDBValue(procedure_type) like '%$data%' ";
	}
);

$result= execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders,' publicDate desc, id_MProcedure, Manager');
if (isset($_GET['id_MUser']))
{
	$result['rows']= unic_procedures($result['rows']);
	$result['records']=count($result['rows']);
}
if (isset($_GET['id_Manager']))
{
	foreach ($result['rows'] as $row)
	{
		if (isset($row->Viewers) && null!=$row->Viewers)
			$row->Viewers= explode('|',$row->Viewers);
	}
}
echo nice_json_encode($result);

