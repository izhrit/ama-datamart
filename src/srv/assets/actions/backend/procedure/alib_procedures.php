<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

function prepare_procedures_jquery_fields_from_where_for_manager()
{
	global $fields, $from_where, $auth_info;
	$id_Manager= CheckMandatoryGET_id('id_Manager');
	CheckAuthManager();
	CheckAccessToManagerIfManager($auth_info, $id_Manager);
	$archive_conditions= '';
	$efrsb_archive_conditions= '';
	if (!isset($_GET['show_archive']) || 'false'==$_GET['show_archive'])
	{
		$archive_conditions= ' and 0=if(mp.ArchivedByUser is null,mp.Archive,mp.ArchivedByUser) ';
		$efrsb_archive_conditions= ' and 0=d.Archive ';
	}
	if (!isset($_GET['version']))
	{
		$auth_info_id_Contract= $auth_info->id_Contract;
		$from_where= "
			from MProcedure mp 
			inner join Debtor d on d.id_Debtor=mp.id_Debtor
			inner join Manager m on mp.id_manager=m.id_Manager 
			where mp.id_Manager=$id_Manager && m.id_Contract=$auth_info_id_Contract $archive_conditions";
		$fields.= ",(
			select group_concat(distinct mu.UserName order by mu.id_ManagerUser separator \"|\")
			from MProcedureUser mpu
			inner join ManagerUser mu on mpu.id_ManagerUser = mu.id_ManagerUser
			where mpu.id_MProcedure=mp.id_MProcedure
		) Viewers,
		if(mp.ArchivedByUser is null,mp.Archive,mp.ArchivedByUser) Archived";
	}
	else
	{
		$fields= ' * ';
		$id_Contract_query = "";
		if (isset($auth_info->id_Contract))
		{
			$auth_info_id_Contract= $auth_info->id_Contract;
			$id_Contract_query.= " && m.id_Contract=$auth_info_id_Contract";
		}

		$ArbitrManagerID= null==$auth_info->ArbitrManagerID ? '"no-manager"' : $auth_info->ArbitrManagerID;

		$from_where= " from
		(
			select
				null id_Debtor_Manager,
				mp.id_MProcedure,
				mp.id_Manager,
				mp.caseNumber,
				mp.procedure_type,
				d.INN debtorInn,
				d.OGRN debtorOgrn,
				d.SNILS debtorSnils,
				d.Name debtorName,
				mp.publicDate,
				short_fio(m.lastName,m.firstName,m.middleName) Manager,
				(select group_concat(distinct mu.UserName order by mu.id_ManagerUser separator \"|\")
					from MProcedureUser mpu
					inner join ManagerUser mu on mpu.id_ManagerUser = mu.id_ManagerUser
					where mpu.id_MProcedure=mp.id_MProcedure
				) Viewers,
				if(mp.ArchivedByUser is null,mp.Archive,mp.ArchivedByUser) Archived
				from MProcedure mp
				inner join Debtor d ON d.id_Debtor=mp.id_Debtor
				inner join Manager m ON mp.id_manager=m.id_Manager
				where mp.id_Manager=$id_Manager $id_Contract_query $archive_conditions

			UNION

			select
				edm.id_Debtor_Manager id_Debtor_Manager,
				NULL id_MProcedure,
				m.id_Manager,
				NULL caseNumber,
				NULL procedure_type,
				d.INN debtorInn,
				d.OGRN debtorOgrn,
				d.SNILS debtorSnils,
				d.Name debtorName,
				d.LastMessageDate publicDate,
				short_fio(m.LastName,m.FirstName,m.MiddleName) Manager,
				NULL Viewers,
				d.Archive Archived
			from efrsb_debtor_manager edm
			inner join efrsb_debtor d ON d.BankruptId=edm.BankruptId
			inner join efrsb_manager m ON edm.ArbitrManagerID=m.ArbitrManagerID
			where edm.ArbitrManagerID=$ArbitrManagerID $efrsb_archive_conditions
			AND not exists (select d2.INN  from MProcedure mp2  inner join Debtor d2 ON d2.id_Debtor=mp2.id_Debtor where d2.INN = d.INN)
			AND not exists (select d2.OGRN from MProcedure mp2  inner join Debtor d2 ON d2.id_Debtor=mp2.id_Debtor where d2.OGRN = d.OGRN)
			AND not exists (select d2.SNILS from MProcedure mp2 inner join Debtor d2 ON d2.id_Debtor=mp2.id_Debtor where d2.SNILS = d.SNILS)
		) tu
		where 1=1";
	}
}

function prepare_procedures_jquery_fields_from_where_for_customer()
{
	global $fields, $from_where, $auth_info;
	CheckMandatoryGET_id('id_Contract');
	$id_Contract= intval($_GET['id_Contract']);
	CheckCustomerAccessToContract($auth_info, $id_Contract);
	$auth_info_id_Contract= $auth_info->id_Contract;
	$from_where= "
		from MProcedure mp
		inner join Debtor d on d.id_Debtor=mp.id_Debtor
		inner join Manager m on mp.id_manager=m.id_Manager
		where m.id_Contract=$id_Contract && m.id_Contract=$auth_info_id_Contract
		 and 0=if(mp.ArchivedByUser is null,mp.Archive,mp.ArchivedByUser) ";
}

function prepare_procedures_jquery_fields_from_where_for_viewer()
{
	global $fields, $from_where, $auth_info;

	CheckMandatoryGET_id('id_MUser');
	CheckAuthViewer();
	$id_MUser= intval($_GET['id_MUser']);
	CheckAccessToViewerIfViewer($auth_info, $id_MUser);

	$fields= ' * ';

	$from_where= " from (
	select
		mp.id_MProcedure id_MProcedure
		,mp.id_Manager id_Manager
		,mp.caseNumber caseNumber
		,mp.procedure_type procedure_type
		,d.INN debtorInn
		,d.OGRN debtorOgrn
		,d.SNILS debtorSnils
		,mp.publicDate publicDate

		,d.Name debtorName
		,concat(m.lastName
			,if(m.firstName is null || 0=length(m.firstName),'',concat(' ',substr(m.firstName,1,1),'.'))
			,if(m.middleName is null || 0=length(m.middleName),'',concat(' ',substr(m.middleName,1,1),'.'))) Manager

		,null id_Request, null state

	from MProcedure mp
	inner join Debtor d on d.id_Debtor=mp.id_Debtor
	inner join Manager m on mp.id_manager=m.id_Manager
	inner join MProcedureUser mpu on mp.id_MProcedure=mpu.id_MProcedure
	inner join ManagerUser mu on mu.id_ManagerUser=mpu.id_ManagerUser
	where mu.id_MUser=$id_MUser
	union
	select
		 null id_MProcedure
		,null id_Manager
		,null caseNumber
		,null procedure_type
		,d.INN debtorInn
		,d.OGRN debtorOgrn
		,d.SNILS debtorSnils
		,null publicDate

		,d.Name debtorName, rq.managerName Manager

		,rq.id_Request id_Request, rq.State state
	from Request rq
	inner join Debtor d on rq.id_Debtor = d.id_Debtor
	where rq.id_MUser = $id_MUser

	) tu
	where 1=1";
}

function prepare_procedures_jquery_fields_from_where()
{
	global $fields, $from_where, $auth_info;
	$fields= "
		mp.id_MProcedure
		,mp.id_Manager
		,mp.caseNumber
		,mp.procedure_type
		,d.INN debtorInn
		,d.OGRN debtorOgrn
		,d.SNILS debtorSnils
		,d.Name debtorName
		,mp.publicDate
		,concat(m.lastName
			,if(m.firstName is null || 0=length(m.firstName),'',concat(' ',substr(m.firstName,1,1),'.'))
			,if(m.middleName is null || 0=length(m.middleName),'',concat(' ',substr(m.middleName,1,1),'.'))) Manager
	";
	if (isset($_GET['id_Manager']))
	{
		prepare_procedures_jquery_fields_from_where_for_manager();
	}
	else if (isset($_GET['id_Contract']))
	{
		prepare_procedures_jquery_fields_from_where_for_customer();
	}
	else if (isset($_GET['id_MUser']))
	{
		prepare_procedures_jquery_fields_from_where_for_viewer();
	}
	else
	{
		exit_bad_request("skipped _GET['id_Contract'], _GET['id_Manager'], _GET['id_MUser']");
	}
}

function unic_procedures($rows)
{
	$tmp_rows=$rows;
	$rows=array();
	for ($i=0;$i<count($tmp_rows);$i++)
	{
		if(!$tmp_rows[$i]->id_MProcedure)
		{
			for ($j=0;$j<count($tmp_rows);$j++)
			{
				if($i!=$j && (($tmp_rows[$j]->debtorInn && $tmp_rows[$i]->debtorInn==$tmp_rows[$j]->debtorInn)
				|| ($tmp_rows[$j]->debtorOgrn && $tmp_rows[$i]->debtorOgrn==$tmp_rows[$j]->debtorOgrn)
				|| ($tmp_rows[$j]->debtorSnils && $tmp_rows[$i]->debtorSnils==$tmp_rows[$j]->debtorSnils))
				)
				{
					unset($tmp_rows[$i]);
					break;
				}
			}
		}
	}
	foreach ($tmp_rows as $row)
		$rows[]=$row;
	return $rows;
}