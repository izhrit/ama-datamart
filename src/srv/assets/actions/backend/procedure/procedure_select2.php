<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';
require_once '../assets/actions/backend/constants/alib_constants.php';
require_once '../assets/actions/backend/constants/debtorName.etalon.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManagerOrViewer();
CheckMandatoryGET('q');

if (isset($_GET['id_Manager']))
{
	CheckAuthManager();
	$id_Manager= CheckMandatoryGET_id('id_Manager');
	CheckAccessToManagerIfManager($auth_info, $id_Manager);

	$arg_query= array('ss',$id_Manager,$_GET['q']);
	$txt_query= '
		select
				mp.id_MProcedure id
			, concat(d.Name,", ",ProcedureType_SafeShortForDBValue(mp.procedure_type)) text
		from MProcedure mp
		inner join Debtor d on d.id_Debtor=mp.id_Debtor
		inner join Manager m on m.id_Manager=mp.id_Manager
		where mp.id_Manager=? && instr(d.Name,?) > 0';
	if (isset($auth_info->id_Contract))
	{
		$txt_query.= " && m.id_Contract=? ";
		$arg_query[0].= 's';
		$arg_query[]= $auth_info->id_Contract;
	}

	if (isset($_GET['version']))
	{
		$txt_query.= ' union
			select
				  concat("e",edm.id_Debtor_Manager) id
				, d.Name text
			from efrsb_debtor_manager edm
			inner join efrsb_debtor d on d.BankruptId=edm.BankruptId
			inner join efrsb_manager m on edm.ArbitrManagerID=m.ArbitrManagerID
			where edm.ArbitrManagerID=? && instr(d.Name,?) > 0';
		$arg_query[0].= 'ss';
		$arg_query[]= $auth_info->ArbitrManagerID;
		$arg_query[]= $_GET['q'];
	}
	$rows= execute_query($txt_query,$arg_query);
}
else if (isset($_GET['id_Contract']))
{
	CheckMandatoryGET_id('id_Contract');
	CheckAuthCustomer();
	$id_Contract= $_GET['id_Contract'];
	CheckCustomerAccessToContract($auth_info, $id_Contract);
	$txt_query= "
		select
			  mp.id_MProcedure id
			, concat(d.Name,', ',ProcedureType_SafeShortForDBValue(mp.procedure_type)) text
		from MProcedure mp
		inner join Debtor d on d.id_Debtor=mp.id_Debtor
		inner join Manager m on m.id_Manager=mp.id_Manager
		where m.id_Contract=? && instr(d.Name,?) > 0
	;";
	$rows= execute_query($txt_query,array('ss',$id_Contract,$_GET['q']));
}
else if (isset($_GET['id_MUser']))
{
	require_once '../assets/actions/backend/procedure/alib_procedures.php';
	CheckMandatoryGET_id('id_MUser');
	CheckAuthViewer();
	$id_MUser= $_GET['id_MUser'];
	CheckAccessToViewerIfViewer($auth_info, $id_MUser);
	$query_prefix_approved="
		select
			mp.id_MProcedure id
			, concat(d.Name,', ',ProcedureType_SafeShortForDBValue(mp.procedure_type)) text
			,d.INN debtorInn
			,d.OGRN debtorOgrn
			,d.SNILS debtorSnils
			,mp.id_MProcedure id_MProcedure
		from MProcedure mp
		inner join Debtor d on d.id_Debtor=mp.id_Debtor
		inner join MProcedureUser mpu on mpu.id_MProcedure=mp.id_MProcedure
		inner join ManagerUser mu on mu.id_ManagerUser=mpu.id_ManagerUser";
	$query_prefix_unapproved="
		select
			concat('r',re.id_Request) id
			, d.Name text
			,d.INN debtorInn
			,d.OGRN debtorOgrn
			,d.SNILS debtorSnils
			,null id_MProcedure
		from Debtor d
		inner join Request re  on re.id_Debtor=d.id_Debtor
		inner join MUser mu on re.id_Muser=mu.id_Muser";
	$query_postfix="
	where mu.id_MUser=? && instr(d.Name,?) > 0";

	$txt_query=$query_prefix_approved.$query_postfix.' union '.$query_prefix_unapproved.$query_postfix.';';
	$rows= execute_query($txt_query,array('ssss',$id_MUser,$_GET['q'],$id_MUser,$_GET['q']));
	$rows= unic_procedures($rows);
}
else
{
	exit_bad_request("skipped _GET['id_Contract'], _GET['id_Manager'], _GET['id_MUser']");
}

foreach ($rows as $row)
	$row->text= beautify_debtorName($row->text);

echo nice_json_encode(array('results'=>$rows));
