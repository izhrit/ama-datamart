<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthManager();
$auth_info_id_Manager= $auth_info->id_Manager;

class Procedure_viewers_crud extends Base_crud
{
	function read($id_MProcedure)
	{
		global $auth_info_id_Manager;
		$txt_query= "
			select 
				  d.Name НаименованиеДолжника
				, procedure_type ТипПроцедуры
				, id_MProcedure 
			from MProcedure mup
			inner join Debtor d on d.id_Debtor=mup.id_Debtor 
			where id_MProcedure=? && id_Manager=?
		;";
		$rows= execute_query($txt_query,array('ss',$id_MProcedure,$auth_info_id_Manager));
		if (1!=count($rows))
		{
			exit_not_found("can not find procedure id_MProcedure=$id_MProcedure");
		}
		else
		{
			$procedure= $rows[0];
			$txt_query= "
				select 
					mu.id_ManagerUser id
					, mu.UserName text
				from MProcedureUser mpu
				inner join ManagerUser mu on mu.id_ManagerUser = mpu.id_ManagerUser
				where mpu.id_MProcedure = ?
			;";
			$rows= execute_query($txt_query,array('s',$id_MProcedure));
			$procedure->Доступ_у_наблюдателей= $rows;
			echo nice_json_encode($procedure);
		}
	}

	function update($id_MProcedure,$procedure)
	{
		global $auth_info;
		$this->check_Object_mandatory('id_Manager');
		$id_Manager= $procedure['id_Manager'];
		CheckAccessToManagerIfManager($auth_info, $id_Manager);

		$txt_query= "select * from MProcedure where id_MProcedure=? && id_Manager=?";
		$rows= execute_query($txt_query,array('ss',$id_MProcedure,$id_Manager));
		$count_rows= count($rows);
		if (1!=$count_rows)
			exit_not_found("can not find procedure where id_MProcedure=$id_MProcedure && id_Manager=$id_Manager");

		$connection= default_dbconnect();

		$connection->begin_transaction();
		$txt_query= "
			delete mpu 
			from MProcedureUser mpu 
			inner join MProcedure mp on mp.id_MProcedure=mpu.id_MProcedure
			where mpu.id_MProcedure=? && mp.id_Manager=?;";
		$affected= $connection->execute_query_get_affected_rows($txt_query,array('ii',$id_MProcedure,$id_Manager));

		if (isset($procedure['Доступ_у_наблюдателей']))
		{
			$viewers= $procedure['Доступ_у_наблюдателей'];
			$txt_query= "
				insert into MProcedureUser 
					(id_ManagerUser,    id_MProcedure)
				select 
					 mu.id_ManagerUser, ?
				from ManagerUser mu
				where mu.id_Manager=? && mu.id_ManagerUser=?;";
			foreach ($viewers as $viewer)
			{
				$id_ManagerUser= $viewer['id'];
				$affected= $connection->execute_query_get_affected_rows($txt_query,array('iii',$id_MProcedure,$id_Manager,$id_ManagerUser));
				if (1!=$affected)
				{
					$connection->rollback();
					exit_not_found("can not insert MProcedureUser where id_Manager=$id_Manager && id_ManagerUser=$id_ManagerUser (affected $affected rows)");
				}
			}
		}

		$connection->commit();

		echo '{ "ok": true }';
	}
}

$crud= new Procedure_viewers_crud();
$crud->process_cmd();
