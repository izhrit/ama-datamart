<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthManager();
$auth_info_id_Manager= $auth_info->id_Manager;

class Procedure_crud extends Base_crud
{
	function read($id_MProcedure)
	{
		global $auth_info_id_Manager;
		$txt_query= "select 
				  d.Name debtorName
				, d.INN debtorInn
				, d.OGRN debtorOgrn
				, d.SNILS debtorSnils
				, caseNumber 
				, procedure_type 
				, id_MProcedure 
				, uncompress(ExtraParams) ExtraParams
				, Archive
				, ArchivedByUser
			from MProcedure mp
			inner join Debtor d on d.id_Debtor=mp.id_Debtor 
			where id_MProcedure=? && id_Manager=?;";
		$rows= execute_query($txt_query,array('ss',$id_MProcedure,$auth_info_id_Manager));
		if (1!=count($rows))
		{
			exit_not_found("can not find procedure id_MProcedure=$id_MProcedure");
		}
		else
		{
			$row_Procedure= $rows[0];
			$procedure= (object)array(
				'Должник'=>(object)array(
					'Наименование'=> $row_Procedure->debtorName
					, 'ИНН'=> $row_Procedure->debtorInn
					, 'ОГРН'=> $row_Procedure->debtorOgrn
					, 'СНИЛС'=> $row_Procedure->debtorSnils
				)
				, 'НомерДела'=> $row_Procedure->caseNumber
				, 'ТипПроцедуры'=> $row_Procedure->procedure_type
				, 'id_MProcedure'=> $row_Procedure->id_MProcedure
				, 'В_архив'=> ((null===$row_Procedure->ArchivedByUser) ? (1==$row_Procedure->Archive) : (1==$row_Procedure->ArchivedByUser))
			);
			if (null!=$row_Procedure->ExtraParams)
			{
				$extra_params=json_decode($row_Procedure->ExtraParams);
				if (isset($extra_params->Адрес))
					$procedure->Должник->Адрес= $extra_params->Адрес;
			}
			echo nice_json_encode($procedure);
		}
	}

	function update($id_MProcedure,$procedure)
	{
		global $auth_info;

		$this->check_Object_mandatory('id_Manager');
		$id_Manager= $procedure['id_Manager'];
		CheckAccessToManagerIfManager($auth_info, $id_Manager);

		global $auth_info_id_Manager;

		$new_ArchivedByUser= (isset($procedure['В_архив']) && 'false'!=$procedure['В_архив']) ? 1 : 0;
		$params= null;
		$txt_query= 'update MProcedure set ArchivedByUser=if((ArchivedByUser is not null || ?<>Archive),?,ArchivedByUser)';

		if (!isset($procedure['Должник']['Адрес']))
		{
			$params= array('isss',$new_ArchivedByUser,$new_ArchivedByUser,$id_MProcedure,$auth_info_id_Manager);
		}
		else
		{
			$address= $procedure['Должник']['Адрес'];
			$txt_ExtraParams= nice_json_encode(array('Адрес'=>$address));
			$txt_query.= ', ExtraParams=compress(?)';
			$params= array('iisss',$new_ArchivedByUser,$new_ArchivedByUser,$txt_ExtraParams,$id_MProcedure,$auth_info_id_Manager);
		}
		$txt_query.= ' where id_MProcedure=? && id_Manager=?;';
		execute_query_no_result($txt_query,$params);

		echo '{ "ok": true }';
	}
}

$crud= new Procedure_crud();
$crud->process_cmd();
