<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/actions/backend/constants/alib_constants.php';
require_once '../assets/actions/backend/constants/debtorName.etalon.php';

require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthViewer();
$id_MUser = $auth_info->id_MUser;
$inn= isset($_GET['INN']) ? $_GET['INN'] : '';

$txt_query= "
	select 
		mp.id_MProcedure id
		, concat(d.Name,', ',ProcedureType_SafeShortForDBValue(mp.procedure_type)) text
	from MProcedure mp
		inner join Debtor d on d.id_Debtor=mp.id_Debtor 
		inner join MProcedureUser mpu on mpu.id_MProcedure=mp.id_MProcedure
		inner join ManagerUser mu on mu.id_ManagerUser=mpu.id_ManagerUser
		where mu.id_MUser=? && d.INN=?
	order by mp.publicDate desc
;";

$rows= execute_query($txt_query,array('is',$id_MUser,$inn));
$data= array();
if (0!=count($rows))
{
	$data= $rows[0];
	$data->text= beautify_debtorName($data->text);
}

echo nice_json_encode($data);
