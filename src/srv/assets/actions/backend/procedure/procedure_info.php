<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/zip.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManagerOrViewer();

$rows= null;
if (isset($_GET['id_MProcedure']))
{
	CheckMandatoryGET_id('id_MProcedure');
	$id_MProcedure= $_GET['id_MProcedure'];
}
else if(isset($_GET['inn'])&&'viewer'==$auth_info->category)
{
	CheckMandatoryGET_id('inn');
	$inn= $_GET['inn'];
}
else exit_bad_request("skipped _GET['id_MProcedure'] and _GET['inn']");


if ('manager'==$auth_info->category)
{
	$txt_query= "select 
		d.fileData 
	from PData d 
	inner join MProcedure p on d.id_MProcedure=p.id_MProcedure 
	where d.id_MProcedure=? && p.id_Manager=? 
	order by d.revision desc 
	limit 1;";
	$rows= execute_query($txt_query,array('ii',$id_MProcedure,$auth_info->id_Manager));
}
else if ('customer'==$auth_info->category)
{
	$txt_query= "select 
		d.fileData 
	from PData d 
	inner join MProcedure p on d.id_MProcedure=p.id_MProcedure 
	inner join Manager m on m.id_Manager=p.id_Manager
	where d.id_MProcedure=? && m.id_Contract=? 
	order by d.revision desc 
	limit 1;";
	$rows= execute_query($txt_query,array('ii',$id_MProcedure,$auth_info->id_Contract));
}
else if ('viewer'==$auth_info->category)
{
	if(isset($id_MProcedure))
	{
		$txt_query= "select 
			d.fileData 
		from PData d 
		inner join MProcedure p on d.id_MProcedure=p.id_MProcedure 
		inner join MProcedureUser mpu on mpu.id_MProcedure=p.id_MProcedure
		inner join ManagerUser mu on mu.id_ManagerUser=mpu.id_ManagerUser
		where d.id_MProcedure=? && mu.id_MUser=? 
		order by d.revision desc 
		limit 1;";
		$rows= execute_query($txt_query,array('ii',$id_MProcedure,$auth_info->id_MUser));
		if(count($rows)==0)
		{
			$txt_query="select 
				mp.id_MProcedure id
			from MProcedure mp
			inner join Debtor d on d.id_Debtor=mp.id_Debtor
			inner join Request re  on re.id_Debtor=d.id_Debtor
			inner join MUser mu on re.id_Muser=mu.id_Muser
			where mp.id_MProcedure=? && mu.id_MUser=?
			limit 1 
			;";
			$ans= execute_query($txt_query,array('ii',$id_MProcedure,$auth_info->id_MUser));
		}
	}
	else if(isset($inn))
	{
		$txt_query= "select 
			d.fileData 
		from PData d 
		inner join MProcedure p on d.id_MProcedure=p.id_MProcedure
		inner join Debtor de on de.id_Debtor=p.id_Debtor 
		inner join MProcedureUser mpu on mpu.id_MProcedure=p.id_MProcedure
		inner join ManagerUser mu on mu.id_ManagerUser=mpu.id_ManagerUser
		where de.INN=? && mu.id_MUser=? 
		order by d.revision desc 
		limit 1;";
		$rows= execute_query($txt_query,array('ii',$inn,$auth_info->id_MUser));
	}
}
else
{
	$auth_info_category= $auth_info->category;
	exit_internal_server_error("unknown auth_info->category=\"$auth_info_category\"");
}

if (null==$rows || 1!=count($rows))
{
	if (isset($ans) && count($ans)==1 )
	{
		echo 'not available';
	}
	else
	{
		if (isset($id_MProcedure))
		{
			exit_not_found("can not find procedure id_MProcedure=$id_MProcedure");
		}
		else
		{
			exit_not_found("can not find debtor inn=$inn");
		}
	}
}
else
{
	global $filename_in_zip;
	if (!isset($_GET['section']))
	{
		$filename_in_zip= array('registry.xml','report.xml');
	}
	else
	{
		require_once '../assets/actions/backend/constants/SectionName.etalon.php';
		$section_arg= $_GET['section'];
		if ('*'!=$section_arg)
		{
			$section_arg_parts= explode(",", $section_arg);
			$new_filename_in_zip= array();
			foreach ($filename_in_zip as $filename)
			{
				if (in_array($filename,$section_arg_parts))
					$new_filename_in_zip[]= $filename;
			}
			$filename_in_zip= $new_filename_in_zip;
		}
	}

	$xml_content_array = array();
	for ($i= 0; $i<count($filename_in_zip); $i++)
		$xml_content_array[]= '';

	$tf= tmpfile();
	try
	{
		fwrite($tf,$rows[0]->fileData);
		fflush($tf);

		$meta_data= stream_get_meta_data($tf);
		$filepath= $meta_data['uri'];

		$zip = zip_open($filepath);
		while ($zip_entry = zip_read($zip))
		{
			$zip_entry_name= zip_entry_name($zip_entry);
			for ($i= 0; $i<count($filename_in_zip); $i++)
			{
				if ($filename_in_zip[$i]==$zip_entry_name)
					$xml_content_array[$i]= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
			}
		}
		zip_close($zip);
		fclose($tf); // Этот файл автоматически удаляется после закрытия
	}
	catch (Exception $ex)
	{
		fclose($tf); // Этот файл автоматически удаляется после закрытия
		throw $ex;
	}

	if (!isset($_GET['format']) || 'json'!=isset($_GET['format']))
	{
		$result_text= implode("<!--/-->", $xml_content_array);
		echo $result_text;
	}
	else
	{
		require_once '../assets/helpers/json.php';
		$result= (object)array();
		for ($i= 0; $i<count($filename_in_zip); $i++)
		{
			$xml_content= $xml_content_array[$i];
			if (''!=$xml_content)
			{
				switch ($filename_in_zip[$i])
				{
					case 'registry.xml':
					{
						require_once '../assets/libs/codecs/registry.codec.php';
						$registry_codec= new Registry_codec();
						$result->Требования_кредиторов= $registry_codec->Decode($xml_content);
					}
					break;
					case 'report.xml':
					{
						require_once '../assets/libs/codecs/report.codec.php';
						$report_codec= new Report_codec();
						$result->Отчёт= $report_codec->Decode($xml_content);
					}
					break;
					case 'km.xml':
					{
						require_once '../assets/libs/codecs/km.codec.php';
						$km_codec= new KM_codec();
						$result->Конкурсная_масса= $km_codec->Decode($xml_content);
					}
					break;
					case 'current_claims.xml':
					{
						require_once '../assets/libs/codecs/current_claims.codec.php';
						$current_claims_codec= new Current_claims_codec();
						$result->Текущие_требования= $current_claims_codec->Decode($xml_content);
					}
					break;
				}
			}
		}
		echo nice_json_encode($result);
	}
}

