<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

global $auth_info;
$auth_info= CheckAuthCustomerOrManagerOrViewer();

$rows= null;
CheckMandatoryGET_id('id_MProcedure');
$id_MProcedure= $_GET['id_MProcedure'];

if ('manager'==$auth_info->category)
{
	$txt_query= "select 

	  m.firstName m_firstName
	, m.lastName m_lastName
	, m.middleName m_middleName
	, m.efrsbNumber m_efrsbNumber
	, m.INN m_INN

	, d.Name p_debtorName
	, d.INN p_debtorInn
	, d.OGRN p_debtorOgrn
	, d.SNILS p_debtorSnils

	, mp.procedure_type p_procedure_type
	, mp.caseNumber p_caseNumber

	, mp.publicDate p_publicDate
	, if (mp.revision=mp.ctb_revision,1,0) p_ctb_allowed

	from MProcedure mp
	inner join Debtor d on d.id_Debtor=mp.id_Debtor 
	inner join Manager m on m.id_Manager=mp.id_Manager
	where mp.id_MProcedure=? && m.id_Manager=?
	;";
	$rows= execute_query($txt_query,array('ii',$id_MProcedure,$auth_info->id_Manager));
}
else if ('customer'==$auth_info->category)
{
	$txt_query= "select 

	  m.firstName m_firstName
	, m.lastName m_lastName
	, m.middleName m_middleName
	, m.efrsbNumber m_efrsbNumber
	, m.INN m_INN

	, d.Name p_debtorName
	, d.INN p_debtorInn
	, d.OGRN p_debtorOgrn
	, d.SNILS p_debtorSnils

	, mp.procedure_type p_procedure_type
	, mp.caseNumber p_caseNumber

	, mp.publicDate p_publicDate
	, if (mp.revision=mp.ctb_revision,1,0) p_ctb_allowed

	from MProcedure mp
	inner join Debtor d on d.id_Debtor=mp.id_Debtor 
	inner join Manager m on m.id_Manager=mp.id_Manager
	where mp.id_MProcedure=? && m.id_Contract=? 
	;";
	$rows= execute_query($txt_query,array('ii',$id_MProcedure,$auth_info->id_Contract));
}
else if ('viewer'==$auth_info->category)
{
	$txt_query= "select 

	  m.firstName m_firstName
	, m.lastName m_lastName
	, m.middleName m_middleName
	, m.efrsbNumber m_efrsbNumber
	, m.INN m_INN

	, d.Name p_debtorName
	, d.INN p_debtorInn
	, d.OGRN p_debtorOgrn
	, d.SNILS p_debtorSnils

	, mp.procedure_type p_procedure_type
	, mp.caseNumber p_caseNumber

	, mp.publicDate p_publicDate
	, if (mp.revision=mp.ctb_revision,1,0) p_ctb_allowed

	from MProcedure mp
	inner join Debtor d on d.id_Debtor=mp.id_Debtor 
	inner join Manager m on m.id_Manager=mp.id_Manager
	inner join MProcedureUser mpu on mpu.id_MProcedure=mp.id_MProcedure
	inner join ManagerUser mu on mu.id_ManagerUser=mpu.id_ManagerUser
	where mp.id_MProcedure=? && mu.id_MUser=? 
	;";
	$rows= execute_query($txt_query,array('ii',$id_MProcedure,$auth_info->id_MUser));
}
else
{
	$auth_info_category= $auth_info->category;
	exit_internal_server_error("unknown auth_info->category=\"$auth_info_category\"");
}

if (null!=$rows && 1==count($rows))
{
	echo nice_json_encode($rows[0]);
}
else
{
	exit_not_found("can not find procedure id_MProcedure=$id_MProcedure");
}

