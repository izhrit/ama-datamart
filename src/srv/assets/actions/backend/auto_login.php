<?php
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/db.php';
require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/realip.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';

function auto_login_fail($msg,$auth_info= null,$token_info= null)
{
	require_once '../assets/libs/log/access_log.php';
	global $autologin_error_text;
	$autologin_error_text= $msg;
	$txt= "autologin fail: $msg";
	if (null!=$auth_info)
	{
		$txt.= "\r\nauth information:\r\n";
		$txt.= print_r($auth_info,true);
	}
	if (null!=$token_info)
	{
		$txt.= "\r\ntoken information:\r\n";
		$txt.= print_r($token_info,true);
	}
	write_to_access_log_details('login/ama/error',$txt);
	throw new Exception($txt);
}

function auto_login_ok($ainfo,$category)
{
	global $_SESSION, $auth_info;
	if (!isset($_SESSION))
		session_start();
	$auth_info= $ainfo;
	$auth_info->category= $category;
	$_SESSION['auth_info']= $auth_info;

	require_once '../assets/libs/log/access_log.php';
	switch ($category)
	{
		case 'manager': write_to_access_log_id('login/manager/ama',$auth_info->id_Manager); break;
		case 'customer': write_to_access_log_id('login/customer/ama',$auth_info->id_Contract); break;
		case 'viewer': write_to_access_log_id('login/viewer/fa',$ainfo->id_MUser); break;
	}
}

function autologin_from_ama($auth)
{
	require_once '../assets/libs/auth/license.php';

	global $autologin_auth_options;
	$auth_args= decrypt_auth($auth, $autologin_auth_options);
	if (null==$auth_args)
		auto_login_fail('can not decrypt auth');

	Проверить_актуальность_авторизационных_данных_для_входа($auth_args);
	require_once '../assets/libs/auth/license-ama.php';
	Авторизоваться_по_информации_токена_короткоживущей_лицензиии($auth_args);
}

function autologin_from_fa($fauth)
{
	require_once '../assets/libs/auth/license.php';

	global $autologin_fauth_options;
	$auth_args= decrypt_auth($fauth, $autologin_fauth_options);
	if (null==$auth_args)
		auto_login_fail('can not decrypt fauth');
		
	if (!isset($auth_args->email))
		auto_login_fail('undefined email in fauth!',$auth_args);

	Проверить_актуальность_авторизационных_данных_для_входа($auth_args);
	require_once '../assets/libs/auth/license-fa.php';
	Авторизоваться_по_информации_токена_короткоживущей_лицензиии($auth_args);
}

try
{
	if (isset($_GET['auth']))
	{
		write_to_log('----------- auto_login ---------------');
		autologin_from_ama($_GET['auth']);
	}
	else
	{
		write_to_log('----------- auto_login from fa ---------------');
		autologin_from_fa($_GET['fauth']);
	}
}
catch (Exception $ex) // При любом исключении, записываем его в лог и открываем обычную страницу с логином паролем
{
	global $_SESSION, $auth_info;
	if (isset($_SESSION))
		unset($_SESSION['auth_info']);
	$auth_info= null;
	write_to_log('Unhandled exception during autologin occurred: ' . get_class($ex) . ' - ' . $ex->getMessage());
}