<?
require_once '../assets/helpers/log.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthAdmin();

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/custom_query.php';

global $custom_query_directory_path;
load_and_execute_custom_query($custom_query_directory_path);
