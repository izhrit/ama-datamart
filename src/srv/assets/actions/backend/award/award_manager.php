<?
require_once  __DIR__.'/../../../config.php';
require_once __DIR__.'/../../../helpers/log.php';
require_once __DIR__.'/../../../helpers/json.php';
require_once __DIR__.'/../../../helpers/db.php';
require_once __DIR__.'/../../../helpers/validate.php';

$inn= !isset($_GET['inn']) ? '' : $_GET['inn'];

if (''==$inn)
	exit_bad_request("unknown inn");

$txt_query= "
	select
		v.inn inn
		, v.VoteTime VoteTime
		, if(v.BulletinSignature is null,0,1) Signed
	from AwardVote v
	where v.inn=?
	;
";

$rows= execute_query($txt_query,array('s',$inn));
if (0==count($rows))
{
	echo nice_json_encode(null);
}
else
{
	echo nice_json_encode($rows[0]);
}
