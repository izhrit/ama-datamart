<?
require_once  __DIR__.'/../../../helpers/db.php';
require_once  __DIR__.'/../../../config.php';
require_once __DIR__.'/../../../helpers/log.php';
require_once __DIR__.'/../../../helpers/json.php';
require_once __DIR__.'/../../../helpers/validate.php';

$txt_query= "select 
	firstName Имя
	,lastName Фамилия
	,middleName Отчество
	,SRO СРО
	,inn ИНН
	,URL URL
	,id_AwardNominee id_AwardNominee
from AwardNominee order by lastName
";
$rows= execute_query($txt_query,array());
echo nice_json_encode($rows);
