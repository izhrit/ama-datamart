<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/time.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/award/award_managers_server.php';

session_start();

$Голосующий= (object)$_POST['Голосующий'];

$ПроголосовалЗа_ИНН= $_POST['ПроголосовалЗа']['ИНН'];

function PrepareAwardVoteLetter($ФИО_голосующего,$Бюллетень)
{
	$letter= (object)array('subject'=>'Голосование "Антикризисный управляющий 2021 года"');
	ob_start();
	include "../assets/actions/backend/award/letters/vote.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();

	$letter->attachments= array(array('filename'=>'Бюллетень.txt','content'=>$Бюллетень));

	return $letter;
}

function PrepareAwardVoteLetterWIthoutBulletin($ФИО_голосующего,$ФИО_Проголосовал_За)
{
	$letter= (object)array('subject'=>'Голосование "Антикризисный управляющий 2021 года"');
	ob_start();
	include "../assets/actions/backend/award/letters/vote2.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();

	return $letter;
}

function Учесть_голос_из_ПАУ($Голосующий,$ПроголосовалЗа_ИНН)
{
	$Голосующий_ИНН= $Голосующий->ИНН;
	$SRO= !isset($Голосующий->СРО) ? '' : $Голосующий->СРО;

	$license_id= !isset($_POST["license_id"]) ? null : $_POST["license_id"];
	// todo что то сделать, если токен лицензии не указан..

	$email = GetEmailByLecenseId($license_id);
	// todo что то сделать, если email не возвращается (токен не верный)..

	$nominee_rows = execute_query("SELECT lastName, firstName, middleName, inn, SRO FROM AwardNominee WHERE inn=?", array('s', $ПроголосовалЗа_ИНН));
	// todo что то сделать, если номинантов несколько или ни одного..

	$votes_rows = execute_query("SELECT id_AwardVote FROM AwardVote WHERE inn=?",array('s',$Голосующий_ИНН));
	// todo что то сделать, если голосов более одного..

	$b= !isset($_POST['Бюллетень']) ? null : $_POST['Бюллетень'];

	$current_date_sql= date_format(safe_date_create(),'Y-m-d\TH:i:s');
	if (!empty($votes_rows))
	{
		$id_AwardVote= $votes_rows[0]->id_AwardVote;
		$txt_query= "
			update AwardVote set 
				  VoteTime= ?
				, id_AwardNominee= (select id_AwardNominee from AwardNominee where inn=?)
				, BulletinText= ?
			where inn=?;";
		execute_query_no_result($txt_query,array('ssss',$current_date_sql,$ПроголосовалЗа_ИНН,$b,$Голосующий_ИНН));
	}
	else
	{
		$txt_query= "
			insert into AwardVote 
				  (inn,VoteTime,id_AwardNominee,lastName,firstName,middleName,SRO,BulletinText,email)
			select ?,  ?,       id_AwardNominee,?,       ?,        ?,         ?,  ?,           ?
			from AwardNominee
			where inn=?
		;";
		$id_AwardVote= execute_query_get_last_insert_id($txt_query,array('sssssssss'
			,$Голосующий_ИНН,$current_date_sql,$Голосующий->Фамилия,$Голосующий->Имя,$Голосующий->Отчество,$SRO,$b,$email
			,$ПроголосовалЗа_ИНН));
	}

	$Голосующий->email= $email;
	$ФИО_голосующего= "$Голосующий->Фамилия $Голосующий->Имя $Голосующий->Отчество";

	$nominee= $nominee_rows[0];
	$ПроголосоваЗа = "$nominee->lastName $nominee->firstName $nominee->middleName (ИНН:$nominee->inn, $nominee->SRO)";
	$letter        = PrepareAwardVoteLetterWIthoutBulletin($ФИО_голосующего, $ПроголосоваЗа);
	$row_SentEmail = PostLetter($letter, $Голосующий->email, $ФИО_голосующего, 'голос за премию АУ', $Голосующий_ИНН);
}

function Учесть_голос_подписью_ЭП($Голосующий,$ПроголосовалЗа_ИНН)
{
	/*
	todo:
	1. проверить что подпись соответствует бюллетеню (криптосервисом) и получить параметры подписавшего
	2. проверить и выяснить что такой АУ есть (обращением на ефрсб)
	*/
	$Голосующий_ИНН= $Голосующий->ИНН;
	$SRO= !isset($Голосующий->СРО) ? '' : $Голосующий->СРО;
	$current_date_sql= date_format(safe_date_create(),'Y-m-d\TH:i:s');
	execute_query_no_result("delete from AwardVote where inn= ? and BulletinSignature is null;",array('s',$Голосующий_ИНН));
	$txt_query= "
		insert into AwardVote
		      (inn,VoteTime,id_AwardNominee,BulletinText,BulletinSignature,lastName,firstName,middleName,SRO,email)
		select ?,  ?,       id_AwardNominee,?,           ?,                ?,       ?,        ?,         ?,  ?
		from AwardNominee
		where inn=?
	;";
	$id_AwardVote= execute_query_get_last_insert_id($txt_query,array('ssssssssss',
		$Голосующий_ИНН,$current_date_sql,$_POST['Бюллетень'],$_POST['cms']
		,$Голосующий->Фамилия,$Голосующий->Имя,$Голосующий->Отчество,$SRO,$Голосующий->email
		,$ПроголосовалЗа_ИНН));

	$ФИО_голосующего= $Голосующий->Фамилия.' '.$Голосующий->Имя.' '.$Голосующий->Отчество;
	$letter= PrepareAwardVoteLetter($ФИО_голосующего,$_POST['Бюллетень']);
	$row_SentEmail= PostLetter($letter,$Голосующий->email,$ФИО_голосующего,'голос за премию АУ',$id_AwardVote);
}

if (!isset($_POST['cms'])) // голосование из ПАУ..
{
	Учесть_голос_из_ПАУ($Голосующий,$ПроголосовалЗа_ИНН);
}
else
{
	Учесть_голос_подписью_ЭП($Голосующий,$ПроголосовалЗа_ИНН);
}

echo '{ "ok": true }';
