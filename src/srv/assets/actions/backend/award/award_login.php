<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/alib_emails.php';
require_once '../assets/actions/backend/award/award_managers_server.php';

$login= $_POST['login'];
$password= $_POST['password'];
$logged_user= null;

global $award_admin_users;
foreach ($award_admin_users as $u)
{
	if ($login==$u->login && $password==$u->password)
	{
		$logged_user= $u;
		break;
	}
}

if (null==$logged_user)
{
	echo '{ "ok": false }';
}
else
{
	echo '{ "ok": true }';
}

