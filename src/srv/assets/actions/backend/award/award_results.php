<?
require_once  __DIR__.'/../../../helpers/db.php';
require_once  __DIR__.'/../../../config.php';
require_once __DIR__.'/../../../helpers/log.php';
require_once __DIR__.'/../../../helpers/json.php';
require_once __DIR__.'/../../../helpers/validate.php';

$txt_query= "select 
	 concat(n.lastName, ' ', left(n.firstName,1), '. ', left(n.middleName,1), '.') Номинант
	,n.SRO СРО
	,count(v.id_AwardVote) Голосов
from AwardNominee n
left join AwardVote v on n.id_AwardNominee=v.id_AwardNominee
group by Номинант, СРО
order by Номинант
";
$rows= execute_query($txt_query,array());
echo nice_json_encode($rows);
