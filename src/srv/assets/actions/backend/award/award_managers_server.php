<?php
require_once __DIR__ . '/../../../helpers/log.php';
require_once __DIR__ . '/../../../helpers/db.php';
require_once __DIR__ . '/../../../config.php';
require_once __DIR__ . '/../../../helpers/json.php';
require_once __DIR__ . '/../../../helpers/realip.php';

if (isset($_GET["operation"]))
{
	switch ($_GET["operation"])
	{
		case "checkmanagerinserver": {
			ExistManagerInServerLicense($_GET["managerName"],$_GET["efrsbNumber"],$_GET["inn"]);
		} break;
		case "contractAuth": {
			ContractAuth($_GET["login"],$_GET["password"],$_GET["efrsbNumber"],$_GET["inn"]);
		} break;
	}
}


function GetManagers($license_id, $reg_num)
{
	if (is_null($license_id))
	{
		return null;
	}
	else
	{
		if (is_null($reg_num))
		{
			return GetManagersByLicenseId($license_id);
		}
		else
		{
			return GetManagerByLicenseIdAndRegnum($license_id, $reg_num);
		}
	}
}

function GetManagersByLicenseId($license_id)
{
	global $use_server_license_url;
	$managers_response = file_get_contents($use_server_license_url . "?action=getManagersByLicenseId&license_id=" . $license_id . "&ip=".getRealIPAddr());

	$managers = FillAlreadyVoted(json_decode($managers_response, true));

	return $managers;
}

function FillAlreadyVoted($managers)
{
	$txt_query= "
		SELECT 
			AN.* 
		FROM AwardVote AV 
		INNER JOIN AwardNominee AN on AV.id_AwardNominee = AN.id_AwardNominee 
		WHERE AV.inn =?
	;";
	for ($i = 0, $j = count($managers); $i < $j; $i++)
	{
		$awardvote = execute_query($txt_query, array('s', $managers[$i]["inn"]));
		if (!empty($awardvote))
		{
			$managers[$i]["voted_for"] = array(
				"last_name"   => $awardvote[0]->lastName,
				"first_name"  => $awardvote[0]->firstName,
				"middle_name" => $awardvote[0]->middleName,
				"inn" => $awardvote[0]->inn,
				"sro"         => $awardvote[0]->SRO
			);
		}
	}
	return $managers;
}

function GetManagerByLicenseIdAndRegnum($license_id, $regnum)
{
	global $use_server_license_url ;

	$managers_response = file_get_contents($use_server_license_url
			. "?action=getManagerByLicenseIdAndRegnum&license_id="
			. $license_id . "&regnum=" . $regnum . "&ip=".getRealIPAddr());

	$managers = FillAlreadyVoted(json_decode($managers_response, true));

	return $managers;
}

function GetEmailByLecenseId($license_id)
{
	global $use_server_license_url;
	$url= "$use_server_license_url?action=getEmailByLicenseId&license_id=$license_id";
	write_to_log("request $url");
	$email_response = file_get_contents($url);
	write_to_log("got:");
	write_to_log($email_response);
	return $email_response;
}

function ExistManagerInServerLicense($managerName, $efrsbNumber, $inn)
{
	global $use_server_license_url;
	$url = $use_server_license_url."?action=checkManagerInServer";
	$data = array('managerName' => $managerName, 'efrsbNumber' => $efrsbNumber);

	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
//	if ($result === FALSE) {}
	echo $result;
}

function ContractAuth($login, $password, $efrsbNumber, $inn)
{
	global $use_server_license_url;
	$url = $use_server_license_url."?action=contractAuth";
	$data = array('login' => $login, 'password' => $password, 'efrsbNumber' => $efrsbNumber);

	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
//	if ($result === FALSE) {}
	echo $result;
}