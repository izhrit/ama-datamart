<?php

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$fields= "
	v.id_AwardVote id_AwardVote
	,concat(v.lastName, ' ', left(v.firstName,1), '. ', left(v.middleName,1), '.') АУ
	,v.SRO СРО
	,DATE_FORMAT(v.VoteTime, '%d.%m %H:%i') VoteTime
	,concat(n.lastName, ' ', left(n.firstName,1), '. ', left(n.middleName,1), '.') За
";
$from_where= "
		from AwardVote v 
		inner join AwardNominee n on v.id_AwardNominee = n.id_AwardNominee";

$filter_rule_builders= array(
	 'АУ'=> function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and v.lastName like '$data%' ";
	}
	,'СРО'=> function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and v.SRO like '$data%' ";
	}
	,'VoteTime'=> 'std_filter_rule_builder'
	,'За'=> function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and n.lastName like '$data%' ";
	}
);

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders,' v.VoteTime desc');
