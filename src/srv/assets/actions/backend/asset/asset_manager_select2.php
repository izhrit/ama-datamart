<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';

$txt_query= "
	select
		  id_Manager id
		, concat(lastName,' '
			,left(firstName,1),'.',left(middleName,1), '.'
			,if(INN is null,'',concat(' (ИНН:',INN,')'))
		) text
	from Manager
	where lastName like ?
	limit 20
;";
$params = array('s',$_GET['q'].'%');

$rows= execute_query($txt_query,$params);

echo nice_json_encode(array('results'=>$rows));