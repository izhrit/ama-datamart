<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';

$txt_query= "
	select
		  id_Debtor id
		, concat(Name
			,if(SNILS is not null,
				concat(' (СНИЛС:',SNILS,')'),
				if(INN is not null and 10<>length(INN),'',concat(' (ИНН:',INN,')'))
			)
		) text
	from Debtor
	where Name like ?
	limit 20
;";
$params = array('s',$_GET['q'].'%');

$rows= execute_query($txt_query,$params);

echo nice_json_encode(array('results'=>$rows));