<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';

$txt_query= "
	select
		  id_AssetGroup id
		, Title text
	from AssetGroup
	where Title like ?
	limit 20
;";
$params = array('s',$_GET['q'].'%');

$rows= execute_query($txt_query,$params);

echo nice_json_encode(array('results'=>$rows));