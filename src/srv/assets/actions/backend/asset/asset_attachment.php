<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/get_mime_by_filename.php';

$id_AssetAttachment= CheckMandatoryGET_id('id_AssetAttachment');

$txt_query= "select FileName, FileSize, uncompress(Body) Body, octet_length(uncompress(Body)) BodyLength from AssetAttachment where id_AssetAttachment=?;";
$rows= execute_query($txt_query,array('s',$id_AssetAttachment));
$count_rows= count($rows);
if (1!=$count_rows)
	exit_not_found("found $count_rows rows for id_AssetAttachment=$id_AssetAttachment!");

$row= $rows[0];

$path_parts = pathinfo($row->FileName);
$extension= $path_parts['extension'];
$content_type= _mime_content_type($extension);


if (!isset($_GET['no-headers']))
{
	header("Content-type: $content_type");
	header("Content-Length: {$row->BodyLength}");
	header("Content-Disposition: attachment; filename={$row->FileName}");
}
echo $row->Body;
exit;
