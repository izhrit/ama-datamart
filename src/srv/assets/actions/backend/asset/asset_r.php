<?php

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';

require_once '../assets/actions/backend/constants/AssetState.etalon.php';

function find_state_title($code)
{
	global $Asset_State_spec;
	foreach ($Asset_State_spec as $s)
	{
		if ($code==$s['code'])
			return $s['title'];
	}
	return $code;
}

class Asset_crud extends Base_crud
{
	function read($id)
	{
		$txt_query= "
			select 
				  a.id_Asset
				, a.Title Краткое_наименование
				, a.Description Развёрнутое_описание
				, a.Address Адрес
				, g.Title Категория
				, a.State Стадия
				, mp.casenumber НомерДела
				, d.Name Должник_Наименование
				, d.INN Должник_ИНН
				, d.OGRN Должник_ОГРН
				, d.SNILS Должник_СНИЛС
				, concat(m.LastName,' ',m.FirstName,' ',m.MiddleName) АУ
				, m.ArbitrManagerID ArbitrManagerID
			from Asset a
			inner join MProcedure mp on a.id_MProcedure=mp.id_MProcedure
			inner join Manager m on m.id_Manager=mp.id_Manager
			inner join Debtor d on d.id_Debtor=mp.id_Debtor
			left join AssetGroup g on a.id_AssetGroup=g.id_AssetGroup
			where a.id_Asset=?
		;";
		$rows= execute_query($txt_query,array('s',$id));

		$count_rows= count($rows);
		if (1!=$count_rows)
			exit_not_found("found $count_rows rows for id_Asset=$id");
		$asset= $rows[0];

		$asset->Стадия= find_state_title($asset->Стадия);
		$asset->Должник= array(
			'Наименование'=>$asset->Должник_Наименование
			,'ИНН'=>$asset->Должник_ИНН
			,'ОГРН'=>$asset->Должник_ОГРН
			,'СНИЛС'=>$asset->Должник_СНИЛС
		);
		unset($asset->Должник_Наименование);
		unset($asset->Должник_ИНН);
		unset($asset->Должник_ОГРН);
		unset($asset->Должник_СНИЛС);

		$txt_query= "
			select 
				  e.Code Класс
				, e.Title Наименование
			from EfrsbAssetClass e
			inner join Asset_EfrsbAssetClass ae on e.id_EfrsbAssetClass=ae.id_EfrsbAssetClass
			where ae.id_Asset=?
		;";
		$rows= execute_query($txt_query,array('s',$id));
		$asset->КлассификацияЕФРСБ= $rows;

		$txt_query= "
			select 
				  a.FileName
				, a.FileSize
				, a.id_AssetAttachment
			from AssetAttachment a
			where a.id_Asset=?
		;";
		$rows= execute_query($txt_query,array('s',$id));
		$asset->Приложения= $rows;

		echo nice_json_encode($asset);
	}
}

$crud= new Asset_crud();
$crud->process_cmd();
