<?php

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/libs/auth/check.php';

write_to_log_named('post',$_POST);

$filter= (object)$_POST;

$fields= "
	id_Asset
	,a.Title Title
	,g.Title Category
	,r.Name Address
	,date_format(a.TimeChanged,'%d.%m.%Y') TimeChanged
	,a.State
";

$from_where= 'from Asset a 
inner join MProcedure mp on a.id_MProcedure=mp.id_MProcedure
left join AssetGroup g on a.id_AssetGroup=g.id_AssetGroup 
left join region r on a.id_Region=r.id_Region
where 1=1 ';

$connection= default_dbconnect();
$dblink= $connection->get_db_link();

global $auth_info;
if (!isset($auth_info))
	$auth_info= SafeGetAuth();
write_to_log_named('auth_info',$auth_info);
if (!isset($auth_info) || null==$auth_info || 'viewer'<>$auth_info->category)
{
	$from_where.= "\r\n and 0<>a.isAvailableToAll \r\n ";
}
else
{
	$id_MUser= $auth_info->id_MUser;
	$from_where.= "\r\n and (0<>a.isAvailableToAll or exists(select av.id_MUser from Asset_MUser av where av.id_Asset=a.id_Asset and av.id_MUser=$id_MUser))\r\n ";
}

/*if (isset($filter->Слова) && null!=$filter->Слова && ''!=$filter->Слова && 'null'!=$filter->Слова)
{
	$par= mysqli_real_escape_string($dblink,$filter->Слова);
	$from_where.= "\r\n and match (Description) against ('$par' in natural language mode)\r\n ";
}*/

if (isset($filter->Регион) && null!=$filter->Регион && ''!=$filter->Регион && 'null'!=$filter->Регион)
{
	$regions= explode('|',$filter->Регион);
	$fixed_regions= array();
	foreach ($regions as $r)
		$fixed_regions[]= '"'.mysqli_real_escape_string($dblink,$r).'"';
	$par= implode(',',$fixed_regions);
	$from_where.= "\r\n and r.OKATO in ($par)\r\n ";
}

if (isset($filter->Стадия) && null!=$filter->Стадия && ''!=$filter->Стадия && 'null'!=$filter->Стадия)
{
	$states= explode('|',$filter->Стадия);
	$fixed_states= array();
	foreach ($states as $s)
		$fixed_states[]= '"'.mysqli_real_escape_string($dblink,$s).'"';
	$par= implode(',',$fixed_states);
	$from_where.= "\r\n and a.State in ($par)\r\n ";
}

if (isset($filter->АУ) && null!=$filter->АУ && ''!=$filter->АУ && 'null'!=$filter->АУ)
{
	$mids= explode('|',$filter->АУ);
	$par= implode(',',$mids);
	$from_where.= "\r\n and mp.id_Manager in ($par)\r\n ";
}

if (isset($filter->Должник) && null!=$filter->Должник && ''!=$filter->Должник && 'null'!=$filter->Должник)
{
	$dids= explode('|',$filter->Должник);
	$par= implode(',',$dids);
	$from_where.= "\r\n and mp.id_Debtor in ($par)\r\n ";
}

if (isset($filter->Категория) && null!=$filter->Категория && ''!=$filter->Категория && 'null'!=$filter->Категория)
{
	$cids= explode('|',$filter->Категория);
	$par= implode(',',$cids);
	$from_where.= "\r\n and a.id_AssetGroup in ($par)\r\n ";
}

if (isset($filter->КлассификацияЕФРСБ) && null!=$filter->КлассификацияЕФРСБ && ''!=$filter->КлассификацияЕФРСБ && 'null'!=$filter->КлассификацияЕФРСБ)
{
	$classes= explode('|',$filter->КлассификацияЕФРСБ);
	$fixed_classes= array();
	foreach ($classes as $c)
		$fixed_classes[]= '"'.mysqli_real_escape_string($dblink,$c).'"';
	$par= implode(',',$fixed_classes);
	$from_where.= "\r\n and exists(select ac.id_EfrsbAssetClass from Asset_EfrsbAssetClass ac inner join EfrsbAssetClass c on ac.id_EfrsbAssetClass=c.id_EfrsbAssetClass where ac.id_Asset=a.id_Asset and c.Code in ($par))\r\n ";
}

$filter_rule_builders= array(
	'Title'=>prep_std_filter_rule_builder_for_expression('a.Title')
	,'Category'=>prep_std_filter_rule_builder_for_expression('g.Title')
	,'Address'=>prep_std_filter_rule_builder_for_expression('r.Name')
	,'State'=>prep_std_filter_rule_builder_for_select_expression('a.State')
);

global $trace_jqgrid_query;
$trace_jqgrid_query= true;

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders, 'TimeChanged desc');
