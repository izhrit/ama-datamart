<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/PHPMailer/PHPMailerAutoload.php';
require_once '../assets/actions/backend/constants/alib_constants.php';
require_once '../assets/actions/backend/constants/SentEmail.etalon.php';
require_once '../assets/helpers/time.php';

function GetSentEmailDescription($descr)
{
	global $EmailType_descriptions;
	global $RecipientType_description;

	$EmailType= FindDescription($EmailType_descriptions,'descr',$descr);
	if (null!=$EmailType)
	{
		if (!isset($EmailType['RecipientType_description']))
			$EmailType['RecipientType_description']= FindDescription($RecipientType_description,'table',$EmailType['table']);
	}
	return $EmailType;
}

$email_postfix= "С уважением, компания
\"Русские информационные технологии\"
телефон/факс: (3412) 57-04-02
эл.адрес: info@russianit.ru
сайт www.russianit.ru
";

function PostLetter($letter,$to_email,$to_name,$EmailType_descr,$RecipientId)
{
	global $email_settings;
	$email_description= GetSentEmailDescription($EmailType_descr);
	if (null==$email_description)
	{
		write_to_log("wrong EmailType_descr: \"{$EmailType_descr}\"");
		return false;
	}
	else
	{
		$EmailType= $email_description['code'];
		$RecipientType= $email_description['RecipientType_description']['code'];
		$ExtraParams= array('Subject'=>$letter->subject, 'Body'=>$letter->body_txt, 'Receiver'=>$to_name);
		$dispatch_time= safe_date_create();

		$txt_query= "insert into SentEmail set
			  RecipientEmail=?, RecipientId=?, RecipientType=?
			, EmailType=?
			, TimeDispatch=?
			, ExtraParams=compress(?);";
		$id_SentEmail= execute_query_get_last_insert_id($txt_query,
			array('ssssss'
			, $to_email,        $RecipientId,  $RecipientType
			, $EmailType
			, date_format($dispatch_time,'Y-m-d\TH:i:s')
			, nice_json_encode($ExtraParams)));

		execute_query("insert into EmailToSend set id_SentEmail=?",array('s', $id_SentEmail));
		if (isset($letter->attachments))
		{
			$txt_query= "insert into Attachment set id_SentEmail=?, FileName=?, Content=?;";
			foreach ($letter->attachments as $a)
				execute_query($txt_query,array('sss', $id_SentEmail, $a['filename'], $a['content']));
		}

		return (object)array('EmailType'=>$EmailType
			, 'TimeDispatch'=> date_format($dispatch_time,'d.m.Y H:i:s')
			, 'id_SentEmail'=>$id_SentEmail
		);
	}
}

$txt_query_select_to_send= "select
	  e.id_SentEmail id_SentEmail
	, e.RecipientEmail RecipientEmail
	, e.RecipientId RecipientId
	, e.RecipientType RecipientType
	, e.EmailType EmailType
	, e.TimeDispatch TimeDispatch
	, uncompress(e.ExtraParams) ExtraParams
from EmailToSend t
inner join SentEmail e on e.id_SentEmail=t.id_SentEmail
where ErrorText is null
order by TimeDispatch desc
limit 10
;";

$txt_query_select_attachments= "select
FileName, Content
from Attachment
where id_SentEmail=?
order by id_Attachment
;";

$txt_query_update_SentEmail= "update SentEmail set 
	  TimeSent=? 
	, SenderServer=?
	, SenderUser=?
	, Message=?
where id_SentEmail=?;";

function SendEmail($row_email_to_send, $rows_attachment)
{
	global $email_settings;

	$mail = new PHPMailer;
	$mail->CharSet = 'UTF-8';
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $email_settings->smtp_server;           // Specify main and backup SMTP servers
	if (isset($email_settings->smtp_user) && null!=$email_settings->smtp_user)
	{
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $email_settings->smtp_user;         // SMTP username
		$mail->Password = $email_settings->smtp_password;     // SMTP password
	}
	//$mail->SMTPSecure = 'tls';                          // Enable TLS encryption, `ssl` also accepted
	$mail->Port = $email_settings->smtp_port;             // TCP port to connect to

	$mail->setFrom($email_settings->smtp_from_email, $email_settings->smtp_from_name);
	$mail->addAddress($row_email_to_send->RecipientEmail, $row_email_to_send->ExtraParams->Receiver); // Add a recipient
	$mail->addReplyTo($email_settings->smtp_from_email, $email_settings->smtp_from_name);

	$mail->isHTML(false);                                  // Set email format to HTML

	$mail->Subject = $row_email_to_send->ExtraParams->Subject;
	$mail->Body    = $row_email_to_send->ExtraParams->Body;

	if (null!=$rows_attachment)
	{
		foreach ($rows_attachment as $row_attachment)
			$mail->addStringAttachment($row_attachment->Content,$row_attachment->FileName);
	}

	if (!$mail->send())
	{
		$txt_error= "Mailer Error: {$mail->ErrorInfo}";
		write_to_log($txt_error);
		throw new Exception($txt_error);
	}

	return $mail->getSentMIMEMessage();
}

function ExceptionErrorText($ex)
{
	$ex_class= get_class($ex);
	$ex_Message= $ex->getMessage();
	$error_text= "$ex_class - $ex_Message";
	return $error_text;
}

function deliver_emails($max_portion_size= 100)
{
	return SendPortionOfEmailToSend(/*$max_portion_size=*/100);
}

function SendPortionOfEmailToSend($max_portion_size= 100)
{
	global $email_settings, $txt_query_select_to_send, $txt_query_select_attachments, $txt_query_update_SentEmail;
	$count_sent_email= 0;
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	do
	{
		$rows_email_to_send= $connection->execute_query($txt_query_select_to_send,array());
		foreach ($rows_email_to_send as $row_email_to_send)
		{
			$count_sent_email++;
			$row_email_to_send->ExtraParams= json_decode($row_email_to_send->ExtraParams);
			$sql_args= array('s',$row_email_to_send->id_SentEmail);
			$rows_attachment= $connection->execute_query($txt_query_select_attachments,$sql_args);

			$mime_message= null;
			try
			{
				$mime_message= SendEmail($row_email_to_send, $rows_attachment);
			}
			catch (Exception $ex)
			{
				echo job_time() . " exception for email {$row_email_to_send->id_SentEmail}";
				$connection->execute_query("update EmailToSend set ErrorText=? where id_SentEmail=?;",
					array('ss',ExceptionErrorText($ex),$row_email_to_send->id_SentEmail));
			}
			if (null!=$mime_message)
			{
				$connection->begin_transaction();
				try
				{
					$connection->execute_query('delete from EmailToSend where id_SentEmail=?;',$sql_args);
					$connection->execute_query($txt_query_update_SentEmail,array('sssss'
						,date_format(safe_date_create(),'Y-m-d\TH:i:s')
						,$email_settings->smtp_server
						,$email_settings->smtp_user
						,json_encode($mime_message)
						,$row_email_to_send->id_SentEmail)
					);
					$connection->commit();
				}
				catch (Exception $ex)
				{
					$connection->rollback();
					throw $ex;
				}
				echo job_time() . " sent email {$row_email_to_send->id_SentEmail}\r\n";
			}
		}
	}
	while (count($rows_email_to_send)>0 && $count_sent_email<$max_portion_size);
	return "отправлено: $count_sent_email";
}
