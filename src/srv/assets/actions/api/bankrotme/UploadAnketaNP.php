<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/actions/api/alib_api_auth.php';

$access_token= null;

CheckMandatoryGET('debtor');
CheckMandatoryGET('manager');
CheckMandatoryGET('content-hash');

$auth_info= std_AuthByToken();

if (null==$access_token)
	throw new Exception("undefined access_token after auth!");

$manager= $_GET['manager'];
$debtor= $_GET['debtor'];

function ReadZip()
{
	switch ($_SERVER["CONTENT_TYPE"])
	{
		case 'application/zip': 
			$result= file_get_contents('php://input');
			return $result;
		case 'application/null': 
			return null;
		case 'application/xml':
			$tmp_file_path = tempnam("tmp", "zip");
			$zip = new ZipArchive();
			$zip->open($tmp_file_path, ZipArchive::OVERWRITE);
			$xml_content= file_get_contents('php://input');
			$zip->addFromString('anketanp.xml', $xml_content);
			$zip->close();
			$result= file_get_contents($tmp_file_path);
			unlink($tmp_file_path);
			return $result;
	}
	throw new Exception("unknown request content-type ".$_SERVER["CONTENT_TYPE"]);
}

function Upload($access_token, $owner, $manager, $debtor)
{
	global $partner_accounts, $auth_info;
	execute_query_no_result('call SafeUploadAnketa(?,?, ?,?,?,?,?,?,?,?,@error_text);',
		array('ssssssssbs',
		$manager['inn'],$partner_accounts[$auth_info->id_Owner]['id_MUser'],
		$debtor['first-name'], $debtor['last-name'], $debtor['middle-name'],
		(!isset($debtor['email'])||''==$debtor['email'])?null:$debtor['email'],
		(!isset($debtor['snils'])||''==$debtor['snils'])?null:$debtor['snils'],
		$debtor['inn'],
		ReadZip(),$_GET['content-hash']
	));

	$res_rows= execute_query('select @error_text error_text;', array());
	$res_row= $res_rows[0];
	if (null!=$res_row->error_text)
	{
		echo nice_json_encode(array('ok'=>false,'message'=>$res_row->error_text));
	}
	else
	{
		OkAuthByToken($access_token);
		echo json_encode(array('ok'=>true));
	}

	exit;
}

try
{
	Upload($access_token, $auth_info->id_Owner, $manager, $debtor);
}
catch (Exception $exception)
{
	write_to_log('$auth_info:');
	write_to_log($auth_info);
	throw $exception;
}

header("HTTP/1.1 401 Unauthorized");
echo write_to_log("insufficient rights to this action");
exit;