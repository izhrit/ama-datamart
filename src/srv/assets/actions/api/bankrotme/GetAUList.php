<?php
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/api/alib_api_auth.php';

$auth_info= std_AuthByToken();

mb_internal_encoding("utf-8");

CheckMandatoryGET('q');

$q= $_GET['q'].'%';

$txt_query= "select
	m.id_Manager
	, concat(m.lastName, ' ', m.firstName, ' ', m.middleName) Name
	, m.INN
	, m.efrsbNumber
from manager m
where
m.lastName like ? 
or m.firstName like ? 
or m.middleName like ? 
or m.INN like ? 
or m.efrsbNumber like ?
order by Name
limit 20;";

$rows= execute_query($txt_query, array('sssss',$q,$q,$q,$q,$q));

echo nice_json_encode($rows);