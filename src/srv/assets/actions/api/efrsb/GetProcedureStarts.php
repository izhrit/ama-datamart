<?php
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

mb_internal_encoding("utf-8");

$field_name= null;
$parameters= array('s');
if (isset($_GET['RegNum']) && ''!=$_GET['RegNum'])
{
	$field_name= 'm.efrsbNumber';
	$parameters[]= $_GET['RegNum'];
}
else if (isset($_GET['INN']) && ''!=$_GET['INN'])
{
	$field_name= 'm.INN';
	$parameters[]= $_GET['INN'];
}
else
{
	require_once '../assets/actions/api/alib_api_auth.php';
	$auth_info= std_AuthByToken();
	switch ($auth_info->Who)
	{
		case 'c':
			$field_name= 'ps.id_Contract';
			$parameters[]= $auth_info->id_Owner;
			break;
		case 'm':
			$field_name= 'ps.id_Manager';
			$parameters[]= $auth_info->id_Owner;
			break;
		default:
			exit_bad_request('skipped mandatory argument INN or RegNum!');
	}
}

$txt_query= "SELECT 
  ps.DebtorName Наименование
  , ps.DebtorINN ИНН
  , ps.DebtorSNILS СНИЛС
  , ps.DebtorOGRN ОГРН
  , ps.DebtorAddress Адрес
  , ps.CaseNumber
  , c.Name Суд
  , m.INN m_INN
  , m.efrsbNumber m_efrsbNumber
FROM ProcedureStart ps
INNER JOIN Manager m on ps.id_Manager = m.id_Manager
INNER JOIN Court c on ps.id_Court = c.id_Court
WHERE $field_name = ?
and ReadyToRegistrate<>0
order by ps.id_ProcedureStart desc;";

$rows= execute_query($txt_query, $parameters);

echo nice_json_encode($rows);