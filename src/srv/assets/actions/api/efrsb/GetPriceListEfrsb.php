<?php
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

$txt_query= "select id_pefrsb, date_of_set_price, price_for_physical_entity, price_for_physical_entity_with_commission, price_for_legal_entity, price_for_legal_entity_with_commission from price_efrsb;";

$rows= execute_query($txt_query, array());

echo nice_json_encode($rows);