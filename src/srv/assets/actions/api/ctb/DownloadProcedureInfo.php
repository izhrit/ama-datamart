<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';

require_once '../assets/actions/api/alib_api_auth.php';

$auth_info= std_AuthByToken_ctb();

$id_Procedure= $_GET['id_MProcedure'];

$txt_query= "select 
	fileData
	, octet_length(fileData) as length
	, revision 
from PData 
where id_MProcedure=? && 1=ctb_allowed
order by revision desc 
limit 1
;";

$rows= execute_query($txt_query,array('i',$id_Procedure));
$rows_count= count($rows);
if (1!=$rows_count)
{
	header("HTTP/1.1 404 Not Found");
	echo 'can not find available procedure info';
	write_to_log("found $rows_count rows for id_MProcedure=$id_Procedure");
}
else
{
	$row= $rows[0];
	$length= $row->length;
	$revision= $row->revision;
	header('Content-Description: File Transfer');
	header('Content-Type: application/zip',false);
	header('Content-Type: application/octet-stream',false);
	header("Content-Length: $length");
	header("Content-Disposition: attachment; filename=Procedure_$id_Procedure.zip");
	header("ama-dm-revision: $revision");
	header('Content-Transfer-Encoding: binary');
	echo $row->fileData;
	write_to_log("written $length bytes for id_MProcedure=$id_Procedure, revision=$revision");
}