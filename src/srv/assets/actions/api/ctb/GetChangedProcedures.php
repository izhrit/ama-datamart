<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';

require_once '../assets/actions/api/alib_api_auth.php';

$auth_info= std_AuthByToken_ctb();

$txt_query_prefix= "select SQL_CALC_FOUND_ROWS
	  mp.id_MProcedure id_MProcedure
	, mp.ctb_revision revision
FROM MProcedure mp 
WHERE mp.ctb_revision > ? && mp.ctb_revision > 0 && 'v'=VerificationState
";

$txt_query_postfix= "
ORDER BY mp.ctb_revision 
LIMIT ?;";

$from_revison= $_GET['ama-dm-revision-greater-than'];
$limit= $_GET['limit'];

$requested= array('процедур_загруженных_после_ревизии'=>$from_revison,'ограниченый_длиной_не_более'=>$limit);
if (!isset($_GET['procedure']) || !isset($_GET['procedure']['case_number']))
{
	$result= execute_query_get_found_rows($txt_query_prefix.$txt_query_postfix,array('ii',$from_revison,$limit));
}
else
{
	$case_number= $_GET['procedure']['case_number'];
	$requested['по_делу_номер']= $case_number;
	$result= execute_query_get_found_rows($txt_query_prefix.' && casenumber=? '.$txt_query_postfix
		,array('isi',$from_revison,$case_number,$limit));
}

echo nice_json_encode(array(
	'Запрошен_список'=>$requested
	,'Всего_после_указанной_в_запросе_ревизии_загружено_процедур'=>$result['found_rows']
	, 'Запрошенный_список_процедур'=>$result['rows']
));
