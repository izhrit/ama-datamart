<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

function encrypt($message)
{
	global $session_token_salt;

	$key =  pack('H*', $session_token_salt);

	$method= 'AES-128-CBC';
	$nonceSize = openssl_cipher_iv_length($method);
	$nonce = openssl_random_pseudo_bytes($nonceSize);

	$options= 0;
	if (version_compare(PHP_VERSION, '5.4.0') >= 0)
	{
		$options= OPENSSL_RAW_DATA;
	}
	else
	{
		$options= 'OPENSSL_RAW_DATA';
	}
	$ciphertext = openssl_encrypt(
		$message,
		$method,
		$key,
		$options,
		$nonce
	);

	return bin2hex($nonce.$ciphertext);
}

function prepare_token($as,$id)
{
	$open_token= time().$as.'|'.$id.'|';
	$crypted_token= encrypt($open_token);
	return $crypted_token;
}

function ClearOldSessions()
{
	global $auth_max_session_seconds, $auth_max_session_idle_seconds;

	$txt_query= "delete from Session 
	where now() > date_add(TimeStarted, interval ? second) 
	   || now() > date_add(TimeLastUsed, interval ? second);";
	execute_query_no_result($txt_query,array('ii',$auth_max_session_seconds,$auth_max_session_idle_seconds));
}

function OkLogin($as,$id)
{
	$token= prepare_token($as,$id);
	execute_query_no_result('delete from Session where Who=? && id_Owner=?;',array('si',$as,$id));
	execute_query_no_result('insert into Session set Token=?, Who=?, id_Owner=?, TimeStarted=now(), TimeLastUsed=now();',
							array('ssi',$token,$as,$id));
	ClearOldSessions();
	echo nice_json_encode(array('token'=>$token));
}

function AuthByToken($access_token)
{
	global $auth_max_session_seconds, $auth_max_session_idle_seconds;

	$txt_query= "select 
		  Who
		, id_Owner
		, TimeStarted
		, TimeLastUsed 
	from Session 
	where Token=? 
	&& now() < date_add(TimeStarted, interval ? second) 
	&& now() < date_add(TimeLastUsed, interval ? second);";
	$rows= execute_query($txt_query,array('sii',$access_token,$auth_max_session_seconds,$auth_max_session_idle_seconds));

	if (1!=count($rows))
	{
		header("HTTP/1.1 401 Unauthorized");
		write_to_log("wrong access token \"$access_token\"");
		if (function_exists('write_error_to_access_log'))
			write_error_to_access_log("wrong access token \"$access_token\"");
		exit;
	}

	return (object)$rows[0];
}

function OkAuthByToken($access_token)
{
	$affected_rows= execute_query_get_affected_rows('update Session set TimeLastUsed=now() where Token=?;',array('s',$access_token));
	if (1!=$affected_rows)
		write_to_log("affected_rows=$affected_rows for update Session set TimeLastUsed=now() where Token=$access_token");
}

function std_AuthByToken()
{
	global $access_token;

	$access_token= null;
	if (isset($_SERVER['HTTP_AMA_DATAMART_ACCESS_TOKEN']))
		$access_token= $_SERVER['HTTP_AMA_DATAMART_ACCESS_TOKEN'];

	return AuthByToken($access_token);
}

function std_AuthByToken_ctb()
{
	global $partner_accounts;
	$auth_info= std_AuthByToken();

	if ('p'!=$auth_info->Who || 'ctb'!=$partner_accounts[$auth_info->id_Owner]['login'])
	{
		header("HTTP/1.1 401 Unauthorized");
		echo "insufficient rights to this action";
		write_to_log("function inaccessible for:\r\n".print_r($auth_info,true));
		exit;
	}

	return $auth_info;
}

function std_AuthByToken_contract()
{
	global $partner_accounts;
	$auth_info= std_AuthByToken();

	if ('c'!=$auth_info->Who)
	{
		header("HTTP/1.1 401 Unauthorized");
		echo "insufficient rights to this action";
		write_to_log("function is for contract and is inaccessible for:\r\n".print_r($auth_info,true));
		exit;
	}

	return $auth_info;
}

function std_AuthByToken_manager()
{
	global $partner_accounts;
	$auth_info= std_AuthByToken();

	if ('m'!=$auth_info->Who)
	{
		header("HTTP/1.1 401 Unauthorized");
		echo "insufficient rights to this action";
		write_to_log("function is for manager and is inaccessible for:\r\n".print_r($auth_info,true));
		exit;
	}

	return $auth_info;
}

function std_AuthByToken_orpau()
{
	global $partner_accounts;
	$auth_info= std_AuthByToken();

	if ('p'!=$auth_info->Who || 'orpau'!=$partner_accounts[$auth_info->id_Owner]['login'])
	{
		header("HTTP/1.1 401 Unauthorized");
		echo "insufficient rights to this action";
		write_to_log("function inaccessible for:\r\n".print_r($auth_info,true));
		exit;
	}

	return $auth_info;
}
