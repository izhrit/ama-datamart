<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';

require_once '../assets/libs/log/access_log.php';
require_once '../assets/libs/auth/license.php';
require_once '../assets/libs/auth/license-ama.php';

require_once 'alib_api_auth.php';

$login= null;
$password= null;
$category= null;

if (isset($_GET['login']))
{
	$login= $_GET['login'];
	$password= $_POST['password'];
	if (isset($_GET['category']))
		$category= $_GET['category'];
}
else
{
	$s_in_data= file_get_contents('php://input');
	$in_data= json_decode($s_in_data);
	if (isset($in_data->login))
	{
		$login= $in_data->login;
		$password= $in_data->password;
		if (isset($in_data->as))
			$category= $in_data->as;
	}
}

if (null==$category)
	$category= 'customer';

function LoginAsCustomer($login,$password,$as)
{
	global $crm2_db_options;
	$crm2db= new Reusable_mysql_connection($crm2_db_options);
	$Contract_table_name= $crm2_db_options->Contract_table_name;
	$txt_query= "select 
		  ContractNumber
	from $Contract_table_name
	where ContractNumber=? && Password=?;";
	$rows= $crm2db->execute_query($txt_query,array('ss',$login,$password));
	$count= count($rows);
	if (1!=$count)
	{
		write_to_log("wrong login \"$login\" password \"$password\" as \"$as\"");
		echo 'null';
	}
	else
	{
		$txt_query= "select  ContractNumber, id_Contract from Contract where ContractNumber=?;";
		$rows= execute_query($txt_query,array('s',$login));
		$count= count($rows);
		if (1==$count)
		{
			$id_Contract= $rows[0]->id_Contract;
		}
		else
		{
			$txt_query= "insert into Contract set ContractNumber=?;";
			$id_Contract= execute_query_get_last_insert_id($txt_query,array('s',$login));
		}
		OkLogin($as,$id_Contract);
		write_to_access_log_id('auth/customer',$id_Contract);
	}
}

function LoginAsManager($login,$password,$as)
{
	if (false===strpos($login,'@'))
	{
		write_to_log('wrong login, try to login as customer');
		LoginAsCustomer($login,$password,'c');
	}
	else
	{
		$txt_query= "select id_Manager from Manager where ManagerEmail=? && Password=?;";
		$rows= execute_query($txt_query,array('ss',$login,$password));
		$count= count($rows);
		if (1!=$count)
		{
			write_to_log("wrong login \"$login\" password \"$password\" as \"$as\"");
			echo 'null';
		}
		else
		{
			$id_Manager= $rows[0]->id_Manager;
			OkLogin($as,$id_Manager);
			write_to_access_log_id('auth/manager',$id_Manager);
		}
	}
}

function LoginAsPartner($login,$password,$as)
{
	global $partner_accounts;
	foreach ($partner_accounts as $i => $partner_account)
	{
		if ($login!=$partner_account['login'] || $password!=$partner_account['password'])
		{
			write_to_log("wrong login \"$login\" password \"$password\" as \"$as\"");
		}
		else
		{
			OkLogin($as,$i);
			write_to_access_log_id('auth/partner',$i);
			return;
		}
	}
	echo 'null';
}

function auto_login_ok($ainfo,$category)
{
	switch ($category)
	{
		case 'customer':
			OkLogin('c',$ainfo->id_Contract);
			write_to_access_log_id('auth/customer/ama',$ainfo->id_Contract);
			break;
		case 'manager':
			OkLogin('m',$ainfo->id_Manager);
			write_to_access_log_id('auth/manager/ama',$ainfo->id_Manager);
			break;
		default: echo 'null'; break;
	}
	exit();
}

function auto_login_fail($msg,$auth_info= null,$token_info= null)
{
	$txt= "api autologin fail: $msg";
	if (null!=$auth_info)
	{
		$txt.= "\r\nauth information:\r\n";
		$txt.= print_r($auth_info,true);
	}
	if (null!=$token_info)
	{
		$txt.= "\r\ntoken information:\r\n";
		$txt.= print_r($token_info,true);
	}
	write_to_log($txt);
	echo 'null';
	exit();
}

function LoginUsingLicenseToken($auth)
{
	global $api_autologin_auth_options;
	$auth_args= decrypt_auth($auth,$api_autologin_auth_options);
	if (null==$auth_args)
	{
		write_to_log('can not decrypt auth arg!');
		echo 'null';
	}
	else
	{
		Проверить_актуальность_авторизационных_данных_для_входа($auth_args);
		Авторизоваться_по_информации_токена_короткоживущей_лицензиии($auth_args);
	}
}

if (isset($_POST['auto']))
{
	require_once '../assets/helpers/time.php';
	require_once '../assets/helpers/sync_encrypt_decrypt.php';
	require_once '../assets/helpers/realip.php';
	require_once '../assets/libs/auth/license-ama.php';
	LoginUsingLicenseToken($_POST['auto']);
}
elseif (!isset($login))
{
	header("HTTP/1.1 401 Unauthorized");
	echo "undefined login";
	exit;
}
elseif (!isset($password))
{
	header("HTTP/1.1 401 Unauthorized");
	echo "undefined password";
	exit;
}
else
{
	switch ($category)
	{
		case 'customer': LoginAsCustomer($login,$password,'c'); exit;
		case 'manager': LoginAsManager($login,$password,'m'); exit;
		case 'partner': LoginAsPartner($login,$password,'p'); exit;
		default:
			header("HTTP/1.1 401 Unauthorized");
			echo "unexpected category";
			exit;
	}
}

header("HTTP/1.1 401 Unauthorized");
echo "unexpected end";
