<?

require_once '../assets/helpers/db.php';

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/post.php';

require_once '../assets/libs/log/access_log.php';
require_once '../assets/libs/procedure/prepare_proc.php';

require_once '../assets/actions/api/alib_api_auth.php';

$auth_info= std_AuthByToken_manager();
$id_Manager = intval($auth_info->id_Owner);

function write_error_to_access_log($error_text)
{
	$details= array(
		'error'=>$error_text
		,'GET'=>$_GET
		,'POST'=>$_POST
	);
	write_to_access_log_details('electrokk/create/ama/error',nice_json_encode($details));
}

function create_meeting($connection, $meeting)
{
	$date= date_create_from_format('d.m.Y H:i',$meeting['Дата_заседания'].' '.$meeting['Время_заседания']);
	$date_sql= date_format($date,'Y-m-d\TH:i:s');
	$txt_query= "insert into Meeting set id_MProcedure=?, Date=?, Body=compress(?);";
	$id_Meeting= $connection->execute_query_get_last_insert_id($txt_query,
				array('sss',$meeting['id_MProcedure'],$date_sql,nice_json_encode($meeting)));
	return $id_Meeting;
}

function prepare_procedure($connection,$Должник,$id_Manager,$procedure_type, $case_number)
{
	return safe_prep_MProcedure($connection,(object)$Должник,$id_Manager,$procedure_type, $case_number,'При попытке создать заседание ЭлектроКК');
}

function update_Manager($connection, $manager_phone, $id_Manager)
{
	$txt_query= "select uncompress(ExtraParams) ExtraParams from Manager where id_Manager=?;";
	$rows=$connection->execute_query($txt_query,array('i',$id_Manager));
	$count_rows= count($rows);
	if (1!=$count_rows)
		throw new Exception("found $count_rows Managers to update for id_Manager=$id_Manager");
	$txt_ExtraParams= $rows[0]->ExtraParams;
	$ExtraParams= null;
	if (null==$txt_ExtraParams)
	{
		$ExtraParams= (object)array();
	}
	else
	{
		$ExtraParams= json_decode($txt_ExtraParams);
		if (null==$ExtraParams || !is_object($ExtraParams))
			throw new Exception("can not parse ExtraParams for id_Manager=$id_Manager: $txt_ExtraParams");
	}
	$ExtraParams->Телефон= $manager_phone;
	$txt_ExtraParams= nice_json_encode($ExtraParams);
	$txt_query= "update Manager set ExtraParams=compress(?) where id_Manager=?;";
	$affected_rows= $connection->execute_query_get_affected_rows($txt_query,array('si',$txt_ExtraParams,$id_Manager));
	if (1<$affected_rows)
		throw new Exception("updated $affected_rows Managers for id_Manager=$id_Manager!");
}

function update_MProcedure($connection, $debtor_address,$id_MProcedure,$id_Manager)
{
	$txt_query= "select uncompress(ExtraParams) ExtraParams from MProcedure
				 where id_MProcedure=? && id_Manager=?;";
	$rows= $connection->execute_query($txt_query,array('ii',$id_MProcedure,$id_Manager));
	$count_rows= count($rows);
	if (1!=$count_rows)
		throw new Exception("found $count_rows MProcedures to update for id_MProcedure=$id_MProcedure and id_Manager=$id_Manager");
	$txt_ExtraParams= $rows[0]->ExtraParams;
	$ExtraParams= null;
	if (null==$txt_ExtraParams)
	{
		$ExtraParams= (object)array();
	}
	else
	{
		$ExtraParams= json_decode($txt_ExtraParams);
		if (null==$ExtraParams || !is_object($ExtraParams))
			throw new Exception("can not parse ExtraParams for id_MProcedure=$id_MProcedure: $txt_ExtraParams");
	}
	$ExtraParams->Адрес= $debtor_address;
	$txt_ExtraParams= nice_json_encode($ExtraParams);
	$txt_query= "update MProcedure set ExtraParams=compress(?) where id_MProcedure=? && id_Manager=?;";
	$affected_rows= $connection->execute_query_get_affected_rows($txt_query,array('sii',$txt_ExtraParams,$id_MProcedure,$id_Manager));
	if (1<$affected_rows)
		throw new Exception("updated $affected_rows MProcedures for id_MProcedure=$id_MProcedure!");
}

function prepare_meeting_info($connection,$meeting,$id_Manager,$procedure_type, $case_number,$debtor_address, $manager_phone)
{
	$Должник= $meeting['Должник'];
	$Должник['СНИЛС']= preg_replace('~\D+~','', $Должник['СНИЛС']);
	$id_MProcedure= prepare_procedure($connection,$Должник,$id_Manager,$procedure_type, $case_number);
	$meeting['id_MProcedure']= $id_MProcedure;
	update_MProcedure($connection, $debtor_address,$id_MProcedure,$id_Manager);
	update_Manager($connection, $manager_phone,$id_Manager);
	$meeting['Телефон_АУ'] = $manager_phone;
	$Должник['Местонахождение'] = $debtor_address;
	$meeting['Должник']= $Должник;
	return $meeting;
}

function CreateAssembly_with_transaction($id_Manager, $meeting, $debtor_address, $procedure_type, $case_number, $manager_phone)
{
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		$meeting_info= prepare_meeting_info($connection, $meeting,$id_Manager,$procedure_type,$case_number,$debtor_address,$manager_phone);
		$id_Meeting= create_meeting($connection, $meeting_info);
		$connection->commit();
		return $id_Meeting;
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
}

$created= false;
try
{
	$meeting= safe_POST(CheckMandatoryPOST('Meeting'));
	$debtor_address= CheckMandatoryPOST('debtor_address');
	$procedure_type= CheckMandatoryNotEmptyPOST('procedure_type');
	$case_number= CheckMandatoryNotEmptyPOST('case_number');
	$manager_phone= CheckMandatoryNotEmptyPOST('manager_phone');

	$id_Meeting= CreateAssembly_with_transaction($id_Manager, $meeting, $debtor_address, $procedure_type, $case_number, $manager_phone);

	echo json_encode((object)array('id_Meeting'=>$id_Meeting,'ok'=>true));
	$created= true;
}
catch (Exception $ex)
{
	$res= array('ok'=>false,'message'=>'Исключительная ситуация в ходе регистрации заседания КК на витрине (заседание НЕ зарегистрировано)!');
	echo nice_json_encode($res);
	$ex_class= get_class($ex);
	$ex_message= $ex->getMessage();
	write_error_to_access_log("Exception $ex_class message: $ex_message");
	exit();
}

if ($created)
{
	$fields= array('Id'=>$id_Manager,'Details'=>nice_json_encode(array('id_Meeting'=>$id_Meeting,'POST'=>$_POST)));
	write_to_access_log_fields('electrokk/create/ama',$fields);
}
