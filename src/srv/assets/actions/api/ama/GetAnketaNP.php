<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/actions/api/alib_api_auth.php';

$auth_info= std_AuthByToken();

CheckMandatoryGET('id');

switch ($auth_info->Who)
{
	case 'c':
		$check_id_Owner= ' inner join Manager m on m.id_Manager=mu.id_Manager where m.id_Contract';
		break;
	case 'm':
		$check_id_Owner= ' where mu.id_Manager';
		break;
	default:
		exit_unauthorized('authorized user can not use this method!');
}

$txt_query= "select fileData 
from MData md
inner join ManagerUser mu on md.id_ManagerUser=mu.id_ManagerUser
$check_id_Owner=? && id_MData = ?;";

$rows = execute_query($txt_query, array ('ss', $auth_info->id_Owner,$_GET['id']));
$count_rows= count($rows);
if (1!=$count_rows)
	exit_not_found("found $count_rows rows..");


$content_len= strlen($rows[0]->fileData);
header('Content-Length: '.$content_len);

echo $rows[0]->fileData;
