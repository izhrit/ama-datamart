<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';

require_once '../assets/actions/api/alib_api_auth.php';

$access_token= null;

$auth_info= std_AuthByToken();
if (null==$access_token)
	throw new Exception("undefined access_token after auth!");

$manager= $_GET['manager'];
$debtor= $_GET['debtor'];
$procedure= $_GET['procedure'];

if (!isset($_GET['ama-version']))
{
	header("HTTP/1.1 406 Not Acceptable");
	echo "not defined ama-version query argument";
	write_to_log("not defined ama-version");
	exit;
}

function ReadZip()
{
	switch ($_SERVER["CONTENT_TYPE"])
	{
		case 'application/zip': 
			$result= file_get_contents('php://input');
			return $result;
		case 'application/null': 
			return null;
		case 'application/xml':
			$tmp_file_path = tempnam("tmp", "zip");
			$zip = new ZipArchive();
			$zip->open($tmp_file_path, ZipArchive::OVERWRITE);
			$xml_content= file_get_contents('php://input');
			$zip->addFromString('registry.xml', $xml_content);
			$zip->close();
			$result= file_get_contents($tmp_file_path);
			unlink($tmp_file_path);
			return $result;
	}
	throw new Exception("unknown request content-type ".$_SERVER["CONTENT_TYPE"]);
}

function EncryptSignature($attrs)
{
	global $signature_options;

	$attrs['time']= time();
	$attrs['ama-version']= $_GET['ama-version'];
	$txt_res= nice_json_encode($attrs);

	$encrypted_text = sync_encrypt($txt_res,$signature_options);
	return base64_encode($encrypted_text);
}

function PrepareBthAccount($manager)
{
	if (!isset($manager['bt']))
	{
		return null;
	}
	else
	{
		global $bt_options, $bt_password_options;

		$bt_urldecoded= urldecode($manager['bt']);
		$bt= base64_decode(str_replace(' ','+',$bt_urldecoded));

		$decrypted_text= sync_decrypt($bt,$bt_options);

		$bt_obj= json_decode($decrypted_text);
		$bt_obj->password= base64_encode(sync_encrypt($bt_obj->password,$bt_password_options));

		return json_encode($bt_obj);
	}
}

function Upload($access_token, $id_Contract, $manager, $debtor, $procedure)
{
	global $casebook_account;

	$ctb_allow= (isset($_GET['ctb_allow']) && 'true'==$_GET['ctb_allow'])?1:0;
	$content_hash= (!isset($_GET['content-hash']))?0:$_GET['content-hash'];
	$zip_content= ReadZip();

	$qarguments= array('issssssssssssis',
		$id_Contract,
		$manager['name'],$manager['patronymic'],$manager['surname'],$manager['efrsb_number'],
		(!isset($manager['inn'])||''==$manager['inn'])?null:$manager['inn'],
		PrepareBthAccount($manager),
		$debtor['inn'],$debtor['name'],
		(!isset($debtor['ogrn'])||''==$debtor['ogrn'])?null:$debtor['ogrn'],
		(!isset($debtor['snils'])||''==$debtor['snils'])?null:$debtor['snils'],
		$procedure['case_number'],$procedure['type'],
		$ctb_allow,$content_hash
	);

	if (null==$zip_content)
	{
		$rows= execute_query('call SafeUploadProcedureInfo(?, ?,?,?,?,?,?, ?,?,?,?, ?,null,?, ?, ?, null);',$qarguments);
	}
	else
	{
		$qarguments[0].= 'b';
		$qarguments[]= $zip_content;
		$rows= execute_query('call SafeUploadProcedureInfo(?, ?,?,?,?,?,?, ?,?,?,?, ?,null,?, ?, ?, ?);',$qarguments);
	}
	OkAuthByToken($access_token);

	$constraint_parameters_problems= null;
	$rows_count= count($rows);
	if (0==$rows_count && isset($_GET['ctb_allow']) && 'true'==$_GET['ctb_allow'])
	{
		$res= array('digest'=>$casebook_account['login'],'subject'=>$casebook_account['password']);
	}
	else
	{
		$res= array('digest'=>'ueue','subject'=>'5467');
		if (0!=$rows_count)
		{
			require_once '../assets/libs/constraints/constraints_details.php';
			$res['text']= 'Загрузка информации на витрину данных завершилась неудачно!'
				.contraint_error_text_by_rows($rows);
			$constraint_parameters_problems= array(
				'parameters'=>array('manager'=>$manager,'debtor'=>$debtor,'procedure'=>$procedure),
				'problems'=>contraint_problems_by_rows($rows)
			);
			$res['constraints']= $constraint_parameters_problems;
		}
	}
	echo json_encode(array('Signature'=>EncryptSignature($res)));
	if (null!=$constraint_parameters_problems)
	{
		require_once '../assets/libs/log/access_log.php';
		write_constraint_error_to_access_log($constraint_parameters_problems);
	}
	exit;
}

function UploadAsCustomer($access_token, $auth_info, $manager, $debtor, $procedure)
{
	Upload($access_token, $auth_info->id_Owner, $manager, $debtor, $procedure);
}

function UploadAsManager($access_token, $auth_info, $manager, $debtor, $procedure)
{
	$id_Manager= $auth_info->id_Owner;
	$txt_query= "select 
		  firstName
		, middleName
		, lastName
		, efrsbNumber
		, id_Contract
	from Manager 
	where id_Manager=?;";
	$rows= execute_query($txt_query,array('i',$id_Manager));
	$mrow= $rows[0];
	if ($mrow->efrsbNumber==$manager['efrsb_number'] 
		&& $mrow->firstName==$manager['name'] 
		&& $mrow->middleName==$manager['patronymic'] 
		&& $mrow->lastName==$manager['surname'])
	{
		Upload($access_token, $mrow->id_Contract, $manager, $debtor, $procedure);
	}
	else
	{
		write_to_log(
			"bad manager!\r\nmanager (id_Manager:$id_Manager):\r\n"
			.print_r($mrow,true)
			."tried to upload procedure for manager:\r\n"
			.print_r($manager,true)
		);
	}
}

function UploadAsPartner($access_token, $auth_info)
{
	$id= $auth_info->id_Owner;
	write_to_log("partner $id can not UploadProcedureInfo!");
}

try
{
	switch ($auth_info->Who)
	{
		case 'c': UploadAsCustomer($access_token, $auth_info, $manager, $debtor, $procedure); break;
		case 'm': UploadAsManager($access_token, $auth_info, $manager, $debtor, $procedure); break;
		case 'p': UploadAsPartner($access_token, $auth_info); break;
	}
}
catch (Exception $exception)
{
	write_to_log('$auth_info:');
	write_to_log($auth_info);
	throw $exception;
}

header("HTTP/1.1 401 Unauthorized");
echo write_to_log("insufficient rights to this action");
exit;
