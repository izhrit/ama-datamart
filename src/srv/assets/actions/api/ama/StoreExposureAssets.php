<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/api/alib_api_auth.php';
require_once '../assets/actions/backend/constants/AssetState.etalon.php';
require_once '../assets/libs/log/access_log.php';
require_once '../assets/libs/exposure/assetmd5.php';
require_once '../assets/actions/backend/alib_emails.php';

$auth_info= std_AuthByToken_manager();
$id_Manager = intval($auth_info->id_Owner);

require_once '../assets/libs/procedure/prepare_proc.php';

$_POST_text= file_get_contents("php://input");
$id_MProcedure= CheckMandatoryGET('id_MProcedure');

$to_exposure= json_decode($_POST_text);

write_to_access_log_fields('exposure/store/asset/start'
	,array(
		'Id'=>$id_Manager
		,'Details'=>nice_json_encode(array('id_MProcedure'=>$id_MProcedure,'to_exposure'=>$to_exposure))
	)
);

function find_state_code($Стадия)
{
	global $Asset_State_spec;
	foreach ($Asset_State_spec as $s)
	{
		if ($Стадия==$s['title'])
			return $s['code'];
	}
	return null;
}

function Сохранить_КлассификациюЕФРСБ_объекта($connection,$id_MProcedure,$o,$id_Asset)
{
	$connection->execute_query_no_result('delete from Asset_EfrsbAssetClass where id_Asset=?',array('s',$id_Asset));
	if (isset($o->КлассификацияЕФРСБ) && 0!=count($o->КлассификацияЕФРСБ))
	{
		$codes= array();
		foreach ($o->КлассификацияЕФРСБ as $c)
			$codes[]= '"'.$c->code.'"';
		$codes= implode(',',$codes);

		$txt_query= "insert into Asset_EfrsbAssetClass 
			(id_Asset,id_EfrsbAssetClass)
			select a.id_Asset, c.id_EfrsbAssetClass 
			from EfrsbAssetClass c 
			inner join Asset a on a.id_Asset=?
			where Code in ($codes)";
		$connection->execute_query($txt_query,array('s',$id_Asset));
	}
}

function get_id_MUser_for_id_Exposure_recipient_pro($id)
{
	global $exposure_recipiet_pro_list;
	foreach ($exposure_recipiet_pro_list as $erp)
	{
		if ($erp->id==$id)
			return !isset($erp->id_MUser) ? null : $erp->id_MUser;
	}
	return null;
}

function PrepareLetter($user_name,$debtor_name,$au_name,$au_INN)
{
	$letter= (object)array('subject'=>"ПАУ: имущество $debtor_name");
	ob_start();
	include "../assets/libs/exposure/letter.html";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();
	return $letter;
}

function Направить_письма_получателям_объектов($connection,$id_MProcedure,$id_MUser_values)
{
	write_to_log('Направить_письма_получателям_объектов {');
	$txt_query= "select 
	mu.id_MUser id_MUser, UserEmail, UserName, d.Name dName, m.LastName, m.FirstName, m.MiddleName, m.INN
	from MUser mu
	inner join MProcedure mp on mp.id_MProcedure=?
	inner join Manager m on m.id_Manager=mp.id_Manager
	inner join Debtor d on d.id_Debtor=mp.id_Debtor
	left join Asset_proc_for_MUser am on am.id_MProcedure=mp.id_MProcedure and am.id_MUser=mu.id_MUser
	where mu.id_MUser in ($id_MUser_values) and am.TimeCreated is null";
	$rows= $connection->execute_query($txt_query, array('s',$id_MProcedure));
	$count_rows= count($rows);
	if (0!=$count_rows)
	{
		$id_MUser_to_insert_values= array();
		foreach ($rows as $row)
		{
			$id_MUser_to_insert_values[]= $row->id_MUser;
			$au_name= "{$row->LastName} {$row->FirstName} {$row->MiddleName}";
			$letter= PrepareLetter($row->UserName,$row->dName,$au_name,$row->INN);
			PostLetter($letter,$row->UserEmail,$row->UserName,'информация об экспозиции',$row->id_MUser);
		}
		$id_MUser_to_insert_values= implode(',',$id_MUser_to_insert_values);
		$txt_query= "insert into Asset_proc_for_MUser (id_MProcedure,id_MUser,Details)
		select mp.id_MProcedure, mu.id_MUser, compress('{}')
		from MUser mu
		inner join MProcedure mp on mp.id_MProcedure=?
		where mu.id_MUser in ($id_MUser_values);";
		$rows= $connection->execute_query_no_result($txt_query, array('s',$id_MProcedure));
	}
}

function Сохранить_Получателей_объекта($connection,$id_MProcedure,$o,$id_Asset)
{
	write_to_log('Сохранить_Получателей_объекта {');
	$connection->execute_query_no_result('delete from Asset_MUser where id_Asset=?',array('s',$id_Asset));
	if (isset($o->Круг_лиц_с_доступом->ToSelectedPro) 
		&& true==$o->Круг_лиц_с_доступом->ToSelectedPro
		&& isset($o->Круг_лиц_с_доступом->SelectedPro)
		&& 0!=count($o->Круг_лиц_с_доступом->SelectedPro))
	{
		$id_MUser_values= array();
		foreach ($o->Круг_лиц_с_доступом->SelectedPro as $av)
		{
			$id_MUser= get_id_MUser_for_id_Exposure_recipient_pro($av->id);
			if (null!=$id_MUser)
				$id_MUser_values[]= $id_MUser;
		}
		$id_MUser_values= implode(',',$id_MUser_values);

		$txt_query= "insert into Asset_MUser (id_Asset,id_MUser)
			select a.id_Asset, av.id_MUser 
			from MUser av 
			inner join Asset a on a.id_Asset=?
			where av.id_MUser in ($id_MUser_values)";
		$connection->execute_query($txt_query,array('s',$id_Asset));
		Направить_письма_получателям_объектов($connection,$id_MProcedure,$id_MUser_values);
	}
	write_to_log('Сохранить_Получателей_объекта }');
}

function Сохранить_объект($connection,$id_Manager,$id_MProcedure,$o)
{
	$Стадия_code= !(isset($o->Стадия)) ? 'a' : find_state_code($o->Стадия);

	$md5hash= asset_md5($o);

	$isAvailableToAll= (isset($o->Круг_лиц_с_доступом->ToAll) && true==$o->Круг_лиц_с_доступом->ToAll) ? 1 : 0;
	$ExtraFields= nice_json_encode($o);

	$txt_query= 'call StoreExposureAsset(?,?,?, ?,?,?, ?,?,?, ?,?,?);';
	$rows= $connection->execute_query($txt_query,array('sssssssssisb'
		,$id_MProcedure,$id_Manager,$o->ID_Object
		,$Стадия_code,$o->ОКАТО,$o->Категория
		,$o->Краткое_наименование,$o->Развёрнутое_описание,$o->Адрес
		,$isAvailableToAll,$md5hash,$ExtraFields));
	$count_rows= count($rows);
	if (1==$count_rows)
	{
		$id_Asset= $rows[0]->id_Asset;
		Сохранить_Получателей_объекта($connection,$id_MProcedure,$o,$id_Asset);
		Сохранить_КлассификациюЕФРСБ_объекта($connection,$id_MProcedure,$o,$id_Asset);
	}
}

function Сохранить_объекты($connection,$id_Manager,$id_MProcedure,$объекты)
{
	foreach ($объекты as $o)
		Сохранить_объект($connection,$id_Manager,$id_MProcedure,$o);
}

function Удалить_объекты($connection,$id_Manager,$id_MProcedure,$to_exposure)
{
	if (isset($to_exposure->ID_Object_ОбъектовКМ_для_удаления) 
		&& null!=$to_exposure->ID_Object_ОбъектовКМ_для_удаления 
		&& 0!=count($to_exposure->ID_Object_ОбъектовКМ_для_удаления))
	{
		$ID_Object_to_delete= array();
		foreach ($to_exposure->ID_Object_ОбъектовКМ_для_удаления as $id)
			$ID_Object_to_delete[]= '"'.$id.'"';
		$ID_Object_to_delete= implode(',',$ID_Object_to_delete);
		$txt_query= "delete from Asset where ID_Object in ($ID_Object_to_delete) and id_MProcedure=?";
		$connection->execute_query($txt_query,array('s',$id_MProcedure));
	}
}

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$connection= default_dbconnect();
$connection->begin_transaction();
try
{
	Сохранить_объекты($connection,$id_Manager,$id_MProcedure,$to_exposure->ОбъектыКМ);
	Удалить_объекты($connection,$id_Manager,$id_MProcedure,$to_exposure);
	$connection->commit();
}
catch (Exception $ex)
{
	$connection->rollback();
	throw $ex;
}

header('Content-Type: application/json');
echo '{ok:true}';

write_to_access_log_id('exposure/store/asset/finish',$id_Manager);
