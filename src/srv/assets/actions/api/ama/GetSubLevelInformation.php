<?  
require_once '../assets/helpers/db.php';

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/log.php';

require_once '../assets/libs/log/access_log.php';

require_once '../assets/actions/api/alib_api_auth.php';

function get_dadata_okato($suggestion_data)
{
	$okato=substr($suggestion_data->okato,0,2);
	switch ($okato)
	{
		case '11':
			switch ($suggestion_data->region)
			{
				case 'Ненецкий': return '11 100';
				default: return '11 000';
			}
		case '71':
			switch ($suggestion_data->region)
			{
				case 'Ямало-Ненецкий': return '71 140';
				case 'Ханты-Мансийский Автономный округ - Югра': return '71 100';
				default: return '71 000';
			}
		default:
			return "$okato 000";
	}
}

function request_dadata_region($query_string)
{
	global $dadata_API_KEY, $use_pretty_json_print;
	$API_KEY= $dadata_API_KEY;

	$params= array(
		'query'=>$query_string
		,'locations'=>array(array('country_iso_code'=>'RU'))
		,'count'=>2
	);
	$old_use_pretty_json_print= $use_pretty_json_print;
	$use_pretty_json_print= false;
	$post_body= nice_json_encode($params);
	$use_pretty_json_print= $old_use_pretty_json_print;

	$headers=array("Content-Type: application/json"
		,"Accept: application/json" 
		,"Authorization: Token $API_KEY" );
	$url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_FAILONERROR, 1);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

	$responce= curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if (200!=$httpcode)
	{
		write_to_log("receive code $httpcode for url \"$url\") with text:\"$responce\" API_KEY=$API_KEY");
		//exit_internal_server_error("can not get information from 'Dadata'");
		return '';
	}
	curl_close($curl);

	$ans= json_decode($responce);

	return (!isset($ans->suggestions[0]->data)) ? '' : get_dadata_okato($ans->suggestions[0]->data);
}

$auth_info= std_AuthByToken();

$date = !isset($_GET['date']) ? '' : $_GET['date'];
$addres = !isset($_GET['addres']) ? '' : $_GET['addres'];

$regions_by_address= array(
    'Республика Адыгея'=>'79 000',
    'Республика Башкортостан'=>'80 000',
    'Республика Бурятия'=>'81 000',
    'Республика Алтай'=>'84 000',
    'Республика Дагестан'=>'82 000',
    'Республика Ингушетия'=>'26 000',
    'Кабардино-Балкарская Республика'=>'83 000',
    'Республика Калмыкия'=>'85 000',
    'Карачаево-Черкесская Республика'=>'91 000',
    'Республика Карелия'=>'86 000',
    'Республика Коми'=>'87 000',
    'Республика Марий Эл'=>'88 000',
    'Республика Мордовия'=>'89 000',
    'Республика Саха (Якутия)'=>'98 000',
    'Республика Северная Осетия — Алания'=>'90 000',
    'Республика Татарстан'=>'92 000',
    'Республика Тыва'=>'93 000',
    'Удмуртская Республика'=>'94 000',
    'Республика Хакасия'=>'95 000',
    'Чеченская Республика'=>'96 000',
    'Чувашская Республика'=>'97 000',
    'Алтайский край'=>'01 000',
    'Краснодарский край'=>'03 000',
    'Красноярский край'=>'04 000',
    'Приморский край'=>'05 000',
    'Ставропольский край'=>'07 000',
    'Хабаровский край'=>'08 000',
    'Амурская область'=>'10 000',
    'Архангельская область'=>'11 000',
    'Астраханская область'=>'12 000',
    'Белгородская область'=>'14 000',
    'Брянская область'=>'15 000',
    'Владимирская область'=>'17 000',
    'Волгоградская область'=>'18 000',
    'Вологодская область'=>'19 000',
    'Воронежская область'=>'20 000',
    'Ивановская область'=>'24 000',
    'Иркутская область'=>'25 000',
    'Калининградская область'=>'27 000',
    'Калужская область'=>'29 000',
    'Камчатский край'=>'30 000',
    'Кемеровская область'=>'32 000',
    'Кировская область'=>'33 000',
    'Костромская область'=>'34 000',
    'Курганская область'=>'37 000',
    'Курская область'=>'38 000',
    'Ленинградская область'=>'41 000',
    'Липецкая область'=>'42 000',
    'Магаданская область'=>'44 000',
    'Московская область'=>'46 000',
    'Мурманская область'=>'47 000',
    'Нижегородская область'=>'22 000',
    'Новгородская область'=>'49 000',
    'Новосибирская область'=>'50 000',
    'Омская область'=>'52 000',
    'Оренбургская область'=>'53 000',
    'Орловская область'=>'54 000',
    'Пензенская область'=>'56 000',
    'Пермский край'=>'57 000',
    'Псковская область'=>'58 000',
    'Ростовская область'=>'60 000',
    'Рязанская область'=>'61 000',
    'Самарская область'=>'36 000',
    'Саратовская область'=>'63 000',
    'Сахалинская область'=>'64 000',
    'Свердловская область'=>'65 000',
    'Смоленская область'=>'66 000',
    'Тамбовская область'=>'68 000',
    'Тверская область'=>'28 000',
    'Томская область'=>'69 000',
    'Тульская область'=>'70 000',
    'Тюменская область'=>'71 000',
    'Ульяновская область'=>'73 000',
    'Челябинская область'=>'75 000',
    'Забайкальский край'=>'76 000',
    'Ярославская область'=>'78 000',
    'Москва'=>'45 000',
    'Санкт-Петербург'=>'40 000',
    'Еврейская автономная область'=>'99 000',
    'Ненецкий автономный округ'=>'11 100',
    'Ханты-Мансийский автономный округ - Югра Автономный округ'=>'71 100',
    'Чукотский автономный округ'=>'77 000',
    'Ямало-Ненецкий автономный округ'=>'71 140',
    'Республика Крым'=>'35 000',
    'Севастополь'=>'67 000',
    'Байконур'=>'55 000'
);

$region = isset($regions_by_address[$addres]) && ''!=$regions_by_address[$addres] 
	? $regions_by_address[$addres] :  request_dadata_region($addres);

$txt_query_prefix= " select
	  sub.id_Region
	, sub.StartDate
	, sub.Sum_Common Common
	, sub.Sum_Employable Employable
	, sub.Sum_Infant Infant
	, sub.Sum_Pensioner Pensioner
	, sub.Reglament_Title Title
	, sub.Reglament_Date Date
	, sub.Reglament_Url Url
	, sub.id_Sub_level Id
	, r.OKATO
from sub_level sub
inner join region r on sub.id_Region = r.id_Region
";
$order="order by OKATO desc, sub.StartDate desc , sub.id_Contract desc;";

if (!isset($auth_info->id_Owner) || !isset($auth_info->Who))
{
	$from_where= "
	where (r.OKATO=? or '00 000'=r.OKATO) and sub.StartDate <= ? ";
	$query = $txt_query_prefix . $from_where . $order;
	$rows = execute_query($query,array('ss',$region,$date));
}
else
{
	if ('c'==$auth_info->Who)
	{
		$id_Contract = intval($auth_info->id_Owner);
		$from_where= "where (sub.id_Contract is null or sub.id_Contract=?) and (r.OKATO=? or '00 000'=r.OKATO) and sub.StartDate <= ? 
		";
		$parametrs=array('sss',$id_Contract, $region,$date);
	}
	else if ('m'==$auth_info->Who)
	{
		$id_Manager = intval($auth_info->id_Owner);
		$from_where= "
		left join Contract c on sub.id_Contract = c.id_Contract
		left join Manager m on c.id_Contract = m.id_Contract
		where (sub.id_Contract is null or m.id_Manager=?) and (r.OKATO=? or '00 000'=r.OKATO) and sub.StartDate <= ? 
		";
		$parametrs=array('sss',$id_Manager, $region,$date);
	}
	$query = $txt_query_prefix . $from_where . $order;
	$rows=execute_query($query,$parametrs);
}

$result= (object)array
(
	'Common'=>null
	,'Infant'=>null
	,'Employable'=>null
	,'Pensioner'=>null

	//,'Reglaments'=>array()

	,'Region'=>$region
);
$Ids=array(); //temporary
function process_field($field_Name, $row, $result, &$Ids)
{
	$result->$field_Name= $row->$field_Name;
	if (!in_array($row->Id,$Ids))
	{
		if (''==$result->Reglaments[0]['date'])
			$result->Reglaments[0]['date'] = $row->Date ;
		$result->Reglaments[0]['title'].= (''==$result->Reglaments[0]['title']) ? $row->Title : (', '.$row->Title);
		$result->Reglaments[0]['url'].= (0==count($Ids)) ? $row->Id : (','.$row->Id);
		$Ids[]= $row->Id;
	}
}
function try_process_field($field_Name, $row, $result, &$Ids)
{
	if (null!=$row->$field_Name && null==$result->$field_Name)
	{
		process_field($field_Name, $row, $result, $Ids);
	}
}

function finish_process_fields($result)
{
	return null!=$result->Common && null!=$result->Employable && null!=$result->Infant && null!=$result->Pensioner;
}

$result->Reglaments[]= array(
	'title'=>''
	,'date'=>''
	,'url'=>$base_apps_url.'sl.php?date='.$date.'&region='.array_search($region,$regions_by_address).'&id='
); //temporary
foreach ($rows as $row)
{
	if ($region==$row->OKATO)
	{
		try_process_field('Common', $row, $result,$Ids);
		try_process_field('Employable', $row, $result,$Ids);
		try_process_field('Infant', $row, $result,$Ids);
		try_process_field('Pensioner', $row, $result,$Ids);

		if (finish_process_fields($result))
			break;
	}
}

function process_field_rf($field_Name, $row, $result, &$Ids)
{
	$result->$field_Name= $row->Employable;
	if (!in_array($row->Id,$Ids))
	{
		if (''==$result->Reglaments[0]['date'])
			$result->Reglaments[0]['date'] = $row->Date ;
		$result->Reglaments[0]['title'].= (''==$result->Reglaments[0]['title']) ? $row->Title : (', '.$row->Title);
		$result->Reglaments[0]['url'].= (0==count($Ids)) ? $row->Id : (','.$row->Id);
		$Ids[]= $row->Id;
	}
}
function try_process_field_rf($field_Name, $row, $result, &$Ids)
{
	global $region;
	if (null!=$row->Employable && (null==$result->$field_Name || $row->Employable > $result->$field_Name) && !isset($result->{$field_Name.'_rf'}))
	{
		process_field_rf($field_Name, $row, $result, $Ids);
		$result->{$field_Name.'_rf'}= true;
	}
}
function finish_process_fields_rf($result)
{
	return isset($result->Common_rf) && isset($result->Employable_rf) && isset($result->Infant_rf) && isset($result->Pensioner_rf);
}
foreach ($rows as $row)
{
	if ('00 000'==$row->OKATO)
	{
		try_process_field_rf('Common', $row, $result,$Ids);
		try_process_field_rf('Employable', $row, $result,$Ids);
		try_process_field_rf('Infant', $row, $result,$Ids);
		try_process_field_rf('Pensioner', $row, $result,$Ids);

		if (finish_process_fields($result))
			break;
	}
}

if (isset($result->Common_rf))
	unset($result->Common_rf);
if (isset($result->Employable_rf))
	unset($result->Employable_rf);
if (isset($result->Infant_rf))
	unset($result->Infant_rf);
if (isset($result->Pensioner_rf))
	unset($result->Pensioner_rf);

echo(nice_json_encode($result));

write_to_access_log_id('sublevel-info',$auth_info->id_Owner);
