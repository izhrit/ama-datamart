<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/api/alib_api_auth.php';

$auth_info= std_AuthByToken();

global $exposure_recipiet_pro_list;

header('Content-Type: application/json');
echo nice_json_encode($exposure_recipiet_pro_list);