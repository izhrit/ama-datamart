<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/api/alib_api_auth.php';

$auth_info= std_AuthByToken();

$year_greater_than= !isset($_GET['year-greater-than']) ? 0 : $_GET['year-greater-than'];
$revision_greater_than= !isset($_GET['revision-greater-than']) ? 0 : $_GET['revision-greater-than'];

$txt_query= "select Year, Revision, uncompress(Days) Days from wcalendar c inner join region r on r.id_Region=c.id_Region where Year > ? and Revision > ? order by Year, Revision;"; //  and OKATO='00 000' ..
$rows= execute_query($txt_query,array('ii', $year_greater_than, $revision_greater_than));

$years= array();
foreach ($rows as $row)
{
	$days= json_decode($row->Days);
	$region= $days->Регион;
	$days->Регион= null;
	unset($days->Регион);
	$year= (object)array(
		'Год'=>$row->Year
		,'Регион'=>array('ОКАТО'=>$region[0],'Наименование'=>$region[1])
		,'Revision'=>$row->Revision
		,'Дни_календаря'=>$days
	);
	$years[]= $year;
}

$txt_result= nice_json_encode($years);
$txt_result= str_replace('",
					"рабочий": "',
	'", "рабочий": "',$txt_result);
$txt_result= str_replace('{
					"день": "',
	'{ "день": "',$txt_result);
$txt_result= str_replace('"
				},
				{ "день": "',
	'" },
				{ "день": "',$txt_result);
$txt_result= str_replace('"
				}
			],
			"',
	'" }
			],
			"',$txt_result);
$txt_result= str_replace('"
				}
			]
		}',
	'" }
			]
		}',$txt_result);

header('Content-Type: application/json');
echo $txt_result;