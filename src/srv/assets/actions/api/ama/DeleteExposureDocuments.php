<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/api/alib_api_auth.php';
require_once '../assets/actions/backend/constants/AssetState.etalon.php';

$auth_info= std_AuthByToken_manager();
$id_Manager = intval($auth_info->id_Owner);

$_POST_bytes= file_get_contents("php://input");

$id_MProcedure= CheckMandatoryGET('id_MProcedure');
$ID_Object= CheckMandatoryGET('ID_Object');

$filename_документов= json_decode($_POST_bytes);

$txt_query= 'delete aa 
from AssetAttachment aa
inner join Asset a on a.id_Asset=aa.id_Asset
where a.ID_Object=? and aa.FileName=? and a.id_MProcedure=?';

foreach ($filename_документов as $fname)
{
	$affected_rows= execute_query_get_affected_rows($txt_query,array('sss',$ID_Object,$fname,$id_MProcedure));
	$count_affected_rows= count($affected_rows);
	if (1!=$count_affected_rows)
		exit_bad_request("affected $count_affected_rows when insert AssetAttachment");
}

header('Content-Type: application/json');
echo '{ok:true}';