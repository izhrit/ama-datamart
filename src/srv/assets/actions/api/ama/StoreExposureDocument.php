<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/api/alib_api_auth.php';
require_once '../assets/actions/backend/constants/AssetState.etalon.php';
require_once '../assets/libs/log/access_log.php';

$auth_info= std_AuthByToken_manager();
$id_Manager= intval($auth_info->id_Owner);

$_POST_bytes= file_get_contents("php://input");

$id_MProcedure= CheckMandatoryGET('id_MProcedure');
$ID_Object= CheckMandatoryGET('ID_Object');
$FileName= CheckMandatoryGET('FileName');

write_to_access_log_fields('exposure/store/document/start'
	,array(
		'Id'=>$id_Manager
		,'Details'=>nice_json_encode(array(
			'ID_Object'=>$ID_Object
			,'FileName'=>$FileName
		))
	)
);

$txt_query= 'call StoreExposureDocument(?,?,?,?,?);';
execute_query($txt_query,array('ssssb',$id_MProcedure,$id_Manager,$ID_Object,$FileName,$_POST_bytes));

header('Content-Type: application/json');
echo '{ok:true}';

write_to_access_log_id('exposure/store/document/finish',$id_Manager);