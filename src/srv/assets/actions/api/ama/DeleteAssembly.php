<?  
require_once '../assets/helpers/db.php';

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/log.php';

require_once '../assets/libs/log/access_log.php';

require_once '../assets/actions/api/alib_api_auth.php';

$auth_info= std_AuthByToken_manager();
$id_Manager = intval($auth_info->id_Owner);

$id_Meeting= CheckMandatoryGET('id');

$txt_query= "delete m 
	from Meeting m 
	inner join MProcedure mp on m.id_MProcedure=mp.id_MProcedure
	where m.State='a' && m.id_Meeting=? && mp.id_Manager=?;";
$affected_rows= execute_query_get_affected_rows($txt_query,array('ii',$id_Meeting,$id_Manager));

if (1==$affected_rows)
{
	echo '{ "ok": true }';
}
else if (0!=$affected_rows)
{
	exit_bad_request("deleted $affected_rows Meetings width id_Meeting=$id_Meeting!");
}
else
{
	$txt_query= "select m.State, mp.id_Manager from Meeting m inner join MProcedure mp on m.id_MProcedure=mp.id_MProcedure where id_Meeting=?;";
	$rows= execute_query($txt_query,array('s',$id_Meeting));
	$count_rows= count($rows);
	$message= 'Не удалось удалить информацию о заседании Комитета Кредиторов с Витрины данных ПАУ';
	$id_Meeting_can_be_forgotten= false;
	if (0==$count_rows)
	{
		$message= 'На витрине данных ПАУ информация о заседании Комитета Кредиторов отсуствует.
Возможно, информация о заседании была удалена из кабинета на Витрине данных.';
		$id_Meeting_can_be_forgotten= true;
	}
	else if (1!=$count_rows)
	{
		exit_bad_request("found $affected_rows Meetings width id_Meeting=$id_Meeting!");
	}
	else
	{
		$row= $rows[0];
		if ($id_Manager!=$row->id_Manager)
		{
			$message= 'На витрине данных ПАУ информация АУ о заседании Комитета Кредиторов отсуствует.
Возможно, информация о заседании была удалена из кабинета на Витрине данных.';
			$id_Meeting_can_be_forgotten= true;
		}
		else if ('a'==$row->State)
		{
			exit_bad_request("can not delete Meeting id_Meeting=$id_Meeting!");
		}
		else if ('a'!=$row->State)
		{
			require_once '../assets/actions/backend/constants/alib_constants.php';
			require_once '../assets/actions/backend/constants/Meeting_State.etalon.php';
			global $Meeting_State_descriptions;
			$d= FindDescription($Meeting_State_descriptions,'code',$row->State);
			$meeting_state= $d['descr'];
			$message= "Заседание Комитета Кредиторов на Витрине данных ПАУ находится в состоянии '$meeting_state'. 
О засадении уже уведомлены участники.
К сожалению, такое заседание не может быть удалено в соответствии с Положением об использовании ПЭП комитетами кредиторов.
Можно приостановить и отменить заседание, но информация о нём будет храниться на витрине данных положенное время.";
		}
	}
	echo nice_json_encode(array('ok'=>false,'message'=>$message,'id_Meeting_can_be_forgotten'=>$id_Meeting_can_be_forgotten));
}


