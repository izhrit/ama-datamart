<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/api/alib_api_auth.php';
require_once '../assets/actions/backend/constants/AssetState.etalon.php';

$auth_info= std_AuthByToken_manager();
$id_Manager = intval($auth_info->id_Owner);

$after_ID_Object= (!isset($_GET['after_ID_Object'])) ? null : $_GET['after_ID_Object'];
$max_portion_size= 100;
if (isset($_GET['max_portion_size']))
{
	if (!is_numeric($_GET['max_portion_size']))
		exit_bad_request('bad numeric _GET[max_portion_size]');
	$max_portion_size= intval($_GET['max_portion_size']);
	if ($max_portion_size>100)
		$max_portion_size= 100;
}

require_once '../assets/libs/procedure/prepare_proc.php';

$_POST_text= file_get_contents("php://input");

$pd= json_decode($_POST_text);

$Должник= $pd->Должник;
$Процедура= $pd->Процедура;

$id_MProcedure= safe_prep_MProcedure(default_dbconnect(),$Должник,$id_Manager,$Процедура->Тип, $Процедура->Номер_дела,'При попытке экспозиции');

$rows= execute_query('select count(*) c from Asset where id_MProcedure=?;',array('s',$id_MProcedure));
$count_rows= count($rows);
if (1!=$count_rows)
	exit_bad_request("got $count_rows rows when count Assets");

$total_count= $rows[0]->c;

$params= array('s',$id_MProcedure);

$txt_query= 'select 
	a.ID_Object
	, a.md5hash md5
	, (select group_concat(concat(f.FileSize,";",f.md5hash,";",f.FileName) separator ":") from AssetAttachment f where f.id_Asset=a.id_Asset) files
from Asset a
where a.id_MProcedure=?
';

if (null!=$after_ID_Object)
{
	$txt_query.= ' and a.ID_Object>? ';
	$params[0].='s';
	$params[]= $after_ID_Object;
}

$txt_query.="order by a.ID_Object
limit $max_portion_size;";
$rows= execute_query($txt_query,$params);

$portion= array();
foreach ($rows as $row)
{
	$portion[]= $row;
	if (isset($row->files) && null!=$row->files && ''!=$row->files)
	{
		$files= explode(':',$row->files);
		$count_files= count($files);
		if (0!=$count_files)
		{
			$row->Документы= array();
			foreach ($files as $file)
			{
				$p= explode(';',$file);
				$row->Документы[]= array('filename'=>$p[2],'md5'=>$p[1],'size'=>$p[0]);
			}
		}
	}
	unset($row->files);
}

$res= array(
	'after_ID_Object'=>$after_ID_Object
	,'total_count'=>$total_count
	,'portion'=>$portion
	,'id_MProcedure'=>$id_MProcedure
);

header('Content-Type: application/json');
echo nice_json_encode($res);