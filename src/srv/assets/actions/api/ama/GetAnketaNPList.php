<?php
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/actions/api/alib_api_auth.php';

$auth_info= std_AuthByToken();

CheckMandatoryGET('q');

switch ($auth_info->Who)
{
	case 'c':
		$check_id_Owner= ' inner join Manager m on m.id_Manager=mu.id_Manager where m.id_Contract';
		break;
	case 'm':
		$check_id_Owner= ' where mu.id_Manager';
		break;
	default:
		exit_unauthorized('authorized user can not use this method!');
}

$q= $_GET['q'].'%';

$txt_query= "select
	md.id_MData
	, concat(anp.lastName, ' ', anp.firstName, ' ', anp.middleName) Name
	, anp.SNILS
	, anp.INN
	, anp.Email
from MData md
inner join ManagerUser mu on md.id_ManagerUser=mu.id_ManagerUser
inner join AnketaNP anp on anp.id_MData=md.id_MData
$check_id_Owner=? && 'a'=md.MData_Type 
&& (anp.lastName like ? or anp.firstName like ? or anp.Email like ? or anp.INN like ? or anp.SNILS like ?)
order by anp.lastName, anp.Email
limit 20;";

$rows= execute_query($txt_query, array('ssssss',$auth_info->id_Owner,$q,$q,$q,$q,$q));

echo nice_json_encode($rows);
