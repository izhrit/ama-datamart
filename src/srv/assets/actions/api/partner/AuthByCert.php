<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';
require_once '../assets/helpers/validate.php';

function GetTokenToSign()
{
	$txt_in_data= file_get_contents('php://input');
	$запрос_доступа= json_decode($txt_in_data);

	if (null==$запрос_доступа)
		exit_bad_request("can not parse input: $txt_in_data");
	write_to_log_named('запрос_доступа',$запрос_доступа);

	if (!isset($запрос_доступа->АУ->ИНН))
		exit_bad_request("skipped manager inn in input: $txt_in_data");

	$ИНН= $запрос_доступа->АУ->ИНН;
	write_to_log('ИНН:'.$ИНН);

	$rows= execute_query(
		'select id_Manager, ArbitrManagerID, lastName, firstName, middleName, INN, efrsbNumber from Manager where INN=?',
		array('s',$ИНН));
	$count_rows= count($rows);

	if (1!=$count_rows)
	{
		exit_not_found("found $count_rows managers with INN={$ИНН}");
	}
	else
	{
		$row= $rows[0];
		write_to_log_named('найден АУ',$row);
		global $auth_token_options;
		if (isset($_GET['open']) && isset($auth_token_options->enable_open) && !$auth_token_options->enable_open)
		{
			$отметка_сайта= 1;
		}
		else
		{
			$encrypted_token_fields= sync_encrypt(json_encode($запрос_доступа),$auth_token_options);
			$encrypted_token_fields_md5= md5($encrypted_token_fields);
			$отметка_сайта= $encrypted_token_fields_md5;
		}
		$на_витрину= array('Отметка_сайта'=>$отметка_сайта);
		$txt_query= 'insert into ManagerCertAuth set id_Manager=?, Body=compress(?), TimeStarted=now();';
		$id_ManagerCertAuth= execute_query_get_last_insert_id($txt_query,array('ss',$row->id_Manager,json_encode($на_витрину)));
		$на_витрину['id_ManagerCertAuth']= $id_ManagerCertAuth;
		write_to_log_named('return',$на_витрину);
		echo nice_json_encode($на_витрину);
	}
}

function AuthByTokenSignature()
{
	$txt_in_data= file_get_contents('php://input');
	$информация_с_подписью= json_decode($txt_in_data);

	if (null==$информация_с_подписью)
		exit_bad_request("can not parse input: $txt_in_data");

	write_to_log_named('на входе',$информация_с_подписью);
	$id_ManagerCertAuth= $информация_с_подписью->на_витрину->id_ManagerCertAuth;

	$txt_query= 'select uncompress(Body) body from ManagerCertAuth where id_ManagerCertAuth=?;';
	$rows= execute_query($txt_query,array('s',$id_ManagerCertAuth));
	$count_rows= count($rows);

	if (1!=$count_rows)
	{
		exit_bad_request("found $count_rows ManagerCertAuth with id_ManagerCertAuth=$id_ManagerCertAuth");
	}
	else
	{
		$fields= json_decode($rows[0]->body);
		$res= array('id_ManagerCertAuth'=>$id_ManagerCertAuth,'auth'=>$fields->Отметка_сайта);

		$txt_query= 'update ManagerCertAuth set Auth=? where id_ManagerCertAuth=?;';
		execute_query_no_result($txt_query,array('ss',$fields->Отметка_сайта,$id_ManagerCertAuth));

		write_to_log_named('return',$res);
		echo nice_json_encode($res);
	}
}

function Logout()
{
	$id_ManagerCertAuth= CheckMandatoryGET_id('id');

	$txt_query= 'delete from ManagerCertAuth where id_ManagerCertAuth=?;';
	$affected_rows= execute_query_get_affected_rows($txt_query,array('s',$id_ManagerCertAuth));
	if (1!=$affected_rows)
		exit_bad_request("$affected_rows rows affected on delete from ManagerCertAuth where id_ManagerCertAuth=$id_ManagerCertAuth");
}

CheckMandatoryGET_execute_action_variant('cmd',array(
	 'get-token-to-sign'=>       function(){GetTokenToSign();}
	,'auth-by-token-signature'=> function(){AuthByTokenSignature();}
	,'logout'=>                  function(){Logout();}
));
