<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/actions/api/alib_api_auth.php';

$access_token= null;

$auth_info= std_AuthByToken_contract();
if (null==$access_token)
	throw new Exception("undefined access_token after auth!");

$message= '';

if (!isset($_GET['debtor']))
{
	$message.= "\r\n skipped URL parameter \"debtor\"";
}
else
{
	$debtor= $_GET['debtor'];
	if (!isset($debtor['name']))
	{
		$message.= "\r\n skipped URL parameter \"debtor[name]\"";
	}
	else
	{
		$debtor['name']= trim($debtor['name']);
		if (''==$debtor['name'])
			$message.= "\r\n empty URL parameter \"debtor[name]\"";
	}
	if (!isset($debtor['category']))
	{
		$message.= "\r\n skipped URL parameter \"debtor[category]\"";
	}
	else
	{
		$debtor_category= $debtor['category'];
		switch ($debtor_category)
		{
			case 'citizen': $debtor_category= 'n'; break;
			case 'organization': $debtor_category= 'l'; break;
			default: 
				$message.= "\r\n unknown value \"$debtor_category\" for URL parameter \"debtor[category]\" (should be \"citizen\" or \"organization\")";
		}
	}
}

if (!isset($_GET['court']))
{
	$message.= "\r\n skipped URL parameter \"court\"";
}
else
{
	$court= $_GET['court'];
	if (!isset($court['name']))
	{
		$message.= "\r\n skipped URL parameter \"court[name]\"";
	}
	else
	{
		$court['name']= trim($court['name']);
		if (''==$court['name'])
			$message.= "\r\n empty URL parameter \"court[name]\"";
	}
}


if (''!=$message)
{
	echo nice_json_encode(array('ok'=>false,'message'=>$message));
	exit;
}

$court= $_GET['court'];

if (isset($_GET['manager']) && isset($_GET['manager']['inn']))
{
	$manager_inn= $_GET['manager']['inn'];
	$id_Manager= '(select id_Manager from Manager where Manager.INN=?)';
}
else
{
	$manager_inn= null;
	$id_Manager= '?';
}

$txt_query= "insert into ProcedureStart
set 
	 id_Contract=?
	,id_Court= (select id_Court from Court where Court.Name=?)
	,id_Manager= $id_Manager
	,DebtorName=?
	,DebtorINN=?
	,DebtorSNILS=?
	,DebtorOGRN=?
	,DebtorCategory=?
	,DateOfApplication=?
	,TimeOfCreate=now()
;";

$affected_rows= execute_query_get_affected_rows($txt_query,array('sssssssss',
	$auth_info->id_Owner
	,$court['name']
	,$manager_inn
	,$debtor['name']
	,!isset($debtor['inn']) ? null : $debtor['inn']
	,!isset($debtor['snils']) ? null : $debtor['snils']
	,!isset($debtor['ogrn']) ? null : $debtor['ogrn']
	,$debtor_category
	,!isset($debtor['date']) ? null : $_GET['date']
));

if (1!=$affected_rows)
	exit_internal_server_error("can not insert ProcedureStart ($affected_rows affected rows)");

echo json_encode(array('ok'=>true));


