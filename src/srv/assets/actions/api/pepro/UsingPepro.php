<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/realip.php';

$contract= !isset($_GET['contract']) ? null : $_GET['contract'];
$license= !isset($_GET['license']) ? null : $_GET['license'];
$section= !isset($_GET['section']) ? null : $_GET['section'];
$ip= getRealIPAddr();

$txt_query= "insert into PeproUsing set 
ContractNumber=?, LicenseToken=?, IP=?, Section=?, UsingTime=now();";

$params= array('ssss',
$contract,        $license,       $ip,  $section);

$affected_rows= execute_query_get_affected_rows($txt_query, $params);

if (1!=$affected_rows)
{
	write_to_log("can not insert into PeproUsing (affected_rows=$affected_rows), GET:");
	write_to_log($_GET);
	exit_internal_server_error("can not insert into PeproUsing (affected_rows=$affected_rows)");
}
