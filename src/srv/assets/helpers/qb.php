<?

class query_builder
{
	private $params;
	private $comma_params_count;
	private $txt_query;

	public function __construct($txt_start_query)
	{
		$this->txt_query= $txt_start_query;
		$this->params= array('');
		$this->comma_params_count= 0;
	}

	public function parametrized($parametrized_text,$value,$type= 's')
	{
		$this->txt_query.= $parametrized_text;
		$this->params[0].= $type;
		$this->params[]= $value;
	}

	public function comma_par($name,$value,$type= 's')
	{
		$this->txt_query.= (0==$this->comma_params_count++) ? "\r\n  $name=?" : "\r\n ,$name=?";
		$this->params[0].= $type;
		if ($value ==='null')
			$value = null;
		$this->params[]= $value;
	}

	public function safe_comma_field_par($name,$obj,$field_name,$type= 's')
	{
		if (isset($obj->$field_name))
			$this->comma_par($name,$obj->$field_name,$type);
	}

	public function comma_text($txt)
	{
		$this->txt_query.= (0==$this->comma_params_count++) ? "\r\n  $txt" : "\r\n ,$txt";
	}

	public function comma_select2($name,$value)
	{
		$this->comma_par($name,(null==$value || 'null'==$value) 
			? null : (isset($value->id) ? $value->id : $value['id']));
	}

	public function comma_DateTime($name,$value)
	{
		$this->comma_par($name,(null==$value || 'null'==$value) ? null : date_format($value,'Y-m-d H:i:s'));
	}

	public function comma_date_time($name,$value,$mask_from,$mask_to)
	{
		if (''==$value||'null'==$value)
		{
			$this->comma_par($name,null);
		}
		else
		{
			$date_time_value= date_create_from_format($mask_from,$value);
			$sql_date_time_value= date_format($date_time_value,$mask_to);
			$this->comma_par($name,$sql_date_time_value);
		}
	}

	public function comma_ru_date($name,$value)
	{
		$this->comma_date_time($name,$value,'d.m.Y','Y-m-d');
	}

	public function comma_ru_legal_date_time($name,$value)
	{
		$this->comma_date_time($name,$value,'d.m.Y H:i','Y-m-d H:i:00');
	}

	public function comma_ru_date_time($name,$value)
	{
		$this->comma_date_time($name,$value,'d.m.Y H:i:s','Y-m-d H:i:s');
	}

	private function trace($title= null)
	{
		if (null!=$title)
			write_to_log($title);
		write_to_log_named('built query',$this->txt_query);
		write_to_log_named('built params',$this->params);
	}

	public function execute_query_no_result()
	{
		$this->trace('execute_query_no_result');
		execute_query_no_result($this->txt_query,$this->params);
	}

	public function execute_query_get_last_insert_id()
	{
		$this->trace('execute_query_get_last_insert_id');
		return execute_query_get_last_insert_id($this->txt_query,$this->params);
	}

	public function execute_query_get_affected_rows($connection= null)
	{
		$this->trace('execute_query_get_affected_rows');
		return (null==$connection) 
		? execute_query_get_affected_rows($this->txt_query,$this->params)
		: $connection->execute_query_get_affected_rows($this->txt_query,$this->params);
	}

	public function simple_condition($condition)
	{
		$this->txt_query.= $condition;
	}
}
