<?

function ErrorHandler_throw_recoverable_errors($errno, $errstr, $errfile, $errline)
{
	write_to_log('ErrorHandler_throw_recoverable_errors');
	if ( E_RECOVERABLE_ERROR===$errno )
	{
		throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
		// return true;
	}
	return false;
}

function fatal_handler()
{
	$error = error_get_last();
	if ( NULL !== $error )
	{
		$errno=   $error["type"];
		$errfile= $error["file"];
		$errline= $error["line"];
		$errstr=  $error["message"];
		write_to_log("got fatal error:
	message:$errstr
	file:$errfile
	line:$errline, errno:$errno");
		if (E_ERROR==$errno)
			header("HTTP/1.1 500 Internal Server Error");
	}
}

set_error_handler('ErrorHandler_throw_recoverable_errors');
register_shutdown_function('fatal_handler');