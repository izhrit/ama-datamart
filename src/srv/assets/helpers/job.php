<?php

function job_time($show_timezone = false)
{
	global $current_date_time;
	if (null==$current_date_time)
	{
		return date_format(date_create(), $show_timezone ? 'Y-m-d\TH:i:sP' : 'Y-m-d\TH:i:s');
	}
	else
	{
		$current_date_time= date_add($current_date_time, date_interval_create_from_date_string('1 second'));
		return date_format($current_date_time, $show_timezone ? 'Y-m-d\TH:i:sP' : 'Y-m-d\TH:i:s');
	}
}

function echo_job_exception($ex)
{
	$ex_class= get_class($ex);
	$ex_Message= $ex->getMessage();
	$txt= "\r\nException occurred: $ex_class - $ex_Message\r\n";
	echo $txt;
	return $txt;
}

function execute_job_part($exec,$title,$job_part_state= null)
{
	$res= null;
	echo "-------------------------------------------------------------------------------- { \r\n";
	echo job_time(). " $title ($exec)\r\n";
	if (null!=$job_part_state)
		echo "    ExtraStateFields:$job_part_state\r\n";
	if (!is_callable($exec))
	{
		echo "\r\n\r\n\r\n";
		echo "    ATTENTION   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\r\n";
		echo "\r\n\r\n\r\n";
		echo "exec is not callable:\r\n";
		print_r($exec);
		echo "\r\n\r\n\r\n";
		echo "\r\n\r\n\r\n";
	}
	else
	{
		try
		{
			$res= null==$job_part_state ? $exec() : $exec($job_part_state);
		}
		catch (Exception $ex)
		{
			$ex_txt= echo_job_exception($ex);
			$res= (object)array('status'=>1000,'results'=>$ex_txt);
		}
	}
	echo job_time(). " done ($title)\r\n";
	if (is_string($res))
	{
		echo "$res\r\n";
	}
	else
	{
		if (isset($res->results))
			echo "{$res->results}\r\n";
		if (isset($res->state))
			echo "new ExtraStateFields:{$res->state}\r\n";
	}
	echo "-------------------------------------------------------------------------------- } \r\n";
	return $res;
}

function execute_job_parts($parts)
{
	foreach ($parts as $part)
	{
		execute_job_part($part['exec'],$part['title']);
	}
}

function execute_job_parts_with_db_log($parts,$job_db_logger)
{
	foreach ($parts as $part)
	{
		$exec= $part['exec'];
		$title= $part['title'];

		$job_part_state= $job_db_logger->log_job_part_start($exec,$title);

		$res= execute_job_part($exec,$title,$job_part_state);

		$status= 0;
		if (isset($res->status))
			$status= $res->status;
		$results= null;
		if (isset($res->results))
		{
			$results= $res->results;
		}
		else if (isset($res))
		{
			$results= $res;
		}
		$new_job_part_state= null;
		if (isset($res->state))
			$new_job_part_state= $res->state;

		$job_db_logger->log_job_part_finish($status,$results,$new_job_part_state);
	}
}

function execute_job_parts_locked_by_pid_file_with_db_log($parts,$pid_file_path,$job_db_logger)
{
	if (file_exists($pid_file_path))
	{
		echo "\r\n\r\n";
		echo job_time(). " pid file exists! ($pid_file_path)";
		echo "\r\n\r\n";
		exit();
	}

	file_put_contents($pid_file_path,date_format(date_create(),'Y-m-d\TH:i:s'));

	try
	{
		echo "******************************************************************************** { \r\n";
		execute_job_parts_with_db_log($parts,$job_db_logger);
		echo "******************************************************************************** } \r\n";
	}
	catch (Exception $ex)
	{
		unlink($pid_file_path);
		throw $ex;
	}
	unlink($pid_file_path);

	return 0;
}


function execute_job_parts_locked_by_pid_file($parts,$pid_file_path)
{
	if (file_exists($pid_file_path))
	{
		echo "\r\n\r\n";
		echo job_time(). " pid file exists! ($pid_file_path)";
		echo "\r\n\r\n";
		exit();
	}

	file_put_contents($pid_file_path,date_format(date_create(),'Y-m-d\TH:i:s'));

	try
	{
		echo "******************************************************************************** { \r\n";
		execute_job_parts($parts);
		echo "******************************************************************************** } \r\n";
	}
	catch (Exception $ex)
	{
		unlink($pid_file_path);
		throw $ex;
	}
	unlink($pid_file_path);

	return 0;
}

class Job_db_logger
{
	public function init_site($site_name= null,$site_desciption= null)
	{
		global $job_params;
		if (isset($job_params->service))
		{
			$service= $job_params->service;
			if (null==$site_name && isset($service->name))
				$site_name= $service->name;
			if (null==$site_desciption && isset($service->description))
				$site_desciption= $service->description;
		}
		if (null==$site_name)
			$site_name= '�������� �� ������ � �������!';
		if (null==$site_desciption)
			$site_desciption= '�������� �� ������ � �������!';
		$this->site= (object)array('title'=>$site_name,'description'=>$site_desciption);
	}

	public function __construct()
	{
		$this->init_site();
	}

	public function log_job_start()
	{
		$site= $this->site;
		$rows= execute_query('select id_JobSite, Description from JobSite where Name=?',array('s',$site->title));
		$count_rows= count($rows);
		if (0==$count_rows)
		{
			$this->id_JobSite= execute_query_get_last_insert_id('insert into JobSite set Name=?, Description=?;',array('ss',
					$site->title,$site->description));
		}
		else
		{
			$row= $rows[0];
			$this->id_JobSite= $row->id_JobSite;
			if ($site->description!=$row->Description)
			{
				echo job_time() . " change db Site Description\r\n from \"{$row->Description}\"\r\n   to \"{$site->description}\"\r\n";
				execute_query_no_result('update JobSite set Description=? where id_JobSite=?;',array('ss',
						$site->description,$row->id_JobSite));
			}
		}

		$job= $this->job;
		$rows= execute_query('select id_Job, Description, MaxAgeMinutes from Job where Name=? and id_JobSite=?',array('ss',$job->title,$this->id_JobSite));
		$count_rows= count($rows);
		if (0==$count_rows)
		{
			$this->id_Job= execute_query_get_last_insert_id('insert into Job set id_JobSite=?, Name=?, Description=?, MaxAgeMinutes= ?;',array('sssi',
					$this->id_JobSite,$job->title,$job->description,$job->max_age_minutes));
		}
		else
		{
			$row= $rows[0];
			$this->id_Job= $row->id_Job;
			if ($job->description!=$row->Description || $job->max_age_minutes!=$row->MaxAgeMinutes)
			{
				echo job_time() . " change db Job Description\r\n from \"{$row->Description}\"\r\n   to \"{$job->description}\"\r\n and MaxAgeMinutes from {$row->MaxAgeMinutes} to {$job->max_age_minutes}\r\n";
				execute_query_no_result('update Job set Description=?, MaxAgeMinutes= ? where id_Job=?;',array('sis',
						$job->description,$job->max_age_minutes,$row->id_Job));
			}
		}

		$sql_time_now= date_format(date_create_from_format('Y-m-d\TH:i:s',job_time()),'Y-m-d H:i:s');
		$this->id_JobLog= execute_query_get_last_insert_id('insert into JobLog set id_Job=?, Started=?;',array('ss',
			$this->id_Job,$sql_time_now));

		$this->part_number= 0;
		$this->part_ids= array();
	}

	public function log_job_finish($status)
	{
		$sql_time_now= date_format(date_create_from_format('Y-m-d\TH:i:s',job_time()),'Y-m-d H:i:s');
		execute_query('update JobLog set Finished=?, Status=? where id_JobLog=?;',array('sss',
			$sql_time_now,$status,$this->id_JobLog));
		if (0!=count($this->part_ids))
		{
			$part_ids= implode(',',$this->part_ids);
			execute_query("update JobPart set Enabled=0 where id_Job=? && id_JobPart not in ($part_ids);",
				array('s',$this->id_Job));
		}
	}

	public function log_job_part_start($name,$description)
	{
		$ExtraStateFields= null;
		$rows= execute_query('select id_JobPart, Description, ExtraStateFields from JobPart where Name=? and id_Job=?',array('ss',$name,$this->id_Job));
		$count_rows= count($rows);
		if (0==$count_rows)
		{
			$this->id_JobPart= execute_query_get_last_insert_id('insert into JobPart set id_Job=?, Name=?, Description=?;',array('sss',
				$this->id_Job,$name,$description));
		}
		else
		{
			$row= $rows[0];
			$this->id_JobPart= $row->id_JobPart;
			$ExtraStateFields= $row->ExtraStateFields;
			if ($description!=$row->Description)
			{
				echo job_time() . " change db JobPart Description\r\n from \"{$row->Description}\"\r\n   to \"{$description}\"\r\n";
				execute_query_no_result('update JobPart set Enabled=1, Description=? where id_JobPart=?;',array('ss',
						$description,$row->id_JobPart));
			}
		}
		$this->part_ids[]= $this->id_JobPart;

		$sql_time_now= date_format(date_create_from_format('Y-m-d\TH:i:s',job_time()),'Y-m-d H:i:s');
		$this->id_JobPartLog= execute_query_get_last_insert_id('insert into JobPartLog set id_JobPart=?, id_JobLog=?, Started=?, Number=?;',array('sssi',
			$this->id_JobPart,$this->id_JobLog,$sql_time_now,$this->part_number));
		return $ExtraStateFields;
	}

	public function log_job_part_finish($status,$results,$job_part_state= null)
	{
		$sql_time_now= date_format(date_create_from_format('Y-m-d\TH:i:s',job_time()),'Y-m-d H:i:s');
		if (null==$job_part_state)
		{
			execute_query('update JobPartLog jpl 
			set jpl.Finished=?, jpl.Status=?, jpl.Results=? 
			where jpl.id_JobPartLog=?;'
			,array('ssss',$sql_time_now,$status,$results,$this->id_JobPartLog));
		}
		else
		{
			execute_query('update JobPartLog jpl 
			inner join JobPart jp on jpl.id_JobPart=jp.id_JobPart 
			set jpl.Finished=?, jpl.Status=?, jpl.Results=?, jp.ExtraStateFields=? 
			where jpl.id_JobPartLog=?;'
			,array('sssss',$sql_time_now,$status,$results,$job_part_state,$this->id_JobPartLog));
		}

		$this->part_number++;
	}
}

function safe_execute_job_parts_locked_by_pid_file_with_db_log($parts,$pid_file_path,$job_db_logger)
{
	global $echo_errors, $do_not_write_errors_to_log;
	$echo_errors= true;
	$do_not_write_errors_to_log= true;

	$job_db_logger->log_job_start();

	try
	{
		$status= execute_job_parts_locked_by_pid_file_with_db_log($parts,$pid_file_path,$job_db_logger);
	}
	catch (Exception $ex)
	{
		echo_job_exception($ex);
		throw $ex;
	}

	$job_db_logger->log_job_finish($status);
}

function safe_execute_job_parts_locked_by_pid_file($parts,$pid_file_path)
{
	try
	{
		global $echo_errors, $do_not_write_errors_to_log;
		$echo_errors= true;
		$do_not_write_errors_to_log= true;
		execute_job_parts_locked_by_pid_file($parts,$pid_file_path);
	}
	catch (Exception $ex)
	{
		echo_job_exception($ex);
		throw $ex;
	}
}