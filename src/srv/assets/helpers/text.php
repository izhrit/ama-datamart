<?php

function russian_numbering($count, $n1, $n2, $n10)
{
	return (($count % 10 == 1 && $count % 100 != 11) ? $n1 :
			(($count % 10 >= 2 && $count % 10 <= 4 && ($count % 100 < 10 || $count % 100 >= 20)) ? $n2 : $n10));
}

$russian_number_texts_m= array('ноль','один','два','три','четыре','пять','шесть','семь','восем','девять','десять','одиннадцать');
function russian_number_text_male($num)
{
	global $russian_number_texts_m;
	return (!isset($russian_number_texts_m[$num])) ? $num : $russian_number_texts_m[$num];
}
