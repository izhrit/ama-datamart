<?php

function std_filter_rule_builder_dt_compare($l,$rule)
{
	$field= mysqli_real_escape_string($l,$rule->field);
	$txt= trim($rule->data);
	$txt_len= strlen($txt);
	if (2>$txt_len)
	{
		return '';
	}
	else
	{
		$c= substr($txt,0,1);
		switch ($c)
		{
			case '>':
			case '<':
			case '=':
				$c2= substr($txt,1,1);
				if ('='!=$c2)
				{
					$tail= mysqli_real_escape_string($l,trim(substr($txt,1,$txt_len-1)));
					$res= " and $field $c '$tail' ";
					return $res;
				}
				else
				{
					$tail= mysqli_real_escape_string($l,trim(substr($txt,2,$txt_len-2)));
					$res= " and $field $c$c2 '$tail' ";
					return $res;
				}
			default: 
				$txt= mysqli_real_escape_string($l,$txt);
				return " and $field = '$txt' ";
		}
	}
	
}

function std_filter_field_op_data_builder($l,$field,$op,$data)
{
	$where= " and $field";
	switch ($op)
	{
		case 'eq': $where .= " = '".mysqli_real_escape_string($l,$data)."'"; break;
		case 'ne': $where .= " != '".mysqli_real_escape_string($l,$data)."'"; break;
		case 'bw': $where .= " LIKE '".mysqli_real_escape_string($l,$data)."%'"; break;
		case 'bn': $where .= " NOT LIKE '".mysqli_real_escape_string($l,$data)."%'"; break;
		case 'ew': $where .= " LIKE '%".mysqli_real_escape_string($l,$data)."'"; break;
		case 'en': $where .= " NOT LIKE '%".mysqli_real_escape_string($l,$data)."'"; break;
		case 'cn': $where .= " LIKE '%".mysqli_real_escape_string($l,$data)."%'"; break;
		case 'nc': $where .= " NOT LIKE '%".mysqli_real_escape_string($l,$data)."%'"; break;
		case 'nu': $where .= " IS NULL"; break;
		case 'nn': $where .= " IS NOT NULL"; break;
		case 'in': $where .= " IN ('".str_replace(",", "','", mysqli_real_escape_string($l,$data))."')"; break;
		case 'ni': $where .= " NOT IN ('".str_replace(",", "','", mysqli_real_escape_string($l,$data))."')"; break;
	}
	return $where;
}

function std_filter_rule_builder($l,$rule)
{
	return std_filter_field_op_data_builder($l,$rule->field,$rule->op,$rule->data);
}

function prep_std_filter_rule_builder_for_expression($expr)
{
	return function($l,$rule) use ($expr) 
	{
		return std_filter_field_op_data_builder($l,$expr,$rule->op,$rule->data);
	};
}

function prep_std_filter_rule_builder_for_select_expression($expr)
{
	return function($l,$rule) use ($expr) 
	{
		if (!isset($rule->data) || 'null'==$rule->data)
		{
			return '';
		}
		else
		{
			return " and $expr = '".mysqli_real_escape_string($l,$rule->data)."'";
		}
	};
}

function build_where_for_jqgrid_for_filters($l,$filter_rule_builders,$filters)
{
	$where= '';
	$filters= stripslashes($filters);
	$filters = json_decode($filters);
	if (isset($filters->rules))
	{
		foreach ($filters->rules as $index => $rule)
		{
			$field= $rule->field;
			if (isset($filter_rule_builders[$field]))
			{
				$builder= $filter_rule_builders[$field];
				if (is_callable($builder))
				{
					$where.= $builder($l,$rule);
				}
				else if (is_array($builder))
				{
					$where.= std_filter_field_op_data_builder($l,$builder['query_field'],$rule->op,$rule->data);
				}
			}
		}
	}
	return $where;
}

function build_where_for_jqgrid($l,$filter_rule_builders)
{
	if (isset($_GET['filters']))
	{
		return build_where_for_jqgrid_for_filters($l,$filter_rule_builders,$_GET['filters']);
	}
	else if (isset($_POST['filters']))
	{
		return build_where_for_jqgrid_for_filters($l,$filter_rule_builders,$_POST['filters']);
	}
	else
	{
		return '';
	}
}

function build_orderby_for_jqgrid($order)
{
	$orderby= '';
	if (isset($_GET['sord']) && isset($_GET['sidx']) && !$_GET['sord']=='' && !$_GET['sidx']=='')
		$orderby='order by '.$_GET['sidx'].' '.$_GET['sord'];
	if (''!=$order)
	{
		if (''==$orderby)
		{
			$orderby= "order by $order";
		}
		else
		{
			$orderby.= ", $order";
		}
	}
	return $orderby;
}

function execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders,$order='')
{
	$con= default_dbconnect();
	$l= $con->get_db_link();
	$where= build_where_for_jqgrid($l,$filter_rule_builders);
	$order= build_orderby_for_jqgrid($order);
	$page= 0;
	$limit_position= 0;
	$limit_size= 10;
	if (isset($_GET['rows']) || isset($_POST['rows']))
	{
		$limit_size= isset($_GET['rows']) ? $_GET['rows'] : $_POST['rows'];
		if (isset($_GET['page']) || isset($_POST['page']))
		{
			$page= isset($_GET['page']) ? $_GET['page'] : $_POST['page'];
			$limit_position= mysqli_real_escape_string($l,($page-1)*$limit_size);
		}
		$limit_size= mysqli_real_escape_string($l,$limit_size);
	}

	$txt_query= "select $fields $from_where $where $order limit $limit_position, $limit_size;";
	global $trace_jqgrid_query;
	if (isset($trace_jqgrid_query) && true==$trace_jqgrid_query)
		write_to_log_named('txt_query',$txt_query);
	$rows= execute_query($txt_query,array());

	$txt_query= "select count(*) count $from_where $where";
	$rows_count= execute_query($txt_query,array());
	$rows_count= $rows_count[0]->count;

	return array(
		'page'=>$page,
		'total'=>ceil($rows_count/$limit_size),
		'records'=>$rows_count,
		'rows'=>$rows
	);
}

function execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders,$order='')
{
	$result= execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders,$order);
	if (function_exists('nice_json_encode'))
	{
		echo nice_json_encode($result);
	}
	else
	{
		echo json_encode($result);
	}
}

function echo_empty_jqgrid()
{
	$result= array(
		'page'=>0,
		'total'=>0,
		'records'=>0,
		'rows'=>array()
	);
	if (function_exists('nice_json_encode'))
	{
		echo nice_json_encode($result);
	}
	else
	{
		echo json_encode($result);
	}
}
