<?

function CheckFileArgument($argname)
{
	if (!isset($_FILES[$argname]))
		throw new RuntimeException('�� ����� ���� '.$argname);

	$file_param= $_FILES[$argname];
	if (!isset($file_param['error']) || is_array($file_param['error']))
		throw new RuntimeException('�� ����� ������ ��������� ����� '.$argname);

	switch ($file_param['error'])
	{
		case UPLOAD_ERR_OK:        break;
		case UPLOAD_ERR_NO_FILE:   throw new RuntimeException('�� ����� ���� '.$argname);
		case UPLOAD_ERR_INI_SIZE:
		case UPLOAD_ERR_FORM_SIZE: throw new RuntimeException('���� '.$argname.' ������� �������.');
		default:                   throw new RuntimeException('����������� ������ ��� ����� ����� '.$argname);
	}

	if ($file_param['size'] > 1000000)
		throw new RuntimeException('���� '.$argname.' ������� ������� ��� ���.');
	
	return $file_param;
}