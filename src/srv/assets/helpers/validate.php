<?php

function exit_internal_server_error($error_text)
{
	header("HTTP/1.1 500 Internal Server Error");
	echo 'Internal Server Error';
	exit_with_error($error_text);
}

function exit_not_found($error_text)
{
	header("HTTP/1.1 404 Not Found");
	echo 'Not Found';
	exit_with_error($error_text);
}

function exit_bad_request($error_text)
{
	header("HTTP/1.1 400 Bad Request");
	echo 'bad request';
	exit_with_error($error_text);
}

function exit_unauthorized($error_text)
{
	header("HTTP/1.1 401 Unauthorized");
	echo 'Unauthorized';
	exit_with_error($error_text);
}

function exit_with_error($error_text)
{
	write_to_log($error_text);
	if (function_exists('write_to_log_auth_info'))
		write_to_log_auth_info();
	if (function_exists('write_error_to_access_log'))
		write_error_to_access_log($error_text);
	write_to_log('$_GET:');
	write_to_log($_GET);
	exit;
}

function CheckMandatoryGET($name)
{
	if (!isset($_GET[$name]))
		exit_bad_request("skipped _GET['$name']");
	return $_GET[$name];
}

function CheckMandatoryGET_variants($name,$variants)
{
	CheckMandatoryGET($name);
	$variant= $_GET[$name];
	if (!in_array($variant,$variants))
		exit_bad_request("unexpected _GET['$name']=$variant");
	return $variant;
}

function CheckMandatoryGET_get_variant_by_name($name,$variants)
{
	CheckMandatoryGET($name);
	$variant= $_GET[$name];
	if (!isset($variants[$variant]))
		exit_bad_request("unexpected _GET['$name']=$variant");
	return $variants[$variant];
}

function CheckMandatoryGET_execute_action_variant($name,$action_variants)
{
	$action= CheckMandatoryGET_get_variant_by_name($name,$action_variants);
	$action();
}

function CheckMandatoryGET_id($name)
{
	CheckMandatoryGET($name);
	$id= $_GET[$name];
	if (!is_numeric($id))
		exit_bad_request("bad numeric _GET['$name']");
	return $id;
}

function CheckMandatoryPOST($name,$post_data= null)
{
	if (null==$post_data)
		$post_data= $_POST;
	if (!isset($post_data[$name]))
		exit_bad_request("skipped _POST['$name']");
	return $post_data[$name];
}

function CheckMandatoryNotEmptyPOST($name,$post_data= null)
{
	$value= CheckMandatoryPOST($name,$post_data);
	if (!$value)
		exit_bad_request("empty _POST['$name']");
	return $value;
}

