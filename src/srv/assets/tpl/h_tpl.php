<?

require_once __DIR__.'../../helpers/text.php';
require_once __DIR__.'../../libs/Library/NCLNameCaseRu.php';

$nc= new NCLNameCaseRu();

function par_line($line_len= null)
{
	return str_repeat('_',null==$line_len ? 6 : $line_len);
}

function par($field_path, $line_len= null)
{
	global $tpl_par;
	if (null==$field_path)
	{
		return par_line($line_len);
	}
	else if (is_object($field_path))
	{
		return $field_path;
	}
	else if (!isset($tpl_par) || null==$tpl_par || ''==$tpl_par)
	{
		return par_line($line_len);
	}
	else
	{
		$field_names= explode('.',$field_path);
		$current_tpl_par= $tpl_par;
		foreach ($field_names as $field_name)
		{
			if (!isset($current_tpl_par->{$field_name}))
			{
				return par_line($line_len);
			}
			else
			{
				$current_tpl_par= $current_tpl_par->{$field_name};
				if (null==$current_tpl_par || ''==$current_tpl_par)
					return par_line($line_len);
			}
		}
		return $current_tpl_par;
	}
}

$склонения_префиксов_сро= array
(
	'АССОЦИАЦИЯ САМОРЕГУЛИРУЕМАЯ ОРГАНИЗАЦИЯ'=>array(
		 'родительный'=>/*кого/чего?*/'Ассоциации саморегулируемой организации'
		,'дательный'=>  /*кому/чему?*/'Ассоциации саморегулируемой организации'
	)
	,'АССОЦИАЦИЯ'=>array(
		 'родительный'=>/*кого/чего?*/'Ассоциации'
		,'дательный'=>  /*кому/чему?*/'Ассоциации'
	)
	,'НЕКОММЕРЧЕСКОЕ ПАРТНЕРСТВО'=>array(
		 'родительный'=>/*кого/чего?*/'Некоммерческого партнерства'
		,'дательный'=>  /*кому/чему?*/'Некоммерческому партнерству'
	)
	,'САМОРЕГУЛИРУЕМАЯ ОРГАНИЗАЦИЯ'=>array(
		 'родительный'=>/*кого/чего?*/'Саморегулируемой организации'
		,'дательный'=>  /*кому/чему?*/'Саморегулируемой организации'
	)
	,'СОЮЗ'=>array(
		 'родительный'=>/*кого/чего?*/'Союза'
		,'дательный'=>  /*кому/чему?*/'Союзу'
	)
	,'НАЦИОНАЛЬНЫЙ СОЮЗ'=>array(
		 'родительный'=>/*кого/чего?*/'Национального союза'
		,'дательный'=>  /*кому/чему?*/'Национальному союзу'
	)
);

$склонения_префиксов_ас= array
(
	'АРБИТРАЖНЫЙ СУД'=>array(
		 'родительный'=>  /*кого/чего?*/'Арбитражного суда'
		,'дательный'=>    /*кому/чему?*/'Арбитражному суду'
		,'творительный'=> /*кем/чем?*/'Арбитражным судом'
	)
);

function startsWith( $haystack, $needle )
{
	$length = strlen( $needle );
	return substr( $haystack, 0, $length ) === $needle;
}

function склонение_организации($название, $падеж, $склонения_префиксов)
{
	$название= trim($название);
	$название_большими_буквами= mb_strtoupper($название);

	// если первое слово - аббревиатура, то ничего не делаем
	if (!strripos($название, '-') || mb_strtoupper(strstr($название, ' ', true)) != $название_большими_буквами)
	{
		foreach ($склонения_префиксов as $префикс_в_именительном => $склонения_префикса)
		{
			$длина_префикса_в_именительном= mb_strlen($префикс_в_именительном);
			if (startsWith($название_большими_буквами,$префикс_в_именительном))
			{
				$новый_префикс= $склонения_префикса[$падеж];
				$исходный_префикс= mb_substr($название, 0, $длина_префикса_в_именительном);
				if ($название_большими_буквами == $исходный_префикс)
				{
					$новый_префикс= mb_strtoupper($новый_префикс);
				}
				else if (mb_convert_case($исходный_префикс, MB_CASE_TITLE) == $исходный_префикс)
				{
					$новый_префикс= mb_convert_case($новый_префикс, MB_CASE_TITLE);
				}
				return $новый_префикс.mb_substr($название, $длина_префикса_в_именительном);
			}
		}
	}
	return $название;
}

function кого_чего_ас($field_path, $line_len= null)
{
	global $склонения_префиксов_ас;
	return склонение_организации(par($field_path, $line_len), 'родительный', $склонения_префиксов_ас);
}

function кем_чем_ас($field_path, $line_len= null)
{
	global $склонения_префиксов_ас;
	return склонение_организации(par($field_path, $line_len), 'творительный', $склонения_префиксов_ас);
}

function кого_чего_сро($field_path, $line_len= null)
{
	global $склонения_префиксов_сро;
	return склонение_организации(par($field_path, $line_len), 'родительный', $склонения_префиксов_сро);
}

function кому_чему_сро($field_path, $line_len= null)
{
	global $склонения_префиксов_сро;
	return склонение_организации(par($field_path, $line_len), 'дательный', $склонения_префиксов_сро);
}

$переводы_месяцев= array(
	'January'=>'Января'
	,'February'=>'Февраля'
	,'March'=>'Марта'
	,'April'=>'Апреля'
	,'May'=>'Мая'
	,'June'=>'Июня'
	,'July'=>'Июля'
	,'August'=>'Августа'
	,'September'=>'Сентября'
	,'October'=>'Октября'
	,'November'=>'Ноября'
	,'December'=>'Декабря'
);

function полная_дата($field_path, $line_len= null)
{
	$d= par($field_path, $line_len);
	$dt= date_create_from_format('Y-m-d\TH:i:s',$d);
	if (null==$dt)
		$dt= date_create_from_format('Y-m-dTH:i:s',$d);
	if (null==$dt)
		$dt= date_create_from_format('Y-m-d H:i:s',$d);
	if (null==$dt)
		$dt= date_create_from_format('Y-m-d',$d);
	if (null==$dt)
	{
		return $d;
	}
	else
	{
		$day= date_format($dt,'j');
		$month= date_format($dt,'F');
		global $переводы_месяцев;
		if (isset($переводы_месяцев[$month]))
			$month= $переводы_месяцев[$month];
		$year= date_format($dt,'Y');
		return "«{$day}»&nbsp;$month&nbsp;$year";
	}
}

function юр_время($field_path, $line_len= null)
{
	$d= par($field_path, $line_len);
	$dt= date_create_from_format('Y-m-d\TH:i:s',$d);
	if (null==$dt)
		$dt= date_create_from_format('Y-m-dTH:i:s',$d);
	if (null==$dt)
		$dt= date_create_from_format('Y-m-d H:i:s',$d);
	if (null==$dt)
		$dt= date_create_from_format('Y-m-d',$d);
	if (null==$dt)
	{
		return $d;
	}
	else
	{
		$day= date_format($dt,'j');
		$month= date_format($dt,'F');
		global $переводы_месяцев;
		if (isset($переводы_месяцев[$month]))
			$month= $переводы_месяцев[$month];
		$year= date_format($dt,'Y');
		$hours= date_format($dt,'H');
		$minutes= date_format($dt,'i');
		return "$day&nbsp;$month&nbsp;$year года в $hours&nbsp;час. $minutes&nbsp;мин.";
	}
}

function краткая_дата($field_path, $line_len= null)
{
	$d= par($field_path, $line_len);
	$dt= date_create_from_format('Y-m-d\TH:i:s',$d);
	if (null==$dt)
		$dt= date_create_from_format('Y-m-dTH:i:s',$d);
	if (null==$dt)
		$dt= date_create_from_format('Y-m-d H:i:s',$d);
	if (null==$dt)
		$dt= date_create_from_format('Y-m-d',$d);
	if (null==$dt)
	{
		return $d;
	}
	else
	{
		return date_format($dt,'j.m.Y');
	}
}

function инициалы($field_path, $line_len= null)
{
	$obj= par($field_path, $line_len);
	if (is_string($obj))
	{
		return $obj;
	}
	else
	{
		$res= '';
		if (isset($obj->Фамилия))
			$res.= $obj->Фамилия;
		if (isset($obj->Имя))
			$res.= '&nbsp;'.mb_substr($obj->Имя,0,1).'.';
		if (isset($obj->Отчество))
			$res.= '&nbsp;'.mb_substr($obj->Отчество,0,1).'.';
		return $res;
	}
}

function фио($field_path, $line_len= null)
{
	$obj= par($field_path, $line_len);
	if (is_string($obj))
	{
		return $obj;
	}
	else
	{
		$res= '';
		if (isset($obj->Фамилия))
			$res.= $obj->Фамилия;
		if (isset($obj->Имя))
			$res.= ' '.$obj->Имя;
		if (isset($obj->Отчество))
			$res.= ' '.$obj->Отчество;
		return $res;
	}
}

$склонения_фамилий= array
(
	'Лагода'=>array(
		 NCL::$RODITLN=>/*кого/чего?*/'Лагоды'
	)
);

function фио_nc_падеж($field_path, $nc_падеж, $line_len= null)
{
	$obj= par($field_path, $line_len);
	if (is_string($obj))
	{
		return $obj;
	}
	else
	{
		global $nc;

		$Фамилия= $obj->Фамилия;
		$pos0= mb_strpos($Фамилия,"(");
		if (false==$pos0)
		{
			return $nc->qFullName($Фамилия, $obj->Имя, $obj->Отчество, null, $nc_падеж);
		}
		else
		{
			$pos1= mb_strpos($Фамилия,")");
			$Фамилия1= trim(mb_substr($Фамилия,0,$pos0));
			$Фамилия2= trim(mb_substr($Фамилия,$pos0+1,$pos1-$pos0-1));

			$фио1= $nc->qFullName($Фамилия1, $obj->Имя, $obj->Отчество, null, $nc_падеж);
			$фио2= $nc->qFullName($Фамилия2, $obj->Имя, $obj->Отчество, null, $nc_падеж);

			$parts1= explode(' ',$фио1);
			$parts2= explode(' ',$фио2);

			global $склонения_фамилий;
			$падеж_Фамилия1= !isset($склонения_фамилий[$Фамилия1][$nc_падеж]) ? $parts1[0] : $склонения_фамилий[$Фамилия1][$nc_падеж];
			$падеж_Фамилия2= !isset($склонения_фамилий[$Фамилия2][$nc_падеж]) ? $parts2[0] : $склонения_фамилий[$Фамилия2][$nc_падеж];
			$падеж_Имя= $parts1[1];
			$падеж_Отчество= $parts1[2];

			return "$падеж_Фамилия1 ($падеж_Фамилия2) $падеж_Имя $падеж_Отчество";
		}
	}
}

function кому_фио($field_path, $line_len= null)
{
	return фио_nc_падеж($field_path, NCL::$DATELN, $line_len);
}

function кого_фио($field_path, $line_len= null)
{
	return фио_nc_падеж($field_path, NCL::$RODITLN, $line_len);
}

function кого_что_фио($field_path, $line_len= null)
{
	return фио_nc_падеж($field_path, NCL::$VINITELN, $line_len);
}
