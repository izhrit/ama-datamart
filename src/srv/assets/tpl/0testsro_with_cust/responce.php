<?

global $tpl_par;
$tpl_par= $params;
require_once __DIR__.'../../h_tpl.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Тестовый ответ на запрос кандидатуры арбитражного управляющего</title>
	<xml xmlns:w="urn:schemas-microsoft-com:office:word">
		<w:WordDocument>
			<w:View>Print</w:View>
			<w:Zoom>100</w:Zoom>
			<w:DoNotOptimizeForBrowser/>
		</w:WordDocument>
    </xml>
</head>
<body class="mrequest_response">
	<div class="WordPageA4">
		Кастомный ответ на запрос кандидатуры арбитражного управляющего
		<?
			echo '</br>';
			echo 'Входные данные: ';
			echo '</br>';
			echo nice_json_encode($params);
		?>
	</div>
</body>
</html>