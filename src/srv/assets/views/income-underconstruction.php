<?

require_once '../../assets/config.php';
require_once '../../assets/helpers/log.php';
require_once '../../assets/libs/income.php';

$income= get_income_xml_file($_GET['id_MData'],$main_entry_name);

?>
<html>
	<head>
		<title>Витрина данных ПАУ</title>
		<meta http-equiv="X-UA-Compatible" content="IE=11" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />
	</head>

	<body>
		<h1>
			<?= $title_first_line ?><br/>
			Но страница пока в разработке..
		</h1>
		<? if (0===mb_strpos($income,'не удалось')) : ?>
			<?= $income ?>
		<? else : ?>
			Необработанное представление:
			<hr/>
			<pre><?= prepare_xml_for_pre($income) ?></pre>
			<hr/>
		<? endif; ?>
	</body>
</html>
