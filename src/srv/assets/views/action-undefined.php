<?
$title= isset($title) ? $title : 'REST методы для витрины данных ПАУ';
if ($trace_methods)
	write_to_log('   undefined action!');
?>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />
		<title><?= $title ?></title>
	</head>
	<body style="width:800px;margin:60px auto;text-align:center;">
		<h1><?= $title ?></h1>
		<p style="color:red;">
			Параметр действия неопределён
		</p>
		<? if (isset($doc_link)) : ?>
		<p>
			Документацию на API можно почитать <a href="<?= $doc_link ?>">здесь</a>. 
			Там же можно потестировать методы
		</p>
		<p>
			API витрины оперирует данными в виде zip файла,<br/>
			внутри которого упакованы xml файлы с данными из разных разделов ПАУ:
			<style>
				td{border:1px solid gray;padding:5px;}
				tr.title td{background-color:silver;text-align:center;}
			</style>
			<table style="margin:0 auto;border-spacing:0;border-collapse:collapse;">
				<tr class="title">
					<td>Название файла</td>
					<td>Краткое описание</td>
					<td>xsd схема</td>
					<td>пример</td>
				</tr>
				<tr>
					<td>registry.xml</td>
					<td>Данные о реестре требований кредиторов</td>
					<td><a href="docs/rtk.xsd">xsd схема данных о реестре</a></td>
					<td><a href="docs/rtk-example.xml">пример данных о реестре</a></td>
				</tr>
				<tr>
					<td>report.xml</td>
					<td>Данные об отчёте АУ</td>
					<td><a href="docs/report.xsd">xsd&nbsp;схема&nbsp;данных&nbsp;об&nbsp;отчёте</a></td>
					<td><a href="docs/report-example.xml">пример&nbsp;данных&nbsp;об&nbsp;отчёте</a></td>
				</tr>
				<tr>
					<td>current_claims.xml</td>
					<td>Данные о реестре текущих требований</td>
					<td><a href="docs/rtt.xsd">xsd&nbsp;схема&nbsp;данных&nbsp;о&nbsp;реестре текущих требований</a></td>
					<td><a href="docs/rtt-example.xml">пример&nbsp;данных&nbsp;о&nbsp;реестре текущих требований</a></td>
				</tr>
				<tr>
					<td>km.xml</td>
					<td>Данные о конкурсной массе</td>
					<td><a href="docs/km.xsd">xsd&nbsp;схема&nbsp;данных&nbsp;о&nbsp;конкурсной массе</a></td>
					<td><a href="docs/km-example.xml">пример&nbsp;данных&nbsp;о&nbsp;конкурсной массе</a></td>
				</tr>
				<tr>
					<td>deals.xml</td>
					<td>Данные о сделках</td>
					<td><a href="docs/deals.xsd">xsd&nbsp;схема&nbsp;данных&nbsp;о&nbsp;сделках</a></td>
					<td><a href="docs/deals-example.xml">пример&nbsp;данных&nbsp;о&nbsp;сделках</a></td>
				</tr>
				<tr>
					<td>anketanp.xml</td>
					<td>Анкета физ.лица</td>
					<td><a href="docs/anketanp.xsd">xsd&nbsp;схема&nbsp;данных&nbsp;анкеты физ.лица</a></td>
					<td><a href="docs/anketanp-example.xml">пример&nbsp;данных&nbsp;анкеты физ.лица</a></td>
				</tr>
			</table>
		</p>
		<? endif; ?>
	</body>
</html>