<? 

mb_internal_encoding("utf-8");

global $base_url, $login, $password, $test_mode, $access_token, $Manager_INN, $debtor;
$base_url= 'http://local.test/dm/api.php';
$login= 'bankrotme';
$password= 'Hn51Ldt456Abn';
$access_token= null;

$file_path= '../anketanp-example.zip';

$debtor= (object)array(
	'first_name' => ''
	, 'last_name' => ''
	, 'middle_name' => ''
	, 'email' => ''
	, 'snils' => ''
	, 'inn' => ''
);

function plain_Auth($login,$password)
{
	global $base_url;

	$curl = curl_init("$base_url/Auth?login=$login&category=partner");
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, array('password'=>$password));
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	return 200!=$httpcode ? null : $curl_response;
}

function parse_Auth_response($curl_response)
{
	if (null==$curl_response || 'null'==''.$curl_response)
	{
		return null;
	}
	else
	{
		$decoded_curl_response = json_decode($curl_response);
		$token = $decoded_curl_response->token;
		return $token;
	}
}

function verbose_Auth($login,$password)
{
	global $base_url;

	$curl_response = plain_Auth($login,$password);

	if (null==$curl_response || 'null'==''.$curl_response)
	{
		echo "unseccesefull\r\n\r\n";
	}
	else
	{
		$access_token= parse_Auth_response($curl_response);
		$access_token_len= mb_strlen($access_token);
		echo "strlen(access_token)=$access_token_len\r\n\r\n";
	}
}

function Auth($login,$password)
{
	$curl_response = plain_Auth($login,$password);
	return parse_Auth_response($curl_response);
}

function DoAuth()
{
	global $login, $password;
	verbose_Auth($login, $password);
}

function UploadAnketa($token, $Manager_INN, $content_hash, $postFieldsData, $debtor) 
{
	global $base_url;
	$url = "$base_url/UploadAnketaNP?";
	$url .= 'manager[inn]='.$Manager_INN;
	$url .= '&debtor[first-name]='.$debtor->first_name;
	$url .= '&debtor[last-name]='.$debtor->last_name;
	$url .= '&debtor[middle-name]='.$debtor->middle_name;
	$url .= '&debtor[email]='.$debtor->email;
	$url .= '&debtor[snils]='.$debtor->snils;
	$url .= '&debtor[inn]='.$debtor->inn;
	$url .= '&content-hash='.$content_hash;
	$curl = curl_init($url);

	curl_setopt($curl, CURLOPT_HTTPHEADER, array("ama-datamart-access-token:$token","Content-Type:application/zip"));
	if (null!=$postFieldsData)
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postFieldsData);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);
	return 200==$httpcode ? null : $curl_response;
}

function DoUploadAnketa()
{
	global $access_token, $file_path, $login, $password, $Manager_INN, $content_hash, $debtor;
	
	$postFieldsData = null;
	if (null==$access_token)
		$access_token= Auth($login,$password);

	$postFieldsData = file_get_contents($file_path);

	$upload_result = UploadAnketa($access_token, $Manager_INN, $content_hash, $postFieldsData, $debtor);

	if (null==$upload_result)
	{
		echo "ok uploaded!";
	}
	else
	{
		echo $upload_result;
	}
}

function GetAUList($token, $q) 
{
	global $base_url;
	$url = "$base_url/GetAUList?";
	$url .= 'q='.$q;
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("ama-datamart-access-token:$token","Content-Type:application/zip"));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response = curl_exec($curl);
	curl_close($curl);
	return $curl_response;
}

function DoGetAUList()
{
	global $access_token, $q, $login, $password;
	if (null==$access_token)
		$access_token= Auth($login,$password);
	$get_result = GetAUList($access_token, $q);
	if (null==$get_result)
	{
		echo $get_result;
	}
	else
	{
		echo "got list successfully!";
		echo $get_result;
	}
}

global $action;
$action= 'upload';

function ProcessArguments()
{
	global $argv, $action, $login, $password, $test_mode, $tab, $access_token, $Manager_INN, $file_path, $debtor, $q;
	$count= count($argv);
	for ($i= 1; $i<$count; $i++)
	{
		$ap = explode('=',$argv[$i]);
		switch ($ap[0])
		{
			case '--login': $login= $ap[1]; break;
			case '--password': $password = $ap[1]; break;
			case '--action': $action = $ap[1]; break;
			case '--test-mode': $test_mode = true; $tab= "  "; break;
			case '--access-token': $access_token = $ap[1]; break;
			case '--manager-inn': $Manager_INN = $ap[1]; break;
			case '--content-hash': $content_hash = $ap[1]; break;
			case '--file-path': $file_path = $ap[1]; break;
			case '--first-name': $debtor->first_name= $ap[1]; break;
			case '--last-name': $debtor->last_name= $ap[1]; break;
			case '--middle-name': $debtor->middle_name= $ap[1]; break;
			case '--email': $debtor->email= $ap[1]; break;
			case '--snils': $debtor->snils= $ap[1]; break;
			case '--inn': $debtor->inn= $ap[1]; break;
			case '--q': $q= $ap[1]; break;
		}
	}
}

function DoAction()
{
	global $action;
	switch ($action)
	{
		case 'auth': DoAuth(); break;
		case 'upload': DoUploadAnketa(); break;
		case 'getAUList': DoGetAUList(); break;
		default: echo "unknown action $action"; break;
	}
}

ProcessArguments();
DoAction();