<? 

mb_internal_encoding("utf-8");

global $base_url, $login, $password, $access_token, $category, $args;
$base_url= 'http://local.test/dm/api.php';
$login= 'bankrotme';
$password= 'Hn51Ldt456Abn';
$access_token= null;
$category= 'partner';
$args= null;

function plain_Auth($login,$password)
{
	global $base_url, $category;

	$curl = curl_init("$base_url/Auth?login=$login&category=$category");
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, array('password'=>$password));
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	return 200!=$httpcode ? null : $curl_response;
}

function parse_Auth_response($curl_response)
{
	if (null==$curl_response || 'null'==''.$curl_response)
	{
		return null;
	}
	else
	{
		$decoded_curl_response = json_decode($curl_response);
		$token = $decoded_curl_response->token;
		return $token;
	}
}

function verbose_Auth($login,$password)
{
	global $base_url;

	$curl_response = plain_Auth($login,$password);

	if (null==$curl_response || 'null'==''.$curl_response)
	{
		echo "unseccesefull\r\n\r\n";
	}
	else
	{
		$access_token= parse_Auth_response($curl_response);
		$access_token_len= mb_strlen($access_token);
		echo "strlen(access_token)=$access_token_len\r\n\r\n";
	}
}

function Auth($login,$password)
{
	$curl_response = plain_Auth($login,$password);
	return parse_Auth_response($curl_response);
}

function DoAuth()
{
	global $login, $password;
	verbose_Auth($login, $password);
}

function prep_url_args($args)
{
	$args_parts= explode('&',$args);
	$fixed_args= array();
	foreach ($args_parts as $arg)
	{
		$pos= mb_strpos($arg,'=');
		if (false==$pos)
		{
			$fixed_args[]= $arg;
		}
		{
			$name= mb_substr($arg,0,$pos);
			$value= urlencode(mb_substr($arg,$pos+1));
			$fixed_args[]= "$name=$value";
		}
		
	}
	return implode('&',$fixed_args);
}

function RegistrateStart($token) 
{
	global $base_url, $args;
	$url = "$base_url/RegistrateBankruptcyPetition?";
	if (null!=$args)
		$url .= prep_url_args($args);
	//echo "url=$url\r\n";
	$curl = curl_init($url);

	curl_setopt($curl, CURLOPT_HTTPHEADER, array("ama-datamart-access-token:$token","Content-Type:application/zip"));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
	{
		echo "httpcode=$httpcode\r\n";
		exit;
	}
	else
	{
		$responce= json_decode($curl_response);
		if (null!=$responce)
		{
			return $responce;
		}
		else
		{
			echo "can not parse:\r\n$curl_response\r\n";
			exit;
		}
	}
}

function DoRegistrateStart()
{
	global $access_token, $login, $password;
	
	if (null==$access_token)
		$access_token= Auth($login,$password);

	$reg_result= RegistrateStart($access_token);

	$res= print_r($reg_result,true);
	$res= str_replace("\r\n","\n",$res);
	$res= str_replace("\n","\r\n",$res);

	echo $res;
}

global $action;
$action= 'start';

function ProcessArguments()
{
	global $argv, $action, $login, $password, $tab, $access_token, $args, $category;
	$count= count($argv);
	for ($i= 1; $i<$count; $i++)
	{
		$ap = explode('=',$argv[$i]);
		switch ($ap[0])
		{
			case '--login': $login= $ap[1]; break;
			case '--password': $password = $ap[1]; break;
			case '--action': $action = $ap[1]; break;
			case '--as': $category = $ap[1]; break;
			case '--access-token': $access_token = $ap[1]; break;

			case '--args': $args= file_get_contents($ap[1]); break;
		}
	}
}

function DoAction()
{
	global $action;
	switch ($action)
	{
		case 'auth': DoAuth(); break;
		case 'start': DoRegistrateStart(); break;
		default: echo "unknown action $action"; break;
	}
}

ProcessArguments();
DoAction();