﻿select
 id_ProcedureStart
,Court.Name CourtName
,id_Contract
,id_MUser
,id_Manager
,id_MRequest
,CaseNumber
,DebtorName
,DebtorINN
,DebtorSNILS
,DebtorOGRN
,DebtorCategory
,DateOfApplication
,NextSessionDate
,TimeOfLastChecking
,if(0<>ReadyToRegistrate,'true','false') ReadyToRegistrate
from ProcedureStart
inner join Court on Court.id_Court=ProcedureStart.id_Court
\G