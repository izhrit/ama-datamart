Authorize
      on http://local.test/dm/api.php
      using login "ctb"
      and password "*************"..
..authorized
access_token: "****************************************************************"

read revision of downloaded procedures
      from "revision.max.txt" ..
..read: 4

get list of changed procedures
         from revision 4,
         but no longer than 100..
.. got list of 1 procedures from 1.

item 0 from 1 (id_MProcedure:6,revision:7)
{
    read revision of downloaded file for procedure 6
          from "6/revision.txt" ..
    ..read: 0

    download procedure 6, revision more than 7..
    .. downloaded procedure 6, with revision 7.
    stored 10077 bytes.
    content:
      km.xml(size:24139,md5:c698d8db0d1465d4601d61de34ab3c51)
      current_claims.xml(size:5629,md5:ce40d89cf4b5abc397339a2cc063dd8f)
      registry.xml(size:11096,md5:797349badbe33f2d8d9626ff03dfe2e3)
      report.xml(size:35106,md5:bc875322f82b8c22544c3507bae444fb)
    store revision of downloaded procedures ..
    ..stored 7
     in file "revision.max.txt"
}

update is finished.
