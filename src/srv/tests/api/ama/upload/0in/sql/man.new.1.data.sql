select 
	  m.id_Contract
	, m.id_Manager

	, m.lastName
	, m.firstName
	, m.middleName
	, m.efrsbNumber
	, m.INN
	, m.BankroTechAcc

	, p.id_MProcedure
	, p.casenumber
	, p.procedure_type
	, b.Name debtorName
	, b.INN debtorInn
	, b.OGRN debtorOgrn
	, b.SNILS debtorSnils

	, p.revision p_revision
	, p.ctb_revision p_ctb_revision

	, length(d.fileData) data_len
	, d.revision d_revision
	, if(1=d.ctb_allowed,"1","0") ctb_allowed

	, p.Content_hash p_Content_hash
	, d.Content_hash d_Content_hash

from Manager m
inner join MProcedure p on p.id_Manager=m.id_Manager
inner join Debtor b on b.id_Debtor=p.id_Debtor
left join PData d on d.id_MProcedure=p.id_MProcedure
where 5=m.id_Manager
order by b.id_Debtor, d.id_PData
\G