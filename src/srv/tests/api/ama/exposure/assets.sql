﻿select 
     a.id_MProcedure
	,a.Title Краткое_наименование
	,a.Description Развёрнутое_описание
	,a.Address Адрес
	,a.ID_Object ID_Object
	,a.md5hash md5hash
	,r.Name Регион
	,g.Title Категория
	,a.State Стадия
	,(select group_concat(Code) from Asset_EfrsbAssetClass ae inner join EfrsbAssetClass e on ae.id_EfrsbAssetClass=e.id_EfrsbAssetClass where ae.id_Asset=a.id_Asset) КлассификацияЕФРСБ
	,if(0<>a.isAvailableToAll,1,0) isAvailableToAll
	,(select group_concat(UserName) from MUser v inner join Asset_MUser av on av.id_MUser=v.id_MUser where av.id_Asset=a.id_Asset) Получатели
from Asset a
inner join region r on a.id_Region=r.id_Region
left join AssetGroup g on g.id_AssetGroup=a.id_AssetGroup
order by id_Asset desc
\G
select
     id_Asset
    ,FileName
    ,FileSize
    ,md5hash
from AssetAttachment
\G