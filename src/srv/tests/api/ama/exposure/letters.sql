﻿select 
	id_SentEmail
	,RecipientEmail
	,RecipientType
	,replace(uncompress(ExtraParams),'\\r\\n','\r\n') ExtraParams
from SentEmail
where TimeDispatch > '2021-09-01'
\G

select
     a.id_SentEmail
	,a.FileName
	,replace(a.Content,'\\r\\n','\r\n') Content
from Attachment a
inner join SentEmail se on se.id_SentEmail=a.id_SentEmail
where se.TimeDispatch > '2021-09-01'
\G