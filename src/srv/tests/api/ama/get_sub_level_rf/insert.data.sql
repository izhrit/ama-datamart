-- 1 insert into `region` set `id_Region`= 1, `Name`= 'Удмуртская Республика', `OKATO`= '94 000';
insert into `region` set `id_Region`= 7, `Name`= 'Москва', `OKATO`= '45 000';
insert into `region` set `id_Region`= 8, `Name`= 'Санкт-Петербург', `OKATO`= '40 000';
insert into `region` set `id_Region`= 9, `Name`= 'Иркутская область', `OKATO`= '25 000';
-- 3 insert into `region` set `id_Region`= 5, `Name`= 'Российская федерация', `OKATO`= '00 000';

INSERT INTO sub_level
	(`id_Region`, `id_Contract`, `StartDate`,  `Sum_Common`, `Sum_Employable`, `Sum_Infant`, `Sum_Pensioner`, `Reglament_Title`,     `Reglament_Date`, `Reglament_Url`) 
VALUES 
	 ('1',        '1',           '2022-01-01', 11262,         12276,             11144,      9685,         'Приказ №2 по УР',       '2022-12-01',     'http://google1.com')
	,('7',        '1',           '2022-01-01', 18714,         21371,             16174,      14009,        'Приказ №2 по Москве',   '2022-12-02',     'http://google2.com')
	,('8',        '1',           '2022-01-01', 13160.2,       14344.6,           12765.4,    11317.8,      'Приказ №2 по СПБ',      '2022-12-03',     'http://google3.com')
	,('9',        '1',           '2022-01-01', 13413,         14620,             13269,      11535,        'Приказ №2 по Иркутску', '2022-12-04',     'http://google4.com')
	,('3',        '1',           '2022-01-01', 12654,         13793,             12274,      10882,        'Приказ №2 по РФ',       '2022-12-05',     'http://google5.com')
;