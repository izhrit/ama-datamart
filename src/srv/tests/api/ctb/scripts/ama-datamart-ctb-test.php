<? 

mb_internal_encoding("utf-8");

global $base_url, $login, $password, $test_mode, $category,$revision_from,$limit, $access_token, $case_number, $path_to_data_folder, $id_MProcedure, $file_path;
$base_url= 'http://local.test/dm/api.php';
$login= 'ctb';
$password= 'Hn51Ldt456Abn';
$category= 'partner';
$revision_from= 0;
$limit= 100;
$access_token= null;
$case_number= null;
$path_to_data_folder= ".\\";
$id_MProcedure= 0;
$file_path= 'procedure.zip';

$test_mode= false;
$tab= "                       ";

function llog($txt)
{
	global $test_mode;
	if ($test_mode)
	{
		echo $txt;
	}
	else
	{
		$t= date('m/d/Y h:i:s a: ', time());
		echo "$t $txt";
	}
}

function to_asterisks($txt)
{
	$asterisks= '';
	$len= strlen($txt);
	for ($i= 0; $i<$len; $i++)
		$asterisks.= '*';
	return $asterisks;
}

function plain_Auth($login,$password)
{
	global $base_url, $category;

	$curl = curl_init("$base_url/Auth?login=$login&category=$category");
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, array('password'=>$password));
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	return 200!=$httpcode ? null : $curl_response;
}

function parse_Auth_response($curl_response)
{
	if (null==$curl_response || 'null'==''.$curl_response)
	{
		return null;
	}
	else
	{
		$decoded_curl_response = json_decode($curl_response);
		$token = $decoded_curl_response->token;
		return $token;
	}
}

function verbose_Auth($login,$password)
{
	global $base_url, $tab;

	$password_asterisks= to_asterisks($password);
	llog("Authorize\r\n$tab    on $base_url\r\n$tab    using login \"$login\"\r\n$tab    and password \"$password_asterisks\"..\r\n");
	$curl_response = plain_Auth($login,$password);
	llog("..authorized\r\n");

	if (null==$curl_response || 'null'==''.$curl_response)
	{
		llog("unseccesefull\r\n\r\n");
		return null;
	}
	else
	{
		$access_token= parse_Auth_response($curl_response);

		$token_asterisks= to_asterisks($access_token);
		llog("access_token: \"$token_asterisks\"\r\n\r\n");

		return $access_token;
	}
}

function Auth($login,$password)
{
	$curl_response = plain_Auth($login,$password);
	return parse_Auth_response($curl_response);
}

function GetChangedProcedures($token,$revision_from,$limit,$case_number = null)
{
	global $base_url;
	$url= "$base_url/GetChangedProcedures?ama-dm-revision-greater-than=$revision_from&limit=$limit";
	if (null!=$case_number)
		$url.= "&procedure[case_number]=$case_number";
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("ama-datamart-access-token:$token")); 
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	return 200!=$httpcode ? null : json_decode($curl_response);
}

global $catched_header_revision, $header_revision_line_prefix, $header_revision_line_prefix_len;
$catched_header_revision= null;
$header_revision_line_prefix= 'ama-dm-revision: ';
$header_revision_line_prefix_len= strlen($header_revision_line_prefix);
function CatchRevision( $curl, $header_line )
{
	global $catched_header_revision, $header_revision_line_prefix, $header_revision_line_prefix_len;
	$header_line_len= strlen($header_line);
	$header_line_tail_len= $header_line_len - $header_revision_line_prefix_len;
	if ($header_line_tail_len > 0 &&
		substr($header_line, 0, $header_revision_line_prefix_len) === $header_revision_line_prefix)
	{
		$catched_header_revision= trim(substr($header_line, $header_revision_line_prefix_len, $header_line_tail_len));
	}
	return $header_line_len;
}

function DownloadProcedureInfo($token,$id_MProcedure)
{
	global $base_url, $catched_header_revision;

	$curl = curl_init("$base_url/DownloadProcedureInfo?id_MProcedure=$id_MProcedure");
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("ama-datamart-access-token:$token")); 
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$catched_header_revision= null;
	curl_setopt($curl, CURLOPT_HEADERFUNCTION, "CatchRevision");
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	return 200!=$httpcode ? null : array('zip_content' => $curl_response, 'revision' => $catched_header_revision);
}

function get_zip_file_list($prefix,$zip_file_path)
{
	$res= '';
	$zip = zip_open($zip_file_path);
	while ($zip_entry = zip_read($zip))
	{
		$zip_entry_name= zip_entry_name($zip_entry);
		$zip_entry_size= zip_entry_filesize($zip_entry);
		$zip_entry_content= zip_entry_read($zip_entry, $zip_entry_size);
		$zip_entry_md5= md5($zip_entry_content);
		$res.= "$prefix $zip_entry_name(size:$zip_entry_size,md5:$zip_entry_md5)\r\n";
	}
	zip_close($zip);
	return $res;
}

function verbose_DownloadProcedureInfoAndStoreIt($token,$id_MProcedure,$path_to_folder,$old_revision)
{
	llog("    download procedure $id_MProcedure, revision more than $old_revision..\r\n");
	$download_result= DownloadProcedureInfo($token,$id_MProcedure);
	if (null==$download_result)
	{
		llog("    .. can not downloaded procedure $id_MProcedure!\r\n");
		return null;
	}
	else
	{
		$zip_content= $download_result['zip_content'];
		$revision= $download_result['revision'];

		llog("    .. downloaded procedure $id_MProcedure, with revision $revision.\r\n");

		if (!is_dir($path_to_folder))
			mkdir($path_to_folder,0777,true);

		$zip_file_path= "$path_to_folder/procedure.rev$revision.zip";
		$zip_content_len= file_put_contents($zip_file_path,$zip_content);
		file_put_contents("$path_to_folder/revision.txt",$revision);

		llog("    stored $zip_content_len bytes.\r\n");

		llog("    content:\r\n");
		llog(get_zip_file_list('     ',$zip_file_path));

		return $revision;
	}
}

function DoAuth()
{
	global $login, $password;
	verbose_Auth($login, $password);
}

function verbose_GetMaxRevision($revision_max_path, $revision_max_file_name)
{
	global $tab;
	llog("read revision of downloaded procedures\r\n$tab    from \"$revision_max_file_name\" ..\r\n");
	$last_revision_content= !file_exists($revision_max_path) ? false : file_get_contents($revision_max_path);
	$last_revision= false==$last_revision_content ? 0 : intval($last_revision_content);
	llog("..read: $last_revision\r\n\r\n");
	return $last_revision;
}

function verbose_StoreMaxRevision($revision_max_path, $revision_max_file_name, $last_revision)
{
	global $tab;
	echo "";
	llog("    store revision of downloaded procedures ..\r\n");
	file_put_contents($revision_max_path, $last_revision);
	llog("    ..stored $last_revision\r\n$tab   in file \"$revision_max_file_name\"\r\n");
}

function verbose_GetRevision($revision_path, $revision_file_name, $id_MProcedure)
{
	global $tab;
	llog("    read revision of downloaded file for procedure $id_MProcedure\r\n$tab        from \"$revision_file_name\" ..\r\n");
	$revision_content= !file_exists($revision_path) ? false : file_get_contents($revision_path);
	$revision= false==$revision_content ? 0 : intval($revision_content);
	llog("    ..read: $revision\r\n\r\n");
	return $revision;
}

function DoUpdate()
{
	global $path_to_data_folder, $login, $password, $tab, $limit, $test_mode;
	if (!$test_mode)
	{
		llog("---------------------------------------------------\r\n");
		llog("start update\r\n   in folder \"$path_to_data_folder\"\r\n\r\n");
	}
	$access_token= verbose_Auth($login, $password);

	$revision_max_file_name= 'revision.max.txt';
	$revision_max_path= $path_to_data_folder.$revision_max_file_name;

	$last_revision= verbose_GetMaxRevision($revision_max_path, $revision_max_file_name);

	llog("get list of changed procedures\r\n$tab       from revision $last_revision,\r\n$tab       but no longer than $limit..\r\n");
	$changed_procedures= GetChangedProcedures($access_token,$last_revision+1,$limit);
	$changed_procedures_list= $changed_procedures->Запрошенный_список_процедур;
	$changed_procedures_total_count= $changed_procedures->Всего_после_указанной_в_запросе_ревизии_загружено_процедур;
	$changed_procedures_list_count= count($changed_procedures_list);
	llog(".. got list of $changed_procedures_list_count procedures from $changed_procedures_total_count.\r\n\r\n");

	$i= 0;
	foreach ($changed_procedures_list as $procedure)
	{
		$id_MProcedure= $procedure->id_MProcedure;
		$list_revision= $procedure->revision;
		llog("item $i from $changed_procedures_list_count (id_MProcedure:$id_MProcedure,revision:$list_revision)\r\n");
		llog("{\r\n");

		$revision_file_name= "$id_MProcedure/revision.txt";
		$revision_path= "$path_to_data_folder/$revision_file_name";
		$revision= verbose_GetRevision($revision_path, $revision_file_name, $id_MProcedure);

		if ($revision > $list_revision)
		{
			llog("    revision of downloaded procedure is $revision, more than revision $list_revision from list, so we do not have to download it again\r\n\r\n");
		}
		else
		{
			$revision= verbose_DownloadProcedureInfoAndStoreIt($access_token,$id_MProcedure,"$path_to_data_folder/$id_MProcedure",$list_revision);
			if (null!=$revision && $list_revision > $last_revision)
			{
				$last_revision= $list_revision;
				verbose_StoreMaxRevision($revision_max_path, $revision_max_file_name, $last_revision);
			}
		}

		llog("}\r\n\r\n");
		$i++;
	}
	llog("update is finished.\r\n");
}

function DoGetChangedProcedures()
{
	global $login, $password, $limit, $revision_from, $access_token, $case_number;
	if (null==$access_token)
		$access_token= Auth($login,$password);

	$changed_procedures_res= GetChangedProcedures($access_token,$revision_from,$limit,$case_number);
	if (null==$changed_procedures_res)
	{
		echo "can not GetChangedProcedures";
	}
	else
	{
		echo str_replace ("\n","\r\n",print_r($changed_procedures_res,true));
	}
}

function DoDownloadProcedureInfo()
{
	global $login, $password, $access_token, $id_MProcedure, $file_path;
	if (null!=$access_token)
	{
		echo "use access_token:$access_token\r\n";
	}
	else
	{
		echo "login:$login\r\n";
		$access_token= Auth($login,$password);
	}

	$download_result= DownloadProcedureInfo($access_token,$id_MProcedure);
	if (null==$download_result)
	{
		echo "can not DownloadProcedureInfo\r\n";
		echo "id_MProcedure: $id_MProcedure\r\n";
	}
	else
	{
		$zip_content= $download_result['zip_content'];
		$revision= $download_result['revision'];

		$file_size= file_put_contents($file_path, $zip_content);
		$file_name= basename($file_path);
		$md5= md5($zip_content);

		echo "ok DownloadProcedureInfo\r\n";
		echo "id_MProcedure: $id_MProcedure\r\n";
		echo "wrote to $file_name\r\n";
		echo "revision:$revision\r\n";
		echo "file size:$file_size\r\n";
		echo "md5:$md5\r\n";
	}
}

global $action;
$action= 'update';

function ProcessArguments()
{
	global $argv, $action, $login, $password, $test_mode, $tab, $category, $limit, $revision_from, $access_token, $case_number, $id_MProcedure, $file_path, $path_to_data_folder;
	$count= count($argv);
	for ($i= 1; $i<$count; $i++)
	{
		$ap= explode('=',$argv[$i]);
		switch ($ap[0])
		{
			case '--login': $login= $ap[1]; break;
			case '--password': $password = $ap[1]; break;
			case '--category': $category = $ap[1]; break;
			case '--action': $action = $ap[1]; break;
			case '--limit': $limit = $ap[1]; break;
			case '--revision-from': $revision_from = $ap[1]; break;
			case '--test-mode': $test_mode = true; $tab= "  "; break;
			case '--access-token': $access_token = $ap[1]; break;
			case '--case-number': $case_number = $ap[1]; break;
			case '--id-procedure': $id_MProcedure = $ap[1]; break;
			case '--file-path': $file_path = $ap[1]; break;
			case '--data-folder-path': $path_to_data_folder = $ap[1]; break;
		}
	}
}

function DoAction()
{
	global $action;
	switch ($action)
	{
		case 'auth': DoAuth(); break;
		case 'update': DoUpdate(); break;
		case 'get-changed-procedures': DoGetChangedProcedures(); break;
		case 'download-procedure-info': DoDownloadProcedureInfo(); break;
		default: echo "unknown action $action"; break;
	}
}

ProcessArguments();
DoAction();
