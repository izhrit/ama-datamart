@set MYSQLDIR=C:\Program Files\MySQL\MySQL Server 5.7\bin\
@set MYSQLHOST="localhost"
@set DBName="datamartdevel"

call %~dp0\prepdb.bat
call "%MYSQLDIR%mysql.exe" --defaults-extra-file=%~dp0\..\..\db\mysql.conf --host=%MYSQLHOST% %DBName% < %~dp0\..\..\db\sql\procedures.sql
call "%MYSQLDIR%\mysqldump.exe" --defaults-extra-file=%~dp0\..\..\db\mysql.conf --hex-blob --host=%MYSQLHOST% --default-character-set=utf8 --set-charset --routines --no-autocommit --extended-insert --result-file=%~dp0\backups\test_dump.sql %DBName%
