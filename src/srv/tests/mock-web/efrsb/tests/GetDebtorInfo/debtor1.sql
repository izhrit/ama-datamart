﻿insert into mock_efrsb_debtor
set 
	BankruptId=345786
	,Revision=100
	,Name='ООО "УК "ФЕДЕРАЦИЯ"'
	,INN='6671439284'
	,OGRN='1136671037953'
	,Body=compress('{"!Address":"СВЕРДЛОВСКАЯ, ЕКАТЕРИНБУРГ, БЕЛИНСКОГО, 132, 192"}')
;
