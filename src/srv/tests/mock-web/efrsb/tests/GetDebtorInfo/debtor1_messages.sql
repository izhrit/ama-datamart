﻿insert into mock_efrsb_message
set
	efrsb_id='1559661'
	,BankruptId=345786
	,PublishDate='2019-04-01'
	,Revision=100
	,Number='1559661'
	,MessageInfo_MessageType='a'
	,MessageGUID='C291D8F500118B1B44D451447A36329C'
	,Body=compress('<MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1559661</Id>
  <Number>1559661</Number>
  <CaseNumber>А60-40960/2016 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="21181" FirstName="Ольга" MiddleName="Николаевна" LastName="Щепина " INN="432800808256" SNILS="13934687095" RegistryNumber="">
      <OGRN />
      <Sro SroId="16">
        <SroName>САМРО «Ассоциация антикризисных управляющих» - Саморегулируемая межрегиональная общественная организация «Ассоциация антикризисных управляющих»</SroName>
        <SroRegistryNumber>012</SroRegistryNumber>
        <OGRN>1026300003751</OGRN>
        <INN>6315944042</INN>
        <LegalAddress>443072, г. Самара, Московское шоссе, 18-й км</LegalAddress>
      </Sro>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="ArbitralDecree">
    <CourtDecision>
      <Text>Определением Арбитражного суда Свердловской области от 18.01.17 по делу №А60-40960/2016 ООО "Управляющая компания "Федерация" (ОГРН 1136671037953, ИНН 6671439284, адрес: 620142, Свердловская обл., г. Екатеринбург,  ул.Белинского, д. 132, офис 192) введена процедура наблюдения. Временным управляющим утверждена Щепина Ольга Николаевна (ИНН 432800808256, СНИЛС 13934687095, почтовый адрес:610014,г.Киров,ул. Попова,д.30А, член САМРО «Ассоциация антикризисных управляющих», 443072, г. Самара, Московское шоссе, 18-й км, ИНН 6315944042, ОГРН 1026300003751). Требования кредиторов предъявляются в течение тридцати календарных дней с даты публикации сообщения о введении наблюдения в официальном издании в соответствии со ст. 28, 71 ФЗ № 127-ФЗ от 26.10.2002 "О несостоятельности (банкротстве)". Судебное заседание по рассмотрению дела о банкротстве назначено на 07.06.17 на 12:10.</Text>
      <DecisionType Name="Определение о введении наблюдения" Id="11" />
      <CourtDecree>
        <CourtId>97</CourtId>
        <CourtName>Арбитражный суд Свердловской области</CourtName>
        <FileNumber>А60-40960/2016 </FileNumber>
        <DecisionDate>2017-01-18</DecisionDate>
      </CourtDecree>
      <LossesFromArbitrManagerActionsAmount xsi:nil="true" />
      <CitizenNotReleasedFromResponsibility xsi:nil="true" />
      <CreditorClaimRegisterCloseDate xsi:nil="true" />
      <CreditorClaimSettingRequirementsExpirationDate xsi:nil="true" />
      <DecisionMadeDueTorCancellationRestructuringPlan xsi:nil="true" />
    </CourtDecision>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="96829" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ &quot;УПРАВЛЯЮЩАЯ КОМПАНИЯ &quot;ФЕДЕРАЦИЯ&quot;" ShortName="ООО &quot;УК &quot;ФЕДЕРАЦИЯ&quot;" PostAddress="СВЕРДЛОВСКАЯ, ЕКАТЕРИНБУРГ, БЕЛИНСКОГО, 132, 192" OKPO="" OGRN="1136671037953" LegalAddress="СВЕРДЛОВСКАЯ, ЕКАТЕРИНБУРГ, БЕЛИНСКОГО, 132, 192">
      <INN>6671439284</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2017-01-24T13:16:31.127</PublishDate>
  <BankruptId>133429</BankruptId>
  <MessageGUID>C291D8F500118B1B44D451447A36329C</MessageGUID>
</MessageData>')
;