<?
require_once '../assets/config.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $trace_methods;
if ($trace_methods)
	require_once '../assets/helpers/log.php';

$possible_file_actions= array
(
	 'find-debtor'=> 'FindDebtor'
	,'find-debtor-manager'=> 'FindDebtorManager'
	,'find-manager'=> 'FindManager'

	,'messages-brief'=> 'GetBriefMessages'
	,'message-brief'=> 'GetBriefMessage'

	,'messages'=> 'GetMessages'

	,'debtor-for-tk'=> 'GetDebtorForTK'
	,'debtors-for-select2'=> 'GetDebtorsForSelect2'
	,'managers-for-select2'=> 'GetManagersForSelect2'

	,'schedule.events'=> 'GetSchedule_info'

	,'get-debtor-info'=> 'GetDebtorInfo'

	,'manager-info'=> 'manager_info'
	,'start-info'=> 'GetStartInfo'

	, 'custom_query.list'=> 'custom_query/custom_query_list'
	, 'custom_query.execute'=> 'custom_query/custom_query_execute'

	, 'job-monitor'=> 'job-monitor'
);

$action= null;
if (isset($_GET['action']))
{
	$action= $_GET['action'];
}
else if (isset($_SERVER['ORIG_PATH_INFO']))
{
	$path= $_SERVER['ORIG_PATH_INFO'];
	$pos= strrpos($path,'/');
	if (false != $pos)
	{
		$action= substr($path,$pos+1);
		if ('api.php'==$action)
			$action= null;
	}
}
else if (isset($_SERVER['PHP_SELF']))
{
	$path= $_SERVER['PHP_SELF'];
	$pos= strrpos($path,'/');
	if (false != $pos)
	{
		$action= substr($path,$pos+1);
		if ('api.php'==$action)
			$action= null;
	}
}
///////////////////////////////////////////////////////////////////////////////
if (null==$action)
{
	$title= 'REST методы API базы данных ЕФРСБ от РИТ, используемого из Витрины данных ПАУ';
	$doc_link= 'docs/dm-api/index.html';
	require '../assets/views/action-undefined.php';
}
else
{
	global $trace_methods;

	if ($trace_methods)
		write_to_log('------'.$action.'--------------------------');

	if (!isset($possible_file_actions[$action]))
	{
		if ($trace_methods)
			write_to_log('   unknown action!');
		$title= 'REST методы API базы данных ЕФРСБ от РИТ, используемого напрямую из Витрины данных ПАУ';
		$doc_link= 'docs/dm-api/index.html';
		require '../assets/views/action-undefined.php';
	}
	else
	{
		$subpath= $possible_file_actions[$action];
		if (''==$subpath)
			$subpath= $action;
		try
		{
			require_once '../assets/actions/dm-api/'.$subpath.'.php';
		}
		catch (Exception $exception)
		{
			header("HTTP/1.1 500 Internal Server Error");
			require_once '../assets/helpers/log.php';
			write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
			write_to_log('_GET:');
			write_to_log($_GET);
			throw new Exception("can not execute action ".$action);
		}
	}
}
