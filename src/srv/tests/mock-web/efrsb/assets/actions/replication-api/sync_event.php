<?
require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $max_portion;
$max_portion= 2000; // ������������ ������

try
{
	if (!isset($_GET['revision-greater-than']))
		throw new Exception("skipped parameter revision-greater-than");
	$revision= $_GET['revision-greater-than'];
	$portion_size= !isset($_GET['portion-size']) ? $max_portion : $_GET['portion-size'];
	if ($portion_size>$max_portion)
		$portion_size= $max_portion;

	$time= !isset($_GET['later-than-time'])
		? date_create()
		: date_create_from_format('Y-m-d\TH:i:s',$_GET['later-than-time']);

	$sql_time= date_format($time,'Y-m-d H:i:s');

	$txt_query= "
	SELECT 
		e.ArbitrManagerID, e.BankruptId, e.EventTime, e.Revision, e.MessageInfo_MessageType, e.isActive,
		mes.Number, mes.MessageGUID, mes.efrsb_id, mes.PublishDate
	FROM {$tbl_prefix}event e
	inner join {$tbl_prefix}message mes on e.efrsb_id = mes.efrsb_id
	WHERE e.Revision > ? && e.EventTime >= ?
	order by e.Revision
	limit ?
	;";
	
	$events = execute_query($txt_query, array('ssi', $revision,$sql_time,$portion_size));
	write_to_log($events);
	header('Content-Type: text/plain');
	echo nice_json_encode($events);
}
catch (Exception $exception)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not execute sync_event!");
}
