<?
require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $max_portion;
$max_portion= 2000; // ������������ ������
try
{
	if (!isset($_GET['revision-greater-than']))
		throw new Exception("skipped parameter revision-greater-than");
	$revision= $_GET['revision-greater-than'];
	$portion_size= !isset($_GET['portion-size']) ? $max_portion : $_GET['portion-size'];
	if ($portion_size>$max_portion)
		$portion_size= $max_portion;

	$time= !isset($_GET['later-than-time'])
		? date_create()
		: date_create_from_format('Y-m-d\TH:i:s',$_GET['later-than-time']);
	$sql_time= date_format($time,'Y-m-d H:i:s');

	$txt_query= "
	SELECT efrsb_id, ArbitrManagerID, BankruptId, INN, SNILS, OGRN, 
		   PublishDate, Body, MessageInfo_MessageType, Number, MessageGUID, Revision
	FROM {$tbl_prefix}message
	WHERE Revision > ? && PublishDate >= ?
	order by Revision
	limit ?
	;";
	$messages = execute_query($txt_query, array('ssi', $revision,$sql_time,$portion_size));

	foreach ($messages as $m)
	{
		$m->Body= base64_encode($m->Body);
	}

	header('Content-Type: text/plain');
	echo nice_json_encode($messages);
}
catch (Exception $exception)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not execute sync_message!");
}
