<?php

/*

ВНИМАНИЕ!
данный сервис используется в микросервисах компании (ОРПАУ) для "репликации"

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $max_portion;
$max_portion= 200; // максимальная порция

try
{
	if (!isset($_GET['revision-greater-than']))
		throw new Exception("skipped parameter revision-greater-than");
	$revision= $_GET['revision-greater-than'];
	$portion_size= !isset($_GET['portion-size']) ? $max_portion : $_GET['portion-size'];
	if ($portion_size>$max_portion)
		$portion_size= $max_portion;

	$txt_query= "select 
	  id_Manager, ArbitrManagerID
	, SRORegNum
	, FirstName, MiddleName, LastName
	, OGRNIP, INN, SNILS
	, SRORegDate
	, DateDelete
	, RegNum
	, CorrespondenceAddress
	, Revision
	from {$tbl_prefix}manager
	where Revision>? 
	order by Revision
	limit ?;";

	$rows= execute_query($txt_query,array('ss',$revision,$portion_size));

	header('Content-Type: text/plain');
	echo nice_json_encode($rows);
}
catch (Exception $exception)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not execute get_changed_sro!");
}