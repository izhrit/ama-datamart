<?php

/*

ВНИМАНИЕ!
данный сервис используется в сервисе "Витрина данных ПАУ"
для проверки информации об АУ

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $max_portion;
$max_portion= 50; // максимальная порция искомых АУ

function get_string_arg($argname)
{
	global $_GET, $max_portion;
	if (!isset($_GET[$argname]) || ''==$_GET[$argname])
	{
		return null;
	}
	else
	{
		$arg= $_GET[$argname];
		$arg_parts= explode(",",$arg);
		if (count($arg_parts)>$max_portion)
			throw new Exception('too many $argname (more than $max_portion)');
		$fixed_args= array();
		foreach ($arg_parts as $arg_part)
		{
			$fixed_arg_part= preg_replace("/[^0-9,]/", "", $arg_part);
			if (''!=$fixed_arg_part)
				$fixed_args[]= '"'.$fixed_arg_part.'"';
		}
		write_to_log($fixed_args);
		$count_fixed_args= count($fixed_args);
		return 0==$count_fixed_args ? null : (object)array('txt'=>implode(',',$fixed_args),'count'=>$count_fixed_args);
	}
}

$inn= get_string_arg('inn');
$efrsb_num= get_string_arg('efrsb_num');
if (null==$inn && null==$efrsb_num)
	throw new Exception('skipped mandatory GET parameters inn!');

$txt_query= "select OGRNIP, INN, FirstName, LastName, MiddleName, ArbitrManagerID, RegNum, SRORegNum 
from {$tbl_prefix}manager where DateDelete is null and (";
$limit= 0;
if (null!=$inn)
{
	$txt_query.= " INN in ($inn->txt)";
	$limit+= $inn->count;
}
if (null!=$efrsb_num)
{
	if (null!=$inn)
		$txt_query.= " or";
	$txt_query.= " RegNum in ($efrsb_num->txt)";
	$limit+= $efrsb_num->count;
}
$txt_query.= ") order by ArbitrManagerID limit ?;";

$rows= execute_query($txt_query,array('i',$limit*2)); // по 2 варианта

header('Content-Type: text/plain');
echo nice_json_encode($rows);
