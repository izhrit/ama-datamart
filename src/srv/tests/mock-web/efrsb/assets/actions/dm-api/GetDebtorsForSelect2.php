<?php

/*

ВНИМАНИЕ!
данный сервис используется в сервисе "Витрина данных ПАУ"
дял получения информации о должниках, доступ к информации 
о которых можно получить:
- при регистрации наблюдателей на витрине
- при регистрации запросов наблюдателей на доступ к информации

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$q= !isset($_GET['q']) ? '' : $_GET['q'];
$txt_query= "
	select 
			d.BankruptId
		, d.Name d_Name, d.INN d_INN, d.OGRN d_OGRN, d.SNILS d_SNILS
		, m.LastName m_LastName, m.FirstName m_FirstName, m.MiddleName m_MiddleName
	from {$tbl_prefix}debtor d
	inner join {$tbl_prefix}manager m on m.ArbitrManagerID=d.ArbitrManagerID
	where d.Name like ? || d.INN=? || d.OGRN=? || d.SNILS=? limit 10;";
$args= array('ssss',$q.'%',$q,$q,$q);
$rows= execute_query($txt_query,$args);

$debtors= array();
foreach ($rows as $row)
{
	$debtor= (object)array();
	$debtor->id= $row->BankruptId;
	$debtor->text= $row->d_Name;
	$debtor->data= array(
		'Должник'=> array(
			'Наименование'=>$row->d_Name
			, 'ИНН'=>$row->d_INN
			, 'ОГРН'=>$row->d_OGRN
			, 'СНИЛС'=>$row->d_SNILS
		)
		,'АУ'=> array(
			'Фамилия'=>$row->m_LastName
			,'Имя'=>$row->m_FirstName
			,'Отчество'=>$row->m_MiddleName
		)
	);
	$debtors[]= $debtor;
}

header('Content-Type: text/plain');
echo nice_json_encode(array('results'=>$debtors));
