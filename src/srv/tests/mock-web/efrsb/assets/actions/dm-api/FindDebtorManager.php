<?php

/*

ВНИМАНИЕ!
данный сервис используется в сервисе "Витрина данных ПАУ"
для проверки информации о процедурах

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $max_portion;
$max_portion= 50; // максимальная порция искомых должников

function get_string_arg($argname)
{
	global $_GET, $max_portion;
	if (!isset($_GET[$argname]))
	{
		return array();
	}
	else
	{
		$arg= $_GET[$argname];
		$arg_parts= explode(",",$arg);
		if (count($arg_parts)>$max_portion)
			throw new Exception('too many $argname (more than $max_portion)');
		$fixed_args= array();
		foreach ($arg_parts as $arg_part)
			$fixed_args[]= '"'.preg_replace("/[^0-9,]/", "", $arg_part).'"';
		return $fixed_args;
	}
}

$ArbitrManagerIDs= get_string_arg('ArbitrManagerID');
$BankruptIds= get_string_arg('BankruptId');

$ArbitrManagerIDs_count= count($ArbitrManagerIDs);
$BankruptIds_count= count($BankruptIds);

if (0==$ArbitrManagerIDs_count || $ArbitrManagerIDs_count!=$BankruptIds_count)
	throw new Exception('bad mandatory GET parameters ArbitrManagerID|BankruptId!');

$txt_query= "select ArbitrManagerID, BankruptId, id_Debtor_Manager from {$tbl_prefix}debtor_manager where";
for ($i= 0; $i<$ArbitrManagerIDs_count; $i++)
{
	$txt_query.= ((0==$i) ? ' ' : ' or');
	$ArbitrManagerID= $ArbitrManagerIDs[$i];
	$BankruptId= $BankruptIds[$i];
	$txt_query.= " (ArbitrManagerID=$ArbitrManagerID and BankruptId=$BankruptId)";
}
$txt_query.= ' order by ArbitrManagerID, BankruptId limit ?;';

$rows= execute_query($txt_query,array('i',$max_portion*3*2)); // по 2 варианта для каждого из параметров

header('Content-Type: text/plain');
echo nice_json_encode($rows);
