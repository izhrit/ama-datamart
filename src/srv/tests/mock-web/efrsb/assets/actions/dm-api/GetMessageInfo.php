<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

/*

todo:
1. ����� ��������
2. �������� ��������� ������� � �� ����..
3. ��������� ��� ���������..

*/

function safe_prepare_where($row)
{
	$debtor = $row->debtor;
	if (isset($debtor->INN))
	{
		$inn = $debtor->INN;
		if (''==$inn)
			exit_bad_request('empty INN argument!');
		if ($inn!=preg_replace("/[^0-9]/", "", $inn))
			exit_bad_request('INN argument can contain only digits!');
		
		return "where d.INN = $inn";
	}
	else if (isset($debtor->OGRN))
	{
		$ogrn = $debtor->OGRN;
		if (''==$ogrn)
			exit_bad_request('empty OGRN argument!');
		if ($ogrn!=preg_replace("/[^0-9]/", "", $ogrn))
			exit_bad_request('OGRN argument can contain only digits!');

		return "where d.OGRN = $ogrn";
	}
	else if (isset($debtor->SNILS))
	{
		$snils = $debtor->SNILS;
		if (''==$snils)
			exit_bad_request('empty SNILS argument!');
		if ($snils!=preg_replace("/[^0-9]/", "", $snils))
			exit_bad_request('SNILS argument can contain only digits!');

		return "where d.SNILS = $snils";
	}
	else
	{
		return '';
	}
}
global $tbl_prefix; 
$rows= json_decode(file_get_contents('php://input'));
$result = array();

foreach($rows as $row) {
	try
	{
		$where= safe_prepare_where($row);

		if ('' != $where){
			$txt_query= "
				select
				 DATE_FORMAT(me.PublishDate, '%d.%m.%Y') PublishDate
				,me.MessageGUID MessageGUID
				,me.Number Number
				from {$tbl_prefix}message me
				inner join {$tbl_prefix}debtor d on d.Bankruptid=me.Bankruptid
				$where and me.PublishDate > ?
				order by me.PublishDate
				limit 1
				;";
			$mrows = execute_query($txt_query, array('s', $row->DateOfApplication));
			$mrows_count = count($mrows);
			if (1 == $mrows_count)
			{
				$mrow = $mrows[0];
				$mrow->id= $row->id;
				$result[] = $mrow;
			}
		}
	}
	catch (Exception $exception)
	{
		header("HTTP/1.1 500 Internal Server Error");
		write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
		write_to_log('$_GET:');
		write_to_log($_GET);
		throw new Exception("can not get message info for {$row->id}");
	}
}

header('Content-Type: text/plain');
echo nice_json_encode($result);