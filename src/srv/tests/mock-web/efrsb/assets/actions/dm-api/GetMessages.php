<?php

/*

ВНИМАНИЕ!
данный сервис планируется использовать в сервисе "Витрина данных ПАУ"
для демонстрации последних сообщений ЕФРСБ

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/jqgrid.php';

require_once '../assets/libs/events_news/prepare_event.php';
require_once '../assets/libs/events_news/prepare_event_time.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

function prepare_messages(&$rows)
{
	foreach($rows as $row)
	{
		$body= $row->Body;
		unset($row->Body);
		try
		{
			$extra= prepare_brief_message_extra($body, $row->MessageType);
			if (null!=$extra)
				$row->extra= $extra;
		}
		catch (Exception $ex)
		{
			write_to_log("Unhandled exception for brief message {$row->MessageGUID} occurred: " . get_class($ex) . ' - ' . $ex->getMessage());
			$row->Brief= '';
		}
	}
}

if (!isset($_GET['BankruptId']))
	throw new Exception('skipped mandatory argument BankruptId!');

$Bankruptids= preg_replace("/[^0-9,]/", "", $_GET['BankruptId']);
$fields= "
	DATE_FORMAT(me.PublishDate, '%d.%m.%Y %H:%i') PublishDate
	,me.Messageinfo_MessageType MessageType
	,me.Number Number
	,me.MessageGUID MessageGUID
	,UNCOMPRESS(me.Body) Body
	";
$from_where="
	from {$tbl_prefix}message me
	inner join {$tbl_prefix}debtor d on d.Bankruptid=me.Bankruptid
	where me.Bankruptid in ($Bankruptids)
	";
$filter_rule_builders= array(
	'PublishDate'=>function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		if (strlen($data) == 11)
		{
			$newdata= substr($data,-4). substr($data,-7,2). substr($data,-10,2);
			$operator= substr($data,0,1);
			if (in_array($operator,array('>','<','=')))
				return " and me.message $operator $newdata ";
		}
		return '';
	}
	,'Number'=> 'std_filter_rule_builder'
	,'MessageType'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND me.Messageinfo_MessageType = '$data'";
	}
);
header('Content-Type: text/plain');
$result= execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders, ' order by me.PublishDate desc, me.Number desc ');
prepare_messages($result['rows']);
echo nice_json_encode($result);
