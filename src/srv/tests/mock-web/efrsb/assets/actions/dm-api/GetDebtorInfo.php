<?

/*

ВНИМАНИЕ!
данный сервис используется в сервисе "Витрина данных ПАУ"
для получения информации о должнике и АУ, доступ к информации 
о которых можно получить:
- при подаче заявления на вступление в комитет кредиторов

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/validate.php';

$О_введении_процедур= array(
	 'о введении наблюдения'
	,'о признании гражданина банкротом'
	,'о признании должника банкротом'
	,'о введении финансового оздоровления'
	,'о введении внешнего управления'
);

function Судебный_акт_о_введении_процедуры($DecisionType_Name)
{
	global $О_введении_процедур;
	foreach ($О_введении_процедур as $txt) // проверка, что это сообщение о судебном акте на основаниии которого можно подать заявление
	{
		if (stripos(' '.$DecisionType_Name,$txt))
			return true;
	}
	return false;
}

global $result;
$result=(object) array();

function TryParseMessageForDebtor($message_body, $result)
{
	$xml = new SimpleXMLElement($message_body);
	if ('ArbitralDecree'==strval($xml->MessageInfo['MessageType'])) //проверка, что это сообщени о судебном акте
	{
		$decision_name= strval($xml->MessageInfo->CourtDecision->DecisionType['Name']);
		if (Судебный_акт_о_введении_процедуры($decision_name))
		{
			$firstword= strstr(strval($xml->MessageInfo->CourtDecision->Text).' ', ' ', true );

			$result->Cудебный_акт_о_введении_процедуры= array(
				'Тип'=>(stripos($firstword,'решен')!=false) ? 'решение' : 'определение'
				,'Номер'=>strval($xml->MessageInfo->CourtDecision->CourtDecree->FileNumber)
				,'Дата'=>strval($xml->MessageInfo->CourtDecision->CourtDecree->DecisionDate)
				,'Наименование'=>$decision_name
			);

			$result->Суд= array('Наименование'=>strval($xml->MessageInfo->CourtDecision->CourtDecree->CourtName));

			return true;
		}
	}
	return false;
}

function GetDebtorInformation($rows)
{
	global $result;
	foreach ($rows as $row)
	{
		if ('a'==$row->MessageType) // тип сообщения должен быть "сообщение о судебном акте"
		{
			if (TryParseMessageForDebtor($row->me_Body, $result))
			{
				$d_body= json_decode($row->d_Body);
				$result->Должник= array(
					'Наименование'=>$row->DebtorName
					,'ИНН'=>$row->DebtorINN
					,'ОГРН'=>$row->DebtorOGRN
					,'СНИЛС'=>$row->DebtorSNILS
					,'BankruptId'=>$row->BankruptId
					,'Адрес'=>(property_exists($d_body,"!Address")) ? $d_body->{"!Address"} : $d_body->{"!LegalAddress"}
				);

				$result->Сообщение_на_ефрсб_о_введении_процедуры= array(
					'PublishDate'=>$row->PublishDate
					,'Number'=>$row->Number
					,'MessageGUID'=>$row->MessageGUID
				);
				break;
			}
		}
	}
}

function ShortSroTitle($name)
{
	$parts= explode(' - ',$name);
	return $parts[0];
}

function GetManagerInformation($rows,$result)
{
	$len= count($rows);
	for ($i=0;$i<$len;$i++) //нахождение последнего сообщения от арбитражного управляющего
	{
		$row= $rows[$i];
		$xml = new SimpleXMLElement($row->me_Body);
		if(!isset($xml->PublisherInfo)) //проверка нового ли типа это сообещние
		{
			if (isset($xml->Publisher->Sro)) //проверка от арбитражного ли управляющего это сообщение
			{
				$result->АУ= array(
					'Фамилия'=>strval($xml->Publisher->Fio->LastName)
					,'Имя'=>strval($xml->Publisher->Fio->FirstName)
					,'Отчество'=>strval($xml->Publisher->Fio->MiddleName)
					,'СРО'=>ShortSroTitle(strval($xml->PublisherInfo->ArbitrManager->Sro->SroName))
					,'ИНН'=>strval($xml->Publisher->Inn)
					,'Номер_в_ЕФРСБ'=>$row->RegNum
					,'Адрес'=>(strval($xml->Publisher->CorrespondenceAddress)) ? strval($xml->Publisher->CorrespondenceAddress) :
						(strval($xml->Publisher->Sro->Address)) ? strval($xml->Publisher->Sro->Address) : ''
				);
				break;
			}
		}
		else
		{
			if ('ArbitrManager'==strval($xml->PublisherInfo['PublisherType']))
			{
				$result->АУ= array(
					'Фамилия'=>strval($xml->PublisherInfo->ArbitrManager['LastName'])
					,'Имя'=>strval($xml->PublisherInfo->ArbitrManager['FirstName'])
					,'Отчество'=>strval($xml->PublisherInfo->ArbitrManager['LastName'])
					,'СРО'=>ShortSroTitle(strval($xml->PublisherInfo->ArbitrManager->Sro->SroName))
					,'ИНН'=>strval($xml->PublisherInfo->ArbitrManager['INN'])
					,'Номер_в_ЕФРСБ'=>$row->RegNum
					,'Адрес'=>(strval($xml->PublisherInfo->ArbitrManager->CorrespondenceAddress)) ? strval($xml->PublisherInfo->ArbitrManager->CorrespondenceAddress) :
						(strval($xml->PublisherInfo->ArbitrManager->Sro->LegalAddress)) ? strval($xml->PublisherInfo->ArbitrManager->Sro->LegalAddress) : ''
				);
				break;
			}
		}
	}
}

CheckMandatoryGET('BankruptId');
$BankruptId= $_GET['BankruptId'];

$txt_query= "
	select 
		  d.Name DebtorName, d.INN DebtorINN, d.OGRN DebtorOGRN, d.SNILS DebtorSNILS, d.BankruptId BankruptId, UNCOMPRESS(d.Body) d_Body
		, UNCOMPRESS(me.Body) me_Body 
		, me.MessageInfo_MessageType MessageType
		, DATE_FORMAT(me.PublishDate, '%d.%m.%Y') PublishDate
		, me.MessageGUID
		, me.Number Number
		, m.RegNum RegNum
		, sro.ShortTitle
	from {$tbl_prefix}debtor d
	left join {$tbl_prefix}message me on d.BankruptId=me.BankruptId 
	left join {$tbl_prefix}manager m on m.ArbitrManagerID=me.ArbitrManagerID 
	left join {$tbl_prefix}sro sro on m.SRORegNum=sro.RegNum
	where d.BankruptId=?
	order by me.PublishDate desc;";
$args= array('s',$BankruptId);
$rows= execute_query($txt_query,$args);

$count_rows= count($rows);
if (0==$count_rows)
{
	exit_not_found("can not find BankruptId=$BankruptId");
}
else
{
	GetDebtorInformation($rows);
	GetManagerInformation($rows,$result);
	echo nice_json_encode($result);
}
