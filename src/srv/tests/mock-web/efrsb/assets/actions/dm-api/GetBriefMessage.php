<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/jqgrid.php';

require_once '../assets/libs/message/Brief.php';

$MessageInfo_MessageType_desciptions_by_db_value= array();
foreach ($MessageInfo_MessageType_desciptions as $d)
{
	$db_value= $d['db_value'];
	if (isset($MessageInfo_MessageType_desciptions_by_db_value[$db_value]))
		throw new Exception("duplicate db_value \"$db_value\"!");
	$MessageInfo_MessageType_desciptions_by_db_value[$db_value]= $d;
}

function prepare_brief_messages(&$rows)
{
	global $MessageInfo_MessageType_desciptions_by_db_value;
	foreach($rows as $row)
	{
		$MessageType= $row->MessageType;
		$MessageType= $MessageInfo_MessageType_desciptions_by_db_value[$MessageType]['api_name'];
		$body= $row->Body;
		unset($row->Body);
		try
		{
			$parsed_msg= parse_for_Message_brief($body,$MessageType);
			$row->Brief= get_Brief_text($parsed_msg->Text);

			$extra= prepare_brief_message_extra($body, $row->MessageType);
			if (null!=$extra)
				$row->extra= $extra;
		}
		catch (Exception $ex)
		{
			write_to_log("Unhandled exception for brief message {$row->MessageGUID} occurred: " . get_class($ex) . ' - ' . $ex->getMessage());
			$row->Brief= '';
		}
	}
}

set_error_handler(function($errno, $errstr, $errfile, $errline, $errcontext)
{
	if (0 === error_reporting()) // error was suppressed with the @-operator
		return false;

	throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

function safe_prepare_where()
{
	if (isset($_GET['news_id']))
	{
		$news_id= $_GET['news_id'];
		if (''==$news_id)
			exit_bad_request('empty BankruptId argument!');
		if ($news_id!=preg_replace("/[^0-9,]/", "", $news_id))
			exit_bad_request('news_id argument can contain only digits and commas!');
		
		return "where me.efrsb_id = $news_id";
	}
	else
	{
		exit_bad_request('skipped news_id argument!');
	}
}

global $tbl_prefix; 
$where= safe_prepare_where();
$fields= "
	DATE_FORMAT(me.PublishDate, '%d.%m.%Y') PublishDate
	,me.Messageinfo_MessageType MessageType
	,UNCOMPRESS(me.Body) Body
	,me.MessageGUID MessageGUID
	,d.Name Debtor
	";
$from_where="
	from {$tbl_prefix}message me
	inner join {$tbl_prefix}debtor d on d.Bankruptid=me.Bankruptid
	$where
	";
$filter_rule_builders= array(
	'PublishDate'=>'std_filter_rule_builder'
	,'Name'=> 'std_filter_rule_builder'
	,'MessageType'=> 'std_filter_rule_builder'
);
header('Content-Type: text/plain');
$result= execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders);
prepare_brief_messages($result['rows']);
echo nice_json_encode($result);
