<?

/*

ВНИМАНИЕ!
данный сервис планируется использовать в сервисе "Витрина данных ПАУ"
для отображения информации о АУ

*/
require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';


$field_name= null;
$field_value= null;

if (isset($_GET['inn']))
{
	$field_name= 'INN';
	$field_value= $_GET['inn'];
}
else if (isset($_GET['regnum']))
{
	$field_name= 'RegNum';
	$field_value= $_GET['regnum'];
}
else if (isset($_GET['ArbitrManagerID']))
{
	$field_name= 'ArbitrManagerID';
	$field_value= $_GET['ArbitrManagerID'];
}

if (null==$field_name)
	exit_bad_request("skipped manager key argument!");

$txt_query= "SELECT 
		  m.LastName
		, m.FirstName
		, m.MiddleName
		, m.RegNum
		, m.INN
		, m.ArbitrManagerID

		, m.SNILS
		, m.CorrespondenceAddress

		, (select 
				concat(email.address,'|',emsg.MessageGUID,'|',emsg.Number) 
			from {$tbl_prefix}email email
			inner join {$tbl_prefix}email_manager email_manager on email.id_Email=email_manager.id_Email 
			inner join {$tbl_prefix}message emsg on emsg.id_Message=email_manager.id_Message_Last 
			where email_manager.id_Manager=m.id_Manager
			order by emsg.PublishDate desc
			limit 1
		) email_id_Message

		, s.RegNum Регистрационный_номер
		, s.INN ИНН
		, s.OGRN ОГРН
		, s.UrAdress Адрес
		, s.Name Наименование
	from {$tbl_prefix}manager m
	left join {$tbl_prefix}sro s on m.SRORegNum = s.RegNum
	where m.$field_name=?
	order by m.RegNum, m.INN
	limit 2
	;";
$rows= execute_query($txt_query,array('s',$field_value));
$rows_count= count($rows);
if (1!=$rows_count)
	exit_not_found("found $rows_count rows for manager-info with $field_name=\"$field_value\"");

$manager= $rows[0];


header('Content-Type: application/json');
echo nice_json_encode($manager);