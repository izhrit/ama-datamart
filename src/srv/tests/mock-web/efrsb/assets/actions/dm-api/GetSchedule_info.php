<?

/*

ВНИМАНИЕ!
данный сервис планируется использовать в сервисе "Витрина данных ПАУ"
для отображения событий календаря ЕФРСБ

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

$txt_query= "
SELECT
	e.id_Event, 
	e.EventTime, 
	uncompress(mes.Body) Body,
	mes.MessageGUID,
	mes.Number,
	e.MessageInfo_MessageType 
FROM {$tbl_prefix}event e
inner join {$tbl_prefix}message mes on mes.efrsb_id=e.efrsb_id
where e.EventTime between ? AND ?
AND isActive = 1
";

if (isset($_GET['BankruptId']))
{
	$Bankruptids= preg_replace("/[^0-9,]/", "", $_GET['BankruptId']);
	if (''==$Bankruptids)
		exit_bad_request('bad parameter BankruptId');

	$txt_query.= " AND e.BankruptId in ($Bankruptids)";
}
else if (isset($_GET['ArbitrManagerID']))
{
	$ArbitrManagerids= preg_replace("/[^0-9,]/", "", $_GET['ArbitrManagerID']);
	if (''==$ArbitrManagerids)
		exit_bad_request('bad parameter ArbitrManagerID');

	$txt_query.= "  AND e.ArbitrManagerID in ($ArbitrManagerids)";
}
else
{
	exit_bad_request('skipped parameter BankruptId/ArbitrManagerID');
}

$txt_query.= " order by e.EventTime;";

require_once '../assets/libs/events_news/prepare_event.php';
require_once '../assets/libs/events_news/prepare_event_text.php';
require_once '../assets/libs/events_news/check_schedule_start_end.php';

$rows= execute_query($txt_query,array("ss", $_GET['start'], $_GET['end']));

$events = array();
foreach ($rows as $row)
{
	try
	{
		$event0= prepare_event($row);

		$event= (object)array();
		
		if (!isset($_GET['structured']))
		{
			prepare_event_text($event0,$event);
		}
		else
		{
			prepare_event_structured_fields($event0,$event);
		}

		$event->start= $event0->time;
		$event->url= 'https://old.bankrot.fedresurs.ru/MessageWindow.aspx?ID='.$event0->guid;

		$events[]= $event;
	}
	catch (Exception $ex)
	{
		write_to_log('Unhandled exception occurred: ' . get_class($ex) . ' - ' . $ex->getMessage());
		write_to_log('Body:');
		write_to_log($row->Body);
	}
}

header('Content-Type: application/json');
echo nice_json_encode($events);
