<?php

/*

ВНИМАНИЕ!
данный сервис используется в сервисе "Витрина данных ПАУ"
для выбора АУ при голосовании на премию АУ

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$q= !isset($_GET['q']) ? '' : $_GET['q'];

$txt_query= "
	select 
			m.ArbitrManagerID
		, m.LastName
		, m.FirstName
		, m.MiddleName
		, m.RegNum
		, m.INN INN
		, m.SRORegNum
		, s.Name SroName
	from {$tbl_prefix}manager m
	inner join {$tbl_prefix}sro s on m.SRORegNum=s.RegNum
	where (LastName like ? or m.RegNum=? or m.INN=?)
	&& m.SRORegNum is not null
	&& s.RegNum is not null
	&& m.DateDelete is null
	&& m.INN<>''
	limit 10
;";

$rows= execute_query($txt_query,array('sss',$q.'%',$q,$q));

$managers= array();
foreach ($rows as $row)
{
	$manager= (object)array();
	$manager->id= $row->ArbitrManagerID;
	$manager->text= "$row->LastName $row->FirstName $row->MiddleName (ИНН:$row->INN)";
	$data= (object)array();
	$data->Фамилия= $row->LastName;
	$data->Имя= $row->FirstName;
	$data->Отчество= $row->MiddleName;
	$data->efrsbNumber= $row->RegNum;
	$data->inn= $row->INN;
	$data->СРО= null!=$row->SroName ? $row->SroName : "№$row->SRORegNum";
	$manager->data= $data;
	$managers[]= $manager;
}

echo nice_json_encode(array('results'=>$managers));
