<?php

/*

ВНИМАНИЕ!
данный сервис используется в сервисе "Витрина данных ПАУ"
для проверки информации о должниках

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $max_portion;
$max_portion= 50; // максимальная порция искомых должников

function get_string_arg($argname)
{
	global $_GET, $max_portion;
	if (!isset($_GET[$argname]))
	{
		return '';
	}
	else
	{
		$arg= $_GET[$argname];
		$arg_parts= explode(",",$arg);
		if (count($arg_parts)>$max_portion)
			throw new Exception('too many $argname (more than $max_portion)');
		$fixed_args= array();
		foreach ($arg_parts as $arg_part)
			$fixed_args[]= '"'.preg_replace("/[^0-9,]/", "", $arg_part).'"';
		return implode(',',$fixed_args);
	}
}

$inn= get_string_arg('inn');
$snils= get_string_arg('snils');
$ogrn= get_string_arg('ogrn');

if (''==$inn && ''==$snils && ''==$ogrn)
	throw new Exception('skipped mandatory GET parameters inn|snils|ogrn!');

$txt_query= "select Name, INN, SNILS, OGRN, BankruptId from {$tbl_prefix}debtor where 0=1 ";
if (''!=$inn)
	$txt_query.= " or INN in ($inn)";
if (''!=$ogrn)
	$txt_query.= " or OGRN in ($ogrn)";
if (''!=$snils)
	$txt_query.= " or SNILS in ($snils)";
$txt_query.= ' order by BankruptId limit ?;';

$rows= execute_query($txt_query,array('i',$max_portion*3*2)); // по 2 варианта для каждого из параметров inn, ogrn, snils

header('Content-Type: text/plain');
echo nice_json_encode($rows);
