<?php

function get_current_datetime()
{
	global $current_date_time;
	if (null==$current_date_time)
	{
		return date_format(date_create(), 'Y-m-d\TH:i:s');
	}
	else
	{
		$current_date_time= date_add($current_date_time, date_interval_create_from_date_string('1 second'));
		return date_format($current_date_time, 'Y-m-d\TH:i:s');
	}
}

