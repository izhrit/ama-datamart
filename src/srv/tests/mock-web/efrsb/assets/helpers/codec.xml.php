<?php

class XmlErrorException extends Exception
{
	public $errors= null;
	public function __construct($message = null, $code = 0)
	{
		parent::__construct($message, $code);
		$this->errors= array();
		$errors= libxml_get_errors();
		foreach ($errors as $error)
		{
			$this->errors[]= $error;
		}
		libxml_clear_errors();
	}
}

class Xml_codec
{
	function EncodeDataItem($dom, $field_value, $field_name, $schema, $xlmns)
	{
		if (!is_array($field_value) && !is_object($field_value))
		{
			$node= $dom->createElement($field_name);
			$node->appendChild($dom->createTextNode($field_value));
			return $node;
		}
		else
		{
			$node= $dom->createElement($field_name);
			if (null!=$xlmns)
				$node->setAttribute('xmlns',$xlmns);
			foreach ($field_value as $index => $item)
			{
				if (is_int($index))
				{
					$item_schema = (null==$schema || !isset($schema['item'])) ? null : $schema['item'];
					$item_name = $this->getTagName($item_schema, 'item');
					$node->appendChild($this->EncodeDataItem($dom, $item, $item_name, $item_schema, null));
				}
				else
				{
					$field_schema = (null==$schema || !isset($schema['fields']) || !isset($schema['fields'][$index])) ? null : $schema['fields'][$index];
					$node->appendChild($this->EncodeDataItem($dom, $item, $index, $field_schema, null));
				}
			}
			return $node;
		}
	}

	function getTagName($schema, $default_name)
	{
		return (null==$schema || !isset($schema['tagName'])) ? $default_name : $schema['tagName'];
	}

	public $options= 0;
	public $schema= null;
	public $use_attributes= false;

	public function GetRootNamespaceURI()
	{
		return null;
	}

	public function Encode_to_dom($data)
	{
		$dom= new DOMDocument;
		$dom->formatOutput=true;
		$dom->encoding= 'utf-8';
		$root= $this->EncodeDataItem($dom, $data, $this->getTagName($this->schema, 'root'), $this->schema, $this->GetRootNamespaceURI());
		$dom->appendChild($root);
		return $dom;
	}

	public function Encode($data)
	{
		$dom= $this->Encode_to_dom($data);
		return $dom->saveXML();
	}

	function is_ass_array($data)
	{
		$i= 0;
		foreach ($data as $ind => $value)
		{
			if ($i!==$ind)
			{
				return true;
			}
			$i= $i+1;
		}
		return false;
	}

	function DecodeXmlElement($element, $schema)
	{
		$model = null;
		$childs = $element->children();
		$childs_len = count($childs);
		for ($i = 0; $i < $childs_len; $i++)
		{
			$child = $childs[$i];
			if ($child->getName())
			{
				$tname = $child->getName();
				if (null == $model)
				{
					if (null!=$schema && isset($schema['type']) && 'array' == $schema['type'])
					{
						$model = array($this->DecodeXmlElement($child, $schema['item']));
					}
					else
					{
						$model = array();
						$field_schema = (null==$schema || !isset($schema['fields']) || !isset($schema['fields'][$tname])) ? null : $schema['fields'][$tname];
						$field_value = $this->DecodeXmlElement($child, $field_schema);
						$model[$tname] = $field_value;
					}
				}
				else
				{
					if (is_array($model) && !$this->is_ass_array($model))
					{
						$item_schema = (null==$schema || !isset($schema['item'])) ? null : $schema['item'];
						$model[]= $this->DecodeXmlElement($child, $item_schema);
					}
					else
					{
						if (isset($model[$tname]))
						{
							$item_schema = (null==$schema || !isset($schema['item'])) ? null : $schema['item'];
							$model = array($model[$tname], $this->DecodeXmlElement($child, $item_schema));
						}
						else
						{
							$field_schema = (null==$schema || !isset($schema['fields']) || !isset($schema['fields'][$tname])) ? null : $schema['fields'][$tname];
							$model[$tname] = $this->DecodeXmlElement($child, $field_schema);
						}
					}
				}
			}
		}
		if ($this->use_attributes)
			$model= $this->GetElementAttributes($element,$model);
		if (null == $model)
			$model = $this->GetElementText($element,$schema); // под IE работало просто text..
		if (null!=$schema && isset($schema['tag_name_to_field']))
			$model[$schema['tag_name_to_field']]= $element->getName();
		return (!is_array($model) || !$this->is_ass_array($model)) ? $model : (object)$model;
	}

	function GetElementAttributes($element,$model)
	{
		foreach($element->attributes() as $name => $value)
		{
			$tname= "!$name";
			$tvalue= (string)$value;
			$tvalue= null==$tvalue ? '' : $tvalue;
			if (null == $model)
			{
				$model= array($tname => $tvalue);
			}
			else
			{
				$model[$tname] = $tvalue;
			}
		}
		return $model;
	}

	function GetElementText($element,$schema)
	{
		$res= (string)$element;
		if (''==$res && isset($schema) && null!=$schema && isset($schema['null_if_empty']) && true==$schema['null_if_empty'])
		{
			return null;
		}
		else if (null!=$res)
		{
			return $res;
		}
		else
		{
			if (isset($schema) && null!=$schema && isset($schema['nullable']) && true==$schema['nullable'])
				return null;
			return '';
		}
	}

	public function Decode($txt)
	{
		$options= 0;
		if (isset($this->options))
			$options= $this->options;
		$element= new SimpleXMLElement($txt,$options);
		$res= $this->DecodeXmlElement($element, $this->schema);
		$element= null;
		return $res;
	}
}

?>