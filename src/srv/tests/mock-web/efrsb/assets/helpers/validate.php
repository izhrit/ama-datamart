<?php

function exit_internal_server_error($error_text)
{
	header("HTTP/1.1 500 Internal Server Error");
	echo 'Internal Server Error';
	exit_with_error($error_text);
}

function exit_not_found($error_text)
{
	header("HTTP/1.1 404 Not Found");
	echo 'Not Found';
	exit_with_error($error_text);
}

function exit_bad_request($error_text)
{
	header("HTTP/1.1 400 Bad Request");
	echo 'bad request';
	exit_with_error($error_text);
}

function exit_unauthorized($error_text)
{
	header("HTTP/1.1 401 Unauthorized");
	echo 'Unauthorized';
	exit_with_error($error_text);
}

function exit_with_error($error_text)
{
	write_to_log($error_text);
	if (function_exists('write_to_log_auth_info'))
		write_to_log_auth_info();
	write_to_log('$_GET:');
	write_to_log($_GET);
	exit;
}

function CheckMandatoryGET($name)
{
	if (!isset($_GET[$name]))
		exit_bad_request("skipped _GET['$name']");
	return $_GET[$name];
}

function CheckMandatoryGET_id($name)
{
	$res= CheckMandatoryGET($name);
	if (!is_numeric($res))
		exit_bad_request("bad numeric _GET['$name']");
	return $res;
}
