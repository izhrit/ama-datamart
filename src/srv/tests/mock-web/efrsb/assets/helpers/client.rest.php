<?php

class Rest_exception extends Exception
{
	public $responce= null;
	public function __construct($msg,$responce)
	{
		parent::__construct($msg);
		$this->responce= $responce;
	}
}

class Rest_client
{
	private $curl= null;

	public function __construct()
	{
		$this->init_curl();
	}

	protected function std_opt_array()
	{
		$curl_opt_array= array(
			  CURLOPT_FOLLOWLOCATION => true
			, CURLOPT_CONNECTTIMEOUT => 5
			, CURLOPT_TIMEOUT => 5
			, CURLOPT_RETURNTRANSFER => 1
		);

		global $path_to_cookie;
		$curl_opt_array[CURLOPT_COOKIEJAR]= $path_to_cookie;
		$curl_opt_array[CURLOPT_COOKIEFILE]= $path_to_cookie;

		return $curl_opt_array;
	}

	function init_curl()
	{
		$this->curl = curl_init();
		curl_setopt_array($this->curl, $this->std_opt_array());
	}

	public function reset_curl()
	{
		if (null!=$this->curl)
			curl_close($this->curl);
		$this->init_curl();
	}

	public function request($opt_array)
	{
		curl_setopt($this->curl, CURLOPT_PUT, false);
		curl_setopt($this->curl, CURLOPT_HTTPGET, false);
		curl_setopt($this->curl, CURLOPT_POST, false);
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, array());
		if (!isset($opt_array[CURLOPT_POSTFIELDS]))
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, null);
		
		curl_setopt_array($this->curl, $opt_array);

		$curl_response= curl_exec($this->curl);

		$httpcode= curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
		if (200!=$httpcode)
			throw new Rest_exception("got HTTP_CODE=$httpcode, curl_error=".curl_error($this->curl),$curl_response);

		return $curl_response;
	}

	public function safe_request($opt_array)
	{
		curl_setopt_array($this->curl, $opt_array);
		$count= 0;
		do
		{
			$curl_response= curl_exec($this->curl);
			$httpcode= curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
			$count++;
		}
		while (0==$httpcode && $count<5);
		if (200!=$httpcode)
			throw new Exception("got HTTP_CODE=$httpcode, curl_error=".curl_error($this->curl));
		return $curl_response;
	}

	public function close()
	{
		curl_close($this->curl);
	}
}

class Logged_rest_client extends Rest_client
{
	public $logger= null;

	public function __construct($logger)
	{
		parent::__construct();
		$this->logger= $logger;
	}

	protected function prepare_url_to_log($url)
	{
		return $url;
	}

	public function request($opt_array)
	{
		$url= $this->prepare_url_to_log($opt_array[CURLOPT_URL]);
		$this->logger->push("������ \"$url\" ..");

		$res= parent::request($opt_array);

		$this->logger->pop_(".. ����� ������� ����� %timespan%");
		return $res;
	}

	public function safe_request($opt_array)
	{
		$url= $this->prepare_url_to_log($opt_array[CURLOPT_URL]);
		$this->logger->push("������ \"$url\" ..");

		$res= parent::safe_request($opt_array);

		$this->logger->pop_(".. ����� ������� ����� %timespan%");
		return $res;
	}
}
