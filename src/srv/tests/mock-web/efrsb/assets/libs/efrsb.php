<?php

require_once '../assets/config.php';
require_once '../assets/helpers/client.rest.php';
require_once '../assets/helpers/codec.xml.php';

class DebtorList_codec extends Xml_codec
{
	public $use_attributes= true;
	public $schema= array(
		'tagName'=>'DebtorList'
		,'fields'=>array
		(
			'DebtorList'=>array
			(
				'type'=>'array'
				,'item'=>array( 'tag_name_to_field'=>'DebtorType', 'fields'=>array
					(
						'LegalCaseList'=>array('type'=>'array','item'=>array('tagName'=>'LegalCaseInfo',
							'fields'=>array
							(
								'IsApplicantCreditOrg'=>array('null_if_empty'=>true)
								,'IsLiabilitySecured'=>array('null_if_empty'=>true)
							)
						))
						,'NameHistory'=>array('type'=>'array','item'=>array('tagName'=>'NameHistoryItem'))
					) )
			)
		)
	);
}

class DebtorRegisterItem_codec extends Xml_codec
{
	public $use_attributes= true;
	public $schema= array( 'tagName'=>'DebtorRegisterItem', 'fields'=>array
	(
		'LegalCaseList'=>array('type'=>'array','item'=>array('tagName'=>'LegalCaseInfo',
			'fields'=>array
			(
				'IsApplicantCreditOrg'=>array('null_if_empty'=>true)
				,'IsLiabilitySecured'=>array('null_if_empty'=>true)
			)
		))
		,'NameHistory'=>array('type'=>'array','item'=>array('tagName'=>'NameHistoryItem'))
	) );
}

class AMList_codec extends Xml_codec
{
	public $use_attributes= true;
	public $schema= array(
		'tagName'=>'AMList'
		,'fields'=>array
		(
			'AMList'=>array
			(
				'type'=>'array'
				,'item'=>array
				(
					'tagName'=>'ArbitrManager'
				)
			)
		)
	);
}

class SROList_codec extends Xml_codec
{
	public $use_attributes= true;
	public $schema= array(
		'tagName'=>'SROList'
		,'fields'=>array
		(
			'SROList'=>array
			(
				'type'=>'array'
				,'item'=>array
				(
					'tagName'=>'SRO'
					,'fields'=>array
					(
						'AMList'=>array
						(
							'type'=>'array'
							,'item'=>array
							(
								'tagName'=>'ArbitrManager'
								,'fields'=>array
								(
									'DateAffiliations'=>array ( 'type'=>'array','item'=>array('tagName'=>'DateAffiliation') )
								)
							)
						)
					)
				)
			)
		)
	);
}

function compare_LegalCaseInfo($a,$b)
{
	return $a->Number < $b->Number ? -1 : ($a->Number == $b->Number ? 0 : 1);
}

function compare_by_DateLastModif($a,$b)
{
	$DateLastModif= '!DateLastModif';
	$da= date_create($a->$DateLastModif);
	$db= date_create($b->$DateLastModif);
	return ($da < $db) ? -1 : (($da == $db) ? 0 : 1);
}

function debtor_LastPublishDateTime($a)
{
	$aLastReportDate= date_create((!isset($a->LastReportDate) || null==$a->LastReportDate || ''==$a->LastReportDate) ? '2001-01-01' : $a->LastReportDate);
	$aLastMessageDate= date_create((!isset($a->LastMessageDate) || null==$a->LastMessageDate || ''==$a->LastMessageDate) ? '2001-01-01' : $a->LastMessageDate);
	return ($aLastReportDate>$aLastMessageDate) ? $aLastReportDate : $aLastMessageDate;
}

function compare_DebtorPublishDateTime($a,$b)
{
	$da= $a->LastPublishDateTime;
	$db= $b->LastPublishDateTime;
	return ($da < $db) ? -1 : (($da == $db) ? 0 : 1);
}

if (!function_exists("mb_str_replace")) 
{
    function mb_str_replace($needle, $replace_text, $haystack) {
        return implode($replace_text, mb_split($needle, $haystack));
    }
}

class EfrsbClient extends Rest_client
{
	public $base_url= null;

	public function __construct($args= null)
	{
		parent::__construct();
		global $base_efrsb_proxy_url;
		$this->base_url= null!=$args ? $args->base_url : $base_efrsb_proxy_url;
	}

	function std_GET($url_method_args)
	{
		$request_result= $this->request(array(
			CURLOPT_URL => $this->base_url.$url_method_args,
			CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPGET => true, CURLOPT_POST => false, CURLOPT_PUT=> false
		));
		return $request_result;
	}

	function GetMessageContent($id)
	{
		return $this->std_GET("/GetMessageContent?id=$id");
	}

	function GetDebtorByIdBankrupt($id)
	{
		$request_result= $this->std_GET("/GetDebtorByIdBankrupt?idBankrupt=$id");

		$request_result= mb_str_replace('<q1:','<',$request_result);
		$request_result= mb_str_replace('</q1:','</',$request_result);

		$codec= new DebtorRegisterItem_codec();
		$result= $codec->Decode($request_result);

		self::FixDebtor($result);

		$fLastName= '!LastName';
		$result->DebtorType= (isset($result->$fLastName)) ? 'DebtorPerson' : 'DebtorCompany';

		return $result;
	}

	function GetMessageIds($startDate,$endDate= null)
	{
		$url= (null==$endDate) 
			? "/GetMessageIds?startDate=$startDate"
			: "/GetMessageIds2?startDate=$startDate&endDate=$endDate";
		$request_result= $this->std_GET($url);
		return json_decode($request_result);
	}

	function xsi_date_tomorrow()
	{
		$date = date_create();
		date_add($date, date_interval_create_from_date_string('10 days'));
		$res= date_format($date, 'Y-m-d');
		return $res;
	}

	static function FixDebtor(&$debtor)
	{
		if (isset($debtor->LegalCaseList))
			usort($debtor->LegalCaseList,'compare_LegalCaseInfo');
		$debtor->LastPublishDateTime= debtor_LastPublishDateTime($debtor);
	}

	static function FixDebtorList($debtor_list)
	{
		$result= array();

		foreach ($debtor_list as $debtor)
		{
			self::FixDebtor($debtor);
			$result[]= $debtor;
		}

		return $result;
	}

	function GetDebtorsByLastPublicationPeriod($startDate,$endDate= null)
	{
		$fixedEndDate= (null!=$endDate ? $endDate : $this->xsi_date_tomorrow());
		$request_result= $this->std_GET("/GetDebtorsByLastPublicationPeriod?startDate=$startDate&endDate=$fixedEndDate");

		$codec= new DebtorList_codec();
		$result= $codec->Decode($request_result);

		$result= self::FixDebtorList($result->DebtorList);
		usort($result,'compare_DebtorPublishDateTime');

		return $result;
	}

	function GetDebtorRegister($date)
	{
		$request_result= (null==$date)
			? $this->std_GET("/GetDebtorRegisterAll")
			: $this->std_GET("/GetDebtorRegister?date=$date");

		$codec= new DebtorList_codec();
		$result= $codec->Decode($request_result);

		$result= self::FixDebtorList($result->DebtorList);
		usort($result,'compare_DebtorPublishDateTime');

		return $result;
	}

	function GetArbitrManagerRegister($date)
	{
		$request_result= $this->std_GET("/GetArbitrManagerRegister?date=$date");

		$codec= new AMList_codec();
		$result= $codec->Decode($request_result);

		$manager_array= $result->AMList;
		usort($manager_array,'compare_by_DateLastModif');

		return $manager_array;
	}

	function GetSroRegister($date)
	{
		$request_result= $this->std_GET("/GetSroRegister?date=$date");

		$codec= new SROList_codec();
		$result= $codec->Decode($request_result);

		$sro_array= $result->SROList;
		usort($sro_array,'compare_by_DateLastModif');

		return $sro_array;
	}
}

$efrsb_client_count_using= 0;
$efrsb_client= null;

function SafePrepareEfrbClient()
{
	global $efrsb_client, $efrsb_client_count_using, $efrsb_api_params;
	if (null==$efrsb_client || $efrsb_client_count_using>$efrsb_api_params->recreate_client_count)
	{
		unset($efrsb_client);
		$efrsb_client= new EfrsbClient();
		$efrsb_client_count_using= 0;
	}
	$efrsb_client_count_using++;
	return $efrsb_client;
}
