<?php

$current_date_time= null;
function get_current_datetime()
{
	global $current_date_time;
	if (null==$current_date_time)
	{
		return date_format(date_create(), 'Y-m-d\TH:i:s');
	}
	else
	{
		$current_date_time= date_add($current_date_time, date_interval_create_from_date_string('1 second'));
		return date_format($current_date_time, 'Y-m-d\TH:i:s');
	}
}

function safe_store_test_time()
{
	global $test_settings;
	if (isset($test_settings) && isset($test_settings->use_test_time) && true==$test_settings->use_test_time)
	{
		global $_SESSION;
		if (isset($_GET['use-test-time']) && ''!=$_GET['use-test-time'])
		{
			$test_time= $_GET['use-test-time'];
			$_SESSION['current-test-time']= $test_time;
			write_to_log("store_test_time $test_time");
			$_GET['use-test-time']= '';
		}
	}
}

function safe_date_create()
{
	global $test_settings;
	if (isset($test_settings) && isset($test_settings->use_test_time) && true==$test_settings->use_test_time)
	{
		global $_SESSION;
		if (isset($_GET['use-test-time']) && ''!=$_GET['use-test-time'])
		{
			$res= date_create_from_format('Y-m-d\TH:i:s',$_GET['use-test-time']);
			if (null==$res)
				$res= new DateTime($_GET['use-test-time'], new DateTimeZone('UTC'));
			$_SESSION['current-test-time']= date_format($res,'Y-m-d\TH:i:s');
			$_GET['use-test-time']= '';
			return $res;
		}
		else if (isset($_SESSION['current-test-time']))
		{
			$res= date_create_from_format('Y-m-d\TH:i:s',$_SESSION['current-test-time']);
			$res= date_add($res, date_interval_create_from_date_string('1 second'));
			$_SESSION['current-test-time']= date_format($res,'Y-m-d\TH:i:s');
			return $res;
		}
	}
	return date_create();
}
