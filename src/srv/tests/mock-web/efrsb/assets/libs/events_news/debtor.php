<?

function prepare_fio($txt)
{
	$parts= explode(' ',$txt);
	$parts_count= count($parts);
	if (1>=$parts_count)
	{
		return $txt;
	}
	else
	{
		$res= $parts[0];
		$spaced= false;
		if (3<=$parts_count)
		{
			$res.= ' '.mb_substr($parts[$parts_count-2],0,1);
			$spaced= true;
		}
		if (2<=$parts_count)
		{
			if (!$spaced)
				$res.= ' ';
			$res.= mb_substr($parts[$parts_count-1],0,1);
		}
		return $res;
	}
}

global $debtorName_beauty_prefixes;
$debtorName_beauty_prefixes= array(
   array('prefix'=>'общество с ограниченной ответственностью', 'change_to'=>'ООО')
  ,array('prefix'=>'открытое акционерное общество', 'change_to'=>'ОАО')
  ,array('prefix'=>'индивидуальный предприниматель', 'change_to'=>'ИП')
  ,array('prefix'=>'публичное акционерное общество', 'change_to'=>'ПАО')
  ,array('prefix'=>'закрытое акционерное общество', 'change_to'=>'ЗАО')
  ,array('prefix'=>'акционерное общество', 'change_to'=>'АО')
  ,array('prefix'=>'управление федеральной налоговой службы', 'change_to'=>'УФНС')
  ,array('prefix'=>'инспекция федеральной налоговой службы россии', 'change_to'=>'ИФНС')
  ,array('prefix'=>'инспекция федеральной налоговой службы', 'change_to'=>'ИФНС')
  ,array('prefix'=>'государственное казенное учреждение', 'change_to'=>'ГКУ')
  ,array('prefix'=>'управляющая компания', 'change_to'=>'УК')
  ,array('prefix'=>'муниципальное унитарное предприятие', 'change_to'=>'МУП')
  ,array('prefix'=>'научно производственное объединение', 'change_to'=>'НПО')
  ,array('prefix'=>'межрайонная инспекция федеральной налоговой службы', 'change_to'=>'МИФНС')
  ,array('prefix'=>'кредитный потребительский кооператив', 'change_to'=>'КПК')
);

function excludePrefix($txt, $prefix)
{
	$prefix_len= mb_strlen($prefix);
	if ($prefix_len < mb_strlen($txt))
	{
		$txtPrefix = mb_strtolower(mb_substr($txt, 0, $prefix_len));
		if ($txtPrefix == $prefix)
			return mb_substr($txt,$prefix_len);
	}
	return $txt;
}

function excludePrefix_and_trim($txt, $prefix)
{
	$res= excludePrefix($txt, $prefix);
	if (mb_strlen($res)!=mb_strlen($txt))
		$res= trim($res);
	return $res;
}

function prepare_debtor($txt)
{
	if (null==$txt)
	{
		return $txt;
	}
	{
		$res= trim($txt);
		if (0!=mb_strlen($res))
		{
			global $debtorName_beauty_prefixes;
			foreach ($debtorName_beauty_prefixes as $change)
			{
				$res= excludePrefix_and_trim($res,$change['prefix']);
				$res= excludePrefix_and_trim($res,mb_strtolower($change['change_to']));
			}
		}
		return trim_and_fix_quotes($res);
	}
}
