<?

require_once '../assets/libs/events_news/debtor.php';

global $event_type_descr;
$event_type_descr= array(
	'a'=> array('prefix'=>'Суд: ', 'title'=>'судебное заседание')
	,'b'=> array('prefix'=>'Торги: ', 'title'=>'торги')
	,'q'=> array('prefix'=>'КК: ', 'title'=>'заседание комитета кредиторов')
	,'c'=> array('prefix'=>'СК: ', 'title'=>'собрание кредиторов')
	,'6'=> array('prefix'=>'СД: ', 'title'=>'собрание участников строительства (дольщиков)')
	,'о'=> array('prefix'=>'СР: ', 'title'=>'собрание работников и бывших работников')
);

global $days_of_week, $monthes;
$days_of_week= array('в воскресенье','в понедельник','во вторник','в среду','в четверг','в пятницу','в субботу');
$monthes= array('','января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря');

function event_title($event)
{
	global $event_type_descr;

	$title= !isset($event_type_descr[$event->type]) ? 'событие: ' : $event_type_descr[$event->type]['prefix'];

	if (isset($event->должник_фио))
	{
		$title.= prepare_fio($event->должник_фио);
	}
	else if (isset($event->должник))
	{
		$title.= prepare_debtor($event->должник);
	}
	else
	{
		$title.= '?';
	}

	return $title;
}

function event_description($event)
{
	global $event_type_descr;

	$description= !isset($event_type_descr[$event->type]) ? 'событие' : $event_type_descr[$event->type]['title'];
	$description.= ' ';

	$date_parts= explode('.',$event->time);
	$etime= $date_parts[0];
	if (false==mb_strpos($etime,':'))
		$etime.= ' 00:00:00';
	$date= date_create_from_format('Y-m-d\TH:i:s',$etime);
	if (false===$date)
		$date= date_create_from_format('Y-m-d H:i:s',$etime);

	if (false!==$date)
	{
		global $days_of_week, $monthes;
		$description.= $days_of_week[date_format($date, 'w')];
		$description.= ' ' . date_format($date, 'j');
		$description.= ' ' . $monthes[date_format($date, 'n')];
		$t= date_format($date, 'H:i');
		if ('00:00'!=$t)
			$description.= ' в ' . $t;
	}

	if (isset($event->должник_фио))
	{
		$description.= "\r\nдолжник: ".$event->должник_фио;
	}
	else if (isset($event->должник))
	{
		$description.= "\r\nдолжник: ".$event->должник;
	}

	if (isset($event->где))
		$description.= "\r\nместо проведения: {$event->где}";
	if (isset($event->форма))
		$description.= "\r\nформа: {$event->форма}";

	$description.= "\r\n(из объявления ефрсб № $event->number)";

	return $description;
}

function prepare_event_text($event,$event_res)
{
	$event_res->title= event_title($event);
	$event_res->description= event_description($event);
}

function prepare_event_structured_fields($event,$event_res)
{
	global $event_type_descr;

	$event_res->event=  !isset($event_type_descr[$event->type]) ? 'событие' : $event_type_descr[$event->type]['title'];

	$event_res->efrsb_number= $event->number;

	if (isset($event->где))
		$event_res->location= $event->где;
	if (isset($event->форма))
		$event_res->form= $event->форма;

	if (isset($event->должник_фио))
	{
		$event_res->debtor_fio= $event->должник_фио;
	}
	else if (isset($event->должник))
	{
		$event_res->debtor= $event->должник;
	}
}
