<?

if (!isset($_GET['start']))
	exit_bad_request('skipped parameter start!');
$dstart= date_create_from_format('Y-m-d\TH:i:s',$_GET['start']);
if (null==$dstart)
	exit_bad_request('bad parameter start!');

if (!isset($_GET['end']))
	exit_bad_request('skipped parameter end!');
$dend= date_create_from_format('Y-m-d\TH:i:s',$_GET['end']);
if (null==$dend)
	exit_bad_request('bad parameter end!');

if ($dend <= $dstart)
	exit_bad_request('start after end!');

$last_end= date_add($dstart,date_interval_create_from_date_string("43 days"));
if ($dend > $last_end)
	exit_bad_request('too large period!');
