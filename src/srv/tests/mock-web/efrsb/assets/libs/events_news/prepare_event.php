<?

require_once '../assets/helpers/sax.php';
require_once '../assets/libs/events_news/debtor.php';

function trim_and_fix_quotes($txt)
{
	$res= trim($txt);

	$len= mb_strlen($res);
	if ($len>2)
	{
		$first_char= mb_substr($res,0,1);
		$last_char= mb_substr($res,$len-1,1);

		if ('«'==$first_char && '»'==$last_char)
			$res= mb_substr($res,1,$len-2);
		if ('"'==$first_char && '"'==$last_char)
		{
			$count= 0;
			for ($i= 1; $i<($len-1); $i++)
			{
				if ('"'==mb_substr($res,$i,1))
					$count++;
			}
			$res= (0==($count%2)) ? mb_substr($res,1,$len-2) : mb_substr($res,1,$len-1);
		}
	}

	return $res;
}

class Должник_parser extends AFields_parser_named_combination
{
	public function __construct()
	{
		parent::__construct(array(
			'BankruptFirm_ShortName'=> new Field_parser(array('MessageData','BankruptInfo','BankruptFirm','@SHORTNAME'))

			,'BankruptPerson_FirstName'=> new Field_parser(array('MessageData','BankruptInfo','BankruptPerson','@FIRSTNAME'))
			,'BankruptPerson_LastName'=> new Field_parser(array('MessageData','BankruptInfo','BankruptPerson','@LASTNAME'))
			,'BankruptPerson_MiddleName'=> new Field_parser(array('MessageData','BankruptInfo','BankruptPerson','@MIDDLENAME'))

			,'Bankrupt_Name'=> new Field_parser(array('MessageData','Bankrupt','Name'))

			,'Bankrupt_FIO_FirstName'=> new Field_parser(array('MessageData','Bankrupt','Fio','FirstName'))
			,'Bankrupt_FIO_LastName'=> new Field_parser(array('MessageData','Bankrupt','Fio','LastName'))
			,'Bankrupt_FIO_MiddleName'=> new Field_parser(array('MessageData','Bankrupt','Fio','MiddleName'))
		));
	}

	public function set_должник_or_фио($res)
	{
		$p= $this->named_parsers;
		$debtorName= any_not_null_value(array($p->BankruptFirm_ShortName, $p->Bankrupt_Name));
		if (null!=$debtorName)
		{
			$res->должник= trim($debtorName);
		}
		else
		{
			$LastName= any_not_null_value(array($p->BankruptPerson_LastName, $p->Bankrupt_FIO_LastName));
			if (null!=$LastName)
			{
				$fio= trim($LastName);
				$FirstName= any_not_null_value(array($p->BankruptPerson_FirstName, $p->Bankrupt_FIO_FirstName));
				if (null!=$FirstName)
					$fio.= ' '.trim($FirstName);
				$MiddleName= any_not_null_value(array($p->BankruptPerson_MiddleName, $p->Bankrupt_FIO_MiddleName));
				if (null!=$MiddleName)
					$fio.= ' '.trim($MiddleName);
				$res->должник_фио= $fio;
			}
		}
	}
}

class Где_parser extends AFields_parser_combination
{
	public function __construct()
	{
		parent::__construct(array(
			new Field_parser(array('MessageData','MessageInfo','Auction','TradeSite'))
			,new Field_parser(array('MessageData','MessageInfo','Committee','MeetingSite'))
			,new Field_parser(array('MessageData','MessageInfo','Meeting','MeetingSite'))
			,new Field_parser(array('MessageData','MessageInfo','MeetingParticipantsBuilding','MeetingSite'))
			,new Field_parser(array('MessageData','MessageInfo','MeetingWorker','MeetingSite'))
		));
	}
}

class Форма_parser extends AFields_parser_combination
{
	public function __construct()
	{
		parent::__construct(array(
			 new Field_parser(array('MessageData','MessageInfo','Meeting','MeetingForm'))
			,new Field_parser(array('MessageData','MessageInfo','MeetingParticipantsBuilding','MeetingForm'))
			,new Field_parser(array('MessageData','MessageInfo','MeetingWorker','MeetingForm'))
		));
	}
}

function prepare_event_fields($xml_body, $message_type)
{
	$должник_parser= new Должник_parser();
	$где_parser= new Где_parser();
	$форма_parser= new Форма_parser();

	$field_parsers= array_merge($должник_parser->get_parsers(),$где_parser->get_parsers(),$форма_parser->get_parsers());

	try
	{
		parse_fields($xml_body,$field_parsers);
	}
	catch (Exception $ex)
	{
		write_to_log('Unhandled exception occurred during parsing message for event fields: ' . get_class($ex) . ' - ' . $ex->getMessage());
		write_to_log('Body:');
		write_to_log($xml_body);
	}

	$res= (object)array();

	$должник_parser->set_должник_or_фио($res);

	$site= $где_parser->any_not_null_value();
	if (null!=$site)
		$res->где= trim_and_fix_quotes($site);

	$form= $форма_parser->any_not_null_value();
	if (null!=$form && in_array($form,array('Заочная','Заочное голосование','Заочная (электронная)')))
		$res->форма= $form;

	return $res;
}

function prepare_event($row)
{
	$event= prepare_event_fields($row->Body,$row->MessageInfo_MessageType);

	$event->type= $row->MessageInfo_MessageType;
	$event->time= $row->EventTime;

	$event->guid= $row->MessageGUID;
	$event->number= $row->Number;

	return $event;
}

function prepare_creditor_name($cname)
{
	if (null==$cname)
	{
		return $cname;
	}
	{
		$parts= explode(' (ИНН ',$cname);
		$res= trim($parts[0]);
		if (0!=mb_strlen($res))
		{
			global $debtorName_beauty_prefixes;
			foreach ($debtorName_beauty_prefixes as $change)
			{
				$prefix= $change['prefix'];
				$prefix_len= mb_strlen($prefix);
				if ($prefix_len < mb_strlen($res))
				{
					$txtPrefix = mb_strtolower(mb_substr($res, 0, $prefix_len));
					if ($txtPrefix == $prefix)
						$res= $change['change_to'].mb_substr($res,$prefix_len);
				}
			}
		}
		return trim_and_fix_quotes($res);
	}
}

function prepare_brief_message_extra($xml_body, $message_type)
{
	try
	{
		$event_time_parser= get_event_time_parser_by_message_type($message_type);
		$форма_parser= new Форма_parser();

		$суд_акт_parser= new Field_parser(array('MessageData','MessageInfo', 'CourtDecision', 'DecisionType', '@ID'));
		$кредитор_parser= new Field_parser(array('MessageData','MessageInfo', 'ReceivingCreditorDemand', 'CreditorName'));
		$на_сумму_parser= new Field_parser(array('MessageData','MessageInfo', 'ReceivingCreditorDemand', 'DemandSum'));

		$field_parsers= array_merge($event_time_parser->get_parsers(),$форма_parser->get_parsers(),array($суд_акт_parser,$кредитор_parser,$на_сумму_parser));

		try
		{
			parse_fields($xml_body,$field_parsers);
		}
		catch (Exception $ex)
		{
			write_to_log('Unhandled exception occurred during parsing message for event time: ' . get_class($ex) . ' - ' . $ex->getMessage());
			write_to_log('Body:');
			write_to_log($xml_body);
			return null;
		}

		$res= array();

		$time= $event_time_parser->value();
		if (null!=$time)
			$res['event']= $time;

		$form= $форма_parser->any_not_null_value();
		if (null!=$form && in_array($form,array('Заочная','Заочное голосование','Заочная (электронная)')))
			$res['форма']= $form;
		if (null!=$суд_акт_parser->value)
			$res['тип_акта']= $суд_акт_parser->value;
		if (null!=$кредитор_parser->value)
			$res['кредитор']= prepare_creditor_name($кредитор_parser->value);
		if (null!=$на_сумму_parser->value)
			$res['на_сумму']= $на_сумму_parser->value;

		return 0==count($res) ? null : (object)$res;
	}
	catch (Exception $ex)
	{
		return null;
	}
}