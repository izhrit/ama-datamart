<?php
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/sax.php';
require_once '../assets/libs/efrsb.php';
require_once '../assets/libs/time.php';

function message_short_time($time)
{
	return substr($time, 0, strpos($time, "."));
}

class Event_time_parser extends AFields_parser_named_combination
{
	private function prepare_named_parsers($message_type)
	{
		$this->message_type= $message_type;
		switch ($message_type)
		{
			case 'a': // ArbitralDecree
				return array('DecisionDate_parser'=>new Field_parser(array('MessageData','MessageInfo', 'CourtDecision', 'NextCourtSessionDate')));
			case 'b': // Auction
				return array(
					'DecisionDate_parser' => new Field_parser(array('MessageData','MessageInfo', 'Auction', 'Date'))
					,'DecisionDate_parser_begin' => new Field_parser(array('MessageData','MessageInfo', 'Auction', 'Application', 'TimeBegin'))
				);
			case 'c': // Meeting
				return array(
					'DecisionDate_parser'=> new Field_parser(array('MessageData','MessageInfo', 'Meeting', 'MeetingDate'))
					,'DecisionDate_parser_begin'=> new Field_parser(array('MessageData','MessageInfo', 'Meeting', 'MeetingDateBegin'))
					,'DecisionDate_parser_time'=> new Field_parser(array('MessageData','MessageInfo', 'Meeting', 'MeetingTimeBegin'))
				);
			case 'q': // Committee
				return array('DecisionDate_parser'=> new Field_parser(array('MessageData','MessageInfo', 'Committee', 'MeetingDate')));
			case '6': // MeetingParticipantsBuilding
				return array('DecisionDate_parser'=> new Field_parser(array('MessageData','MessageInfo', 'MeetingParticipantsBuilding', 'MeetingDate')));
			case 'о': // MeetingWorker
				return array(
					'DecisionDate_parser' => new Field_parser(array('MessageData','MessageInfo', 'MeetingWorker', 'MeetingDate'))
					,'BallotsReceptionEndDate_parser' => new Field_parser(array('MessageData','MessageInfo', 'MeetingWorker', 'BallotsReceptionEndDate'))
				);
			default:
				return array();
		}
	}

	public function __construct($message_type)
	{
		parent::__construct($this->prepare_named_parsers($message_type));
	}

	public function value()
	{
		$p= $this->named_parsers;

		if (!isset($p->DecisionDate_parser))
			return null;

		if (empty($p->DecisionDate_parser->value))
		{
			switch ($this->message_type)
			{
				case 'b': /* Auction */ 
					return $p->DecisionDate_parser_begin->value;
				case 'c': /* Meeting */ 
					$meeting_time= message_short_time($p->DecisionDate_parser_time->value);
					return (''==$meeting_time) ? $p->DecisionDate_parser_begin->value : $p->DecisionDate_parser_begin->value."T".$meeting_time;
				case 'о': /* MeetingWorker */ 
					return $p->BallotsReceptionEndDate_parser->value;
			}
		}
		return $p->DecisionDate_parser->value;
	}
}

global $event_time_parsers_by_message_type;
$event_time_parsers_by_message_type= array();

function get_event_time_parser_by_message_type($message_type)
{
	/*global $event_time_parsers_by_message_type;
	if (isset($event_time_parsers_by_message_type[$message_type]))
	{
		return $event_time_parsers_by_message_type[$message_type];
	}
	else
	{
		$event_time_parser= new Event_time_parser($message_type);
		$event_time_parsers_by_message_type[$message_type]= $event_time_parser;
		return $event_time_parser;
	}*/
	return new Event_time_parser($message_type);
}

function parse_event_time($msg, $message_type)
{
	$event_time_parser= get_event_time_parser_by_message_type($message_type);

	try
	{
		parse_fields($msg,$event_time_parser->get_parsers());
	}
	catch (Exception $ex)
	{
		write_to_log('Unhandled exception occurred during parsing message for event time: ' . get_class($ex) . ' - ' . $ex->getMessage());
		write_to_log('Body:');
		write_to_log($msg);
		return null;
	}

	return $event_time_parser->value();
}
