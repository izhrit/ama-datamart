<?php

require_once '../assets/helpers/sax.php';

require_once 'MessageType.php';

function parse_for_Message_brief($msg,$MessageType)
{
	$parsed_msg= (object)array();

	$xml = new SimpleXMLElement($msg);

	global $MessageInfo_MessageType_desciptions_by_api_name;
	$TextWrapperTagName= $MessageType;
	if (isset($MessageInfo_MessageType_desciptions_by_api_name[$MessageType]['TextTag']))
		$TextWrapperTagName= $MessageInfo_MessageType_desciptions_by_api_name[$MessageType]['TextTag'];

	if (isset($xml->MessageInfo->$TextWrapperTagName->Text))
	{
		$parsed_msg->Text= (string)$xml->MessageInfo->$TextWrapperTagName->Text;
	}
	else if (isset($xml->MessageInfo->Other->Text))
	{
		$parsed_msg->Text= (string)$xml->MessageInfo->Other->Text;
	}
	else
	{
		$parsed_msg->Text= null;
	}
	return $parsed_msg;
}

function parse_for_Message_brief2($msg,$MessageType)
{
	global $MessageInfo_MessageType_desciptions_by_api_name;

	$TextWrapperTagName= $MessageType;
	if (isset($MessageInfo_MessageType_desciptions_by_api_name[$MessageType]['TextTag']))
		$TextWrapperTagName= $MessageInfo_MessageType_desciptions_by_api_name[$MessageType]['TextTag'];

	echo "TextWrapperTagName=$TextWrapperTagName\r\n";

	$Text_parser= new Field_parser(array('MessageData','MessageInfo',$TextWrapperTagName,'Text'));

	$msg_parser = new XMLParserArray(array(
		$Text_parser
		//,new XMLParser_echor()
	));

	$xml_parser = xml_parser_create();
	$msg_parser->bind($xml_parser);
	if (!xml_parse($xml_parser, $msg, /* is_final= */TRUE))
	{
		$ex_text= sprintf("Ошибка XML: %s на строке %d",
						xml_error_string(xml_get_error_code($xml_parser)),
						xml_get_current_line_number($xml_parser));
		xml_parser_free($xml_parser);
		throw new Exception($ex_text);
	}
	xml_parser_free($xml_parser);

	$parsed_msg= (object)array();

	$parsed_msg->Text= $Text_parser->value;

	return $parsed_msg;
}

global $brief_text_delimiters;
$brief_text_delimiters= array
(
	array('сделаны следующие выводы:',' сделаны выводы:')
	,array('пришел к выводам:',' сделаны выводы:')
	,array('уведомляет заинтересованных лиц о том, что','')
	,array('Настоящим сообщаю о том, что','')
	,array('Настоящим уведомляю:','')
	,array('Настоящим уведомляю, что','')
	,array('Настоящим  уведомляю , что:','')
	,array('Настоящим  уведомляю , что','')
	,array('Настоящим сообщаю, что','')
	,array('сообщаем Вам о том, что','')
	,array('сообщаю Вам, что','')
	,array('Настоящим уведомляю о',' о')
	,array('Настоящим уведомляем об',' об')
	,array('публикует сведения об',' об')
	,array('уведомляет о том, что','')
	,array('сообщает , что:','')
	,array('сообщает , что','')
	,array('сообщаю, что','')
	,array('уведомляет, что','')
	,array('сообщает, что','')
	,array('сообщает что:','')
	,array('сообщает что','')
	,array('Сообщаю, что','')
	,array('информирует, что','')
	,array('Сообщаем, что','')
	,array('сообщает о том, что','')
	,array('сообщает, что','')
	,array('сообщает, о том, что','')
	,array('извещаю Вас о том, что','')
	,array('размещаю информацию о',' о')
	,array('размещает сведения о',' о')
	,array('настоящим сообщает о',' о')
	,array('публикует сведения о',' о')
	,array('публикует информацию о',' о')
	,array('Уведомляем кредиторов о',' о')
	,array('обратился',' обратился')
	,array('о п р е д е л и л :','')
	,array('ПРИКАЗЫВАЮ:','')
	,array('определил:','')
	,array('конкурсный управляющий сообщает о',' о')
	,array('Суд постановил:','')
	,array('извещает клиентов о',' о')
	,array('Настоящим сообщаю о',' о')
	,array('сообщает о',' о')
	,array('сообщаю о',' о')
	,array('Сообщает о',' о')
	,array('уведомляю о',' о')
	,array('сообщается о',' о')
	,array('объявляет о',' о')
	,array('извещает о',' о')
	,array('информирует о',' о')
	,array('включает в ЕФРСБ сведения о',' о')
	,array('уведомляет',' уведомление')
	,array('сообщает:','')
	,array('сообщает','')
	,array('проводит',' проводит')
	,array('предлагает',' предлагает')
	,array('скорректировало смету в следующей редакции:',' скорректировало смету в следующей редакции:')
	,array('утвердило смету в следующей редакции:',' утвердило смету в следующей редакции:')
	,array('проводилось заседание комитета кредиторов',' проводилось заседание комитета кредиторов')
	,array('публикуем отчет об оценке',' публикуем отчет об оценке')
	,array('публикует отчет об оценке',' публикует отчет об оценке')
	,array('Настоящим уведомляю,','')
	,array('публикует положение о',' положение о')
	,array('размещает Положение о',' положение о')
);

function get_Brief_text($txt)
{
	$res= $txt;

	global $brief_text_delimiters;
	foreach ($brief_text_delimiters as $r)
	{
		$delimiter=$r[0];
		$replace= $r[1];
		$pos= mb_strpos($res,$delimiter);
		if (false!==$pos)
		{
			$res= '..'.$replace.mb_substr($res,$pos+mb_strlen($delimiter));
			break;
		}
	}

	$max_length= 800;
	$postfix= ' ..';
	//$max_length-= mb_strlen($postfix);
	if (mb_strlen($res)>$max_length)
		$res= mb_substr($res,0,$max_length).' ..';

	return $res;
}