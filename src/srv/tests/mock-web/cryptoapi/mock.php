<? 

require_once __DIR__.'\..\..\..\..\etc\built-local\config.php';
require_once __DIR__.'\..\..\..\assets\helpers\json.php';

mb_internal_encoding("utf-8");

function write_to_log_string($txt)
{
	global $log_file_name;
	file_put_contents($log_file_name,date('m/d/Y h:i:s a: ', time()).$txt."\r\n",FILE_APPEND);
	return $txt;
}

function write_to_log($data)
{
	return write_to_log_string(is_string($data) ? $data : print_r($data,true));
}

$action= null;
if (isset($_SERVER['ORIG_PATH_INFO']))
{
	$path= $_SERVER['ORIG_PATH_INFO'];
	$pos= strrpos($path,'/');
	if (false != $pos)
	{
		$action= substr($path,$pos+1);
		if ('test.php'==$action)
			$action= null;
	}
}
else if (isset($_SERVER['PHP_SELF']))
{
	$path= $_SERVER['PHP_SELF'];
	$pos= strrpos($path,'/');
	if (false != $pos)
	{
		$action= substr($path,$pos+1);
		if ('test.php'==$action)
			$action= null;
	}
}

function VerifySignature()
{
	$post_fields= (object)json_decode(file_get_contents('php://input'), true);
	write_to_log($post_fields);

	$document= base64_decode($post_fields->base64_encoded_document);
	$signature= base64_decode($post_fields->base64_encoded_signature);

	write_to_log('document:');
	write_to_log($document);
	write_to_log('base64_encoded_signature:');
	write_to_log($post_fields->base64_encoded_signature);
	write_to_log('signature:');
	write_to_log($signature);

	if ('bad_signature'==$signature)
	{
		$result= (object)array(
			'signature_checked'=>false
			,'signature_checking_message'=>'fault!'
		);
	}
	else
	{
		$result= (object)array(
			'signature_checked'=>true
			,'signature_checking_message'=>'everything are ok!'
		);
	}

	echo nice_json_encode($result);
}

function VerifySignatureFromCapicom()
{
	$post_fields= (object)json_decode(file_get_contents('php://input'), true);
	write_to_log($post_fields);

	$document= $post_fields->document;
	$signature= base64_decode($post_fields->base64_encoded_signature);

	write_to_log('document:');
	write_to_log($document);
	write_to_log('base64_encoded_signature:');
	write_to_log($post_fields->base64_encoded_signature);
	write_to_log('signature:');
	write_to_log($signature);

	if ('bad_signature'==$signature)
	{
		$result= (object)array(
			'signature_checked'=>false
			,'signature_checking_message'=>'fault!'
		);
	}
	else
	{
		$result= (object)array(
			'signature_checked'=>true
			,'signature_checking_message'=>'everything are ok!'
		);
	}

	echo nice_json_encode($result);
}


function Sign()
{

}

try
{
	switch ($action)
	{
		case 'VerifySignature':
			write_to_log('------ mock cryptoapi VerifySignature -----');
			VerifySignature();
			break;
		case 'VerifySignatureFromCapicom':
			write_to_log('------ mock cryptoapi VerifySignatureFromCapicom -----');
			VerifySignatureFromCapicom();
			break;
		case 'Sign':
			write_to_log('------ mock cryptoapi Sign ----------------');
			Sign();
			break;
		default: 
			write_to_log('------ mock cryptoapi unknown action! -----');
			echo 'unknown action!';
			break;
	}
}
catch (Exception $ex)
{
	write_to_log('Unhandled exception occurred: ' . get_class($ex) . ' - ' . $ex->getMessage());
	exit_internal_server_error('Unhandled exception!');
}