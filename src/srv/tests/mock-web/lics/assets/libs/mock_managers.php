<?

$mock_managers = array(
	array(
		"last_name"       => "Иванов",
		"first_name"      => "Иван",
		"middle_name"     => "Иванович",
		"inn"     => "0001",
		"contract_number" => "666666",
		"voted_for"       => array(
			"last_name"   => "Серёгин",
			"first_name"  => "Серёга",
			"middle_name" => "Сергеевич",
			"EFRSBNumber" => "322",
			"sro"         => "СРО задорных"
		)
	),
	array(
		"last_name"       => "Петров",
		"first_name"      => "Петр",
		"middle_name"     => "Петрович",
		"inn"     => "0002",
		"contract_number" => "666666"
	),
	array(
		"last_name"       => "Петров",
		"first_name"      => "Иван",
		"middle_name"     => "Петрович",
		"inn"     => "0003",
		"contract_number" => "666666",
	)
);
