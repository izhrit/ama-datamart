<?

require_once '../assets/libs/mock_licenses.php';
require_once '../assets/helpers/json.php';

try
{
	if (!isset($_GET['license_id']))
		throw new Exception('skipped license_id');

	$license_token= $_GET['license_id'];

	if (!isset($license_info_by_token[$license_token]))
		throw new Exception('wrong license_token');

	$license_info= $license_info_by_token[$license_token];

	echo $license_info->email;
}
catch (Exception $exception)
{
	echo '{"status":false,"message":"Error checking user"}';
}
