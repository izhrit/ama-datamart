<?

require_once '../assets/libs/mock_licenses.php';
require_once '../assets/helpers/json.php';

try
{
	if (!isset($_GET['license_token']))
		throw new Exception('skipped license_token');

	$license_token= $_GET['license_token'];

	if (!isset($license_info_by_token[$license_token]))
		throw new Exception('wrong license_token');

	$license_info= $license_info_by_token[$license_token];

	if (isset($license_info->email))
		unset($license_info->email);

	echo nice_json_encode(array('status'=>true,'license_info'=>$license_info));
}
catch (Exception $exception)
{
	echo '{"status":false,"message":"Error checking user"}';
}
