<?

require_once '../assets/helpers/log.php';

write_to_log('------ validateTokenForDataMart -------');

function validateAma()
{
	if (!isset($_GET['user']))
		throw new Exception('skipped user');
	if (!isset($_GET['license_token']))
		throw new Exception('skipped license_token');

	$user= $_GET['user'];
	$license_token= $_GET['license_token'];

	require_once '../assets/libs/mock_licenses.php';

	if (!isset($license_info_by_token[$license_token]))
		throw new Exception('wrong license_token');

	$license_info= $license_info_by_token[$license_token];

	if (isset($_GET['contract']) && $license_info->ContractNumber!=$_GET['contract'])
		throw new Exception('wrong contract');

	if (isset($_GET['efrsb_number']) && $license_info->manager->efrsbNumber!=$_GET['efrsb_number'])
		throw new Exception('wrong manager');

	echo '{"status":true,"message":null}';
}

function validateFA()
{
	if (!isset($_GET['license_token']))
		throw new Exception('skipped license_token');

	$license_token= $_GET['license_token'];

	require_once '../assets/libs/mock_licenses_fa.php';

	if (!isset($fa_license_info_by_token[$license_token]))
		throw new Exception("wrong license_token $license_token");

	$license_info= $fa_license_info_by_token[$license_token];

	if (isset($_GET['email']) && $license_info->email!=$_GET['email'])
		throw new Exception('wrong email!');

	echo '{"status":true,"message":null}';
}

try
{
	if (isset($_GET['product']) && 'FA'==$_GET['product'])
	{
		validateFA();
	}
	else
	{
		validateAma();
	}
}
catch (Exception $exception)
{
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	echo '{"status":false,"message":"Error checking user"}';
}