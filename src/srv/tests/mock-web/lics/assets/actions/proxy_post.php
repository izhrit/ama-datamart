<?

require_once '../assets/config.php';

global $use_server_license_url;
$url = $use_server_license_url."?action=checkManagerInServer";

$options = array(
	'http' => array(
		'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		'method'  => 'POST',
		'content' => http_build_query($_POST)
	)
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);

echo $result;


