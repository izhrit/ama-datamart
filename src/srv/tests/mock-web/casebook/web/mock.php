<?

function FindOutDetailsOfTheCases()
{
	$in= json_decode(file_get_contents('php://input'));
	if (isset($in->cases))
	{
		$cases= $in->cases;
		if (1==count($cases) && 'Абайдуллин Валерий Кимович'==$cases[0]->debtor->Name)
		{
			$res= array(
				array(
					'CaseNumber'=> "Ё34/2021"
					,'NextSessionDate'=> '12.02.2021 10:00'
					,'id'=> '1'
					,'Applicant'=> array(
						 'isCitizen'=> true
						,'Name'=> 'Сергеев Сергей Сергеевич'
						,'INN'=> '383765009604'
						,'OGRN'=> '2064256002446'
						,'SNILS'=> '65246718108'
						,'Address'=> 'Ижевск, Ленина 100'
					)
					,'Debtors'=> array(
						array(
							 'isCitizen'=> true
							,'Name'=> 'Васильев Василий Васильевич'
							,'INN'=> '831100784792'
							,'OGRN'=> '5045516609144'
							,'SNILS'=> '13398475700'
							,'Address'=> 'Воткинск, Пушкинская 11'
						)
					)
				)
			);
			echo json_encode($res);
		}
		else if (5==count($cases))
		{
			$res= array(
				array(
					'CaseNumber'=> "А82-650\/2021",
					'NextSessionDate'=> null,
					'id'=> '3'
				)
				,array(
					'CaseNumber'=> "А84-381\/2021",
					'NextSessionDate'=> '24.02.2021 10:20:00',
					'id'=> '4'
				)
			);
			echo json_encode($res);
		}
	}
}

$path= $_SERVER['PHP_SELF'];
$pos= strrpos($path,'/');
if (false != $pos)
	$action= substr($path,$pos+1);

if ($action="FindOutDetailsOfTheCases")
	FindOutDetailsOfTheCases();
