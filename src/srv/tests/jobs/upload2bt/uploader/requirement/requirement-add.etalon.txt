авторизуемся { --------------------------------------------
сессия не создана, создадим новую!
запрос "https://russianit.bankro.tech/authentication/account/login" ..
.. ответ получен через 1,000 сек.
авторизуемся } --------------------------------------------
  чистим базу { -------------------------------------------
запрос "https://russianit.bankro.tech/api/Projects/GetGroupedProjects" ..
.. ответ получен через 1,000 сек.
запрос "https://russianit.bankro.tech/api/Participants/GetParticipants" ..
.. ответ получен через 1,000 сек.
очистили базу } -------------------------------------------
готовим процедуру { ---------------------------------------
Ищем дело по номеру "А71-14496/2013" ..
 Загружаем перечень дел с Bankro.TECH ..
  запрос "https://russianit.bankro.tech/api/Projects/GetGroupedProjects" ..
  .. ответ получен через 1,000 сек.
 Загрузили перечень дел с Bankro.TECH в количестве 0 за 3,000 сек.
 Определяем номер судебного дела для дел из перечня пока не найдём искомое
 НЕ нашли в перечне судебных дел дела с искомым номером, потратили 1,000 сек.
НЕ нашли дело по номеру, на поиск потратили 7,000 сек.
Создаём дело "А71-14496/2013" (Н)..
 запрос "https://russianit.bankro.tech/api/UserProfiles/Get" ..
 .. ответ получен через 1,000 сек.
 Готовим должника ИНН:6410002242 ("Роги и ноги") ..
  Ищем участника по ИНН 6410002242:
   запрос "https://russianit.bankro.tech/api/Participants/GetParticipants" ..
   .. ответ получен через 1,000 сек.
  Нашли участников в количестве 0.
  запрос "https://russianit.bankro.tech/api/Participants/PutParticipant" ..
  .. ответ получен через 1,000 сек.
 Подготовили должника за 7,000 сек.
 запрос "https://russianit.bankro.tech/api/Projects/CreateProject" ..
 .. ответ получен через 1,000 сек.
 запрос "https://russianit.bankro.tech/api/Projects/UpdateProjectWithBlocks" ..
 .. ответ получен через 1,000 сек.
Создали дело за 15,000 сек.
готовим процедуру } ---------------------------------------
готовим кредитора { ---------------------------------------
Готовим кредитора ИНН:5208368650 ("Роги и ноги") ..
 Ищем участника по ИНН 5208368650:
  запрос "https://russianit.bankro.tech/api/Participants/GetParticipants" ..
  .. ответ получен через 1,000 сек.
 Нашли участников в количестве 0.
 запрос "https://russianit.bankro.tech/api/Participants/PutParticipant" ..
 .. ответ получен через 1,000 сек.
Подготовили кредитора за 7,000 сек.
готовим кредитора } ---------------------------------------
добавляем требование { ------------------------------------
запрос "https://russianit.bankro.tech/api/Requirements/PutRequirement" ..
.. ответ получен через 1,000 сек.
добавляем требование } ------------------------------------
очищаем требования { --------------------------------------
Чистим старые требования перед загрузкой..
 Запрашиваем старый перечень требований:
  запрос "https://russianit.bankro.tech/api/Requirements/GetRequirementsByProjectId" ..
  .. ответ получен через 1,000 сек.
 Получили перечень старых требований за 3,000 сек.
 количество кредиторов в полученном перечне:1.
     требований:1.
 Удаляем старые требования:
  запрос "https://russianit.bankro.tech/api/Requirements/BulkDelete" ..
  .. ответ получен через 1,000 сек.
 Удалили старые требования за 3,000 сек.
Очистили старые требования перед загрузкой за 9,000 сек.
очищаем требования } --------------------------------------
созданное требование:
stdClass Object
(
    [Id] => fixed_Id
    [ExternalId] => 
    [Number] => 1
    [Project] => stdClass Object
        (
            [Id] => fixed_Id
            [Name] => Дело №А71-14496/2013, должник: Роги и ноги
            [IsArchive] => 
            [Permissions] => 16
        )

    [Creditor] => stdClass Object
        (
            [Id] => fixed_Id
            [Name] => Роги и ноги
        )

    [Reasons] => Array
        (
        )

    [DocumentFolder] => stdClass Object
        (
            [Id] => fixed_Id
            [Name] => 1 (Роги и ноги)
        )

    [LiabilityType] => 
    [Items] => Array
        (
            [0] => stdClass Object
                (
                    [Id] => fixed_Id
                    [State] => stdClass Object
                        (
                            [SysName] => EstablishedByCourt
                            [Id] => 7b6b5c88-42fc-e711-80bd-0cc47a7b67ab
                            [Name] => Установлено судом
                        )

                    [Type] => stdClass Object
                        (
                            [SysName] => Collateral
                            [Id] => 746b5c88-42fc-e711-80bd-0cc47a7b67ab
                            [Name] => 3.1 Залоговые требования
                        )

                    [Amount] => 34556
                    [Date] => 2013-03-09
                    [IsCreatedByReplacementCreditor] => 
                )

        )

    [Payments] => Array
        (
        )

    [Exclusions] => Array
        (
        )

    [Totals] => stdClass Object
        (
            [TotalByState] => 
            [Total] => 34556
            [Percentage] => 
            [TotalPaidByState] => 
            [Paid] => 
            [PaidPercentage] => 
            [TotalExcludedByState] => 
            [Excluded] => 
            [TotalDueByState] => 
            [Due] => 34556
            [DuePercentage] => 
            [MaxAmountForChangeCreditor] => 34556
        )

    [Permissions] => 5
)
реестр в созданном деле "А71-14496/2013":
запрос "https://russianit.bankro.tech/api/projects/GetProject?ProjectId=fixed_id" ..
.. ответ получен через 1,000 сек.
stdClass Object
(
    [Номер_судебного_дела] => А71-14496/2013
)
  чистим базу { -------------------------------------------
запрос "https://russianit.bankro.tech/api/Projects/GetGroupedProjects" ..
.. ответ получен через 1,000 сек.
запрос "https://russianit.bankro.tech/api/projects/Archive?Id=fixed_id" ..
.. ответ получен через 1,000 сек.
запрос "https://russianit.bankro.tech/api/projects/DeleteProject?Id=fixed_id" ..
.. ответ получен через 1,000 сек.
запрос "https://russianit.bankro.tech/api/Participants/GetParticipants" ..
.. ответ получен через 1,000 сек.
Роги и ноги
запрос "https://russianit.bankro.tech/api/participants/DeleteParticipant/fixed_id" ..
.. ответ получен через 1,000 сек.
Роги и ноги
запрос "https://russianit.bankro.tech/api/participants/DeleteParticipant/fixed_id" ..
.. ответ получен через 1,000 сек.
запрос "https://russianit.bankro.tech/api/Participants/GetParticipants" ..
.. ответ получен через 1,000 сек.
очистили базу } -------------------------------------------
