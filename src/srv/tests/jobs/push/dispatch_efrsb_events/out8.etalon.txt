
################################## QUERY ##################################

	insert ignore into Pushed_event (id_Event, id_Push_receiver, Time_dispatched)
	select e.id_Event, pr.id_Push_receiver, '2020-11-26 13:07:55' + interval pr.Timezone hour

	from	   event e
	inner join Manager man on man.ArbitrManagerID = e.ArbitrManagerID
	inner join Push_receiver pr on pr.Category='m' && pr.id_Manager_Contract_MUser = man.id_Manager

	where pr.Transport <> 'd'
	&& '2020-11-26 13:07:55' + interval pr.Timezone hour between
	CONCAT(date('2020-11-26 13:07:55' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_start hour 
	AND
	CONCAT(date('2020-11-26 13:07:55' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday('2020-11-26 13:07:55') BETWEEN 0 AND 4, true, false))
	&& DATE('2020-11-26 13:07:55' + interval pr.Timezone hour) = CASE
				WHEN e.MessageType = 'a' THEN DATE(getNotifyDate(pr.For_CourtDecision, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'о' THEN DATE(getNotifyDate(pr.For_MeetingWorker, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'b' THEN DATE(getNotifyDate(pr.For_Auction, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = '6' THEN DATE(getNotifyDate(pr.For_MeetingPB, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'q' THEN DATE(getNotifyDate(pr.For_Committee, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'c' THEN DATE(getNotifyDate(pr.For_Meeting, e.EventTime, pr.Check_day_off))
				ELSE '2000-01-01'
			END
	&& e.EventTime > '2020-11-26 13:07:55' + interval pr.Timezone hour
	&& e.isActive = 1;
	
################################## END QUERY ##################################

2020-11-26T14:07:56 notification for Manager ..
2020-11-26T14:07:57 .. notification finished with 0 affected rows

################################## QUERY ##################################

	insert ignore into Pushed_event (id_Event, id_Push_receiver, Time_dispatched) 
	select e.id_Event, pr.id_Push_receiver, '2020-11-26 13:07:55' + interval pr.Timezone hour

	from	   event e
	inner join Manager man on man.ArbitrManagerID = e.ArbitrManagerID
	inner join ManagerUser manu on manu.id_Manager = man.id_Manager
	inner join MUser mu on mu.id_MUser = manu.id_MUser
	inner join Push_receiver pr on pr.Category='v' && pr.id_Manager_Contract_MUser = mu.id_MUser

	where pr.Transport <> 'd'
	&& '2020-11-26 13:07:55' + interval pr.Timezone hour between
	CONCAT(date('2020-11-26 13:07:55' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_start hour 
	AND
	CONCAT(date('2020-11-26 13:07:55' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday('2020-11-26 13:07:55') BETWEEN 0 AND 4, true, false))
	&& DATE('2020-11-26 13:07:55' + interval pr.Timezone hour) = CASE
				WHEN e.MessageType = 'a' THEN DATE(getNotifyDate(pr.For_CourtDecision, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'о' THEN DATE(getNotifyDate(pr.For_MeetingWorker, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'b' THEN DATE(getNotifyDate(pr.For_Auction, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = '6' THEN DATE(getNotifyDate(pr.For_MeetingPB, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'q' THEN DATE(getNotifyDate(pr.For_Committee, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'c' THEN DATE(getNotifyDate(pr.For_Meeting, e.EventTime, pr.Check_day_off))
				ELSE '2000-01-01'
			END
	&& e.EventTime > '2020-11-26 13:07:55' + interval pr.Timezone hour
	&& e.isActive = 1;
	
################################## END QUERY ##################################

2020-11-26T14:07:58 notification for MUser ..
2020-11-26T14:07:59 .. notification finished with 1 affected rows

################################## QUERY ##################################

	insert ignore into Pushed_event (id_Event, id_Push_receiver, Time_dispatched) 
	select e.id_Event, pr.id_Push_receiver, '2020-11-26 13:07:55' + interval pr.Timezone hour

	from	   event e
	inner join Manager man on man.ArbitrManagerID = e.ArbitrManagerID
	inner join Contract c on c.id_Contract = man.id_Contract
	inner join Push_receiver pr on pr.Category='c' && pr.id_Manager_Contract_MUser = c.id_Contract

	where pr.Transport <> 'd'
	&& '2020-11-26 13:07:55' between
	CONCAT(date('2020-11-26 13:07:55' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_start hour 
	AND
	CONCAT(date('2020-11-26 13:07:55' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday('2020-11-26 13:07:55') BETWEEN 0 AND 4, true, false))
	&& DATE('2020-11-26 13:07:55' + interval pr.Timezone hour) = CASE
				WHEN e.MessageType = 'a' THEN DATE(getNotifyDate(pr.For_CourtDecision, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'о' THEN DATE(getNotifyDate(pr.For_MeetingWorker, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'b' THEN DATE(getNotifyDate(pr.For_Auction, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = '6' THEN DATE(getNotifyDate(pr.For_MeetingPB, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'q' THEN DATE(getNotifyDate(pr.For_Committee, e.EventTime, pr.Check_day_off))
				WHEN e.MessageType = 'c' THEN DATE(getNotifyDate(pr.For_Meeting, e.EventTime, pr.Check_day_off))
				ELSE '2000-01-01'
			END
	&& e.EventTime > '2020-11-26 13:07:55' + interval pr.Timezone hour
	&& e.isActive = 1;
	
################################## END QUERY ##################################

2020-11-26T14:08:00 notification for Contract ..
2020-11-26T14:08:01 .. notification finished with 2 affected rows
results:
отправлено напоминаний о событиях с ЕФРСБ: 3