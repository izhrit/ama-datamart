﻿delete from Pushed_event;
delete from Pushed_message;
delete from push_receiver;
delete from event;

insert into event set EventTime='2020-09-18 12:00:00'
,efrsb_id=1
,ArbitrManagerID=1
,BankruptId=1
,MessageType='a'
,Revision=1
;

insert into push_receiver set For_Meeting='a'
,For_Committee='a'
,For_MeetingPB='a'
,For_MeetingWorker='a'
,For_Auction='a'
,For_CourtDecision='a'
,Time_start=11
,For_Message_after='2019-10-11 18:00:00'
,Time_finish=19
,Check_day_off=1
,Transport='c'
,Timezone=0
,Category='m'
,id_Manager_Contract_MUser=4
;