
################################## QUERY ##################################

	insert ignore into Pushed_message (id_Message, id_Push_receiver, Time_dispatched)
	select mes.id_Message, pr.id_Push_receiver, '2019-11-27 11:00:01' + interval pr.Timezone hour
	from	   Message mes
	inner join Manager man on man.ArbitrManagerID = mes.ArbitrManagerID
	inner join Push_receiver pr on pr.Category='m' && pr.id_Manager_Contract_MUser = man.id_Manager
	left  join Pushed_message pm on mes.id_Message = pm.id_Message && pm.id_Push_receiver = pr.id_Push_receiver
	where  pr.For_Message_after is not null 
	&& mes.PublishDate > (pr.For_Message_after - interval 1 hour) + interval pr.Timezone hour
	&& mes.PublishDate > ('2019-11-27 11:00:01' - interval 1000 day)
	&& pm.Time_dispatched is null
	&& pr.Transport <> 'd'
	&& '2019-11-27 11:00:01' + interval pr.Timezone hour between
	CONCAT(date('2019-11-27 11:00:01' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_start hour 
	AND
	CONCAT(date('2019-11-27 11:00:01' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday('2019-11-27 11:00:01') BETWEEN 0 AND 4, true, false))
	
################################## END QUERY ##################################

2019-11-27T12:00:02 message notification for Manager ..
2019-11-27T12:00:03 .. message notification finished with 2 affected rows

################################## QUERY ##################################

	insert ignore into Pushed_message (id_Message, id_Push_receiver, Time_dispatched)
	select mes.id_Message, pr.id_Push_receiver, '2019-11-27 11:00:01' + interval pr.Timezone hour
	from	   Message mes
	inner join Manager man on man.ArbitrManagerID = mes.ArbitrManagerID
	inner join ManagerUser manu on manu.id_Manager = man.id_Manager
	inner join MUser mu on mu.id_MUser = manu.id_MUser
	inner join Push_receiver pr on pr.Category='v' && pr.id_Manager_Contract_MUser = mu.id_MUser
	left  join Pushed_message pm on mes.id_Message = pm.id_Message && pm.id_Push_receiver = pr.id_Push_receiver
	where  pr.For_Message_after is not null 
	&& mes.PublishDate > (pr.For_Message_after - interval 1 hour) + interval pr.Timezone hour
	&& mes.PublishDate > ('2019-11-27 11:00:01' - interval 1000 day)
	&& pm.Time_dispatched is null
	&& pr.Transport <> 'd'
	&& '2019-11-27 11:00:01' + interval pr.Timezone hour between
	CONCAT(date('2019-11-27 11:00:01' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_start hour 
	AND
	CONCAT(date('2019-11-27 11:00:01' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday('2019-11-27 11:00:01') BETWEEN 0 AND 4, true, false))
	
################################## END QUERY ##################################

2019-11-27T12:00:04 message notification for MUser ..
2019-11-27T12:00:05 .. message notification finished with 0 affected rows

################################## QUERY ##################################

	insert ignore into Pushed_message (id_Message, id_Push_receiver, Time_dispatched)
	select mes.id_Message, pr.id_Push_receiver, '2019-11-27 11:00:01' + interval pr.Timezone hour
	from	   Message mes
	inner join Manager man on man.ArbitrManagerID = mes.ArbitrManagerID
	inner join Contract c on c.id_Contract = man.id_Contract
	inner join Push_receiver pr on pr.Category='c' && pr.id_Manager_Contract_MUser = c.id_Contract
	left  join Pushed_message pm on mes.id_Message = pm.id_Message && pm.id_Push_receiver = pr.id_Push_receiver
	where  pr.For_Message_after is not null 
	&& mes.PublishDate > (pr.For_Message_after - interval 1 hour) + interval pr.Timezone hour
	&& mes.PublishDate > ('2019-11-27 11:00:01' - interval 1000 day)
	&& pm.Time_dispatched is null
	&& pr.Transport <> 'd'
	&& '2019-11-27 11:00:01' + interval pr.Timezone hour between
	CONCAT(date('2019-11-27 11:00:01' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_start hour 
	AND
	CONCAT(date('2019-11-27 11:00:01' + interval pr.Timezone hour)," 00:00:00") + interval pr.Time_finish hour
	&& IF(pr.Check_day_off = 1, true, IF(weekday('2019-11-27 11:00:01') BETWEEN 0 AND 4, true, false))
	
################################## END QUERY ##################################

2019-11-27T12:00:06 message notification for Contract ..
2019-11-27T12:00:07 .. message notification finished with 0 affected rows
results:
отправлено уведомлений об объявлениях с ЕФРСБ: 2