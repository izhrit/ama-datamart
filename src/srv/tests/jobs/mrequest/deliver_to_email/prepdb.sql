﻿delete from SentEmail;
delete from MRequest;

insert into efrsb_sro set id_SRO=3
,RegNum='213'
,INN='213'
,Revision=3
,ShortTitle='Стратегия'
;

insert into Manager set id_SRO=3
,firstName='Иван'
,lastName='Иванов'
,middleName='Иванович'
,ManagerEmail='test@test.ru'
,TimeCreated='2019-06-30 10:01:00'
,HasIncoming=0
;


insert into MRequest set id_Court=1
,id_SRO=3
,DebtorName='Федоров Николай Владимирович'
,CourtDecisionURL='https://kad.arbitr.ru/Kad/PdfDocument/148f8e04-cd85-4bf4-8527-15c46e48f130/e87e4569-fa1e-4563-b4ef-5bf860957fee/A82-1156-2021_20210318_Opredelenie.pdf'
,DebtorCategory='n'
,DateOfCreation='2021-06-30 10:01:00'
,DateOfRequest='2021-06-20 10:01:00'
,CaseNumber='А76-21928/2021'
;

insert into MRequest set id_Court=1
,id_SRO=3
,DebtorName='ПАО Сбербанк'
,CourtDecisionURL='https://kad.arbitr.ru/Kad/PdfDocument/148f8e04-cd85-4bf4-8527-15c46e48f130/e87e4569-fa1e-4563-b4ef-5bf860957fee/A82-1156-2021_20210318_Opredelenie.pdf'
,DebtorCategory='l'
,DateOfCreation='2021-06-30 10:01:00'
,DateOfRequest='2021-06-20 10:01:00'
,CaseNumber='А76-21928/2022'
;

