delete from mock_efrsb_email_manager;
delete from mock_efrsb_message;
delete from mock_efrsb_debtor;
delete from ProcedureStart;
delete from mrequest;
delete from PreDebtor;
delete from Court;


insert into mock_efrsb_debtor
set 
BankruptId=345786
,Revision=100
,Name='ПАО \"Тутаевский моторный завод\"'
,INN='7611000399'
,Body=compress('{"!Address":"СВЕРДЛОВСКАЯ, ЕКАТЕРИНБУРГ, БЕЛИНСКОГО, 132, 192"}')
;

INSERT INTO mock_efrsb_message set 
  `id_Message`= 8, `ArbitrManagerID`= 1, `BankruptId`= 345786, `INN`= '3812141040'
, `OGRN`= '1136671037953', `PublishDate`= '2021-09-20 05:31:12', `MessageInfo_MessageType`= 'a'
, `Number`= '2088745', `MessageGUID`= 'D43F6E8B0F195399C6949BBE5586917A', `efrsb_id`= '2088746', `Revision`=1,Body=compress('{"test":"test"}');

INSERT INTO mock_efrsb_message set 
  `id_Message`= 9, `ArbitrManagerID`= 1, `BankruptId`= 345786, `INN`= '3812141040'
, `OGRN`= '1136671037953', `PublishDate`= '2021-09-21 05:31:12', `MessageInfo_MessageType`= 'a'
, `Number`= '2088746', `MessageGUID`= 'D43F6E8B0F195399C6949BBE5586917b', `efrsb_id`= '2088747', `Revision`=2,Body=compress('{"test":"test"}');

INSERT INTO mock_efrsb_message set 
  `id_Message`= 10, `ArbitrManagerID`= 1, `BankruptId`= 345786, `INN`= '3812141040'
, `OGRN`= '1136671037953', `PublishDate`= '2021-09-22 05:31:12', `MessageInfo_MessageType`= 'a'
, `Number`= '2088747', `MessageGUID`= 'D43F6E8B0F195399C6949BBE5586917c', `efrsb_id`= '2088748', `Revision`=3,Body=compress('{"test":"test"}');


insert into Court set id_Court=3, Name="АС Ярославской области";

insert into ProcedureStart set id_Court=3
,DebtorName='ПАО \"Тутаевский моторный завод\"'
,DebtorINN="7611000399"
,DateOfApplication="2021-03-01 00:00:00"
,TimeOfCreate="2021-01-19 00:00:00"
;
