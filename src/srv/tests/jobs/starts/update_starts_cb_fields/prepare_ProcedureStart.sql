insert into Court set id_Court=3, Name="АС Ярославской области";
insert into Court set id_Court=4, Name="АС города Севастополя";

insert into ProcedureStart set id_Court=3
,DebtorName='ПАО \"Тутаевский моторный завод\"'
,DebtorINN="7611000399"
,DebtorOGRN="1027601272082"
,TimeOfCreate=now()
;

insert into ProcedureStart set id_Court=4
,DebtorName= 'ООО \"Алвион Европа\"'
,DebtorINN='9204001129'
,DebtorOGRN='1149204001968'
,TimeOfCreate=now()
;
