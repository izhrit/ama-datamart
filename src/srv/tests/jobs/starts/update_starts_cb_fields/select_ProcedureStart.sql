select 
	id_ProcedureStart
	, DebtorName
	, DebtorINN
	, DebtorOGRN
	, DebtorSNILS
	, CaseNumber
	, NextSessionDate
	, TimeOfLastChecking 
from ProcedureStart\G

select Name, Id
from access_log
inner join log_type on log_type.id_Log_type=access_log.id_Log_type
where log_type.id_Log_type<>-1
order by Time, id_Log
;