<?php 

require_once '../assets/helpers/client.rest.php';
require_once '../assets/libs/upload2bt/client.bt.php';

function test_bt_auth($domain,$login,$password)
{
	$bankro_tech_client= new Bankro_tech_client(new Test_logged_rest_client());
	$auth_info= $bankro_tech_client->login($domain,$login,$password);

	$bankro_tech_client->stop_logging();

	clear_bt($bankro_tech_client,true);

	print_r($auth_info);
}

function test_bt_func($domain,$login,$password,$func)
{
	$bankro_tech_client= new Bankro_tech_client(new Test_logged_rest_client(30));
	$bankro_tech_client->login($domain,$login,$password);
	$res= $func($bankro_tech_client);
	print_r($res);
}

function test_bt_GetGroupedProjects($domain,$login,$password,$FullSearchString= '')
{
	global $gFullSearchString;
	$gFullSearchString= $FullSearchString;
	test_bt_func($domain,$login,$password,function($bankro_tech_client)
	{
		global $gFullSearchString;
		return $bankro_tech_client->GetGroupedProjects_NotArchived($gFullSearchString);
	});
}

function test_ProjectCustomValues_for_projectId($domain,$login,$password,$id)
{
	global $test_projectId;
	$test_projectId= $id;
	test_bt_func($domain,$login,$password,function($bankro_tech_client)
	{
		global $test_projectId;
		$ProjectCustomValues= $bankro_tech_client->ProjectCustomValues_for_projectId($test_projectId);

		$Номер_дела= Номер_дела_из_ProjectCustomValues($ProjectCustomValues);
		echo "Номер дела:\"$Номер_дела\"\r\n";

		return $ProjectCustomValues;
	});
}

function test_GetRequirements_for_projectId($domain,$login,$password,$id)
{
	global $test_projectId;
	$test_projectId= $id;
	test_bt_func($domain,$login,$password,function($bankro_tech_client){
		global $test_projectId;
		$requirements= $bankro_tech_client->GetRequirementsByProjectId($test_projectId);
		return $requirements;
	});
}

function test_GetParticipants($domain,$login,$password,$txt_filter_fields)
{
	global $filter_fields;
	$fixed_txt_filter_fields= str_replace("'","\"",$txt_filter_fields);
	$filter_fields= json_decode($fixed_txt_filter_fields);
	test_bt_func($domain,$login,$password,function($bankro_tech_client){
		global $filter_fields;
		$participants= $bankro_tech_client->GetParticipants($filter_fields);
		return $participants;
	});
}

function test_GetProject_for_projectId($domain,$login,$password,$id)
{
	global $test_projectId;
	$test_projectId= $id;
	test_bt_func($domain,$login,$password,function($bankro_tech_client)
	{
		global $test_projectId;
		return $bankro_tech_client->GetProject("?projectId=$test_projectId");
	});
}

function test_UserProfiles_GetId($domain,$login,$password)
{
	test_bt_func($domain,$login,$password,function($bankro_tech_client)
	{
		return $bankro_tech_client->UserProfiles_GetId();
	});
}

function test_UserProfiles_Get($domain,$login,$password)
{
	test_bt_func($domain,$login,$password,function($bankro_tech_client)
	{
		return $bankro_tech_client->UserProfiles_Get();
	});
}

function fix_created_project_for_etalon($created_project)
{
	fix_volatile_field($created_project,'Id');
	fix_volatile_field($created_project,'Number');
	fix_volatile_field($created_project,'DocumentFolderId');
	fix_volatile_field($created_project,'CreationDate');
	fix_volatile_field($created_project,'ChangeDate');
	return $created_project;
}

function test_CreateProject($domain,$login,$password,$project_name)
{
	global $gproject_name;
	$gproject_name= $project_name;
	test_bt_func($domain,$login,$password,function($bankro_tech_client)
	{
		$UserProfile= $bankro_tech_client->UserProfiles_Get();
		global $gproject_name;
		$project_to_create= projectBody('КП',$gproject_name,$UserProfile);
		$created_project= $bankro_tech_client->CreateProject($project_to_create);
		clear_bt($bankro_tech_client);
		return fix_created_project_for_etalon($created_project);
	});
}

function fix_created_participant_for_etalon($created_participant)
{
	fix_volatile_field($created_participant,'Id');
	fix_volatile_field($created_participant,'CreationDate');
	return $created_participant;
}

function test_CreateParticipant($domain,$login,$password,$name,$ogrn,$inn)
{
	global $gname, $ginn, $gogrn;
	$gname= $name;
	$gogrn= $ogrn;
	$ginn= $inn;
	test_bt_func($domain,$login,$password,function($bankro_tech_client)
	{
		global $gname, $ginn, $gogrn;
		$participant_to_create= participantBody_Юридическое_лицо((object)array('Наименование'=>$gname,'ОГРН'=>$gogrn,'ИНН'=>$ginn));
		$created_participant= $bankro_tech_client->PutParticipant($participant_to_create);
		clear_bt($bankro_tech_client);
		return fix_created_participant_for_etalon($created_participant);
	});
}

function bt_tests($fixed_cmd, $prefix, $argv)
{
	switch ($fixed_cmd)
	{
		case 'login': test_bt_auth($argv[2],$argv[3],$argv[4]); break;
		case 'UserProfiles_GetId': test_UserProfiles_GetId($argv[2],$argv[3],$argv[4]); break;
		case 'UserProfiles_Get': test_UserProfiles_Get($argv[2],$argv[3],$argv[4]); break;

		case 'GetGroupedProjects': test_bt_GetGroupedProjects($argv[2],$argv[3],$argv[4],count($argv)<6?'':$argv[5]); break;
		case 'ProjectCustomValues_for_projectId': test_ProjectCustomValues_for_projectId($argv[2],$argv[3],$argv[4],$argv[5]); break;
		case 'GetProject_for_projectId': test_GetProject_for_projectId($argv[2],$argv[3],$argv[4],$argv[5]); break;
		case 'CreateProject': test_CreateProject($argv[2],$argv[3],$argv[4],$argv[5]); break;

		case 'GetRequirements_for_projectId': test_GetRequirements_for_projectId($argv[2],$argv[3],$argv[4],$argv[5]); break;

		case 'GetParticipants': test_GetParticipants($argv[2],$argv[3],$argv[4],$argv[5]); break;
		case 'CreateParticipant': test_CreateParticipant($argv[2],$argv[3],$argv[4],$argv[5],$argv[6],$argv[7]); break;

		default: echo "unknown test command $prefix$fixed_cmd\r\n";
	}
}
