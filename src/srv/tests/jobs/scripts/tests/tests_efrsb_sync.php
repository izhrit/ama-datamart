<?php 

function test_get_messages($static_date_time,$portion_size= null,$max_to_insert= null)
{
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	require_once '../assets/libs/efrsb/sync_Efrsb_message.php';
	$result= sync_messages($portion_size,$max_to_insert);
	echo "results:\r\n$result";
}

function test_get_events($static_date_time,$portion_size= null,$max_to_insert= null)
{
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	require_once '../assets/libs/efrsb/sync_Efrsb_event.php';
	$result= sync_events($portion_size,$max_to_insert);
	echo "results:\r\n$result";
}

function test_get_debtors()
{
	require_once '../assets/libs/efrsb/sync_Efrsb_debtor.php';
	$result= sync_debtors(1,1);
	echo "results:\r\n$result";
}

function test_get_managers()
{
	require_once '../assets/libs/efrsb/sync_Efrsb_manager.php';
	$result= sync_managers(1,1);
	echo "results:\r\n$result";
}

function test_get_sros()
{
	global $echo_errors, $do_not_write_errors_to_log;
	$echo_errors= true;
	$do_not_write_errors_to_log= true;

	require_once '../assets/libs/efrsb/sync_Efrsb_sro.php';
	$result= sync_sros(1,1);
	echo "results:\r\n$result";
}

function test_delete_old_messages($static_date_time)
{
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	require_once '../assets/libs/efrsb/sync_Efrsb_message.php';
	$result= delete_old_messages();
	echo "results:\r\n$result";
}

function test_delete_old_events($static_date_time)
{
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	require_once '../assets/libs/efrsb/sync_Efrsb_event.php';
	$result= delete_old_events();
	echo "results:\r\n$result";
}

function test_get_debtor_managers()
{
	require_once '../assets/libs/efrsb/sync_Efrsb_debtor_manager.php';
	$result= sync_debtor_managers(1,1);
	echo "results:\r\n$result";
}

function test_efrsb_sync_commands($fixed_cmd, $prefix, $argv)
{
	$static_date_time = !isset($argv[2]) ? '' : $argv[2];

	switch ($fixed_cmd)
	{
		case 'get_messages': test_get_messages($static_date_time,/*$portion_size=*/1,/*$max_to_insert=*/1); break;
		case 'get_events': test_get_events($static_date_time,/*$portion_size=*/1,/*$max_to_insert=*/1); break;
		case 'get_debtors': test_get_debtors(); break;
		case 'get_managers': test_get_managers(); break;
		case 'get_sros': test_get_sros(); break;
		case 'get_debtor_managers': test_get_debtor_managers(); break;

		case 'delete_old_messages': test_delete_old_messages($static_date_time); break;
		case 'delete_old_events': test_delete_old_events($static_date_time); break;

		case 'get_all_messages': test_get_messages($static_date_time); break;
		case 'get_all_events': test_get_events($static_date_time); break;

		default: echo "unknown test command $prefix$fixed_cmd\r\n";
	}
}