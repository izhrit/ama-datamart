<?php 

require_once '../assets/helpers/client.rest.php';
require_once '../assets/libs/upload2bt/client.bt.php';
require_once '../assets/libs/upload2bt/uploader.bt.php';

function load_registry_from_bt($id_Процедуры,$bt_client)
{
	$registry= (object)array();

	$project_details= $bt_client->GetProject_for_projectId($id_Процедуры);

	$registry->Номер_судебного_дела= $project_details->CasebookNumber;

	return $registry;
}

function test_UploaderAuth($domain,$login,$password)
{
	$uploader= new Registry2bt_uploader(new Bankro_tech_client(new Test_logged_rest_client(/*$atimeout_sec= */30)));

	$пустые_параметры_доступа= (object)array('domain_name'=>'','login'=>'','password'=>'');
	$параметры_доступа= (object)array('domain_name'=>$domain,'login'=>$login,'password'=>$password);
	echo "1) сессия для $domain, $login\r\n";
	$uploader->Использовать_сессию_для($параметры_доступа);
	echo "2) сессия для $domain, $login\r\n";
	$uploader->Использовать_сессию_для($параметры_доступа);
	echo "3) сессия для пустых данных\r\n";
	try
	{
		$uploader->Использовать_сессию_для($пустые_параметры_доступа);
	}
	catch (Exception $ex)
	{
		$exception_class= get_class($ex);
		$exception_Message= $ex->getMessage();
		echo "\r\nexception occurred: $exception_class - $exception_Message\r\n";
	}
}

function Параметры_тестовой_процедуры($Номер_судебного_дела,$ИНН)
{
	return (object)array(
		'Номер_судебного_дела'=>$Номер_судебного_дела
		,'Тип_процедуры'=>'Н'
		,'Должник'=>(object)array
		(
			'ИНН'=>$ИНН
			,'Наименование'=>'Роги и ноги'
		)
	);
}

function test_UploaderCasePrep($domain,$login,$password,$Номер_судебного_дела,$ИНН)
{
	$uploader= new Registry2bt_uploader(new Bankro_tech_client(new Test_logged_rest_client(/*$atimeout_sec= */30)));

	echo "авторизуемся { --------------------------------------------\r\n";
	$uploader->Использовать_сессию_для((object)array('domain_name'=>$domain,'login'=>$login,'password'=>$password));
	echo "авторизуемся } --------------------------------------------\r\n";

	clear_bt($uploader);

	$параметры_процедуры= Параметры_тестовой_процедуры($Номер_судебного_дела,$ИНН);
	echo "готовим процедуру { --------- 1 раз - создаём -------------r\n";
	$id_Процедуры= $uploader->Подготовить_процедуру($параметры_процедуры);
	echo "готовим процедуру } ---------------------------------------\r\n";
	echo "готовим процедуру { --------- 2 раз - в кэше после создания\r\n";
	$uploader->Подготовить_процедуру($параметры_процедуры);
	echo "готовим процедуру } ---------------------------------------\r\n";
	$uploader->Очистить_кэш();
	echo "готовим процедуру { --------- 3 раз - читаем из базы ------\r\n";
	$uploader->Подготовить_процедуру($параметры_процедуры);
	echo "готовим процедуру } ---------------------------------------\r\n";
	echo "готовим процедуру { --------- 4 раз - в кэше после чтения -\r\n";
	$uploader->Подготовить_процедуру($параметры_процедуры);
	echo "готовим процедуру } ---------------------------------------\r\n";

	echo "реестр в созданном деле \"$Номер_судебного_дела\":\r\n";
	print_r(load_registry_from_bt($id_Процедуры,$uploader->bt_client));

	clear_bt($uploader);
}

function Параметры_тестового_кредитора($ИНН)
{
	return (object)array('Организация'=>(object)array('ИНН'=>$ИНН,'Наименование'=>'Роги и ноги'));
}

function test_UploaderParticipantPrep($domain,$login,$password,$ИНН)
{
	$uploader= new Registry2bt_uploader(new Bankro_tech_client(new Test_logged_rest_client(/*$atimeout_sec= */30)));

	echo "авторизуемся { --------------------------------------------\r\n";
	$uploader->Использовать_сессию_для((object)array('domain_name'=>$domain,'login'=>$login,'password'=>$password));
	echo "авторизуемся } --------------------------------------------\r\n";

	clear_bt($uploader);

	$параметры_кредитора= Параметры_тестового_кредитора($ИНН);
	echo "готовим кредитора { --------- 1 раз - создаём -------------\r\n";
	$uploader->Подготовить_кредитора(null,$параметры_кредитора,0);
	echo "готовим кредитора } ---------------------------------------\r\n";
	echo "готовим кредитора { --------- 2 раз - в кэше после создания\r\n";
	$uploader->Подготовить_кредитора(null,$параметры_кредитора,0);
	echo "готовим кредитора } ---------------------------------------\r\n";
	$uploader->Очистить_кэш();
	echo "готовим кредитора { --------- 3 раз - читаем из базы ------\r\n";
	$uploader->Подготовить_кредитора(null,$параметры_кредитора,0);
	echo "готовим кредитора } ---------------------------------------\r\n";
	echo "готовим кредитора { --------- 4 раз - в кэше после чтения -\r\n";
	$uploader->Подготовить_кредитора(null,$параметры_кредитора,0);
	echo "готовим кредитора } ---------------------------------------\r\n";

	clear_bt($uploader);
}

function fix_created_requirement_for_etalon($created_requirement)
{
	fix_volatile_field($created_requirement,'Id');
	fix_volatile_field($created_requirement->DocumentFolder,'Id');
	fix_volatile_field($created_requirement->Project,'Id');
	fix_volatile_field($created_requirement->Creditor,'Id');
	foreach ($created_requirement->Items as $Item)
	{
		fix_volatile_field($Item,'Id');
	}
	return $created_requirement;
}

function test_UploaderRequirementAdd($domain,$login,$password,$Номер_судебного_дела,$ИНН,$сумма)
{
	$uploader= new Registry2bt_uploader(new Bankro_tech_client(new Test_logged_rest_client(/*$atimeout_sec= */30)));

	echo "авторизуемся { --------------------------------------------\r\n";
	$uploader->Использовать_сессию_для((object)array('domain_name'=>$domain,'login'=>$login,'password'=>$password));
	echo "авторизуемся } --------------------------------------------\r\n";

	clear_bt($uploader);

	$параметры_процедуры= Параметры_тестовой_процедуры($Номер_судебного_дела,'6410002242');
	echo "готовим процедуру { ---------------------------------------\r\n";
	$id_Процедуры= $uploader->Подготовить_процедуру($параметры_процедуры);
	echo "готовим процедуру } ---------------------------------------\r\n";

	$параметры_кредитора= Параметры_тестового_кредитора($ИНН);
	echo "готовим кредитора { ---------------------------------------\r\n";
	$id_Кредитора= $uploader->Подготовить_кредитора(null,$параметры_кредитора,0);
	echo "готовим кредитора } ---------------------------------------\r\n";

	$параметры_требования= (object)array(
		'Очередь'=>'3.1'
		,'Размер'=>$сумма
		,'Реестр'=>(object)array('Внесено'=>(object)array('Дата'=>'09.03.2013'))
	);
	$параметры_кредитора->Требования= array($параметры_требования);
	echo "добавляем требование { ------------------------------------\r\n";
	$added_requirement= $uploader->Добавить_требование_вернуть_requirement($id_Процедуры,$id_Кредитора,$параметры_требования,1);
	echo "добавляем требование } ------------------------------------\r\n";

	echo "очищаем требования { --------------------------------------\r\n";
	$uploader->Очистить_требования_для_id_Процедуры($id_Процедуры);
	echo "очищаем требования } --------------------------------------\r\n";

	echo "созданное требование:\r\n";
	fix_created_requirement_for_etalon($added_requirement);
	print_r($added_requirement);

	echo "реестр в созданном деле \"$Номер_судебного_дела\":\r\n";
	print_r(load_registry_from_bt($id_Процедуры,$uploader->bt_client));

	clear_bt($uploader);
}

function fix_downloaded_date($date)
{
	$parts= explode('-',$date);
	return $parts[2].'.'.$parts[1].'.'.$parts[0];
}

function download_registry_Требование($bt_client, $project, $requirement, $rr)
{
	$rrequirement= $bt_client->GetRequirement($rr->Id);

	$требование= (object)array();

	foreach ($rrequirement->Items as $item)
	{
		if (isset($item->State->SysName) && 'EstablishedByCourt'==$item->State->SysName)
		{
			if (isset($item->Type->SysName) && 'NoCollateral'==$item->Type->SysName)
			{
				$требование->Очередь= '3';
				$требование->Реестр= (object)array('Внесено'=>(object)array('Дата'=>fix_downloaded_date($item->Date)));
				$требование->Размер= $item->Amount;
				$требование->Вид_обязательства= 'Основной долг';
			}
		}
	}

	$требование->Основания= (object)array('Текстом'=>'','Документы'=>array());
	$требование->Погашения= array();
	foreach ($rrequirement->Payments as $payment)
	{
		$требование->Погашения[]= (object)array
		(
			'Дата'=>fix_downloaded_date($payment->Date)
			,'Сумма'=>$payment->Amount
		);
	}

	$требование->Залоговые_обязательства= array();

	$properties= $bt_client->GetRelatedObjects_RequirementToProperties($rr->Id);
	foreach ($properties as $rproperty)
	{
		$property= $bt_client->GetProperty($rproperty->Id);
		$Залог= (object)array();
		foreach ($property->PropertyContracts as $contract)
		{
			$Залог->Основания= $contract->NameBasisCollateralSum;
			$Залог->Размер_залогового_обеспечения= $contract->CollateralSum;
		}
		$Залог->Предмет= (object)array('Текстом'=>$property->Name);
		$требование->Залоговые_обязательства[]= $Залог;
	}

	$требование->Документы= array();

	return $требование;
}

function download_registry_Кредитор($bt_client, $project, $requirement)
{
	$participant= $bt_client->GetParticipant($requirement->Id);

	$лицо= null;
	$кредитор= null;
	if ('individual'!=$participant->Type->NameEn)
	{
		$лицо= (object)array('Наименование'=>$requirement->DisplayName);
		$кредитор= (object)array('Организация'=>$лицо);
		if (isset($participant->ContactDetail->Address))
			$лицо->Адрес= $participant->ContactDetail->Address;
	}
	else
	{
		$лицо= (object)array
		(
			'Имя'=>$participant->FirstName
			,'Фамилия'=>$participant->LastName
			,'Отчество'=>$participant->MiddleName
		);
		$кредитор= (object)array('Физическое_лицо'=>$лицо);
	}

	if (isset($participant->ContactDetail->Address))
		$кредитор->Адрес_почтовых_уведомлений= $participant->ContactDetail->Address;

	if (isset($participant->ContactDetail->Phone))
		$кредитор->Контактные_телефоны= $participant->ContactDetail->Phone;

	if (''!=$participant->OGRN)
		$лицо->ОГРН= $participant->OGRN;
	if (''!=$participant->INN)
		$лицо->ИНН= $participant->INN;

	$требования= array();
	foreach ($requirement->Requirements as $rr)
	{
		$требования[]= download_registry_Требование($bt_client, $project, $requirement, $rr);
	}

	$кредитор->Требования= $требования;

	return $кредитор;
}

function download_registry_Кредиторы($bt_client, $project)
{
	$Кредиторы= array();

	$requirements= $bt_client->GetRequirementsByProjectId($project->Id);
	foreach ($requirements as $requirement)
	{
		$Кредиторы[]= download_registry_Кредитор($bt_client, $project, $requirement);
	}

	return $Кредиторы;
}

function download_registry($bt_client,$project_id)
{
	echo "выкачиваем реестр { ---------------------------------------\r\n";
	$registry= (object)array();

	$ProjectCustomValues= $bt_client->ProjectCustomValues_for_projectId($project_id);
	$project= $bt_client->GetProject_for_projectId($project_id);

	$registry->Схема= '2';
	$registry->Информация_на_дату= '';

	$должник= $bt_client->GetParticipant($project->Client->Id);

	$registry->Должник= (object)array
	(
		'Наименование'=>$project->Client->Name
		,'ОГРН'=>$должник->OGRN
		,'ИНН'=>$должник->INN
	);

	$procedure_fields= fields_from_ProjectCustomValues($ProjectCustomValues);

	$registry->Номер_судебного_дела= $project->CasebookNumber;
	$registry->Тип_процедуры= $procedure_fields->Тип_процедуры;
	$registry->Особенности= '';

	$registry->Кредиторы= download_registry_Кредиторы($bt_client,$project);

	echo "выкачиваем реестр } ---------------------------------------\r\n";
	return $registry;
}

function test_UploaderUpload($domain,$login,$password,$registry_file_path)
{
	require_once '../assets/libs/codecs/registry.codec.php';
	require_once '../assets/libs/upload2bt/upload_registry.php';

	$параметры_доступа= (object)array('domain_name'=>$domain,'login'=>$login,'password'=>$password);

	$uploader= new Registry2bt_uploader(new Bankro_tech_client(new Test_logged_rest_client(/*$atimeout_sec= */30)));

	echo "авторизуемся { --------------------------------------------\r\n";
	$uploader->Использовать_сессию_для($параметры_доступа);
	echo "авторизуемся } --------------------------------------------\r\n";

	clear_bt($uploader);

	$registry_xml= file_get_contents($registry_file_path);
	$registry_codec= new Registry_codec();
	$registry= $registry_codec->Decode($registry_xml);

	echo "загружаем реестр { --------- 1 раз - создаём ------------\r\n";
	$id_Процедуры= UploadRegistry($registry,$uploader,$параметры_доступа);
	echo "загружаем реестр } --------------------------------------\r\n";

	$dregistry= download_registry($uploader->bt_client,$id_Процедуры);
	file_put_contents("$registry_file_path.uploaded.result.xml",$registry_codec->Encode($dregistry));

	sleep(5); // 3 секунды нужно для того, чтобы проиндексировались физ лица по отчеству
	
	echo "загружаем реестр { -------- 2 раз - в кэше после создания\r\n";
	$id_Процедуры2= UploadRegistry($registry,$uploader,$параметры_доступа);
	echo "загружаем реестр } --------------------------------------\r\n";

	$dregistry2= download_registry($uploader->bt_client,$id_Процедуры2);
	if ($dregistry!=$dregistry2 || $id_Процедуры2!=$id_Процедуры)
	{
		echo "2й реестр отличается!\r\n";
		file_put_contents("$registry_file_path.uploaded.result2.xml",$registry_codec->Encode($dregistry2));
	}

	$uploader->Очистить_кэш();
	echo "загружаем реестр { -------- 3 раз - читаем из базы ------\r\n";
	$id_Процедуры3= UploadRegistry($registry,$uploader,$параметры_доступа);
	echo "загружаем реестр } --------------------------------------\r\n";

	$dregistry3= download_registry($uploader->bt_client,$id_Процедуры3);
	if ($dregistry!=$dregistry3 || $id_Процедуры3!=$id_Процедуры)
	{
		echo "3й реестр отличается!\r\n";
		file_put_contents("$registry_file_path.uploaded.result3.xml",$registry_codec->Encode($dregistry3));
	}

	echo "загружаем реестр { -------- 4 раз - в кэше после чтения--\r\n";
	$id_Процедуры4= UploadRegistry($registry,$uploader,$параметры_доступа);
	echo "загружаем реестр } --------------------------------------\r\n";

	$dregistry4= download_registry($uploader->bt_client,$id_Процедуры4);
	if ($dregistry!=$dregistry4 || $id_Процедуры4!=$id_Процедуры)
	{
		echo "4й реестр отличается!\r\n";
		file_put_contents("$registry_file_path.uploaded.result4.xml",$registry_codec->Encode($dregistry4));
	}

	clear_bt($uploader);
}

function uploader_tests($fixed_cmd, $prefix, $argv)
{
	try
	{
		switch ($fixed_cmd)
		{
			case 'auth': test_UploaderAuth($argv[2],$argv[3],$argv[4]); break;

			case 'case.prep': test_UploaderCasePrep($argv[2],$argv[3],$argv[4],$argv[5],$argv[6]); break;
			case 'participant.prep': test_UploaderParticipantPrep($argv[2],$argv[3],$argv[4],$argv[5]); break;
			case 'requirement.add': test_UploaderRequirementAdd($argv[2],$argv[3],$argv[4],$argv[5],$argv[6],$argv[7]); break;

			case 'upload': test_UploaderUpload($argv[2],$argv[3],$argv[4],$argv[5]); break;

			default: echo "unknown test command $prefix$fixed_cmd\r\n";
		}
	}
	catch (Rest_exception $ex)
	{
		echo "catch rest excaption with responce:\r\n";
		echo $ex->responce;
		echo "-----------------------------------\r\n";
		throw $ex;
	}
}
