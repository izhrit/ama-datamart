<?php 

function test_verify_Debtors()
{
	require_once '../assets/libs/verify/verify_Debtors.php';
	$result= verify_Debtors();
	echo "results:\r\n$result";
}

function test_verify_Managers()
{
	require_once '../assets/libs/verify/verify_Managers.php';
	$result= verify_Managers();
	echo "results:\r\n$result";
}

function test_verify_Procedures()
{
	require_once '../assets/libs/verify/verify_Procedures.php';
	$result= verify_Procedures();
	echo "results:\r\n$result";
}

function tests_verify($fixed_cmd, $prefix, $argv)
{
	switch ($fixed_cmd)
	{
		case 'Debtors': test_verify_Debtors(); break;
		case 'Managers': test_verify_Managers(); break;
		case 'Procedures': test_verify_Procedures(); break;

		default: echo "unknown test command $prefix$fixed_cmd\r\n";
	}
}