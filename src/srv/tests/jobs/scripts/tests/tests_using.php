<?php 

function test_index_procedure()
{
	require_once '../assets/libs/using/using_procedure_index.php';
	$result= move_procedure_using_to_indexed();
	echo "results:\r\n$result";
}

function tests_using_commands($fixed_cmd, $prefix, $argv)
{
	switch ($fixed_cmd)
	{
		case 'procedure.index': test_index_procedure(); break;

		default: echo "unknown test command $prefix$fixed_cmd\r\n";
	}
}