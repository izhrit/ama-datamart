<?php 

function test_print_r($data)
{
	$res= print_r($data,true);
	$res= str_replace("\r\n","\n",$res);
	$res= str_replace("\n","\r\n",$res);
	echo $res;
}

function test_read_decode_Registry($file_path_in)
{
	require_once '../assets/libs/codecs/registry.codec.php';

	$registry_xml= file_get_contents($file_path_in);

	$registry_codec= new Registry_codec();
	$registry= $registry_codec->Decode($registry_xml);
	test_print_r($registry);
}

function test_read_decode_Report($file_path_in)
{
	require_once '../assets/libs/codecs/report.codec.php';

	$eport_xml= file_get_contents($file_path_in);

	$report_codec= new Report_codec();
	$report= $report_codec->Decode($eport_xml);
	test_print_r($report);
}

function test_read_decode_KM($file_path_in)
{
	require_once '../assets/libs/codecs/km.codec.php';

	$km_xml= file_get_contents($file_path_in);

	$km_codec= new KM_codec();
	$km= $km_codec->Decode($km_xml);
	test_print_r($km);
}

function test_read_decode_RTT($file_path_in)
{
	require_once '../assets/libs/codecs/current_claims.codec.php';

	$rtt_xml= file_get_contents($file_path_in);

	$rtt_codec= new Current_claims_codec();
	$rtt= $rtt_codec->Decode($rtt_xml);
	test_print_r($rtt);
}

function test_read_decode_Deals($file_path_in)
{
	require_once '../assets/helpers/codec.xml.php';
	require_once '../assets/libs/codecs/deals.codec.php';

	$deals_xml= file_get_contents($file_path_in);

	$deals_codec= new Deals_codec();
	$deals= $deals_codec->Decode($deals_xml);
	test_print_r($deals);
}

function tests_decode($fixed_cmd, $prefix, $argv)
{
	switch ($fixed_cmd)
	{
		case 'registry': test_read_decode_Registry($argv[2]); break;
		case 'report': test_read_decode_Report($argv[2]); break;
		case 'km': test_read_decode_KM($argv[2]); break;
		case 'rtt': test_read_decode_RTT($argv[2]); break;
		case 'deals': test_read_decode_Deals($argv[2]); break;

		default: echo "unknown test command $prefix$fixed_cmd\r\n";
	}
}