<?php 

class Test_logged_rest_client extends Logged_rest_client
{
	public function __construct($atimeout_sec= 5)
	{
		parent::__construct(new Test_logger(), $atimeout_sec);
	}

	static $url_to_log_pattern_replacements= array
	(
		array(	'p'=>'/api\/projects\/Archive\?Id=(.+)/i'
		,		'r'=>'api/projects/Archive?Id=fixed_id')

		,array(	'p'=>'/api\/projects\/DeleteProject\?Id=(.+)/i'
		,		'r'=>'api/projects/DeleteProject?Id=fixed_id')

		,array(	'p'=>'/api\/projects\/GetProject\?ProjectId=(.+)/i'
		,		'r'=>'api/projects/GetProject?ProjectId=fixed_id')

		,array(	'p'=>'/api\/Requirements\/GetRequirement\?Id=(.+)/i'
		,		'r'=>'api/Requirements/GetRequirement?Id=fixed_id')

		,array(	'p'=>'/api\/Participants\/GetParticipant\?participantId=(.+)/i'
		,		'r'=>'api/Participants/GetParticipant?participantId=fixed_id')

		,array(	'p'=>'/api\/Properties\/GetProperty\?Id=(.+)/i'
		,		'r'=>'api/Properties/GetProperty?Id=fixed_id')

		,array(	'p'=>'/api\/participants\/DeleteParticipant\/(.+)/i'
		,		'r'=>'api/participants/DeleteParticipant/fixed_id')

		,array(	'p'=>'/api\/RelatedObjects\/GetRelatedObjects\?Criterion.id=(.+)/i'
		,		'r'=>'api/RelatedObjects/GetRelatedObjects?Criterion.id=fixed_id')

		,array(	'p'=>'/api\/ProjectCustomValues\/\?request.projectId=(.+)/i'
		,		'r'=>'api/ProjectCustomValues/?request.projectId=fixed_id')
	);

	protected function prepare_url_to_log($url)
	{
		foreach (self::$url_to_log_pattern_replacements as $pattern_replacement)
		{
			$url= preg_replace($pattern_replacement['p'], $pattern_replacement['r'], $url);
		}
		return $url;
	}
}
