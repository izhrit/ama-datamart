<?php 

function clear_bt($bt,$quiet_mode= false)
{
	if (isset($bt->bt_client))
		$bt= $bt->bt_client;

	echo "  чистим базу { -------------------------------------------\r\n";

	$project_groups= $bt->GetGroupedProjects_NotArchived();
	foreach ($project_groups as $project_group)
	{
		foreach ($project_group->Projects as $project)
		{
			$bt->ArchiveProject($project->Id);
			$bt->DeleteProject($project->Id);
		}
	}


	for ($participants= $bt->GetParticipants((object)array()); 
			0!=count($participants); 
			$participants= $bt->GetParticipants((object)array()))
	{
		foreach ($participants as $participant)
		{
			$name= $participant->DisplayName;
			if (!$quiet_mode)
				echo "$name\r\n";
			$bt->DeleteParticipant($participant->Id);
		}
	}

	echo "очистили базу } -------------------------------------------\r\n";
}
