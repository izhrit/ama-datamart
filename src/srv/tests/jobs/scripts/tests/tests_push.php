<?php

function trace($obj)
{
	$txt= print_r($obj,true);
	$txt= str_replace("\r\n","\n",$txt);
	$txt= str_replace("\n","\r\n",$txt);
	echo $txt;
}

function send_notification_to_firebase($token, $push_notification, $push_data)
{
	echo "send_notification_to_firebase {\r\n";
	echo "token:$token\r\n";
	echo "notification:\r\n";
	trace($push_notification);
	echo "data:\r\n";
	trace($push_data);
	echo "send_notification_to_firebase }\r\n";
	return 'ok';
}

function test_dispatch_efrsb_events($static_date_time)
{
	require_once '../assets/libs/push/dispatch_efrsb_events.php';

	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);
	$result= dispatch_efrsb_events();
	echo "results:\r\n$result";
}

function test_dispatch_gc_events($static_date_time)
{
	require_once '../assets/libs/push/dispatch_gc_events.php';
	
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);
	$result= dispatch_gc_events();
	echo "results:\r\n$result";
}

function test_deliver_efrsb_events($static_date_time)
{
	require_once '../assets/libs/push/deliver_efrsb_events.php';
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	$result= deliver_efrsb_events($static_date_time);
	echo "results:\r\n$result";
}

function test_deliver_gc_events($static_date_time)
{
	require_once '../assets/libs/push/deliver_gc_events.php';
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	$result= deliver_gc_events($static_date_time);
	echo "results:\r\n$result";
}

function test_dispatch_efrsb_messages($static_date_time)
{
	require_once '../assets/libs/push/dispatch_efrsb_messages.php';
	global $max_message_day_age_to_dispatch;
	$max_message_day_age_to_dispatch= 1000;

	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	$result= dispatch_efrsb_messages();
	echo "results:\r\n$result";
}

function test_deliver_efrsb_messages($static_date_time)
{
	require_once '../assets/libs/push/deliver_efrsb_messages.php';
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);
	deliver_efrsb_messages($static_date_time);
}

function test_delete_old_pushed_efrsb_events($static_date_time)
{
	require_once '../assets/libs/push/delete_old_pushed_efrsb_events.php';

	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	$result= delete_old_pushed_efrsb_events(/*max_pushed_days_age=*/1);
	echo "results:\r\n$result";
}

function test_delete_old_pushed_efrsb_messages($static_date_time)
{
	require_once '../assets/libs/push/delete_old_pushed_efrsb_messages.php';

	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	$result= delete_old_pushed_efrsb_messages(1);
	echo "results:\r\n$result";
}

function test_delete_old_pushed_gc_events($static_date_time)
{
	require_once '../assets/libs/push/delete_old_pushed_gc_events.php';

	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	$result= delete_old_pushed_efrsb_events(/*max_pushed_days_age=*/1);
	echo "results:\r\n$result";
}

function tests_push_commands($fixed_cmd, $prefix, $argv)
{
	switch ($fixed_cmd)
	{
		case 'dispatch_gc_events': test_dispatch_gc_events($argv[2]); break;
		case 'deliver_gc_events': test_deliver_gc_events($argv[2]); break;
		case 'delete_old_pushed_gc_events': test_delete_old_pushed_gc_events($argv[2]); break;

		case 'dispatch_efrsb_events': test_dispatch_efrsb_events($argv[2]); break;
		case 'deliver_efrsb_events': test_deliver_efrsb_events($argv[2]); break;
		case 'delete_old_pushed_efrsb_events': test_delete_old_pushed_efrsb_events($argv[2]); break;

		case 'dispatch_efrsb_messages': test_dispatch_efrsb_messages($argv[2]); break;
		case 'deliver_efrsb_messages': test_deliver_efrsb_messages($argv[2]); break;
		case 'delete_old_pushed_efrsb_messages': test_delete_old_pushed_efrsb_messages($argv[2]); break;

		default: echo "unknown test command $prefix$fixed_cmd\r\n";
	}
}
