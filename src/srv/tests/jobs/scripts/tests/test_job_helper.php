<?php 

function success_job1()
{
	echo job_time() . " action for success_job1!\r\n";
}

function success_job2()
{
	echo job_time() . " action for success_job2!\r\n";
	return (object)array('status'=>3,'results'=>'i got it!');
}

function success_job3()
{
	echo job_time() . " action for success_job3!\r\n";
	return 'everything is ok!';
}

function exception_job()
{
	echo job_time() . " action for exception_job!\r\n";
	throw new Exception('test job exception!');
}

function crash_job()
{
	echo "action for crash_job!\r\n";
	exit;
}

global $success_job_parts, $exception_job_parts, $crash_job_parts, $changed_success_job_parts;
$success_job_parts= array
(
	 array('exec'=>'success_job1', 'title'=>'success job1')
	,array('exec'=>'success_job2', 'title'=>'success job2')
	,array('exec'=>'success_job3', 'title'=>'success job3')
);

$changed_success_job_parts= array
(
	 array('exec'=>'success_job1', 'title'=>'changed success job1')
	,array('exec'=>'success_job2', 'title'=>'changed success job2')
);

$exception_job_parts= array
(
	 array('exec'=>'success_job1', 'title'=>'success job1')
	,array('exec'=>'exception_job', 'title'=>'job with exception')
	,array('exec'=>'success_job2', 'title'=>'success job2')
);

$crash_job_parts= array
(
	 array('exec'=>'success_job1', 'title'=>'success job1')
	,array('exec'=>'crash_job', 'title'=>'job with crash')
	,array('exec'=>'success_job2', 'title'=>'success job2')
);

function test_job_success($pid_file_path)
{
	global $success_job_parts;
	safe_execute_job_parts_locked_by_pid_file($success_job_parts,$pid_file_path);
}

function test_job_exception($pid_file_path)
{
	global $exception_job_parts;
	safe_execute_job_parts_locked_by_pid_file($exception_job_parts,$pid_file_path);
}

function prep_Job_logger()
{
	$job_db_logger= new Job_db_logger();

	$job_db_logger->site= (object)array('title'=>'test site','description'=>'test site description');
	$job_db_logger->job= (object)array('title'=>'test job','description'=>'test job description', 'max_age_minutes'=>60*24);

	return $job_db_logger;
}

function prep_changed_Job_logger()
{
	$job_db_logger= new Job_db_logger();

	$job_db_logger->site= (object)array('title'=>'test site','description'=>'changed test site description');
	$job_db_logger->job= (object)array('title'=>'test job','description'=>'changed test job description', 'max_age_minutes'=>60*24);

	return $job_db_logger;
}

function test_job_db_success($pid_file_path)
{
	$job_db_logger= prep_Job_logger();
	global $success_job_parts;
	safe_execute_job_parts_locked_by_pid_file_with_db_log($success_job_parts,$pid_file_path,$job_db_logger);
}

function test_job_db_success_changed($pid_file_path)
{
	$job_db_logger= prep_changed_Job_logger();
	global $changed_success_job_parts;
	safe_execute_job_parts_locked_by_pid_file_with_db_log($changed_success_job_parts,$pid_file_path,$job_db_logger);
}

function test_job_db_exception($pid_file_path)
{
	$job_db_logger= prep_Job_logger();
	global $exception_job_parts;
	safe_execute_job_parts_locked_by_pid_file_with_db_log($exception_job_parts,$pid_file_path,$job_db_logger);
}

function test_job_crash($pid_file_path)
{
	global $crash_job_parts;
	safe_execute_job_parts_locked_by_pid_file($crash_job_parts,$pid_file_path);
}

require_once '../assets/libs/remote_jobs/update_remote_job_logs.php';

function test_sync_remote_efrsb_job_log()
{
	sync_remote_efrsb_job_log();
}

function test_job_helper($fixed_cmd, $prefix, $argv)
{
	switch ($fixed_cmd)
	{
		case 'success': test_job_success($argv[2]); break;
		case 'exception': test_job_exception($argv[2]); break;
		case 'crash': test_job_crash($argv[2]); break;

		case 'db.success': test_job_db_success($argv[2]); break;
		case 'db.success_changed': test_job_db_success_changed($argv[2]); break;
		case 'db.exception': test_job_db_exception($argv[2]); break;
		case 'db.crash': test_job_crash($argv[2]); break;

		case 'sync-remote-efrsb-job-log': test_sync_remote_efrsb_job_log(); break;

		default: echo "unknown test command $prefix$fixed_cmd\r\n";
	}
}