<?php 

class Test_logger extends Logger
{
	private $microseconds= 0;
	public function microtime_as_float()
	{
		$this->microseconds++;
		return $this->microseconds;
	}

	static $log_pattern_replacements= array
	(
		array(	'p'=>'/id=\"(.+)\"/i'
		,		'r'=>'id="fixed_id"')
	);

	public function base_log_txt($txt)
	{
		foreach (self::$log_pattern_replacements as $pattern_replacement)
		{
			$txt= preg_replace($pattern_replacement['p'], $pattern_replacement['r'], $txt);
		}
		parent::base_log_txt($txt);
	}
}

function fix_volatile_field($o,$name)
{
	if (isset($o->$name))
		$o->$name= "fixed_$name";
}
