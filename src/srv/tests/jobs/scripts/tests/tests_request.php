<?php

function trace($obj)
{
	$txt= print_r($obj,true);
	$txt= str_replace("\r\n","\n",$txt);
	$txt= str_replace("\n","\r\n",$txt);
	echo $txt;
}

function test_deliver_manager_requests($static_date_time)
{
	require_once '../assets/libs/requests/deliver_manager_requests.php';
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);
	deliver_manager_requests(100);
}

function tests_request_commands($fixed_cmd, $prefix, $argv)
{
	switch ($fixed_cmd)
	{
		case 'deliver_manager_requests': test_deliver_manager_requests($argv[2]); break;
		default: echo "unknown test command $prefix$fixed_cmd\r\n";
	}
}
