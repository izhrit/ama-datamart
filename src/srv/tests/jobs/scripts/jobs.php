<?php 

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';

function test_parse_Leaks()
{
	require_once '../assets/libs/parse_Leaks.php';
	ProcessPData();
}

function test_upload_from_db($account)
{
	require_once '../assets/libs/upload2bt/logger.php';
	require_once '../assets/libs/upload2bt/upload_from_db.php';
	require_once '../assets/libs/upload2bt/uploader.mock.php';
	require_once '../assets/helpers/client.rest.php';
	require_once 'tests/logger.php';

	$uploader= new Registry_uploader(new Test_logger());

	global $bt_log_time;
	$bt_log_time= false;

	if ('wrong_account'==$account)
	{
		global $test_bt_account;
		$test_bt_account->login= 'unexisted-login';
	}
	else if ('exception'==$account)
	{
		global $test_bt_account;
		$test_bt_account->login= 'exceptioned-login';
	}
	else if ('limited_account'==$account)
	{
		global $test_bt_account;
		$test_bt_account->login= 'limited-account';
	}

	upload($uploader);
}

function load_and_test_job_helper($fixed_cmd, $prefix, $argv)
{
	require_once '../assets/helpers/db.php';
	require_once '../assets/helpers/job.php';

	require_once 'tests/test_job_helper.php';

	test_job_helper($fixed_cmd, $prefix, $argv);
}

function load_and_test_bt($fixed_cmd, $prefix, $argv)
{
	require_once '../assets/libs/upload2bt/logger.php';
	require_once '../assets/helpers/client.rest.php';
	require_once '../assets/libs/upload2bt/client.bt.php';

	require_once 'tests/logger.php';
	require_once 'tests/logged_rest.php';
	require_once 'tests/tests_bt.php';
	require_once 'tests/cleaner.php';

	bt_tests($fixed_cmd, $prefix, $argv);
}

function load_and_test_uploader($fixed_cmd, $prefix, $argv)
{
	global $debtorName_beauty_prefixes;
	require_once '../assets/libs/upload2bt/logger.php';
	require_once '../assets/helpers/client.rest.php';
	require_once '../assets/libs/upload2bt/client.bt.php';
	require_once '../assets/actions/backend/constants/debtorName.etalon.php';
	require_once '../assets/actions/backend/constants/alib_constants.php';
	require_once '../assets/libs/upload2bt/uploader.bt.php';

	require_once 'tests/logger.php';
	require_once 'tests/logged_rest.php';
	require_once 'tests/tests_uploader.php';
	require_once 'tests/cleaner.php';

	uploader_tests($fixed_cmd, $prefix, $argv);
}

function load_and_test_decode($fixed_cmd, $prefix, $argv)
{
	require_once 'tests/tests_decode.php';
	tests_decode($fixed_cmd, $prefix, $argv);
}

function load_and_test_verify($fixed_cmd, $prefix, $argv)
{
	require_once 'tests/tests_verify.php';
	tests_verify($fixed_cmd, $prefix, $argv);
}

function load_and_test_push($fixed_cmd, $prefix, $argv)
{
	require_once 'tests/tests_push.php';
	tests_push_commands($fixed_cmd, $prefix, $argv);
}

function load_and_test_using($fixed_cmd, $prefix, $argv)
{
	require_once 'tests/tests_using.php';
	tests_using_commands($fixed_cmd, $prefix, $argv);
}

function load_and_test_request($fixed_cmd, $prefix, $argv)
{
	require_once 'tests/tests_request.php';
	tests_request_commands($fixed_cmd, $prefix, $argv);
}

function load_and_test_efrsb_sync($fixed_cmd, $prefix, $argv)
{
	require_once 'tests/tests_efrsb_sync.php';
	test_efrsb_sync_commands($fixed_cmd, $prefix, $argv);
}

function test_log_type_sql()
{
	require_once '../assets/libs/log/access_log_event_types.php';

	echo "-- ВНИМАНИЕ! ATTENTION! AHTUNG!\r\n";
	echo "-- данный файл создаётся в ходе выполнения тестов\r\n";
	echo "-- Он НЕ должен редактироваться вручную!\r\n";
	echo "\r\n";
	echo "insert into log_type(id_Log_type,Name) values\r\n";

	$ids= array();

	global $log_event_type;
	$i= 0;
	foreach ($log_event_type as $txt => $id)
	{
		if ($id>0)
		{
			if (!isset($ids[$id]))
			{
				$ids[$id]= $id;
			}
			else
			{
				echo "идентификатор $id уже был!\r\n";
			}
			echo (0==$i) ? '  ' : ', ';
			echo "( $id, '$txt' )\r\n";
			$i++;
		}
	}
	echo ";\r\n";
}

function test_h_MessageType_js()
{
	require_once '../assets/libs/message/MessageType.php';

	echo "define(function ()\r\n{\r\n";
	echo " // ВНИМАНИЕ! ATTENTION! AHTUNG!\r\n";
	echo " // данный файл создаётся в ходе выполнения тестов\r\n";
	echo " // Он НЕ должен редактироваться вручную!\r\n\r\n";

	echo " // элементы массивов: [db_value, Readable, Readable_short_about]\r\n\r\n";

	echo "	var MessageInfo_MessageType_desciptions=\r\n	[\r\n";

	$count= 0;
	foreach ($MessageInfo_MessageType_desciptions as $md)
	{
		$d= (object)$md;
		echo (0==$count) ? "	 [" : "	,[";
		echo '"'.$d->db_value.'",';
		echo '"'.$d->Readable.'",';
		echo '"'.$d->Readable_short_about.'",';
		echo "]\r\n";
		$count++;
	}

	echo "	];\r\n";

	echo "	return MessageInfo_MessageType_desciptions;\r\n";

	echo "});\r\n";
}

function test_delete_old_PData($static_date_time)
{
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	require_once '../assets/libs/truncate_PData.php';
	$result= delete_old_PData();
	echo "results:\r\n$result";
}

function test_get_gc_events($static_date_time)
{
	global $current_date_time;
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$static_date_time);

	require_once '../assets/libs/google_calendar/sync_gc_event.php';
	$result= sync_events();
	echo "results:\r\n$result";
}

function test_create_gc_event($static_date_time, $id_GoogleCalendar)
{
	global $current_date_time, $google_calendar_options, $google_calendar_secret;
	require_once '../assets/helpers/db.php';
	require_once '../assets/helpers/google_calendar_api.php';

	$txt_query= "select refresh_token from GoogleCalendar where id_GoogleCalendar=?";
	$row = execute_query($txt_query,array('i',$id_GoogleCalendar));
	$refresh_token = $row[0]->refresh_token;

	$capi = new GoogleCalendarApi();
	$new_access_token = $capi->RefreshAccessToken($google_calendar_options->CLIENT_ID, $google_calendar_secret, $refresh_token);
	
	$event_id = $capi->CreateTestCalendarEvent('primary','test title', 'test description', '2021-11-29', $new_access_token['access_token']);
	if(isset($event_id)) {
		echo "created a test event \r\n";
	}
	test_get_gc_events($static_date_time);
	$deleted_http_code = $capi->DeleteCalendarEvent('primary', $event_id, $new_access_token['access_token']);
	if($deleted_http_code == 204) {
		echo "\r\nevent has been removed \r\n";
	}
}

function test_job_parts($job_parts)
{
	foreach ($job_parts as $p)
	{
		echo $p['title']."\r\n";
		$exec= $p['exec'];
		if (!is_callable($exec))
			echo "exec is not callable!!!!!!! \r\n";
	}
}

function test_minutely_job_parts()
{
	require_once '../assets/libs/job_parts/minutely_job_parts.php';
	test_job_parts($minutely_job_parts);
}

function test_hourly_job_parts()
{
	require_once '../assets/libs/job_parts/hourly_job_parts.php';
	test_job_parts($hourly_job_parts);
}

function test_nightly_job_parts()
{
	require_once '../assets/libs/job_parts/nightly_job_parts.php';
	test_job_parts($nightly_job_parts);
}

function test_fill_case_numbers()
{
	global $echo_errors, $do_not_write_errors_to_log;
	$echo_errors= true;
	$do_not_write_errors_to_log= true;
	require_once '../assets/libs/starts/update_starts_cb_fields.php';
	$result= update_starts_cb_fields(/*$active_started_last_30_days=*/false);
	echo "results:\r\n$result";
}

function test_update_procedure_start_efrsb_fields()
{
	global $echo_errors, $do_not_write_errors_to_log;
	$echo_errors= true;
	$do_not_write_errors_to_log= true;
	require_once '../assets/libs/starts/update_starts_efrsb_fields.php';
	$result= update_starts_efrsb_fields(100);
	echo "results:\r\n$result";
}

function test_upload_manager_document()
{
	require_once '../assets/helpers/log.php';
	require_once '../assets/helpers/db.php';

	global $argv;
	$id_Manager= $argv[2];
	$filename= $argv[3];
	$doctype= $argv[4];
	$filepath= $argv[5];

	$txt_query= "insert into ManagerDocument set id_Manager=?, FileName=?, DocumentType=?, Body=?";
	execute_query($txt_query,array('ssss',$id_Manager,$filename,$doctype,file_get_contents($filepath)));
}

function test_archivate_procedures()
{
	global $echo_errors, $do_not_write_errors_to_log;
	$echo_errors= true;
	$do_not_write_errors_to_log= true;
	require_once '../assets/libs/archivate_procedures.php';
	$result= archivate_procedures();
	echo "results:\r\n";
	$txt_res= print_r($result,true);
	echo str_replace("\n","\r\n",str_replace("\r\n","\n",$txt_res));
}

function test_exposure_md5()
{
	require_once '../assets/libs/exposure/assetmd5.php';
	global $argv;
	$ao_txt= file_get_contents($argv[2]);
	$ao= json_decode($ao_txt);
	$md5= asset_md5($ao);
	echo $md5;
}

function test_exposure_text_for_md5()
{
	require_once '../assets/libs/exposure/assetmd5.php';
	global $argv;
	$ao_txt= file_get_contents($argv[2]);
	$ao= json_decode($ao_txt);
	$text_for_md5= asset_text_for_md5($ao);
	echo $text_for_md5;
}

global $current_date_time;
$current_date_time= date_create('2019-07-23');

global $prefixed_test_groups;
$prefixed_test_groups= array
(
	  (object)array('prefix'=>'bt.',         'tests_func'=>'load_and_test_bt')
	, (object)array('prefix'=>'uploader.',   'tests_func'=>'load_and_test_uploader')
	, (object)array('prefix'=>'decode.',     'tests_func'=>'load_and_test_decode')
	, (object)array('prefix'=>'verify.',     'tests_func'=>'load_and_test_verify')
	, (object)array('prefix'=>'job_helper.', 'tests_func'=>'load_and_test_job_helper')
	, (object)array('prefix'=>'push.',       'tests_func'=>'load_and_test_push')
	, (object)array('prefix'=>'using.',      'tests_func'=>'load_and_test_using')
	, (object)array('prefix'=>'request.',    'tests_func'=>'load_and_test_request')
	, (object)array('prefix'=>'efrsb.',      'tests_func'=>'load_and_test_efrsb_sync')
);

function Process_command_line_arguments()
{
	global $argv, $prefixed_test_groups;
	$cmd= $argv[1];

	$static_date_time = !isset($argv[2]) ? '' : $argv[2];

	foreach ($prefixed_test_groups as $prefixed_test_group)
	{
		$prefix= $prefixed_test_group->prefix;
		if (0===strpos($cmd,$prefix))
		{
			$fixed_test_name= substr($cmd,strlen($prefix));
			$tests_func= $prefixed_test_group->tests_func;
			$tests_func($fixed_test_name,$prefix,$argv);
			return;
		}
	}

	switch ($cmd)
	{
		case 'parse_Leaks': test_parse_Leaks(); break;
		case 'upload_from_db': test_upload_from_db(count($argv)<3?'':$argv[2]); break;
		case 'log_type_sql': test_log_type_sql(); break;
		case 'h_MessageType_js': test_h_MessageType_js(); break;

		case 'get_gc_events': test_get_gc_events($static_date_time); break;

		case 'delete_old_PData': test_delete_old_PData($static_date_time); break;

		case 'minulety_job_parts': test_minutely_job_parts(); break;
		case 'hourly_job_parts': test_hourly_job_parts(); break;
		case 'nightly_job_parts': test_nightly_job_parts(); break;

		case 'fill_case_numbers': test_fill_case_numbers(); break;

		case 'update_procedure_start_efrsb_fields': test_update_procedure_start_efrsb_fields(); break;

		case 'upload_manager_document': test_upload_manager_document(); break;

		case 'create_gc_test_event': test_create_gc_event($static_date_time, $argv[3]); break;

		case 'archivate_procedures': test_archivate_procedures(); break;
		case 'exposure_md5': test_exposure_md5(); break;
		case 'exposure_text_for_md5': test_exposure_text_for_md5(); break;

		default: echo "unknown test command $cmd\r\n";
	}
}

try
{
	Process_command_line_arguments();
}
catch (Exception $ex)
{
	$exception_class= get_class($ex);
	$exception_Message= $ex->getMessage();
	echo "\r\nUnhandled exception occurred: $exception_class - $exception_Message\r\n";
	echo 'catched Exception:';
	print_r($ex);
}