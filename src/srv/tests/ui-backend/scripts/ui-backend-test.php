<? 

mb_internal_encoding("utf-8");

global $url;

function get_name_value($arg)
{
	$pos= strpos($arg,'=');
	return (object)array('name_part'=>substr($arg,0,$pos),'value'=>substr($arg,$pos+1));
}

function get_responce_body($curl_response)
{
	$pos= strpos($curl_response,"\r\n\r\n");

	$res= substr($curl_response,$pos+4);

	$res= str_replace("\r\n","\n",$res);
	$res= str_replace("\n","\r\n",$res);

	return $res;
}

function get_PHPSESSID($curl_response)
{
	$prefix= 'PHPSESSID=';
	$pos= strpos($curl_response,$prefix);
	if (-1==$pos)
	{
		return null;
	}
	else
	{
		$pos1= $pos+strlen($prefix);
		$pos2= strpos($curl_response,';',$pos1);
		return substr($curl_response,$pos1,$pos2-$pos1);
	}
}

function post_prepare($post)
{
	$post=str_replace("\r\n","",$post);
	$post=str_replace("\n","",$post);
	return $post;
}

function ProcessArguments()
{
	global $url, $argv, $postfields, $session_file_to_write_to, $session_file_to_read_from, $file_path;
	$count= count($argv);
	for ($i= 1; $i<$count; $i++)
	{
		$nv= get_name_value($argv[$i]);
		switch ($nv->name_part)
		{
			case '--url': $url= str_replace(' ','%20',$nv->value); break;
			case '--post': $postfields= $nv->value; break;
			case '--post-from-file': $postfields= post_prepare(file_get_contents($nv->value)); break;
			case '--session-file-to-write-to': $session_file_to_write_to= $nv->value; break;
			case '--session-file-to-read-from': $session_file_to_read_from= $nv->value; break;
		}
	}
}

function DoRequest()
{
	global $url, $postfields, $session_file_to_write_to, $session_file_to_read_from, $file_path;

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, 1);
	curl_setopt($curl, CURLOPT_HTTPHEADER,array("Expect:"));

	if (isset($postfields))
	{
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
	}

	if (isset($session_file_to_read_from))
	{
		$PHPSESSID= file_get_contents($session_file_to_read_from);
		curl_setopt($curl, CURLOPT_COOKIE, "PHPSESSID=$PHPSESSID;");
	}

	$curl_response= curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (isset($session_file_to_write_to))
		file_put_contents($session_file_to_write_to,get_PHPSESSID($curl_response));

	if (200==$httpcode)
	{
		echo get_responce_body($curl_response);
	}
	else
	{
		echo "HTTP CODE=$httpcode";
		switch ($httpcode)
		{
			case 400: echo " Bad Request"; break;
			case 401: echo " Unauthorized"; break;
			case 404: echo " Not Found"; break;
		}
	}
}

ProcessArguments();
DoRequest();
