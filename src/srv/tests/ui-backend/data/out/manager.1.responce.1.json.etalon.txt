<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Ответ на запрос кандидатуры арбитражного управляющего</title>
	<xml xmlns:w="urn:schemas-microsoft-com:office:word">
	<w:WordDocument>
					<w:DocumentProtection>ReadOnly</w:DocumentProtection>
		
		<w:View>Print</w:View>
		<w:Zoom>100</w:Zoom>
		<w:DoNotOptimizeForBrowser/>
	</w:WordDocument>
</xml>	<style>
		.plug {
			width: auto !important;
		}
		@page WordPageA4 {
			size:21cm 29.7cmt;  /* A4 */
			mso-page-orientation: portrait;
			margin:42.5pt 2.0cm 3.0cm 2.0cm;
			mso-header-margin:35.4pt;
			mso-footer-margin:35.4pt;
			mso-paper-source:0;
			mso-footer:page-footer;
		}
	
		.mrequest_response {
			width: 21cm;
		}
		.mrequest_response div.WordPageA4 {
			page:WordPageA4;
			margin:42.5pt 2.0cm 3.0cm 2.0cm;
		}
		.mrequest_response table{
			width: 100%;
			border-collapse: collapse; 
			border: none;
			table-layout: auto;
		}
		.mrequest_response table td { width: 50%; vertical-align: top; }
		.mrequest_response .underline {
			text-decoration: underline;
		}
		.mrequest_response .mb-15 {
			margin-bottom: 15px;
		}
		.mrequest_response .header .right_part span { display: inline-block; width: 100%; }
		.mrequest_response .header .left_part .current_date {
			margin-bottom: 15px;
		}
		.mrequest_response .header .right_part .case_number {
			text-align: right;
		}
		.mrequest_response .header .right_part .case_number>span{
			width: auto;
			padding-bottom: 1px;
			margin-bottom: 2px;
		}
		.mrequest_response .header .right_part .court_name {
			padding-bottom: 2px;
			border-bottom: 3px solid black;
		}
		.mrequest_response .main { padding: 15px 0; }
		.mrequest_response .main p { margin: 0;  text-indent: 37.8px; text-align: justify;}
		.mrequest_response .footer .right_part {
			vertical-align:bottom;
			text-align: center;
		}
	</style>
</head>
<body class="mrequest_response">
	<div class="WordPageA4">
		<table class="header">
			<tr>
				<td class="left_part">
					<div class="current_date hide-for-tests">
						«19» января 2021г.
					</div>
					<div> № 
						<span class="underline"> 
							1													</span>
											</div>
				</td>
				<td class="right_part">
					<div class="mb-15 court_info" style="text-align:center">
						<div class="court_name"><b>
							Арбитражный суд Удмуртской Республики													</b></div>
						<div><b>
							г. Ижевск, ул. Ломоносова 5							 
						</b></div>
					</div>
					<div class="mb-15">
						<div><b> Заявитель:
							ООО УК "СВЕТЛЫЙ ГОРОД"													</b></div>
						<div>
							<span class="plug">________________________________________</span> </br><span class="plug">________________________________________</span>													</div>
					</div>
					<div class="mb-15">
						<div><b>
							Должник:
							�													</b></div>
						<div>
							<span class="plug">________________________________________</span> </br><span class="plug">________________________________________</span>													</div>
					</div>
					<div class="case_number">
						<span class="underline"><b> Дело №
							<span class="plug">____________</span>							 
						</b></span>
					</div>
				</td>
			</tr>
		</table>
		<div class="main">
			<p>
				В соответствии с определением
				Арбитражного суда Удмуртской Республики				
				от 
				15.01.2021				 г.

				по делу №&nbsp;
				<span class="plug">____________</span>				
				и со ст. 45 ФЗ «О несостоятельности (банкротстве)» сообщаем информацию о соответствии кандидатуры
				<b>
					Иванова Ивана Ивановича					
					(ИНН
					364484332544									
					, регистрационный номер в сводном государственном реестре арбитражных управляющих
					123					
					, адрес для направления корреспонденции: 
					<span class="plug">____________________________________________________________</span>					)
				</b>
				требованиям ст. 20 ФЗ «О несостоятельности (банкротстве)».
			</p>
			<p>
				Согласно Постановлению Пленума ВАС РФ от 23 июля 2009 г. N 60 О некоторых вопросах,
				связанных с принятием Федерального закона от 30.12.2008 № 296-ФЗ «О внесении изменений в 
				Федеральный закон «О несостоятельности (банкротстве)» саморегулируемая организация не представляет
				суду документы, подтверждающие соответствие кандидатуры арбитражного управляющего установленным требованиям.
			</p>
			<p><b>Саморегулируемая организация несет ответственность за предоставление недостоверных сведений об арбитражных управляющих.</b></p>
			<p>
				Сведениями о наличии обстовятельств, препятствующих утверждению, предусмотренных ч.2 ст.20.2
				Федерального закона № 127-ФЗ от 26.10.2002 г., не располагаем.
			</p>
			<p>
				<span>Пленума Высшего Арбитражного Суда Российской Федерации от 17.02.2011 №12
				«О некоторых вопросах применения Арбитражного процессуального кодекса Российской Федерации в 
				редакции федерального закона от 27.07.2010 №228-ФЗ «О внесении изменений в Арбитражный 
				процессуальный кодекс Российской Федерации» разъяснено, что если АПК РФ предусматривается
				направление лицам, участвующим в деле, копии судебного акта, совершения отдельного процессуального
				действия и суд располагает сведениями об адресе электронной почты, то копия судебного акта может 
				направляться этому лицу по электронной почте. На основании вышеизложенного </span>
				<i> 
					Национальный Союз профессионалов антикризисного управления					 

					просит Вас направлять копии судебных актов по электронной почте 
					<b class="underline">
						s@s.ru						 
					</b>
					<b>
											</b>
				</i>
			</p>
		</div>
		<div class="attachment">
			<b>Приложение (только в адрес арбитражного суда):</b>
			<ol>
				<li>Заявление о согласии быть утвержденным в деле о банкротстве на 1 листе.</li>
				<li>Свидетельство об аккредитации на 1 листе.</li>
				<li>Свидетельство о повышении квалификации на 1 листе.</li>
				<li>Протокол Заседания комитета №  по отбору кандидатуры на 2 листах.</li>
				<li>Выписка из Протокола заседания Совета на 1 листе.</li>
				<li>Доказательства направления информации заявителю и должнику на 1 листе.</li>
			</ol>
		</div>
		<table class="footer">
			<td class="left_part">
			<b>Президент</b>
			<div><b>
					Национального Союза профессионалов антикризисного управления									</b></div>
			</td>
			<td class="right_part">
				<span class="sro_president_name"><b>
					Макрошин Леонид Богданович									</b></span>
			</td>
		</table>
	</div>
</body>
</html>