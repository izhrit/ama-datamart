﻿select 
	v.id_AwardVote
	,v.inn
	,v.VoteTime
	,v.BulletinText
	,v.firstName
	,v.lastName
	,v.middleName
	,v.SRO
	,v.email
	,length(v.BulletinSignature) Signature_length

	,n.inn nominee_inn
	,n.firstName nominee_firstName
	,n.lastName nominee_lastName
	,n.middleName nominee_middleName
	,n.SRO nominee_SRO
	,n.URL nominee_URL
from AwardVote v
inner join AwardNominee n on v.id_AwardNominee=n.id_AwardNominee
where v.VoteTime > '2020-09-01'
\G