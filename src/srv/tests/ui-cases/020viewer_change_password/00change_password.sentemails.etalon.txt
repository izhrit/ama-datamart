*************************** 1. row ***************************
  id_SentEmail: 1
RecipientEmail: e@r.p
   RecipientId: 1
 RecipientType: c
     EmailType: n
   ExtraParams: {
  "Body": "приглашаю на заседание КК",
  "Subject": "приглашение на КК"
}
*************************** 2. row ***************************
  id_SentEmail: 2
RecipientEmail: test@test.ru
   RecipientId: 1
 RecipientType: m
     EmailType: y
   ExtraParams: {
  "Subject": "Уведомление о запросах",
  "Body": "Тестовое сообщение",
  "Receiver": "Иван"
}
*************************** 3. row ***************************
  id_SentEmail: 3
RecipientEmail: test2@test.ru
   RecipientId: 4
 RecipientType: m
     EmailType: y
   ExtraParams: {
  "Subject": "Уведомление о запросах",
  "Body": "Тестовое сообщение",
  "Receiver": "Иван"
}
*************************** 4. row ***************************
  id_SentEmail: 4
RecipientEmail: a@o.u
   RecipientId: 1
 RecipientType: v
     EmailType: m
   ExtraParams: {
	"Subject": "Учётные данные на Витрине данных ПАУ",
	"Body": "Здравствуйте, Сергеева Светлана Михайловна (Банк Возрождение).

Для использования Витрины данных ПАУ ( http:\/\/local.test\/dm\/ui.php )

используйте пароль new_test_pass

С уважением, компания
\"Русские информационные технологии\"
телефон\/факс: (3412) 57-04-02
эл.адрес: info@russianit.ru
сайт www.russianit.ru
",
	"Receiver": "Сергеева Светлана Михайловна (Банк Возрождение)"
}
