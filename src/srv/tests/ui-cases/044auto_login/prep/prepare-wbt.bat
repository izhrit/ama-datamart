copy %~dp0\tpl_newline.txt /a %~dp0\in.lib.txt /b /y

call :out-autologin-lines auth-customer-1-20200402T000101
call :out-autologin-lines auth-customer-2222-20200502T000101
call :out-autologin-lines auth-manager-1-20200401T000102
call :out-autologin-lines auth-manager-2-20200501T000102
call :out-autologin-lines auth-manager-3-20200301T000102

call :out-autologin-lines auth-customer-unexisted-20200502T000101
call :out-autologin-lines auth-manager-unexisted-20200501T000102

call :out-autologin-lines auth-customer-1-bad-token-20200402T000101
call :out-autologin-lines auth-manager-3-bad-token-20200301T000102

call :out-autologin-lines auth-manager-new-20200301T000102
call :out-autologin-lines auth-customer-new-20200502T000101
call :out-autologin-lines auth-manager-new2-20200301T000102

exit /B

:out-autologin-lines
echo start_store_lines_as %1 >> %~dp0\in.lib.txt
copy %~dp0\in.lib.txt+%~dp0\tpl_goto_wait_prefix.txt /a %~dp0\in.lib.txt /b /y
copy %~dp0\in.lib.txt+%~dp0\..\..\..\..\..\clnt\test-client-ama\tests\prepare-auth\%1.etalon.txt /a %~dp0\in.lib.txt /b /y
copy %~dp0\in.lib.txt+%~dp0\tpl_goto_wait_postfix.txt /a %~dp0\in.lib.txt /b /y
echo stop_store_lines >> %~dp0\in.lib.txt
copy %~dp0\in.lib.txt+%~dp0\tpl_newline.txt /a %~dp0\in.lib.txt /b /y
exit /B
