include prep\in.lib.txt quiet

#####################################
# вход от контракта №1

wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

goto_wait "&start&use-test-time=2020-04-02T00:01:01"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-customer-1-20200402T000101
wait_click_text "Процедуры"
wait_text "Василёк"
shot_check_png ..\..\shots\00new_logged_customer.png

#####################################
# вход от АУ №1

goto_wait "&start&use-test-time=2020-04-01T00:01:02"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-manager-1-20200401T000102 
wait_click_text "Процедуры"
wait_text "КАРИНА"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_logged_manager.png

#####################################
# вход от контракта №2

goto_wait "&start&use-test-time=2020-05-02T00:01:01"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-customer-2222-20200502T000101 
wait_click_text "Процедуры"
wait_text "Ромашка"

#####################################
# вход от АУ №2

goto_wait "&start&use-test-time=2020-05-01T00:01:02"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-manager-2-20200501T000102
wait_click_text "Процедуры"
wait_text "Петров"
wait_text "Василёк"

#####################################
# вход от АУ №3

goto_wait "&start&use-test-time=2020-03-01T00:01:02"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-manager-3-20200301T000102
wait_click_text "Процедуры"
wait_text "Сидоров"
wait_text "Ромашка"

#####################################
# неверный аргумент auth..

goto_wait "&start&use-test-time&auth=bad-auth"
wait_text "can not decrypt auth"

#####################################
# токен через 16 минут..

goto_wait "&start&use-test-time=2020-03-01T00:17:02"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-manager-3-20200301T000102
wait_text "time is wrong!"

#####################################
# несуществующий контракт

goto_wait "&start&use-test-time=2020-05-02T00:01:01"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-customer-unexisted-20200502T000101 
wait_text "can not check admin license token with license server"

#####################################
# несуществующий АУ

goto_wait "&start&use-test-time=2020-05-01T00:01:01"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-manager-unexisted-20200501T000102 
wait_text "can not check manager license token with license server"

#####################################
# абонент с неправильным токеном

goto_wait "&start&use-test-time=2020-04-02T00:01:01"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-customer-1-bad-token-20200402T000101 
wait_text "can not check admin license token with license server"

#####################################
# АУ с неправильным токеном

goto_wait "&start&use-test-time=2020-03-01T00:01:01"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-manager-3-bad-token-20200301T000102 
wait_text "can not check manager license token with license server"

#####################################
# незарегистрированный на витрине Договор

goto_wait "&start&use-test-time=2020-05-02T00:01:01"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-customer-new-20200502T000101
wait_text "333"
wait_click_text "Процедуры"
wait_text "Нет записей для просмотра"

#####################################
# незарегистрированный на витрине АУ

goto_wait "&start&use-test-time=2020-03-01T00:01:02"
wait_text "Войти"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_viewer.png

play_stored_lines auth-manager-new-20200301T000102
wait_text "Николаев"
wait_click_text "Процедуры"
wait_text "Нет записей для просмотра"

#####################################
# незарегистрированный на витрине АУ2

play_stored_lines auth-manager-new2-20200301T000102
wait_text "Алексеев"
wait_click_text "Процедуры"
wait_text "Нет записей для просмотра"

exit