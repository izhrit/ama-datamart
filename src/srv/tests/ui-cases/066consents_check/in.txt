﻿execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
jw !!window.wbt

wait_click_text "Кабинет СРО АУ"

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетСРО"] ';
js wbt.SetModelFieldValue("Login", "s@s.ru");
js wbt.SetModelFieldValue("Password", "1");
js wbt.Model_selector_prefix= '';
wait_click_full_text "Войти"
wait_text_disappeared "ajax"
wait_click_text "Согласия"

wait_text "Показано согласий 2 из 2"
shot_check_png ..\..\shots\sro2_consents_initial.png

wait_click_text "Зарегистрировать согласие"
js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", 'Абайдуллин');
js wbt.SetModelFieldValue("Должник.Физ_лицо.Имя", 'Валерий');
js wbt.SetModelFieldValue("Должник.Физ_лицо.Отчество", 'Кимович');
js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Удмуртской');
js wbt.Model_selector_prefix= '.cpw-ama-datamart-consent  '
    js $('.select-au').select2('open');
    js wbt.SetModelFieldValue("АУ",  "Ивано");
js wbt.Model_selector_prefix= '';
wait_click_text "Свериться"
wait_text_disappeared "ajax"
wait_text "новая информация"
wait_click_text "OK"
wait_click_text "Сохранить"

wait_text "Показано согласий 3 из 3"

wait_click_text "Зарегистрировать согласие"
js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", 'Парамонов');
js wbt.SetModelFieldValue("Должник.Физ_лицо.Имя", 'Парамон');
js wbt.SetModelFieldValue("Должник.Физ_лицо.Отчество", 'Парамонович');
js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Москв');
js wbt.Model_selector_prefix= '.cpw-ama-datamart-consent  '
    js $('.select-au').select2('open');
    js wbt.SetModelFieldValue("АУ",  "Ивано");
js wbt.Model_selector_prefix= '';
wait_click_text "Только что"
wait_text_disappeared "ajax"
wait_click_text "Сохранить"

wait_text_disappeared "Загрузка"
wait_text "Показано согласий 4 из 4"

wait_click_text "Абайдуллин"
wait_click_text "Свериться"
wait_text_disappeared "ajax"
wait_text "новой информации"
wait_click_text "OK"
wait_click_text "Сохранить"

wait_text "Показано согласий 4 из 4"

exit