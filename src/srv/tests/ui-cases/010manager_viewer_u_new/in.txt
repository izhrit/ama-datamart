execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
jw !!window.wbt

wait_click_text "Кабинет АУ"

# shot_check_png ..\..\shots\00new_manager.png

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетАУ"] ';
js wbt.SetModelFieldValue("Login", "x@y.com");
js wbt.SetModelFieldValue("Password", "1");
wait_click_full_text "Войти"
wait_text_disappeared "Вызов ajax"
js wbt.Model_selector_prefix= '';

wait_click_text "Контакты"
wait_text_disappeared "Загрузка"
je $("td[title='Селянин И О'] ~ td > div.actions-button").click();
wait_click_text "Редактировать"
wait_text_disappeared "Редактировать"
wait_text "Доступ к информации о процедурах"

  js wbt.SetModelFieldValue("Email", "a@o.u");

wait_click_text "Сохранить настройки наблюдателя"
wait_text "Не удалось"
shot_check_png ..\..\shots\petrov_change_viewer_bad.png
wait_click_text "OK"

je $("td[title='Селянин И О'] ~ td > div.actions-button").click();
wait_click_text "Редактировать"
wait_text_disappeared "Редактировать"
wait_text "Доступ к информации о процедурах"
 
  js wbt.SetModelFieldValue("Email", "new@new.new");
  js wbt.SetModelFieldValue("Имя", "Не Селянин Игорь Олегович");
  js wbt.SetModelFieldValue("Доступ_к_процедурам", ["Новострой"]);

wait_click_text "Сохранить настройки наблюдателя"
wait_text_disappeared "Сохранение записи о наблюдателе на сервер"
wait_text_disappeared "Загрузка"
shot_check_png ..\..\shots\petrov_with_neselianon.png

je $("td[title='Не Селянин Игорь Олегович'] ~ td > div.actions-button").click();
wait_click_text "Редактировать"
wait_text_disappeared "Редактировать"
wait_text_disappeared "Получение данных наблюдателя с сервера"
wait_text "Доступ к информации о процедурах"
shot_check_png ..\..\shots\petrov_neselianin.png

  js wbt.SetModelFieldValue("Email", "a@o.u");

exit