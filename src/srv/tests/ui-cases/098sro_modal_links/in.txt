﻿execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
include ..\..\..\..\uc\forms\ama\datamart\common\court_decision\tests\cases\in.lib.txt quiet
include ..\..\..\..\uc\forms\ama\datamart\common\efrsb_report\tests\cases\in.lib.txt quiet
jw !!window.wbt
je app.cpw_Now= function(){return new Date(2020, 4, 24, 17, 36, 0, 0 );}
wait_click_text "Кабинет СРО АУ"
wait_text "Обращайтесь за паролем"

shot_check_png ..\..\shots\00new_sro.png

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетСРО"] ';
js wbt.SetModelFieldValue("Login", "s@s.ru");
js wbt.SetModelFieldValue("Password", "1");
shot_check_png ..\..\shots\00test.png
wait_click_full_text "Войти"
wait_text_disappeared "ajax"
js wbt.Model_selector_prefix= '';

wait_click_text "Согласия"
wait_text_disappeared "ajax"

wait_click_text "Зарегистрировать"
wait_text_disappeared "ajax"

# Начало: Создание согласия без данных в modal-links

wait_text "Регистрация согласия на предоставление кандидатуры АУ"
js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", "АУБезСсылок-модалок");
js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Москвы');

js wbt.Model_selector_prefix= '.cpw-ama-datamart-consent  '
	js $('.select-au').select2('open');
	js wbt.SetModelFieldValue("АУ",  "Иванов");
js wbt.Model_selector_prefix= '';

wait_click_full_text "Сохранить"
wait_text_disappeared "Регистрация согласия на предоставление кандидатуры АУ"

# Конец: Создание согласия без данных в modal-links

wait_click_text "Зарегистрировать"
wait_text_disappeared "ajax"


# Начало: Создание согласия со всеми данными в modal-links (кроме дополнительной информации)

wait_text "Регистрация согласия на предоставление кандидатуры АУ"
js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", "АУССсылками-модалкамиБезДополнительнойИнформации");
js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Москвы');
js 

js $(".cpw-ama-datamart-consent .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
play_stored_lines ama_datamart_court_decision_fields_1
js wbt.SetModelFieldValue("Дополнительная_информация", "");
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

js $(".cpw-ama-datamart-consent .appointment").click();
wait_text "Судебный акт о назначении кандидатуры АУ"
play_stored_lines ama_datamart_court_decision_fields_1
js wbt.SetModelFieldValue("Дополнительная_информация", "");
wait_click_text "Сохранить информацию о назначении"
wait_text_disappeared "Судебный акт о назначении кандидатуры АУ"

js $(".cpw-ama-datamart-consent .efrsb_report").click();
wait_text "Сообщение на ЕФРСБ о введении процедуры"
play_stored_lines ama_datamart_efrsb_report_fields_1
js wbt.SetModelFieldValue("Дополнительная_информация", "");
wait_click_text "Сохранить информацию о сообщении"
wait_text_disappeared "Сообщение на ЕФРСБ о введении процедуры"

js wbt.Model_selector_prefix= '.cpw-ama-datamart-consent  '
	js $('.select-au').select2('open');
	js wbt.SetModelFieldValue("АУ",  "Иванов");
js wbt.Model_selector_prefix= '';

wait_click_full_text "Сохранить"
wait_text_disappeared "Регистрация согласия на предоставление кандидатуры АУ"

# Конец: Создание согласия со всеми данными в modal-links (кроме дополнительной информации)


wait_click_text "Зарегистрировать"
wait_text_disappeared "ajax"


# Начало: Создание согласия со всеми данными в modal-links

wait_text "Регистрация согласия на предоставление кандидатуры АУ"
js wbt.SetModelFieldValue("Должник.Физ_лицо.Фамилия", "АУССсылками-модалкамиСДополнительнойИнформацией");
js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Москвы');
js 

js $(".cpw-ama-datamart-consent .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
play_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

js $(".cpw-ama-datamart-consent .appointment").click();
wait_text "Судебный акт о назначении кандидатуры АУ"
play_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о назначении"
wait_text_disappeared "Судебный акт о назначении кандидатуры АУ"

js $(".cpw-ama-datamart-consent .efrsb_report").click();
wait_text "Сообщение на ЕФРСБ о введении процедуры"
play_stored_lines ama_datamart_efrsb_report_fields_1
wait_click_text "Сохранить информацию о сообщении"
wait_text_disappeared "Сообщение на ЕФРСБ о введении процедуры"

js wbt.Model_selector_prefix= '.cpw-ama-datamart-consent  '
	js $('.select-au').select2('open');
	js wbt.SetModelFieldValue("АУ",  "Иванов");
js wbt.Model_selector_prefix= '';

wait_click_full_text "Сохранить"
wait_text_disappeared "Регистрация согласия на предоставление кандидатуры АУ"

# Конец: Создание согласия со всеми данными в modal-links


je $(".profile-dropdown").click();
wait_click_text "Выйти"
wait_text_disappeared "Вызов ajax"


wait_click_text "Кабинет абонента ИС ПАУ"

shot_check_png ..\..\shots\00new_customer.png

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетАбонента"] ';
js wbt.SetModelFieldValue("Login", "1");
js wbt.SetModelFieldValue("Password", "1");
js wbt.Model_selector_prefix= '';
wait_click_full_text "Войти"
wait_text_disappeared "ajax"

wait_click_text "Назначения"

wait_text "Показано запросов 2 из 2"
shot_check_png ..\..\shots\customer1_requests_initial.png

wait_click_text "Согласия"
wait_text_disappeared "ajax"

wait_click_text "АУБезСсылок-модалок"
wait_text "Согласие АУ на предоставление кандидатуры"
wait_text "судом не зарегистирован"
wait_text "не зарегистировано"
shot_check_png ..\..\shots\customer1_start_made_by_sro_without_modal_links.png
wait_click_full_text "Закрыть"
wait_text_disappeared "Согласие АУ на предоставление кандидатуры"

wait_click_text "АУССсылками-модалкамиБезДополнительнойИнформации"
wait_text "Согласие АУ на предоставление кандидатуры"
wait_text "судебным актом от 01.05.2021"
wait_text "№6632452 от 11.05.2021"
shot_check_png ..\..\shots\customer1_start_made_by_sro_with_modal_links_without_add_info.png
wait_click_full_text "Закрыть"
wait_text_disappeared "Согласие АУ на предоставление кандидатуры"

wait_click_text "АУССсылками-модалкамиСДополнительнойИнформацией"
wait_text "Согласие АУ на предоставление кандидатуры"

wait_text "судебным актом от 01.05.2021"
wait_text "дополнительная информация"
js $(".cpw-ama-datamart-start .decision-add-info").click();
wait_text "Дополнительная информация"
js wbt.CheckModelFieldValue("Дополнительная_информация", "Сомнительное дело");
js $('[aria-describedby="cpw-form-ama-consent-court-decision-request-au-add-info"] .ui-dialog-buttonpane .ui-button-text:contains("Закрыть")').click()
wait_text_disappeared "Дополнительная информация"

js $(".cpw-ama-datamart-start .appointment-add-info").click();
wait_text "Дополнительная информация"
js wbt.CheckModelFieldValue("Дополнительная_информация", "Сомнительное дело");
js $('[aria-describedby="cpw-form-ama-consent-court-decision-appoint-au-add-info"] .ui-dialog-buttonpane .ui-button-text:contains("Закрыть")').click()
wait_text_disappeared "Дополнительная информация"

wait_text "№6632452 от 11.05.2021"
wait_text "дополнительная информация"
js $(".cpw-ama-datamart-start .efrsb_report-add-info").click();
wait_text "Дополнительная информация"
js wbt.CheckModelFieldValue("Дополнительная_информация", "Сомнительное дело");
js $('[aria-describedby="cpw-form-ama-consent-efrsb-report-add-info"] .ui-dialog-buttonpane .ui-button-text:contains("Закрыть")').click()
wait_text_disappeared "Дополнительная информация"

shot_check_png ..\..\shots\customer1_start_made_by_sro_with_modal_links_add_info.png
wait_click_full_text "Закрыть"
wait_text_disappeared "Согласие АУ на предоставление кандидатуры"

exit