﻿execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
jw !!window.wbt

# ------------------------------------ Как АУ
wait_click_text "Кабинет АУ"

shot_check_png ..\..\shots\00new_manager_ev.png

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетАУ"] ';
js wbt.SetModelFieldValue("Login", "x@y.com");
js wbt.SetModelFieldValue("Password", "1");
wait_click_full_text "Войти"
wait_click_text "Процедуры"
wait_text "КАРИНА"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_logged_manager.png

goto_wait "?enable_viewers2"
execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
jw !!window.wbt

wait_text_disappeared "Вызов ajax"
wait_click_text "Процедуры"
wait_text "КАРИНА"
shot_check_png ..\..\shots\00new_logged_manager.png

je $(".profile-dropdown").click();
wait_click_text "Выйти"
wait_text_disappeared "Вызов ajax"

# ------------------------------------ Как Клиент
wait_click_text "Кабинет абонента ИС ПАУ"
wait_text "договор"

shot_check_png ..\..\shots\00new_customer.png

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетАбонента"] ';
js wbt.SetModelFieldValue("Login", "1");
js wbt.SetModelFieldValue("Password", "1");

wait_click_full_text "Войти"
wait_text_disappeared "Вызов ajax"
wait_click_text "Процедуры"
wait_text "Василёк"
shot_check_png ..\..\shots\00new_logged_customer.png

goto_wait "?enable_viewers2"
execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
jw !!window.wbt

wait_text_disappeared "Вызов ajax"
wait_click_text "Процедуры"
wait_text "Василёк"
shot_check_png ..\..\shots\00new_logged_customer.png

je $(".profile-dropdown").click();
wait_click_text "Выйти"
wait_text_disappeared "Вызов ajax"

# ------------------------------------ Как Наблюдатель
wait_click_text "Витрина данных"

shot_check_png ..\..\shots\00new_viewer.png

js wbt.Model_selector_prefix= '[data-fc-selector="Витрина"] ';
js wbt.SetModelFieldValue("Login", "a@o.u");
js wbt.SetModelFieldValue("Password", "1");
wait_click_full_text "Войти"
wait_text_disappeared "Вызов ajax"
wait_click_text "Процедуры"
wait_text "Показано процедур 5 из 5"
shot_check_png ..\..\shots\00new_logged_viewer_new.png

goto_wait "?enable_viewers2"
execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
jw !!window.wbt

wait_text_disappeared "Вызов ajax"
wait_click_text "Процедуры"
wait_text "Показано процедур 5 из 5"
shot_check_png ..\..\shots\00new_logged_viewer_new.png

je $(".profile-dropdown").click();
wait_click_text "Выйти"

exit