execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
jw !!window.wbt

wait_click_text "Витрина данных"

je $('button.login').focus();
shot_check_png ..\..\shots\00new_viewer.png

js wbt.Model_selector_prefix= '[data-fc-selector="Витрина"] ';
js wbt.SetModelFieldValue("Login", "a@o.u");
js wbt.SetModelFieldValue("Password", "1");
wait_click_full_text "Войти"
js wbt.Model_selector_prefix= '';

wait_text_disappeared "Вызов ajax"
wait_click_text "Процедуры"
wait_text "Троцкий"

wait_click_text "Сергеева"
wait_click_text "Профиль"
wait_text_disappeared "Уведомления"
wait_text "Параметры пользователя витрины"
shot_check_png ..\..\shots\viewer_profile.png
js wbt.SetModelFieldValue("Имя", "Канцельгоген");
js wbt.SetModelFieldValue("Email", "kk@ee.uu");
wait_click_text "Сохранить свойства пользователя витрины"
wait_text "Не удалось сохранить"
click_text "OK"
wait_text_disappeared "Не удалось сохранить"

exit