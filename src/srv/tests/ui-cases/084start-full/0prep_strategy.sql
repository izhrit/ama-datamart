set names utf8;

insert into efrsb_sro set
        INN= "3666101342"
,       Name= 'Союз «Саморегулируемая организация арбитражных управляющих "Стратегия"'
,       OGRN= "1023601559035"
,     RegNum= "0015"
,   Revision= "163"
, ShortTitle= 'Союз "СРО АУ "Стратегия"'
,      Title= 'Союз "Саморегулируемая организация арбитражных управляющих "Стратегия"'
,   UrAdress= "123308, г Москва, г. Москва, проспект Маршала Жукова, д.6, стр.1"
,   SROEmail= "sro@strategy.ru"
,SROPassword= "ss"
;
set @id_SRO= last_insert_id();

insert into efrsb_manager set
       ArbitrManagerID= 20377
,CorrespondenceAddress= 'Москва, пр-кт Маршала Жукова, д. 6, стр.1'
,            FirstName= 'Надежда'
,                  INN= '681801451967'
,             LastName= 'Лагода'
,           MiddleName= 'Серафимовна'
,               RegNum= '15395'
,             Revision= '43548'
,           SRORegDate= '2020-09-16 00:00:00'
,            SRORegNum= '0015'
;

insert into Manager set
  firstName='Надежда'
, lastName='Лагода'
, middleName='Серафимовна'
, efrsbNumber='15395'
, ArbitrManagerID= 20377
, INN='681801451967'
, id_SRO=@id_SRO
, ManagerEmail="nadezhda@lagoda.rf"
, Password='nl'
;
set @id_Manager= last_insert_id();

insert into Court set
       Name= 'Арбитражный суд Ставропольского края'
,CasebookTag= 'STAVROPOL'
, SearchName= 'Ставропольского'
,  ShortName= 'АС Ставропольского кр.'
,    Address= '355000, Россия, г. Ставрополь, ул. Мира д. 458 Б'
;