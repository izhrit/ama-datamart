﻿execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
include ..\..\..\..\uc\forms\ama\datamart\common\start\tests\cases\in.lib.txt quiet
jw !!window.wbt

wait_click_text "Кабинет АУ"

shot_check_png ..\..\shots\00new_manager_ev.png

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетАУ"] ';
js wbt.SetModelFieldValue("Login", "x@y.com");
js wbt.SetModelFieldValue("Password", "1");
js wbt.Model_selector_prefix= '';
wait_click_full_text "Войти"
wait_click_text "Процедуры"
wait_text "КАРИНА"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_logged_manager.png

wait_click_text "Назначения"
wait_click_text "Согласия"

wait_text "Показано согласий 2 из 2"
shot_check_png ..\..\shots\manager1_starts_initial.png

wait_click_text "Зарегистрировать"
wait_text "Регистрация"


# Начало заполнения формы создания согласия

play_stored_lines ama_datamart_start_fields_1

wait_click_text "Заявителем является должник (кликните, чтобы поменять)"
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-start-applicant"] '
	wait_text "Информация о заявителе"
	play_stored_lines ama_datamart_applicant_fields_1
	wait_click_text "Сохранить информацию о заявителе"
	wait_text "Тростников С.М."
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-start .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
js wbt.SetModelFieldValue("Ссылка", "123");
js wbt.SetModelFieldValue("Время.акта", "03.05.2021");
js wbt.SetModelFieldValue("Показывать_СРО", null);
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

js $(".cpw-ama-datamart-start .appointment").click();
wait_text "Судебный акт о назначении кандидатуры АУ"
play_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о назначении"
wait_text_disappeared "Судебный акт о назначении кандидатуры АУ"

js $(".cpw-ama-datamart-start .efrsb_report").click();
wait_text "Сообщение на ЕФРСБ о введении процедуры"
play_stored_lines ama_datamart_efrsb_report_fields_1
wait_click_text "Сохранить информацию о сообщении"
wait_text_disappeared "Сообщение на ЕФРСБ о введении процедуры"

# Конец заполнения формы создания согласия

wait_click_full_text "Сохранить"


wait_text "Показано согласий 3 из 3"

wait_click_text "Голохвастов"
wait_text "Согласие АУ на предоставление кандидатуры"

# Начало заполнения формы создания согласия

js $(".cpw-ama-datamart-start .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
js wbt.SetModelFieldValue("Ссылка", "123");
js wbt.SetModelFieldValue("Время.акта", "03.05.2021");
js wbt.SetModelFieldValue("Показывать_СРО", "checked");
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

js $(".cpw-ama-datamart-start .appointment").click();
wait_text "Судебный акт о назначении кандидатуры АУ"
play_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о назначении"
wait_text_disappeared "Судебный акт о назначении кандидатуры АУ"

js $(".cpw-ama-datamart-start .efrsb_report").click();
wait_text "Сообщение на ЕФРСБ о введении процедуры"
play_stored_lines ama_datamart_efrsb_report_fields_1
wait_click_text "Сохранить информацию о сообщении"
wait_text_disappeared "Сообщение на ЕФРСБ о введении процедуры"

# Конец заполнения формы создания согласия

wait_click_full_text "Сохранить"
wait_text_disappeared ""Согласие АУ на предоставление кандидатуры"


wait_text "Показано согласий 3 из 3"

wait_click_text "Зарегистрировать"
wait_text "Регистрация"


# Начало заполнения формы создания согласия

play_stored_lines ama_datamart_start_fields_2

wait_click_text "Заявителем является должник (кликните, чтобы поменять)"
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-start-applicant"] '
	wait_text "Информация о заявителе"
	play_stored_lines ama_datamart_applicant_fields_1
	wait_click_text "Сохранить информацию о заявителе"
	wait_text "Тростников С.М."
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-start .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
js wbt.SetModelFieldValue("Ссылка", "1234");
js wbt.SetModelFieldValue("Время.акта", "03.05.2021");
js wbt.SetModelFieldValue("Показывать_СРО", "checked");
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

js $(".cpw-ama-datamart-start .appointment").click();
wait_text "Судебный акт о назначении кандидатуры АУ"
play_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о назначении"
wait_text_disappeared "Судебный акт о назначении кандидатуры АУ"

js $(".cpw-ama-datamart-start .efrsb_report").click();
wait_text "Сообщение на ЕФРСБ о введении процедуры"
play_stored_lines ama_datamart_efrsb_report_fields_1
wait_click_text "Сохранить информацию о сообщении"
wait_text_disappeared "Сообщение на ЕФРСБ о введении процедуры"

# Конец заполнения формы создания согласия

wait_click_full_text "Сохранить"


wait_text "Показано согласий 4 из 4"

wait_click_text "Зарегистрировать"
wait_text "Регистрация"


# Начало заполнения формы создания согласия

play_stored_lines ama_datamart_start_fields_2

wait_click_text "Заявителем является должник (кликните, чтобы поменять)"
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-start-applicant"] '
	wait_text "Информация о заявителе"
	play_stored_lines ama_datamart_applicant_fields_1
	wait_click_text "Сохранить информацию о заявителе"
	wait_text "Тростников С.М."
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-start .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
js wbt.SetModelFieldValue("Ссылка", "1234");
js wbt.SetModelFieldValue("Время.акта", "03.05.2021");
js wbt.SetModelFieldValue("Показывать_СРО", "checked");
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

js $(".cpw-ama-datamart-start .appointment").click();
wait_text "Судебный акт о назначении кандидатуры АУ"
play_stored_lines ama_datamart_court_decision_fields_1
wait_click_text "Сохранить информацию о назначении"
wait_text_disappeared "Судебный акт о назначении кандидатуры АУ"

js $(".cpw-ama-datamart-start .efrsb_report").click();
wait_text "Сообщение на ЕФРСБ о введении процедуры"
play_stored_lines ama_datamart_efrsb_report_fields_1
wait_click_text "Сохранить информацию о сообщении"
wait_text_disappeared "Сообщение на ЕФРСБ о введении процедуры"

# Конец заполнения формы создания согласия

wait_click_full_text "Сохранить"


wait_text "Показано согласий 5 из 5"

je $('.profile-dropdown').click();
wait_click_text "Выйти"
wait_click_text "Кабинет СРО АУ"

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетСРО"] ';
js wbt.SetModelFieldValue("Login", "s@s.ru");
js wbt.SetModelFieldValue("Password", "1");
js wbt.Model_selector_prefix= '';
wait_click_full_text "Войти"
wait_text_disappeared "ajax"

wait_click_text "Запросы"
wait_text "Показано запросов 4 из 4"
wait_text "Трамп"
wait_text "Голохвастов"

wait_click_text "Трамп"
wait_text "Регистрация запроса кандидатур АУ из суда"
js wbt.SetModelFieldValue("Запрос.Время.опрошен_ау", "checked");
wait_click_full_text "Сохранить"
wait_text_disappeared "Регистрация запроса кандидатур АУ из суда"


je $('.profile-dropdown').click();
wait_click_text "Выйти"
wait_click_text "Кабинет абонента ИС ПАУ"

wait_click_text "Кабинет АУ"

shot_check_png ..\..\shots\00new_manager_ev.png

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетАУ"] ';
js wbt.SetModelFieldValue("Login", "x@y.com");
js wbt.SetModelFieldValue("Password", "1");
js wbt.Model_selector_prefix= '';
wait_click_full_text "Войти"
wait_click_text "Процедуры"
wait_text "КАРИНА"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00new_logged_manager.png

wait_click_full_text "Назначения"
wait_text "Показано запросов 2 из 2"
wait_text "1й Иванов И.И."

exit