﻿execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
include ..\..\..\..\uc\forms\ama\datamart\common\push\tests\cases\in.lib.txt quiet
jw !!window.wbt

# ------------------------------------ Как АУ 1
wait_click_text "Кабинет АУ"

shot_check_png ..\..\shots\00new_manager_ev.png

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетАУ"] ';
js wbt.SetModelFieldValue("Login", "x@y.com");
js wbt.SetModelFieldValue("Password", "1");
js wbt.Model_selector_prefix= '';
wait_click_full_text "Войти"
wait_text_disappeared "Вызов ajax"
wait_click_text "Процедуры"
wait_text "Искать"

shot_check_png ..\..\shots\00new_logged_manager.png

je $(".profile-dropdown").click();
wait_click_text "Уведомления"
wait_text_disappeared "Вызов ajax"
shot_check_png ..\..\shots\00man_push_settings_before.png

play_stored_lines ama_datamart_push_receiver_settings_1

shot_check_png ..\..\shots\00man_push_settings_edited.png

wait_click_text "Сохранить настройки уведомлений"

wait_text_disappeared "Вызов ajax"

je $(".profile-dropdown").click();
wait_click_text "Уведомления"
wait_text_disappeared "Вызов ajax"

shot_check_png ..\..\shots\00man_push_settings_edited.png

wait_click_text "Журнал"
wait_text_disappeared "Загрузка"
shot_check_png ..\..\shots\00man_push_settings_log.png

wait_click_text "Отмена"

je $(".profile-dropdown").click();
wait_click_text "Выйти"

# ------------------------------------ Как АУ 2

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетАУ"] ';
js wbt.SetModelFieldValue("Login", "z@w.com");
js wbt.SetModelFieldValue("Password", "2");
js wbt.Model_selector_prefix= '';

wait_click_full_text "Войти"
wait_text_disappeared "Вызов ajax"
wait_click_text "Процедуры"
wait_text "Искать"

je $(".profile-dropdown").click();
wait_click_text "Уведомления"
wait_text_disappeared "Вызов ajax"

play_stored_lines ama_datamart_push_receiver_settings_2

shot_check_png ..\..\shots\00man_push_settings_edited2_new.png

wait_click_text "Сохранить настройки уведомлений"
wait_text_disappeared "Вызов ajax"

je $(".profile-dropdown").click();
wait_click_text "Уведомления"
wait_text_disappeared "Вызов ajax"

shot_check_png ..\..\shots\00man_push_settings_edited2.png

exit