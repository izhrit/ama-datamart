﻿set names utf8;
delete from Asset;

insert into Asset
set 
 id_MProcedure=1
,id_AssetGroup=2
,id_Region=5
,Title="Запорожец 1985 года"
,TimeChanged="2022-01-09 12:00:00"
,TimeExpositionStart="2022-01-09 12:00:00"
,Description="Замечательная самодвижущаяся повозка светло салатового цвета с перламутровыми вставками, 27 лошадинных сил. Пробег 10 км, зимняя резина"
,Address="Ижевск, Лихвинцева 56-67"
,ExtraFields=compress("{}")
,ID_Object="{4BDB8B58-665F-4A45-B22C-596AE3749EDF}"
,md5hash="b5dbb0c0189880787df1bafe239438a9"
,State='d'
,isAvailableToAll=1
,Revision=1
;
set @id_z= last_insert_id();
insert into Asset_EfrsbAssetClass set id_Asset=@id_z, id_EfrsbAssetClass=(select c.id_EfrsbAssetClass from EfrsbAssetClass c where c.Title like '%Право оперативного управления%');

insert into Asset
set 
 id_MProcedure=4
,id_AssetGroup=1
,id_Region=6
,Title="Домик для уточек"
,TimeChanged="2022-02-09 12:00:00"
,TimeExpositionStart="2022-02-09 12:00:00"
,Description="3-х этажное здание"
,Address="Москва, Божедомка 8"
,ExtraFields=compress("{}")
,ID_Object="{1BA45330-7800-4C69-89F6-B0B85ED730C6}"
,md5hash="b5dbb0c0189880787df1bafe239438a9"
,State='h'
,isAvailableToAll=1
,Revision=2
;
set @id_d= last_insert_id();
insert into Asset_EfrsbAssetClass set id_Asset=@id_d, id_EfrsbAssetClass=(select c.id_EfrsbAssetClass from EfrsbAssetClass c where c.Title like '%Жилые здания%');