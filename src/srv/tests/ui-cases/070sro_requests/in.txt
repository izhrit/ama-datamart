﻿execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
include ..\..\..\..\uc\forms\ama\datamart\sro\request\tests\cases\in.lib.txt quiet
jw !!window.wbt
je app.cpw_Now= function(){return new Date(2020, 4, 24, 17, 36, 0, 0 );}
wait_click_text "Кабинет СРО АУ"

js wbt.Model_selector_prefix= '[data-fc-selector="КабинетСРО"] ';
js wbt.SetModelFieldValue("Login", "s@s.ru");
js wbt.SetModelFieldValue("Password", "1");
js wbt.Model_selector_prefix= '';
wait_click_full_text "Войти"
wait_text_disappeared "ajax"
shot_check_png ..\..\shots\00newsro_requests_logged_sro.png

wait_text "Согласия"
wait_click_text "Запросы"

wait_click_text "Зарегистрировать запрос"


# Начало заполнения формы создания запроса

js wbt.SetModelFieldValue("Должник.Тип", "Супруги");

js $('.cpw-ama-datamart-request .cpw-ama-datamart-married_couple .debtor1').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_1
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js $('.cpw-ama-datamart-request .cpw-ama-datamart-married_couple .debtor2').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_2
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Москвы');

wait_click_text "Заявителями являются должники (кликните, чтобы поменять)"
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-request-applicant"] '
	wait_text "Информация о заявителе"
	play_stored_lines ama_datamart_applicant_fields_1
	wait_click_text "Сохранить информацию о заявителе"
	wait_text "Тростников С.М."
js wbt.Model_selector_prefix= '';
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-request .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
play_stored_lines ama_datamart_court_decision_fields_2
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

# Конец заполнения формы создания запроса

shot_check_png ..\..\shots\63sro_requests_filled5.png
wait_click_text "Отмена"


wait_text "Показано запросов 2 из 2"
shot_check_png ..\..\shots\sro1_requests_initial.png

wait_click_text "Зарегистрировать запрос"


# Начало заполнения формы создания запроса

js wbt.SetModelFieldValue("Должник.Тип", "Супруги");

js $('.cpw-ama-datamart-request .cpw-ama-datamart-married_couple .debtor1').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_1
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js $('.cpw-ama-datamart-request .cpw-ama-datamart-married_couple .debtor2').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_2
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Москвы');

wait_click_text "Заявителями являются должники (кликните, чтобы поменять)"
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-request-applicant"] '
	wait_text "Информация о заявителе"
	play_stored_lines ama_datamart_applicant_fields_1
	wait_click_text "Сохранить информацию о заявителе"
	wait_text "Тростников С.М."
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-request .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
play_stored_lines ama_datamart_court_decision_fields_2
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

# Конец заполнения формы создания запроса

# shot_check_png ..\..\shots\63sro_requests_filled5.png
wait_click_text "Сохранить"
wait_text_disappeared "ajax"
wait_text_disappeared "Регистрация запроса кандидатур АУ из суда"


wait_text "Показано запросов 3 из 3"

wait_click_text "Щербаков"
wait_text ""Регистрация запроса кандидатур АУ из суда"


# Начало заполнения формы создания запроса

play_stored_lines request_fields_6

wait_click_text "Заявителем является должник (кликните, чтобы поменять)"
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-request-applicant"] '
	wait_text "Информация о заявителе"
	js wbt.SetModelFieldValue("Заявитель_должник", "checked");
	wait_click_text "Сохранить информацию о заявителе"
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-request .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
play_stored_lines ama_datamart_court_decision_fields_3
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

# Конец заполнения формы создания запроса

shot_check_png ..\..\shots\63sro_requests_filled6.png
wait_click_text "Отмена"
wait_text_disappeared "Регистрация запроса кандидатур АУ из суда"


wait_text "Показано запросов 3 из 3"
shot_check_png ..\..\shots\63sro_requests_added.png

wait_click_text "Щербаков"
wait_text ""Регистрация запроса кандидатур АУ из суда"


# Начало заполнения формы создания запроса

play_stored_lines request_fields_6

wait_click_text "Заявителем является должник (кликните, чтобы поменять)"
js wbt.Model_selector_prefix= '[aria-describedby="cpw-form-ama-request-applicant"] '
	wait_text "Информация о заявителе"
	js wbt.SetModelFieldValue("Заявитель_должник", "checked");
	wait_click_text "Сохранить информацию о заявителе"
js wbt.Model_selector_prefix= '';

js $(".cpw-ama-datamart-request .decision").click();
wait_text "Судебный акт о запросе кандидатур АУ"
play_stored_lines ama_datamart_court_decision_fields_3
wait_click_text "Сохранить информацию о запросе"
wait_text_disappeared "Судебный акт о запросе кандидатур АУ"

# Конец заполнения формы создания запроса

shot_check_png ..\..\shots\63sro_requests_filled6.png
wait_click_text "Сохранить"
wait_text_disappeared "ajax"
wait_text_disappeared "Регистрация запроса кандидатур АУ из суда"


wait_text "Показано запросов 3 из 3"

wait_click_text "Ясная поляна"
wait_text "Регистрация запроса кандидатур АУ из суда"
wait_click_text "Отмена"
wait_text "Показано запросов 3 из 3"
shot_check_png ..\..\shots\63sro_requests_edited.png

je $("#cpw-ama-dm-requests-grid #1 div.actions-button").click();
wait_click_text "Удалить"
wait_click_text "Отмена"
wait_text "Показано запросов 3 из 3"
shot_check_png ..\..\shots\63sro_requests_edited.png

je $("#cpw-ama-dm-requests-grid #1 div.actions-button").click();
wait_click_text "Удалить"
wait_click_text "Да, удалить"
wait_text_disappeared "ajax"
wait_text "Показано запросов 2 из 2"
shot_check_png ..\..\shots\sro1_requests_initial.png

# проверка поиска суда про номеру дела

wait_click_text "Зарегистрировать запрос"

wait_text "kad.arbitr"

wait_click_text "по номеру дела"
wait_text "Отказ искать"
wait_click_text "OK"
wait_text_disappeared "Отказ искать"

js wbt.SetModelFieldValue("Номер_судебного_дела", 'X40-184222/2015');
wait_click_text "по номеру дела"
wait_text "Отказ искать"
wait_click_text "OK"
wait_text_disappeared "Отказ искать"

je court_name= function(name) {var data= $('[data-fc-selector="Заявление_на_банкротство.В_суд"]').select2('data'); return data && null!=data && -1!=data.text.indexOf(name);}

je wbt.SetModelFieldValue("Номер_судебного_дела", 'А40-184222/2015');
wait_click_text "по номеру дела"
wait_text_disappeared ajax
jw court_name("АС г. Москвы");

je wbt.SetModelFieldValue("Номер_судебного_дела", 'А71-184222/2015');
wait_click_text "по номеру дела"
wait_text_disappeared ajax
jw court_name("АС Удмуртской респ.");

exit