copy %~dp0\tpl_newline.txt /a %~dp0\in.lib.txt /b /y

call :out-autoredirect-lines auth-customer-1-20200402T000101-section-sl-ur            auth-customer-1-20200402T000101             section_UR
call :out-autoredirect-lines auth-manager-1-20200401T000102-section-sl-kr             auth-manager-1-20200401T000102              section_KR
call :out-autoredirect-lines bad-auth-section-sl-ur                                   auth-manager-3-bad-token-20200301T000102    section_UR

exit /B

:out-autoredirect-lines
echo start_store_lines_as %1 >> %~dp0\in.lib.txt
copy %~dp0\in.lib.txt+%~dp0\tpl_goto_wait_fa_prefix.txt /a %~dp0\in.lib.txt /b /y
copy %~dp0\in.lib.txt+%~dp0\..\..\..\..\..\clnt\test-client-ama\tests\prepare-auth\%2.etalon.txt /a %~dp0\in.lib.txt /b /y
if "" == "%3" goto :no-extra-args
copy %~dp0\in.lib.txt+%~dp0\%3.txt /a %~dp0\in.lib.txt /b /y
:no-extra-args
copy %~dp0\in.lib.txt+%~dp0\tpl_goto_wait_postfix.txt /a %~dp0\in.lib.txt /b /y
echo stop_store_lines >> %~dp0\in.lib.txt
copy %~dp0\in.lib.txt+%~dp0\tpl_newline.txt /a %~dp0\in.lib.txt /b /y
exit /B


