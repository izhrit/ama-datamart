select 
	  d.Name debtorName
	, mpu.id_MProcedure
	, mu.UserName
	, mu.id_MUser
	, u.UserName
from MProcedure mp
inner join Debtor d on d.id_Debtor=mp.id_Debtor 
left join MProcedureUser mpu on mpu.id_MProcedure=mp.id_MProcedure 
left join ManagerUser mu on mu.id_ManagerUser=mpu.id_ManagerUser
left join MUser u on mu.id_MUser=u.id_MUser
where d.Name like 'Алексашенко%'\G

select 
	id_MUser
	, UserName 
from MUser\G