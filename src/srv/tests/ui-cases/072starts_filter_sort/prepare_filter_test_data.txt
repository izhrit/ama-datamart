wait_click_text "Зарегистрировать согласие"
wait_text "Регистрация согласия АУ на предоставление кандидатуры"

# Начало заполнения формы создания запроса

js wbt.SetModelFieldValue("Должник.Тип", "Супруги");

js $('.cpw-ama-datamart-start .cpw-ama-datamart-married_couple .debtor1').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_1
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js $('.cpw-ama-datamart-start .cpw-ama-datamart-married_couple .debtor2').click();
wait_text "Информация о должнике"
play_stored_lines ama_datamart_debtor_physical_fields_2
js $('[aria-describedby="cpw-form-ama-married_couple-debtor_physical"] .ui-dialog-buttonpane .ui-button-text:contains("Сохранить")').click()
wait_text_disappeared "Информация о должнике"

js wbt.SetModelFieldValue("Заявление_на_банкротство.В_суд", 'Москвы');

js wbt.Model_selector_prefix= '.cpw-ama-datamart-start  '
	js $('.select-au').select2('open');
	js wbt.SetModelFieldValue("АУ",  "Иванов");
js wbt.Model_selector_prefix= '';

# Конец заполнения формы создания запроса

wait_click_text "Сохранить"
wait_text_disappeared "Регистрация согласия АУ на предоставление кандидатуры"