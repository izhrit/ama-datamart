select 
	  m.lastName
	, d.Name debtorName
	, mpu.id_MProcedure
	, mu.UserName
	, mu.id_MUser
	, u.UserName
from manager m
left join MProcedure mp on m.id_Manager=mp.id_Manager
left join Debtor d on d.id_Debtor=mp.id_Debtor 
left join MProcedureUser mpu on mpu.id_MProcedure=mp.id_MProcedure 
left join ManagerUser mu on mu.id_ManagerUser=mpu.id_ManagerUser
left join MUser u on mu.id_MUser=u.id_MUser
where m.lastName like 'Иванов%'
order by mpu.id_MProcedure, mu.id_ManagerUser
\G
