select 
id_ProcedureStart
, id_Manager
, id_Court
, id_SRO
, id_MRequest
, CaseNumber
, DebtorCategory
, DebtorName
, DebtorINN
, DebtorSNILS
, DebtorOGRN
, DateOfApplication
, DateOfRequestAct
, NextSessionDate
from ProcedureStart
\G

select
id_MRequest
, id_Court
, id_SRO
, id_Manager
, CaseNumber
, DebtorCategory
, DebtorName
, DebtorINN
, DebtorSNILS
, DebtorOGRN
, DateOfRequestAct
, DateOfRequest
, DateOfOffer
, DateOfResponce
, NextSessionDate
from MRequest
\G