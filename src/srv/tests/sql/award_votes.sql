select 
	 v.inn   vote_inn
	, replace(v.BulletinText,'\\r\\n','\n') BulletinText
    ,n.inn  nominee_inn
    ,concat(n.lastName,' ',n.firstName,' ',n.middleName)  nominee_FIO
	,v.lastName
	,v.middleName
	,v.firstName
	,v.SRO
	,v.email
from AwardVote v
inner join AwardNominee n on v.id_AwardNominee=n.id_AwardNominee
order by v.VoteTime
\G