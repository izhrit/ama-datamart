select
	id_SentEmail
	, RecipientEmail
	, RecipientId
	, RecipientType
	, EmailType
	, replace(uncompress(ExtraParams),'\\r\\n','\n') ExtraParams
from SentEmail\G
