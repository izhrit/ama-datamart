set names utf8;
INSERT INTO `mock_efrsb_debtor` 
(`id_Debtor`, `ArbitrManagerID`, `BankruptId`, `Name`, `INN`, `OGRN`, Revision, `Body`) 
VALUES ('7', '1', '123123', 'ООО \"БайкалИнжиниринг\"', '3812141040', '1123850021426', 100
, compress('{
	"LastMessageDate": "2017-09-20T05:31:12.54",
	"LastReportDate": "2017-09-20T05:36:50.413",
	"LegalCaseList": {
		"LegalCaseInfo": [
			{
				"Number": "А19-12951\/2016 ",
				"DateCreate": "2016-10-20T11:42:20.78",
				"Court": "Арбитражный суд Иркутской области",
				"IsApplicantCreditOrg": null,
				"IsLiabilitySecured": null
			}
		]
	},
	"!Category": "Обычная организация",
	"!CategoryCode": "SimpleOrganization",
	"!Region": "Иркутская область",
	"!DateLastModif": "2016-10-20T11:41:57.567",
	"!INN": "3812141040",
	"!BankruptId": "123123",
	"!FullName": "ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \\"БАЙКАЛИНЖИНИРИНГ\\"",
	"!ShortName": "ООО \\"БАЙКАЛИНЖИНИРИНГ\\"",
	"!OGRN": "1123850021426",
	"!LegalAddress": "ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341\/3"
}'));

INSERT INTO mock_efrsb_message set `id_Message`= 8, `ArbitrManagerID`= 1, `BankruptId`= 123123, `INN`= '3812141040', `OGRN`= '1123850021426', `PublishDate`= '2017-09-20T05:31:12', `MessageInfo_MessageType`= 'a', `Number`= '2088745', `MessageGUID`= 'D43F6E8B0F195399C6949BBE5586917F', `efrsb_id`= '2088745', `Revision`=6
, `Body`= compress('<MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>2088745</Id>
  <Number>2088745</Number>
  <CaseNumber>А19-12951/2016 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="14678" FirstName="Татьяна" MiddleName="Владимировна" LastName="Келене (Бровина)" INN="381108251427" SNILS="10706165423" RegistryNumber="10207">
      <OGRN />
      <Sro SroId="25">
        <SroName>НП "СГАУ" - Некоммерческое партнерство "Сибирская гильдия антикризисных управляющих"</SroName>
        <SroRegistryNumber>0009</SroRegistryNumber>
        <OGRN>1028600516735</OGRN>
        <INN>8601019434</INN>
        <LegalAddress>628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18</LegalAddress>
      </Sro>
      <CorrespondenceAddress>664003, г. Иркутск, а/я 281</CorrespondenceAddress>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="ArbitralDecree">
    <CourtDecision>
      <Text>Временный управляющий ООО «Байкалинжиниринг» (ИНН 3812141040, ОГРН 1123850021426, адрес место нахождения: 664017, г. Иркутск, ул. Лермонтова, 341/3) Келене Татьяна Владимировна  (ИНН 381108251427, СНИЛС 107-061-654-23, адрес для корреспонденции: 664003, г. Иркутск, а/я 281), член СРО НП "Сибирская гильдия антикризисных управляющих" (ИНН 8601019434, ОГРН 1028600516735, адрес: 628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18) сообщает о прекращении производства по делу №А19-12951/2016 на основании резолютивной части Определения АС Иркутской области от 18.09.2017г. </Text>
      <DecisionType Name="о прекращении производства по делу" Id="8" />
      <CourtDecree>
        <CourtId>71</CourtId>
        <CourtName>Арбитражный суд Иркутской области</CourtName>
        <FileNumber>А19-12951/2016 </FileNumber>
        <DecisionDate>2017-09-18</DecisionDate>
      </CourtDecree>
      <LossesFromArbitrManagerActionsAmount xsi:nil="true" />
      <CitizenNotReleasedFromResponsibility xsi:nil="true" />
      <CreditorClaimRegisterCloseDate xsi:nil="true" />
      <CreditorClaimSettingRequirementsExpirationDate xsi:nil="true" />
      <DecisionMadeDueTorCancellationRestructuringPlan xsi:nil="true" />
      <ArbitrManagerType xsi:nil="true" />
    </CourtDecision>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="93193" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ &quot;БАЙКАЛИНЖИНИРИНГ&quot;" ShortName="ООО &quot;БАЙКАЛИНЖИНИРИНГ&quot;" PostAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3" OKPO="" OGRN="1123850021426" LegalAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3">
      <INN>3812141040</INN>
    </BankruptFirm>
  </BankruptInfo>
  <FileInfoList>
    <FileInfo>
      <Name>О прекращении процедуры.pdf</Name>
      <Hash>6880EC9BD681D65DBCEE7631F6FBD023D32B99CB1ED82CFF34E692A135AF78A6</Hash>
    </FileInfo>
  </FileInfoList>
  <PublishDate>2017-09-20T05:31:12.54</PublishDate>
  <BankruptId>123123</BankruptId>
  <MessageGUID>D43F6E8B0F195399C6949BBE5586917F</MessageGUID>
  <MessageURLList>
    <MessageURL URLName="О прекращении процедуры.pdf" URL="http://bankrot.fedresurs.ru/Download/file.fo?id=1100854&amp;type=MessageDocument" DownloadSize="210528" />
  </MessageURLList>
</MessageData>');
INSERT INTO mock_efrsb_message set `id_Message`= 9, `ArbitrManagerID`= 1, `BankruptId`= 123123, `INN`= '3812141040', `OGRN`= '1123850021426', `PublishDate`= '2017-08-22T05:38:55', `MessageInfo_MessageType`= 'd', `Number`= '2025273', `MessageGUID`= '04FF96902C5C86CBB2F4C71C4485E386', `efrsb_id`= '2025273', `Revision`=6
, `Body`= compress('<MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>2025273</Id>
  <Number>2025273</Number>
  <CaseNumber>А19-12951/2016 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="14678" FirstName="Татьяна" MiddleName="Владимировна" LastName="Келене (Бровина)" INN="381108251427" SNILS="10706165423" RegistryNumber="10207">
      <OGRN />
      <Sro SroId="25">
        <SroName>НП "СГАУ" - Некоммерческое партнерство "Сибирская гильдия антикризисных управляющих"</SroName>
        <SroRegistryNumber>0009</SroRegistryNumber>
        <OGRN>1028600516735</OGRN>
        <INN>8601019434</INN>
        <LegalAddress>628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18</LegalAddress>
      </Sro>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="MeetingResult">
    <MeetingResult>
      <Text>Временный управляющий ООО «БайкалИнжиниринг» (ИНН 3812141040, ОГРН 1123850021426, адрес место нахождения: 664017, г. Иркутск, ул. Лермонтова, 341/3) Келене Татьяна Владимировна (ИНН 381108251427, СНИЛС 107-061-654-23, адрес для корреспонденции: 664003, г. Иркутск, а/я 281), член Некоммерческого партнерства "Сибирская гильдия антикризисных управляющих" (ИНН 8601019434, ОГРН1028600516735, адрес: 628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18), действующий на основании Определения Арбитражного суда Иркутской области по делу №А19-12951/2016 от 01.08.2017г. сообщает, что повторное первое собрание кредиторов ООО «БайкалИнжиниринг» назначенное на «22» августа 2017 г. не состоялось в виду отсутствия кворума.

</Text>
      <MeetingForm>Очная</MeetingForm>
    </MeetingResult>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="93193" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ &quot;БАЙКАЛИНЖИНИРИНГ&quot;" ShortName="ООО &quot;БАЙКАЛИНЖИНИРИНГ&quot;" PostAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3" OKPO="" OGRN="1123850021426" LegalAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3">
      <INN>3812141040</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2017-08-22T05:38:55.907</PublishDate>
  <BankruptId>123123</BankruptId>
  <MessageGUID>04FF96902C5C86CBB2F4C71C4485E386</MessageGUID>
</MessageData>');
INSERT INTO mock_efrsb_message set `id_Message`= 10, `ArbitrManagerID`= 1, `BankruptId`= 123123, `INN`= '3812141040', `OGRN`= '1123850021426', `PublishDate`= '2017-08-22T05:38:55', `MessageInfo_MessageType`= 'c', `Number`= '1989937', `MessageGUID`= 'AEB56EF63BD1C5395D54B5406F8990EB', `efrsb_id`= '1989937', `Revision`=6
, `Body`= compress('<MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1989937</Id>
  <Number>1989937</Number>
  <CaseNumber>А19-12951/2016 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="14678" FirstName="Татьяна" MiddleName="Владимировна" LastName="Келене (Бровина)" INN="381108251427" SNILS="10706165423" RegistryNumber="10207">
      <OGRN />
      <Sro SroId="25">
        <SroName>НП "СГАУ" - Некоммерческое партнерство "Сибирская гильдия антикризисных управляющих"</SroName>
        <SroRegistryNumber>0009</SroRegistryNumber>
        <OGRN>1028600516735</OGRN>
        <INN>8601019434</INN>
        <LegalAddress>628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18</LegalAddress>
      </Sro>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="Meeting">
    <Meeting>
      <Text>Временный управляющий ООО «БайкалИнжиниринг» (ИНН 3812141040, ОГРН 1123850021426, адрес место нахождения: 664017, г. Иркутск, ул. Лермонтова, 341/3) Келене Татьяна Владимировна (ИНН 381108251427, СНИЛС 107-061-654-23, адрес для корреспонденции: 664003, г. Иркутск, а/я 281), член Некоммерческого партнерства "Сибирская гильдия антикризисных управляющих" (ИНН 8601019434, ОГРН1028600516735, адрес: 628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18), действующий на основании Определения Арбитражного суда Иркутской области по делу №А19-12951/2016 от 31.05.2017г. сообщает, что в соответствии с требованиями ст. 12-14, 72 Федерального закона от 26.10.2002 г. № 127-ФЗ «О несостоятельности (банкротстве)» повторное первое собрание кредиторов ООО «БайкалИнжиниринг» состоится «22» августа 2017 г. 
Место проведения собрания: г. Иркутск, ул. Кожова, 14/3, 4 этаж, офис 3.
Начало собрания: 10 часов 00 минут местного времени.
Регистрация участников собрания по месту проведения собрания 22.08.2017г. с 09 час. 30 мин. до 09 час. 50 мин. 
При регистрации необходимо иметь: 
          1)  документ, удостоверяющий личность;
          2) документ, подтверждающий полномочия представителя (доверенность, выданная в установленном законом порядке)
Прошу также предоставить копии документов, подтверждающих полномочия участников собрания с целью последующего приобщения к протоколу собрания кредиторов в соответствии с требованиями закона.
Повестка собрания:
1.	Отчет временного управляющего о проделанной работе.
2.	Принятие решения о введении дальнейшей процедуры банкротства.
3.	Определение кандидатуры арбитражного управляющего или саморегулируемой организации, из числа членов которой должен быть утвержден арбитражный управляющий.
4.	Избрание комитета кредиторов, определение количественного состава и полномочий комитета кредиторов, избрание членов комитета кредиторов.
5.	Определение дополнительных требований к кандидатуре арбитражного управляющего (административного управляющего, внешнего управляющего, конкурсного управляющего).
6.	Привлечение реестродержателя к ведению реестра требований кредиторов должника.
Ознакомиться с материалами к собранию кредиторов Вы можете с 15.08.2017г. по адресу: г. Иркутск, ул. Кожова, 14/3, 4 этаж, офис 3, ежедневно в рабочие дни с 10 часов 00 минут до 17 часов 00 минут по предварительной договоренности по тел. 89021709958.
</Text>
      <MeetingSite>г. Иркутск, ул. Кожова, 14/3, 4 этаж, офис 3</MeetingSite>
      <MeetingDate xsi:nil="true" />
      <MeetingDateBegin>2017-08-22</MeetingDateBegin>
      <MeetingTimeBegin>10:00:00.0000000+03:00</MeetingTimeBegin>
      <MeetingForm>Очная</MeetingForm>
      <RegistrationSite>г. Иркутск, ул. Кожова, 14/3, 4 этаж, офис 3</RegistrationSite>
      <RegistrationDate>2017-08-22</RegistrationDate>
      <RegistrationTimeBegin>09:30:00.0000000+03:00</RegistrationTimeBegin>
      <RegistrationTimeEnd>09:50:00.0000000+03:00</RegistrationTimeEnd>
      <ExaminationSite>г. Иркутск, ул. Кожова, 14/3, 4 этаж, офис 3</ExaminationSite>
      <ExaminationDate>2017-08-15</ExaminationDate>
      <Comment>ежедневно в рабочие дни с 10 часов 00 минут до 17 часов 00 минут по предварительной договоренности по тел. 89021709958</Comment>
      <FuMailAddress>664003, г. Иркутск, а/я 281</FuMailAddress>
    </Meeting>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="93193" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ &quot;БАЙКАЛИНЖИНИРИНГ&quot;" ShortName="ООО &quot;БАЙКАЛИНЖИНИРИНГ&quot;" PostAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3" OKPO="" OGRN="1123850021426" LegalAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3">
      <INN>3812141040</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2017-08-07T11:04:48.823</PublishDate>
  <BankruptId>123123</BankruptId>
  <MessageGUID>AEB56EF63BD1C5395D54B5406F8990EB</MessageGUID>
</MessageData>');
INSERT INTO mock_efrsb_message set `id_Message`= 11, `ArbitrManagerID`= 1, `BankruptId`= 123123, `INN`= '3812141040', `OGRN`= '1123850021426', `PublishDate`= '2017-08-07T11:04:36', `MessageInfo_MessageType`= 'd', `Number`= '1989928', `MessageGUID`= '665ACD6379F3927A1544057DB6B028F0', `efrsb_id`= '1989928', `Revision`=6
, `Body`= compress('<MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1989928</Id>
  <Number>1989928</Number>
  <CaseNumber>А19-12951/2016 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="14678" FirstName="Татьяна" MiddleName="Владимировна" LastName="Келене (Бровина)" INN="381108251427" SNILS="10706165423" RegistryNumber="10207">
      <OGRN />
      <Sro SroId="25">
        <SroName>НП "СГАУ" - Некоммерческое партнерство "Сибирская гильдия антикризисных управляющих"</SroName>
        <SroRegistryNumber>0009</SroRegistryNumber>
        <OGRN>1028600516735</OGRN>
        <INN>8601019434</INN>
        <LegalAddress>628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18</LegalAddress>
      </Sro>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="MeetingResult">
    <MeetingResult>
      <Text>Временный управляющий ООО «БайкалИнжиниринг» (ИНН 3812141040, ОГРН 1123850021426, адрес место нахождения: 664017, г. Иркутск, ул. Лермонтова, 341/3) Келене Татьяна Владимировна (ИНН 381108251427, СНИЛС 107-061-654-23, адрес для корреспонденции: 664003, г. Иркутск, а/я 281), член Некоммерческого партнерства "Сибирская гильдия антикризисных управляющих" (ИНН 8601019434, ОГРН1028600516735, адрес: 628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18), действующий на основании Определения Арбитражного суда Иркутской области по делу №А19-12951/2016 от 01.08.2017г. сообщает, что первое собрание кредиторов ООО «БайкалИнжиниринг» назначенное на «07» августа 2017 г. не состоялось в виду отсутствия кворума.</Text>
      <MeetingForm>Очная</MeetingForm>
    </MeetingResult>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="93193" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ &quot;БАЙКАЛИНЖИНИРИНГ&quot;" ShortName="ООО &quot;БАЙКАЛИНЖИНИРИНГ&quot;" PostAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3" OKPO="" OGRN="1123850021426" LegalAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3">
      <INN>3812141040</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2017-08-07T11:04:36.213</PublishDate>
  <BankruptId>123123</BankruptId>
  <MessageGUID>665ACD6379F3927A1544057DB6B028F0</MessageGUID>
</MessageData>');
INSERT INTO mock_efrsb_message set `id_Message`= 12, `ArbitrManagerID`= 1, `BankruptId`= 123123, `INN`= '3812141040', `OGRN`= '1123850021426', `PublishDate`= '2017-07-24T04:28:39', `MessageInfo_MessageType`= 'c', `Number`= '1952000', `MessageGUID`= '22432B98432148A8AE44D5AA8D93AB83', `efrsb_id`= '1952000', `Revision`=6
, `Body`= compress('<MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1952000</Id>
  <Number>1952000</Number>
  <CaseNumber>А19-12951/2016 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="14678" FirstName="Татьяна" MiddleName="Владимировна" LastName="Келене (Бровина)" INN="381108251427" SNILS="10706165423" RegistryNumber="10207">
      <OGRN />
      <Sro SroId="25">
        <SroName>НП "СГАУ" - Некоммерческое партнерство "Сибирская гильдия антикризисных управляющих"</SroName>
        <SroRegistryNumber>0009</SroRegistryNumber>
        <OGRN>1028600516735</OGRN>
        <INN>8601019434</INN>
        <LegalAddress>628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18</LegalAddress>
      </Sro>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="Meeting">
    <Meeting>
      <Text>Временный управляющий ООО «БайкалИнжиниринг» (ИНН 3812141040, ОГРН 1123850021426, адрес место нахождения: 664017, г. Иркутск, ул. Лермонтова, 341/3) Келене Татьяна Владимировна (ИНН 381108251427, СНИЛС 107-061-654-23, адрес для корреспонденции: 664003, г. Иркутск, а/я 281), член Некоммерческого партнерства "Сибирская гильдия антикризисных управляющих" (ИНН 8601019434, ОГРН1028600516735, адрес: 628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18), действующий на основании Определения Арбитражного суда Иркутской области по делу №А19-12951/2016 от 31.05.2017г. сообщает, что в соответствии с требованиями ст. 12-14, 72 Федерального закона от 26.10.2002 г. № 127-ФЗ «О несостоятельности (банкротстве)» первое собрание кредиторов ООО «БайкалИнжиниринг» состоится «07» августа 2017 г.
Место проведения собрания: г. Иркутск, ул. Кожова, 14/3, 4 этаж, офис 3.
Начало собрания: 11 часов 00 минут местного времени.
Регистрация участников собрания по месту проведения собрания 07.08.2017г. с 10 час. 30 мин. до 10 час. 50 мин. 
При регистрации необходимо иметь: 
          1)  документ, удостоверяющий личность;
          2) документ, подтверждающий полномочия представителя (доверенность, выданная в установленном законом порядке)
Прошу также предоставить копии документов, подтверждающих полномочия участников собрания с целью последующего приобщения к протоколу собрания кредиторов в соответствии с требованиями закона.
Повестка собрания:
1.	Отчет временного управляющего о проделанной работе.
2.	Принятие решения о введении дальнейшей процедуры банкротства.
3.	Определение кандидатуры арбитражного управляющего или саморегулируемой организации, из числа членов которой должен быть утвержден арбитражный управляющий.
4.	Избрание комитета кредиторов, определение количественного состава и полномочий комитета кредиторов, избрание членов комитета кредиторов.
5.	Определение дополнительных требований к кандидатуре арбитражного управляющего (административного управляющего, внешнего управляющего, конкурсного управляющего).
6.	Привлечение реестродержателя к ведению реестра требований кредиторов должника.
Ознакомиться с материалами к собранию кредиторов Вы можете с 31.07.2017г. по адресу: г. Иркутск, ул. Кожова, 14/3, 4 этаж, офис 3, ежедневно в рабочие дни с 10 часов 00 минут до 17 часов 00 минут по предварительной договоренности по тел. 89021709958.
</Text>
      <MeetingSite>г. Иркутск, ул. Кожова, 14/3, 4 этаж, оф. 3</MeetingSite>
      <MeetingDate xsi:nil="true" />
      <MeetingDateBegin>2017-08-07</MeetingDateBegin>
      <MeetingTimeBegin>11:00:00.0000000+03:00</MeetingTimeBegin>
      <MeetingForm>Очная</MeetingForm>
      <RegistrationSite>г. Иркутск, ул. Кожова, 14/3, 4 этаж, оф. 3</RegistrationSite>
      <RegistrationDate>2017-08-07</RegistrationDate>
      <RegistrationTimeBegin>10:30:00.0000000+03:00</RegistrationTimeBegin>
      <RegistrationTimeEnd>10:50:00.0000000+03:00</RegistrationTimeEnd>
      <ExaminationSite>г. Иркутск, ул. Кожова, 14/3, 4 этаж, оф. 3</ExaminationSite>
      <ExaminationDate>2017-07-31</ExaminationDate>
      <Comment>ежедневно в рабочие дни с 10 часов 00 минут до 17 часов 00 минут по предварительной договоренности по тел. 89021709958</Comment>
      <FuMailAddress>664003, г. Иркутск, а/я 281</FuMailAddress>
    </Meeting>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="93193" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ &quot;БАЙКАЛИНЖИНИРИНГ&quot;" ShortName="ООО &quot;БАЙКАЛИНЖИНИРИНГ&quot;" PostAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3" OKPO="" OGRN="1123850021426" LegalAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3">
      <INN>3812141040</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2017-07-24T04:28:39.657</PublishDate>
  <BankruptId>123123</BankruptId>
  <MessageGUID>22432B98432148A8AE44D5AA8D93AB83</MessageGUID>
</MessageData>');
INSERT INTO mock_efrsb_message set `id_Message`= 13, `ArbitrManagerID`= 1, `BankruptId`= 123123, `INN`= '3812141040', `OGRN`= '1123850021426', `PublishDate`= '2017-02-20T14:19:20', `MessageInfo_MessageType`= 'd', `Number`= '1619492', `MessageGUID`= '2C849D0A58791FCB3D44EC76F2394F55', `efrsb_id`= '1619492', `Revision`=6
, `Body`= compress('<MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1619492</Id>
  <Number>1619492</Number>
  <CaseNumber>А19-12951/2016 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="14678" FirstName="Татьяна" MiddleName="Владимировна" LastName="Келене (Бровина)" INN="381108251427" RegistryNumber="10207">
      <OGRN />
      <Sro SroId="25">
        <SroName>НП "СГАУ" - Некоммерческое партнерство "Сибирская гильдия антикризисных управляющих"</SroName>
        <SroRegistryNumber>0009</SroRegistryNumber>
        <OGRN>1028600516735</OGRN>
        <INN>8601019434</INN>
        <LegalAddress>628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18</LegalAddress>
      </Sro>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="MeetingResult">
    <MeetingResult>
      <Text>Временный управляющий ООО «БайкалИнжиниринг» (ИНН 3812141040, ОГРН 1123850021426, адрес место нахождения: 664017, г. Иркутск, ул. Лермонтова, 341/3) Келене Татьяна Владимировна (ИНН 381108251427, СНИЛС 107-061-654-23, адрес для корреспонденции: 664003, г. Иркутск, а/я 281), член Некоммерческого партнерства "Сибирская гильдия антикризисных управляющих" (ИНН 8601019434, ОГРН1028600516735, адрес: 628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18), действующий на основании Определения Арбитражного суда Иркутской области по делу №А19-12951/2016 от 21.10.2016г. сообщает, что первое собрание кредиторов ООО «Байкалинжиниринг», назначенное на «21» февраля 2017г. (публикация в ЕФРСБ №1587318) ОТМЕНЕНО в связи с запретом, определенным Определением Арбитражного суда Иркутской области от 16.02.2017г. по делу №А19-12951/2016.</Text>
      <MeetingForm>Очная</MeetingForm>
    </MeetingResult>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="93193" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ &quot;БАЙКАЛИНЖИНИРИНГ&quot;" ShortName="ООО &quot;БАЙКАЛИНЖИНИРИНГ&quot;" PostAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3" OKPO="" OGRN="1123850021426" LegalAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3">
      <INN>3812141040</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2017-02-20T14:19:20.087</PublishDate>
  <BankruptId>123123</BankruptId>
  <MessageGUID>2C849D0A58791FCB3D44EC76F2394F55</MessageGUID>
  <MessageURLList>
    <MessageURL URLName="ОПРЕДЕЛЕНИЕ о принятии обеспечительных.pdf" URL="http://bankrot.fedresurs.ru/Download/file.fo?id=821489&amp;type=MessageDocument" DownloadSize="222790" />
  </MessageURLList>
</MessageData>');
INSERT INTO mock_efrsb_message set `id_Message`= 14, `ArbitrManagerID`= 1, `BankruptId`= 123123, `INN`= '3812141040', `OGRN`= '1123850021426', `PublishDate`= '2017-02-06T14:44:58', `MessageInfo_MessageType`= 'c', `Number`= '1587318', `MessageGUID`= '15839E94F20EE1697AB41645D513E2C4', `efrsb_id`= '1587318', `Revision`=6
, `Body`= compress('<MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1587318</Id>
  <Number>1587318</Number>
  <CaseNumber>А19-12951/2016 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="14678" FirstName="Татьяна" MiddleName="Владимировна" LastName="Келене (Бровина)" INN="381108251427" RegistryNumber="10207">
      <OGRN />
      <Sro SroId="25">
        <SroName>НП "СГАУ" - Некоммерческое партнерство "Сибирская гильдия антикризисных управляющих"</SroName>
        <SroRegistryNumber>0009</SroRegistryNumber>
        <OGRN>1028600516735</OGRN>
        <INN>8601019434</INN>
        <LegalAddress>628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18</LegalAddress>
      </Sro>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="Meeting">
    <Meeting>
      <Text>Временный управляющий ООО «БайкалИнжиниринг» (ИНН 3812141040, ОГРН 1123850021426, адрес место нахождения: 664017, г. Иркутск, ул. Лермонтова, 341/3) Келене Татьяна Владимировна (ИНН 381108251427, СНИЛС 107-061-654-23, адрес для корреспонденции: 664003, г. Иркутск, а/я 281), член Некоммерческого партнерства "Сибирская гильдия антикризисных управляющих" (ИНН 8601019434, ОГРН1028600516735, адрес: 628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18), действующий на основании Определения Арбитражного суда Иркутской области по делу №А19-12951/2016 от 21.10.2016г. сообщает, что в соответствии с требованиями ст. 12-14, 72 Федерального закона от 26.10.2002 г. № 127-ФЗ «О несостоятельности (банкротстве)» первое собрание кредиторов ООО «БайкалИнжиниринг» состоится «21» февраля 2017г.&lt;br /&gt;Место проведения собрания: г. Иркутск, ул. Кожова, 14/3, 4 этаж, офис 3.&lt;br /&gt;Начало собрания: 11 часов 00 минут местного времени.&lt;br /&gt;Регистрация участников собрания – 21.02.2017г. по месту проведения собрания с 10 час. 40 мин. до 10 час. 55 мин. &lt;br /&gt;При регистрации необходимо иметь: документ, удостоверяющий личность; документ, подтверждающий полномочия представителя (доверенность, выданная в установленном законом порядке).&lt;br /&gt;Необходимо также предоставить копии документов, подтверждающих полномочия участников собрания с целью последующего приобщения к протоколу собрания кредиторов в соответствии с требованиями закона.&lt;br /&gt;Повестка собрания:&lt;br /&gt;1.	Утверждение отчета временного управляющего о проделанной работе.&lt;br /&gt;2.	Принятие решения о введении дальнейшей процедуры банкротства.&lt;br /&gt;3.	Определение кандидатуры арбитражного управляющего или саморегулируемой организации, из числа членов которой должен быть утвержден арбитражный управляющий.&lt;br /&gt;4.	Избрание комитета кредиторов, определение количественного состава и полномочий комитета кредиторов, избрание членов комитета кредиторов.&lt;br /&gt;5.	Определение дополнительных требований к кандидатуре арбитражного управляющего (административного управляющего, внешнего управляющего, конкурсного управляющего).&lt;br /&gt;6.	Привлечение реестродержателя к ведению реестра требований кредиторов должника.&lt;br /&gt;Ознакомиться с материалами к собранию кредиторов Вы можете с 13.02.2017г. по адресу: г. Иркутск, ул. Кожова, 14/3, 4 этаж, офис 3, ежедневно в рабочие дни с 10 часов 00 минут до 17 часов 00 минут  по предварительной договоренности по тел. 89021709958 либо отправив заявку в электронной форме по адресу tvb_i-k@mail.ru&lt;br /&gt;</Text>
      <MeetingSite>г. Иркутск, ул. Кожова, 14/3, 4 этаж, оф. 3</MeetingSite>
      <MeetingDate>2017-02-21T11:00:00</MeetingDate>
      <MeetingForm>Очная</MeetingForm>
      <RegistrationSite>г. Иркутск, ул. Кожова, 14/3, 4 этаж, оф. 3</RegistrationSite>
      <RegistrationDate>2017-02-21</RegistrationDate>
      <RegistrationTimeBegin>10:40:00.0000000+03:00</RegistrationTimeBegin>
      <RegistrationTimeEnd>10:55:00.0000000+03:00</RegistrationTimeEnd>
      <ExaminationSite>г. Иркутск, ул. Кожова, 14/3, 4 этаж, оф. 3</ExaminationSite>
      <ExaminationDate>2017-02-13</ExaminationDate>
      <Comment>ежедневно в рабочие дни с 10 часов 00 минут до 17 часов 00 минут  по предварительной договоренности по тел. 89021709958 либо отправив заявку в электронной форме по адресу tvb_i-k@mail.ru</Comment>
      <FuMailAddress>664003, г. Иркутск, а/я 281</FuMailAddress>
    </Meeting>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="93193" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ &quot;БАЙКАЛИНЖИНИРИНГ&quot;" ShortName="ООО &quot;БАЙКАЛИНЖИНИРИНГ&quot;" PostAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3" OKPO="" OGRN="1123850021426" LegalAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3">
      <INN>3812141040</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2017-02-06T14:44:58.723</PublishDate>
  <BankruptId>123123</BankruptId>
  <MessageGUID>15839E94F20EE1697AB41645D513E2C4</MessageGUID>
</MessageData>');
INSERT INTO mock_efrsb_message set `id_Message`= 15, `ArbitrManagerID`= 1, `BankruptId`=123123, `INN`= '3812141040', `OGRN`= '1123850021426', `PublishDate`= '2016-10-21T07:11:33', `MessageInfo_MessageType`= 'a', `Number`= '1371418', `MessageGUID`= 'F5BE1C6FE9DFBD79C984B6A68EDAB502', `efrsb_id`= '1371418', `Revision`=6
, `Body`= compress('<MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1371418</Id>
  <Number>1371418</Number>
  <CaseNumber>А19-12951/2016 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="14678" FirstName="Татьяна" MiddleName="Владимировна" LastName="Келене (Бровина)" INN="381108251427" RegistryNumber="10207">
      <OGRN />
      <Sro SroId="25">
        <SroName>НП "СГАУ" - Некоммерческое партнерство "Сибирская гильдия антикризисных управляющих"</SroName>
        <SroRegistryNumber>0009</SroRegistryNumber>
        <OGRN>1028600516735</OGRN>
        <INN>8601019434</INN>
        <LegalAddress>628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18</LegalAddress>
      </Sro>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="ArbitralDecree">
    <CourtDecision>
      <Text>Определением  Арбитражного суда Иркутской области, резолютивная часть которого объявлена 18.10.2016г., по делу № А19-12951/2016 в отношении ООО «Байкалинжиниринг» (ИНН 3812141040, ОГРН 1123850021426, адрес место нахождения: 664017, г. Иркутск, ул. Лермонтова, 341/3) введена процедура наблюдения, временным управляющим  утвержден Келене Татьяна Владимировна  (ИНН 381108251427, СНИЛС 107-061-654-23, адрес для корреспонденции: 664003, г. Иркутск, а/я 281), член СРО НП "Сибирская гильдия антикризисных управляющих" (ИНН 8601019434, ОГРН 1028600516735, адрес: 628001, ХМАО-Югра, г. Ханты-Мансийск, ул. Конева, д. 18). Рассмотрение отчета временного управляющего в суде назначено на 06.03.2017г.</Text>
      <DecisionType Name="Определение о введении наблюдения" Id="11" />
      <CourtDecree>
        <CourtId>71</CourtId>
        <CourtName>Арбитражный суд Иркутской области</CourtName>
        <FileNumber>А19-12951/2016 </FileNumber>
        <DecisionDate>2016-10-18</DecisionDate>
      </CourtDecree>
      <LossesFromArbitrManagerActionsAmount xsi:nil="true" />
      <CitizenNotReleasedFromResponsibility xsi:nil="true" />
      <DecisionMadeDueTorCancellationRestructuringPlan xsi:nil="true" />
    </CourtDecision>
  </MessageInfo>
  <BankruptInfo ClaimantTypeIsPledged="false" ClaimantTypeIsCreditOrg="false" BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="93193" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ &quot;БАЙКАЛИНЖИНИРИНГ&quot;" ShortName="ООО &quot;БАЙКАЛИНЖИНИРИНГ&quot;" PostAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3" OKPO="" OGRN="1123850021426" LegalAddress="ИРКУТСКАЯ, ИРКУТСК, ЛЕРМОНТОВА, 341/3">
      <INN>3812141040</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2016-10-21T07:11:33.697</PublishDate>
  <BankruptId>123123</BankruptId>
  <MessageGUID>F5BE1C6FE9DFBD79C984B6A68EDAB502</MessageGUID>
</MessageData>');
