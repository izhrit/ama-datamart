<? 

require_once '../../../assets/config.php';
require_once '../../../assets/helpers/log.php';

require_once '../../../assets/libs/tpl/tpl_by_sro_and_filename.php';

mb_internal_encoding("utf-8");

$current_time= '2021-02-03T10:00:00';

function ProcessArguments()
{
	global $argv, $tpl_name, $sro_tpl_folder, $params_file_path, $current_time;

	$count= count($argv);
	for ($i= 1; $i<$count; $i++)
	{
		$ap = explode('=',$argv[$i]);
		switch ($ap[0])
		{
			case '--tpl': $tpl_name= $ap[1]; break;
			case '--sro-tpl-folder': $sro_tpl_folder= $ap[1]; break;
			case '--params-file-path': $params_file_path= $ap[1]; break;
		}
	}
}

ProcessArguments();

function endsWith( $haystack, $needle )
{
	return substr( $haystack, -strlen($needle) ) === $needle;
}

$tpl_par= null;
if ($params_file_path && !endsWith($params_file_path,'no-params.json'))
{
	$params_json_txt= file_get_contents($params_file_path);
	$tpl_par= json_decode($params_json_txt);
	if (null==$tpl_par)
	{
		echo "read params file $params_file_path!\r\n";
		echo "---------\r\n";
		echo $params_json_txt;
		echo "\r\n---------\r\n";
		echo "json_decode returns null!";
	}
}

$sro_params_path= get_sro_params_path_for_sro_tpl_folder($sro_tpl_folder);
if (file_exists($sro_params_path))
{
	$sro_params_json_txt= file_get_contents($sro_params_path);
	$sro_params= json_decode($sro_params_json_txt);
	if (null!=$sro_params)
	{
		if (null==$tpl_par)
			$tpl_par= (object)array();
		$tpl_par->СРО= $sro_params;
	}
	else
	{
		echo "read sro params file $sro_params_json_txt!\r\n";
		echo "---------\r\n";
		echo $sro_params_json_txt;
		echo "\r\n---------\r\n";
		echo "json_decode returns null!";
	}
}

if (null==$tpl_par)
	$tpl_par= (object)array();
$tpl_par->Текущее_время= $current_time;

$tpl_path= get_tpl_path_for_sro_tpl_folder($sro_tpl_folder, $tpl_name);
if ($tpl_path)
{
	require_once $tpl_path;
}
else
{
	echo 'empty tpl:';
	echo $tpl_path;
}