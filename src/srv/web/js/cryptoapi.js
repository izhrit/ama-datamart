app.cryptoapi = {

	ChooseCertificateAnd: function (do_with_certificate)
	{
		var cert = $.capicom.getDialogBoxCertificates();
		do_with_certificate(cert);
	}

	, SignBase64: function (rawData, detached, cert)
	{
		return $.capicom.signBase64(rawData, detached, cert.thumbprint);
	}
};
