<?

function redirect($url, $statusCode = 303)
{
	header('Location: ' . $url, true, $statusCode);
	exit;
}

function bad_request($msg)
{
	require_once '../assets/config.php';
	require_once '../assets/helpers/log.php';
	write_to_log('bad info request:');
	write_to_log($msg);
	header("HTTP/1.1 400 Bad Request");
	exit;
}

function info_map()
{
	$url= 'https://yandex.ru/maps/?text=';
	if (isset($_GET['address']))
		$url.= $_GET['address'];
	redirect($url);
}

function info_cadastr_number()
{
	$url='https://rosreestr.net/kadastr/';
	if (isset($_GET['number']))
		$url.= str_replace(':', '-', $_GET['number']);
	redirect($url);
}

try
{
	if (!isset($_GET['action']))
	{
		bad_request('skipped parameter action');
	}
	else
	{
		$action= $_GET['action'];
		switch ($action)
		{
			case 'map': info_map(); break;
			case 'cadastr-number': info_cadastr_number(); break;
			default: bad_request("unknown action=$action"); break;
		}
	}
}
catch (Exception $exception)
{
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	header("HTTP/1.1 500 Internal Server Error");
}

