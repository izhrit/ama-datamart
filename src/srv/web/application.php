<?php
require_once '../assets/helpers/json.php';
require_once '../assets/config.php';
require_once '../assets/helpers/log.php';

session_start();
if (isset($_GET['start']))
	$_SESSION= array();
$from = isset($_SESSION['auth_info']) ? 'datamart':'application';
?>
<html>
	<head>
		<title>Требования о включении в реестр требований кредиторов</title>
		<meta http-equiv="X-UA-Compatible" content="IE=11" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/ico_ama.png">
		<script type="text/javascript" src="js/vendors/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>
		<script type="text/javascript" src="js/vendors/capicom.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui-new.css" /> 
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.custom.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min_new.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>
		
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/datamart.css" />
		<!-- вот это надо в extension ы! } -->

		<script type="text/javascript">
		app= { };
		function RegisterCpwFormsExtension(extension)
		{
			if ('ama'==extension.key)
			{
				var base_url= '<?= $datamart_bck_url ?>';
				var sel= 'body.cpw-ama > div.cpw-ama-application-main';
				var form_spec = extension.forms.application.CreateController({base_url:base_url, from:'<?= $from ?>'});
				form_spec.CreateNew(sel);
			}
		}
		window.onerror = function (message, source, lineno)
		{
			alert("Ошибка:" + message + "\n" +
					"файл:" + source + "\n" +
					"строка:" + lineno);
		}

		window.use_activex = '<?= $use_activex ?>';
		window.use_validate_certificate = '<?= $use_validate_certificate ?>';
		</script>

		<script type="text/javascript" src="js/ama-datamart.js?2020_11_27_1614"></script>
	</head>
	<body class="cpw-ama">
		<div class="cpw-ama-application-main">

			<div class="ui-placeholder">
				<div>
					<small>
						<?= date_format(date_create(),'Y-m-d H:i:s') ?><br/>
						<div style="text-align:right">формируется html-страница на сервере.</div><br/>
						Здесь на html-странице должна отобразиться<br/>
					</small>
					<p style="text-align:center;">

						Форма требования кредитора

					</p>
					<small>
						После загрузки и выполнения javascript.
						<br/><br/>
						<script>
							var t= new Date();
							var m= t.getMonth()+1;
							var d= t.getDate();
							if (d<10)
								d= '0' + d;
							if (m<10)
								m= '0' + m;
							document.write(t.getFullYear()+'-'+m+'-'+d+' '+t.toLocaleTimeString());
						</script>
						<div style="text-align:right">выполняется javascript в браузере.</div><br/>
					</small>
					<center><img src="img/loading-spinner.gif" /></center>
				</div>
			</div>

		</div>
	</body>
</html>