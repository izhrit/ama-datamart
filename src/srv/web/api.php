<?

require_once '../assets/config.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $trace_methods;
if ($trace_methods)
	require_once '../assets/helpers/log.php';

$possible_file_actions= array
(
	 'Auth'=> 'Auth'

	 ,'UploadProcedureInfo'=> 'ama/UploadProcedureInfo'
	 ,'GetSubLevelInformation'=> 'ama/GetSubLevelInformation'
	 ,'GetAnketaNP' => 'ama/GetAnketaNP'
	 ,'GetAnketaNPList' => 'ama/GetAnketaNPList'
	 ,'UsingProcedure' => 'ama/UsingProcedure'
	 ,'UsingPepro' => 'pepro/UsingPepro'

	 ,'CreateAssembly'=>'ama/CreateAssembly'
	 ,'DeleteAssembly'=>'ama/DeleteAssembly'

	 ,'GetChangedProcedures'=> 'ctb/GetChangedProcedures'
	 ,'DownloadProcedureInfo'=> 'ctb/DownloadProcedureInfo'

	 ,'UploadAnketaNP' => 'bankrotme/UploadAnketaNP'
	 ,'GetAUList' => 'bankrotme/GetAUList'

	 ,'RegistrateBankruptcyPetition' => 'contract/RegistrateBankruptcyPetition'

	 ,'GetProcedureStarts' => 'efrsb/GetProcedureStarts'

	 ,'AuthByCert'=>'partner/AuthByCert'

	 ,'GetWcalendar'=> 'ama/GetWcalendar'

	 ,'GetExposureRecipientProList'=> 'ama/GetExposureRecipientProList'
	 ,'StoreExposureAssets'=> 'ama/StoreExposureAssets'
	 ,'GetExposureAssetDigestsOrderedBy_ID_Object'=> 'ama/GetExposureAssetDigestsOrderedBy_ID_Object'
	 ,'StoreExposureDocument'=> 'ama/StoreExposureDocument'
	 ,'DeleteExposureDocuments'=> 'ama/DeleteExposureDocuments'
);

$action= null;
if (isset($_GET['action']))
{
	$action= $_GET['action'];
}
else if (isset($_SERVER['ORIG_PATH_INFO']))
{
	$path= $_SERVER['ORIG_PATH_INFO'];
	$pos= strrpos($path,'/');
	if (false != $pos)
	{
		$action= substr($path,$pos+1);
		if ('api.php'==$action)
			$action= null;
	}
}
else if (isset($_SERVER['PHP_SELF']))
{
	$path= $_SERVER['PHP_SELF'];
	$pos= strrpos($path,'/');
	if (false != $pos)
	{
		$action= substr($path,$pos+1);
		if ('api.php'==$action)
			$action= null;
	}
}
///////////////////////////////////////////////////////////////////////////////
if (null==$action)
{
	$title= 'REST методы API витрины данных ПАУ';
	$doc_link= 'docs/pau-datamart-api/index.html';
	require '../assets/views/action-undefined.php';
}
else
{
	global $trace_methods;

	if ($trace_methods)
		write_to_log('------'.$action.'--------------------------');

	if (!isset($possible_file_actions[$action]))
	{
		if ($trace_methods)
			write_to_log('   unknown action!');
		$title= 'REST методы API витрины данных ПАУ';
		$doc_link= 'docs/pau-datamart-api/index.html';
		require '../assets/views/action-undefined.php';
	}
	else
	{
		$subpath= $possible_file_actions[$action];
		if (''==$subpath)
			$subpath= $action;
		try
		{
			require_once '../assets/actions/api/'.$subpath.'.php';
		}
		catch (Exception $ex)
		{
			header("HTTP/1.1 500 Internal Server Error");
			require_once '../assets/helpers/log.php';
			$msg=  "During action $action unhandled exception occurred: " . get_class($ex) . "\r\n";
			$msg.= 'exception message: ' . $ex->getMessage() . "\r\n";
			$msg.= "_GET: \r\n";
			$msg.= print_r($_GET,true);
			$msg.= "\r\n!!!!!!\r\n";
			write_to_log($msg);
			exit;
		}
	}
}
