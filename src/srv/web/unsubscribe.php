<?
require_once '../assets/config.php';
require_once '../assets/helpers/validate.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $trace_methods;
if ($trace_methods)
	require_once '../assets/helpers/log.php';

///////////////////////////////////////////////////////////////////////////////
if (!isset($_GET['email']) || !isset($_GET['guid']))
{
	exit_not_found('Parameters missing!');
}

$title= isset($title) ? $title : 'Отписка от рассылки';
$email= $_GET['email'];
if ($trace_methods)
	write_to_log('   undefined action!');
?>

<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/ico_ama.png">
		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>
		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.custom.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>
		<!-- вот это надо в extension ы! } -->
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/datamart.css" />


		<title><?= $title ?></title>
		<script type="text/javascript">
		app= { };
		function RegisterCpwFormsExtension(extension)
		{
			if ('ama'==extension.key)
			{
				var base_url= '<?= $datamart_bck_url ?>';
				var sel= 'body.cpw-ama > div.cpw-unsubscribe-ui-content';
				var form_spec = extension.forms.unsubscribe.CreateController({base_url:base_url});
				form_spec.CreateNew(sel);
			}
		}
		window.onerror = function (message, source, lineno)
		{
			alert("Ошибка:" + message + "\n" +
					"файл:" + source + "\n" +
					"строка:" + lineno);
		}

		</script>
		<script type="text/javascript" src="js/ama-datamart.js?<?= time() ?>"></script>
	</head>
	<body class="cpw-ama" style="width:800px;margin:60px auto;text-align:left;">
		<div class="cpw-unsubscribe-ui-content">
			Здесь должна быть форма отписки от рассылки
		</div>
	</body>
</html>