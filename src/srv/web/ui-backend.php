<?
require_once '../assets/config.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $trace_methods;
if ($trace_methods)
	require_once '../assets/helpers/log.php';

$possible_file_actions= array
(
	  'login'=> 'login' // tested: +(АУ,Клиент,Наблюдатель)
	, 'logout'=> 'logout' // tested: +(АУ,Клиент,Наблюдатель)
	, 'test-time'=> 'test_time'

	, 'procedure.jqgrid'=> 'procedure/procedure_jqgrid'   // tested: +(Клиент,АУ,Наблюдатель)
	, 'procedure.select2'=> 'procedure/procedure_select2' // tested: +(Клиент,АУ,Наблюдатель)
	, 'proc-viewers.ru'=> 'procedure/proc_viewers_ru' // tested: r(+(АУ)) u(+(АУ))
	, 'procedure.ru'=> 'procedure/procedure_ru'
	, 'procedure.info'=> 'procedure/procedure_info' // tested: +(Клиент,АУ,Наблюдатель)
	, 'procedure.dbinfo'=> 'procedure/procedure_dbinfo' // tested: +(Клиент,АУ,Наблюдатель)
	, 'procedure.find.select2'=> 'procedure/procedure_find_select2'
	, 'procedure.msg.prep'=> 'procedure/procedure_msg_prep'
	, 'procedure.params.preview'=> 'procedure/procedure_params_preview'

	, 'viewer.jqgrid'=> 'viewer/viewer_jqgrid' // tested: +(АУ)
	, 'viewer.crud'=> 'viewer/viewer_crud' // tested: c(+(АУ)) u(+(АУ,Наблюдатель)) r(+(АУ)) d(+(АУ))
	, 'viewer.select2'=> 'viewer/viewer_select2' // tested: +(АУ)
	, 'viewer.register'=> 'viewer/viewer_register'
	, 'viewer.info'=> 'viewer/viewer_info'
	, 'viewer.outcome.jqgrid'=> 'viewer/viewer_outcome_jqgrid'
	, 'viewer.photo'=> 'viewer/viewer_photo'

	, 'leaks.jqgrid'=> 'viewer/leaks_jqgrid'

	, 'sro.props.crud'=> 'sro/sro_props_crud'
	, 'sro.document.ru'=> 'sro/sro_document_ru'
	, 'sro.document.dwnld'=> 'sro/sro_document_dwnld'

	, 'leaks.jqgrid'=> 'viewer/leaks_jqgrid'

	, 'sro.props.crud'=> 'sro/sro_props_crud'
	, 'sro.document.ru'=> 'sro/sro_document_ru'
	, 'sro.document.dwnld'=> 'sro/sro_document_dwnld'

	, 'leaks.jqgrid'=> 'viewer/leaks_jqgrid'

	, 'sro.props.crud'=> 'sro/sro_props_crud'
	, 'sro.document.ru'=> 'sro/sro_document_ru'
	, 'sro.document.dwnld'=> 'sro/sro_document_dwnld'

	, 'leaks.jqgrid'=> 'viewer/leaks_jqgrid'

	, 'sro.props.crud'=> 'sro/sro_props_crud'
	, 'sro.document.ru'=> 'sro/sro_document_ru'
	, 'sro.document.dwnld'=> 'sro/sro_document_dwnld'

	, 'manager.jqgrid'=> 'manager/manager_jqgrid' // tested: +(Клиент)
	, 'manager.income.jqgrid'=> 'manager/manager_income_jqgrid'
	, 'manager.ru'=> 'manager/manager_ru' // tested: r(+(Клиент,АУ)) u(+(Клиент,АУ))
	, 'manager.sro.ru'=> 'manager/manager_sro_ru'
	, 'manager.password'=> 'manager/manager_password' // tested: b(+(Клиент,АУ)) c(+(Клиент,АУ))
	, 'manager.select2'=> 'manager/manager_select2'
	, 'manager.cert.login'=> 'manager/manager_cert_login'
	, 'manager.document.ru'=> 'manager/manager_document_ru'
	, 'manager.document.dwnld'=> 'manager/manager_document_dwnld'
	, 'manager.notifications.jqgrid'=> 'manager/manager_notifications_jqgrid'
	, 'manager.notifications.info'=> 'manager/manager_notifications_info'
	, 'manager.notifications.mime.dwnld'=> 'manager/manager_notifications_mime_dwnld'
	, 'manager.info'=> 'manager/manager_info'
	, 'manager.contract'=> 'manager/manager_contract'

	, 'efrsb-manager.select2'=> 'manager/efrsb_manager_select2'

	, 'efrsb.jqgrid'=> 'efrsb/efrsb_jqgrid'
	, 'efrsb.debtor.select2'=> 'efrsb/efrsb_debtor_select2'

	, 'meeting.nearest'=> 'meeting/select2/meeting_nearest'
	, 'meeting.select2'=> 'meeting/select2/meeting_select2'
	, 'meeting.crud'=> 'meeting/meeting_crud'
	, 'meeting.vote'=> 'meeting/meeting_vote'
	, 'meeting.document'=> 'meeting/documents/meeting_document'
	, 'meeting.download'=> 'meeting/documents/meeting_download'
	, 'meeting.state'=> 'meeting/meeting_state'
	, 'meeting.cabinetcc'=> 'meeting/meeting_cabinetcc'
	, 'meeting.fill-new'=> 'meeting/meeting_fill_new'
	, 'meeting.bulletins'=> 'meeting/documents/meeting_bulletins'
	, 'meeting.protocol'=> 'meeting/documents/meeting_protocol'
	, 'meeting.agreement'=> 'meeting/documents/meeting_agreement'

	, 'award.nominees'=> 'award/award_nominees'
	, 'award.nominee'=> 'award/award_nominee'
	, 'award.vote'=> 'award/award_vote'
	, 'award.managers'=> 'award/award_managers'
	, 'award.managers_server' => 'award/award_managers_server'
	, 'award.manager'=> 'award/award_manager'
	, 'award.votes'=> 'award/award_votes'
	, 'award.results'=> 'award/award_results'
	, 'award.jqgrid'=> 'award/award_votes_jqgrid'
	, 'award.login'=> 'award/award_login'

	, 'request.jqgrid'=> 'request/request_jqgrid'
	, 'request.crud'=> 'request/request_crud'
	, 'request.resolve'=> 'request/request_resolve'
	, 'request.fields'=> 'request/request_fields'

	, 'sl.jqgrid'=> 'handbook/sl_jqgrid'
	, 'sl.crud'=> 'handbook/sl_crud'
	, 'rosreestr.crud'=> 'handbook/rosreestr/rosreestr_crud'
	, 'rosreestr.jqgrid'=> 'handbook/rosreestr/rosreestr_jqgrid'
	, 'wcalendar.crud'=> 'handbook/wcalendar_crud'
	, 'wcalendar.list'=> 'handbook/wcalendar_list'
	, 'wcalendar.jqgrid'=> 'handbook/wcalendar_jqgrid'
	, 'admin.lastresult.jqgrid'=> 'handbook/admin_lastresult_jqgrid'
	

	, 'court.jqgrid'=> 'handbook/court/court_jqgrid'
	, 'court.crud'=> 'handbook/court/court_crud'
	, 'court.select2'=> 'handbook/court/court_select2'
	, 'court.by-prefix'=> 'handbook/court/court_by_prefix'

	, 'api-GetSubLevelInformation' => 'handbook/sl_calc'

	, 'news.jqgrid'=> 'events_news/news_jqgrid'

	, 'get_news_by_id'=> 'events_news/news_by_id'

	, 'schedule.events'=> 'events_news/schedule_info'
	, 'push.crud'=> 'events_news/push_crud'

	, 'custom_query.list'=> 'custom_query/custom_query_list'
	, 'custom_query.execute'=> 'custom_query/custom_query_execute'

	, 'push.test'=> 'events_news/push_test'
	, 'push.unsubscribe'=> 'events_news/push_unsubscribe'
	, 'push.jqgrid'=> 'events_news/push_jqgrid'
	, 'push.get'=> 'events_news/push_get'
	, 'push.update-token'=> 'events_news/push_update_token'
	, 'push.log_by_time'=> 'events_news/push_log_by_time'

	, 'start.jqgrid'=> 'start/start_jqgrid'
	, 'start.crud'=> 'start/start_crud'
	, 'start.check'=> 'start/start_check'
	, 'start.agree'=> 'start/start_agree'
	, 'start.consent_doc'=> 'start/consent_doc'
	, 'start.new'=> 'start/start_new'
	, 'start.request'=> 'start/start_request'

	, 'mrequest.jqgrid'=> 'mrequest/mrequest_jqgrid'
	, 'mrequest.crud'=> 'mrequest/mrequest_crud'
	, 'mrequest.select2'=> 'mrequest/mrequest_select2'
	, 'mrequest.typeahead'=> 'mrequest/procedurestart_typeahead'
	, 'mrequest.dwnl_doc'=> 'mrequest/mrequest_dwnld_doc'
	, 'mrequest.manager.select2'=> 'mrequest/manager_select2'

	, 'consent.jqgrid'=> 'consent/consent_jqgrid'
	, 'consent.crud'=> 'consent/consent_crud'
	, 'consent.check'=> 'consent/consent_check'

	, 'job-monitor'=> 'job-monitor'

	, 'fa.upload-deals' => 'fa-api/fa_upload-deals'
	, 'fa.debtor-managers' => 'fa-api/fa_debtor-managers'
	, 'fa.using' => 'fa-api/fa_using'

	, 'email.jqgrid' => 'email/email_jqgrid'
	, 'email.get' => 'email/email_get'
	, 'email.download' => 'email/email_download'

	, 'access-log.jqgrid' => 'access_log/access_log_jqgrid'
	, 'access-log.get' => 'access_log/access_log_get'
	, 'access-log.explain' => 'access_log/access_log_explain'

	, 'application.debtor'=> 'application/prepare/debtor'
	, 'application.creditor.select2'=> 'application/prepare/creditor_select2'
	, 'application.file'=> 'application/prepare/file'
	, 'application.befor-view-document'=> 'application/preview/befor_view_document'
	, 'application.view-document'=> 'application/preview/view_document'
	, 'application.befor-load-document'=> 'application/preview/befor_load_document'
	, 'application.send.mail'=> 'application/confirmation/send_mail'
	, 'application.confirmation'=> 'application/confirmation/confirmation'
	, 'application.load-document'=> 'application/preview/load_document'
	, 'application.save'=> 'application/preview/save'

	, 'google.calendar.control'=> 'google_calendar/google_calendar_control'
	, 'google.calendar.update.token'=> 'google_calendar/google_calendar_update_token'

	, 'procedures.filter.ru'=> 'procedures_filter/procedures_filter_ru'

	, 'debtor.login'=> 'debtor_login'
	, 'debtor.jqgrid'=> 'debtor/debtor_jqgrid'
	, 'debtor.crud'=> 'debtor/debtor_crud'
	, 'debtor.address_typeahead'=> 'debtor/debtor_address_typeahead'
	, 'debtor.region_typeahead'=> 'debtor/debtor_region_typeahead'
	, 'debtor.select2'=> 'debtor/debtor_select2'
	, 'debtor.params'=> 'debtor/debtor_params'
	, 'debtor.docs.jqgrid'=> 'debtor/docs/debtor_docs_jqgrid'
	, 'debtor.docs.page.crud'=> 'debtor/docs/debtor_docs_page_crud'
	, 'debtor.docs.status'=> 'debtor/docs/debtor_docs_status'
	, 'debtor.docs.status.crud'=> 'debtor/docs/debtor_docs_status_crud'
	, 'debtor.docs.crud'=> 'debtor/docs/debtor_docs_crud'
	, 'debtor.docs.doc'=> 'debtor/docs/debtor_docs_doc'
	, 'debtor.docs.dwnld'=> 'debtor/docs/debtor_docs_dwnld'
	, 'debtor.docs.default_doc' => 'debtor/docs/debtor_docs_default_doc'
	, 'debtor.docs.select2' => 'debtor/docs/debtor_docs_select2'
	, 'debtor.send.cabinet_link' => 'debtor/debtor_send_cabinet_link'
	, 'debtor.cabinet.docs' => 'debtor/cabinet/debtor_docs'
	, 'debtor.cabinet.debtor_info'=> 'debtor/cabinet/debtor_info'
	, 'debtor.cabinet.escort_info'=> 'debtor/cabinet/debtor_escort_info'
	, 'debtor.cabinet.company_info'=> 'debtor/cabinet/debtor_company_info'
	, 'debtor.upload.bankruptcy.docs'=> 'debtor/docs/upload_default_bankruptcy_docs/upload_default_bankruptcy_docs'

	, 'asset.jqgrid'=> 'asset/asset_jqgrid'
	, 'asset.r'=> 'asset/asset_r'
	, 'asset.manager.select2'=> 'asset/asset_manager_select2'
	, 'asset.debtor.select2'=> 'asset/asset_debtor_select2'
	, 'asset.category.select2'=> 'asset/asset_category_select2'
	, 'asset.attachment'=> 'asset/asset_attachment'
);

///////////////////////////////////////////////////////////////////////////////
if (!isset($_GET['action']))
{
	require '../assets/views/action-undefined.php';
}
else
{
	$action= $_GET['action'];

	global $trace_methods;
	if ($trace_methods)
		write_to_log('------'.$action.'--------------------------');

	if (!isset($possible_file_actions[$action]))
	{
		require_once '../assets/helpers/log.php';
		require_once '../assets/helpers/validate.php';
		exit_bad_request('unknown action!');
	}
	else
	{
		$subpath= $possible_file_actions[$action];
		if (''==$subpath)
			$subpath= $action;
		try
		{
			require_once '../assets/actions/backend/'.$subpath.'.php';
		}
		catch (Exception $exception)
		{
			require_once '../assets/helpers/log.php';
			require_once '../assets/helpers/validate.php';
			write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
			exit_internal_server_error('Unhandled exception!');
		}
	}
}
