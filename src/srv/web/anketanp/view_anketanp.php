<?

$main_entry_name= 'anketanp.xml';

require_once '../../assets/config.php';
require_once '../../assets/helpers/log.php';
require_once '../../assets/libs/income.php';

require_once '../../assets/helpers/codec.xml.php';
require_once '../../assets/helpers/json.php';

$income_raw= get_income_xml_file($_GET['id_MData'],$main_entry_name);

if (0===mb_strpos($income_raw,'не удалось'))
	exit_bad_request($income);

$income= str_replace("\r\n","\n",$income_raw);
$income= str_replace("\n","\r\n",$income);
$income= str_replace("\r\n","\\r\\n",$income);
$income= str_replace("\"","\\\"",$income);

?>
<html xml:lang="en" lang="ru">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=10" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<script type="text/javascript" src="../js/vendors/jquery/jquery.js"></script>

	<script type="text/javascript">
		function RegisterCpwFormsExtension(extension)
		{
			if ('bfl' === extension.key)
			{
				var controller = extension.forms.anketa_view.CreateController();
				var content= "<?= $income ?>";
				controller.SetFormContent(content);
				controller.Edit('#bfl-anketanp-content');
			}
		}
	</script>

	<script type="text/javascript" src="js/ama-datamart-bfl.js"></script>
</head>
<body>
	<div id="bfl-anketanp-content">Здесь должна отображаться анкета на банкротство гражданина!</div>

	<h1>
			Но страница пока в разработке..
	</h1>
	<? if (0===mb_strpos($income_raw,'не удалось')) : ?>
		<?= $income_raw ?>
	<? else : ?>
		Необработанное представление:
		<hr/>
		<pre><?= prepare_xml_for_pre($income_raw) ?></pre>
		<hr/>
	<? endif; ?>
</body>
</html>

