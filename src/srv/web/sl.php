<?

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/validate.php';

try
{
	if (!isset($_GET['id']) || !isset($_GET['date']) || !isset($_GET['region']))
	{
		exit_bad_request('skipped parameter: id or date or region');
	}
	else
	{
		$ids= $_GET['id'];
		if (''==$ids)
			exit_bad_request('empty BankruptId argument!');
		if ($ids!=preg_replace("/[^0-9,]/", "", $ids))
			exit_bad_request('BankruptId argument can contain only digits and commas!');

		$txt_query= " select
			Reglament_Title Title
			, Reglament_Date Date
			, Reglament_Url Url
			from sub_level
			where id_Sub_level in ($ids);
		";
		$rows = execute_query($txt_query,array());

		$html="<div style=\"width:800px;margin:0 auto; font-size:large;\"><p>Расчёт прожиточного минимума для региона ".htmlentities($_GET['region'],ENT_NOQUOTES,"UTF-8")
			."<br> на ".htmlentities(date("d.m.Y", strtotime($_GET['date'])),ENT_NOQUOTES,"UTF-8")." определяется на основании нормативно правовых актов:</p>";
		if($rows!=0)
		{
			$html.="<ol>";
			foreach($rows as $row)
			{
				if($row->Title)
				{
					$html.=("
					<li><a target=\"_blank\" href=\"".$row->Url."\">".$row->Title." от ".$row->Date)."</a></li>";
				}
			}
			$html.="
			</ol></div>";
		}
		header('Content-Type: text/html; charset=UTF-8');
		echo $html;
	}
}
catch (Exception $exception)
{
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	header("HTTP/1.1 500 Internal Server Error");
}

