<?php
require_once '../assets/config.php';
?>

<html>
	<head>
		<title>Премия антикризисный управляющий 2019</title>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/award.png">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/award.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>
		<script type="text/javascript" src="js/vendors/capicom.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>
		<!-- вот это надо в extension ы! } -->

		<script type="text/javascript">
		app= {
			users:[
				""
				<? 
					global $award_admin_users;
					foreach ($award_admin_users as $u)
					{ 
				?>,"<?= $u->login ?>"<?	} ?>
			]
		};
		function RegisterCpwFormsExtension(extension)
		{
			if ('ama'==extension.key)
			{
				var base_url= '<?= $datamart_bck_url ?>';
				var sel= 'body.cpw-ama > div.cpw-award-ui-content';
				var form_spec = extension.forms.award_results.CreateController({base_url:base_url});
				form_spec.CreateNew(sel);
			}
		}
		window.onerror = function (message, source, lineno)
		{
			alert("Ошибка:" + message + "\n" +
					"файл:" + source + "\n" +
					"строка:" + lineno);
		}

		</script>

		<script type="text/javascript" src="js/ama-datamart.js?<?= time() ?>"></script>
	</head>
	<body class="cpw-ama">

		<div class="cpw-award-ui-content">
			Здесь должна быть форма с результатами голосования
		</div>

	</body>
</html>