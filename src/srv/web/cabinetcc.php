<?
require_once '../assets/config.php';
require_once '../assets/actions/backend/login_cabinetcc.php';
?>
<html>
	<head>
		<title>Кабинет голосования комитета кредиторов на витрине данных ПАУ</title>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/ico_ama.png">

		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.custom.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>

		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/datamart.css" />
		<!-- вот это надо в extension ы! } -->

		<script type="text/javascript">
		app= {
			a_href_attrs: function(url)
			{
				return 'OpenDatamartUrl' in window.external 
				? 'href="#" onclick="window.external.OpenDatamartUrl(\'/' + url + '?action=\')"' 
				: 'href="' + url.replace('/','\\') + '"';
			}
			, window_open: function(url, name, params)
			{
				if ('OpenDatamartUrl' in window.external)
					window.external.OpenDatamartUrl(encodeURI('/'+url));
				else
					window.open(encodeURI(url), name, params);
			}
		 };
		function RegisterCpwFormsExtension(extension)
		{
			if ('ama'==extension.key)
			{
				var base_url= '<?= $datamart_bck_url ?>';
				var sel= 'body.cpw-ama > div.cpw-datamart-ui-content';
				var form_spec = extension.forms.cabinetcc.CreateController({base_url:base_url});
				var initial_cabinetcc_model= <?= json_encode($cabinetcc_model) ?>;
				form_spec.SetFormContent(initial_cabinetcc_model);
				form_spec.Edit(sel);
			}
		}
		window.onerror = function (message, source, lineno)
		{
			alert("Ошибка:" + message + "\n" +
					"файл:" + source + "\n" +
					"строка:" + lineno);
		}
		</script>

		<script type="text/javascript" src="js/ama-datamart.js?<?= time() ?>"></script>
	</head>

	<body class="cpw-ama">
		<div class="cpw-datamart-ui-content">
			Здесь должна форма кабинета голосования комитета кредиторов
		</div>
	</body>
</html>