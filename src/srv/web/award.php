<?php

require_once '../assets/actions/backend/award/award_managers_server.php';
require_once '../assets/helpers/json.php';

global $test_settings;
if (isset($test_settings) && isset($test_settings->use_test_time) && true==$test_settings->use_test_time && isset($_GET['use-test-time']))
{
	require_once '../assets/helpers/time.php';
	session_start();
	safe_store_test_time();
}

$manager_regnum = !isset($_GET["manager_regnum"]) ? null : $_GET["manager_regnum"];
$managers = !isset($_GET["license_id"]) ? array() : GetManagers($_GET["license_id"], $manager_regnum);

?>

<html>
	<head>
		<title>Премия антикризисный управляющий 2021</title>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/award.png">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/award.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>
		<script type="text/javascript" src="js/vendors/capicom.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>
		<!-- вот это надо в extension ы! } -->

<?
$blocked= true;
if (isset($_GET['blocked']))
	$blocked= true;
if (isset($_GET['unblocked']))
	$blocked= false;
?>

		<script type="text/javascript">
		app= { };
		function RegisterCpwFormsExtension(extension)
		{
			if ('ama'==extension.key)
			{
<? if (!$blocked) : ?>
				var base_url= '<?= $datamart_bck_url ?>';
				var sel= 'body.cpw-ama > div.cpw-award-ui-content';
				var form_spec = extension.forms.award.CreateController({base_url:base_url});
				var managers=<?= nice_json_encode($managers) ?>;
				form_spec.SetFormContent(managers);
				form_spec.CreateNew(sel);
<? endif; ?>
			}
		}
		window.onerror = function (message, source, lineno)
		{
			alert("Ошибка:" + message + "\n" +
					"файл:" + source + "\n" +
					"строка:" + lineno);
		}

		window.use_activex = '<?= $use_activex ?>';
		window.use_validate_certificate = '<?= $use_validate_certificate ?>';
		</script>

		<script type="text/javascript" src="js/ama-datamart.js?<?= time() ?>"></script>
	</head>
	<body class="cpw-ama">

		<div class="cpw-award-ui-content">
<? if ($blocked) : ?>
			<br/>
			<br/>
			<br/>
			<H1><center>Голосование в номинации <br/>"Антикризисный управляющий 2021 года" <br/><span>завершено.</span></center></H1>
<? endif; ?>
		</div>

	</body>
</html>