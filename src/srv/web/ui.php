<?
require_once '../assets/config.php';
global $test_settings;
if (isset($test_settings) && isset($test_settings->use_test_time) && true==$test_settings->use_test_time && isset($_GET['use-test-time']))
{
	require_once '../assets/helpers/time.php';
	session_start();
	safe_store_test_time();
}

if (isset($_COOKIE[session_name()]) && !isset($_GET['start']))
{
	global $_SESSION, $auth_info;
	session_start();
	if (isset($_SESSION['auth_info']))
		$auth_info= $_SESSION['auth_info'];
}
global $auth_info;
if (isset($_GET['auth']) || isset($_GET['fauth']))
	require_once '../assets/actions/backend/auto_login.php';

global $start_with_section;
$start_with_section= 'false';
if (isset($_GET['section']))
{
	if (!isset($autologin_error_text))
	{
		$start_with_section= $_GET['section'];
	}
	else
	{
		$start_with_section= isset($_GET['fauth']) 
			? "cannot_be_opened_from_fa"
			: "cannot_be_opened_from_ama";
	}
}

if (isset($_COOKIE['email']) && isset($_COOKIE['Confirmation_token']) && isset($_GET['token']))
{
	require_once '../assets/actions/backend/application/dm_login.php';
}
?>
<html>
	<head>
		<title>Витрина данных ПАУ</title>
		<meta http-equiv="X-UA-Compatible" content="IE=11" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/pau-logo<?= (!isset($test_mode) || true!=$test_mode) ? '' : '-test' ?>.png" />

		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>
		<script type="text/javascript" src="js/vendors/capicom.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.custom.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>

		<script type="text/javascript" src="js/vendors/ofi.min.js"></script>

		<script>
		if("<?=isset($_GET['enable_viewers2'])?true:false?>"||'function'!=typeof (Intl.NumberFormat))
		{
			$.getScript('js/vendors/polyfill.js');
		}
		</script>  
		<link rel="stylesheet" type="text/css" href="css/vendors/fullcalendar/main.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/fullcalendar/tooltip.css" />
		<script type="text/javascript" src="js/vendors/fullcalendar/popper.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/tooltip.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/main.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/locales/ru.js"></script>
		<!-- вот это надо в extension ы! } -->

		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/datamart.css" />

		<script type="text/javascript" src="js/ui-fixes.js"></script>

		<script type="text/javascript">
		window.onerror = function (message, source, lineno)
		{
			var msg = "Ошибка: <b>" + message + "</b><br/><br/>\n" +
				"файл: <b>" + source + "</b><br/>\n" +
				"строка: <b>" + lineno + '</b>';
			var div = $("#cpw-unhandled-error-message-form");
			div.html(msg);
			div.dialog({
				modal: true,
				width:800,height:"auto",
				buttons: { Ok: function () { $(this).dialog("close"); } }
			});
		}
		<? global $autologin_error_text, $test_mode; ?>
		app= { 
			disable_viewers2:<?= isset($_GET['enable_viewers2']) ? 'false' : 'true' ?>
			,disable_viewers3:<?= isset($_GET['enable_viewers3']) ? 'false' : 'true' ?>
			, disable_viewers_registration:<?= (isset($_GET['enable_viewers2']) || (isset($enable_viewer_registration) && true==$enable_viewer_registration)) ? 'false' : 'true' ?>
			, open_section: "<?= $start_with_section ?>"
			<? if (isset($_GET['download'])&&isset($_GET['id_MData'])) : ?>
				, download_MData: "<?= $_GET['id_MData'] ?>"
			<? endif; ?>
			<? if (isset($_GET['debtor_inn'])) : ?>
				, debtor_inn: "<?= $_GET['debtor_inn'] ?>"
			<? endif; ?>
			<? if (isset($_GET['region'])) : ?>
				, region: "<?= $_GET['region'] ?>"
			<? endif; ?>
			,base_application_url:"<?= $datamart_application_url ?>"
			<? if (isset($test_mode) && true==$test_mode) : ?>
			, test_mode: true
			<? endif; ?>
			, a_href_attrs: function(url)
			{
				return 'href="' + url.replace('/','\\') + '"';
			}
			,window_open: function(url, name, params)
			{
				window.open(encodeURI(url), name, params);
			}
		};
		function RegisterCpwFormsExtension(extension)
		{
			if ('ama'==extension.key)
			{
				var base_url= '<?= $datamart_bck_url ?>';
				var sel= 'body.cpw-ama > div.cpw-datamart-ui-content';
				var form_spec = extension.forms.datamart.CreateController({base_url:base_url});
				form_spec.CreateNew(sel);
<? if (isset($auth_info) && null!=$auth_info) : ?>
<?
	$category= $auth_info->category;
	$id= null;
	switch ($category)
	{
		case 'manager': $id= $auth_info->id_Manager; break;
		case 'customer': $id= $auth_info->id_Contract; break;
		case 'viewer': $id= $auth_info->id_MUser; break;
		case 'sro': $id= $auth_info->id_SRO; break;
	}
?>
				baseUrl = window.location.href.split("?")[0];
				window.history.pushState('name', '', baseUrl);
<? if (null!=$id) : ?>
				form_spec.AutoLogin('<?= $category ?>','<?= $id ?>');
<? endif; ?>
<? endif; ?>
			}
		}
		</script>
		<script type="text/javascript" src="js/cryptoapi.js"></script>
		<script type="text/javascript" src="js/ama-datamart.js?2022_03_10_1625"></script>
	</head>

	<body class="cpw-ama">
		<div id="cpw-unhandled-error-message-form" title="Необработанная ошибка!"></div>
		<div class="cpw-datamart-ui-content">

			<div class="ui-placeholder">
				<div>
					<small>
						<?= date_format(date_create(),'Y-m-d H:i:s') ?><br/>
						<div style="text-align:right">формируется html-страница на сервере.</div><br/>
						Здесь на html-странице должна отобразиться<br/>
					</small>
					<p style="text-align:center;">

						Форма входа на витрину ПАУ

					</p>
					<small>
						После загрузки и выполнения javascript.
						<br/><br/>
						<script>
							var t= new Date();
							var m= t.getMonth()+1;
							var d= t.getDate();
							if (d<10)
								d= '0' + d;
							if (m<10)
								m= '0' + m;
							document.write(t.getFullYear()+'-'+m+'-'+d+' '+t.toLocaleTimeString());
						</script>
						<div style="text-align:right">выполняется javascript в браузере.</div><br/>
					</small>
					<center><img src="img/loading-spinner.gif" /></center>
				</div>
			</div>

		</div>

<? global $autologin_error_text, $test_settings; ?>
<? if (isset($autologin_error_text) && isset($test_settings) && isset($test_settings->show_autologin_errors) && true==$test_settings->show_autologin_errors) : ?>
		<div style="text-align:center;">
			<?= $autologin_error_text ?>
		</div>
<? endif; ?>

		<div class="dm-footer">
			<div>
				Доступ к витрине может быть удобнее через специальные
				<b>бесплатные</b> 
				мобильные приложения "ПАУ".<br/>
				<!--
				Кроме того что доступно через веб, они помогают фотографировать документы и конкурсную массу.<br/>
				-->
				Приложения доступны для смартфонов под управлением 
				<a target="_blank" href="https://russianit.ru/mobile-ama/">iOS</a>
				и 
				<a target="_blank" href="https://russianit.ru/mobile-ama/">Android</a>.<br/>
				При возникновении вопросов, замечаний или проблем в работе с витриной, 
				обращайтесь в <a target="_blank" href="https://russianit.ru/support/">службу поддержки</a>.
			</div>
		</div>
	</body>
</html>
