<?
require_once '../assets/config.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/db.php';

write_to_log('--------------------- for-orpau.php --------------');

CheckMandatoryGET('widget');

$widget_names= array(
	'pdatas'=>'datamart_orpau'
	,'news'=>'news_orpau'
	,'schedule'=>'schedule_orpau'
);

$controller_name= CheckMandatoryGET_get_variant_by_name('widget',$widget_names);

$id_ManagerCertAuth= CheckMandatoryGET_id('id_ManagerCertAuth');
CheckMandatoryGET('auth');
$auth= $_GET['auth'];

$txt_query= 'select
	m.firstName firstName
	, m.lastName lastName
	, m.middleName middleName
	, m.efrsbNumber efrsbNumber
	, m.INN INN
	, m.id_Manager id_Manager
	, m.id_Contract id_Contract
	, m.id_Contract id_Contract
	, c.ContractNumber ContractNumber
	, m.ArbitrManagerID
	, m.HasIncoming

from ManagerCertAuth mca
inner join Manager m on m.id_Manager=mca.id_Manager
inner join Contract c on c.id_Contract=m.id_Contract
where mca.id_ManagerCertAuth=? and mca.Auth=?;';

$rows= execute_query($txt_query,array('ss',$id_ManagerCertAuth,$auth));
$count_rows= count($rows);
if (1!=$count_rows)
	exit_bad_request("found $count_rows ManagerCertAuth with id_ManagerCertAuth=$id_ManagerCertAuth and Auth=$auth");

global $_SESSION, $auth_info;
session_start();
$auth_info= $rows[0];
$auth_info->category= 'manager';
$_SESSION['auth_info']= $auth_info;

global $test_settings;
if (isset($test_settings) && isset($test_settings->use_test_time) && true==$test_settings->use_test_time && isset($_GET['use-test-time']))
{
	require_once '../assets/helpers/time.php';
	safe_store_test_time();
}

?>
<html>
	<head>
		<title>Витрина данных ПАУ</title>
		<meta http-equiv="X-UA-Compatible" content="IE=11" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/pau-logo.png">
		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.custom.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>

		<script>
		if("<?=isset($_GET['enable_viewers2'])?true:false?>"||'function'!=typeof (Intl.NumberFormat))
		{
			$.getScript('js/vendors/polyfill.js');
		}
		</script>  
		<link rel="stylesheet" type="text/css" href="css/vendors/fullcalendar/main.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/fullcalendar/tooltip.css" />
		<script type="text/javascript" src="js/vendors/fullcalendar/popper.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/tooltip.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/main.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/locales/ru.js"></script>
		<!-- вот это надо в extension ы! } -->

		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/datamart.css" />
		<link rel="stylesheet" type="text/css" href="css/for-orpau.css" />

		<script type="text/javascript" src="js/ui-fixes.js"></script>

		<script type="text/javascript">
		window.onerror = function (message, source, lineno)
		{
			var msg = "Ошибка: <b>" + message + "</b><br/><br/>\n" +
				"файл: <b>" + source + "</b><br/>\n" +
				"строка: <b>" + lineno + '</b>';
			var div = $("#cpw-unhandled-error-message-form");
			div.html(msg);
			div.dialog({
				modal: true,
				width:800,height:"auto",
				buttons: { Ok: function () { $(this).dialog("close"); } }
			});
		}
		<? global $autologin_error_text; ?>
		app= { 
			disable_viewers2:<?= isset($_GET['enable_viewers2']) ? 'false' : 'true' ?>
		};
		function RegisterCpwFormsExtension(extension)
		{
			if ('ama'==extension.key)
			{
				var base_url= '<?= $datamart_bck_url ?>';
				var sel= 'body.cpw-ama > div.cpw-datamart-ui-content';
				var controller = extension.forms.<?= $controller_name ?>.CreateController({base_url:base_url});
				controller.SetFormContent(<?= json_encode($auth_info) ?>);
				controller.Edit(sel);
			}
		}
		</script>

		<script type="text/javascript" src="js/ama-datamart-orpau.js?2020_11_27_1614"></script>
	</head>

	<body class="cpw-ama">
		<div id="cpw-unhandled-error-message-form" title="Необработанная ошибка!"></div>
		<div class="cpw-datamart-ui-content">

			<div class="ui-placeholder">
				<div>
					<small>
						<?= date_format(date_create(),'Y-m-d H:i:s') ?><br/>
						<div style="text-align:right">формируется html-страница на сервере.</div><br/>
						Здесь на html-странице должна отобразиться<br/>
					</small>
					<p style="text-align:center;">

						Форма входа на витрину ПАУ

					</p>
					<small>
						После загрузки и выполнения javascript.
						<br/><br/>
						<script>
							var t= new Date();
							var m= t.getMonth()+1;
							var d= t.getDate();
							if (d<10)
								d= '0' + d;
							if (m<10)
								m= '0' + m;
							document.write(t.getFullYear()+'-'+m+'-'+d+' '+t.toLocaleTimeString());
						</script>
						<div style="text-align:right">выполняется javascript в браузере.</div><br/>
					</small>
					<center><img src="img/loading-spinner.gif" /></center>
				</div>
			</div>

		</div>

<? global $autologin_error_text, $test_settings; ?>
<? if (isset($autologin_error_text) && isset($test_settings) && isset($test_settings->show_autologin_errors) && true==$test_settings->show_autologin_errors) : ?>
		<div style="text-align:center;">
			<?= $autologin_error_text ?>
		</div>
<? endif; ?>

	</body>
</html>
