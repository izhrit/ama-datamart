<?

$title_first_line= 'Здесь должнен отображаться перечень сделок.';
$main_entry_name= 'deals.xml';

require_once '../../assets/config.php';
require_once '../../assets/helpers/log.php';
require_once '../../assets/libs/income.php';

require_once '../../assets/helpers/codec.xml.php';
require_once '../../assets/helpers/json.php';

require_once '../../assets/libs/codecs/deals.php';

$income_raw= get_income_xml_file($_GET['id_MData'],$main_entry_name);

$error_message= null;
$deals= null;

if (0===mb_strpos($income_raw,'не удалось'))
{
	write_to_log("view_deals income:$income_raw");
	$error_message= 'Не удалось извлечь пакет данных из базы данных!';
}
else
{
	$deals_codec= new Deals_codec();
	try
	{
		$deals= $deals_codec->Decode($income_raw);
		$debtor = get_debtor_by_idMData($_GET['id_MData']);
		$debtor_name = $debtor[0]->Name;
	}
	catch (Exception $ex)
	{
		write_to_log('Unhandled exception during parse of deals: ' . get_class($ex) . ' - ' . $ex->getMessage());
		write_to_log_named('deals to parse',$income_raw);
		$error_message= 'Не удалось прочитать xml из базы данных:'."\r\n".$income_raw;
	}
}
?>
<html xml:lang="en" lang="ru">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=10" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />

<? if (null==$error_message) : ?>

	<link rel="stylesheet" type="text/css" href="css/vendors/bootstrap/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="js/vendors/summernote/summernote.css" />
	<link rel="stylesheet" type="text/css" href="css/main-frame-style.css" />
	<link rel="stylesheet" type="text/css" href="css/deals-table-style.css" />
	<link rel="stylesheet" type="text/css" href="css/typeahead-style.css" />
	<link rel="stylesheet" type="text/css" href="css/deals-card-style.css" />

	<link rel="stylesheet" type="text/css" href="css/performance-form-style.css" />

	<link rel="stylesheet" type="text/css" href="css/conclusions-style.css" />
	<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

	<script type="text/javascript" src="js/vendors/jquery/jquery-2.1.4.js"></script>
	<script type="text/javascript" src="js/vendors/bootstrap/bootstrap.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
	<script type="text/javascript" src="js/vendors/validate/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/vendors/select2/select2.js"></script>

	<script type="text/javascript" src="js/vendors/jquery/typeahead.bundle.js"></script>
	<script type="text/javascript" src="js/vendors/summernote/summernote.min.js"></script>
	<script type="text/javascript" src="js/vendors/summernote/lang/summernote-ru-RU.js"></script>

	<script type="text/javascript" src="js/vendors/underscore.js"></script>
	<script type="text/javascript" src="js/vendors/inputmask/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="js/vendors/inputmask/inputmask.numeric.extensions.js"></script>
	<script type="text/javascript" src="js/fa-deals.js"></script>


	<script type="text/javascript">
		function RegisterCpwFormsExtension(extension) {
			if (!window.fa)
				window.fa = {}
			if (!window.fa.Extensions)
				window.fa.Extensions = {};

			window.fa.Extensions[extension.key] = extension;

			var readOnlyDeals = true;
			if ('fa' === extension.key) {
				var controller = extension.forms.f_main.CreateController(readOnlyDeals);
				var content = <?= nice_json_encode($deals) ?>;
				controller.SetFormContent(content, {name: '<?= $debtor_name ?>'});
				controller.Render('#fa-deals-content');
			}
		}
	</script>
<? endif; ?>
</head>

<body>
<? if (null==$error_message) : ?>
	<div id="fa-deals-content">Здесь должны быть сделки!</div>
<? else : ?>
	<h1>При попытке отображения сделок возникла ошибка</h1>
	<pre><?= prepare_xml_for_pre($error_message) ?></pre>
<? endif; ?>
</body>

</html>