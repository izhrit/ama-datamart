spec = {
  "openapi": "3.0.0",
  "servers": [   
	{
      "url": "https://datamart-test.rsit.ru/api.php",
      "description": "тестовый сервер Витрины данных ПАУ"
    },
    {
      "url": "https://datamart.rsit.ru/api.php",
      "description": "рабочий сервер Витрины данных ПАУ"
    },   
	{
      "url": "http://192.168.0.93:81/dm/api.php",
      "description": "тестовый сервер Витрины данных ПАУ из локальной сети РИТ"
    },
	{
      "url": "http://local.test/dm/api.php",
      "description": "локальный тестовый сервер разработчика"
    }
  ],
  "info": {
    "version": "0.2",
    "title": "API Витрины данных ПАУ",
    "description": "Методы для взаимодействия с сервером Витрины данных ПАУ.\n\nДанные на Витрину данных загружаются из ПАУ и выгружаются в BankroTECH\n * [ПАУ](http://www.russianit.ru/software/bunkrupt/) - программный продукт \"Помощник Арбитражного Управляющего\", для автоматизации прведения процедур банкротства арбитражными управляющими (АУ)\n * [BankroTECH](https://www.bankro.tech/) - информационная система по ведению процедур банкротства кредиторами \n"
  },
  "tags": [
    {
      "name": "Авторизация",
      "description": "проверка и предоставление прав на прочие действия"
    },
    {
      "name": "Из ИС Абонентов",
      "description": "передача данных на Витрину из ИС Абонентов"
    },
    {
      "name": "Из ПАУ",
      "description": "передача данных на Витрину из ПАУ"
    },
    {
      "name": "Из BankroTECH",
      "description": "получение данных с Витрины для ЦТБ"
    }
  ],
  "paths": {
    "/Auth": {
      "post": {
        "tags": [
          "Авторизация"
        ],
        "summary": "Получить токен доступа для дальнейшей работы",
        "description": "Метод проверяет предоставленные ему логин/пароль и для известных системе параметров возвращает токен доступа, необходимый во всех методах как параметр access_token",
        "parameters": [
          {
            "in": "query",
            "name": "login",
            "description": "Логин пользователя API",
            "required": true,
            "schema": {
              "type": "string"
            },
            "example": 1
          },
          {
            "in": "query",
            "name": "category",
            "description": "Категория пользователя API\n * customer - покупатель ПАУ (пароль для кабинета на rsit.ru выданный при оплате)\n * manager - Арбитражный Управляющий (пароль может выдать покупатель в кабинете на rsit.ru)\n * partner - Внешняя информационная система (пароль может выдать РИТ)\n",
            "schema": {
              "type": "string",
              "enum": [
                "customer",
                "manager",
                "partner"
              ],
              "default": "customer"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/x-www-form-urlencoded": {
              "schema": {
                "type": "object",
                "properties": {
                  "password": {
                    "description": "Пароль пользователя API",
                    "type": "string"
                  }
                },
                "required": [
                  "password"
                ],
                "example": {
                  "password": 1
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Успешно сформированный токен авторизации",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "token": {
                      "type": "string",
                      "example": "b5943b3e273259640b4d6c373da389edc501c2968c378a6307c670f596399d78"
                    }
                  }
                }
              }
            }
          },
          "401": {
            "description": "Отказано в доступе"
          }
        }
      }
    },
    "/RegistrateBankruptcyPetition": {
      "post": {
        "tags": [
          "Из ИС Абонентов"
        ],
        "summary": "Зарегистрировать информацию о поданном заявлении о банкротстве",
        "description": "Метод регистрирует информацию о поданном заявлении о банкротстве, чтобы автоматически заполнять параметры процедуры при её регистрации в ПАУ",
        "security": [
          {
            "Доступ по токену": []
          }
        ],
        "parameters": [
          {
            "in": "query",
            "name": "court[name]",
            "description": "Наименование арбитражного суда в который было подано заявление",
            "required": true,
            "schema": {
              "type": "string"
            },
            "example": "Арбитражный суд города Москвы"
          }
          ,{
            "in": "query",
            "name": "debtor[category]",
            "description": "Категория должника. Один из вариантов:\n * citizen - для физического лица\n * organization - для юридического лица",
            "required": true,
            "schema": {
              "type": "string"
            },
            "example": "citizen"
          }
          ,{
            "in": "query",
            "name": "debtor[name]",
            "description": "Наименование должника (ФИО для физ лица)",
            "required": true,
            "schema": {
              "type": "string"
            },
            "example": "Лосев Лось Лосевич"
          }
          ,{
            "in": "query",
            "name": "debtor[inn]",
            "description": "ИНН должника",
            "schema": {
              "type": "string"
            },
            "example": "739670329476"
          }
          ,{
            "in": "query",
            "name": "debtor[snils]",
            "description": "СНИЛС должника",
            "schema": {
              "type": "string"
            },
            "example": "14115456526"
          }
          ,{
            "in": "query",
            "name": "debtor[ogrn]",
            "description": "ОГРН должника",
            "schema": {
              "type": "string"
            },
            "example": "318699839159011"
          }
        ],
        "responses": {
          "200": {
            "description": "Успешно сформированный токен авторизации",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "ok": {
                      "type": "boolean",
                      "required": true,
                      "example": "true"
                    }
                    ,"message": {
                      "type": "string",
                      "required": false,
                      "example": "wrong karma"
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/UploadProcedureInfo": {
      "post": {
        "tags": [
          "Из ПАУ"
        ],
        "summary": "Загрузить информацию о процедуре на Витрину",
        "description": "Метод позволяет загрузить информацию о банкротной процедуре из ПАУ на Витрину.\n\nДанные загружаются в виде сжатого zip ом xml файла.\n\nВ параметрах URL метода указывается исчерпывающая информация для идентификации: \n * должника (ИНН, а также ОГРН для юр.лица или СНИЛС для физ.лица)\n * процедуры (Номер судебного дела, тип процедуры)\n * Арбитражного управляющего (номер на ЕФРСБ и ФИО)\n \nОбязательно должны быть указаны либо ОГРН либо СНИЛС.\n \nПри отсутствии на витрине необходимых записей (АУ или процедуры), записи заводятся автоматически.\n\nПри конфликте с ранее загруженными данными, загрузка происходит с пометкой о конфликте. \n\nКонфликт пока должны разрешать операторы ИС \"Витрин аданных ПАУ\".\n\nВарианты конфликтов (список не исчерпывающий):\n * данные уже ранее загружались от лица другого покупателя ПАУ\n * данные уже ранее загружались от лица другого АУ\n * данные уже загружались с другим номером судебного дела\n",
        "security": [
          {
            "Доступ по токену": []
          }
        ],
        "parameters": [
          {
            "in": "query",
            "name": "manager[surname]",
            "required": true,
            "description": "Фамилия Арбитражного Управляющего",
            "schema": {
              "type": "string"
            },
            "example": "Иванов"
          },
          {
            "in": "query",
            "name": "manager[name]",
            "required": true,
            "description": "Имя Арбитражного Управляющего",
            "schema": {
              "type": "string"
            },
            "example": "Иван"
          },
          {
            "in": "query",
            "name": "manager[patronymic]",
            "required": true,
            "description": "Отчество Арбитражного Управляющего",
            "schema": {
              "type": "string"
            },
            "example": "Иванович"
          },
          {
            "in": "query",
            "name": "manager[efrsb_number]",
            "required": true,
            "description": "Номер Арбитражного Управляющего на ЕФРСБ",
            "schema": {
              "type": "string"
            },
            "example": "н1"
          },
          {
            "in": "query",
            "name": "manager[bt]",
            "description": "Аккаунт Арбитражного Управляющего на Bankro.TECH",
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "query",
            "name": "debtor[name]",
            "required": true,
            "description": "Наименование должника",
            "schema": {
              "type": "string"
            },
            "example": "ООО Роги и ноги"
          },
          {
            "in": "query",
            "name": "debtor[inn]",
            "required": true,
            "description": "ИНН должника",
            "schema": {
              "type": "number"
            },
            "example": 3664069397
          },
          {
            "in": "query",
            "name": "debtor[ogrn]",
            "description": "ОГРН должника - юр.лица",
            "schema": {
              "type": "number"
            },
            "example": 1053600591197
          },
          {
            "in": "query",
            "name": "debtor[snils]",
            "description": "СНИЛС должника - физ.лица",
            "schema": {
              "type": "number"
            }
          },
          {
            "in": "query",
            "name": "debtor[category]",
            "description": "Категория организации - должника, c особенностями банкротства из главы IX ЗОБ  (127-ФЗ):\n * Г - градообразующая организация (§ 2. главы IX ЗОБ)\n * C - сельскохозяйственная организация (§ 3. главы IX ЗОБ)\n * Ф - финансовая организация (§ 4. главы IX ЗОБ)\n * К - кредитная организация (§ 4.1. главы IX ЗОБ)\n * Р - стратегическое предприятие или организация (§ 5. главы IX ЗОБ)\n * М - субъект естественных монополий (§ 6. главы IX ЗОБ)\n * З - застройщик (§ 7. главы IX ЗОБ)\n * Л - участник клиринга или клиент участника клиринга (§ 8. главы IX ЗОБ)\n",
            "schema": {
              "type": "string",
              "enum": [
                "Г",
                "С",
                "Ф",
                "К",
                "Р",
                "М",
                "З",
                "Л"
              ]
            }
          },
          {
            "in": "query",
            "required": true,
            "name": "procedure[type]",
            "description": "Тип проецдуры в соответствии со ст.2 ЗОБ (127-ФЗ)\n * Н - Наблюдение\n * КП - Конкурсное производство\n * ВУ - Внешнее управление\n * РД - Реструктуризация долгов\n * РИ - Реализация имущества\n * ОД - Отсутствующий должник\n",
            "schema": {
              "type": "string",
              "enum": [
                "Н",
                "КП",
                "ВУ",
                "РД",
                "РИ",
                "ОД"
              ]
            },
            "example": "Н"
          },
          {
            "in": "query",
            "required": true,
            "name": "procedure[case_number]",
            "description": "Номер судебного дела",
            "schema": {
              "type": "string"
            },
            "example": "номер несуществующего дела 1"
          },
          {
            "in": "query",
            "required": true,
            "name": "ctb_allow",
            "description": "Разрешение на передачу данных BankroTECH",
            "schema": {
              "type": "boolean",
              "default": false
            },
            "example": false
          },
          {
            "in": "query",
            "required": true,
            "name": "ama-version",
            "description": "Версия ПАУ из которой происходит выгрузка",
            "schema": {
              "type": "string",
              "default": "3.9.8.1"
            },
            "example": "3.9.8"
          },
          {
            "in": "query",
            "required": false,
            "name": "content-hash",
            "description": "hash содержимого zip-файла",
            "schema": {
              "type": "string",
              "default": "0"
            },
            "example": "3489567DF"
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/xml": {
              "schema": {
                "type": "string"
              }
            },
            "application/zip": {
              "schema": {
                "type": "string",
                "format": "binary"
              }
            },
            "application/null": {
              "schema": {
                "type": "string",
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Загрузка успешно завершена",
			"content": {
				"application/json": {
					"schema": {
						"type": "object",
						"properties": {
							"Signature": {
								"type": "string",
							}
						}
					}
				}
			}
          }
        }
      }
    },
    "/GetChangedProcedures": {
      "post": {
        "summary": "Получить список процедур, информация о которых загружалась на Витрину",
        "tags": [
          "Из BankroTECH"
        ],
        "security": [
          {
            "Доступ по токену": []
          }
        ],
        "parameters": [
          {
            "in": "query",
            "name": "ama-dm-revision-greater-than",
            "schema": {
              "type": "integer",
              "default": 0
            },
            "example": 0
          },
          {
            "in": "query",
            "name": "limit",
            "schema": {
              "type": "integer",
              "default": 1000
            },
            "example": 10
          },
          {
            "in": "query",
            "name": "procedure[case_number]",
            "schema": {
              "type": "string",
              "default": ""
            },
            "example": "А71-14496/2013"
          }
        ],
        "responses": {
          "200": {
            "description": "Загрузка успешно завершена",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "Запрошен_список": {
                      "type": "object",
                      "properties": {
                        "процедур_загруженных_после_ревизии": {
                          "type": "integer"
                        },
                        "ограниченый_длиной_не_более": {
                          "type": "integer",
                          "example": 10
                        }
                      }
                    },
                    "Всего_после_указанной_в_запросе_ревизии_загружено_процедур": {
                      "type": "integer"
                    },
                    "Запрошенный_список_процедур": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "id_MProcedure": {
                            "type": "string",
                            "example": 1
                          },
                          "revision": {
                            "type": "integer",
                            "example": 3
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/DownloadProcedureInfo": {
      "post": {
        "tags": [
          "Из BankroTECH"
        ],
        "summary": "Выгрузить информации о процедуре с Витрины",
        "description": "Метод позволяет выгрузить информацию о банкротной процедуре с Витрины ПАУ.",
        "security": [
          {
            "Доступ по токену": []
          }
        ],
        "parameters": [
          {
            "in": "query",
            "name": "id_MProcedure",
            "required": false,
            "description": "Идентификатор процедуры на Витрине",
            "schema": {
              "type": "number"
            },
            "example": 0
          }
        ],
        "responses": {
          "200": {
            "description": "Загрузка успешно завершена",
            "headers": {
              "ama-dm-revision": {
                "description": "Номер ревизии информации о процедуре",
                "schema": {
                  "type": "integer"
                }
              }
            }
          }
        }
      }
      },
      "/GetWcalendar": {
          "post": {
              "summary": "Получить производственный календарь",
              "tags": [
                  "Из BankroTECH"
              ],
              "security": [
                  {
                      "Доступ по токену": []
                  }
              ],
              "parameters": [
                  {
                      "in": "query",
                      "name": "year-greater-than",
                      "schema": {
                          "type": "integer",
                          "default": 0
                      },
                      "example": 2020
                  },
                  {
                      "in": "query",
                      "name": "revision-greater-than",
                      "schema": {
                          "type": "integer",
                          "default": 0
                      },
                      "example": 0
                  }
              ],
              "responses": {
                  "200": {
                      "description": "Загрузка успешно завершена",
                      "content": {
                          "application/json": {
                              "schema": {
                                  "type": "object",
                                  "properties": {
                                      "Запрошен_список": {
                                          "type": "object",
                                          "properties": {
                                              "изменения_загруженных_после_ревизии": {
                                                  "type": "integer"
                                              },
                                              "начиная_с_года": {
                                                  "type": "integer",
                                                  "example": 2020
                                              }
                                          }
                                      },
                                      "Запрошенные_дни": {
                                          "type": "array",
                                          "items": {
                                              "type": "object",
                                              "properties": {
                                                  "Year": {
                                                      "type": "integer",
                                                      "example": 2020
                                                  },
                                                  "Revision": {
                                                      "type": "integer",
                                                      "example": 10
                                                  },
                                                  "Days": {
                                                      "type": "string"
                                                  }
                                              }
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
          }
      }
  },
  "components": {
    "securitySchemes": {
      "Доступ по токену": {
        "description": "---\n\nДля работы с API Витрины данных ПАУ,\n\nнеобходимо получить токен доступа методом Авторизации \"**Auth**\"\n\nв дальнейшем этот токен всегда должен подаваться как поле в header\n\nama-datamart-access-token:...\n\nAPI генерирует одноразовый токен с ограниченным временем действия.\n\n---\n",
        "type": "apiKey",
        "in": "header",
        "name": "ama-datamart-access-token"
      }
    }
  }
}