<?
require_once '../assets/config.php';
require_once '../assets/helpers/log.php';

write_to_log('------------- committee from ama ----------');

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';

require_once '../assets/libs/log/access_log.php';

$id_Meeting= (isset($_GET['id_Meeting']) && $_GET['id_Meeting']) ? $_GET['id_Meeting'] : 'false';
$details= array('id_Meeting'=>$id_Meeting);
write_to_access_log_details('electrokk/open/ama',nice_json_encode($details));

function write_error_to_access_log($error_text)
{
	$details= array(
		'error'=>$error_text
		,'GET'=>$_GET
	);
	write_to_access_log_details('electrokk/open/ama/error',nice_json_encode($details));
}

session_start();

global $_SESSION, $auth_info, $test_settings;
if (isset($test_settings) && isset($test_settings->use_test_time) && true==$test_settings->use_test_time && isset($_GET['use-test-time']))
{
	require_once '../assets/helpers/time.php';
	safe_store_test_time();
}

$_SESSION['auth_info']= null;
unset($_SESSION['auth_info']);

if (isset($_GET['auth']))
{
	require_once '../assets/actions/backend/auto_login.php';
}
else
{
	write_error_to_access_log('skipped auth arg!');
}

?>
<html>
	<head>
		<title>Витрина данных ПАУ</title>
		<meta http-equiv="X-UA-Compatible" content="IE=11" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/pau-logo.png">
		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.custom.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>

		<script>
		if("<?=isset($_GET['enable_viewers2'])?true:false?>"||"<?=isset($_GET['ama'])?true:false?>"||'function'!=typeof (Intl.NumberFormat))
		{
			$.getScript('js/vendors/polyfill.js');
		}
		</script>  
		<link rel="stylesheet" type="text/css" href="css/vendors/fullcalendar/main.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/fullcalendar/tooltip.css" />
		<script type="text/javascript" src="js/vendors/fullcalendar/popper.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/tooltip.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/main.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/locales/ru.js"></script>
		<!-- вот это надо в extension ы! } -->

		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/datamart.css" />

		<script type="text/javascript" src="js/ui-fixes.js"></script>

		<script type="text/javascript">
		window.onerror = function (message, source, lineno)
		{
			var msg = "Ошибка: <b>" + message + "</b><br/><br/>\n" +
				"файл: <b>" + source + "</b><br/>\n" +
				"строка: <b>" + lineno + '</b>';
			var div = $("#cpw-unhandled-error-message-form");
			div.html(msg);
			div.dialog({
				modal: true,
				width:800,height:"auto",
				buttons: { Ok: function () { $(this).dialog("close"); } }
			});
		}
		app= { 
			id_Meeting: '<?= $id_Meeting ?>'
			, ContractNumber: '<?= !isset($auth_info->ContractNumber) ? '' : $auth_info->ContractNumber ?>'
			, АУ: '<?= !isset($auth_info->id_Manager) ? '' : "$auth_info->lastName $auth_info->firstName $auth_info->middleName" ?>'
			, externalOpenUrl: 'OpenDatamartUrl' in window.external
			, a_href_attrs: function(url)
			{
				return 'OpenDatamartUrl' in window.external && 'OpenUrl' in window.external
				? ( url.substring(0,4)!='http'
					? 'href="#" onclick="window.external.OpenDatamartUrl(\'/' + url + '\')"'
					: 'href="#" onclick="window.external.OpenUrl(\'' + url + '\')"'
				)
				: 'href="' + url.replace('/','\\') + '"';
			}
			, window_open: function(url, name, params)
			{
				if ('OpenDatamartUrl' in window.external)
					window.external.OpenDatamartUrl(encodeURI('/'+url));
				else
					window.open(encodeURI(url), name, params);
			}
		};
		function RegisterCpwFormsExtension(extension)
		{
			if ('ama'==extension.key)
			{
				var base_url= '<?= $datamart_bck_url ?>';
				var sel= 'body.cpw-ama > div.cpw-datamart-ui-content';
				var form_spec = extension.forms.committee_ama.CreateController({base_url:base_url});
				form_spec.CreateNew(sel);
			}
		}
		</script>

		<script type="text/javascript" src="js/ama-datamart.js?2020_11_27_1614"></script>
	</head>

	<body class="cpw-ama" oncontextmenu="return false">
		<div id="cpw-unhandled-error-message-form" title="Необработанная ошибка!"></div>
		<div class="cpw-datamart-ui-content">

			<div class="ui-placeholder">
				<div>
					<small>
						<?= date_format(date_create(),'Y-m-d H:i:s') ?><br/>
						<div style="text-align:right">формируется html-страница на сервере.</div><br/>
						Здесь на html-странице должна отобразиться<br/>
					</small>
					<p style="text-align:center;">

						Форма Комитета Кредиторов

					</p>
					<small>
						После загрузки и выполнения javascript.
						<br/><br/>
						<script>
							var t= new Date();
							var m= t.getMonth()+1;
							var d= t.getDate();
							if (d<10)
								d= '0' + d;
							if (m<10)
								m= '0' + m;
							document.write(t.getFullYear()+'-'+m+'-'+d+' '+t.toLocaleTimeString());
						</script>
						<div style="text-align:right">выполняется javascript в браузере.</div><br/>
					</small>
					<center><img src="img/loading-spinner.gif" /></center>
				</div>
			</div>

		</div>
	</body>
</html>
<?

$details= headers_list();
write_to_access_log_details('electrokk/open/ama/ok',nice_json_encode($details));

?>