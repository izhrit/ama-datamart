#! /bin/sh

echo "$(date) ------------- start deploy -----------------"

DATE=$(date +"%Y%m%d%H%M")

. ./deploy_config.sh

backup_path=${backups_path}/${DATE}
mkdir ${backup_path}

backup_db()
{
	echo "$(date)   backup_db {"
	mysqldump_options='--hex-blob --default-character-set=utf8 --set-charset --routines --no-autocommit --extended-insert'
	mysqldump --defaults-extra-file=./mysql.conf $mysqldump_options --result-file=$backup_path/backup.sql $DBNAME
	echo "$(date)     zip"
	zip -D $backup_path/backup.zip $backup_path/backup.sql
	echo "$(date)     delete sql"
	rm $backup_path/backup.sql
	echo "$(date)   }"
}

backup_log()
{
	echo "$(date)   backup_log {"
	cp $log_path $backup_path/
	echo '' > $log_path
	echo "$(date)   }"
}

backup_src()
{
	echo "$(date)   backup_src {"
	cp -r -f $web_path    $backup_path/
	cp -r -f $assets_path $backup_path/
	echo "$(date)   }"
}

backup_src_config()
{
	echo "$(date) backup_src_config {"
	backup_src
	echo "$(date)   backup config"
	cp -r -f $config_path $backup_path/
	echo "$(date) }"
}

backup_db_log()
{
    echo "$(date) backup_db_log {"
    backup_db
    backup_log
    echo "$(date) }"
}

migrate_db()
{
    echo "$(date) migrate_db {"
	echo "$(date) fix migration numbers {"
	./run_mysql.sh < $repo_path/src/dm/db/migrations/fix_migration_numbers.sql
	echo "$(date) fix migration numbers }"
    for filepath in $repo_path/src/dm/db/migrations/m*.sql
    do
	filename=$(basename "$filepath" ".sql")
	migrated=`echo "select count(*) from tbl_migration where concat(MigrationNumber,'_',MigrationName)='$filename'" | ./run_mysql.sh | sed 1d`
	if [ $migrated -eq 0 ]; then
	    echo "$(date)    $filename {"
	    ./run_mysql.sh < $filepath
	    echo "$(date)    }"
	fi
    done
    echo "$(date) }"
}

deploy_assets()
{
    echo "$(date) deploy_assets {"
    set -x
    # echo "$(date)   actions"
    cp -r -f -u -v $repo_path/src/dm/assets/actions   $assets_path/
    # echo "$(date)   helpers"
    cp -r -f -u -v $repo_path/src/dm/assets/helpers   $assets_path/
    # echo "$(date)   PHPMailer"
    cp -r -f -u -v $repo_path/src/dm/assets/PHPMailer $assets_path/
    # echo "$(date)   views"
    cp -r -f -u -v $repo_path/src/dm/assets/views     $assets_path/
    set +x
    echo "$(date) }"
}

deploy_jobs()
{
    echo "$(date) deploy_jobs {"
    set -x
    cp -r -f -u -v $repo_path/src/dm/jobs/*   $jobs_path/
    set +x
    echo "$(date) }"
}

block_web()
{
    echo "$(date) block_web {"
    cp -r -f $repo_path/src/dm/wblocked/*.php $web_path/
    echo "$(date) }"
}

deploy_web()
{
    echo "$(date) deploy_web {"
    cp -r -f $repo_path/src/dm/web/* $web_path/
    echo "$(date) }"
}

backup_src_config
block_web
backup_db_log
migrate_db
deploy_assets
deploy_jobs
deploy_web
