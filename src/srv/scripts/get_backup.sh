#! /bin/sh

DATE=$(date +"%Y%m%d%H%M")

mysqldump --defaults-extra-file=./mysql.conf --hex-blob --default-character-set=utf8 --set-charset --routines --no-autocommit --extended-insert --result-file=./backup/db/b.$DATE.sql datamart-test
