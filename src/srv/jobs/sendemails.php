<?

require_once '../assets/config.php';
require_once '../assets/actions/backend/alib_emails.php';

exit();

function Job_send_emails()
{
	global $job_params;

	if (file_exists($job_params->pid_file_path_for_send_email_job))
	{
		write_to_log('job send emails pid file exists!');
		exit();
	}

	file_put_contents($job_params->pid_file_path_for_send_email_job,date_format(date_create(),'Y-m-d\TH:i:s'));

	try
	{
		SendPortionOfEmailToSend(/* $max_portion_size= */100);
	}
	catch (Exception $ex) 
	{
		unlink($job_params->pid_file_path_for_send_email_job);
		throw $ex;
	}
	unlink($job_params->pid_file_path_for_send_email_job);
}

try
{
	Job_send_emails();
}
catch (Exception $exception)
{
	$exception_class= get_class($exception);
	$exception_Message= $exception->getMessage();
	write_to_log("Unhandled exception in job send emails occurred: $exception_class - $exception_Message");
	throw $exception;
}


