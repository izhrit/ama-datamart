@for /f "eol=# delims== tokens=1,2" %%i in (%~dp0..\..\etc\settings.txt) do @set %%i=%%j
@if exist %~dp0..\..\etc\settings.local.txt @for /f "eol=# delims== tokens=1,2" %%i in (%~dp0\..\..\etc\settings.local.txt) do @set %%i=%%j

@call %PHP% parse_handbook.php %*
