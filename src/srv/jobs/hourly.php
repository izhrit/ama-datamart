<?php

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';
require_once '../assets/helpers/job.php';
require_once '../assets/helpers/throw.php';

global $job_params;

if (!isset($job_params))
{
	echo '$job_params is undefined!';
	exit();
}

$job_db_logger= new Job_db_logger();
$job_db_logger->job= (object)array('title'=>'hourly','description'=>'ежечасные действия', 'max_age_minutes'=>60);

if (!isset($job_params->pid_file_path_for_hourly_job))
{
	echo '$job_params->pid_file_path_for_hourly_job is undefined!';
	exit();
}

require_once '../assets/libs/job_parts/hourly_job_parts.php';

$hourly_job_pid_file_path= $job_params->pid_file_path_for_hourly_job;

safe_execute_job_parts_locked_by_pid_file_with_db_log($hourly_job_parts,$hourly_job_pid_file_path,$job_db_logger);