<?php

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';

global $echo_errors, $do_not_write_errors_to_log;
$echo_errors= true;
$do_not_write_errors_to_log= true;

$fields_list= 'select d.id_PData
, d.publicDate
, p.id_Manager
, concat(m.LastName," ",left(m.FirstName,1)," ",left(m.MiddleName,1)) manager_FIO
, m.efrsbNumber efrsbNumber
, b.Name debtor_Name
, b.INN debtor_INN
, b.OGRN debtor_OGRN
, b.SNILS debtor_SNILS
, p.casenumber
, d.fileData';

$from_where= '
from MProcedure p
inner join Debtor b on b.id_Debtor=p.id_Debtor
inner join Manager m on p.id_Manager=m.id_Manager
inner join PData d on p.id_MProcedure=d.id_MProcedure and p.ctb_revision=d.revision
where d.ctb_allowed<>0 and "v"=p.VerificationState and "v"=m.VerificationState and "v"=b.VerificationState';

$rows_in_portion= 100;

function request_data($publicDate= null, $id_PData= null)
{
	global $fields_list, $from_where, $rows_in_portion;

	echo "----------\r\n";
	if (null==$publicDate)
	{
		$txt_query= $fields_list.$from_where."
order by d.publicDate desc, d.id_PData desc
limit $rows_in_portion;";
		echo "request for $rows_in_portion rows..\r\n";
		$rows= execute_query($txt_query, array());
	}
	else
	{
	$txt_query= $fields_list.$from_where."
and d.publicDate<? and d.id_PData<?
order by d.publicDate desc, d.id_PData desc
limit $rows_in_portion;";
		echo "request for $rows_in_portion rows where publicDate<'$publicDate' and id_PData<$id_PData ..\r\n";
		$rows= execute_query($txt_query, array('ss',$publicDate,$id_PData));
	}

	
	$count_rows= count($rows);
	echo ".. got $count_rows rows.\r\n";

	return $rows;
}

$processed_zip_count= 0;
$processed_zip_size= 0;

function process_report($row,$report)
{
	$dir= "reports/$row->id_PData";
	mkdir($dir);
	echo "   got report!\r\n";

	file_put_contents("$dir/report.xml",$report);

	unset($row->fileData);

	$row->АУ= array(
		'ФИО'=>$row->manager_FIO
		,'РегНомер'=>$row->efrsbNumber
	);
	unset($row->manager_FIO);
	unset($row->efrsbNumber);

	$row->Должник= array(
		'Имя'=>$row->debtor_Name
		,'ИНН'=>$row->debtor_INN
		,'ОГРН'=>$row->debtor_OGRN
		,'СНИЛС'=>$row->debtor_SNILS
	);
	unset($row->debtor_Name);
	unset($row->debtor_INN);
	unset($row->debtor_OGRN);
	unset($row->debtor_SNILS);

	file_put_contents("$dir/dbinfo.json.txt",nice_json_encode($row));
}

function process_row($row)
{
	global $processed_zip_count, $processed_zip_size;

	$zip_size= strlen($row->fileData);
	echo "process id_PData={$row->id_PData}, size of zip: $zip_size\r\n";

	$tf= tmpfile();
	try
	{
		fwrite($tf,$row->fileData);
		fflush($tf);

		$meta_data= stream_get_meta_data($tf);
		$filepath= $meta_data['uri'];

		$zip = zip_open($filepath);
		while ($zip_entry = zip_read($zip))
		{
			$zip_entry_name= zip_entry_name($zip_entry);
			if ('report.xml'==$zip_entry_name)
			{
				$report= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
				process_report($row,$report);
			}
		}
		zip_close($zip);
		fclose($tf); // Этот файл автоматически удаляется после закрытия
	}
	catch (Exception $ex)
	{
		fclose($tf); // Этот файл автоматически удаляется после закрытия
		throw $ex;
	}

	$processed_zip_count++;
	$processed_zip_size+= $zip_size;
}

function main()
{
	global $processed_zip_count, $processed_zip_size;

	mkdir('reports');

	for ($rows= request_data(), $count_rows= count($rows);
		0!=$count_rows;
		$rows= request_data($row->publicDate, $row->id_PData), $count_rows= count($rows))
	{
		foreach ($rows as $row)
		{
			process_row($row);
		}
		echo "processed $processed_zip_count zip files. total size $processed_zip_size\r\n";
	}
}

try
{
	main();
}
catch (Exception $exception)
{
	$exception_class= get_class($exception);
	$exception_Message= $exception->getMessage();
	echo "\r\nUnhandled exception occurred: $exception_class - $exception_Message\r\n";
	throw $exception;
}
