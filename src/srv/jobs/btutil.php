<?php

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';

require_once '../assets/libs/upload2bt/logger.php';
require_once '../assets/helpers/client.rest.php';
require_once '../assets/libs/upload2bt/client.bt.php';
require_once '../assets/actions/backend/constants/debtorName.etalon.php';
require_once '../assets/actions/backend/constants/alib_constants.php';
require_once '../assets/libs/upload2bt/uploader.bt.php';
require_once '../assets/libs/upload2bt/upload_from_db.php';
require_once '../assets/libs/upload2bt/uploader.mock.php';
require_once '../assets/libs/codecs/registry.codec.php';
require_once '../assets/libs/upload2bt/upload_registry.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';

global $echo_errors;
$echo_errors= true;

function prepare_password($password)
{
	global $bt_password_options;
	return sync_decrypt(base64_decode($password),$bt_password_options);
}

function get_au_account($id_Manager)
{
	echo "id_Manager=$id_Manager\r\n";

	$txt_query= "select BankroTechAcc from Manager where id_Manager=?;";
	$rows= execute_query($txt_query,array('s',$id_Manager));
	$count_rows= count($rows);
	if (1!=$count_rows)
	{
		echo "found $count_rows rows from Manager where id_Manager=$id_Manager\r\n";
	}
	else
	{
		$row= $rows[0];
		if (null==$row->BankroTechAcc)
		{
			echo "null==row->BankroTechAcc\r\n";
		}
		else
		{
			global $bt_password_options;
			echo "{$row->BankroTechAcc}\r\n";
			$cr= json_decode($row->BankroTechAcc);
			$password= prepare_password($cr->password);
			echo "password=\"$password\"\r\n";
		}
	}
}

function mb_str_pad( $input, $pad_length, $pad_string = ' ', $pad_type = STR_PAD_RIGHT)
{
	$diff = strlen( $input ) - mb_strlen( $input );
	return str_pad( $input, $pad_length + $diff, $pad_string, $pad_type );
}

function get_astat()
{
	$txt_query= "select id_Manager, LastName, FirstName, MiddleName, BankroTechAcc, BankroTechAccVerified from Manager where BankroTechAcc is not null;";
	$rows= execute_query($txt_query,array());
	$i= 1;
	$count_rows= count($rows);
	$counts= (object)array('can_not_decode'=>0,'can_not_login'=>0,'can_not_get_projects'=>0,'have_projects'=>0);
	foreach ($rows as $row)
	{
		$num= str_pad($i,4,' ');
		$v= null==$row->BankroTechAccVerified ? ' ' : 0==$row->BankroTechAccVerified ? '0' : '1';
		$fio= mb_str_pad($row->LastName.' '.mb_substr($row->FirstName,0,1).'.'.mb_substr($row->MiddleName,0,1).'.',30,' ');
		$id_Manager= str_pad($row->id_Manager,10,' ');
		echo "$num) $id_Manager ($fio$v) projects: ";
		try
		{
			$cr= json_decode($row->BankroTechAcc);
			$cr->password= prepare_password($cr->password);
		}
		catch (Exception $ex)
		{
			echo "can not decode account {$row->BankroTechAcc}!\r\n";
			$counts->can_not_decode++;
			$i++;
			continue;
		}
		$btclient= new Bankro_tech_client(new Rest_client(new Logger()));
		try
		{
			$btclient->login($cr->domain_name,$cr->login,$cr->password);
		}
		catch (Exception $ex)
		{
			echo "can not login {$row->BankroTechAcc}!\r\n";
			$counts->can_not_login++;
			$i++;
			continue;
		}
		try
		{
			$p= $btclient->GetGroupedProjects_NotArchived();
			$count_projects= !isset($p[0]->Projects) ? 0 : count($p[0]->Projects);
			echo "$count_projects\r\n";
		}
		catch (Exception $ex)
		{
			echo "can not get projects {$row->BankroTechAcc}!\r\n";
			$counts->can_not_get_projects++;
		}
		if (0!=$count_projects)
		{
			$counts->have_projects++;
			echo "!\r\n";
		}
		$i++;
	}
	echo "Указали аккаунт на bankro.tech:    $count_rows\r\n";
	echo "не удалось расшифровать аккаунт:   {$counts->can_not_decode}\r\n";
	echo "не удалось авторизоваться:         {$counts->can_not_login}\r\n";
	echo "не удалось получить проекты:       {$counts->can_not_get_projects}\r\n";
	echo "удалось получить более 0 проектов: {$counts->have_projects}\r\n";
}

function load_id_MProcedure_for_project_Name($name,$id_Manager)
{
	$prefix= 'Дело №';
	$postfix= ', должник: ';
	$pos0= mb_strpos($name,$prefix);
	if (0==$pos0)
	{
		$posz= mb_strpos($name,$postfix);
		if (false!=$posz)
		{
			$casenumber= mb_substr($name,mb_strlen($prefix),$posz-mb_strlen($prefix));
			$txt_query= 'select id_MProcedure, id_Manager from MProcedure where casenumber=?;';
			$rows= execute_query($txt_query,array('s',$casenumber));
			if (0!=count($rows))
			{
				echo ', id_MProcedure=(';
				$i= 0;
				foreach ($rows as $row)
				{
					if (0!=$i)
						echo ',';
					echo $row->id_MProcedure;
					if ($id_Manager!=$row->id_Manager)
						echo "(id_Manager={$row->id_Manager})";
					$i++;
				}
				echo ')';
			}
		}
	}
}

function get_au_projects($id_Manager)
{
	echo "id_Manager=$id_Manager\r\n";

	$txt_query= "select BankroTechAcc from Manager where id_Manager=?;";
	$rows= execute_query($txt_query,array('s',$id_Manager));
	$count_rows= count($rows);
	if (1!=$count_rows)
	{
		echo "found $count_rows rows from Manager where id_Manager=$id_Manager\r\n";
	}
	else
	{
		$row= $rows[0];
		if (null==$row->BankroTechAcc)
		{
			echo "null==row->BankroTechAcc\r\n";
		}
		else
		{
			global $bt_password_options;
			echo "{$row->BankroTechAcc}\r\n";
			$cr= json_decode($row->BankroTechAcc);
			$password= prepare_password($cr->password);

			$btclient= new Bankro_tech_client(new Logged_rest_client(new Logger()));
			$btclient->login($cr->domain_name,$cr->login,$password);

			$groups= $btclient->GetGroupedProjects_NotArchived();
			$count_groups= count($groups);
			echo "loaded $count_groups project_groups\r\n";
			if (0!=$count_groups)
			{
				$group= $groups[0];
				$count_projects= count($group->Projects);
				echo "loaded $count_projects projects\r\n";
				$i= 1;
				foreach ($group->Projects as $project)
				{
					$num= str_pad($i,4,' ');
					echo "$num) {$project->Name}";
					load_id_MProcedure_for_project_Name($project->Name,$id_Manager);
					echo "\r\n";
					$i++;
				}
			}
		}
	}
}

function main()
{
	global $argv, $prefixed_test_groups;
	$cmd= $argv[1];

	switch ($cmd)
	{
		case 'account': get_au_account($argv[2]); break;
		case 'astat': get_astat(); break;
		case 'projects': get_au_projects($argv[2]); break;
	}
}

try
{
	main();
}
catch (Exception $exception)
{
	$exception_class= get_class($exception);
	$exception_Message= $exception->getMessage();
	echo "\r\nUnhandled exception occurred: $exception_class - $exception_Message\r\n";
	throw $exception;
}
