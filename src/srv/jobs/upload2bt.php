<?php

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';

require_once '../assets/helpers/job.php';

global $debtorName_beauty_prefixes;
require_once '../assets/libs/upload2bt/logger.php';
require_once '../assets/helpers/client.rest.php';
require_once '../assets/libs/upload2bt/client.bt.php';
require_once '../assets/actions/backend/constants/debtorName.etalon.php';
require_once '../assets/actions/backend/constants/alib_constants.php';
require_once '../assets/libs/upload2bt/uploader.bt.php';
require_once '../assets/libs/upload2bt/upload_from_db.php';
require_once '../assets/libs/upload2bt/uploader.mock.php';
require_once '../assets/libs/codecs/registry.codec.php';
require_once '../assets/libs/upload2bt/upload_registry.php';

global $echo_errors;
$echo_errors= true;

try
{
	echo "******************************************************************************** { \r\n";
	echo job_time()."   host:{$default_db_options->host}, user:{$default_db_options->user}, dbname:{$default_db_options->dbname}\r\n";

	$logged_rest_client= new Logged_rest_client(new Logger(), /*$atimeout_sec= */30);
	$uploader= new Registry2bt_uploader(new Bankro_tech_client($logged_rest_client));
	upload($uploader
		,/*$portion_size= */5
		,/*$max_count_to_update=*/10
		,/*$trace_db_select_query= */true
		,/*$id_Manager=*/null
	);

	echo "******************************************************************************** } \r\n";
}
catch (Exception $exception)
{
	$exception_class= get_class($exception);
	$exception_Message= $exception->getMessage();
	echo "\r\nUnhandled exception occurred: $exception_class - $exception_Message\r\n";
	throw $exception;
}
