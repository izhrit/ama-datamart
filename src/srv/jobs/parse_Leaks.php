<?php

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';
require_once '../assets/libs/parse_Leaks.php';

try
{
	echo "******************************************************************************** { \r\n";
	echo job_time()."   host:$default_db_options->host, user:$default_db_options->user, dbname:$default_db_options->dbname\r\n";
	ProcessPData();
	echo "******************************************************************************** } \r\n";
}
catch (Exception $exception)
{
	$exception_class= get_class($exception);
	$exception_Message= $exception->getMessage();
	echo "\r\nUnhandled exception occurred: $exception_class - $exception_Message\r\n";
	throw $exception;
}
