select 
 date(Time) day
 , Name
 , count(distinct id_Log) records
 , count(distinct IP) ips 
 , count(distinct Id) Ids 
from access_log
inner join log_type on access_log.id_Log_type=log_type.id_Log_type
where Time > DATE_SUB(now(), INTERVAL 7 DAY)
group by day, Name
order by day, Name;
